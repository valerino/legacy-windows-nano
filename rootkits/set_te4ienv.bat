@echo off

if "%1"=="" goto usage

@echo on
set SVN_ROOT=%1

set SVN_INC_PATH=%SVN_ROOT%/shared/inc
set SVN_INC_WIN_PATH=%SVN_ROOT%/shared/inc/win
set SVN_TERA_INC_PATH=%SVN_ROOT%/tera/include
set SVN_TERA_PATH=%SVN_ROOT%/tera

set SVN_LIB_PATH=%SVN_ROOT%/shared/lib
set SVN_LIB_WIN_PATH=%SVN_ROOT%/shared/lib/win

set SVN_ROOTKITS_WIN_PATH=%SVN_ROOT%/rootkits/windows
set SVN_ROOTKITS_WINMOBILE_PATH=%SVN_ROOT%/rootkits/mobile/winmobile
set SVN_ROOTKITS_SHARED_PATH=%SVN_ROOT%/rootkits/rkshared
set SVN_PLUGINS_WIN_PATH=%SVN_ROOTKITS_WIN_PATH%/plugins
set SVN_TOOLS_WIN_PATH=%SVN_ROOT%\shared\bin\win
set SVN_INFECTOR_INC_PATH=%SVN_ROOT%\infector\include
set KMDF_VERSION_PATH=1.9
set WDF_ROOT=%BASEDIR%\inc\wdf\kmdf\%KMDF_VERSION_PATH%

if "%2"=="-vc" goto setvcenv
goto continue1

:setvcenv
"%VS100COMNTOOLS%vsvars32.bat"

:continue1
if "%2"=="-legacy" goto setlegacy
goto exit

:setlegacy
"%VS100COMNTOOLS%vsvars32.bat"
goto exit

:usage
echo "usage: set_te4ienv.bat <driveletter> [-vc to set visualstudio env|-legacy to set legacy env]"

:exit
