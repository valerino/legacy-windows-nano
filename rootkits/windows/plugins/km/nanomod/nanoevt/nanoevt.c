#include <nanocore.h>
#include <wdf.h>
#include <klib.h>
#include <rkcfghandle.h>
#include <modrkcore.h>
#include <nanoevt.h>
#include "evtfs.h"

#define POOL_TAG 'tven'

typedef struct _NANOXML_FILTER_DATA
{
	PFLT_FILTER FilterHandle;
} NANOXML_FILTER_DATA, *PNANOXML_FILTER_DATA;

typedef enum _EvtRegistryType
{
	Registry_Create = 0,
	Registry_Open,
	Registry_SetValue,
	Registry_QueryValue,
	Registry_Delete,
	Registry_DeleteValue,
	Registry_Invalid = -1
} EvtRegistryType;

typedef enum _EvtSystemType
{
	System_Process_Create = 0,
	System_Process_Close,
	System_Module_Load,
	System_Module_Unload,
	System_Invalid = -1
} EvtSystemType;

typedef struct _nanoevt_reg_params
{
	WCHAR keyname[128];
	WCHAR vname[128];
	WCHAR vvalue[128];
} nanoevt_reg_params;

typedef struct _nanoevt_sys_params
{
	WCHAR filename[MAX_PATH];
} nanoevt_sys_params;

FLT_POSTOP_CALLBACK_STATUS FltPostCreateCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);
FLT_PREOP_CALLBACK_STATUS FltPreCloseCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext);
FLT_POSTOP_CALLBACK_STATUS FltPostCloseCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);
FLT_POSTOP_CALLBACK_STATUS FltPostReadCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);
FLT_POSTOP_CALLBACK_STATUS FltPostWriteCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);
FLT_POSTOP_CALLBACK_STATUS FltPostMountCallback(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);

NANOXML_FILTER_DATA FilterData;

CONST FLT_OPERATION_REGISTRATION Callbacks[] =
{
	{
		IRP_MJ_CREATE,
		0,
		fsevt_HandlePreCreate,
		fsevt_HandlePostCreate
	},
	{
		IRP_MJ_SET_INFORMATION,
		0,
		fsevt_HandlePreSetInformation,
		fsevt_HandlePostSetInformation
	},
	{
		IRP_MJ_OPERATION_END
	}
};

FLT_REGISTRATION FilterRegistration = 
{
	sizeof(FLT_REGISTRATION),
	FLT_REGISTRATION_VERSION,
	0,

	NULL,
	Callbacks,
	NULL,

	NULL,
	NULL,
	NULL,
	NULL,

	NULL,
	NULL,
	NULL
};

WDFDRIVER g_wdfDriver;
WDFDEVICE g_wdfControlDevice;
BOOLEAN g_bEnabled = TRUE;
PEPROCESS g_pSystemProcess;

/*
static EvtRegistryType GetRegistryTypeFromString(IN PUCHAR TypeString)
{
	PAGED_CODE();

	if(!TypeString)
		return Registry_Invalid;

	if(strcmp(TypeString, "create") == 0)
		return Registry_Create;
	else if(strcmp(TypeString, "open") == 0)
		return Registry_Open;
	else if(strcmp(TypeString, "setvalue") == 0)
		return Registry_SetValue;
	else if(strcmp(TypeString, "queryvalue") == 0)
		return Registry_QueryValue;
	else if(strcmp(TypeString, "delete") == 0)
		return Registry_Delete;
	else if(strcmp(TypeString, "deletevalue") == 0)
		return Registry_DeleteValue;

	return Registry_Invalid;
}
*/
/*
static EvtSystemType GetSystemTypeFromString(IN PUCHAR TypeString)
{
	PAGED_CODE();

	if(!TypeString)
		return System_Invalid;

	if(strcmp(TypeString, "moduleload") == 0)
		return System_Module_Load;
	else if(strcmp(TypeString, "moduleunload") == 0)
		return System_Module_Unload;
	else if(strcmp(TypeString, "processcreate") == 0)
		return System_Process_Create;
	else if(strcmp(TypeString, "processclose") == 0)
		return System_Process_Close;

	return System_Invalid;
}
*/

VOID nanoevt_EvtDeviceIoControl(IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DBG_OUT (("nanoevt_EvtDeviceIoControl called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
	default : 
		break;
	}

	WdfRequestComplete(Request, Status);
}

VOID nanoevt_EvtCreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanoevt_EvtDeviceCreate called\n"));
	WdfRequestComplete(Request, Status);
}
int nanoevt_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	UXmlNode *evtNode = NULL;
	UXmlAttribute *pEventAttribute = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	NTSTATUS status = STATUS_SUCCESS;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode != NULL)
	{
		if(strcmp(optNode->Name, "option") != 0)
			break;

		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(!NT_SUCCESS(status))
			break;

		if(pName)
		{
			if(strcmp(pName, "enabled") == 0)
			{
				if(pValue)
				{
					g_bEnabled = (BOOLEAN)KAtol(pValue);
					DBG_OUT (("nanoevt_parsecfg ENABLED = %s\n", pValue));
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	evtNode = optNode;
	if(strcmp(evtNode->Name, "events") == 0)
	{
		status = fsevt_ParseFilesystemEvents(evtNode);
		if(!NT_SUCCESS(status))
			return -1;
	}

	return 0;
}

int nanoevt_reinitcfg ()
{
	UXml *ConfigXml;
	UXmlNode *ModuleConfig;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	fsevt_Shutdown();
	fsevt_Initialize();

	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanoevt", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanoevt_parsecfg(ModuleConfig) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

__exit:	
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanoevt_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	uninstall
*
*/
int nanoevt_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	/// uninstall nanofs
	name.Buffer = WdfDriverGetRegistryPath(WdfDeviceGetDriver(g_wdfControlDevice));
	name.Length = (USHORT)str_lenbytesw(name.Buffer);
	name.MaximumLength = name.Length;
	Status = KDeleteKeyTree(&name,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = WdfDriverGetRegistryPath(WdfDeviceGetDriver(g_wdfControlDevice));
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

__exit:
	DBG_OUT (("nanoevt_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	WDF_DRIVER_CONFIG wdfConfig;
	WDF_OBJECT_ATTRIBUTES wdfObjectAttributes;
	PWDFDEVICE_INIT pDevInit = NULL;
	WDF_IO_QUEUE_CONFIG wdfIoQueueConfig;
	WDFQUEUE wdfQueue;
	WDF_FILEOBJECT_CONFIG wdfFileObjConfig;

	CHAR szPluginBuildString[32];
	UNICODE_STRING name;
	WCHAR buffer[256];

	DBG_OUT (("nanoevt DriverEntry\n"));

	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof(buffer);
	RtlUnicodeStringPrintf(&name, L"\\Device\\%s", RK_KMCORE_SYMLINK_NAME);
	if(!KWaitForObjectPresent(&name, TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return STATUS_UNSUCCESSFUL;
	}

	g_pSystemProcess = IoGetCurrentProcess();

	WDF_DRIVER_CONFIG_INIT(&wdfConfig, NULL);
	status = WdfDriverCreate(DriverObject, RegistryPath, WDF_NO_OBJECT_ATTRIBUTES, &wdfConfig, &g_wdfDriver);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("nanoevt DriverEntry cannot create wdf device, status %08x\n", status));
		return status;
	}

	pDevInit = WdfControlDeviceInitAllocate(g_wdfDriver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if(!pDevInit)
	{
		status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanoevt DriverEntry cannot crate deviceinit\n"));
		return status;
	}

	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof(buffer);
	RtlUnicodeStringPrintf(&name, L"\\Device\\%s", RK_NANOEVT_SYMLINK_NAME);
	status = WdfDeviceInitAssignName(pDevInit, &name);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("nanoevt DriverEntry cannot assign devinit name, status %08x\n", status));
		return status;
	}

	WDF_FILEOBJECT_CONFIG_INIT(&wdfFileObjConfig, nanoevt_EvtCreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(pDevInit, &wdfFileObjConfig, WDF_NO_OBJECT_ATTRIBUTES);

	WDF_OBJECT_ATTRIBUTES_INIT(&wdfObjectAttributes);
	status = WdfDeviceCreate(&pDevInit, &wdfObjectAttributes, &g_wdfControlDevice);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("nanoevt DriverEntry cannot create control device, status %08x\n", status));
		return status;
	}
	DBG_OUT (("nanoevt DriverEntry controldevice %x created\n", g_wdfControlDevice));

	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof(buffer);
	RtlUnicodeStringPrintf(&name, L"\\DosDevices\\%s", RK_NANOEVT_SYMLINK_NAME);
	status = WdfDeviceCreateSymbolicLink(g_wdfControlDevice, &name);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("nanoevt DriverEntry cannot create sym link, status %08x\n", status));
		return status;
	}

	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&wdfIoQueueConfig, WdfIoQueueDispatchSequential);
	wdfIoQueueConfig.EvtIoDeviceControl = nanoevt_EvtDeviceIoControl;
	status = WdfIoQueueCreate(g_wdfControlDevice, &wdfIoQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("nanoevt DriverEntry cannot create io queue, status %08x\n", status));
		return status;
	}

	WdfControlFinishInitializing(g_wdfControlDevice);

	fsevt_Initialize();

	RtlStringCbPrintfA(szPluginBuildString, sizeof(szPluginBuildString), "%s(%s %s)", SVNREV_STRING, __DATE__, __TIME__);
	nanocore_registerplugin("nanoevt", szPluginBuildString, nanoevt_reinitcfg, nanoevt_uninstall, PLUGIN_TYPE_KERNELMODE);

	if (nanoevt_reinitcfg() != 0)
	{
		DBG_OUT (("nanoevt DriverEntry cannot read config\n"));
		status = STATUS_UNSUCCESSFUL;
	}

	FilterRegistration.InstanceSetupCallback = fsevt_HandleInstanceSetup;
	status = FltRegisterFilter(DriverObject, &FilterRegistration, &FilterData.FilterHandle);
	ASSERT(NT_SUCCESS(status));

	if(NT_SUCCESS(status))
	{
		status = FltStartFiltering(FilterData.FilterHandle);

		if(!NT_SUCCESS(status))
			FltUnregisterFilter(FilterData.FilterHandle);
	}

	DBG_OUT (("nanoevt DriverEntry status = %08x\n", status));

	return STATUS_SUCCESS;
}