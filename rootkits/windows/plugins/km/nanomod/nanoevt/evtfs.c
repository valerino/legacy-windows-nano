#include <nanocore.h>
#include <wdf.h>
#include <modrkcore.h>
#include <klib.h>
#include <rkcfghandle.h>
#include <nanoevt.h>
#include "evtfs.h"

#define POOL_TAG 'tven'

#define FILE_TYPE_FILE 0
#define FILE_TYPE_DIR 1
#define FILE_TYPE_ALL 2

typedef enum _EvtFilesystemType
{
	Filesystem_Create = 0,
	Filesystem_Read,
	Filesystem_Write,
	Filesystem_Close,
	Filesystem_Rename,
	Filesystem_Delete,
	Filesystem_Mount,
	Filesystem_Invalid = -1
} EvtFilesystemType;

//parameters meaning:
//filename: for create/delete/close operation, filename represents the name of the file being created/closed/deleted
//			for rename, is the original file name
//type: file/dir for file operations, volume type for mount operations
//filenameExt: extension
typedef struct _nanoevt_fs_params
{
	WCHAR filename[MAX_PATH];
	WCHAR processname[32];
	ULONG type;
	WCHAR filenameExt[8];
} nanoevt_fs_params;

static EvtFilesystemType GetFilesystemTypeFromString(IN PUCHAR TypeString);

//global data
LIST_ENTRY g_PreEventsList; //events for which we are listening in the pre-path
LIST_ENTRY g_PostEventsList; //events for which we are listening in the post-path

NTSTATUS fsevt_Initialize ()
{
	InitializeListHead(&g_PreEventsList);
	InitializeListHead(&g_PostEventsList);
	return STATUS_SUCCESS;
}

NTSTATUS fsevt_ParseFilesystemEvents(IN UXmlNode *EventsNode)
{
	UXmlAttribute *pAttribute = NULL;
	nanoevt_event *pEvent = NULL;
	nanoevt_fs_params *pFsParams = NULL;
	UXmlNode *pFsEvent = NULL;
	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN bPrePath = FALSE;

	if(!EventsNode)
		return STATUS_UNSUCCESSFUL;

	for(pFsEvent = UXmlGetChildNode(EventsNode); pFsEvent != NULL; pFsEvent = UXmlGetNextNode(pFsEvent))
	{
		if(strcmp(pFsEvent->Name, "filesystem") != 0)
			continue;

		pAttribute = UXmlGetAttributeByName(pFsEvent, "event");
		if(!pAttribute)
		{
			DBG_OUT (("fsevt_ParseFilesystemEvents: missing required attribute \"event\"..skipping this event\n"));
			continue;
		}

		pEvent = ExAllocatePoolWithTag(PagedPool, sizeof(nanoevt_event), POOL_TAG);
		if(!pEvent)
		{
			status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}
		memset(pEvent, 0, sizeof(nanoevt_event));

		InitializeListHead(&pEvent->actions);

		pEvent->type = GetFilesystemTypeFromString(pAttribute->Value);
		if(pEvent->type == Filesystem_Invalid)
		{
			DBG_OUT (("fsevt_ParseFilesystemEvent: unrecognized filesystem event %s..skipping this event\n", pAttribute->Value));
			ExFreePoolWithTag(pEvent, POOL_TAG);
			continue;
		}
		DBG_OUT (("fsevt_ParseFilesystemEvent: filesystem event id %d, string %s\n", pEvent->type, pAttribute->Value));

		pFsParams = ExAllocatePoolWithTag(PagedPool, sizeof(nanoevt_fs_params), POOL_TAG);
		if(!pFsParams)
		{
			status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		pAttribute = UXmlGetAttributeByName(pFsEvent, "name");
		if(pAttribute)
		{
			RtlStringCbPrintfW(pFsParams->filename, sizeof(pFsParams->filename), L"%S", pAttribute->Value);
			KUpcaseCharW(pFsParams->filename, sizeof(pFsParams->filename)/sizeof(WCHAR));
			DBG_OUT (("fsevt_ParseFilesystemEvent: event filename %s\n", pAttribute->Value));
		}
		else
			pFsParams->filename[0] = L'\0';

		pAttribute = UXmlGetAttributeByName(pFsEvent, "type");
		if(!pAttribute)
		{
			DBG_OUT (("fsevt_ParseFilesystemEvent: required attribute \"type\" is missing..skipping this event\n"));
			ExFreePoolWithTag(pEvent, POOL_TAG);
			ExFreePoolWithTag(pFsParams, POOL_TAG);
			continue;
		}

		if(pEvent->type != Filesystem_Mount)
		{
			if(strcmp(pAttribute->Value, "dir") == 0)
				pFsParams->type = FILE_TYPE_DIR;
			else if(strcmp(pAttribute->Value, "file") == 0)
				pFsParams->type = FILE_TYPE_FILE;
			else if(strcmp(pAttribute->Value, "*") == 0)
				pFsParams->type = FILE_TYPE_ALL;
		}
		else
		{
			if(strcmp(pAttribute->Value, "disk") == 0)
				pFsParams->type = FILE_DEVICE_DISK_FILE_SYSTEM;
			else if(strcmp(pAttribute->Value, "network") == 0)
				pFsParams->type = FILE_DEVICE_NETWORK_FILE_SYSTEM;
			else if(strcmp(pAttribute->Value, "dvd") == 0)
				pFsParams->type = FILE_DEVICE_CD_ROM_FILE_SYSTEM;
			else if(strcmp(pAttribute->Value, "*") == 0)
				pFsParams->type = -1;
		}

		pAttribute = UXmlGetAttributeByName(pFsEvent, "ext");
		if(pAttribute)
		{
			RtlStringCbPrintfW(pFsParams->filenameExt, sizeof(pFsParams->filenameExt), L"%S", pAttribute->Value);
			KUpcaseCharW(pFsParams->filenameExt, sizeof(pFsParams->filenameExt)/sizeof(WCHAR));
			DBG_OUT (("fsevt_ParseFilesystemEvent: event extension %s\n", pAttribute->Value));
		}
		else
			pFsParams->filenameExt[0] = L'*';

		pAttribute = UXmlGetAttributeByName(pFsEvent, "process");
		if(pAttribute)
		{
			RtlStringCbPrintfW(pFsParams->processname, sizeof(pFsParams->processname), L"%S", pAttribute->Value);
			KUpcaseCharW(pFsParams->processname, sizeof(pFsParams->processname)/sizeof(WCHAR));
			DBG_OUT (("fsevt_ParseFilesystemEvent: event process %s\n", pAttribute->Value));
		}
		else
			pFsParams->processname[0] = L'*';

		pAttribute = UXmlGetAttributeByName(pFsEvent, "when");
		if(pAttribute)
		{
			if(strcmp(pAttribute->Value, "pre") == 0)
			{
				bPrePath = TRUE;
				DBG_OUT (("fsevt_ParseFilesystemEvent: event is in pre-path\n"));
			}
		}

		pEvent->params = pFsParams;
		if(bPrePath)
		{
			InsertTailList(&g_PreEventsList, &pEvent->chain);
		}
		else
		{
			InsertTailList(&g_PostEventsList, &pEvent->chain);
		}
	}
	if(!NT_SUCCESS(status))
	{
		DBG_OUT (("fsevt_ParseFilesystemEvent: error while parsing filesystem events\n"));
		if(pEvent)
			ExFreePoolWithTag(pEvent, POOL_TAG);
		if(pFsParams)
			ExFreePoolWithTag(pFsParams, POOL_TAG);
	}
	else
		DBG_OUT (("fsevt_ParseFilesystemEvent: event added.\n"));

	return status;
}

nanoevt_event *fsevt_MatchEvent(PLIST_ENTRY EventsList, PFLT_FILE_NAME_INFORMATION Filename, PUNICODE_STRING Processname, PFLT_CALLBACK_DATA Data, EvtFilesystemType fsEvent)
{
	nanoevt_event *pEvent = NULL;
	nanoevt_fs_params *pFsParams = NULL;
	PLIST_ENTRY pCurrentItem = NULL;
	BOOLEAN bIsDir = FALSE;
	WCHAR processname[MAX_PATH];
	WCHAR filename[MAX_PATH];
	WCHAR fileExt[8];
	NTSTATUS status = STATUS_SUCCESS;

	if(!EventsList || !Filename || !Data)
		return NULL;

	status = FltParseFileNameInformation(Filename);
	if(!NT_SUCCESS(status))
		return NULL;

	KUnicodeStringToCharW(processname, sizeof(processname), Processname);
	KUnicodeStringToCharW(filename, sizeof(filename), &Filename->Name);
	KUnicodeStringToCharW(fileExt, sizeof(fileExt), &Filename->Extension);
	KUpcaseCharW(processname, sizeof(processname)/sizeof(WCHAR));
	KUpcaseCharW(filename, sizeof(filename)/sizeof(WCHAR));
	KUpcaseCharW(fileExt, sizeof(fileExt)/sizeof(WCHAR));

	for(pCurrentItem = EventsList->Flink; pCurrentItem != NULL && pCurrentItem != EventsList; pCurrentItem = pCurrentItem->Flink)
	{
		pEvent = (nanoevt_event *)pCurrentItem;
		if(pEvent->type != fsEvent)
			continue;

		pFsParams = (nanoevt_fs_params *)pEvent->params;
		if(pFsParams->type == FILE_TYPE_DIR && !bIsDir)
			continue;

		if(Processname != NULL)
		{
			if(pFsParams->processname[0] != L'*')
			{
				if(wcsstr(processname, pFsParams->processname) == NULL)
					continue;
			}
		}

		if(!bIsDir)
		{
			if(pFsParams->filenameExt[0] != L'*')
			{
				if(wcsstr(fileExt, pFsParams->filenameExt) == NULL)
					continue;
			}
		}

		if(pFsParams->filename[0] != L'*')
		{
			if(wcsstr(filename, pFsParams->filename) == NULL)
				continue;
		}

		return pEvent;
	}

	return NULL;
}

FLT_PREOP_CALLBACK_STATUS fsevt_HandlePreCreate(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext)
{
	PFLT_FILE_NAME_INFORMATION pFilenameInformation = NULL;
	nanoevt_event *pEvent = NULL;
	ULONG_PTR pid;
	PUNICODE_STRING pProcessName = NULL;
	ULONG mask = 0;
	NTSTATUS status = STATUS_SUCCESS;

	*CompletionContext = NULL;

	/// skip if nanocore is not initialized
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !g_bEnabled)
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if (FLT_IS_REISSUED_IO (Data))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;
	if (FLT_IS_FS_FILTER_OPERATION (Data))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if(FltGetRequestorProcess(Data) == g_pSystemProcess)
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if(IsListEmpty(&g_PreEventsList))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED, &pFilenameInformation);
	if(!NT_SUCCESS(status))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	*CompletionContext = pFilenameInformation;

	pid = FltGetRequestorProcessId(Data);
	status = KGetProcessFullPathByPid((HANDLE)pid, &pProcessName);
	if(!NT_SUCCESS(status))
		DBG_OUT (("fsevt_HandlePreOperation: could not get process name, matching all processes\n"));

	/// skip anything else than plain open file operations
	mask = Data->Iopb->Parameters.Create.Options;
	mask = (mask >> 24) & 0xff; /// create disposition
	if (!(mask & FILE_OPEN) || !(mask & FILE_OPEN_IF))
	{
		FltReleaseFileNameInformation(pFilenameInformation);
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;
	}

	pEvent = fsevt_MatchEvent(&g_PreEventsList, pFilenameInformation, pProcessName, Data, Filesystem_Create);
#ifdef DBG
	if(pEvent != NULL)
	{
		if(KeGetCurrentIrql() == PASSIVE_LEVEL)
			DBG_OUT (("fsevt_HandlePreCreate: event matched! filename %wZ\n", &pFilenameInformation->Name));
		else
			DBG_OUT (("fsevt_HandlePreCreate: event matched!\n"));
	}
#endif

	FltReleaseFileNameInformation(pFilenameInformation);
	return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}

//post-create is guaranteed to run at PASSIVE_LEVEL, other post-callbacks may run at IRQL <= DISPATCH_LEVEL
FLT_POSTOP_CALLBACK_STATUS fsevt_HandlePostCreate(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects,
													  IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags)
{
	PFLT_FILE_NAME_INFORMATION pFilenameInformation = NULL;
	nanoevt_event *pEvent = NULL;
	ULONG_PTR pid;
	PUNICODE_STRING pProcessName = NULL;
	ULONG mask = 0;
	NTSTATUS status = STATUS_SUCCESS;

	/// skip if nanocore is not initialized
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !g_bEnabled)
		return FLT_POSTOP_FINISHED_PROCESSING;

	if (FLT_IS_REISSUED_IO (Data))
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (FLT_IS_FS_FILTER_OPERATION (Data))
		return FLT_POSTOP_FINISHED_PROCESSING;

	if(FltGetRequestorProcess(Data) == g_pSystemProcess)
		return FLT_POSTOP_FINISHED_PROCESSING;

/*
	if(CompletionContext != NULL)
	{
		PFLT_FILE_NAME_INFORMATION pTunneledFilename;
		pFilenameInformation = (PFLT_FILE_NAME_INFORMATION)CompletionContext;
		status = FltGetTunneledName(Data, pFilenameInformation, &pTunneledFilename);
		if(!NT_SUCCESS(status))
			return FLT_POSTOP_FINISHED_PROCESSING;

		pFilenameInformation = pTunneledFilename;
	}
*/
	if(IsListEmpty(&g_PostEventsList))
		return FLT_POSTOP_FINISHED_PROCESSING;

//	if(pFilenameInformation == NULL)
//	{
		status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED, &pFilenameInformation);
		if(!NT_SUCCESS(status))
			return FLT_POSTOP_FINISHED_PROCESSING;
//	}

	pid = FltGetRequestorProcessId(Data);
	status = KGetProcessFullPathByPid((HANDLE)pid, &pProcessName);
	if(!NT_SUCCESS(status))
		DBG_OUT (("fsevt_HandlePreOperation: could not get process name, matching all processes\n"));

	/// skip anything else than plain open file operations
	mask = Data->Iopb->Parameters.Create.Options;
	mask = (mask >> 24) & 0xff; /// createdisposition
	if (!(mask & FILE_OPEN) || !(mask & FILE_OPEN_IF))
	{
		FltReleaseFileNameInformation(pFilenameInformation);
		return FLT_POSTOP_FINISHED_PROCESSING;
	}

	pEvent = fsevt_MatchEvent(&g_PostEventsList, pFilenameInformation, pProcessName, Data, Filesystem_Create);
#ifdef DBG
	if(pEvent != NULL)
	{
		if(KeGetCurrentIrql() == PASSIVE_LEVEL)
			DBG_OUT (("fsevt_HandlePostCreate: event matched! filename %wZ\n", &pFilenameInformation->Name));
		else
			DBG_OUT (("fsevt_HandlePostCreate: event matched!\n"));
	}
#endif

	FltReleaseFileNameInformation(pFilenameInformation);
	return FLT_POSTOP_FINISHED_PROCESSING;
}

FLT_PREOP_CALLBACK_STATUS fsevt_HandlePreSetInformation(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext)
{
	PFLT_FILE_NAME_INFORMATION pFilenameInformation = NULL;
	nanoevt_event *pEvent = NULL;
	ULONG_PTR pid;
	PUNICODE_STRING pProcessName = NULL;
	NTSTATUS status = STATUS_SUCCESS;

	/// skip if nanocore is not initialized
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !g_bEnabled)
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if (FLT_IS_REISSUED_IO (Data))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;
	if (FLT_IS_FS_FILTER_OPERATION (Data))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if(FltGetRequestorProcess(Data) == g_pSystemProcess)
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if(IsListEmpty(&g_PreEventsList))
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;

	if(Data->Iopb->Parameters.SetFileInformation.FileInformationClass == FileDispositionInformation)
	{
		BOOLEAN bDelete = (BOOLEAN)(*(BOOLEAN *)Data->Iopb->Parameters.SetFileInformation.InfoBuffer);
		if(bDelete)
		{
		//	status = fsevt_HandleDelete()
			status = FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED, &pFilenameInformation);

			if(!NT_SUCCESS(status))
				return FLT_PREOP_SUCCESS_NO_CALLBACK;

			pEvent = fsevt_MatchEvent(&g_PreEventsList, pFilenameInformation, pProcessName, Data, Filesystem_Delete);
#ifdef DBG
			if(pEvent)
			{
				if(KeGetCurrentIrql() == PASSIVE_LEVEL)
					DBG_OUT (("fsevt_HandlePreSetInformation: event matched! filename %wZ\n", &pFilenameInformation->Name));
				else
					DBG_OUT (("fsevt_HandlePreSetInformation: event matched!\n"));
			}
#endif

		}
	}
	else if(Data->Iopb->Parameters.SetFileInformation.FileInformationClass == FileRenameInformation)
	{
		//TODO: rename needs special handling, not needed now...
	}

	if(pFilenameInformation != NULL)
		FltReleaseFileNameInformation(pFilenameInformation);

	return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}
FLT_POSTOP_CALLBACK_STATUS fsevt_HandlePostSetInformation(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects,
														  IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags)
{
	return FLT_POSTOP_FINISHED_PROCESSING;
}

NTSTATUS fsevt_HandleInstanceSetup(IN PCFLT_RELATED_OBJECTS FltObjects, IN FLT_INSTANCE_SETUP_FLAGS Flags, IN DEVICE_TYPE  VolumeDeviceType,
								   IN FLT_FILESYSTEM_TYPE  VolumeFilesystemType)
{
	PLIST_ENTRY pCurrentEntry = NULL;
	nanoevt_event *pEvent = NULL;
	nanoevt_fs_params *pFsParams = NULL;
	WCHAR volumeNameBuf[MAX_PATH];
	UNICODE_STRING VolumeName;
	ULONG sizeNeeded = 0;
	NTSTATUS status = STATUS_SUCCESS;

	if(!(Flags & FLTFL_INSTANCE_SETUP_NEWLY_MOUNTED_VOLUME))
		return STATUS_SUCCESS;

	memset(volumeNameBuf, 0, sizeof(volumeNameBuf));
	RtlInitEmptyUnicodeString(&VolumeName, volumeNameBuf, sizeof(volumeNameBuf));

	//we don't use fsevt_MatchEvent since we don't have any filename/porcess/ext to match
	for(pCurrentEntry = g_PostEventsList.Flink; pCurrentEntry != NULL && pCurrentEntry != &g_PostEventsList; pCurrentEntry = pCurrentEntry->Flink)
	{
		pEvent = (nanoevt_event *)pCurrentEntry;
		if(pEvent->type != Filesystem_Mount)
			continue;

		pFsParams = (nanoevt_fs_params *)pEvent->params;
		if(pFsParams->type != VolumeDeviceType)
			continue;

		status = FltGetVolumeName(FltObjects->Volume, &VolumeName, NULL);
		if(NT_SUCCESS(status))
		{
			DBG_OUT (("fsevt_HandlePostMount,MountWorker volume %wZ mounted\n", &VolumeName));
		}
		return STATUS_SUCCESS;
	}

	return STATUS_SUCCESS;
}

NTSTATUS
fsevt_Shutdown(
			   )
{
	return STATUS_SUCCESS;
}

static EvtFilesystemType GetFilesystemTypeFromString(IN PUCHAR TypeString)
{
	PAGED_CODE();

	if(!TypeString)
		return Filesystem_Invalid;

	if(strcmp(TypeString, "create") == 0)
		return Filesystem_Create;
	else if(strcmp(TypeString, "close") == 0)
		return Filesystem_Close;
	else if(strcmp(TypeString, "read") == 0)
		return Filesystem_Read;
	else if(strcmp(TypeString, "write") == 0)
		return Filesystem_Write;
	else if(strcmp(TypeString, "delete") == 0)
		return Filesystem_Delete;
	else if(strcmp(TypeString, "rename") == 0)
		return Filesystem_Rename;
	else if(strcmp(TypeString, "mount") == 0)
		return Filesystem_Mount;

	return Filesystem_Invalid;
}