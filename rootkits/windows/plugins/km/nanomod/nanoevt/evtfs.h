#ifndef _EVTFS_H
#define _EVTFS_H

NTSTATUS fsevt_Initialize ();

FLT_PREOP_CALLBACK_STATUS fsevt_HandlePreCreate(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext);
FLT_POSTOP_CALLBACK_STATUS fsevt_HandlePostCreate(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects,
												  IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);

FLT_PREOP_CALLBACK_STATUS fsevt_HandlePreSetInformation(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, OUT PVOID *CompletionContext);
FLT_POSTOP_CALLBACK_STATUS fsevt_HandlePostSetInformation(IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects,
														  IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags);

NTSTATUS fsevt_HandleInstanceSetup(IN PCFLT_RELATED_OBJECTS FltObjects, IN FLT_INSTANCE_SETUP_FLAGS Flags, IN DEVICE_TYPE  VolumeDeviceType,
								   IN FLT_FILESYSTEM_TYPE  VolumeFilesystemType);

NTSTATUS fsevt_ParseFilesystemEvents(IN UXmlNode *EventsNode);


NTSTATUS fsevt_Shutdown ();

#endif