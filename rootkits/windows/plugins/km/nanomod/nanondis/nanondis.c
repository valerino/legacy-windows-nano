/*
 *	ndis 5.x packet filter for nanomod
 *  -vx-
 */

#include <nanocore.h>
#include <wdf.h>
#include <ndis.h>
#include <klib.h>
#include <rkcfghandle.h>
#include <modrkcore.h>
#include <nanondis.h>
#define POOL_TAG 'sdnn'

/*
 *	globals
 *
 */

#define NPROT_MAC_ADDR_LEN				6

///  Receive packet pool bounds
#define MIN_RECV_PACKET_POOL_SIZE    4
#define MAX_RECV_PACKET_POOL_SIZE    20

///  Max receive packets we allow to be queued up
#define MAX_RECV_QUEUE_SIZE          4

///  Flags
#define NPROT_SET_FLAGS(_FlagsVar, _Mask, _BitsToSet)    \
	(_FlagsVar) = ((_FlagsVar) & ~(_Mask)) | (_BitsToSet)

#define NPROT_TEST_FLAGS(_FlagsVar, _Mask, _BitsToCheck)    \
	(((_FlagsVar) & (_Mask)) == (_BitsToCheck))


///  Definitions for Flags above.
#define NPROTO_BIND_IDLE             0x00000000
#define NPROTO_BIND_OPENING          0x00000001
#define NPROTO_BIND_FAILED           0x00000002
#define NPROTO_BIND_ACTIVE           0x00000004
#define NPROTO_BIND_CLOSING          0x00000008
#define NPROTO_BIND_FLAGS            0x0000000F  /// State of the binding

#define NPROTO_OPEN_IDLE             0x00000000
#define NPROTO_OPEN_ACTIVE           0x00000010
#define NPROTO_OPEN_FLAGS            0x000000F0  /// State of the I/O open

#define NPROTO_RESET_IN_PROGRESS     0x00000100
#define NPROTO_NOT_RESETTING         0x00000000
#define NPROTO_RESET_FLAGS           0x00000100

#define NPROTO_MEDIA_CONNECTED       0x00000000
#define NPROTO_MEDIA_DISCONNECTED    0x00000200
#define NPROTO_MEDIA_FLAGS           0x00000200

#define NPROTO_READ_SERVICING        0x00100000  /// Is the read service

#define NPROTO_READ_FLAGS            0x00100000  /// routine running?

#define NPROTO_UNBIND_RECEIVED       0x10000000  /// Seen NDIS Unbind?
#define NPROTO_UNBIND_FLAGS          0x10000000

/// promiscuous mode packet filter
#define NPROTO_PACKET_FILTER_PROMISCUOUS  (NDIS_PACKET_TYPE_DIRECTED|NDIS_PACKET_TYPE_MULTICAST|NDIS_PACKET_TYPE_BROADCAST|NDIS_PACKET_TYPE_PROMISCUOUS)

/// standard packet filter
#define NPROTO_PACKET_FILTER (NDIS_PACKET_TYPE_DIRECTED|NDIS_PACKET_TYPE_MULTICAST|NDIS_PACKET_TYPE_BROADCAST) 


///  Block the calling thread for the given duration:
#define NPROT_SLEEP(_Seconds)                            \
{                                                       \
	NDIS_EVENT  _SleepEvent;                            \
	NdisInitializeEvent(&_SleepEvent);                  \
	(VOID)NdisWaitEvent(&_SleepEvent, _Seconds*1000);   \
}

#define NDIS_STATUS_TO_NT_STATUS(_NdisStatus, _pNtStatus)                           \
{                                                                                   \
	/*                                                                              \
	 *  The following NDIS status codes map directly to NT status codes.            \
	 */                                                                             \
	if (((NDIS_STATUS_SUCCESS == (_NdisStatus)) ||                                  \
		(NDIS_STATUS_PENDING == (_NdisStatus)) ||                                   \
		(NDIS_STATUS_BUFFER_OVERFLOW == (_NdisStatus)) ||                           \
		(NDIS_STATUS_FAILURE == (_NdisStatus)) ||                                   \
		(NDIS_STATUS_RESOURCES == (_NdisStatus)) ||                                 \
		(NDIS_STATUS_NOT_SUPPORTED == (_NdisStatus))))                              \
	{                                                                               \
		*(_pNtStatus) = (NTSTATUS)(_NdisStatus);                                    \
	}                                                                               \
	else if (NDIS_STATUS_BUFFER_TOO_SHORT == (_NdisStatus))                         \
	{                                                                               \
		/*                                                                          \
		 *  The above NDIS status codes require a little special casing.            \
		 */                                                                         \
		*(_pNtStatus) = STATUS_BUFFER_TOO_SMALL;                                    \
	}                                                                               \
	else if (NDIS_STATUS_INVALID_LENGTH == (_NdisStatus))                           \
	{                                                                               \
		*(_pNtStatus) = STATUS_INVALID_BUFFER_SIZE;                                 \
	}                                                                               \
	else if (NDIS_STATUS_INVALID_DATA == (_NdisStatus))                             \
	{                                                                               \
		*(_pNtStatus) = STATUS_INVALID_PARAMETER;                                   \
	}                                                                               \
	else if (NDIS_STATUS_ADAPTER_NOT_FOUND == (_NdisStatus))                        \
	{                                                                               \
		*(_pNtStatus) = STATUS_NO_MORE_ENTRIES;                                     \
	}                                                                               \
	else if (NDIS_STATUS_ADAPTER_NOT_READY == (_NdisStatus))                        \
	{                                                                               \
		*(_pNtStatus) = STATUS_DEVICE_NOT_READY;                                    \
	}                                                                               \
	else                                                                            \
	{                                                                               \
		*(_pNtStatus) = STATUS_UNSUCCESSFUL;                                        \
	}                                                                               \
}
typedef struct _NPROT_RECV_PACKET_RSVD
{
	LIST_ENTRY              Link;
	PNDIS_BUFFER            pOriginalBuffer;  /// used if we had to partial-map

} NPROT_RECV_PACKET_RSVD, *PNPROT_RECV_PACKET_RSVD;

typedef struct _NDISPROT_INDICATE_STATUS
{
	ULONG            IndicatedStatus;        /// NDIS_STATUS
	ULONG            StatusBufferOffset;    /// from start of this struct
	ULONG            StatusBufferLength;    /// in bytes
} NDISPROT_INDICATE_STATUS, *PNDISPROT_INDICATE_STATUS;

typedef struct _NDISPROT_OPEN_CONTEXT
{
	LIST_ENTRY              Link;           /// Link into global list
	ULONG                   Flags;          /// State information
	ULONG                   RefCount;
	NDIS_SPIN_LOCK          Lock;
	NDIS_HANDLE             BindingHandle;
	NDIS_HANDLE             RecvPacketPool;
	NDIS_HANDLE             RecvBufferPool;
	ULONG                   MacOptions;
	ULONG                   MaxFrameSize;
	ULONG                   PendedRequest;
	NET_DEVICE_POWER_STATE  PowerState;
	NDIS_EVENT              PoweredUpEvent; /// signaled if PowerState is D0
	NDIS_STRING             DeviceName;     /// used in NdisOpenAdapter
	NDIS_STRING             DeviceDescr;    /// friendly name
	NDIS_STATUS             BindStatus;     /// for Open/CloseAdapter
	NDIS_EVENT              BindEvent;      /// for Open/CloseAdapter
	UCHAR                   CurrentAddress[NPROT_MAC_ADDR_LEN];
} NDISPROT_OPEN_CONTEXT, *PNDISPROT_OPEN_CONTEXT;

typedef struct _NDISPROT_GLOBALS
{
	NDIS_HANDLE NdisProtocolHandle;
	LIST_ENTRY  OpenList;         /// of OPEN_CONTEXT structures
	NDIS_SPIN_LOCK  GlobalLock;   /// to protect the above
	NDIS_EVENT BindsComplete;     /// have we seen NetEventBindsComplete?
} NDISPROT_GLOBALS, *PNDISPROT_GLOBALS;

typedef struct _NDISPROT_REQUEST
{
	NDIS_REQUEST	Request;
	NDIS_EVENT		ReqEvent;
	ULONG			Status;
} NDISPROT_REQUEST, *PNDISPROT_REQUEST;

///  Receive packet queueing.
#define NPROT_LIST_ENTRY_TO_RCV_PKT(_pEnt)   \
	CONTAINING_RECORD(CONTAINING_RECORD(_pEnt, NPROT_RECV_PACKET_RSVD, Link), NDIS_PACKET, ProtocolReserved)

#define NPROT_RCV_PKT_TO_LIST_ENTRY(_pPkt)   \
	(&((PNPROT_RECV_PACKET_RSVD)&((_pPkt)->ProtocolReserved[0]))->Link)


///  In case we allocate a receive packet of our own to copy and queue
///  received data, we might have to also allocate an auxiliary NDIS_BUFFER
///  to map part of the receive buffer (skipping the header bytes), so as
///  to satisfy NdisTransferData. In such cases, we keep a pointer to the
///  fully mapped receive buffer in the packet reserved space:
#define NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(_pPkt)  \
	(((PNPROT_RECV_PACKET_RSVD)&((_pPkt)->ProtocolReserved[0]))->pOriginalBuffer)

#include <pshpack1.h>
typedef struct _NDISPROT_ETH_HEADER
{
	UCHAR       DstAddr[NPROT_MAC_ADDR_LEN];
	UCHAR       SrcAddr[NPROT_MAC_ADDR_LEN];
	USHORT      EthType;

} NDISPROT_ETH_HEADER;
typedef struct _NDISPROT_ETH_HEADER UNALIGNED * PNDISPROT_ETH_HEADER;
#include <poppack.h>

NDISPROT_GLOBALS globals;		/// globals
nanondis_cfg nanondiscfg;		/// configuration
WDFDEVICE nanondiscontroldev;			/// control device
NPAGED_LOOKASIDE_LIST ls_entries; /// entries to log are allocated from here

/*
*	allocate a receive packet
*
*/
PNDIS_PACKET nanondis_allocatereceivepacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN UINT DataLength, OUT PUCHAR * ppDataBuffer)
{
	PNDIS_PACKET            pNdisPacket;
	PNDIS_BUFFER            pNdisBuffer;
	PUCHAR                  pDataBuffer;
	NDIS_STATUS             Status;

	pNdisPacket = NULL;
	pNdisBuffer = NULL;
	pDataBuffer = NULL;

	NdisAllocateMemoryWithTag (&pDataBuffer, DataLength, POOL_TAG);
	if (pDataBuffer == NULL)
		goto __exit;

	///  Make this an NDIS buffer.
	NdisAllocateBuffer(&Status, &pNdisBuffer, pOpenContext->RecvBufferPool, pDataBuffer, DataLength);
	if (Status != NDIS_STATUS_SUCCESS)
		goto __exit;

	NdisAllocatePacket(&Status, &pNdisPacket, pOpenContext->RecvPacketPool);
	if (Status != NDIS_STATUS_SUCCESS)
		goto __exit;

	NDIS_SET_PACKET_STATUS(pNdisPacket, 0);
	NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pNdisPacket) = NULL;

	NdisChainBufferAtFront(pNdisPacket, pNdisBuffer);

	*ppDataBuffer = pDataBuffer;

__exit:
	if (pNdisPacket == NULL)
	{
		///  cleanup
		if (pNdisBuffer != NULL)
			NdisFreeBuffer(pNdisBuffer);

		if (pDataBuffer != NULL)
			NdisFreeMemory (pDataBuffer,0,0);
	}
	return pNdisPacket;
}

/*
 *	free resources associated with a received packet
 *
 */
VOID nanondis_freereceivepacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN PNDIS_PACKET pNdisPacket)
{
	PNDIS_BUFFER        pNdisBuffer;
	UINT                TotalLength;
	UINT                BufferLength;
	PUCHAR              pCopyData;

	if (NdisGetPoolFromPacket(pNdisPacket) == pOpenContext->RecvPacketPool)
	{
		///  this is a local copy.
#ifdef NDIS51
		NdisGetFirstBufferFromPacketSafe(pNdisPacket, &pNdisBuffer, (PVOID *)&pCopyData, &BufferLength, &TotalLength, NormalPagePriority);
#else
		NdisGetFirstBufferFromPacket(pNdisPacket, &pNdisBuffer, (PVOID *)&pCopyData, &BufferLength, &TotalLength);
#endif

		NdisFreePacket(pNdisPacket);
		NdisFreeBuffer(pNdisBuffer);
		NdisFreeMemory(pCopyData,0,0);
	}
	else
	{
		/// return packet to ndis
		NdisReturnPackets(&pNdisPacket, 1);
	}
}

/*
 *	called by ndis on request completion
 *
 */
VOID nanondis_requestcomplete(IN NDIS_HANDLE ProtocolBindingContext, IN PNDIS_REQUEST pNdisRequest,IN NDIS_STATUS Status)
{
	PNDISPROT_OPEN_CONTEXT       pOpenContext;
	PNDISPROT_REQUEST            pReqContext;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

	///  Get at the request context.
	pReqContext = CONTAINING_RECORD(pNdisRequest, NDISPROT_REQUEST, Request);

	///  Save away the completion status.
	pReqContext->Status = Status;

	///  Wake up the thread blocked for this request to complete.
	NdisSetEvent(&pReqContext->ReqEvent);
}


/*
 *	called by ndis when reset completes
 *
 */
VOID nanondis_resetcomplete(IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_STATUS Status)
{
	return;
}

/*
 *	called by ndis when miniport changes status
 *
 */
VOID nanondis_status(IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_STATUS GeneralStatus, IN PVOID StatusBuffer, IN UINT StatusBufferSize)
{
	PNDISPROT_OPEN_CONTEXT       pOpenContext;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
	DBG_OUT (("nanondis_status : openctx %x, status %x\n", pOpenContext,GeneralStatus));
	
	NdisAcquireSpinLock (&pOpenContext->Lock);

	if (pOpenContext->PowerState != NetDeviceStateD0)
	{
		///  the device is in a low power state.
		DBG_OUT (("nanondis_status : openctx %x, powerstate %d, status %x ignored\n", pOpenContext,pOpenContext->PowerState,GeneralStatus));	
	
		switch(GeneralStatus)
		{
			case NDIS_STATUS_RESET_START:
				NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_RESET_FLAGS, NPROTO_RESET_IN_PROGRESS);
			break;

			case NDIS_STATUS_RESET_END:
				NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_RESET_FLAGS, NPROTO_NOT_RESETTING);
			break;

			case NDIS_STATUS_MEDIA_CONNECT:
				NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_MEDIA_FLAGS, NPROTO_MEDIA_CONNECTED);
			break;

			case NDIS_STATUS_MEDIA_DISCONNECT:
				NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_MEDIA_FLAGS, NPROTO_MEDIA_DISCONNECTED);
			break;

			default:
				break;
		}
	}	
	NdisReleaseSpinLock (&pOpenContext->Lock);
}

/*
 *	called by ndis when status completes
 *
 */
VOID nanondis_statuscomplete(IN NDIS_HANDLE ProtocolBindingContext)
{
	/// ignored
	return;
}

/*
*	parse tagged-value linked list and build inmemory configuration
*
*/
int nanondis_parsecfg (UXmlNode *ModuleConfig, nanondis_cfg* cfg)
{
	UXmlNode *optNode;
	PUCHAR pName;
	PUCHAR pValue;
	NTSTATUS status = STATUS_SUCCESS;

	/// check params
	if(!ModuleConfig || !cfg)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		if(pName)
		{
			if(strcmp(pName, "log_ndis") == 0)
			{
				if(pValue)
				{
					cfg->enabled = KAtol(pValue);
					DBG_OUT (("nanondis_parsecfg LOG_NDIS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "ndis_promiscuous") == 0)
			{
				if(pValue)
				{
					cfg->promiscuous = KAtol(pValue);
					DBG_OUT (("nanondis_parsecfg NDIS_PROMISCUOUS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "log_nonip_packets") == 0)
			{
				if(pValue)
				{
					cfg->log_noip = KAtol(pValue);
					DBG_OUT (("nanondis_parsecfg LOG_NONIP_PACKETS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "log_noneth_packets") == 0)
			{
				if(pValue)
				{
					cfg->log_noeth = KAtol(pValue);
					DBG_OUT (("nanondis_parsecfg LOG_NONETH_PACKETS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "ndis_rule") == 0)
			{
				UXmlNode *pParamNode;
				ndis_rule *ndisrule;

				ndisrule = ExAllocatePoolWithTag(NonPagedPool, sizeof(ndis_rule), POOL_TAG);
				if(ndisrule)
				{
					memset(ndisrule, 0, sizeof(ndis_rule));

					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != STATUS_SUCCESS)
							break;

						if(pName && pValue)
						{
							if(strcmp(pName, "protocol") == 0)
							{
								if(*pValue == '*')
									ndisrule->protocol = IPPROTO_IP;
								else if(_stricmp(pValue, "tcp") == 0)
									ndisrule->protocol = IPPROTO_TCP;
								else if(_stricmp(pValue, "udp") == 0)
									ndisrule->protocol = IPPROTO_UDP;
								else
									ndisrule->protocol = KAtol(pValue);

								DBG_OUT (("nanondis_parsecfg NDIS_RULE_protocol = %s\n", pValue));
							}
							else if(strcmp(pName, "srcip") == 0)
							{
								if(*pValue == '*')
									ndisrule->srcip = 0;
								else
									ndisrule->srcip = htonl(Kinet_addr(pValue));
								DBG_OUT (("nanondis_parsecfg NDIS_RULE_srcip = %s\n", pValue));

							}
							else if(strcmp(pName, "srcport") == 0)
							{

								if(*pValue == '*')
									ndisrule->srcport = 0;
								else
									ndisrule->srcport = (USHORT)KAtol(pValue);

								DBG_OUT (("nanondis_parsecfg NDIS_RULE_srcport = %s\n", pValue));
							}
							else if(strcmp(pName, "dstip") == 0)
							{
								if(*pValue == '*')
									ndisrule->dstip = 0;
								else
									ndisrule->dstip = htonl(Kinet_addr(pValue));
								DBG_OUT (("nanondis_parsecfg NDIS_RULE_dstip = %s\n", pValue));
							}
							else if(strcmp(pName, "dstport") == 0)
							{
								if(*pValue == '*')
									ndisrule->dstport = 0;
								else
									ndisrule->dstport = (USHORT)KAtol(pValue);
								DBG_OUT (("nanondis_parsecfg NDIS_RULE_dstport = %s\n", pValue));
							}
							else if(strcmp(pName, "direction") == 0)
							{
								if(*pValue == '*')
									ndisrule->direction = LOG_DIRECTION_ALL;
								else if(_stricmp(pValue, "in") == 0)
									ndisrule->direction = LOG_DIRECTION_INBOUND;
								else if(_stricmp(pValue, "out") == 0)
									ndisrule->direction = LOG_DIRECTION_OUTBOUND;
								DBG_OUT (("nanondis_parsecfg NDIS_RULE_direction = %s\n", pValue));
							}
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList (&cfg->ndisrules,&ndisrule->chain);
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}

/*
*	uninstall
*
*/
int nanondis_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	/// uninstall nanondis
	name.Buffer = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanondiscontroldev));
	name.Length = (USHORT)str_lenbytesw(name.Buffer);
	name.MaximumLength = name.Length;
	Status = KDeleteKeyTree(&name,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = WdfDriverGetRegistryPath(WdfDeviceGetDriver(nanondiscontroldev));
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

	/// remove binding
	Status = KRemoveProtocolBinding (p);

__exit:
	DBG_OUT (("nanondis_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	initialize/reinitialize configuration (called by nanocore too on new configuration message)
*
*/
int nanondis_reinitcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// read configuration
	/// warning : repeatdly calling this causes leak (by design). cfg should be updated on reboot.
	if (!IsListEmpty(&nanondiscfg.ndisrules))
		InitializeListHead(&nanondiscfg.ndisrules);

	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanondis", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanondis_parsecfg(ModuleConfig,&nanondiscfg) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

__exit:
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanondis_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
 *	check if we're already bound
 *
 */
PNDISPROT_OPEN_CONTEXT nanondis_lookupdevice(IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength)
{
	PNDISPROT_OPEN_CONTEXT pOpenContext = NULL;
	PLIST_ENTRY pEnt = NULL;

	NdisAcquireSpinLock(&globals.GlobalLock);
	for (pEnt = globals.OpenList.Flink; pEnt != &globals.OpenList; pEnt = pEnt->Flink)
	{
		pOpenContext = CONTAINING_RECORD(pEnt, NDISPROT_OPEN_CONTEXT, Link);

		///  check if this has the name we are looking for
		if ((pOpenContext->DeviceName.Length == BindingInfoLength) &&
			NdisEqualMemory(pOpenContext->DeviceName.Buffer, pBindingInfo, BindingInfoLength))
		{
			/// ref added
			NdisInterlockedIncrement((PLONG)&pOpenContext->RefCount);
			break;
		}
		pOpenContext = NULL;
	}
	NdisReleaseSpinLock(&globals.GlobalLock);

	return (pOpenContext);
}

/*
 *	send ndis request to miniport
 *
 */
NDIS_STATUS nanondis_dorequest(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN NDIS_REQUEST_TYPE RequestType,
	IN NDIS_OID Oid, IN PVOID InformationBuffer, IN ULONG InformationBufferLength, OUT PULONG pBytesProcessed)
{
	NDISPROT_REQUEST ReqContext;
	PNDIS_REQUEST pNdisRequest = &ReqContext.Request;
	NDIS_STATUS Status = STATUS_UNSUCCESSFUL;

	/// initialize event
	NdisInitializeEvent(&ReqContext.ReqEvent);
	
	/// build request
	pNdisRequest->RequestType = RequestType;
	switch (RequestType)
	{
		case NdisRequestQueryInformation:
			pNdisRequest->DATA.QUERY_INFORMATION.Oid = Oid;
			pNdisRequest->DATA.QUERY_INFORMATION.InformationBuffer = InformationBuffer;
			pNdisRequest->DATA.QUERY_INFORMATION.InformationBufferLength = InformationBufferLength;
		break;

		case NdisRequestSetInformation:
			pNdisRequest->DATA.SET_INFORMATION.Oid = Oid;
			pNdisRequest->DATA.SET_INFORMATION.InformationBuffer = InformationBuffer;
			pNdisRequest->DATA.SET_INFORMATION.InformationBufferLength = InformationBufferLength;
		break;

		default:
			break;
	}

	/// send request
	NdisRequest(&Status, pOpenContext->BindingHandle, pNdisRequest);
	if (Status == NDIS_STATUS_PENDING)
	{
		NdisWaitEvent(&ReqContext.ReqEvent, 0);
		Status = ReqContext.Status;
	}
	if (Status == NDIS_STATUS_SUCCESS)
	{
		*pBytesProcessed = (RequestType == NdisRequestQueryInformation)? pNdisRequest->DATA.QUERY_INFORMATION.BytesWritten: pNdisRequest->DATA.SET_INFORMATION.BytesRead;

		/// The driver below should set the correct value to BytesWritten
		/// or BytesRead. But now, we just truncate the value to InformationBufferLength
		if (*pBytesProcessed > InformationBufferLength)
			*pBytesProcessed = InformationBufferLength;
	}

	return Status;
}

/*
 *	free resources allocated while binding
 *
 */
VOID nanondis_freebindresources(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
	if (pOpenContext->RecvPacketPool != NULL)
	{
		NdisFreePacketPool(pOpenContext->RecvPacketPool);
		pOpenContext->RecvPacketPool = NULL;
	}

	if (pOpenContext->RecvBufferPool != NULL)
	{
		NdisFreeBufferPool(pOpenContext->RecvBufferPool);
		pOpenContext->RecvBufferPool = NULL;
	}

	if (pOpenContext->DeviceName.Buffer != NULL)
	{
		NdisFreeMemory(pOpenContext->DeviceName.Buffer,0,0);
		pOpenContext->DeviceName.Buffer = NULL;
		pOpenContext->DeviceName.Length =
			pOpenContext->DeviceName.MaximumLength = 0;
	}

	if (pOpenContext->DeviceDescr.Buffer != NULL)
	{
		/// this would have been allocated by NdisQueryAdpaterInstanceName.
		NdisFreeMemory(pOpenContext->DeviceDescr.Buffer, 0, 0);
		pOpenContext->DeviceDescr.Buffer = NULL;
	}
}

/*
 *	validate open
 *
 */
NDIS_STATUS nanondis_validateopenanddorequest(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
	IN NDIS_REQUEST_TYPE RequestType, IN NDIS_OID Oid, IN PVOID InformationBuffer,
	IN ULONG InformationBufferLength, OUT PULONG pBytesProcessed, IN BOOLEAN bWaitForPowerOn)
{
	NDIS_STATUS             Status;

	if (pOpenContext == NULL)
	{
		Status = NDIS_STATUS_INVALID_DATA;
		return Status;
	}
	
	NdisAcquireSpinLock(&pOpenContext->Lock);

	///  proceed only if we have a binding.
	if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_ACTIVE))
	{
		NdisReleaseSpinLock(&pOpenContext->Lock);
		Status = NDIS_STATUS_INVALID_DATA;
		return Status;
	}

	///  make sure that the binding does not go away until we are finished with the request.
	NdisInterlockedIncrement((PLONG)&pOpenContext->PendedRequest);
	NdisReleaseSpinLock(&pOpenContext->Lock);

	if (bWaitForPowerOn)
	{
		///  Wait for the device below to be powered up.
		///  We don't wait indefinitely here - this is to avoid
		///  a PROCESS_HAS_LOCKED_PAGES bugcheck that could happen
		///  if the calling process terminates, and this Request doesn't
		///  complete within a reasonable time. An alternative would
		///  be to explicitly handle cancellation of this Request.
			NdisWaitEvent(&pOpenContext->PoweredUpEvent, 4500);
	}

	Status = nanondis_dorequest(pOpenContext, RequestType, Oid, InformationBuffer, InformationBufferLength, pBytesProcessed);

	///  Let go of the binding.
	NdisInterlockedDecrement((PLONG)&pOpenContext->PendedRequest);

	DBG_OUT (("nanondis_validateopenanddorequest status = %x\n",Status));

	return Status;
}

/*
 *	wait for any pended io request
 *
 */
VOID nanondis_waitpendingio(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
	ULONG           LoopCount;
#ifdef NDIS51
	NDIS_STATUS     Status;
	ULONG           PendingCount;

	///  Wait for any pending sends or requests on the binding to complete.
	for (LoopCount = 0; LoopCount < 60; LoopCount++)
	{
		Status = NdisQueryPendingIOCount(pOpenContext->BindingHandle, &PendingCount);

		if ((Status != NDIS_STATUS_SUCCESS) || (PendingCount == 0))
			break;

		NPROT_SLEEP(2);
	}
#endif // NDIS51

	///  Make sure any threads trying to send have finished.
	for (LoopCount = 0; LoopCount < 60; LoopCount++)
	{
		if (pOpenContext->PendedRequest == 0)
			break;

		NPROT_SLEEP(1);
	}
}

/*
 *	shutdown ndis binding
 *
 */
VOID nanondis_shutdownbinding(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
	NDIS_STATUS Status = STATUS_UNSUCCESSFUL;
	BOOLEAN  DoCloseBinding = FALSE;
	ULONG    PacketFilter = 0;
	ULONG    BytesRead = 0;

	DBG_OUT (("nanondis_shutdownbinding\n"));	

	NdisAcquireSpinLock(&pOpenContext->Lock);
	if (NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_OPENING))
	{
		///  We are still in the process of setting up this binding.
		NdisReleaseSpinLock(&pOpenContext->Lock);
		return;
	}

	if (NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_ACTIVE))
	{
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_CLOSING);
		DoCloseBinding = TRUE;
	}
	NdisReleaseSpinLock(&pOpenContext->Lock);

	if (DoCloseBinding)
	{
		/// set packet filter to 0 before closing the binding
		Status = nanondis_dorequest(pOpenContext, NdisRequestSetInformation, OID_GEN_CURRENT_PACKET_FILTER,
				&PacketFilter, sizeof(PacketFilter), &BytesRead);
		if (Status != NDIS_STATUS_SUCCESS)
			DBG_OUT (("nanondis_shutdownbinding setpacketfilter failed with error %x\n",Status));

		/// set multicast list to null before closing the binding
		Status = nanondis_dorequest(pOpenContext, NdisRequestSetInformation, OID_802_3_MULTICAST_LIST,
				NULL, 0, &BytesRead);
		if (Status != NDIS_STATUS_SUCCESS)
			DBG_OUT (("nanondis_shutdownbinding setmulticastlist failed with error %x\n",Status));

		nanondis_waitpendingio(pOpenContext);

		///  close the binding now
		NdisInitializeEvent(&pOpenContext->BindEvent);

		DBG_OUT (("nanondis_shutdownbinding closing opencontext %x, bindinghandle %x\n",pOpenContext, pOpenContext->BindingHandle));
		NdisCloseAdapter(&Status, pOpenContext->BindingHandle);

		if (Status == NDIS_STATUS_PENDING)
		{
			NdisWaitEvent(&pOpenContext->BindEvent, 0);
			Status = pOpenContext->BindStatus;
		}
		pOpenContext->BindingHandle = NULL;

		NdisAcquireSpinLock(&pOpenContext->Lock);
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_IDLE);
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_UNBIND_FLAGS, 0);
		NdisReleaseSpinLock(&pOpenContext->Lock);
	}

	///  Remove it from the global list.
	NdisAcquireSpinLock(&globals.GlobalLock);
	RemoveEntryList(&pOpenContext->Link);
	NdisReleaseSpinLock(&globals.GlobalLock);

	///  Free any other resources allocated for this bind.
	nanondis_freebindresources(pOpenContext);

	//  free context
	if (NdisInterlockedDecrement((PLONG)&pOpenContext->RefCount) == 0)
		NdisFreeMemory(pOpenContext,0,0);
	
	return;
}

/*
 *	create ndis binding for the specified adapter
 *
 */
NDIS_STATUS nanondis_createbinding (IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength)
{
	NDIS_STATUS             Status;
	NDIS_STATUS             OpenErrorCode;
	NDIS_MEDIUM             MediumArray[1] = {NdisMedium802_3};
	UINT                    SelectedMediumIndex;
	PNDISPROT_OPEN_CONTEXT   pTmpOpenContext;
	BOOLEAN                 fDoNotDisturb = FALSE;
	BOOLEAN                 fOpenComplete = FALSE;
	ULONG                   BytesProcessed;
	ULONG                   GenericUlong = 0;
	ULONG                   safeMultResult;

	Status = NDIS_STATUS_SUCCESS;

	/// check if we already have a binding to this device.
	pTmpOpenContext = nanondis_lookupdevice(pBindingInfo, BindingInfoLength);
	if (pTmpOpenContext != NULL)
	{
		DBG_OUT (("nanondis_createbinding opencontext %x already bound on device %S\n",pTmpOpenContext,pTmpOpenContext->DeviceName.Buffer));

		/// dereference and free in case
		if (NdisInterlockedDecrement((PLONG)&pTmpOpenContext->RefCount) == 0)
			NdisFreeMemory(pTmpOpenContext,0,0);

		Status = NDIS_STATUS_FAILURE;
		goto __exit;
	}
	NdisAcquireSpinLock(&pOpenContext->Lock);

	///  check if this open context is already bound/binding/closing.
	if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_IDLE) ||
		NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_UNBIND_FLAGS, NPROTO_UNBIND_RECEIVED))
	{
		NdisReleaseSpinLock(&pOpenContext->Lock);
		Status = NDIS_STATUS_NOT_ACCEPTED;

		/// Make sure we don't abort this binding on failure cleanup.
		fDoNotDisturb = TRUE;
		goto __exit;
	}
	
	NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_OPENING);
	NdisReleaseSpinLock(&pOpenContext->Lock);

	///  copy in the device name. add room for a NULL terminator
	NdisAllocateMemoryWithTag(&pOpenContext->DeviceName.Buffer, BindingInfoLength + sizeof(WCHAR),POOL_TAG);
	if (pOpenContext->DeviceName.Buffer == NULL)
	{
		DBG_OUT (("nanondis_createbinding out of memory allocating name buffer\n"));
		Status = NDIS_STATUS_RESOURCES;
		goto __exit;
	}
	NdisMoveMemory(pOpenContext->DeviceName.Buffer, pBindingInfo, BindingInfoLength);
	*(PWCHAR)((PUCHAR)pOpenContext->DeviceName.Buffer + BindingInfoLength) = L'\0';
	NdisInitUnicodeString(&pOpenContext->DeviceName, pOpenContext->DeviceName.Buffer);

	///  allocate packet pool for receives
	NdisAllocatePacketPoolEx(&Status, &pOpenContext->RecvPacketPool, MIN_RECV_PACKET_POOL_SIZE,
			MAX_RECV_PACKET_POOL_SIZE - MIN_RECV_PACKET_POOL_SIZE, sizeof(NPROT_RECV_PACKET_RSVD));
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding failed to allocate receive packets pool\n"));
		goto __exit;	
	}

	///  buffer pool for receives
	NdisAllocateBufferPool(&Status, &pOpenContext->RecvBufferPool, MAX_RECV_PACKET_POOL_SIZE);
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding failed to allocate receive packets buffer\n"));
		goto __exit;	
	}

	///  assume that the device is powered up.
	pOpenContext->PowerState = NetDeviceStateD0;

	///  open the adapter
	NdisInitializeEvent(&pOpenContext->BindEvent);
	NdisOpenAdapter(&Status,&OpenErrorCode, &pOpenContext->BindingHandle, &SelectedMediumIndex, &MediumArray[0],
		sizeof(MediumArray) / sizeof(NDIS_MEDIUM), globals.NdisProtocolHandle, (NDIS_HANDLE)pOpenContext, &pOpenContext->DeviceName,
		0, NULL);
	if (Status == NDIS_STATUS_PENDING)
	{
		NdisWaitEvent(&pOpenContext->BindEvent, 0);
		Status = pOpenContext->BindStatus;
	}

	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding : ndisopenadapter failed (device=%S, status=%x)\n",pOpenContext->DeviceName.Buffer,Status));
		goto __exit;	
	}

	///  Note down the fact that we have successfully bound.
	///  We don't update the state on the open just yet - this
	///  is to prevent other threads from shutting down the binding.
	fOpenComplete = TRUE;

	///  get the friendly name for the adapter
	NdisQueryAdapterInstanceName(&pOpenContext->DeviceDescr, pOpenContext->BindingHandle);
	
	/// get current address
	Status = nanondis_dorequest(pOpenContext, NdisRequestQueryInformation, OID_802_3_CURRENT_ADDRESS,
			&pOpenContext->CurrentAddress[0], NPROT_MAC_ADDR_LEN, &BytesProcessed);
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding : query current address failed with error %x\n",Status));
		goto __exit;
	}

	///  get mac options
	Status = nanondis_dorequest(pOpenContext, NdisRequestQueryInformation, OID_GEN_MAC_OPTIONS,
		&pOpenContext->MacOptions, sizeof(pOpenContext->MacOptions), &BytesProcessed);

	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding : query MAC options failed with error %x\n",Status));
		goto __exit;
	}

	///  Get the max frame size
	Status = nanondis_dorequest(pOpenContext, NdisRequestQueryInformation, OID_GEN_MAXIMUM_FRAME_SIZE,
		&pOpenContext->MaxFrameSize, sizeof(pOpenContext->MaxFrameSize), &BytesProcessed);
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding : query maxframesize failed with error %x\n",Status));
		goto __exit;
	}

	///  Get the media connect status
	Status = nanondis_dorequest(pOpenContext, NdisRequestQueryInformation, OID_GEN_MEDIA_CONNECT_STATUS,
			&GenericUlong, sizeof(GenericUlong), &BytesProcessed);
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis_createbinding : query mediaconnectstatus failed with error %x\n",Status));
		goto __exit;
	}
	if (GenericUlong == NdisMediaStateConnected)
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_MEDIA_FLAGS, NPROTO_MEDIA_CONNECTED);
	else
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_MEDIA_FLAGS, NPROTO_MEDIA_DISCONNECTED);

	///  Mark this open. Also check if we received an Unbind while
	///  we were setting this up.
	NdisAcquireSpinLock(&pOpenContext->Lock);
	NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_ACTIVE);

	///  Did an unbind happen in the meantime?
	if (NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_UNBIND_FLAGS, NPROTO_UNBIND_RECEIVED))
			Status = NDIS_STATUS_FAILURE;
	NdisReleaseSpinLock(&pOpenContext->Lock);

__exit:
	if ((Status != NDIS_STATUS_SUCCESS) && !fDoNotDisturb)
	{
		NdisAcquireSpinLock(&pOpenContext->Lock);

		///  Check if we had actually finished opening the adapter.
		if (fOpenComplete)
			NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_ACTIVE);
		else if (NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_OPENING))
			NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_BIND_FLAGS, NPROTO_BIND_FAILED);

		NdisReleaseSpinLock(&pOpenContext->Lock);

		nanondis_shutdownbinding(pOpenContext);
	}

	DBG_OUT (("nanondis_createbinding : opencontext %x, status %x\n",pOpenContext,Status));

	return Status;
}

/*
*	set the packet filter
*
*/
NTSTATUS nanondis_setpacketfilter(PNDISPROT_OPEN_CONTEXT pOpenContext)
{
	NTSTATUS                NtStatus;
	ULONG                   PacketFilter;
	NDIS_STATUS             NdisStatus;
	ULONG                   BytesProcessed;
	PNDISPROT_OPEN_CONTEXT   pCurrentOpenContext = NULL;

	NdisAcquireSpinLock(&pOpenContext->Lock);
	if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NPROTO_OPEN_FLAGS, NPROTO_OPEN_IDLE))
	{
		NdisReleaseSpinLock(&pOpenContext->Lock);
		if (NdisInterlockedDecrement((PLONG)&pOpenContext->RefCount) == 0)
			NdisFreeMemory(pOpenContext,0,0);
		NtStatus = STATUS_DEVICE_BUSY;
		return NtStatus;
	}

	NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_OPEN_FLAGS, NPROTO_OPEN_ACTIVE);
	NdisReleaseSpinLock(&pOpenContext->Lock);

	///  Set the packet filter now
	if (nanondiscfg.promiscuous)
		PacketFilter = NPROTO_PACKET_FILTER_PROMISCUOUS;
	else
		PacketFilter = NPROTO_PACKET_FILTER;

	// do wait for power on on request
	NdisStatus = nanondis_validateopenanddorequest(pOpenContext, NdisRequestSetInformation, OID_GEN_CURRENT_PACKET_FILTER,
		&PacketFilter, sizeof(PacketFilter), &BytesProcessed, TRUE);

	if (NdisStatus != NDIS_STATUS_SUCCESS)
	{
		///  Undo all that we did above.
		NdisAcquireSpinLock(&pOpenContext->Lock);
		NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_OPEN_FLAGS, NPROTO_OPEN_IDLE);
		NdisReleaseSpinLock(&pOpenContext->Lock);
		if (NdisInterlockedDecrement((PLONG)&pOpenContext->RefCount) == 0)
			NdisFreeMemory(pOpenContext,0,0);
		NDIS_STATUS_TO_NT_STATUS(NdisStatus, &NtStatus);
		DBG_OUT (("nanondis_setpacketfilter error %x\n",NtStatus));	
		return NtStatus;
	}

	NtStatus = STATUS_SUCCESS;
	DBG_OUT (("nanondis_setpacketfilter ok\n"));		
	return (NtStatus);
}

/*
*	bind adapter
*
*/
VOID nanondis_bindadapter(OUT PNDIS_STATUS pStatus, IN NDIS_HANDLE BindContext, IN PNDIS_STRING pDeviceName, IN PVOID SystemSpecific1,IN PVOID SystemSpecific2)
{
	PNDISPROT_OPEN_CONTEXT pOpenContext;
	NDIS_STATUS Status;

	///  allocate our context for this open
	NdisAllocateMemoryWithTag(&pOpenContext, sizeof(NDISPROT_OPEN_CONTEXT), POOL_TAG);
	if (pOpenContext == NULL)
	{
		Status = NDIS_STATUS_RESOURCES;
		goto __exit;
	}
	NdisZeroMemory(pOpenContext, sizeof(NDISPROT_OPEN_CONTEXT));
	NdisAllocateSpinLock(&pOpenContext->Lock);
	NdisInitializeEvent(&pOpenContext->PoweredUpEvent);

	///  start off by assuming that the device below is powered up
	NdisSetEvent(&pOpenContext->PoweredUpEvent);
	NdisInterlockedIncrement((PLONG)&pOpenContext->RefCount);

	///  add it to the global list
	NdisAcquireSpinLock(&globals.GlobalLock);
	InsertTailList(&globals.OpenList, &pOpenContext->Link);
	NdisReleaseSpinLock(&globals.GlobalLock);

	/// set up the NDIS binding
	Status = nanondis_createbinding(pOpenContext, (PUCHAR)pDeviceName->Buffer, pDeviceName->Length);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set packet filter
	Status = nanondis_setpacketfilter(pOpenContext);

__exit:
	*pStatus = Status;
	DBG_OUT (("nanondis_bindadapter status=%x\n",Status));
	return;
}

/*
 *	called by ndis on open adapter completion
 *
 */
VOID nanondis_openadaptercomplete(IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_STATUS Status, IN NDIS_STATUS OpenErrorCode)
{
	PNDISPROT_OPEN_CONTEXT           pOpenContext;
	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
	pOpenContext->BindStatus = Status;
	NdisSetEvent(&pOpenContext->BindEvent);
}

/*
 *	called when the adapter is being unbound
 *
 */
VOID nanondis_unbindadapter(OUT PNDIS_STATUS pStatus, IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_HANDLE UnbindContext)
{
	PNDISPROT_OPEN_CONTEXT           pOpenContext;

	DBG_OUT (("nanondis_unbindadapter\n"));	

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

	///  Mark this open as having seen an Unbind.
	NdisAcquireSpinLock(&pOpenContext->Lock);
	NPROT_SET_FLAGS(pOpenContext->Flags, NPROTO_UNBIND_FLAGS, NPROTO_UNBIND_RECEIVED);

	///  In case we had threads blocked for the device below to be powered up, wake them up.
	NdisSetEvent(&pOpenContext->PoweredUpEvent);

	NdisReleaseSpinLock(&pOpenContext->Lock);

	/// shutdown binding
	nanondis_shutdownbinding(pOpenContext);
	
	*pStatus = NDIS_STATUS_SUCCESS;
	return;
}


/*
 *	called by ndis on close adapter completion
 *
 */
VOID nanondis_closeadaptercomplete(IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_STATUS Status)
{
	PNDISPROT_OPEN_CONTEXT           pOpenContext;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
	pOpenContext->BindStatus = Status;
	NdisSetEvent (&pOpenContext->BindEvent);
}

/*
 *	called by ndis on a pnp event
 *
 */
NDIS_STATUS nanondis_pnpeventhandler( IN NDIS_HANDLE ProtocolBindingContext, IN PNET_PNP_EVENT pNetPnPEvent)
{
	PNDISPROT_OPEN_CONTEXT           pOpenContext;
	NDIS_STATUS                     Status;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

	switch (pNetPnPEvent->NetEvent)
	{
		case NetEventSetPower:
			pOpenContext->PowerState = *(PNET_DEVICE_POWER_STATE)pNetPnPEvent->Buffer;
			if (pOpenContext->PowerState > NetDeviceStateD0)
			{
				///  The device below is transitioning to a low power state.
				///  Block any threads attempting to query the device while
				///  in this state.
				NdisInitializeEvent (&pOpenContext->PoweredUpEvent);

				///  Wait for any I/O in progress to complete.
				nanondis_waitpendingio(pOpenContext);
			}
			else
			{
				///  the device below is powered up.
				NdisSetEvent(&pOpenContext->PoweredUpEvent);
			}
			DBG_OUT (("nanondis_pnpeventhandler : opencontext %x, set power to %d\n",pOpenContext,pOpenContext->PowerState));
			Status = NDIS_STATUS_SUCCESS;
		break;

		case NetEventQueryPower:
			Status = NDIS_STATUS_SUCCESS;
		break;

		case NetEventBindsComplete:
			NdisSetEvent(&globals.BindsComplete);
			Status = NDIS_STATUS_SUCCESS;
		break;

		case NetEventQueryRemoveDevice:
		case NetEventCancelRemoveDevice:
		case NetEventReconfigure:
		case NetEventBindList:
		case NetEventPnPCapabilities:
			Status = NDIS_STATUS_SUCCESS;
		break;

		default:
			Status = NDIS_STATUS_NOT_SUPPORTED;
		break;
	}

	return Status;
}

/*
 *	called by ndis on send completion
 *
 */
VOID nanondis_sendcomplete(IN NDIS_HANDLE ProtocolBindingContext, IN PNDIS_PACKET pNdisPacket, IN NDIS_STATUS Status)
{
	return;
}

/*
*	called by ndis on receive completion
*
*/
VOID nanondis_receivecomplete(IN NDIS_HANDLE ProtocolBindingContext)
{
	return;
}

/*
 *	read packet data from an ndis packet, walking ndis buffers
 *
 */
void nanondis_readpacket(IN PNDIS_PACKET Packet, OUT PUCHAR lpBuffer, IN ULONG nNumberOfBytesToRead, IN ULONG nOffset, OUT PULONG lpNumberOfBytesRead, OUT PULONG pTotalSize)
{
	PNDIS_BUFFER    CurrentBuffer;
	UINT            nBufferCount, TotalPacketLength;
	PUCHAR          VirtualAddress = NULL;
	UINT            CurrentLength, CurrentOffset;
	UINT            AmountToMove;

	*lpNumberOfBytesRead = 0;
	*pTotalSize = 0;

	if (!nNumberOfBytesToRead)
		return;

	/// query packet
	NdisQueryPacket((PNDIS_PACKET )Packet, (PUINT )NULL, (PUINT )&nBufferCount, &CurrentBuffer, &TotalPacketLength);

	*pTotalSize = TotalPacketLength;

	/// query first buffer
	NdisQueryBufferSafe	(CurrentBuffer, &VirtualAddress, &CurrentLength,NormalPagePriority);
	CurrentOffset = 0;
	
	/// loop
	while( nOffset || nNumberOfBytesToRead )
	{
		while( !CurrentLength )
		{
			NdisGetNextBuffer(CurrentBuffer, &CurrentBuffer);

			/// If we've reached the end of the packet.  We return with what
			/// we've done so far (which must be shorter than requested).
			if (!CurrentBuffer)
				return;
			
			NdisQueryBufferSafe(CurrentBuffer, &VirtualAddress, &CurrentLength,NormalPagePriority);
			CurrentOffset = 0;
		}

		if( nOffset )
		{
			// Compute how much data to move from this fragment
			if( CurrentLength > nOffset )
				CurrentOffset = nOffset;
			else
				CurrentOffset = CurrentLength;

			nOffset -= CurrentOffset;
			CurrentLength -= CurrentOffset;
		}

		if( nOffset )
		{
			CurrentLength = 0;
			continue;
		}

		if( !CurrentLength )
			continue;

		// Compute how much data to move from this fragment
		if (CurrentLength > nNumberOfBytesToRead)
			AmountToMove = nNumberOfBytesToRead;
		else
			AmountToMove = CurrentLength;

		// Copy the data.
		NdisMoveMemory(lpBuffer, &VirtualAddress[ CurrentOffset ], AmountToMove);

		// Update destination pointer
		lpBuffer += AmountToMove;

		// Update counters
		*lpNumberOfBytesRead +=AmountToMove;
		nNumberOfBytesToRead -=AmountToMove;
		CurrentLength = 0;
	}
}

/*
 *	decide on logging data coming from these ports/proto
 *
 */
BOOL nanondis_mustlog(IN ULONG srcip, IN USHORT srcport, IN ULONG dstip, IN USHORT dstport, IN USHORT protocol)
{
	PLIST_ENTRY		CurrentListEntry = NULL;
	ndis_rule*		ndisrule = NULL;
	BOOL			accepted = FALSE;

	/// if list is empty, don't care
	if (IsListEmpty(&nanondiscfg.ndisrules))
		return FALSE;

	/// walk list
	CurrentListEntry = nanondiscfg.ndisrules.Flink;
	while (TRUE)
	{
		ndisrule = (ndis_rule*)CurrentListEntry;

		/// check protocol
		if (ndisrule->protocol != protocol && ndisrule->protocol != IPPROTO_IP)
			goto __nextentry;

		/// protocol matches, check ports
		switch (ndisrule->direction)
		{
			case LOG_DIRECTION_INBOUND:
				/// in traffic
				if (((htons (ndisrule->srcport) == srcport) || ndisrule->srcport == 0) &&
					((ndisrule->srcip == srcip) || ndisrule->srcip == 0))
					accepted = TRUE;
			break;
			
			case LOG_DIRECTION_OUTBOUND:
				/// out traffic
				if (((htons (ndisrule->dstport) == dstport) || ndisrule->dstport == 0) &&
					((ndisrule->dstip == dstip) || ndisrule->dstip == 0))
					accepted = TRUE;
			break;
		
			case LOG_DIRECTION_ALL:
				// in/out traffic
				if ((htons (ndisrule->srcport) == srcport || ndisrule->srcport == 0) && 
					(htons (ndisrule->dstport) == dstport || ndisrule->dstport == 0) &&
					((ndisrule->srcip == srcip) || ndisrule->srcip == 0) &&
					((ndisrule->dstip == srcip) || ndisrule->dstip == 0))
					accepted = TRUE;
			break;
		}

		if (!accepted)
			goto __nextentry;

		/// accepted
		return TRUE;

__nextentry:
		if (CurrentListEntry->Flink == &nanondiscfg.ndisrules || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}

	return FALSE;
}

/*
*	walk packet and apply rules for filtering
*
*/
int nanondis_filterpacket(IN PNDIS_PACKET packet, IN int* sizetolog)
{
	ULONG bytesread = 0;
	ULONG totalsize = 0;
	ULONG fullpacketsize = 0;
	USHORT ethtype = 0;
	unsigned char hdr_eth [16];
	int result = NDIS_PASS_PACKET;
	ULONG srcip = 0;
	ULONG dstip = 0;
	USHORT srcport = 0;
	USHORT dstport = 0;
	struct ip ipHeader;
	struct tcphdr tcpHeader;
	struct udphdr udpHeader;
	struct icmp icmpHeader;
	USHORT	offset = 0;

	*sizetolog = 0;
	
	/// read ethernet header and store full packet size
	nanondis_readpacket(packet, hdr_eth, ETHER_HDR_LEN, 0, &bytesread, &totalsize);
	fullpacketsize = totalsize;
	
	/// read eth-type (IEEE 802.2/802.3 RFC 1042)
	nanondis_readpacket(packet, (PUCHAR )&ethtype, sizeof(USHORT), ETHER_ADDR_LEN*2, &bytesread, &totalsize);
	
	/// check for IEEE 802.2/802.3 (RFC 1042) encapsulation
	ethtype = htons (ethtype);
	if (ethtype <= 1500)
	{
		DBG_OUT (("nanondis_filterpacket : non-IEEE802.2/802.3 packet\n"));	
		if (nanondiscfg.log_noeth)
			result = NDIS_LOG_PACKET;
		goto __exit;
	}
	else
	{
		/// check for ethernet encapsulation (RFC 894)
		switch (ethtype)
		{
			case ETHERTYPE_IP:
				/// ip packets are processed later
			break;

			default:
				DBG_OUT (("nanondis_filterpacket : non-ip packet\n"));		
				if (nanondiscfg.log_noip)
					result = NDIS_LOG_PACKET;
			break;
		}
	}
	
	/// check if the non-ip packet must be logged
	if (result == NDIS_LOG_PACKET)
		goto __exit;

	/// read ip header
	nanondis_readpacket(packet, (PUCHAR)&ipHeader, sizeof(struct ip), ETHER_HDR_LEN, &bytesread, &totalsize);
	srcip = ipHeader.ip_src.s_addr;
	dstip = ipHeader.ip_dst.s_addr;

	/// check if its our communication (do not log)
	if (nanocore_is_reflector(htonl (srcip)) || nanocore_is_reflector(htonl (dstip)))
	{
		// too loud, disable this msg!
		// DBG_OUT (("nanondis_filterpacket : our packet, do not log\n"));	
		result = NDIS_PASS_PACKET;
		goto __exit;
	}

	switch(ipHeader.ip_p)
	{
		case IPPROTO_TCP:
			nanondis_readpacket(packet, (PUCHAR)&tcpHeader, sizeof(struct tcphdr), ETHER_HDR_LEN+ipHeader.ip_hl*4, &bytesread, &totalsize);
			srcport = tcpHeader.th_sport;
			dstport = tcpHeader.th_dport;
		break;

		case IPPROTO_UDP:
			nanondis_readpacket(packet, (PUCHAR)&udpHeader, sizeof(struct udphdr), ETHER_HDR_LEN+ipHeader.ip_hl*4, &bytesread, &totalsize);
			srcport = udpHeader.uh_sport;
			dstport = udpHeader.uh_dport;
		break;

		case IPPROTO_ICMP:
			nanondis_readpacket(packet,(PUCHAR)&icmpHeader, sizeof(struct icmp), ETHER_HDR_LEN+ipHeader.ip_hl*4, &bytesread, &totalsize);
			srcport = icmpHeader.icmp_type;
			dstport = icmpHeader.icmp_code;
		break;

		default:
			/// add others .....
		break;
	}

	/// check ip rules
	if (!nanondis_mustlog (srcip,srcport,dstip,dstport,ipHeader.ip_p))
		goto __exit;

	result = NDIS_LOG_PACKET;

	DBG_OUT (("nanondis_filterpacket : packet logged, rule matched : srcip %x, srcport %d, dstip %x, dstport %d, proto %d, size %d\n",
		srcip, htons (srcport), dstip, htons (dstport), ipHeader.ip_p, fullpacketsize));	

__exit:
	if (result == NDIS_LOG_PACKET)
		*sizetolog = fullpacketsize;
	return result;
}

/*
 *	process a received packet on the NIC, decide for logging and send packet to nanocore in case
 *
 */
VOID nanondis_processpacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN PNDIS_PACKET pRcvPacket)
{
	bpf_hdr* entry = NULL;
	ULONG sizetolog = 0;
	ULONG sizehdr = 0;
	BOOLEAN fromls = FALSE;
	ULONG bytesread = 0;
	ULONG totalsize = 0;
	LARGE_INTEGER now;

	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled) || !nanondiscfg.enabled)
		goto __exit;
	
	/// check if we must log this packet
	if (nanondis_filterpacket(pRcvPacket,&sizetolog) != NDIS_LOG_PACKET)
		goto __exit;

	entry = KAllocateFromLsOrPool(sizetolog + sizeof (bpf_hdr) + 1,NonPagedPool, &ls_entries, 1700, POOL_TAG, &fromls);
	if (!entry)
		goto __exit;
	memset (entry,0,sizetolog + sizeof (bpf_hdr));

	/// fill entry data
	nanondis_readpacket(pRcvPacket,((char*)entry)+sizeof (bpf_hdr), sizetolog, 0, &bytesread, &totalsize);

	KeQuerySystemTime (&now);
	ExSystemTimeToLocalTime (&now,&now);
	entry->bh_caplen = bytesread;
	entry->bh_datalen = totalsize;
	
	/// convert time to unix time
	entry->bh_tstamp.tv_sec = (LONG)(now.QuadPart/10000000-11644473600);
	entry->bh_tstamp.tv_usec = (LONG)((now.QuadPart%10000000)/10);
	
	/// add entry
	REVERT_BPFHDR (entry);
	log_addentry(NULL,NULL,entry,sizetolog + sizeof (bpf_hdr),RK_EVENT_TYPE_PACKETDATA, FALSE, FALSE);

__exit:
	if (entry)
	{
		if (fromls)
			KFreeFromLsOrPool (entry,&ls_entries,NonPagedPool);
		else
			KFreeFromLsOrPool (entry,NULL,0);
	}

	nanondis_freereceivepacket(pOpenContext,pRcvPacket);
	return;
}

/*
 *	called by ndis when NdisTransferData completes
 *
 */
VOID nanondis_transferdatacomplete(IN NDIS_HANDLE ProtocolBindingContext, IN PNDIS_PACKET pNdisPacket,
	IN NDIS_STATUS TransferStatus, IN UINT BytesTransferred)
{
	PNDISPROT_OPEN_CONTEXT   pOpenContext;
	PNDIS_BUFFER            pOriginalBuffer, pPartialBuffer;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

	///  Check if an NDIS_BUFFER was created to map part of the receive buffer;
	///  if so, free it and link back the original NDIS_BUFFER that maps
	///  the full receive buffer to the packet.
	pOriginalBuffer = NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pNdisPacket);
	if (pOriginalBuffer != NULL)
	{
		///  We had stashed off the NDIS_BUFFER for the full receive
		///  buffer in the packet reserved area. Unlink the partial
		///  buffer and link in the full buffer.
		NdisUnchainBufferAtFront(pNdisPacket, &pPartialBuffer);
		NdisChainBufferAtBack(pNdisPacket, pOriginalBuffer);

		///  Free up the partial buffer.
		NdisFreeBuffer(pPartialBuffer);
	}

	///  Queue this up for receive processing or free the packet
	if (TransferStatus == NDIS_STATUS_SUCCESS)
		nanondis_processpacket(pOpenContext, pNdisPacket);
	else
		nanondis_freereceivepacket(pOpenContext, pNdisPacket);

	return;
}

/*
 *	called by ndis on packet reception
 *
 */
INT nanondis_receivepacket(IN NDIS_HANDLE ProtocolBindingContext, IN PNDIS_PACKET pNdisPacket)
{
	PNDISPROT_OPEN_CONTEXT   pOpenContext;
	PNDIS_BUFFER            pNdisBuffer;
	UINT                    BufferLength;
	PNDISPROT_ETH_HEADER     pEthHeader;
	PNDIS_PACKET            pCopyPacket;
	PUCHAR                  pCopyBuf;
	UINT                    TotalPacketLength;
	UINT                    BytesCopied;
	INT                     RefCount = 0;
	NDIS_STATUS             Status;

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

#ifdef NDIS51
	NdisGetFirstBufferFromPacketSafe(pNdisPacket, &pNdisBuffer, &pEthHeader, &BufferLength, &TotalPacketLength, NormalPagePriority);
	///  The system is low on resources. Set up to handle failure below.
	if (pEthHeader == NULL)
	{
		BufferLength = 0;
	}
#else
	NdisGetFirstBufferFromPacket(pNdisPacket, &pNdisBuffer, &pEthHeader, &BufferLength, &TotalPacketLength);
#endif

	if (BufferLength < sizeof(NDISPROT_ETH_HEADER))
	{
		Status = NDIS_STATUS_NOT_ACCEPTED;
		goto __exit;
	}

	///  If the miniport is out of resources, we can't queue this packet - make a copy if this is so.
	if ((NDIS_GET_PACKET_STATUS(pNdisPacket) == NDIS_STATUS_RESOURCES))
	{
		pCopyPacket = nanondis_allocatereceivepacket(pOpenContext, TotalPacketLength, &pCopyBuf);
		if (pCopyPacket == NULL)
			goto __exit;

		NdisCopyFromPacketToPacket(pCopyPacket, 0, TotalPacketLength, pNdisPacket, 0, &BytesCopied);
		pNdisPacket = pCopyPacket;
	}
	else
	{
		///  We can queue the original packet - return
		///  a packet reference count indicating that
		///  we will call NdisReturnPackets when we are
		///  done with this packet.
		RefCount = 1;
	}

	//  queue this up
	nanondis_processpacket(pOpenContext, pNdisPacket);

__exit:
	return RefCount;
}

/*
 *	protocol receive handler called by ndis
 *
 */
NDIS_STATUS nanondis_receive(IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_HANDLE MacReceiveContext, IN PVOID pHeaderBuffer,
	IN UINT HeaderBufferSize, IN PVOID pLookaheadBuffer, IN UINT LookaheadBufferSize, IN UINT PacketSize)
{
	PNDISPROT_OPEN_CONTEXT   pOpenContext;
	NDIS_STATUS             Status;
	PNDIS_PACKET            pRcvPacket;
	PUCHAR                  pRcvData;
	UINT                    BytesTransferred;
	PNDIS_BUFFER            pOriginalNdisBuffer, pPartialNdisBuffer;
	
	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
	pRcvPacket = NULL;
	pRcvData = NULL;
	Status = NDIS_STATUS_SUCCESS;

	if (HeaderBufferSize != sizeof(NDISPROT_ETH_HEADER))
	{
		Status = NDIS_STATUS_NOT_ACCEPTED;
		return Status;
	}
	
	///  allocate resources for queueing this up.
	pRcvPacket = nanondis_allocatereceivepacket(pOpenContext,PacketSize+HeaderBufferSize, &pRcvData);
	if (pRcvPacket == NULL)
	{
		Status = NDIS_STATUS_NOT_ACCEPTED;
		return Status;
	}
	
	NdisMoveMappedMemory(pRcvData, pHeaderBuffer, HeaderBufferSize);

	///  Check if the entire packet is within the lookahead.
	if (PacketSize == LookaheadBufferSize)
	{
		NdisCopyLookaheadData(pRcvData+HeaderBufferSize, pLookaheadBuffer, LookaheadBufferSize, pOpenContext->MacOptions);
		
		///  queue this up for receive processing
		nanondis_processpacket(pOpenContext, pRcvPacket);
	}
	else
	{
		///  Allocate an NDIS buffer to map the receive area
		///  at an offset "HeaderBufferSize" from the current
		///  start. This is so that NdisTransferData can copy
		///  in at the right point in the destination buffer.
		NdisAllocateBuffer(&Status, &pPartialNdisBuffer, pOpenContext->RecvBufferPool, pRcvData + HeaderBufferSize, PacketSize);
		if (Status == NDIS_STATUS_SUCCESS)
		{
			///  Unlink and save away the original NDIS Buffer that maps the full receive buffer.
			NdisUnchainBufferAtFront(pRcvPacket, &pOriginalNdisBuffer);
			NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pRcvPacket) = pOriginalNdisBuffer;

			///  Link in the partial buffer for NdisTransferData to operate on.
			NdisChainBufferAtBack(pRcvPacket, pPartialNdisBuffer);
			NdisTransferData(&Status, pOpenContext->BindingHandle, MacReceiveContext, 0,
					PacketSize, pRcvPacket, &BytesTransferred);
		}
		else
		{
			///  Failure handled in TransferDataComplete.
			BytesTransferred = 0;
		}

		if (Status != NDIS_STATUS_PENDING)
			nanondis_transferdatacomplete((NDIS_HANDLE)pOpenContext, pRcvPacket, Status, BytesTransferred);
	}
	return Status;
}

/*
*	irp_mj_create event request handler
*
*/
VOID nanondis_evtcreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanondis_evtdevicecreate called\n"));
	WdfRequestComplete(Request, Status);
}

/*
*	irp_mj_device_control event request handler
*
*/
VOID nanondis_evtdevicecontrol (IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DBG_OUT (("nanondis_evtdevicecontrol called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
	default : 
		break;
	}

	WdfRequestComplete(Request, Status);
}

/*
 *	main
 *
 */
NTSTATUS DriverEntry(IN PDRIVER_OBJECT   DriverObject, IN PUNICODE_STRING RegistryPath)
{
	NDIS_PROTOCOL_CHARACTERISTICS protocolchar;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	WDF_DRIVER_CONFIG config;
	WDFDRIVER driver;
	PWDFDEVICE_INIT devinit = NULL;
	WCHAR buffer [256] = {0};
	CHAR pluginbldstring[32];
	WDF_OBJECT_ATTRIBUTES attributes;
	WDF_IO_QUEUE_CONFIG ioqueuecfg;
	WDFQUEUE queue;
	WDF_FILEOBJECT_CONFIG fileobjcfg;

	DBG_OUT (("nanondis DriverEntry\n"));
	
	/// check if nanocore is present
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	if (!KWaitForObjectPresent(&name,TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return STATUS_UNSUCCESSFUL;
	}

	/// initialize wdf driver
	WDF_DRIVER_CONFIG_INIT (&config,NULL);
	config.DriverInitFlags |= WdfDriverInitNonPnpDriver;
	Status = WdfDriverCreate (DriverObject,RegistryPath,WDF_NO_OBJECT_ATTRIBUTES,&config,&driver);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanondis DriverEntry driver %x created\n", driver));

	/// setup control device
	devinit = WdfControlDeviceInitAllocate(driver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if (!devinit)
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanondis DriverEntry cannot create deviceinit\n"));
		goto __exit;
	}

	/// assign name
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_NANONDIS_SYMLINK_NAME);
	Status = WdfDeviceInitAssignName(devinit,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set create handler
	WDF_FILEOBJECT_CONFIG_INIT(&fileobjcfg,nanondis_evtcreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(devinit,&fileobjcfg,WDF_NO_OBJECT_ATTRIBUTES);

	/// create control device
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	Status = WdfDeviceCreate (&devinit,&attributes,&nanondiscontroldev);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanondis DriverEntry controldevice %x created\n", nanondiscontroldev));

	/// create symbolic link
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\DosDevices\\%s",RK_NANONDIS_SYMLINK_NAME);
	Status = WdfDeviceCreateSymbolicLink (nanondiscontroldev,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create a default queue for requests
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchSequential);
	ioqueuecfg.EvtIoDeviceControl = nanondis_evtdevicecontrol;
	Status = WdfIoQueueCreate(nanondiscontroldev, &ioqueuecfg, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// control device is ready
	WdfControlFinishInitializing (nanondiscontroldev);

	/// register with nanocore
	RtlStringCbPrintfA (pluginbldstring,sizeof (pluginbldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	nanocore_registerplugin("nanondis",pluginbldstring,nanondis_reinitcfg,nanondis_uninstall,PLUGIN_TYPE_KERNELMODE);

	/// read configuration
	memset (&nanondiscfg,0,sizeof (nanondis_cfg));
	InitializeListHead(&nanondiscfg.ndisrules);
	if (nanondis_reinitcfg() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// set protocol characteristics and register protocol with ndis
	memset (&protocolchar,0,sizeof(NDIS_PROTOCOL_CHARACTERISTICS));
	RtlInitUnicodeString(&name,L"nanondis");
	
	protocolchar.MajorNdisVersion            = 5;
#ifdef NDIS51
	protocolchar.MinorNdisVersion            = 1;
#else
	protocolchar.MinorNdisVersion            = 0;
#endif
	protocolchar.Name                        = name;
	protocolchar.OpenAdapterCompleteHandler  = nanondis_openadaptercomplete;
	protocolchar.CloseAdapterCompleteHandler = nanondis_closeadaptercomplete;
	protocolchar.SendCompleteHandler = nanondis_sendcomplete;
	protocolchar.TransferDataCompleteHandler = nanondis_transferdatacomplete;
	protocolchar.ResetCompleteHandler = nanondis_resetcomplete;
	protocolchar.RequestCompleteHandler = nanondis_requestcomplete;
	protocolchar.ReceiveHandler = nanondis_receive;
	protocolchar.ReceiveCompleteHandler = nanondis_receivecomplete;
	protocolchar.StatusHandler = nanondis_status;
	protocolchar.StatusCompleteHandler = nanondis_statuscomplete;
	protocolchar.BindAdapterHandler = nanondis_bindadapter;
	protocolchar.UnbindAdapterHandler = nanondis_unbindadapter;
	protocolchar.ReceivePacketHandler = nanondis_receivepacket;
	protocolchar.PnPEventHandler = nanondis_pnpeventhandler;

	NdisRegisterProtocol((PNDIS_STATUS)&Status, &globals.NdisProtocolHandle, &protocolchar, sizeof(NDIS_PROTOCOL_CHARACTERISTICS));
	if (Status != NDIS_STATUS_SUCCESS)
	{
		DBG_OUT (("nanondis DriverEntry failed to register protocol (%x)\n", Status));
		goto __exit;
	}

	/// initialize globals
	NdisInitializeEvent (&globals.BindsComplete);
	InitializeListHead(&globals.OpenList);
	NdisAllocateSpinLock (&globals.GlobalLock);

	/// initialize a lookaside list to hold entries (> MTU size)
	ExInitializeNPagedLookasideList(&ls_entries, NULL, NULL, 0, 1700, 'trpn', 0);

__exit:
	return STATUS_SUCCESS;
}

