#ifndef __nanonet_common_h__
#define __nanonet_common_h__

#include <ntifs.h>
#include <nanonet.h>
#include <rkcfghandle.h>

#define POOL_TAG 'tenn'

#ifndef IPPROTO_IP
#define IPPROTO_IP              0
#endif

#ifndef IPPROTO_TCP
#define IPPROTO_TCP				6
#endif

#ifndef IPPROTO_UDP
#define IPPROTO_UDP				17
#endif

/*
*	globals
*
*/
nanonet_cfg	nanonetcfg;			/// module configuration
NPAGED_LOOKASIDE_LIST ls_httpreqinfo;	/// http request info ls list

/*
*	ansi version for processname
*
*/
long nanonet_mustloga (OPTIONAL IN PCHAR processname, OPTIONAL IN int processnamesize, IN OUT int* direction, OPTIONAL IN unsigned long srcip, OPTIONAL IN unsigned short srcport, OPTIONAL IN unsigned long dstip, OPTIONAL IN unsigned short destport, OPTIONAL IN int protocol);

/*
*	check the network rules, returns size to be logged. on return direction holds the rule->direction that matches
*
*/
long nanonet_mustlog (OPTIONAL IN PWCHAR processname, OPTIONAL IN int processnamesize, IN OUT int* direction, OPTIONAL IN unsigned long srcip, OPTIONAL IN unsigned short srcport, OPTIONAL IN unsigned long dstip, OPTIONAL IN unsigned short destport, OPTIONAL IN int protocol);

/*
*	check the http rules on the provided url string
*
*/
BOOLEAN nanonet_mustloghttp (IN net_http_req_info* reqinfo);

/*
*	parse tagged-value linked list and build inmemory configuration
*
*/
int nanonet_parsecfg (UXmlNode *ModuleConfig, nanonet_cfg* cfg);

/*
*	scan buffer for URL and http rules matching. return FALSE if we're not interested in this buffer
*
*/
BOOLEAN nanonet_process_stream_buffer (IN net_data* hdr, IN ctx_netdatabuffer* buf, OPTIONAL IN long* nextoffset);

/*
*	uninstall
*
*/
int nanonet_uninstall ();

/*
*	initialize/reinitialize configuration (called by nanocore too on new configuration message)
*
*/
int nanonet_reinitcfg ();

/*
*	nanonet common initialization
*
*/
int nanonet_common_init (PUNICODE_STRING RegistryPath);

/*
*	common initialization last stage
*
*/
int nanonet_common_finish_initialization ();

#endif /// __nanonet_common_h__