/*
*	nano network filter (using tdi)
*  -vx-
*/

#include <nanocore.h>
#include <klib.h>
#include <modrkcore.h>
#include <tdikrnl.h>
#include <tdi.h>
#include <nanonet.h>
#include <rkcfghandle.h>

#include "nanonet_common.h"

#define POOL_TAG 'tenn'

/*
 *	structs
 *
 */
typedef struct _evthandlers
{
	PTDI_IND_CONNECT					conn_evt;
	void*								conn_ctx;
	PTDI_IND_DISCONNECT					disconn_evt;
	void*								disconn_ctx;
	PTDI_IND_RECEIVE					recv_evt;
	void*								recv_ctx;
	PTDI_IND_CHAINED_RECEIVE			chainedrecv_evt;
	void*								chainedrecv_ctx;
	PTDI_IND_RECEIVE_EXPEDITED			recvexpedited_evt;
	void*								recvexpedited_ctx;
	PTDI_IND_CHAINED_RECEIVE_EXPEDITED	chainedrecvexpedited_evt;
	void*								chainedrecvexpedited_ctx;
} evthandlers;

typedef struct _tdiaddress
{
	LIST_ENTRY							chain;
	PFILE_OBJECT						fileobj;
	evthandlers							handlers;
	struct _tdiconnection*				associatedconnection;
	unsigned long						localip;
	unsigned short						localport;
} tdiaddress;

typedef struct _tdiconnection {
	LIST_ENTRY		chain;
	PFILE_OBJECT	fileobj;
	void*			ctx;
	ctx_netdata		ctxnetdata;
	PEPROCESS		eprocess;
	tdiaddress*		associatedaddress;
	PIO_WORKITEM	wrkitem;
	int skip;
	PIRP			oldirp;			/// if a new irp is being allocated, the old is here
}tdiconnection;

/*
 *	globals
 *
 */

PDEVICE_OBJECT tcpdevobj; /// lower device object
PDEVICE_OBJECT filterdevice; /// our filter
NPAGED_LOOKASIDE_LIST tdiconn_lookaside; /// connections are allocated from this lookaside
LIST_ENTRY tdiconn_list;
NPAGED_LOOKASIDE_LIST tdiaddr_lookaside; /// addresses are allocated from this lookaside
LIST_ENTRY tdiaddr_list;
NPAGED_LOOKASIDE_LIST tdiconninfo_lookaside; /// connection information lookaside list (used to get local ip)
KSPIN_LOCK tdiconn_lock; /// lock for tdi connections list
KSPIN_LOCK tdiaddr_lock; /// lock for tdi address list
unsigned long processnameoffset; /// for querying processname from eprocess

/*
 *	call this in systemprocess to query for processname offset (must be called only once)
 *
 */
unsigned long tdi_lookup_processnameoffset ()
{
	PEPROCESS	current = PsGetCurrentProcess();
	unsigned long idx = 0;
	
	__try
	{
		for (idx = 0; idx < 0x1000; idx++)
		{
			if (!strncmp("System", (PCHAR)current + idx, strlen("System")))
				return idx;
		}
	}

	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		return 0;
	}

	return 0;
}

/*
 *	get processname from eprocess (need to call lookup_processnameoffset before, once, to work)
 *  
 */
void tdi_processnamebyprocess (IN PEPROCESS process, OUT PWCHAR processname, IN int processnamesize)
{
	char* n = NULL;
	int i = 0;
	int len;

	if (!processnameoffset || !process || !processname || (processnamesize < 16))
	{
		wcsncpy (processname,L"???????",processnamesize);
		return;
	}
	
	if (!MmIsAddressValid((PVOID)((ULONG)process + processnameoffset)))
	{
		wcsncpy (processname,L"???????",processnamesize);
		return;
	}
	
	/// copy, converting to unicode
	n = (char*)process + processnameoffset;
	memset ((char*)processname,0,processnamesize);
	for (i=0;i < 16; i++)
	{
		processname[i] = (WCHAR)n[i];
	}
	return;
}

/*
 *	decide whether to log data for this connection. this will fill relevant fields for subsequent operations on connection
 *
 */
int tdi_flag_connection_for_logging (tdiconnection* conn)
{
	
	long sizetolog = 0;
	ctx_netdata* ctx = NULL;

	if (!conn)
		return 0;

	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// decide for logging
	sizetolog = nanonet_mustlog((PWCHAR)ctx->info.process, 32*sizeof(WCHAR), &ctx->logdirection, ctx->info.srcip, htons(ctx->info.srcport), ctx->info.dstip, htons(ctx->info.dstport), 0);
	if (sizetolog == 0)
	{
		/// do not log this connection, neither data nor info
		return 0;
	}
	else
	{		
		conn->skip = FALSE;

		/// setup context
		switch (sizetolog)
		{
			case NET_LOG_INFO :
				/// no data to be logged, only info
				ctx->sizetolog = 0;
			break;

			case NET_LOG_DATA_FULL:
				/// set to max
				ctx->sizetolog = nanonetcfg.mem_max;
			break;

			default:
				/// adjust
				if (sizetolog > nanonetcfg.mem_max)
					ctx->sizetolog = nanonetcfg.mem_max;
			break;
		}
	}

	return 1;
}

/*
*	find an address object in list from a given fileobject
*
*/
tdiaddress* tdi_find_address_from_fileobj (IN PFILE_OBJECT fileobj)
{
	KIRQL	Irql;
	tdiaddress*	addr = NULL;

	KeAcquireSpinLock(&tdiaddr_lock, &Irql);
	addr = (tdiaddress*)tdiaddr_list.Flink;

	// walk our address circular buffer to scan for address object
	while (!IsListEmpty(&tdiaddr_list) && addr != (tdiaddress*)&tdiaddr_list)
	{
		if (addr->fileobj == fileobj)
		{
			KeReleaseSpinLock(&tdiaddr_lock, Irql);
			return addr;
		}

		addr = (tdiaddress*)addr->chain.Flink;
	}

	KeReleaseSpinLock(&tdiaddr_lock, Irql);

	return NULL;
}

/*
*	find a connection object in list from a given fileobject
*
*/
tdiconnection* tdi_find_connection_from_fileobj (IN PFILE_OBJECT fileobj)
{
	KIRQL			Irql;
	tdiconnection*	conn = NULL;

	KeAcquireSpinLock(&tdiconn_lock, &Irql);

	// walk our connections circular buffer to scan for connection object
	conn = (tdiconnection*)tdiconn_list.Flink;
	while (!IsListEmpty(&tdiconn_list) && conn != (tdiconnection*)&tdiconn_list)
	{
		if (conn->fileobj == fileobj)
		{
			KeReleaseSpinLock(&tdiconn_lock, Irql);
			return conn;
		}

		conn = (tdiconnection*)conn->chain.Flink;
	}

	KeReleaseSpinLock(&tdiconn_lock, Irql);
	return NULL;
}

/*
*	find a connection object in list from a given context
*
*/
tdiconnection* tdi_find_connection_from_context (IN PVOID ctx)
{
	KIRQL			Irql;
	tdiconnection*	conn = NULL;

	KeAcquireSpinLock(&tdiconn_lock, &Irql);
	
	conn = (tdiconnection*)tdiconn_list.Flink;

	// walk our connections circular buffer to scan for connection object
	while (!IsListEmpty(&tdiconn_list) && conn != (tdiconnection*)&tdiconn_list)
	{
		if (conn->ctx == ctx)
		{
			KeReleaseSpinLock(&tdiconn_lock, Irql);
			return conn;
		}

		conn = (tdiconnection*) conn->chain.Flink;
	}

	KeReleaseSpinLock(&tdiconn_lock, Irql);
	return NULL;
}

/*
*	allocate connection object
*
*/
tdiconnection* tdi_allocateconnection ()
{
	tdiconnection*	p = NULL;

	p = ExAllocateFromNPagedLookasideList(&tdiconn_lookaside);
	if (!p)
		return NULL;

	memset (p,0,sizeof (tdiconnection));
	return p;
}

/*
*	free connection object
*
*/
void tdi_freeconnection (IN tdiconnection* connentry)
{
	if (!connentry)
		return;

	ExFreeToNPagedLookasideList(&tdiconn_lookaside, connentry);
}

/*
*	allocate address object
*
*/
tdiaddress* tdi_allocateaddress ()
{
	tdiaddress* p = NULL;

	p = ExAllocateFromNPagedLookasideList(&tdiaddr_lookaside);
	if (!p)
		return NULL;

	memset (p,0,sizeof (tdiaddress));
	return p;
}

/*
*	free address object
*
*/
void tdi_freeaddress (IN tdiaddress* addressentry)
{
	if (!addressentry)
		return;

	ExFreeToNPagedLookasideList(&tdiaddr_lookaside, addressentry);
}

/*
*	standard completion routine which sets an event and returns status more processing
*	required. The irp must then be completed by us.
*/
NTSTATUS tdi_seteventcomplete(IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID ctx)
{
	// optimize setting event only when the irp is pending. This avoid to hold the dispatcher lock everytime
	if (irp->PendingReturned && ctx)
	{
		KeSetEvent((PKEVENT)ctx, IO_NO_INCREMENT, FALSE);
	}

	return STATUS_MORE_PROCESSING_REQUIRED;
}

/*
*	simple completion routine
*	
*/
NTSTATUS tdi_standard_complete(IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID ctx)
{	
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);

	return STATUS_SUCCESS;
}

/*
*	query address information
*
*/
NTSTATUS tdi_queryaddressinfo (IN PDEVICE_OBJECT devobj, IN PFILE_OBJECT fileobj, IN unsigned char* infobuf, IN unsigned long* infobufsize)
{
	NTSTATUS          Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK   iosb;
	PIRP  irp = NULL;
	PMDL  mdl = NULL;
	KEVENT evt;
	
	// check params
	if (!devobj || !fileobj || !infobuf || !infobufsize)
		goto __exit;
	if (!*infobufsize)
		goto __exit;

	// set device object
	memset (infobuf,0,*infobufsize);

	// allocate irp
	irp = IoAllocateIrp( devobj->StackSize + 3, FALSE );
	if( !irp )
		return STATUS_INSUFFICIENT_RESOURCES;

	// allocate and probe mdl
	mdl = IoAllocateMdl (infobuf, *infobufsize, FALSE, FALSE, NULL);
	if (!mdl)
		goto __exit;
	
	try
	{
		MmProbeAndLockPages( mdl, KernelMode, IoModifyAccess );
	}
	__except( EXCEPTION_EXECUTE_HANDLER )
	{
		IoFreeMdl( mdl );
		mdl = NULL;
		goto __exit;
	}
	mdl->Next = NULL;

	// build tdiqueryinformation irp
	TdiBuildQueryInformation(irp, devobj, fileobj, tdi_seteventcomplete,  NULL, TDI_QUERY_ADDRESS_INFO, mdl );

	// call lower driver
	KeInitializeEvent (&evt,NotificationEvent,FALSE);
	IoSetCompletionRoutine (irp, tdi_seteventcomplete, &evt, TRUE, TRUE, TRUE);
	Status = IoCallDriver (devobj,irp);

	// and wait if needed
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&evt,Executive,KernelMode,FALSE,NULL);
		Status = irp->IoStatus.Status;
	}
	*infobufsize = irp->IoStatus.Information;

__exit:
	// free resources
	if (mdl)
	{
		MmUnlockPages (mdl);
		IoFreeMdl(mdl);
	}
	if (irp)
		IoFreeIrp(irp);

	return Status;
}

/*
*	dump collected data calling nanonet log function
*
*/
VOID nanonet_dumpdata (IN tdiconnection* conn)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ctx_netdatabuffer* netdatabuf = NULL;
	net_data* hdr = NULL;
	net_data_chunk* dataChunk = NULL;
	unsigned char* buffer = NULL;
	int offset = 0;
	ctx_netdata* ctx = NULL;
	char srcipstring [20] = {0};
	char dstipstring [20] = {0};
	
	DBG_OUT (("nanonet_dumpdata\n"));
	if (!conn)
		return;

	/// get netdata context
	ctx = (ctx_netdata*)&conn->ctxnetdata;
	if (conn->skip)
	{
		/// should be empty already....
		KFreeList(&ctx->outinchain); 
		return;
	}

	/// allocate a buffer to hold the whole content
	buffer = ExAllocatePoolWithTag(NonPagedPool, ctx->currentsizelogged + sizeof (net_data) + 1024,POOL_TAG);
	if (!buffer)
	{
		/// be sure to free memory before return
		KFreeList(&ctx->outinchain);
		return;
	}

#if DBG
	KIpToAscii(ctx->info.srcip,srcipstring,sizeof (srcipstring));
	KIpToAscii(ctx->info.dstip,dstipstring,sizeof (dstipstring));
#endif

	hdr = (net_data *)buffer;
	
	/// copy these from connection context
	hdr->datasize = htonl(ctx->currentsizelogged);
	hdr->dstip = htonl (ctx->info.dstip);	/// must be nbo on receive, must be htonled
	hdr->srcip = htonl (ctx->info.srcip);	/// must be nbo on receive, must be htonled
	hdr->srcport = ctx->info.srcport;		/// no htons, will be htonsed on receive
	hdr->dstport = ctx->info.dstport;		/// no htons, will be htonsed on receive
	wcsncpy ((PWCHAR)&hdr->process,(PWCHAR)&ctx->info.process,32);

	/// fill these other ourself
	hdr->cbsize = sizeof (net_data);
	hdr->protocol = IPPROTO_TCP; /// forced on nanomod xp (no udp logging)
	KeQuerySystemTime(&hdr->timestampEnd);
	ExSystemTimeToLocalTime(&hdr->timestampEnd, &hdr->timestampEnd);
	hdr->timestampEnd.LowPart = htonl(hdr->timestampEnd.LowPart);
	hdr->timestampEnd.HighPart = htonl(hdr->timestampEnd.HighPart);

	offset += sizeof(net_data);
	while (!IsListEmpty(&ctx->outinchain))
	{
		netdatabuf = (ctx_netdatabuffer*)RemoveHeadList(&ctx->outinchain);
		dataChunk = (net_data_chunk *)(buffer + offset);
		dataChunk->cbsize = htonl(sizeof(net_data_chunk));
		dataChunk->datasize = htonl(netdatabuf->datasize);
		dataChunk->direction = htonl(netdatabuf->direction);
		dataChunk->timestamp.LowPart = htonl(netdatabuf->timestamp.LowPart);
		dataChunk->timestamp.HighPart = htonl(netdatabuf->timestamp.HighPart);
		offset += sizeof(net_data_chunk);

		/// copy data
		memcpy (buffer+offset,&netdatabuf->data,netdatabuf->datasize);
		offset+=netdatabuf->datasize;			

		/// free netdatabuffer context
		ExFreePool(netdatabuf);
	}

	/// pass data to nanocore for logging
	DBG_OUT (("nanonet_dumpdata logged conn %x process %S dstport %d, dstip %s, srcport %d, srcip %s\n", conn, (PWCHAR)hdr->process, htons (hdr->dstport), dstipstring, htons (hdr->srcport), srcipstring));
	log_addentry(NULL,NULL,buffer,offset,RK_EVENT_TYPE_NETDATA, KeGetCurrentIrql() == PASSIVE_LEVEL ? TRUE : FALSE,(BOOLEAN)nanonetcfg.flushexpedited);
	ExFreePool(buffer);

	return;
}

/*
*	cleanup connection object, here data will be dumped
*
*/
NTSTATUS nanonet_cleanup_conn (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	PFILE_OBJECT fileobj = irpsp->FileObject;
	tdiconnection* conn = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// free connection
	DBG_OUT (("nanonet_cleanup_conn\n"));
	conn = tdi_find_connection_from_fileobj(fileobj);
	if (conn)
	{
		/// dump data and free
		RemoveEntryList(&conn->chain);		
		nanonet_dumpdata (conn);
		tdi_freeconnection(conn);
	}

	/// check stack locations
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, tdi_standard_complete, NULL, TRUE, TRUE, TRUE);
	return IoCallDriver(devext->AttachedDevice, irp);
}

/*
 *	cleanup address object
 *
 */
NTSTATUS nanonet_cleanup_addr (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	PFILE_OBJECT fileobj = irpsp->FileObject;
	tdiaddress* addr = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	DBG_OUT (("nanonet_cleanup_addr\n"));

	/// free address
	addr = tdi_find_address_from_fileobj(fileobj);
	if (addr)
	{
		RemoveEntryList(&addr->chain);
		tdi_freeaddress(addr);
	}

	/// check stack locations
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, tdi_standard_complete, NULL, TRUE, TRUE, TRUE);
	return IoCallDriver(devext->AttachedDevice, irp);
}

/*
*	create connection completion routine
*
*/
NTSTATUS nanonet_create_conn_complete (IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID Context)
{
	NTSTATUS Status = irp->IoStatus.Status;
	ctx_netdata* ctx = NULL;
	tdiconnection* conn = NULL;
	PFILE_FULL_EA_INFORMATION ea = (PFILE_FULL_EA_INFORMATION)irp->AssociatedIrp.SystemBuffer;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);

	DBG_OUT (("nanonet_create_conn_complete\n"));
	
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);

	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;
	
	/// allocate connection
	conn = tdi_allocateconnection();
	if (!conn)
		return STATUS_SUCCESS;
	
	/// initialize
	ctx = (ctx_netdata*)&conn->ctxnetdata;
	InitializeListHead(&conn->ctxnetdata.outinchain);
	conn->ctx = *((CONNECTION_CONTEXT*) &(ea->EaName[ea->EaNameLength + 1]));
	conn->eprocess = IoGetRequestorProcess(irp);
	conn->fileobj = irpsp->FileObject;
	conn->skip = TRUE;

	/// add to list
	ExInterlockedInsertTailList(&tdiconn_list, &conn->chain, &tdiconn_lock);
	return STATUS_SUCCESS;
}

/*
*	create connection handler
*
*/
NTSTATUS nanonet_create_conn (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	/// set completion
	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, nanonet_create_conn_complete, NULL, TRUE, TRUE, TRUE);
	return IoCallDriver(devext->AttachedDevice, irp);
}

/*
 *	create address completion routine
 *
 */
NTSTATUS nanonet_create_addr_complete (IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID ctx)
{
	NTSTATUS Status = irp->IoStatus.Status;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	tdiaddress* addr = NULL;
	PTDI_ADDRESS_INFO addrinfo = NULL;
	PTRANSPORT_ADDRESS transportaddr = NULL;

	DBG_OUT (("nanonet_create_addr_complete\n"));
	
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);
	
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// allocate address object
	addr = tdi_allocateaddress();
	if (!addr)
		goto __exit;
	addr->fileobj = irpsp->FileObject;

	/// get local address
	if (ctx)
	{
		addrinfo = (PTDI_ADDRESS_INFO)ctx;
		transportaddr = &addrinfo->Address;
		addr->localip = (unsigned long)((PTDI_ADDRESS_IP)transportaddr->Address[0].Address)->in_addr;
		addr->localport = (unsigned short)((PTDI_ADDRESS_IP)transportaddr->Address[0].Address)->sin_port;
		DBG_OUT (("nanonet_create_addr_complete, localip = %x, localport = %d\n",addr->localip, htons (addr->localport)));
	}		

	/// and add to list
	ExInterlockedInsertTailList(&tdiaddr_list, &addr->chain, &tdiaddr_lock);

	
__exit:
	/// free connection info (for local ip)
	if (ctx)
		ExFreeToNPagedLookasideList(&tdiconninfo_lookaside,ctx);
	return STATUS_SUCCESS;
}

/*
*	create address handler
*
*/
NTSTATUS nanonet_create_addr (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	unsigned long infobufsize = 260;
	unsigned char* buffer = NULL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	DBG_OUT (("nanonet_create_addr\n"));
	
/*
	/// while we're at passive level, get the local ip for address
	buffer = ExAllocateFromNPagedLookasideList(&tdiconninfo_lookaside);
	if (!buffer)
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice,irp);
		return Status;
	}
		
	Status = tdi_queryaddressinfo(devext->AttachedDevice,irpsp->FileObject,buffer,&infobufsize);
	if (!NT_SUCCESS (Status))
	{
		ExFreeToNPagedLookasideList(&tdiconninfo_lookaside,buffer);
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice,irp);
		return Status;
	}
*/
	
	/// set completion passing the local address buffer
	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, nanonet_create_addr_complete, buffer, TRUE, TRUE, TRUE);
	return IoCallDriver(devext->AttachedDevice, irp);
}

/*
*	client disconnect handler
*
*/
NTSTATUS nanonet_evt_clientdisconn(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext, IN LONG DisconnectDataLength,
								   IN PVOID DisconnectData, IN LONG DisconnectInformationLength, IN PVOID DisconnectInformation, IN ULONG DisconnectFlags)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	DBG_OUT (("nanonet_evt_clientdisconn\n"));
	if (!TdiEventContext)
		return STATUS_SUCCESS;
	if (!((tdiaddress*)TdiEventContext)->handlers.disconn_evt)
		return STATUS_SUCCESS;

	/// call handler
	Status = ((tdiaddress*)TdiEventContext)->handlers.disconn_evt(((tdiaddress*)TdiEventContext)->handlers.disconn_ctx,
		ConnectionContext, DisconnectDataLength, DisconnectData, DisconnectInformationLength, DisconnectInformation, DisconnectFlags);

	return Status;
}

/*
*	client connect handler
*
*/
NTSTATUS nanonet_evt_clientconn (IN PVOID TdiEventContext, IN LONG RemoteAddressLength,
					   IN PVOID RemoteAddress, IN LONG UserDataLength, IN PVOID UserData, IN LONG OptionsLength,
					   IN PVOID Options, OUT CONNECTION_CONTEXT* ConnectionContext, OUT PIRP* AcceptIrp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	tdiconnection*	conn = NULL;
	PTA_IP_ADDRESS	addr = RemoteAddress;
	ctx_netdata* ctx = NULL;

	/// call original handler
	DBG_OUT (("nanonet_evt_clientconn\n"));
	Status = ((tdiaddress*)TdiEventContext)->handlers.conn_evt(((tdiaddress*)TdiEventContext)->handlers.conn_ctx,
		RemoteAddressLength, RemoteAddress, UserDataLength, UserData, OptionsLength, Options, ConnectionContext, AcceptIrp);
	if (Status != STATUS_MORE_PROCESSING_REQUIRED)
		return Status;

	/// get connection
	conn = tdi_find_connection_from_context(*ConnectionContext);
	if (!conn)
		return Status;

	/// get addresses
	ctx = (ctx_netdata*)&conn->ctxnetdata;	
	if (conn->associatedaddress)
	{
		ctx->info.srcip = conn->associatedaddress->localip;
		ctx->info.srcport = conn->associatedaddress->localport;
	}
	ctx->info.dstip = (unsigned long)((PTDI_ADDRESS_IP)addr->Address[0].Address)->in_addr;
	ctx->info.dstport = (unsigned short)((PTDI_ADDRESS_IP)addr->Address[0].Address)->sin_port;

	/// get process
	tdi_processnamebyprocess(conn->eprocess,(PWCHAR)ctx->info.process,32);

	/// decide for logging
	if (!tdi_flag_connection_for_logging(conn))
	{
		/// do not log this connection, neither data nor info
		DBG_OUT (("nanonet_evt_clientconn connection %x dstip %x dstport %d srcip %x srcport %d skipped by rules\n", 
			conn, ctx->info.dstip, ctx->info.dstport, ctx->info.srcip, ctx->info.srcport));
	}
	else
	{		
		DBG_OUT (("nanonet_evt_clientconn connection %x dstip %x dstport %d srcip %x srcport %d accepted by rules\n", 
			conn, ctx->info.dstip, ctx->info.dstport, ctx->info.srcip, ctx->info.srcport));
	}
	
	return Status;
}

/*
*	client receive completion
*
*/
NTSTATUS nanonet_evt_clientrecv_complete (IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID Context)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	tdiconnection*	conn = NULL;
	ctx_netdata*	ctx = NULL;
	ctx_netdatabuffer* netdatabuf = NULL;
	unsigned long datasize = 0;
	unsigned long copied = 0;

	DBG_OUT (("nanonet_evt_clientrecv_complete\n"));
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);
	Status = irp->IoStatus.Status;
	if (!NT_SUCCESS(Status) || !Context)
		return STATUS_SUCCESS;

	conn = ((tdiaddress*)Context)->associatedconnection;
	if (!conn)
		return STATUS_SUCCESS;
	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// no checks here, already done in the client event routine. just log
	if (irp->IoStatus.Information == 0)
		return STATUS_SUCCESS;
	datasize = irp->IoStatus.Information;

	/// allocate and copy buffer
	netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + datasize + 1,POOL_TAG);
	if (!netdatabuf)
		return STATUS_SUCCESS;
	TdiCopyMdlToBuffer(irp->MdlAddress, 0, &netdatabuf->data, 0, datasize, &copied);
	if (copied == 0)
	{
		ExFreePool (netdatabuf);
		return STATUS_SUCCESS;
	}
	netdatabuf->datasize = (unsigned long)copied;

	/// set all other fields and insert in list
	DBG_OUT (("nanonet_evt_clientrecv_complete logging data conn %x, dstip %x\n", conn, ctx->info.dstip));

	netdatabuf->direction = LOG_DIRECTION_INBOUND;
	KeQuerySystemTime(&netdatabuf->timestamp);
	ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
	ctx->currentsizelogged += (long)copied + sizeof(net_data_chunk);
	InsertTailList(&ctx->outinchain, &netdatabuf->chain);

	return STATUS_SUCCESS;
}

/*
*	client receive handler
*
*/
NTSTATUS nanonet_evt_clientrecv (IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
					   IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
					   OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	tdiconnection* conn = NULL;
	ctx_netdata* ctx = NULL;
	ctx_netdatabuffer* netdatabuf = NULL;
	unsigned long datasize = 0;

	// call the original handler
	DBG_OUT (("nanonet_evt_clientrecv\n"));
	Status = ((tdiaddress*) TdiEventContext)->handlers.recv_evt(((tdiaddress*)TdiEventContext)->handlers.recv_ctx,
		ConnectionContext, ReceiveFlags, BytesIndicated, BytesAvailable, BytesTaken, Tsdu, IoRequestPacket);
	if (Status == STATUS_DATA_NOT_ACCEPTED)
		return Status;

	/// get connection and context
	conn = ((tdiaddress*)TdiEventContext)->associatedconnection;
	if (!conn)
		return Status;
	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// check size to log and skip
	if (!ctx->sizetolog || conn->skip)
		return Status;

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if (ctx->logdirection != LOG_DIRECTION_INBOUND)
			return Status;
	}

	/// check maxsize reached
	if ((ctx->currentsizelogged) > ctx->sizetolog)
		return Status;

	/// log in completion routine ?
	if (Status == STATUS_MORE_PROCESSING_REQUIRED && IoRequestPacket)
	{
		if (*IoRequestPacket)
		{
			if ((*IoRequestPacket)->CurrentLocation == 1)
			{
				(*IoRequestPacket)->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
				(*IoRequestPacket)->IoStatus.Information = 0;
				Status = (*IoRequestPacket)->IoStatus.Status;
				IoCompleteRequest(*IoRequestPacket, IO_NO_INCREMENT);
				return Status;
			}

			// set completion and log data there
			IoCopyCurrentIrpStackLocationToNext(*IoRequestPacket);
			IoSetCompletionRoutine(*IoRequestPacket, nanonet_evt_clientrecv_complete, TdiEventContext, TRUE, TRUE, TRUE);
			IoSetNextIrpStackLocation(*IoRequestPacket);
		}
	}
	else
	{
		/// received bytes
		datasize = *BytesTaken;
		if (datasize == 0 || !Tsdu)
			return Status;

		/// allocate and copy buffer
		netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + datasize + 1,POOL_TAG);
		if (!netdatabuf)
			return Status;
		memcpy (&netdatabuf->data,Tsdu,datasize);
		netdatabuf->datasize = datasize;

		/// set all other fields and insert in list
		DBG_OUT (("nanonet_evt_clientrecv logging data conn %x, dstip %x\n", conn, ctx->info.dstip));

		netdatabuf->direction = LOG_DIRECTION_INBOUND;
		KeQuerySystemTime(&netdatabuf->timestamp);
		ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
		ctx->currentsizelogged += (long)datasize + sizeof(net_data_chunk);
		InsertTailList(&ctx->outinchain, &netdatabuf->chain);
	}

	return Status;
}

/*
*	client chained receive expedited handler
*
*/
NTSTATUS nanonet_evt_clientchainedrecvexpedited(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext, 
				IN ULONG ReceiveFlags, IN ULONG ReceiveLength, IN ULONG StartingOffset, IN PMDL Tsdu, IN PVOID TsduDescriptor)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	DBG_OUT (("nanonet_clientchainedrecvexpedited\n"));
	
	// we just call the real handler here....
	Status = ((tdiaddress*) TdiEventContext)->handlers.chainedrecvexpedited_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecvexpedited_ctx,
		ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);

	return Status;
}

/*
*	client receive expedited handler
*
*/
NTSTATUS nanonet_evt_clientrecvexpedited(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
								IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
								OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ctx_netdata*	ctx = NULL;
	ctx_netdatabuffer* netdatabuf = NULL;
	unsigned long datasize = 0;
	unsigned long copied = 0;
	tdiconnection* conn = NULL;

	/// call real handler
	DBG_OUT (("nanonet_evt_clientrecvexpedited\n"));
	Status = ((tdiaddress*)TdiEventContext)->handlers.recvexpedited_evt(((tdiaddress*)TdiEventContext)->handlers.recvexpedited_ctx,
		ConnectionContext, ReceiveFlags, BytesIndicated, BytesAvailable, BytesTaken, Tsdu, IoRequestPacket);
	if (Status == STATUS_DATA_NOT_ACCEPTED)
		return Status;

	conn = ((tdiaddress*)TdiEventContext)->associatedconnection;
	if (!conn)
		return Status;
	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// check size to log or skip
	if (!ctx->sizetolog || conn->skip)
		return Status;

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if (ctx->logdirection != LOG_DIRECTION_INBOUND)
			return Status;
	}

	/// check maxsize reached
	if ((ctx->currentsizelogged) > ctx->sizetolog)
		return Status;

	/// log in completion routine ?
	if (Status == STATUS_MORE_PROCESSING_REQUIRED && IoRequestPacket)
	{
		if (*IoRequestPacket)
		{
			if ((*IoRequestPacket)->CurrentLocation == 1)
			{
				(*IoRequestPacket)->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
				(*IoRequestPacket)->IoStatus.Information = 0;
				Status = (*IoRequestPacket)->IoStatus.Status;
				IoCompleteRequest(*IoRequestPacket, IO_NO_INCREMENT);
				return Status;
			}

			// set completion and log data there
			IoCopyCurrentIrpStackLocationToNext(*IoRequestPacket);
			IoSetCompletionRoutine(*IoRequestPacket, nanonet_evt_clientrecv_complete, TdiEventContext, TRUE, TRUE, TRUE);
			IoSetNextIrpStackLocation(*IoRequestPacket);
		}
	}
	else
	{
		/// received bytes
		datasize = *BytesTaken;
		if (datasize == 0 || !Tsdu)
			return Status;

		/// allocate and copy buffer
		netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + datasize + 1,POOL_TAG);
		if (!netdatabuf)
			return Status;
		memcpy (&netdatabuf->data,Tsdu,datasize);
		netdatabuf->datasize = datasize;

		/// set all other fields and insert in list
		DBG_OUT (("nanonet_evt_clientrecvexpedited logging data conn %x, dstip %x\n", conn, ctx->info.dstip));

		netdatabuf->direction = LOG_DIRECTION_INBOUND;
		KeQuerySystemTime(&netdatabuf->timestamp);
		ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
		ctx->currentsizelogged += (long)datasize + sizeof(net_data_chunk);
		InsertTailList(&ctx->outinchain, &netdatabuf->chain);
	}

	return Status;
}

/*
 *	client chained receive handler
 *
 */
NTSTATUS nanonet_evt_clientchainedrecv (IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
	IN ULONG ReceiveFlags, IN ULONG ReceiveLength, IN ULONG StartingOffset, IN PMDL Tsdu, IN PVOID TsduDescriptor)
{
	NTSTATUS		Status = STATUS_UNSUCCESSFUL;
	tdiconnection*	conn = NULL;
	ctx_netdata*	ctx = NULL;
	ctx_netdatabuffer* netdatabuf = NULL;
	unsigned long datasize = 0;
	unsigned long copied = 0;

	DBG_OUT (("nanonet_evt_clientchainedrecv\n"));
	conn = ((tdiaddress*)TdiEventContext)->associatedconnection;
	if (!conn)
	{
		/// call real handler
		Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
			ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
		return Status;
	}
	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// check size to log or skip
	if (!ctx->sizetolog || conn->skip)
	{
		// call real handler
		Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
			ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
		return Status;
	}

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if (ctx->logdirection != LOG_DIRECTION_INBOUND)
		{
			// call real handler
			Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
				ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
			return Status;
		}
	}

	/// check maxsize reached
	if ((ctx->currentsizelogged) > ctx->sizetolog)
	{
		// call real handler
		Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
			ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
		return Status;
	}

	if (ReceiveLength != 0)
	{
		// copy data to buffer before the real handler is called. This is because ndis will reacquire the buffer later, and we could loose data
		datasize = ReceiveLength;
		netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + datasize + 1,POOL_TAG);
		if (!netdatabuf)
		{
			// call real handler
			Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
				ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
			return Status;
		}

		TdiCopyMdlToBuffer(Tsdu,StartingOffset,&netdatabuf->data, 0, datasize, &copied);
		if (copied == 0)
		{
			ExFreePool(netdatabuf);

			// call real handler
			Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
				ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);
			return Status;
		}
		netdatabuf->datasize = copied;

		/// set all other fields and insert in list
		DBG_OUT (("nanonet_evt_clientchainedrecv logging data conn %x, dstip %x\n", conn, ctx->info.dstip));

		netdatabuf->direction = LOG_DIRECTION_INBOUND;
		KeQuerySystemTime(&netdatabuf->timestamp);
		ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
		ctx->currentsizelogged += (long)datasize + sizeof(net_data_chunk);
		InsertTailList(&ctx->outinchain, &netdatabuf->chain);
	}

	// call real handler, done
	Status = ((tdiaddress*)TdiEventContext)->handlers.chainedrecv_evt(((tdiaddress*)TdiEventContext)->handlers.chainedrecv_ctx, 
		ConnectionContext, ReceiveFlags, ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);

	return Status;
}


/*
*	install tdi client event callbacks (TDI_SET_EVENT handler)
*
*/
NTSTATUS nanonet_install_clienthandlers (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	tdiaddress* addr = NULL;
	PTDI_REQUEST_KERNEL_SET_EVENT	req = NULL;
	PIO_STACK_LOCATION	nextirpsp = IoGetNextIrpStackLocation(irp);
	PTDI_REQUEST_KERNEL_SET_EVENT	nextreq = NULL;

	/// check stack locations
	DBG_OUT (("nanonet_install_clienthandlers\n"));
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	/// set empty completion routine
	IoCopyCurrentIrpStackLocationToNext(irp);
	req = (PTDI_REQUEST_KERNEL_SET_EVENT)&irpsp->Parameters;
	nextreq = (PTDI_REQUEST_KERNEL_SET_EVENT)&nextirpsp->Parameters;
	IoSetCompletionRoutine(irp, NULL, NULL, FALSE, FALSE, FALSE);

	/// get address
	addr = tdi_find_address_from_fileobj(irpsp->FileObject);
	if (!addr)
		return IoCallDriver(devext->AttachedDevice, irp);
	
	/// check the request type and install the proper callback
	switch (req->EventType)
	{
		case TDI_EVENT_CONNECT:
			if (req->EventHandler)
			{
				addr->handlers.conn_evt = req->EventHandler;
				addr->handlers.conn_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientconn;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.conn_evt = NULL;
				addr->handlers.conn_ctx = NULL;
			}
		break;

		case TDI_EVENT_DISCONNECT:
			if (req->EventHandler)
			{
				addr->handlers.disconn_evt = req->EventHandler;
				addr->handlers.disconn_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientdisconn;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.disconn_evt = NULL;
				addr->handlers.disconn_ctx = NULL;
			}
		break;
		
		case TDI_EVENT_RECEIVE:
			if (req->EventHandler)
			{
				addr->handlers.recv_evt = req->EventHandler;
				addr->handlers.recv_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientrecv;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.recv_evt = NULL;
				addr->handlers.recv_ctx = NULL;
			}
		break;

		case TDI_EVENT_CHAINED_RECEIVE:
			if (req->EventHandler)
			{
				addr->handlers.chainedrecv_evt = req->EventHandler;
				addr->handlers.chainedrecv_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientchainedrecv;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.chainedrecv_evt = NULL;
				addr->handlers.chainedrecv_ctx = NULL;
			}
		break;

		case TDI_EVENT_RECEIVE_EXPEDITED:
			if (req->EventHandler)
			{
				addr->handlers.recvexpedited_evt = req->EventHandler;
				addr->handlers.recvexpedited_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientrecvexpedited;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.recvexpedited_evt = NULL;
				addr->handlers.recvexpedited_ctx = NULL;
			}
		break;

		case TDI_EVENT_CHAINED_RECEIVE_EXPEDITED:
			if (req->EventHandler)
			{
				addr->handlers.chainedrecvexpedited_evt = req->EventHandler;
				addr->handlers.chainedrecvexpedited_ctx = req->EventContext;
				nextreq->EventHandler = nanonet_evt_clientchainedrecvexpedited;
				nextreq->EventContext = addr;
			}
			else
			{
				nextreq->EventHandler = NULL;
				nextreq->EventContext = NULL;
				addr->handlers.chainedrecvexpedited_evt = NULL;
				addr->handlers.chainedrecvexpedited_ctx = NULL;
			}
		break;

		default:
			break;
	}

	return IoCallDriver(devext->AttachedDevice, irp);
}

/*
 *	completion for nanonet_associateaddr
 *
 */
NTSTATUS nanonet_associateaddr_complete(IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	tdiconnection* conn = NULL;
	PFILE_OBJECT fileobj = NULL;
	PTDI_REQUEST_KERNEL_ASSOCIATE associatereq = NULL;
	tdiaddress* addr = NULL;

	DBG_OUT (("nanonet_associateaddr_complete\n"));
	
	Status = irp->IoStatus.Status;
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);
	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;

	/// get connection
	conn = tdi_find_connection_from_fileobj(irpsp->FileObject);
	if (!conn)
		return STATUS_SUCCESS;

	/// and address
	associatereq = (PTDI_REQUEST_KERNEL_ASSOCIATE)&irpsp->Parameters;
	Status = ObReferenceObjectByHandle(associatereq->AddressHandle, FILE_ANY_ACCESS, NULL, KernelMode, &fileobj, NULL);
	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;
	
	addr = tdi_find_address_from_fileobj(fileobj);
	ObDereferenceObject(fileobj);
	if (!addr)
		return STATUS_SUCCESS;

	/// associate connection and address
	conn->associatedaddress = addr;
	addr->associatedconnection = conn;

	return STATUS_SUCCESS;
}

/*
 *	associate address with connection
 *
 */
NTSTATUS nanonet_associateaddr (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	DBG_OUT (("nanonet_associateaddr\n"));
	
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, nanonet_associateaddr_complete, NULL, TRUE, TRUE, TRUE);
	Status = IoCallDriver(devext->AttachedDevice, irp);
	return Status;
}

/*
*	TDI_RECEIVE completion
*
*/
NTSTATUS nanonet_recv_complete (IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID Context)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	tdiconnection*	conn = NULL;
	ctx_netdata* ctx = NULL;
	ctx_netdatabuffer* netdatabuf = NULL;
	unsigned long datasize = 0;
	unsigned long copied = 0;

	DBG_OUT (("nanonet_recv_complete\n"));

	Status = irp->IoStatus.Status;
	if (irp->PendingReturned)
		IoMarkIrpPending(irp);

	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;

	/// get connection and context
	conn = (tdiconnection*)Context;
	if (!conn)
		return STATUS_SUCCESS;

	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// received bytes
	datasize = irp->IoStatus.Information;
	if (datasize == 0)
		return STATUS_SUCCESS;

	/// allocate and copy buffer
	netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + datasize + 1,POOL_TAG);
	if (!netdatabuf)
		return STATUS_SUCCESS;
	TdiCopyMdlToBuffer(irp->MdlAddress, 0, &netdatabuf->data, 0, datasize, &copied);
	if (copied == 0)
	{
		ExFreePool (netdatabuf);
		return STATUS_SUCCESS;
	}
	netdatabuf->datasize = (unsigned long)copied;

	/// set all other fields and insert in list
	DBG_OUT (("nanonet_recv_complete logging data conn %x, dstip %x\n", conn, ctx->info.dstip));

	netdatabuf->direction = LOG_DIRECTION_INBOUND;
	KeQuerySystemTime(&netdatabuf->timestamp);
	ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
	ctx->currentsizelogged += (long)copied + sizeof(net_data_chunk);
	InsertTailList(&ctx->outinchain, &netdatabuf->chain);

	return STATUS_SUCCESS;
}

/*
*	TDI_RECEIVE handler
*
*/
NTSTATUS nanonet_recv(IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	tdiconnection* conn = NULL;
	ctx_netdata* ctx = NULL;

	DBG_OUT (("nanonet_recv\n"));
	
	/// check stack locations
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	/// get connection
	conn = tdi_find_connection_from_fileobj(irpsp->FileObject);
	if (!conn)
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
		return Status;
	}
	
	/// check sizetolog
	ctx = (ctx_netdata*)&conn->ctxnetdata;
	if (!ctx->sizetolog || conn->skip)
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
		return Status;
	}

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if (ctx->logdirection != LOG_DIRECTION_INBOUND)
		{
			IoSkipCurrentIrpStackLocation(irp);
			Status = IoCallDriver(devext->AttachedDevice, irp);
			return Status;
		}
	}

	/// check maxsize reached
	if ((ctx->currentsizelogged) > ctx->sizetolog)
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
		return Status;
	}

	/// ok, log data
	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, nanonet_recv_complete, conn, TRUE, TRUE, TRUE);
	Status = IoCallDriver(devext->AttachedDevice, irp);
	return Status;
}

/*
*	TDI_SEND handler. here we inspect http requests too
*
*/
NTSTATUS nanonet_send(IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	tdiconnection* conn = NULL;
	ctx_netdata* ctx = NULL;
	unsigned long sizedata = 0;
	unsigned long copied = 0;
	ctx_netdatabuffer* netdatabuf = NULL;
	int logthis = FALSE;
	long nextoffset = 0;

	DBG_OUT (("nanonet_send\n"));
	
	/// check stack locations
	if (irp->CurrentLocation == 1)
	{
		irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		irp->IoStatus.Information = 0;
		Status = irp->IoStatus.Status;
		IoCompleteRequest(irp, IO_NO_INCREMENT);
		return Status;
	}

	/// get connection
	conn = tdi_find_connection_from_fileobj(irpsp->FileObject);
	if (!conn)
		goto __exit;
	if (conn->skip)
		goto __exit;
	ctx = (ctx_netdata*)&conn->ctxnetdata;

	/// check direction
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		/// if direction is not outbound
		if (ctx->logdirection != LOG_DIRECTION_OUTBOUND)
		{
			/// check for loghttpreq, either skip
			if (!nanonetcfg.loghttpreq)
				goto __exit;
		}
	}

	/// get data size
	sizedata = irp->MdlAddress->ByteCount;
	if (!sizedata)
		goto __exit;

	/// allocate and copy tdi data to our buffer
	netdatabuf = ExAllocatePoolWithTag(NonPagedPool, sizeof (ctx_netdatabuffer) + sizedata + 32,POOL_TAG);
	if (!netdatabuf)
		goto __exit;
	memset (netdatabuf,0,sizeof(ctx_netdatabuffer) + sizedata + 32);
	TdiCopyMdlToBuffer(irp->MdlAddress, 0, &netdatabuf->data, 0, sizedata, &copied);
	if (copied == 0)
	{
		ExFreePool(netdatabuf);
		goto __exit;
	}
	netdatabuf->datasize = (unsigned long)copied;

	/// for http, we need to inspect buffer for requestinfo if asked. We do so inspecting outbound requests on port 80
	if (nanonetcfg.loghttpreq && (ctx->info.dstport == htons (80)))
	{
		while (TRUE)
		{
			logthis+=nanonet_process_stream_buffer (&ctx->info,netdatabuf,&nextoffset);
			if (!nextoffset)
				break;
		}
		if (!logthis)
		{
			/// we're not interested in this buffer (do not match http rules)
			ExFreePool(netdatabuf);
			goto __exit;
		}
	}

	if (ctx->sizetolog == 0 || (ctx->currentsizelogged > ctx->sizetolog))
	{
		/// we're called at info level, or we already logged the requested amount
		ExFreePool(netdatabuf);
		goto __exit;
	}

	/// recheck direction, to log data
	if (ctx->logdirection != LOG_DIRECTION_ALL)
	{
		if (ctx->logdirection != LOG_DIRECTION_OUTBOUND)
		{
			ExFreePool(netdatabuf);
			goto __exit;
		}
	}

	/// set all other fields and insert in list
	DBG_OUT (("nanonet_send logging data conn %x, dstip %x\n", conn, ctx->info.dstip));
	netdatabuf->direction = LOG_DIRECTION_OUTBOUND;
	KeQuerySystemTime(&netdatabuf->timestamp);
	ExSystemTimeToLocalTime(&netdatabuf->timestamp, &netdatabuf->timestamp);
	ctx->currentsizelogged += (long)copied + sizeof(net_data_chunk);
	InsertTailList(&ctx->outinchain, &netdatabuf->chain);
	
__exit:
	IoCopyCurrentIrpStackLocationToNext(irp);
	IoSetCompletionRoutine(irp, tdi_standard_complete, conn, TRUE, TRUE, TRUE);
	Status = IoCallDriver(devext->AttachedDevice, irp);
	return Status;
}

/*
*	TDI_CONNECT completion
*
*/
NTSTATUS nanonet_connect_complete (IN PDEVICE_OBJECT devobj, IN PIRP irp, IN PVOID Context)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	tdiconnection*	conn = NULL;
	long sizetolog = 0;

	DBG_OUT (("nanonet_connect_complete\n"));

	conn = (tdiconnection*)Context;
	
	/// fix netbt bug (w2k+)
	if (conn->oldirp)
	{
		/// complete original irp
		DBG_OUT (("nanonet_connect_complete completing original irp %x and freeing new irp %x\n", conn->oldirp, irp));
		conn->oldirp->IoStatus.Status = irp->IoStatus.Status;
		conn->oldirp->IoStatus.Information = irp->IoStatus.Information;
		IoCompleteRequest(conn->oldirp, IO_NO_INCREMENT);
		conn->oldirp = NULL;

		// and free the new irp
		IoFreeIrp(irp);
		Status = STATUS_MORE_PROCESSING_REQUIRED;
	}
	else
	{
		// handle original irp
		if (irp->PendingReturned)
			IoMarkIrpPending(irp);
		Status = STATUS_SUCCESS;
	}

	return Status;
}

/*
*	TDI_CONNECT handler
*
*/
NTSTATUS nanonet_connect (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	PTDI_REQUEST_KERNEL	req = NULL;
	tdiconnection* conn = NULL;
	ctx_netdata* ctx = NULL;
	PTRANSPORT_ADDRESS	addr = NULL;
	PIRP newirp = NULL;
	char srcipstring [20] = {0};
	char dstipstring [20] = {0};

	DBG_OUT (("nanonet_connect\n"));
	
	/// get connection
	conn = tdi_find_connection_from_fileobj(irpsp->FileObject);
	if (!conn)
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
		return Status;
	}

	/// get remote address from request, and local address from associated address
	ctx = (ctx_netdata*)&conn->ctxnetdata;	
	req = (PTDI_REQUEST_KERNEL)&irpsp->Parameters;
	addr = req->RequestConnectionInformation->RemoteAddress;	
	if (conn->associatedaddress)
	{
		ctx->info.srcip = conn->associatedaddress->localip;
		ctx->info.srcport = conn->associatedaddress->localport;
	}
	ctx->info.dstip = (unsigned long)((PTDI_ADDRESS_IP)addr->Address[0].Address)->in_addr;
	ctx->info.dstport = (unsigned short)((PTDI_ADDRESS_IP)addr->Address[0].Address)->sin_port;

#if DBG
	KIpToAscii(ctx->info.srcip,srcipstring,sizeof (srcipstring));
	KIpToAscii(ctx->info.dstip,dstipstring,sizeof (dstipstring));
#endif

	/// get process
	tdi_processnamebyprocess(conn->eprocess,(PWCHAR)ctx->info.process,32);

	/// decide for logging
	if (!tdi_flag_connection_for_logging(conn))
	{
		/// do not log this connection, neither data nor info
		DBG_OUT (("nanonet_connect connection %x dstip %s dstport %d srcip %s srcport %d skipped by rules\n", 
			conn, dstipstring, ctx->info.dstport, srcipstring, ctx->info.srcport));
	}
	else
	{		
		DBG_OUT (("nanonet_connect connection %x dstip %s dstport %d srcip %s srcport %d accepted by rules\n", 
			conn, dstipstring, ctx->info.dstport, srcipstring, ctx->info.srcport));
	}
	
	/// let the TDI_CONNECT go, by checking stacklocations first
	if (irp->CurrentLocation <= 1)
	{
		/// fix netbt bug (w2k+)
		conn->oldirp = irp;
		newirp = KAllocateCopyIrp (irp, devext->AttachedDevice->StackSize + 3, nanonet_connect_complete, conn);
		if (!newirp)
		{
			// fail the request if we can't allocate the new irp
			irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
			irp->IoStatus.Information = 0;
			Status = irp->IoStatus.Status;
			IoCompleteRequest(irp, IO_NO_INCREMENT);
			return Status;
		}
		Status = IoCallDriver(devext->AttachedDevice, newirp);
	}
	else
	{
		// bug not triggered
		conn->oldirp = NULL;
		IoCopyCurrentIrpStackLocationToNext(irp);
		IoSetCompletionRoutine(irp, nanonet_connect_complete, conn, TRUE, TRUE, TRUE);
		Status = IoCallDriver(devext->AttachedDevice, irp);
	}

	return Status;
}

/*
*	internal devicecontrol dispatch for connection objects
*
*/
NTSTATUS nanonet_internal_devicecontrol_conn (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	DBG_OUT (("nanonet_internal_devicecontrol_conn\n"));
	
	switch (irpsp->MinorFunction)
	{
		case TDI_ASSOCIATE_ADDRESS:
			Status = nanonet_associateaddr (devobj, irp);
		break;

		case TDI_RECEIVE:
			Status = nanonet_recv (devobj, irp);
		break;

		case TDI_SEND:
			Status = nanonet_send (devobj, irp);
		break;

		case TDI_CONNECT:
			Status = nanonet_connect (devobj, irp);
		break;

		case TDI_ACCEPT:
			DBG_OUT (("TDI_ACCEPT not supported"));
			IoSkipCurrentIrpStackLocation(irp);
			Status = IoCallDriver(devext->AttachedDevice, irp);
		break;

		default:
			IoSkipCurrentIrpStackLocation(irp);
			Status = IoCallDriver(devext->AttachedDevice, irp);
		break;
	}

	return Status;
}

/*
*	internal devicecontrol dispatch for address objects
*
*/
NTSTATUS nanonet_internal_devicecontrol_addr (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	DBG_OUT (("nanonet_internal_devicecontrol_addr\n"));

	if (irpsp->MinorFunction == TDI_SET_EVENT_HANDLER)
	{
		/// install client handlers
		Status = nanonet_install_clienthandlers(devobj, irp);
	}
	else
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
	}
	return Status;
}

/*
*	internal devicecontrol dispatch (IRP_MJ_INTERNAL_DEVICE_CONTROL)
*
*/
NTSTATUS nanonet_internal_devicecontrol (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	// 	DBG_OUT (("nanonet_internal_devicecontrol\n"));
	if ((unsigned long)irpsp->FileObject->FsContext2 == TDI_CONNECTION_FILE)
	{
		/// connection object
		return nanonet_internal_devicecontrol_conn(devobj,irp);
	}
	else if ((unsigned long)irpsp->FileObject->FsContext2 == TDI_TRANSPORT_ADDRESS_FILE)
	{
		/// address object
		return nanonet_internal_devicecontrol_addr(devobj,irp);
	}
	else
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver(devext->AttachedDevice, irp);
	}
	return Status;
}

/*
 *	irp_mj_device_control handler
 *
 */
NTSTATUS nanonet_devicecontrol (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;

	// 	DBG_OUT (("nanonet_devicecontrol\n"));
	Status = TdiMapUserRequest (devobj,irp,irpsp);
	if (!NT_SUCCESS (Status))
	{
		IoSkipCurrentIrpStackLocation(irp);
		Status = IoCallDriver (devext->AttachedDevice,irp);
	}
	else
	{
		/// map to internal device control
		Status = nanonet_internal_devicecontrol (devobj,irp);
	}

	return Status;
}

/*
*	main dispatch routine
*
*/
NTSTATUS nanonet_dispatch (IN PDEVICE_OBJECT devobj, IN PIRP irp)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PFILE_FULL_EA_INFORMATION ea = NULL;
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	
	/// check if log is enabled, else skip
	if (!nanonetcfg.enabled)
	{
		IoSkipCurrentIrpStackLocation(irp);
		return IoCallDriver(devext->AttachedDevice,irp);
	}

	switch (irpsp->MajorFunction)
	{
		case IRP_MJ_CREATE:
			ea = (FILE_FULL_EA_INFORMATION *)irp->AssociatedIrp.SystemBuffer;
			if (!ea)
			{
				IoSkipCurrentIrpStackLocation(irp);
				Status = IoCallDriver(devext->AttachedDevice,irp);
				break;
			}				
			if (ea->EaNameLength == TDI_TRANSPORT_ADDRESS_LENGTH) 
			{
				if (memcmp (ea->EaName,TdiTransportAddress,TDI_TRANSPORT_ADDRESS_LENGTH) == 0)
				{
					/// create address object
					Status = nanonet_create_addr(devobj,irp);
				}
			}
			else if (ea->EaNameLength == TDI_CONNECTION_CONTEXT_LENGTH) 
			{
				if (memcmp (ea->EaName,TdiConnectionContext,TDI_CONNECTION_CONTEXT_LENGTH) == 0)			
				{
					/// create connection object
					Status = nanonet_create_conn (devobj,irp);
				}
			}
			else
			{
				IoSkipCurrentIrpStackLocation(irp);
				Status = IoCallDriver(devext->AttachedDevice,irp);
			}
		break;
		
		case IRP_MJ_CLEANUP:
			if ((unsigned long)irpsp->FileObject->FsContext2 == TDI_CONNECTION_FILE)
			{
				/// cleanup connection object
				Status = nanonet_cleanup_conn (devobj,irp);
			}
			else if ((unsigned long)irpsp->FileObject->FsContext2 == TDI_TRANSPORT_ADDRESS_FILE)
			{			
				/// cleanup address object
				Status = nanonet_cleanup_addr (devobj,irp);
			}
			else
			{
				IoSkipCurrentIrpStackLocation(irp);
				Status = IoCallDriver(devext->AttachedDevice,irp);
			}
		break;
		
		case IRP_MJ_DEVICE_CONTROL:
			/// device control request
			Status = nanonet_devicecontrol (devobj, irp);
		break;

		case IRP_MJ_INTERNAL_DEVICE_CONTROL:
			/// internal device control request (TDI_*)
			Status = nanonet_internal_devicecontrol (devobj,irp);
		break;

		default:
			IoSkipCurrentIrpStackLocation(irp);
			Status = IoCallDriver(devext->AttachedDevice, irp);
		break;
	}

	return Status;
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PDEVICE_EXTENSION devext = NULL;
	UNICODE_STRING name;
	int idx = 0;
	
	/// nanonet initialization
	processnameoffset = tdi_lookup_processnameoffset();
	if (nanonet_common_init(RegistryPath) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	ExInitializeNPagedLookasideList(&tdiconn_lookaside, NULL, NULL, 0, sizeof(tdiconnection), 'CdTN', 0);
	ExInitializeNPagedLookasideList(&tdiaddr_lookaside, NULL, NULL, 0, sizeof(tdiaddress), 'AdTN', 0);
	ExInitializeNPagedLookasideList(&tdiconninfo_lookaside, NULL, NULL, 0, 260, 'niTN', 0);
	InitializeListHead(&tdiconn_list);
	InitializeListHead(&tdiaddr_list);
	KeInitializeSpinLock(&tdiconn_lock);
	KeInitializeSpinLock(&tdiaddr_lock);

	// we need to grab the tcp device, and get its top device
	RtlInitUnicodeString(&name,L"\\Device\\Tcp");
	tcpdevobj = KWaitForObjectPresent(&name,FALSE);
	if (!tcpdevobj)
		goto __exit;
	tcpdevobj = IoGetAttachedDevice(tcpdevobj);

	/// create filter device
	Status = IoCreateDevice(DriverObject, sizeof(DEVICE_EXTENSION), NULL, tcpdevobj->DeviceType, 
		tcpdevobj->Characteristics &= ~FILE_DEVICE_SECURE_OPEN, FALSE, &filterdevice);
	if (!NT_SUCCESS(Status))
	{
		DBG_OUT (("nanonet can't create device\n"));
		goto __exit;
	}
	DBG_OUT (("nanonet created wdmdevice %x\n", filterdevice));

	/// attach filter to tcp device
	devext = ((PDEVICE_EXTENSION) (filterdevice->DeviceExtension));
	devext->AttachedDevice = IoAttachDeviceToDeviceStack(filterdevice,tcpdevobj);
	if (!NT_SUCCESS(Status))
	{
		DBG_OUT (("nanonet can't attach filter\n"));
		goto __exit;
	}

	// ok
	devext->Size = sizeof(DEVICE_EXTENSION);
	devext->Type = DEVICE_TYPE_TDIFLTDEVICE;
	devext->DriverObject = DriverObject;
	filterdevice->Flags = tcpdevobj->Flags;
	filterdevice->Flags &= ~DO_DEVICE_HAS_NAME;
	filterdevice->Flags &= ~DO_DEVICE_INITIALIZING;
	Status = STATUS_SUCCESS;

	/// set dispatch routines
	for (idx = 0; idx <= IRP_MJ_MAXIMUM_FUNCTION; idx++)
	{
		DriverObject->MajorFunction[idx] = nanonet_dispatch;
	}

	/// finish initialization
	if (nanonet_common_finish_initialization() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
		
__exit:
	DBG_OUT (("nanonet DriverEntry Status = %x\n",Status));
	return STATUS_SUCCESS;
}
