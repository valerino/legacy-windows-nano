#ifndef __apihklib_h__
#define __apihklib_h__

// Disable warning messages
#pragma warning( disable : 4311 4312 )  

typedef struct __tagAPIHK_EXCLUSION_ENTRY {
	CHAR szSubString [32];			// exclude processes with this substring from apihook
} APIHK_EXCLUSION_ENTRY, *PAPIHK_EXCLUSION_ENTRY;

// trampoline structure
typedef struct __tagAPIHK_HOOK_STRUCT {
	FARPROC TrampolineAddress;			// trampoline address
	ULONG	TrampolineSize;				// trampoline size (without the added JMP)
	PBYTE	FunctionAddress;			// original function address
} APIHK_HOOK_STRUCT, *PAPIHK_HOOK_STRUCT;

// used to call vxdcall on 9x
#define PC_WRITABLE		0x00020000
#define PC_USER			0x00040000
#define PC_PRESENT		0x80000000
#define PC_STATIC		0x20000000
#define PC_DIRTY		0x08000000
#define PageModifyPermissions	0x0001000d

// disasm engine stuff
#define SIZEOFJUMP			5
#define ASMNOP			0x90
#define C_ERROR         0xFFFFFFFF
#define C_PREFIX        0x00000001
#define C_66            0x00000002
#define C_67            0x00000004
#define C_DATA66        0x00000008
#define C_DATA1         0x00000010
#define C_DATA2         0x00000020
#define C_DATA4         0x00000040
#define C_MEM67         0x00000080
#define C_MEM1          0x00000100
#define C_MEM2          0x00000200
#define C_MEM4          0x00000400
#define C_MODRM         0x00000800
#define C_DATAW0        0x00001000
#define C_FUCKINGTEST   0x00002000
#define C_TABLE_0F      0x00004000

//************************************************************************/
// public                                                                     
// 
// 
//************************************************************************/
// these will be exported too, to be available from plugins
__declspec( dllexport )BOOL ApiHkInstallHook (char* DllName, char* FuncName, DWORD ReplacementFunc, void* hk, BOOL useantidetect);
__declspec( dllexport )BOOL ApiHkUninstallHook (void* hk);
__declspec( dllexport )BOOL ApiHkApplyExclusionRules (void* pEntry);                                                                   
#endif // #ifndef __apihklib_h__