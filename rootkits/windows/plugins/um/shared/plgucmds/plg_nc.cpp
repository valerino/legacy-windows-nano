/*
*  netcat module for plgucmds plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <string.h>
#include <time.h>
#include <strsafe.h>
#include <ulib.h>
#include <plgucmds.h>

/*
*	establish tunnel between srcip (ip:port) and tgtip(ip:port) via this machine on the selected ports
*
*	example : this is B
*	B connect to A port 1023
*  B connect to C port 1234
*  recv from A->send to C, recv from C->send to A
*/

int go_tunnel (IN unsigned short* srcip, IN unsigned short* tgtip)
{ 
	char buf[4096];
	char ansisrcaddr [128];
	char ansidstaddr [128];
	fd_set fdsr;
	SOCKET sock1 = 0, sock2 = 0;
	time_t activity = time(NULL);
	int nbyt = 0;
	int closeneeded = 0;
	int maxsock = 0;
	char* p = NULL;
	struct addrinfo* src_addrinfo = NULL;
	struct addrinfo* tgt_addrinfo = NULL;
	struct sockaddr srcaddr;
	struct sockaddr tgtaddr;
	WSADATA wsaData;
	int idletimeout = 300;
	int res = -1;
	size_t numchar = 0;

	if (!srcip || !tgtip)
		return -1;
	if ((wcschr ((PWCHAR)srcip,(WCHAR)':') == NULL) || (wcschr ((PWCHAR)tgtip,(WCHAR)':') == NULL))
		return -1;
	if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
		return -1;

	DBG_OUT (("plg_ucmds go_tunnel\n"));
	
	// get ports and addresses
	wcstombs_s(&numchar,ansisrcaddr,128,(PWCHAR)srcip,_TRUNCATE);
	wcstombs_s(&numchar,ansidstaddr,128,(PWCHAR)tgtip,_TRUNCATE);

	p = strchr (ansisrcaddr,':');
	*p = '\0';
	p++;
	if (getaddrinfo (ansisrcaddr,p,NULL,&src_addrinfo) != 0)
		goto __exit;
	
	p = strchr(ansidstaddr,':');
	*p = '\0';
	p++;
	if (getaddrinfo (ansidstaddr,p,NULL,&tgt_addrinfo) != 0)
		goto __exit;
	memcpy (&srcaddr,&src_addrinfo->ai_addr[0],src_addrinfo->ai_addrlen);
	memcpy (&tgtaddr,&tgt_addrinfo->ai_addr[0],tgt_addrinfo->ai_addrlen);

	// create sockets
	sock1 = socket (AF_INET,SOCK_STREAM,IPPROTO_TCP);
	sock2 = socket (AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if (!sock1 || !sock2)
		goto __exit;

	maxsock = (int)((sock2 > sock1)?sock2:sock1 )+1;

	// connect sockets
	if (connect(sock1,&srcaddr,sizeof (struct sockaddr)) == SOCKET_ERROR)
		goto __exit;
	if (connect(sock2,&tgtaddr,sizeof (struct sockaddr)) == SOCKET_ERROR)
		goto __exit;

	WDBG_OUT ((L"plg_ucmds tunneling from %s to %s\n",srcip,tgtip));

	/* main polling loop. */
	while (1)
	{
		struct timeval tv = {1,0};
		time_t now = time(NULL);

		// setup sockets
		FD_ZERO(&fdsr);
		FD_SET(sock1,&fdsr);
		FD_SET(sock2,&fdsr);

		if (select(maxsock, &fdsr, NULL, NULL, &tv) < 0)
			continue;

		/* service any client connections that have waiting data. */
		if (FD_ISSET(sock1, &fdsr)) 
		{
			if ((nbyt = recv(sock1, buf, sizeof(buf), 0)) <= 0 || send(sock2, buf, nbyt, 0) <= 0) 
				closeneeded = 1;
			else 
				activity = now;
		} 
		if (FD_ISSET(sock2, &fdsr)) 
		{
			if ((nbyt = recv(sock2, buf, sizeof(buf), 0)) <= 0 || send(sock1, buf, nbyt, 0) <= 0) 
				closeneeded = 1;
			else 
				activity = now;
		}

		if ((sock1 || sock2) && now - activity > idletimeout) 
		{
			closeneeded = 1;
		}
		if (closeneeded) 
		{
			closesocket(sock1);
			closesocket(sock2);
			sock1 = 0;
			sock2 = 0;
			closeneeded = 0;
			break;
		}      
	}

	// done
	res = 0;

__exit:
	DBG_OUT (("plg_ucmds go_tunnel exit\n"));

	if (sock1)
		closesocket(sock1);
	if (sock2)
		closesocket(sock2);
	if (src_addrinfo)
		freeaddrinfo (src_addrinfo);
	if (tgt_addrinfo)
		freeaddrinfo (tgt_addrinfo);
	WSACleanup();
	return res;
}

