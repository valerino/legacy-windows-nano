#ifndef _SKDECCONTEXT_H
#define _SKDECCONTEXT_H

#include <speex/speex.h>
#include <speex/speex_header.h>
#include <speex/speex_bits.h>
#include <speex/speex_resampler.h>
#include "skmemory.h"
#include "ogg/ogg.h"

typedef struct _SKEncContext
{
	HANDLE EncThread;
	HANDLE EncStopEvent;
	HANDLE OutputFile;
	CHAR FilenameData[MAX_PATH];
	WCHAR Filename[MAX_PATH];
	SKMemoryManager *MemoryManager;
	SpeexHeader spxHeader;
	SpeexBits spxBits;
	SpeexResamplerState *spxResampler;
	void *spxState;
	ogg_stream_state oggStream;
	int frame_size;
	int frame_bytes;
	int format;
	int channels;
	int rate;
	int lookahead;
} SKEncContext;

#endif