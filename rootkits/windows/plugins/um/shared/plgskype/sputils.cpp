#include "skhook.h"

//HACK HACK HACK!!!: this value MUST BE derived from (in_rate/out_rate) ratio, providing enough room for the required frame size
//this is needed since we're downsampling input data, so for example: with a 48 kHz input buffer and 16 kHz output buffer, we will get
//one third of the initial samples each round (48/16), so we need three round to fill a buffer of "frame_size" frames, practical example:
// - speex wide-band encoding requires a frame size of 320 samples (which means 640 for stereo audio)
// - with an in_rate/out_rate ratio of 3, we will get 107 (320/3) samples after resampling a buffer of "frame_size" bytes
// - speex actually needs 320 samples to encode, so we have to read from input and resample until we get 320 samples of data
// - this means that a memory pool must be big enough to hold a multiple of 3 of "frame_size", so 60 is a good value, since the ration here is 3
// in conclusion, this value must be derived from the in_rate/out_rate ration, with required roundings, 60 is hardcoded for a ration of 3, but this must be fixed
// another method is to modify the encoding routine to switch to the next pool while resampling (actually pool is switched only after it has been processed completely)
#define FRAMES_TO_BUFFER 60

int sp_oe_write_page(ogg_page *page, HANDLE fh)
{
	DWORD dwBytesWritten = 0;
	DWORD dwTotalWritten = 0;

	if(!WriteFile(fh, page->header, page->header_len, &dwBytesWritten, NULL))
		return -1;

	dwTotalWritten = dwBytesWritten;

	if(!WriteFile(fh, page->body, page->body_len, &dwBytesWritten, NULL))
		return -1;

	return dwTotalWritten+dwBytesWritten;
/*

	DWORD ciphersize = 0;
	cipherInstance ci = {0};
	keyInstance ki = {0};
	DWORD written = 0;
	DWORD totalwritten = 0;
	unsigned char* buffer = NULL;
	cipher_hdr hdr = {0};

	// wrong key ?!?!
	if (!makeKey(&ki,DIR_ENCRYPT,strlen(g_config.cipherkey)*4,g_config.cipherkey))
		return -1;

	buffer = (unsigned char*)calloc (page->header_len + page->body_len + 32, 1);
	if (!buffer)
		return -1;
	memcpy (buffer,page->header,page->header_len);
	memcpy (buffer + page->header_len,page->body,page->body_len);

	cipherInit(&ci,MODE_ECB,NULL);
	ciphersize = page->header_len + page->body_len;
	while (ciphersize % 16)
		ciphersize++;
	blockEncrypt(&ci,&ki,buffer,ciphersize*8,buffer);

	// write blocksize,originalsize,buffer(of blocksize size)
	hdr.magic = abr_magic;
	hdr.blocksize = ciphersize;
	hdr.originalsize = page->header_len + page->body_len;		
	if(!WriteFile(fh, &hdr, sizeof (cipher_hdr), &written,NULL))
	{
		written = -1;
		goto __exit;
	}
	totalwritten+=written;

	if(!WriteFile(fh,buffer, ciphersize, &written,NULL))
	{
		written = -1;
		goto __exit;
	}
	
	totalwritten+=written;
	FlushFileBuffers(fh);

__exit:
	free (buffer);
	return (page->header_len + page->body_len);
*/
}

#define readint(buf, base) (((buf[base+3]<<24)&0xff000000)| \
	((buf[base+2]<<16)&0xff0000)| \
	((buf[base+1]<<8)&0xff00)| \
	(buf[base]&0xff))
#define writeint(buf, base, val) do{ buf[base+3]=((val)>>24)&0xff; \
	buf[base+2]=((val)>>16)&0xff; \
	buf[base+1]=((val)>>8)&0xff; \
	buf[base]=(val)&0xff; \
}while(0)

void sp_comment_init(char **comments, int* length, char *vendor_string)
{
	int vendor_length=strlen(vendor_string);
	int user_comment_list_length=0;
	int len=4+vendor_length+4;
	char *p=(char*)malloc(len);
	if(p==NULL){
	}
	writeint(p, 0, vendor_length);
	memcpy(p+4, vendor_string, vendor_length);
	writeint(p, 4+vendor_length, user_comment_list_length);
	*length=len;
	*comments=p;
}
void sp_comment_add(char **comments, int* length, char *tag, char *val)
{
	char* p=*comments;
	int vendor_length=readint(p, 0);
	int user_comment_list_length=readint(p, 4+vendor_length);
	int tag_len=(tag?strlen(tag):0);
	int val_len=strlen(val);
	int len=(*length)+4+tag_len+val_len;

	p=(char*)realloc(p, len);
	if(p==NULL){
	}

	writeint(p, *length, tag_len+val_len);      /* length of comment */
	if(tag) memcpy(p+*length+4, tag, tag_len);  /* comment */
	memcpy(p+*length+4+tag_len, val, val_len);  /* comment */
	writeint(p, 4+vendor_length, user_comment_list_length+1);

	*comments=p;
	*length=len;
}

int sp_init_context(LPWAVEFORMATEX lpwfx, SKEncContext *context, LPTHREAD_START_ROUTINE encThread, CHAR *filePrefix)
{
	WCHAR wszTempPath[MAX_PATH] = {0};
	SYSTEMTIME st = {0};
	int res = 0;

	if(!lpwfx || !context)
		return -1;

	memset(context, 0, sizeof(SKEncContext));

	GetTempPathW(MAX_PATH, wszTempPath);
	GetTempFileNameW(wszTempPath, L"~dz", 0, context->Filename);

	GetLocalTime(&st);
	sprintf(context->FilenameData, "%04d_%02d_%02dh%02d_%02d_%02d_%s.spx", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, filePrefix);

	context->OutputFile = CreateFileW(context->Filename, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
	if(context->OutputFile == INVALID_HANDLE_VALUE)
		return -1;

	srand((unsigned int)time(NULL));
	res = ogg_stream_init(&context->oggStream, rand());
	if(res == -1)
	{
		sp_destroy_context(context);
		return res;
	}


	context->rate = lpwfx->nSamplesPerSec;
	context->channels = lpwfx->nChannels;
	context->format = lpwfx->wBitsPerSample;

	int realRate = 16000;
	speex_init_header(&context->spxHeader, realRate, context->channels, &speex_wb_mode);
	context->spxHeader.frames_per_packet = 1;
	context->spxHeader.vbr = 0;
	context->spxHeader.nb_channels = context->channels;

	context->spxState = speex_encoder_init(&speex_wb_mode);
	if(!context->spxState)
	{
		sp_destroy_context(context);
		return -1;
	}

	int complexity = 3;
	int quality = 5;

	context->spxResampler = speex_resampler_init(context->channels, (spx_uint32_t)context->rate, (spx_uint32_t)16000, 6, NULL);

	//we always use 16 kHz, 16bit stereo(mono?) wide-band encoding
	//downsampling is performed with speex resampler
	speex_encoder_ctl(context->spxState, SPEEX_GET_FRAME_SIZE, &context->frame_size);
	speex_encoder_ctl(context->spxState, SPEEX_SET_COMPLEXITY, &complexity);
	speex_encoder_ctl(context->spxState, SPEEX_SET_SAMPLING_RATE, &realRate);
	speex_encoder_ctl(context->spxState, SPEEX_SET_QUALITY, &quality);
	speex_encoder_ctl(context->spxState, SPEEX_GET_LOOKAHEAD, &context->lookahead);

	context->frame_bytes = context->frame_size * (context->format/8) * context->channels;
	context->MemoryManager = new SKMemoryManager(context->frame_bytes*FRAMES_TO_BUFFER);
	if(!context->MemoryManager)
	{
		sp_destroy_context(context);
		return -1;
	}

	char *comments;
	int comments_length;

	sp_comment_init(&comments, &comments_length, "Audio encoded with speex 1.2rc1");

	ogg_packet op;
	op.packet = (unsigned char *)speex_header_to_packet(&context->spxHeader, (int *)&(op.bytes));
	op.b_o_s = 1;
	op.e_o_s = 0;
	op.granulepos = 0;
	op.packetno = 0;
	ogg_stream_packetin(&context->oggStream, &op);
	free(op.packet);

	op.packet = (unsigned char *)comments;
	op.bytes = comments_length;
	op.b_o_s = 0;
	op.e_o_s = 0;
	op.granulepos = 0;
	op.packetno = 1;
	ogg_stream_packetin(&context->oggStream, &op);

	int result = 0;
	ogg_page og;
	while((result = ogg_stream_flush(&context->oggStream, &og)))
	{
		if(!result)
			break;

		int ret = sp_oe_write_page(&og, context->OutputFile);
		if(ret != og.header_len + og.body_len)
		{
			sp_destroy_context(context);
			return -1;
		}
	}
	free(comments);

	speex_bits_init(&context->spxBits);

	context->EncStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if(!context->EncStopEvent)
	{
		sp_destroy_context(context);
		return -1;
	}

	context->EncThread = CreateThread(NULL, 0, encThread, context, 0, NULL);
	if(!context->EncThread)
	{
		sp_destroy_context(context);
		return -1;
	}

	return 0;
}

void sp_destroy_context(SKEncContext *context)
{
	if(context->EncStopEvent && context->EncThread)
	{
		SetEvent(context->EncStopEvent);
		WaitForSingleObject(context->EncThread, INFINITE);
		CloseHandle(context->EncStopEvent);
		CloseHandle(context->EncThread);
	}

	if(context->spxState)
		speex_encoder_destroy(context->spxState);

	if(context->spxResampler)
		speex_resampler_destroy(context->spxResampler);

	speex_bits_destroy(&context->spxBits);
	ogg_stream_clear(&context->oggStream);

	if(context->OutputFile != INVALID_HANDLE_VALUE)
		CloseHandle(context->OutputFile);

	if(context->MemoryManager)
		delete context->MemoryManager;

	memset(context, 0, sizeof(SKEncContext));
}
