#ifndef _MYDIRECTSOUNDCAPTUREBUFFER_H
#define _MYDIRECTSOUNDCAPTUREBUFFER_H

#include "skenccontext.h"

#include <MMSystem.h>
#include <dsound.h>

DECLARE_INTERFACE_(IMyDirectSoundCaptureBuffer, IDirectSoundCaptureBuffer)
{
	// IUnknown methods
	STDMETHOD(QueryInterface)       (IMyDirectSoundCaptureBuffer *This, REFIID, LPVOID *) PURE;
	STDMETHOD_(ULONG,AddRef)        (IMyDirectSoundCaptureBuffer *This) PURE;
	STDMETHOD_(ULONG,Release)       (IMyDirectSoundCaptureBuffer *This) PURE;

	// IDirectSoundCaptureBuffer methods
	STDMETHOD(GetCaps)              (IMyDirectSoundCaptureBuffer *This, LPDSCBCAPS pDSCBCaps) PURE;
	STDMETHOD(GetCurrentPosition)   (IMyDirectSoundCaptureBuffer *This, LPDWORD pdwCapturePosition, LPDWORD pdwReadPosition) PURE;
	STDMETHOD(GetFormat)            (IMyDirectSoundCaptureBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten) PURE;
	STDMETHOD(GetStatus)            (IMyDirectSoundCaptureBuffer *This, LPDWORD pdwStatus) PURE;
	STDMETHOD(Initialize)           (IMyDirectSoundCaptureBuffer *This, LPDIRECTSOUNDCAPTURE pDirectSoundCapture, LPCDSCBUFFERDESC pcDSCBufferDesc) PURE;
	STDMETHOD(Lock)                 (IMyDirectSoundCaptureBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
		LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags) PURE;
	STDMETHOD(Start)                (IMyDirectSoundCaptureBuffer *This, DWORD dwFlags) PURE;
	STDMETHOD(Stop)                 (IMyDirectSoundCaptureBuffer *This) PURE;
	STDMETHOD(Unlock)               (IMyDirectSoundCaptureBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2) PURE;

	LPDIRECTSOUNDCAPTUREBUFFER lpDSCB;
	LPVOID pLockAddress;
	DWORD dwBufferSize;
	DWORD dwNewReadPosition;
	DWORD dwOldReadPosition;

	SKEncContext *pContext;
};

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_QueryInterface(IMyDirectSoundCaptureBuffer *This, REFIID, LPVOID *);
ULONG STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_AddRef(IMyDirectSoundCaptureBuffer *This);
ULONG STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Release(IMyDirectSoundCaptureBuffer *This);

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetCaps(IMyDirectSoundCaptureBuffer *This, LPDSCBCAPS pDSCBCaps);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetCurrentPosition(IMyDirectSoundCaptureBuffer *This, LPDWORD pdwCapturePosition, LPDWORD pdwReadPosition);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetFormat(IMyDirectSoundCaptureBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetStatus(IMyDirectSoundCaptureBuffer *This, LPDWORD pdwStatus);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Initialize(IMyDirectSoundCaptureBuffer *This, LPDIRECTSOUNDCAPTURE pDirectSoundCapture, LPCDSCBUFFERDESC pcDSCBufferDesc);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Lock(IMyDirectSoundCaptureBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
															LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Start(IMyDirectSoundCaptureBuffer *This, DWORD dwFlags);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Stop(IMyDirectSoundCaptureBuffer *This);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Unlock(IMyDirectSoundCaptureBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2);

#endif