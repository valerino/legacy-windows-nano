#ifndef _SPUTILS_H
#define _SPUTILS_H

#include <stdio.h>
#include "skenccontext.h"
#include "ogg/ogg.h"

int sp_oe_write_page(ogg_page *page, HANDLE fh);
void sp_comment_init(char **comments, int* length, char *vendor_string);
void sp_comment_add(char **comments, int* length, char *tag, char *val);

int sp_init_context(LPWAVEFORMATEX lpwfx, SKEncContext *context, LPTHREAD_START_ROUTINE encThread, CHAR *filePrefix);
void sp_destroy_context(SKEncContext *context);

#endif