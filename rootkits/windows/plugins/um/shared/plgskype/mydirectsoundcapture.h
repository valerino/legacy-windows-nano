#ifndef _MYDIRECTSOUNDCAPTURE_H
#define _MYDIRECTSOUNDCAPTURE_H

#include <MMSystem.h>
#include <dsound.h>
DECLARE_INTERFACE_(IMyDirectSoundCapture, IDirectSoundCapture)
{
	// IUnknown methods
	STDMETHOD(QueryInterface)       (IMyDirectSoundCapture *This, REFIID, LPVOID *) PURE;
	STDMETHOD_(ULONG,AddRef)        (IMyDirectSoundCapture *This) PURE;
	STDMETHOD_(ULONG,Release)       (IMyDirectSoundCapture *This) PURE;

	// IDirectSoundCapture methods
	STDMETHOD(CreateCaptureBuffer)  (IMyDirectSoundCapture *This, LPCDSCBUFFERDESC pcDSCBufferDesc, LPDIRECTSOUNDCAPTUREBUFFER *ppDSCBuffer, LPUNKNOWN pUnkOuter) PURE;
	STDMETHOD(GetCaps)              (IMyDirectSoundCapture *This, LPDSCCAPS pDSCCaps) PURE;
	STDMETHOD(Initialize)           (IMyDirectSoundCapture *This, LPCGUID pcGuidDevice) PURE;

	LPDIRECTSOUNDCAPTURE lpDSC;
};

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_QueryInterface(IMyDirectSoundCapture *This, REFIID, LPVOID *);
ULONG STDMETHODCALLTYPE my_IDirectSoundCapture_AddRef(IMyDirectSoundCapture *This);
ULONG STDMETHODCALLTYPE my_IDirectSoundCapture_Release(IMyDirectSoundCapture *This);

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_CreateCaptureBuffer(IMyDirectSoundCapture *This, LPCDSCBUFFERDESC pcDSCBufferDesc, LPDIRECTSOUNDCAPTUREBUFFER *ppDSCBuffer, LPUNKNOWN pUnkOuter);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_GetCaps(IMyDirectSoundCapture *This, LPDSCCAPS pDSCCaps);
HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_Initialize(IMyDirectSoundCapture *This, LPCGUID pcGuidDevice);

#endif