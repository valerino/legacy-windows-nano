#include "skhook.h"
#include <dsound.h>
#pragma warning(disable:4313 4995)
#define MAX_FRAME_SIZE 2000
#define MAX_FRAME_BYTES 2000

DWORD WINAPI EncodeOutputThread(LPVOID lpParam)
{
	SKEncContext *pContext = (SKEncContext *)lpParam;
	short input[MAX_FRAME_SIZE] = {0};
	short output[MAX_FRAME_SIZE] = {0};
	char cbits[MAX_FRAME_BYTES] = {0};
	int eos = 0;
	ogg_packet oggPacket;
	ogg_page oggPage;
	int total_samples = 0;
	int bytes_written = 0;
	int id = 0;
	DWORD dwWaitRes;
	BOOL bContinue = TRUE;
	SKMemoryPool *pPool = NULL;
	BYTE *pPoolData = NULL;
	DWORD dwPoolSize = 0;
	DWORD dwSizeToGet = 0;
	DWORD dwCurSize = 0;
	SHORT *pShortData = NULL;

	while(bContinue)
	{
		dwWaitRes = WaitForSingleObject(pContext->EncStopEvent, 15);
		if(dwWaitRes != WAIT_TIMEOUT)
			bContinue = FALSE;

		DBG_OUT(("encodeinputthread buffer received\n"));

		while((pPool = pContext->MemoryManager->Pop()) != NULL)
		{
			dwPoolSize = pPool->GetSize();
			if(dwPoolSize == 0)
				continue;

			pPoolData = pPool->GetData();
			dwCurSize = 0;
			do
			{
				ULONG samples_processed = 0;
				ULONG outLen;
				DWORD dwNumSamples = 0;

				do 
				{
					dwSizeToGet = dwPoolSize > pContext->frame_bytes ? pContext->frame_bytes : dwPoolSize;
					dwNumSamples = dwSizeToGet / ((pContext->format / 8)*pContext->channels);

					outLen = MAX_FRAME_SIZE;
					pShortData = (short *)(pPoolData+dwCurSize);
					memset(input, 0, sizeof(input));
					if(pContext->format == 8)
					{
						BYTE *pByteData = (BYTE *)pShortData;
						for(int i = 0; i < dwNumSamples*pContext->channels; i++)
							input[i] = (short)(pByteData[i] << 8) ^ 0x8000;
					}
					else
						memcpy(input, pShortData, dwSizeToGet);

					speex_resampler_process_interleaved_int(pContext->spxResampler, input, (spx_uint32_t *)&dwNumSamples, 
						&output[samples_processed*pContext->channels], (spx_uint32_t *)&outLen);

					total_samples += dwNumSamples;
					samples_processed += outLen;

					dwCurSize += dwSizeToGet;
					dwPoolSize -= dwSizeToGet;
				} while(samples_processed < pContext->frame_size && dwPoolSize);

				if(pContext->channels == 2)
					speex_encode_stereo_int(output, pContext->frame_size, &pContext->spxBits);

				speex_encode_int(pContext->spxState, output, &pContext->spxBits);

				speex_bits_insert_terminator(&pContext->spxBits);
				int nBytes = speex_bits_write(&pContext->spxBits, cbits, MAX_FRAME_BYTES);
				speex_bits_reset(&pContext->spxBits);

				if((dwPoolSize == 0) && pContext->MemoryManager->IsEmpty() && !bContinue)
					oggPacket.e_o_s = 1;
				else
					oggPacket.e_o_s = 0;

				oggPacket.packet = (unsigned char *)cbits;
				oggPacket.bytes = nBytes;
				oggPacket.b_o_s = 0;
				oggPacket.granulepos = (id+1)*pContext->frame_size - pContext->lookahead;
				if(oggPacket.granulepos > total_samples)
					oggPacket.granulepos = total_samples;

				oggPacket.packetno = 2+id;
				ogg_stream_packetin(&pContext->oggStream, &oggPacket);

				while(ogg_stream_pageout(&pContext->oggStream, &oggPage))
				{
					int ret = sp_oe_write_page(&oggPage, pContext->OutputFile);
					bytes_written += ret;
				}

				id++;
			} while (dwPoolSize);

			delete pPool;
		}
	}

	while(ogg_stream_flush(&pContext->oggStream, &oggPage))
	{
		int ret = sp_oe_write_page(&oggPage, pContext->OutputFile);
		bytes_written += ret;
	}

	return 0;
}

// this is the code for our implementation of IDirectSound8
HRESULT STDMETHODCALLTYPE my_IDirectSound_QueryInterface(IMyDirectSound8 *This,  REFIID id, LPVOID *param2)
{
	DBG_OUT(("**IDirectSound::QueryInterface\n"));
	return This->lpVtbl->pDS8->lpVtbl->QueryInterface(This->lpVtbl->pDS8, id, param2);
}

ULONG STDMETHODCALLTYPE my_IDirectSound_AddRef(IMyDirectSound8 *This)
{
	DBG_OUT(("**IDirectSound::AddRef\n"));
	return This->lpVtbl->pDS8->lpVtbl->AddRef(This->lpVtbl->pDS8);
}

ULONG STDMETHODCALLTYPE my_IDirectSound_Release(IMyDirectSound8 *This)
{
	ULONG refCount;
	DBG_OUT(("**IDirectSound::Release\n"));

	// call original Release method
	refCount = This->lpVtbl->pDS8->lpVtbl->Release(This->lpVtbl->pDS8);
	if(refCount == 0)
	{
		// if refCount is 0, then free our stuff
		free(This->lpVtbl);
		free(This);
	}

	return refCount;
}

/**
 * The CreateSoundBuffer function is used to create a sound buffer
 * sound buffer can be primary or secondary buffers.
 * We don't care for primary buffer, since they are only used to mix audio by DX, actually they do not receive
 * audio data, which goes inside secondary buffers.
 */
HRESULT STDMETHODCALLTYPE my_IDirectSound_CreateSoundBuffer(IMyDirectSound8 *This,  LPCDSBUFFERDESC pcDSBufferDesc, LPDIRECTSOUNDBUFFER *ppDSBuffer, LPUNKNOWN pUnkOuter)
{
	HRESULT hRes;
	DBG_OUT(("**IDirectSound::CreateSoundBuffer\n"));

	if(pcDSBufferDesc->lpwfxFormat == NULL)
		return This->lpVtbl->pDS8->lpVtbl->CreateSoundBuffer(This->lpVtbl->pDS8, pcDSBufferDesc, ppDSBuffer, pUnkOuter);

	// call original method
	hRes = This->lpVtbl->pDS8->lpVtbl->CreateSoundBuffer(This->lpVtbl->pDS8, pcDSBufferDesc, ppDSBuffer, pUnkOuter);
	if(!FAILED(hRes))
	{
		IMyDirectSoundBuffer *pMyDirectSoundBuffer = (IMyDirectSoundBuffer *)malloc(sizeof(IMyDirectSoundBuffer));
		if(!pMyDirectSoundBuffer)
			return hRes;

		pMyDirectSoundBuffer->lpVtbl = (IMyDirectSoundBufferVtbl *)malloc(sizeof(IMyDirectSoundBufferVtbl));
		if(!pMyDirectSoundBuffer->lpVtbl)
		{
			free(pMyDirectSoundBuffer);
			return hRes;
		}

		// store original IDirectSoundBuffer8 instance pointer
		pMyDirectSoundBuffer->lpVtbl->pDSBuffer = *ppDSBuffer;
		DBG_OUT(("***Creating buffer %08x of %d bytes\n", *ppDSBuffer, pcDSBufferDesc->dwBufferBytes));

		// log if thi sis a primary buffer
		DBG_OUT(("***Buffer properties:\n"));

		DBG_OUT(("***Sample rate: %d\n", pcDSBufferDesc->lpwfxFormat->nSamplesPerSec));
		DBG_OUT(("***Bits per sample: %d\n", pcDSBufferDesc->lpwfxFormat->wBitsPerSample));
		DBG_OUT(("***Channels: %d\n", pcDSBufferDesc->lpwfxFormat->nChannels));
		DBG_OUT(("**Format: %x\n", pcDSBufferDesc->lpwfxFormat->wFormatTag));
		DBG_OUT(("**cbSize: %x\n", pcDSBufferDesc->lpwfxFormat->cbSize));

#ifdef _DEBUG
		DBG_OUT(("**FLAGS**\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_CTRL3D)
			DBG_OUT(("***DSBCAPS_CTRL3D***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_CTRLFREQUENCY)
			DBG_OUT(("***DSBCAPS_CTRLFREQUENCY***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_CTRLPOSITIONNOTIFY)
			DBG_OUT(("***DSBCAPS_CTRLPOSITIONNOTIFY***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_GETCURRENTPOSITION2)
			DBG_OUT(("***DSBCAPS_GETCURRENTPOSITION2***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_LOCDEFER)
			DBG_OUT(("***DSBCAPS_LOCDEFER***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_LOCHARDWARE)
			DBG_OUT(("***DSBCAPS_LOCHARDWARE***\n"));
		if(pcDSBufferDesc->dwFlags & DSBCAPS_LOCSOFTWARE)
			DBG_OUT(("***DSBCAPS_LOCSOFTWARE***\n"));
#endif

		pMyDirectSoundBuffer->lpVtbl->pContext = (SKEncContext *)malloc(sizeof(SKEncContext));
		SKEncContext *pContext = pMyDirectSoundBuffer->lpVtbl->pContext;
		if(!pContext)
		{
			free(pMyDirectSoundBuffer->lpVtbl);
			free(pMyDirectSoundBuffer);
			return hRes;
		}

		// if we can't safely initialize speex encoder, then just fail without logging anything
		int res = sp_init_context(pcDSBufferDesc->lpwfxFormat, pContext, EncodeOutputThread, "skype_out");
		if(res == -1)
		{
			free(pContext);
			free(pMyDirectSoundBuffer->lpVtbl);
			free(pMyDirectSoundBuffer);
			return hRes;
		}

		pMyDirectSoundBuffer->lpVtbl->dwOldPlayCursor = 0;
		pMyDirectSoundBuffer->lpVtbl->dwCurrentSize = 0;

		// dwBufferBytes is the size of the buffer
		pMyDirectSoundBuffer->lpVtbl->dwBufferSize = pcDSBufferDesc->dwBufferBytes;

		pMyDirectSoundBuffer->lpVtbl->QueryInterface = my_IDirectSoundBuffer_QueryInterface;
		pMyDirectSoundBuffer->lpVtbl->AddRef = my_IDirectSoundBuffer_AddRef;
		pMyDirectSoundBuffer->lpVtbl->Release = my_IDirectSoundBuffer_Release;

		pMyDirectSoundBuffer->lpVtbl->GetCaps = my_IDirectSoundBuffer_GetCaps;
		pMyDirectSoundBuffer->lpVtbl->GetCurrentPosition = my_IDirectSoundBuffer_GetCurrentPosition;
		pMyDirectSoundBuffer->lpVtbl->GetFormat = my_IDirectSoundBuffer_GetFormat;
		pMyDirectSoundBuffer->lpVtbl->GetVolume = my_IDirectSoundBuffer_GetVolume;
		pMyDirectSoundBuffer->lpVtbl->GetPan = my_IDirectSoundBuffer_GetPan;
		pMyDirectSoundBuffer->lpVtbl->GetFrequency = my_IDirectSoundBuffer_GetFrequency;
		pMyDirectSoundBuffer->lpVtbl->GetStatus = my_IDirectSoundBuffer_GetStatus;
		pMyDirectSoundBuffer->lpVtbl->Initialize = my_IDirectSoundBuffer_Initialize;
		pMyDirectSoundBuffer->lpVtbl->Lock = my_IDirectSoundBuffer_Lock;
		pMyDirectSoundBuffer->lpVtbl->Play = my_IDirectSoundBuffer_Play;
		pMyDirectSoundBuffer->lpVtbl->SetCurrentPosition = my_IDirectSoundBuffer_SetCurrentPosition;
		pMyDirectSoundBuffer->lpVtbl->SetFormat = my_IDirectSoundBuffer_SetFormat;
		pMyDirectSoundBuffer->lpVtbl->SetVolume = my_IDirectSoundBuffer_SetVolume;
		pMyDirectSoundBuffer->lpVtbl->SetPan = my_IDirectSoundBuffer_SetPan;
		pMyDirectSoundBuffer->lpVtbl->SetFrequency = my_IDirectSoundBuffer_SetFrequency;
		pMyDirectSoundBuffer->lpVtbl->Stop = my_IDirectSoundBuffer_Stop;
		pMyDirectSoundBuffer->lpVtbl->Unlock = my_IDirectSoundBuffer_Unlock;
		pMyDirectSoundBuffer->lpVtbl->Restore = my_IDirectSoundBuffer_Restore;

		// return our interface
		*ppDSBuffer = (LPDIRECTSOUNDBUFFER)pMyDirectSoundBuffer;
	}
	return hRes;
}


// we don't care for other methods, just log them on debug build
HRESULT STDMETHODCALLTYPE my_IDirectSound_GetCaps(IMyDirectSound8 *This,  LPDSCAPS pDSCaps)
{
	DBG_OUT(("**IDirectSound::GetCaps\n"));
	return This->lpVtbl->pDS8->lpVtbl->GetCaps(This->lpVtbl->pDS8, pDSCaps);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_DuplicateSoundBuffer(IMyDirectSound8 *This,  LPDIRECTSOUNDBUFFER pDSBufferOriginal, LPDIRECTSOUNDBUFFER *ppDSBufferDuplicate)
{
	DBG_OUT(("**IDirectSound::DuplicateSoundBuffer\n"));
	return This->lpVtbl->pDS8->lpVtbl->DuplicateSoundBuffer(This->lpVtbl->pDS8, pDSBufferOriginal, ppDSBufferDuplicate);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_SetCooperativeLevel(IMyDirectSound8 *This,  HWND hwnd, DWORD dwLevel)
{
	DBG_OUT(("**IDirectSound::SetCooperativeLevel\n"));
	return This->lpVtbl->pDS8->lpVtbl->SetCooperativeLevel(This->lpVtbl->pDS8, hwnd, dwLevel);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_Compact(IMyDirectSound8 *This)
{
	DBG_OUT(("**IDirectSound::Compact\n"));
	return This->lpVtbl->pDS8->lpVtbl->Compact(This->lpVtbl->pDS8);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_GetSpeakerConfig(IMyDirectSound8 *This,  LPDWORD pdwSpeakerConfig)
{
	DBG_OUT(("**IDirectSound::GetSpeakerConfig\n"));
	return This->lpVtbl->pDS8->lpVtbl->GetSpeakerConfig(This->lpVtbl->pDS8, pdwSpeakerConfig);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_SetSpeakerConfig(IMyDirectSound8 *This,  DWORD dwSpeakerConfig)
{
	DBG_OUT(("**IDirectSound::SetSpeakerConfig\n"));
	return This->lpVtbl->pDS8->lpVtbl->SetSpeakerConfig(This->lpVtbl->pDS8, dwSpeakerConfig);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_Initialize(IMyDirectSound8 *This,  LPCGUID pcGuidDevice)
{
	DBG_OUT(("**IDirectSound::Initialize\n"));
	return This->lpVtbl->pDS8->lpVtbl->Initialize(This->lpVtbl->pDS8, pcGuidDevice);
}

HRESULT STDMETHODCALLTYPE my_IDirectSound_VerifyCertification(IMyDirectSound8 *This,  LPDWORD pdwCertified)
{
	DBG_OUT(("**IDirectSound::VerifyCertification\n"));
	return This->lpVtbl->pDS8->lpVtbl->VerifyCertification(This->lpVtbl->pDS8, pdwCertified);
}
#pragma warning(default:4313)