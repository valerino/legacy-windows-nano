#ifndef _MYDIRECTSOUND_H
#define _MYDIRECTSOUND_H

#include <dsound.h>

/**
 * The various COM macros are just wrappers which generate C or C++ code, by using CINTERFACE we're forcing them
 * to generate C code.
 *
 * DECLARE_INTERFACE is used to declare a COM interface, it takes two argument, the first is the interface name, the second
 * the base interface.
 *
 * STDMETHOD (STDMETHOD_ is the same, but you can specify the return value, default is HRESULT) is used to declare an interface method, 
 * in C they are declared as function pointer inside the lpVtbl structure; in C++ they are declared as pure virtual function, 
 * so for example, the AddRef method, in C will be something like this:
 * ULONG (*AddRef)(IMyDirectSound8 *This);
 *
 * in C++:
 * virtual ULONG AddRef() = 0;
 *
 * the = 0 suffix is added by the PURE macro, which does nothing in C
 */

// this is our implementation of IDirectSound8
DECLARE_INTERFACE_(IMyDirectSound8, IDirectSound8)
{
	// here methods must be IN THE SAME order they are inside the original interface
	// IUnknown methods
	STDMETHOD(QueryInterface)       (IMyDirectSound8 *This,  REFIID, LPVOID *) PURE;
	STDMETHOD_(ULONG,AddRef)        (IMyDirectSound8 *This) PURE;
	STDMETHOD_(ULONG,Release)       (IMyDirectSound8 *This) PURE;

	// IDirectSound methods
	STDMETHOD(CreateSoundBuffer)    (IMyDirectSound8 *This,  LPCDSBUFFERDESC pcDSBufferDesc, LPDIRECTSOUNDBUFFER *ppDSBuffer, LPUNKNOWN pUnkOuter) PURE;
	STDMETHOD(GetCaps)              (IMyDirectSound8 *This,  LPDSCAPS pDSCaps) PURE;
	STDMETHOD(DuplicateSoundBuffer) (IMyDirectSound8 *This,  LPDIRECTSOUNDBUFFER pDSBufferOriginal, LPDIRECTSOUNDBUFFER *ppDSBufferDuplicate) PURE;
	STDMETHOD(SetCooperativeLevel)  (IMyDirectSound8 *This,  HWND hwnd, DWORD dwLevel) PURE;
	STDMETHOD(Compact)              (IMyDirectSound8 *This) PURE;
	STDMETHOD(GetSpeakerConfig)     (IMyDirectSound8 *This,  LPDWORD pdwSpeakerConfig) PURE;
	STDMETHOD(SetSpeakerConfig)     (IMyDirectSound8 *This,  DWORD dwSpeakerConfig) PURE;
	STDMETHOD(Initialize)           (IMyDirectSound8 *This,  LPCGUID pcGuidDevice) PURE;

	// IDirectSound8 methods
	STDMETHOD(VerifyCertification)  (IMyDirectSound8 *This,  LPDWORD pdwCertified) PURE;

	// from here we can add our methods, variables, etc...
	LPDIRECTSOUND8 pDS8;
};

HRESULT STDMETHODCALLTYPE my_IDirectSound_QueryInterface(IMyDirectSound8 *This,  REFIID, LPVOID *);
ULONG STDMETHODCALLTYPE my_IDirectSound_AddRef(IMyDirectSound8 *This);
ULONG STDMETHODCALLTYPE my_IDirectSound_Release(IMyDirectSound8 *This);
HRESULT STDMETHODCALLTYPE my_IDirectSound_CreateSoundBuffer(IMyDirectSound8 *This,  LPCDSBUFFERDESC pcDSBufferDesc, LPDIRECTSOUNDBUFFER *ppDSBuffer, LPUNKNOWN pUnkOuter);
HRESULT STDMETHODCALLTYPE my_IDirectSound_GetCaps(IMyDirectSound8 *This,  LPDSCAPS pDSCaps);
HRESULT STDMETHODCALLTYPE my_IDirectSound_DuplicateSoundBuffer(IMyDirectSound8 *This,  LPDIRECTSOUNDBUFFER pDSBufferOriginal, LPDIRECTSOUNDBUFFER *ppDSBufferDuplicate);
HRESULT STDMETHODCALLTYPE my_IDirectSound_SetCooperativeLevel(IMyDirectSound8 *This,  HWND hwnd, DWORD dwLevel);
HRESULT STDMETHODCALLTYPE my_IDirectSound_Compact(IMyDirectSound8 *This);
HRESULT STDMETHODCALLTYPE my_IDirectSound_GetSpeakerConfig(IMyDirectSound8 *This,  LPDWORD pdwSpeakerConfig);
HRESULT STDMETHODCALLTYPE my_IDirectSound_SetSpeakerConfig(IMyDirectSound8 *This,  DWORD dwSpeakerConfig);
HRESULT STDMETHODCALLTYPE my_IDirectSound_Initialize(IMyDirectSound8 *This,  LPCGUID pcGuidDevice);
HRESULT STDMETHODCALLTYPE my_IDirectSound_VerifyCertification(IMyDirectSound8 *This,  LPDWORD pdwCertified);

#endif