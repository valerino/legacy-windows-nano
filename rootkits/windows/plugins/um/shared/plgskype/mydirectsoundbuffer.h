#ifndef _MYDIRECTSOUNDBUFFER_H
#define _MYDIRECTSOUNDBUFFER_H

// this is our IDirectSoundBuffer8 implementation
DECLARE_INTERFACE_(IMyDirectSoundBuffer, IDirectSoundBuffer)
{
	// IUnknown methods
	STDMETHOD(QueryInterface)       (IMyDirectSoundBuffer *This, REFIID, LPVOID *) PURE;
	STDMETHOD_(ULONG,AddRef)        (IMyDirectSoundBuffer *This) PURE;
	STDMETHOD_(ULONG,Release)       (IMyDirectSoundBuffer *This) PURE;

	// IDirectSoundBuffer methods
	STDMETHOD(GetCaps)              (IMyDirectSoundBuffer *This, LPDSBCAPS pDSBufferCaps) PURE;
	STDMETHOD(GetCurrentPosition)   (IMyDirectSoundBuffer *This, LPDWORD pdwCurrentPlayCursor, LPDWORD pdwCurrentWriteCursor) PURE;
	STDMETHOD(GetFormat)            (IMyDirectSoundBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten) PURE;
	STDMETHOD(GetVolume)            (IMyDirectSoundBuffer *This, LPLONG plVolume) PURE;
	STDMETHOD(GetPan)               (IMyDirectSoundBuffer *This, LPLONG plPan) PURE;
	STDMETHOD(GetFrequency)         (IMyDirectSoundBuffer *This, LPDWORD pdwFrequency) PURE;
	STDMETHOD(GetStatus)            (IMyDirectSoundBuffer *This, LPDWORD pdwStatus) PURE;
	STDMETHOD(Initialize)           (IMyDirectSoundBuffer *This, LPDIRECTSOUND pDirectSound, LPCDSBUFFERDESC pcDSBufferDesc) PURE;
	STDMETHOD(Lock)                 (IMyDirectSoundBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
		LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags) PURE;
	STDMETHOD(Play)                 (IMyDirectSoundBuffer *This, DWORD dwReserved1, DWORD dwPriority, DWORD dwFlags) PURE;
	STDMETHOD(SetCurrentPosition)   (IMyDirectSoundBuffer *This, DWORD dwNewPosition) PURE;
	STDMETHOD(SetFormat)            (IMyDirectSoundBuffer *This, LPCWAVEFORMATEX pcfxFormat) PURE;
	STDMETHOD(SetVolume)            (IMyDirectSoundBuffer *This, LONG lVolume) PURE;
	STDMETHOD(SetPan)               (IMyDirectSoundBuffer *This, LONG lPan) PURE;
	STDMETHOD(SetFrequency)         (IMyDirectSoundBuffer *This, DWORD dwFrequency) PURE;
	STDMETHOD(Stop)                 (IMyDirectSoundBuffer *This) PURE;
	STDMETHOD(Unlock)               (IMyDirectSoundBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2) PURE;
	STDMETHOD(Restore)              (IMyDirectSoundBuffer *This) PURE;

	// original buffer
	LPDIRECTSOUNDBUFFER pDSBuffer;

	// actual address of the buffer in memory
	LPVOID pLockAddress;

	DWORD dwCurrentSize;
	DWORD dwBufferSize;
	DWORD dwChunkSize;

	// write positions returned by GetCurrentPosition, used to handle SKYPE way of buffering
	DWORD dwOldPlayCursor;

	SKEncContext *pContext;
};

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_QueryInterface(IMyDirectSoundBuffer *This, REFIID, LPVOID *);
ULONG STDMETHODCALLTYPE my_IDirectSoundBuffer_AddRef(IMyDirectSoundBuffer *This);
ULONG STDMETHODCALLTYPE my_IDirectSoundBuffer_Release(IMyDirectSoundBuffer *This);

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetCaps(IMyDirectSoundBuffer *This, LPDSBCAPS pDSBufferCaps);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetCurrentPosition(IMyDirectSoundBuffer *This, LPDWORD pdwCurrentPlayCursor, LPDWORD pdwCurrentWriteCursor);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetFormat(IMyDirectSoundBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetVolume(IMyDirectSoundBuffer *This, LPLONG plVolume);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetPan(IMyDirectSoundBuffer *This, LPLONG plPan);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetFrequency(IMyDirectSoundBuffer *This, LPDWORD pdwFrequency);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetStatus(IMyDirectSoundBuffer *This, LPDWORD pdwStatus);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Initialize(IMyDirectSoundBuffer *This, LPDIRECTSOUND pDirectSound, LPCDSBUFFERDESC pcDSBufferDesc);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Lock(IMyDirectSoundBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
													 LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Play(IMyDirectSoundBuffer *This, DWORD dwReserved1, DWORD dwPriority, DWORD dwFlags);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetCurrentPosition(IMyDirectSoundBuffer *This, DWORD dwNewPosition);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetFormat(IMyDirectSoundBuffer *This, LPCWAVEFORMATEX pcfxFormat);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetVolume(IMyDirectSoundBuffer *This, LONG lVolume);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetPan(IMyDirectSoundBuffer *This, LONG lPan);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetFrequency(IMyDirectSoundBuffer *This, DWORD dwFrequency);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Stop(IMyDirectSoundBuffer *This);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Unlock(IMyDirectSoundBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2);
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Restore(IMyDirectSoundBuffer *This);

#endif