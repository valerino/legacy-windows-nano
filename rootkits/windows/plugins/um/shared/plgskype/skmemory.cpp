#include "skhook.h"

SKMemoryPool::SKMemoryPool(ULONG dataLen)
{
	m_pBufData = (BYTE *)calloc(dataLen, sizeof(BYTE));
	if(m_pBufData)
	{
		m_ulMaxSize = dataLen;
		m_ulSize = 0;
	}
}

ULONG SKMemoryPool::AddData(const BYTE *data, ULONG dataLen)
{
	size_t sizeToAdd = dataLen;

	//buffer is full
	if(m_ulSize == m_ulMaxSize)
		return 0;

	if((m_ulSize + sizeToAdd) > m_ulMaxSize)
		sizeToAdd = (m_ulMaxSize - m_ulSize);

	memcpy(m_pBufData + m_ulSize, data, sizeToAdd);
	m_ulSize += sizeToAdd;

	return sizeToAdd;
}

void SKMemoryPool::Release()
{
	if(m_pBufData)
		free(m_pBufData);

	m_ulMaxSize = 0;
	m_ulSize = 0;
	m_pBufData = NULL;
}

SKMemoryManager::SKMemoryManager(ULONG poolSize)
{
	InitializeCriticalSection(&m_CriticalSection);
	if(poolSize > 0)
	{
		m_pCurrentPool = new SKMemoryPool(poolSize);
		m_ulPoolSize = poolSize;
	}
}

SKMemoryManager::~SKMemoryManager()
{
	while(!m_PoolQueue.empty())
	{
		SKMemoryPool *pPool = m_PoolQueue.front();
		pPool->Release();
		delete pPool;
		m_PoolQueue.pop();
	}
	DeleteCriticalSection(&m_CriticalSection);
}

void SKMemoryManager::Push(SKMemoryPool *pool)
{
	EnterCriticalSection(&m_CriticalSection);
	m_PoolQueue.push(pool);
	LeaveCriticalSection(&m_CriticalSection);
}

SKMemoryPool *SKMemoryManager::Pop()
{
	SKMemoryPool *pPool = NULL;

	EnterCriticalSection(&m_CriticalSection);
	if(!m_PoolQueue.empty())
	{
		pPool = m_PoolQueue.front();
		m_PoolQueue.pop();
	}
	LeaveCriticalSection(&m_CriticalSection);

	return pPool;
}

void SKMemoryManager::BufferData(const BYTE *data, ULONG dataLen)
{
	if(dataLen == 0)
		return;

	ULONG bytesWritten = m_pCurrentPool->AddData(data, dataLen);
	if(bytesWritten != dataLen)
	{
		Push(m_pCurrentPool);
		m_pCurrentPool = new SKMemoryPool(m_ulPoolSize);
		m_pCurrentPool->AddData(data+bytesWritten, dataLen - bytesWritten);
	}
}

bool SKMemoryManager::IsEmpty()
{
	bool isEmpty = false;
	EnterCriticalSection(&m_CriticalSection);
	isEmpty = m_PoolQueue.empty();
	LeaveCriticalSection(&m_CriticalSection);
	return isEmpty;
}

//pushes the last memory pool into the queue, regardless of its state
void SKMemoryManager::Finalize()
{
	if(m_pCurrentPool)
		Push(m_pCurrentPool);

	m_pCurrentPool = NULL;
}