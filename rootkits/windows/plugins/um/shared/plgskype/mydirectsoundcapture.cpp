#include "skhook.h"
#include <speex/speex.h>
#include <speex/speex_header.h>
#include <speex/speex_bits.h>
#include <speex/speex_stereo.h>
#include "ogg/ogg.h"
#include "sputils.h"
#include "mydirectsoundcapture.h"
#include "mydirectsoundcapturebuffer.h"

#pragma warning(disable:4313 4995)
#define DSCAPTURE (This->lpVtbl->lpDSC)

#define MAX_FRAME_SIZE 2000
#define MAX_FRAME_BYTES 2000

DWORD WINAPI EncodeInputThread(LPVOID lpParam)
{
	SKEncContext *pContext = (SKEncContext *)lpParam;
	short input[MAX_FRAME_SIZE] = {0};
	short output[MAX_FRAME_SIZE] = {0};
	char cbits[MAX_FRAME_BYTES] = {0};
	int eos = 0;
	ogg_packet oggPacket;
	ogg_page oggPage;
	int total_samples = 0;
	int bytes_written = 0;
	int id = 0;
	DWORD dwWaitRes;
	BOOL bContinue = TRUE;
	SKMemoryPool *pPool = NULL;
	BYTE *pPoolData = NULL;
	DWORD dwPoolSize = 0;
	DWORD dwSizeToGet = 0;
	DWORD dwCurSize = 0;
	SHORT *pShortData = NULL;

	while(bContinue)
	{
		dwWaitRes = WaitForSingleObject(pContext->EncStopEvent, 15);
		if(dwWaitRes != WAIT_TIMEOUT)
			bContinue = FALSE;

		while((pPool = pContext->MemoryManager->Pop()) != NULL)
		{
			dwPoolSize = pPool->GetSize();
			if(dwPoolSize == 0)
				continue;

			pPoolData = pPool->GetData();
			dwCurSize = 0;
			do
			{
				ULONG samples_processed = 0;
				ULONG outLen;
				DWORD dwNumSamples = 0;

				do 
				{
					dwSizeToGet = dwPoolSize > pContext->frame_bytes ? pContext->frame_bytes : dwPoolSize;
					dwNumSamples = dwSizeToGet / ((pContext->format / 8)*pContext->channels);

					outLen = MAX_FRAME_SIZE;
					pShortData = (short *)(pPoolData+dwCurSize);
					memset(input, 0, sizeof(input));
					if(pContext->format == 8)
					{
						BYTE *pByteData = (BYTE *)pShortData;
						for(int i = 0; i < dwNumSamples*pContext->channels; i++)
							input[i] = (short)(pByteData[i] << 8) ^ 0x8000;
					}
					else
						memcpy(input, pShortData, dwSizeToGet);

					speex_resampler_process_interleaved_int(pContext->spxResampler, input, (spx_uint32_t *)&dwNumSamples, 
						&output[samples_processed*pContext->channels], (spx_uint32_t *)&outLen);

					total_samples += dwNumSamples;
					samples_processed += outLen;

					dwCurSize += dwSizeToGet;
					dwPoolSize -= dwSizeToGet;
				} while(samples_processed < pContext->frame_size && dwPoolSize);

				if(pContext->channels == 2)
					speex_encode_stereo_int(output, pContext->frame_size, &pContext->spxBits);

				speex_encode_int(pContext->spxState, output, &pContext->spxBits);

				speex_bits_insert_terminator(&pContext->spxBits);
				int nBytes = speex_bits_write(&pContext->spxBits, cbits, MAX_FRAME_BYTES);
				speex_bits_reset(&pContext->spxBits);

				if((dwPoolSize == 0) && pContext->MemoryManager->IsEmpty() && !bContinue)
					oggPacket.e_o_s = 1;
				else
					oggPacket.e_o_s = 0;

				oggPacket.packet = (unsigned char *)cbits;
				oggPacket.bytes = nBytes;
				oggPacket.b_o_s = 0;
				oggPacket.granulepos = (id+1)*pContext->frame_size - pContext->lookahead;
				if(oggPacket.granulepos > total_samples)
					oggPacket.granulepos = total_samples;

				oggPacket.packetno = 2+id;
				ogg_stream_packetin(&pContext->oggStream, &oggPacket);

				while(ogg_stream_pageout(&pContext->oggStream, &oggPage))
				{
					int ret = sp_oe_write_page(&oggPage, pContext->OutputFile);
					bytes_written += ret;
				}

				id++;
			} while (dwPoolSize);

			delete pPool;
		}
	}

	while(ogg_stream_flush(&pContext->oggStream, &oggPage))
	{
		int ret = sp_oe_write_page(&oggPage, pContext->OutputFile);
		bytes_written += ret;
	}

	return 0;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_QueryInterface(IMyDirectSoundCapture *This, REFIID iid, LPVOID *lpPtr)
{
	DBG_OUT(("IDirectSoundCapture::QueryInterface"));
	return IDirectSoundCapture_QueryInterface(DSCAPTURE, iid, lpPtr);
}

ULONG STDMETHODCALLTYPE my_IDirectSoundCapture_AddRef(IMyDirectSoundCapture *This)
{
	DBG_OUT(("IDirectSoundCapture::AddRef\n"));
	return IDirectSoundCapture_AddRef(DSCAPTURE);
}

ULONG STDMETHODCALLTYPE my_IDirectSoundCapture_Release(IMyDirectSoundCapture *This)
{
	int refCount;
	DBG_OUT(("IDirectSoundCapture::Release\n"));
	refCount = IDirectSoundCapture_Release(DSCAPTURE);
	if(refCount == 0)
	{
		free(This->lpVtbl);
		free(This);
	}
	return refCount;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_CreateCaptureBuffer(IMyDirectSoundCapture *This, LPCDSCBUFFERDESC pcDSCBufferDesc, LPDIRECTSOUNDCAPTUREBUFFER *ppDSCBuffer, LPUNKNOWN pUnkOuter)
{
	HRESULT hRes;
	DBG_OUT(("IDirectSoundCapture::CreateCaptureBuffer\n"));

	hRes = IDirectSoundCapture_CreateCaptureBuffer(DSCAPTURE, pcDSCBufferDesc, ppDSCBuffer, pUnkOuter);
	if(!FAILED(hRes))
	{
		IMyDirectSoundCaptureBuffer* pMyDirectSoundCaptureBuffer = (IMyDirectSoundCaptureBuffer *)malloc(sizeof(IMyDirectSoundCaptureBuffer));
		if(pMyDirectSoundCaptureBuffer == NULL)
			return hRes;

		pMyDirectSoundCaptureBuffer->lpVtbl = (IMyDirectSoundCaptureBufferVtbl *)malloc(sizeof(IMyDirectSoundCaptureBufferVtbl));
		if(pMyDirectSoundCaptureBuffer->lpVtbl == NULL)
		{
			free(pMyDirectSoundCaptureBuffer);
			return hRes;
		}

		DBG_OUT(("Creating capture buffer of %d bytes\n", pcDSCBufferDesc->dwBufferBytes));
		if(pcDSCBufferDesc->lpwfxFormat != NULL)
		{
			DBG_OUT(("Capture buffer format: \n"));
			DBG_OUT(("Sample rate: %d\n", pcDSCBufferDesc->lpwfxFormat->nSamplesPerSec));
			DBG_OUT(("Bits per sample: %d\n", pcDSCBufferDesc->lpwfxFormat->wBitsPerSample));
			DBG_OUT(("Channels: %d\n", pcDSCBufferDesc->lpwfxFormat->nChannels));

			pMyDirectSoundCaptureBuffer->lpVtbl->pContext = (SKEncContext *)malloc(sizeof(SKEncContext));
			SKEncContext *pContext = pMyDirectSoundCaptureBuffer->lpVtbl->pContext;
			if(!pContext)
			{
				free(pMyDirectSoundCaptureBuffer->lpVtbl);
				free(pMyDirectSoundCaptureBuffer);
				return hRes;
			}

			// if we can't safely initialize speex encoder, then just fail without logging anything
			int res = sp_init_context(pcDSCBufferDesc->lpwfxFormat, pContext, EncodeInputThread, "skype_in");
			if(res == -1)
			{
				free(pContext);
				free(pMyDirectSoundCaptureBuffer->lpVtbl);
				free(pMyDirectSoundCaptureBuffer);
				return hRes;
			}

			pMyDirectSoundCaptureBuffer->lpVtbl->dwOldReadPosition = 0;
			pMyDirectSoundCaptureBuffer->lpVtbl->dwNewReadPosition = 0;
			pMyDirectSoundCaptureBuffer->lpVtbl->dwBufferSize = pcDSCBufferDesc->dwBufferBytes;
		}
		else
			pMyDirectSoundCaptureBuffer->lpVtbl->pContext = NULL;
		
		
		pMyDirectSoundCaptureBuffer->lpVtbl->lpDSCB = *ppDSCBuffer;

		pMyDirectSoundCaptureBuffer->lpVtbl->QueryInterface = my_IDirectSoundCaptureBuffer_QueryInterface;
		pMyDirectSoundCaptureBuffer->lpVtbl->AddRef = my_IDirectSoundCaptureBuffer_AddRef;
		pMyDirectSoundCaptureBuffer->lpVtbl->Release = my_IDirectSoundCaptureBuffer_Release;

		pMyDirectSoundCaptureBuffer->lpVtbl->GetCaps = my_IDirectSoundCaptureBuffer_GetCaps;
		pMyDirectSoundCaptureBuffer->lpVtbl->GetFormat = my_IDirectSoundCaptureBuffer_GetFormat;
		pMyDirectSoundCaptureBuffer->lpVtbl->GetCurrentPosition = my_IDirectSoundCaptureBuffer_GetCurrentPosition;
		pMyDirectSoundCaptureBuffer->lpVtbl->GetStatus = my_IDirectSoundCaptureBuffer_GetStatus;
		pMyDirectSoundCaptureBuffer->lpVtbl->Initialize = my_IDirectSoundCaptureBuffer_Initialize;
		pMyDirectSoundCaptureBuffer->lpVtbl->Lock = my_IDirectSoundCaptureBuffer_Lock;
		pMyDirectSoundCaptureBuffer->lpVtbl->Start = my_IDirectSoundCaptureBuffer_Start;
		pMyDirectSoundCaptureBuffer->lpVtbl->Stop = my_IDirectSoundCaptureBuffer_Stop;
		pMyDirectSoundCaptureBuffer->lpVtbl->Unlock = my_IDirectSoundCaptureBuffer_Unlock;
		*ppDSCBuffer = (LPDIRECTSOUNDCAPTUREBUFFER)pMyDirectSoundCaptureBuffer;
	}
	return hRes;
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_GetCaps(IMyDirectSoundCapture *This, LPDSCCAPS pDSCCaps)
{
	DBG_OUT(("IDirectSoundCapture::GetCaps\n"));
	return IDirectSoundCapture_GetCaps(DSCAPTURE, pDSCCaps);
}

HRESULT STDMETHODCALLTYPE my_IDirectSoundCapture_Initialize(IMyDirectSoundCapture *This, LPCGUID pcGuidDevice)
{
	DBG_OUT(("IDirectSoundCapture::Initialize\n"));
	return IDirectSoundCapture_Initialize(DSCAPTURE, pcGuidDevice);
}
#pragma warning(default:4313)