#ifndef _SKHOOK_H
#define _SKHOOK_H

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <time.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>
#include <queue>
#include <speex/speex.h>
#include <speex/speex_header.h>
#include <speex/speex_bits.h>
#include <speex/speex_stereo.h>
#include "ogg/ogg.h"
#include "sputils.h"
#include "skmemory.h"
#include "mydirectsound.h"
#include "mydirectsoundcapture.h"
#include "mydirectsoundcapturebuffer.h"
#include "mydirectsoundbuffer.h"
#include <plgskype.h>

using namespace std;

extern BOOL g_bInStarted;
extern BOOL g_bOutStarted;

extern HANDLE g_hCallStarted;
extern HANDLE g_hCallEnded;
extern queue<HANDLE> g_ThreadPool;
extern CRITICAL_SECTION g_TPoolCriticalSection;
extern int enabled;

int plg_copydata (void* data, unsigned long size, unsigned long type, int forceflush);

#endif