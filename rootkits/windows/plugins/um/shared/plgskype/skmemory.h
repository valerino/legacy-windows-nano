#ifndef _SKBUFFER_H
#define _SKBUFFER_H
#include <winsock2.h>
#include <queue>
using namespace std;

class SKMemoryPool
{
private:
	BYTE *m_pBufData;
	ULONG m_ulMaxSize;
	ULONG m_ulSize;

public:
	SKMemoryPool(ULONG dataLen);

	inline BYTE *GetData() { return m_pBufData; }
	inline ULONG GetSize() { return m_ulSize; }
	inline ULONG GetMaxSize() { return m_ulMaxSize; }

	ULONG AddData(const BYTE *data, ULONG dataLen);

	bool IsValid() const { return ((m_pBufData != NULL) && (m_ulMaxSize != 0)); }
	void Release();
};

class SKMemoryManager
{
private:
	queue<SKMemoryPool *> m_PoolQueue;
	SKMemoryPool *m_pCurrentPool;
	CRITICAL_SECTION m_CriticalSection;
	ULONG m_ulCurSize;
	ULONG m_ulPoolSize;

	void Push(SKMemoryPool *pool);
public:
	SKMemoryManager(ULONG poolSize);
	~SKMemoryManager();

	SKMemoryPool *Pop();

	void BufferData(const BYTE *data, ULONG dataLen);
	void Finalize();

	bool IsEmpty();
};

#endif