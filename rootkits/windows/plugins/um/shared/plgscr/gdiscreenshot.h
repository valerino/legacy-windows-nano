#ifndef _GDISCREENSHOT_H
#define _GDISCREENSHOT_H

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <time.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <plgucmds.h>

int get_screenshot (WCHAR *Filename);

#endif