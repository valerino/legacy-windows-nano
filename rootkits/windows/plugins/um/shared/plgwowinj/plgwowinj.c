/*
*	wow64 injection stub : inject dll in the specified process. this is meant to run as a 32bit process only.
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <shellapi.h>
#include <ulib.h>

/*
*	initialize plugin and get global objects
*
*/
int plg_init ()
{
	/* not needed here */

	return 0;
}

/*
*	cleanup plugin stuff
*
*/
void plg_cleanup ()
{
	/* not needed here */
	return;
}

/*
*	main
*
*/
int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) 
{	
	LPWSTR* argv = NULL;
	int argc = 0;
	WCHAR dllpath [MAX_PATH*2];
	unsigned long targetpid = 0;
	wchar_t* stopstring = NULL;
	int timeout = 0;
	int res = -1;
	PVOID oldwow64ctx = NULL;

	/// initialize plugin
	if (plg_init () != 0)
		return -1;
	
	/// check commandline
	if (!lpCmdLine)
		return -1;
	argv = CommandLineToArgvW(GetCommandLineW(),&argc);
	if (argc < 3)
	{
		DBG_OUT(("plgwowinj : too short cmdline (argc=%d\n", argc));
		return -1;
	}

	/// parse params (params = dll, targetpid, optional timeout)
	
	// dllpath
	wcscpy_s(dllpath,MAX_PATH*2,argv[1]);
	
	// targetpid
	__try
	{
		targetpid = wcstoul(argv[2],&stopstring,(int)16);
		if (targetpid == 0)
		{
			DBG_OUT(("plgwowinj : error, targetpid=0\n", argc));
			return -1;
		}

	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DBG_OUT(("plgwowinj : error, invalid targetpid\n", argc));
		return -1;
	}
	
	// timeout
	if (argc == 4)
	{
		__try
		{
			timeout = _wtoi (argv[3]);
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			timeout = 0;
		}
	}

	DBG_OUT(("plgwowinj : dllpath=%S, targetpid=%x, timeout=%d\n", dllpath,targetpid,timeout));
	
	// perform injection
	res = proc_inject_dllw((ULONG_PTR)targetpid,dllpath,timeout);
	
	return res;
}

