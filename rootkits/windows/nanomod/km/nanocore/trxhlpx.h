#ifndef __trxhlpx_h__
#define __trxhlpx_h__

#define TRXHLPX_DEVICE_NAME L"{AEFCF59E-22C8-4b9e-9EC4-EFE122E22A6C}"
/*
*	attaches filter to \\device\tcp
*
*/
void trxhlpx_attachtcp (IN PVOID ctx);

/*
*	dispatch routine
*
*/
NTSTATUS trxhlpx_dispatch (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);

/*
 *	externs
 *
 */

#endif // #ifndef __trxhlpx_h__
