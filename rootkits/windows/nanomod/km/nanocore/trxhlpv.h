#ifndef __trxhlpv_h__
#define __trxhlpv_h__

/*
*	waits for the bfe to be loaded and initialize the filter engine callouts we need
*
*/
void trxhv_waitbfe_and_initialize_wfp (PVOID ctx);

/*
*	internal structures to mantain context
*
*/
typedef struct _ctx_flow {
	long isbrowser;
	ULONG_PTR reserved1;
	ULONG_PTR reserved2;
} ctx_flow;

/*
 *	externals
 *
 */

#endif /// #ifndef __trxhlpv_h__