#ifndef __cmdeng_h__
#define __cmdeng_h__

/*
 *	undoc
 *
 */
// system information structures
typedef enum _SYSTEM_INFORMATION_CLASS
{
	SystemBasicInformation, // 0 Y N
	SystemProcessorInformation, // 1 Y N
	SystemPerformanceInformation, // 2 Y N
	SystemTimeOfDayInformation, // 3 Y N
	SystemNotImplemented1, // 4 Y N
	SystemProcessesAndThreadsInformation, // 5 Y N
	SystemCallCounts, // 6 Y N
	SystemConfigurationInformation, // 7 Y N
	SystemProcessorTimes, // 8 Y N
	SystemGlobalFlag, // 9 Y Y
	SystemNotImplemented2, // 10 Y N
	SystemModuleInformation, // 11 Y N
	SystemLockInformation, // 12 Y N
	SystemNotImplemented3, // 13 Y N
	SystemNotImplemented4, // 14 Y N
	SystemNotImplemented5, // 15 Y N
	SystemHandleInformation, // 16 Y N
	SystemObjectInformation, // 17 Y N
	SystemPagefileInformation, // 18 Y N
	SystemInstructionEmulationCounts, // 19 Y N
	SystemInvalidInfoClass1, // 20
	SystemCacheInformation, // 21 Y Y
	SystemPoolTagInformation, // 22 Y N
	SystemProcessorStatistics, // 23 Y N
	SystemDpcInformation, // 24 Y Y
	SystemNotImplemented6, // 25 Y N
	SystemLoadImage, // 26 N Y
	SystemUnloadImage, // 27 N Y
	SystemTimeAdjustment, // 28 Y Y
	SystemNotImplemented7, // 29 Y N
	SystemNotImplemented8, // 30 Y N
	SystemNotImplemented9, // 31 Y N
	SystemCrashDumpInformation, // 32 Y N
	SystemExceptionInformation, // 33 Y N
	SystemCrashDumpStateInformation, // 34 Y Y/N
	SystemKernelDebuggerInformation, // 35 Y N
	SystemContextSwitchInformation, // 36 Y N
	SystemRegistryQuotaInformation, // 37 Y Y
	SystemLoadAndCallImage, // 38 N Y
	SystemPrioritySeparation, // 39 N Y
	SystemNotImplemented10, // 40 Y N
	SystemNotImplemented11, // 41 Y N
	SystemInvalidInfoClass2, // 42
	SystemInvalidInfoClass3, // 43
	SystemTimeZoneInformation, // 44 Y N
	SystemLookasideInformation, // 45 Y N
	SystemSetTimeSlipEvent, // 46 N Y
	SystemCreateSession, // 47 N Y
	SystemDeleteSession, // 48 N Y
	SystemInvalidInfoClass4, // 49
	SystemRangeStartInformation, // 50 Y N
	SystemVerifierInformation, // 51 Y Y
	SystemAddVerifier, // 52 N Y
	SystemSessionProcessesInformation // 53 Y N
}	 SYSTEM_INFORMATION_CLASS;

NTSYSAPI NTSTATUS ZwQuerySystemInformation(IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
	IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL);

typedef struct _SYSTEM_BASIC_INFORMATION
{
	// Information Class 0 (to fix for 64bit, numberprocessors is wrong)
	ULONG		Unknown;
	ULONG		MaximumIncrement;
	ULONG		PhysicalPageSize;
	ULONG		NumberOfPhysicalPages;
	ULONG_PTR	LowestPhysicalPage;
	ULONG_PTR	HighestPhysicalPage;
	ULONG		AllocationGranularity;
	ULONG_PTR	LowestUserAddress;
	ULONG_PTR	HighestUserAddress;
	ULONG	ActiveProcessors;
	UCHAR	NumberProcessors;
} SYSTEM_BASIC_INFORMATION, * PSYSTEM_BASIC_INFORMATION;

typedef struct _SYSTEM_TIME_OF_DAY_INFORMATION
{
	// Information Class 3
	LARGE_INTEGER	BootTime;
	LARGE_INTEGER	CurrentTime;
	LARGE_INTEGER	TimeZoneBias;
	ULONG			CurrentTimeZoneId;
} SYSTEM_TIME_OF_DAY_INFORMATION, * PSYSTEM_TIME_OF_DAY_INFORMATION;

typedef struct _SYSTEM_KERNEL_DEBUGGER_INFORMATION
{
	// Information Class 35
	BOOLEAN	DebuggerEnabled;
	BOOLEAN	DebuggerNotPresent;
} SYSTEM_KERNEL_DEBUGGER_INFORMATION, * PSYSTEM_KERNEL_DEBUGGER_INFORMATION;

/*
	this layout should be used for the following struct if problems arise ...... (from official winternl.h)

	typedef struct _SYSTEM_PROCESS_INFORMATION {
	ULONG NextEntryOffset;
	ULONG NumberOfThreads;
	BYTE Reserved1[48];
	PVOID Reserved2[3];
	HANDLE UniqueProcessId;
	PVOID Reserved3;
	ULONG HandleCount;
	BYTE Reserved4[4];
	PVOID Reserved5[11];
	SIZE_T PeakPagefileUsage;
	SIZE_T PrivatePageCount;
	LARGE_INTEGER Reserved6[6];
	} SYSTEM_PROCESS_INFORMATION;
*/
typedef struct _SYSTEM_PROCESSES
{
	ULONG			NextEntryDelta;
	ULONG			ThreadCount;
	ULONG			Reserved1[6];
	LARGE_INTEGER	CreateTime;
	LARGE_INTEGER	UserTime;
	LARGE_INTEGER	KernelTime;
	UNICODE_STRING	ProcessName;
	LONG			BasePriority;
	HANDLE			ProcessId;
	HANDLE			InheritedFromProcessId;
	ULONG			HandleCount;
	ULONG			Reserved2[2];
} SYSTEM_PROCESSES, * PSYSTEM_PROCESSES;

typedef struct _SYSTEM_MODULE_INFORMATION { // Information Class 11
	// this structure is preceeded by an ULONG_PTR (!!) which tells how many SYSTEM_MODULE_INFORMATION structures follows
	ULONG_PTR Reserved[2]; // highly guessed from debugging on x64
	PVOID Base;
	ULONG Size;
	ULONG Flags;
	USHORT Index;
	USHORT Unknown;
	USHORT LoadCount;
	USHORT ModuleNameOffset;
	CHAR ImageName[256];
} SYSTEM_MODULE_INFORMATION, *PSYSTEM_MODULE_INFORMATION;

/*
*	returns sysinfo structure. must be freed with ExFreePool
*
*/
rk_sysinfo* cmd_getsysinfo (IN rk_cfg* cfg, OUT PULONG sysinfosize);

/*
*	get default browser exe name. Resulting string->buffer must be freed with ExFreePool
*
*/
NTSTATUS cmd_getdefaultbrowser(OUT PUNICODE_STRING name);

/*
*	parse command
*
*/
NTSTATUS cmd_parse (PVOID buffer, ULONG size);

typedef struct _downloadcmd_ctx {
	HANDLE process;						/// process in which to execute the thread 
	download_cfg_info downinfo;					/// depends on command
	ULONG param1;						/// depends on command
	ULONG param2;						/// depends on command
	ULONG param3;						/// depends on command
} downloadcmd_ctx;

/*
*	update configuration from remote server. 
*   ctx.pathstrings must be a strings array in the form servertype\0ip\0port\0hostname\0basepath\0\relativefilepath\0totalfailures 
*   ctx.param1 is applynow
*   ctx.param2 is causereboot 
*/
NTSTATUS cmd_updatecfg_from_server (downloadcmd_ctx* ctx);

/*
*	get installed keyboard layouts for the current user or,if defaultlayout is provided, default layout is returned
*
*/
NTSTATUS cmd_getkbdlayouts (OPTIONAL OUT kbdlayoutinfo* layoutinfos, IN PVOID userobject, OPTIONAL IN PVOID userobjecttype, OPTIONAL OUT PULONG defaultlayout);

/*
 *	undoc
 *
 */
NTSYSAPI NTSTATUS ZwQueryDefaultUILanguage(PULONG LanguageId);

#endif // #ifndef __cmdeng_h__
