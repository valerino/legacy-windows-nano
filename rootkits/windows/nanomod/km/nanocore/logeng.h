#ifndef __logeng_h__
#define __logeng_h__

#include <nmshared.h>

#ifdef _NANOCORE_INTERNAL
#define NANOCORE_IMPORT
#else
#define NANOCORE_IMPORT DECLSPEC_IMPORT
#endif

#define SIZE_SMALLEVT_LIST 32
#define SIZE_MEDIUMEVT_LIST 1024
#define SIZE_BIGEVT_LIST 4096

#define LOGDIR_NAME L"{68C01843-9B78-4fd9-AF65-54FC0A2E2F8F}"	/// logs go there under %systemroot%\\system32

/*
 *	needed for logging into a workitem
 *
 */
typedef struct _log_ctx {
	PIO_WORKITEM	wrkitem;	/// workitem
	PVOID flt;					/// minifilter
	PVOID fltinst;				/// minifilter instance
	ULONG type;					/// internal
} log_ctx;

/*
*	undoc
*
*/
NTSYSAPI NTSTATUS ZwQueryDefaultLocale(IN BOOLEAN ThreadOrSystem, OUT PLCID Locale);

/*
 *	called to flush events
 *
 */
NANOCORE_IMPORT NTSTATUS log_flush_int (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL IN ULONG type);

/*
 *	add entry to logged events list from existing file (readfile and add entry). This will cause flush too!
 *
 */
NANOCORE_IMPORT NTSTATUS log_addentryfromfile (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, OPTIONAL PVOID rootinstance, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OPTIONAL IN PVOID prebuffer, OPTIONAL IN ULONG prebuffersize, OPTIONAL IN ULONG maxsizetolog, ULONG evt_type);

/*
 *	add entry to logged events list
 *
 */
NANOCORE_IMPORT NTSTATUS log_addentry (OPTIONAL IN PVOID flt, OPTIONAL IN PVOID fltinst, IN PVOID buffer, IN ULONG size, IN ULONG type, OPTIONAL IN BOOLEAN forcepagedpool, OPTIONAL IN BOOLEAN forceflush);

/*
*	return current cache used space
*
*/
NANOCORE_IMPORT LONG log_getcurrentusedspace ();

/*
 *	initialize log engine
 *
 */
NTSTATUS log_initialize ();

/*
 *	externs
 *
 */
extern KEVENT	evt_dataready;			/// data is ready to be sent
extern LONG		currentdisksize;		/// current logdir size

#endif /// #ifndef __logeng_h__