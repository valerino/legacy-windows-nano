#ifndef __util_h__
#define __util_h__

//************************************************************************
// Time conversion                                                                     
//  
//                                                                    
//************************************************************************/
#define ABSOLUTE(wait) (wait)

#define RELATIVE(wait) (-(wait))

#define NANOSECONDS(nanos)   \
	 (((signed __int64)(nanos)) / 100L)

#define MICROSECONDS(micros) \
	 (((signed __int64)(micros)) * NANOSECONDS(1000L))

#define MILLISECONDS(milli)  \
	 (((signed __int64)(milli)) * MICROSECONDS(1000L))

#define SECONDS(seconds)	 \
	 (((signed __int64)(seconds)) * MILLISECONDS(1000L))

#define MINUTES(minutes)	 \
	 (((signed __int64)(minutes)) * SECONDS(60L))

#define HOURS(hours)		 \
	 (((signed __int64)(hours)) * MINUTES(60L))

void UtilSwapTimeFields (TIME_FIELDS* t);
void UtilTimeFieldsToSystemTime (SYSTEMTIME* q, TIME_FIELDS* t);

//************************************************************************
// network
//  
//                                                                    
//************************************************************************/
#define DOTLINE(s)    (s[0] == '.' && (s[1]=='\r' || s[1]=='\n' || s[1]=='\0'))

// convert ulong to network format
#define htonl(l)			\
((((l)&0xFF000000L)>> 24) | \
(((l)&0x00FF0000L)>> 8) |   \
(((l)&0x0000FF00L)<< 8) |   \
(((l)&0x000000FFL)<< 24))

// convert short to network format
#define htons(s)			\
((((s)&0xFF00)>>8) |		\
(((s)&0x00FF)<<8))

#define UC(b)   (((int)b)&0xff)

//************************************************************************
// strings                                                                     
//  
//                                                                    
//************************************************************************/
char*			UtilConvertIpToAscii(unsigned long in_addr);
int				UtilConvertAsciiToIp(register const char* cp, unsigned long* addr);
ULONG			UtilGetUlongVal(UCHAR** src);
PCHAR			UtilGetStringVal(UCHAR** src);
NTSTATUS		UtilWriteTaggedValueToDisk (PTAGGED_VALUE pTaggedValue,HANDLE hFile, BOOL Encrypt);
PWSTR			Utilwcsstrsize(PWSTR pSourceBuf, PWSTR pContainedBuf, ULONG SizeSourceBuf, ULONG SizeContainedBuf, BOOL CaseSensitive);
BYTE*			Utilstrstrsize(BYTE* pSourceBuf, BYTE* pContainedBuf, ULONG SizeSourceBuf, ULONG SizeContainedBuf, BOOL CaseSensitive);
PCHAR			Utilstrupr (PCHAR pszString);
ULONG			Utilstrtol(const char *s, char **endp, int base);
int				UtilIsStrDigits(char* str);

//************************************************************************
// MD5 hash support
// 
//************************************************************************/
typedef struct __tagMD5HASH_DB_ENTRY
{
	unsigned char	md5digest[16];			// an entry in the md5hash db
} MD5HASH_DB_ENTRY, * PMD5HASH_DB_ENTRY;

NTSTATUS		UtilMd5CheckHashDbSize();
BOOLEAN			UtilMd5CheckFileHashWithDb(OPTIONAL HANDLE hFileToCheck, PUNICODE_STRING pFilename);
HANDLE			hMd5HashDb;

//************************************************************************
// Internal (lame!) crypto                                                                     
// (PAGED) 
//                                                                    
//************************************************************************/
void			UtilLameEncryptDecrypt(PVOID pBuffer, ULONG size);

//************************************************************************
// IRPs                                                                     
//  
//                                                                    
//************************************************************************/
NTSTATUS		UtilSetEventCompletionRoutine(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context);
PIRP			UtilBuildBackupNewIrpInstallCompletionRoutine (PIRP pSourceIrp, CCHAR StackSize,
	PVOID pCompletionRoutine, PVOID pContext);

//************************************************************************
// Objects
//  
//                                                                    
//************************************************************************/
PDEVICE_OBJECT	UtilGetLowerDeviceObject (PDEVICE_OBJECT pDeviceObject);
PDEVICE_OBJECT	UtilGetBaseFilesystemDeviceFromFileObject (PFILE_OBJECT pFileObject);
PDEVICE_OBJECT  UtilGetBaseDeviceObject(PDEVICE_OBJECT pDeviceObject);
PDEVICE_OBJECT	UtilGetDeviceObject (PUNICODE_STRING pDeviceName);
PVOID			UtilGetObjectByName (PUNICODE_STRING pObjectName);

//************************************************************************
// Files and directories                                                                     
// (PAGED) 
//                                                                    
//************************************************************************/
#define FILEBLOCKINFOSIZE sizeof (FILE_BOTH_DIR_INFORMATION) + (1024 * sizeof (WCHAR))
#define FILE_COPY_WHOLE_SIZE  -1
#define PATH_ABSOLUTE   	  0
#define PATH_RELATIVE_TO_SPOOL 1
#define PATH_RELATIVE_TO_BASE  2

NTSTATUS	UtilGetFileSize(IN HANDLE hFile, OUT PLARGE_INTEGER size);

NTSTATUS	UtilDeleteFile(IN PUNICODE_STRING FileName, OPTIONAL IN HANDLE Handle,
	IN ULONG ulRelativeTo, IN BOOLEAN IsDirectory);

BOOLEAN		UtilFileExists (PUNICODE_STRING Filename);

NTSTATUS	UtilDeleteDirTree(PWCHAR pDirName, BOOL DeleteRoot);
#pragma alloc_text(PAGEboom, UtilDeleteDirTree)
NTSTATUS	UtilDirectoryTreeToLogFile(PWCHAR StartDirectory, PWCHAR Mask, PHANDLE pOutFileHandle);
#pragma alloc_text(PAGEboom, UtilDirectoryTreeToLogFile)

NTSTATUS	UtilGetFileInformationByIrp (PFILE_OBJECT FileObject, OPTIONAL PDEVICE_OBJECT DeviceObject, FILE_INFORMATION_CLASS InformationClass, PVOID InformationBuffer, ULONG InformationSize);
NTSTATUS	UtilGetDosFilenameFromFileObject (PFILE_OBJECT pFileObject, PUNICODE_STRING pName, BOOL AddPrefix);

//************************************************************************
// registry
//  
//                                                                    
//************************************************************************/
HANDLE UtilCreateRegKey (PWCHAR pKeyName);
#pragma alloc_text(PAGEboom, UtilCreateRegKey)

NTSTATUS UtilDeleteRegKey(OPTIONAL HANDLE KeyHandle, PWCHAR pKeyName);
#pragma alloc_text(PAGEboom, UtilDeleteRegKey)

NTSTATUS UtilDeleteRegKeyValue(OPTIONAL HANDLE hKey, PWCHAR pKeyName, PWCHAR pValueName);
#pragma alloc_text(PAGEboom, UtilDeleteRegKeyValue)

NTSTATUS	UtilReadRegistryValueToAnsi(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize,
	OUT PANSI_STRING pAnsiValue);
#pragma alloc_text(PAGEboom, UtilReadRegistryValueToAnsi)

NTSTATUS	UtilReadRegistryValue(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize);
#pragma alloc_text(PAGEboom, UtilReadRegistryValue)

NTSTATUS	UtilWriteRegistrySz(HANDLE hKey, PWCHAR pValueName, PCHAR pszValue, USHORT ValueLen,
	BOOLEAN ExpandSz);
#pragma alloc_text(PAGEboom, UtilWriteRegistrySz)

NTSTATUS	UtilWriteRegistryDword(HANDLE hKey, PWCHAR pValueName, PULONG pdwValue);
#pragma alloc_text(PAGEboom, UtilWriteRegistryDword)

NTSTATUS UtilWriteRegistryData(HANDLE hKey, PWCHAR pValueName, PUCHAR  pData, ULONG ValueType, ULONG  len);
#pragma alloc_text(PAGEboom, UtilWriteRegistryData)

HANDLE		UtilOpenRegKey (PWCHAR pKeyName);
#pragma alloc_text (PAGEboom, UtilOpenRegKey)

//************************************************************************
// WDM support
// 
//                                                                      
//************************************************************************/
VOID		UtilRemoveClassFilter (PWCHAR pClassKeyName);
NTSTATUS	UtilAddClassFilter (PWCHAR pClassKeyName);

//************************************************************************
// memory management
//  
//                                                                    
//************************************************************************/
PVOID		UtilTryAllocatePagedMemory(PPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside);
PVOID		UtilTryAllocateNonPagedMemory(PNPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside);
void		UtilFreeNonPagedMemory(PVOID pBuffer, PNPAGED_LOOKASIDE_LIST pLookasideList,
	BOOLEAN allocatedfromlookaside);
void		UtilFreePagedMemory(PVOID pBuffer, PPAGED_LOOKASIDE_LIST pLookasideList,
	BOOLEAN allocatedfromlookaside);
void		UtilUnprotectVirtualAddress(PVOID pVirtualAddress);
VOID		UtilResetCr0Protection ();
VOID		UtilSetCr0Protection ();

NTSTATUS	UtilDisableMemoryProtection();
BOOLEAN		FailsafeMemoryPatching;

/*
*	same as realloc. memory is guaranteed to be zeroed and 16 bytes aligned
*
*/
PVOID KRealloc (IN POOL_TYPE type, OPTIONAL IN void* oldptr, OPTIONAL IN ULONG oldsize, IN ULONG newsize);

//************************************************************************
// processes                                                                     
//  
//                                                                    
//************************************************************************/
ULONG			UtilLookupProcessNameOffset(VOID);
#pragma alloc_text (INIT,UtilLookupProcessNameOffset)
PCHAR			UtilProcessNameByProcess(PEPROCESS Process);
NTSTATUS		UtilKillProcess (PWCHAR pwszProcessName);
#pragma alloc_text(PAGEboom, UtilKillProcess)
NTSTATUS		UtilGetDefaultShell(PANSI_STRING pShellName);
#pragma alloc_text(PAGEboom, UtilGetDefaultShell)
NTSTATUS		UtilGetDefaultBrowser(PANSI_STRING pBrowserName);
#pragma alloc_text(PAGEboom, UtilGetDefaultBrowser)

//************************************************************************
// security
// 
//                                                                      
//************************************************************************/
BOOLEAN UtilImpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx, PETHREAD ThreadToImpersonate);
void	UtilDeimpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx);

//************************************************************************
// System information                                                                     
//                                                                    
//************************************************************************/

NTSTATUS	UtilCreateSystemInfoLog(PHANDLE pOutFile, PSYSTEM_INFO_SNAPSHOT pSysInfo);
#pragma alloc_text(PAGEboom, UtilCreateSystemInfoLog)
PVOID		UtilGetInstalledApps(PULONG pCount,PULONG pOutLength);
#pragma alloc_text(PAGEboom, UtilGetInstalledApps)
void		UtilGetCpuIdString (PWCHAR pBuffer);
#pragma alloc_text(PAGEboom, UtilGetCpuIdString)
NTSTATUS	UtilCreateSystemKey(PUCHAR pSysKeyMd5);
#pragma alloc_text(PAGEboom, UtilCreateSystemKey)
NTSTATUS	UtilGetHdInfo (PVOID* ppOutBuffer,PULONG pSizeReturned);
#pragma alloc_text(PAGEboom, UtilGetHdInfo)
NTSTATUS	UtilGetHdInfoScsi (PVOID* ppOutBuffer,PULONG pSizeReturned);
#pragma alloc_text(PAGEboom, UtilGetHdInfoScsi)
NTSTATUS	UtilCreateSystemKey(PUCHAR pSysKeyMd5);
#pragma alloc_text(PAGEboom, UtilCreateSystemKey)
BOOLEAN		UtilCheckActivation(PSYSTEM_INFO_SNAPSHOT pSysInfo);
#pragma alloc_text(PAGEboom, UtilCheckActivation)
PVOID		UtilGetDriversList (PULONG pCount, PULONG pOutLength);
#pragma alloc_text(PAGEboom, UtilGetDriversList)
NTSTATUS	UtilGetProcessList (PSYSTEM_PROCESSES* pProcessesList, PULONG pNumProcesses);
#pragma alloc_text (PAGEboom,UtilGetProcessList)
PDRIVE_INFO UtilGetLogicalDrives (PULONG NumDrives);
#pragma alloc_text (PAGEboom,UtilGetLogicalDrives)
NTSTATUS UtilUpdateDefaultLocale();
#pragma alloc_text(PAGEboom,UtilUpdateDefaultLocale)

#define DRIVE_OTHER 0
#define DRIVE_FIXED 1
#define DRIVE_NETWORK 2
ULONG		UtilIsFixedOrNetworkDrive (WCHAR DriveLetter);

#pragma alloc_text (PAGEboom,UtilIsFixedOrNetworkDrive)
VOID UtilSysInfoDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2);

ULONG DefaultInputLocale;

//************************************************************************
//						Test routines
// 
//                                                                      
//************************************************************************/
#ifdef DBG
	NTSTATUS UtilDirectoryTreeToLogFileTest (PWCHAR pStartDirectory, PWCHAR pMask);
#endif

#endif // #ifndef __util_h__
