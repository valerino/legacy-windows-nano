#ifndef __undoc_h__
#define __undoc_h__

#include <stdio.h>
#include <stdlib.h>
#include <windef.h>

/*
 *	stuff not in wdm.h (ripped from ntifs.h)
 *
 */

#ifdef DEVICE_TYPE
#undef DEVICE_TYPE
#endif
#ifdef DEVICE_TYPE_FROM_CTL_CODE
#undef DEVICE_TYPE_FROM_CTL_CODE
#endif
#ifdef METHOD_FROM_CTL_CODE
#undef METHOD_FROM_CTL_CODE
#endif

#define TOKEN_SOURCE_LENGTH 8

typedef enum _TOKEN_INFORMATION_CLASS {
  TokenUser = 1,
  TokenGroups,
  TokenPrivileges,
  TokenOwner,
  TokenPrimaryGroup,
  TokenDefaultDacl,
  TokenSource,
  TokenType,
  TokenImpersonationLevel,
  TokenStatistics,
  TokenRestrictedSids,
  TokenSessionId,
  TokenGroupsAndPrivileges,
  TokenSessionReference,
  TokenSandBoxInert,
  TokenAuditPolicy,
  TokenOrigin
} TOKEN_INFORMATION_CLASS, *PTOKEN_INFORMATION_CLASS;

typedef struct _TOKEN_SOURCE {
	CHAR SourceName[TOKEN_SOURCE_LENGTH];
	LUID SourceIdentifier;
} TOKEN_SOURCE, *PTOKEN_SOURCE;

typedef struct _TOKEN_CONTROL {
	LUID TokenId;
	LUID AuthenticationId;
	LUID ModifiedId;
	TOKEN_SOURCE TokenSource;
} TOKEN_CONTROL, *PTOKEN_CONTROL;

typedef struct _SID_AND_ATTRIBUTES {
  PSID  Sid;
  ULONG  Attributes;
} SID_AND_ATTRIBUTES, *PSID_AND_ATTRIBUTES;

typedef struct _TOKEN_USER {
  SID_AND_ATTRIBUTES  User;
} TOKEN_USER, *PTOKEN_USER;

typedef struct _SECURITY_CLIENT_CONTEXT {
	SECURITY_QUALITY_OF_SERVICE SecurityQos;
	PACCESS_TOKEN               ClientToken;
	BOOLEAN                     DirectlyAccessClientToken;
	BOOLEAN                     DirectAccessEffectiveOnly;
	BOOLEAN                     ServerIsRemote;
	TOKEN_CONTROL               ClientTokenControl;
} SECURITY_CLIENT_CONTEXT, *PSECURITY_CLIENT_CONTEXT;

typedef struct _FILE_DIRECTORY_INFORMATION {
	ULONG  NextEntryOffset;
	ULONG  FileIndex;
	LARGE_INTEGER  CreationTime;
	LARGE_INTEGER  LastAccessTime;
	LARGE_INTEGER  LastWriteTime;
	LARGE_INTEGER  ChangeTime;
	LARGE_INTEGER  EndOfFile;
	LARGE_INTEGER  AllocationSize;
	ULONG  FileAttributes;
	ULONG  FileNameLength;
	WCHAR  FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

typedef struct _FILE_BOTH_DIR_INFORMATION {
	ULONG  NextEntryOffset;
	ULONG  FileIndex;
	LARGE_INTEGER  CreationTime;
	LARGE_INTEGER  LastAccessTime;
	LARGE_INTEGER  LastWriteTime;
	LARGE_INTEGER  ChangeTime;
	LARGE_INTEGER  EndOfFile;
	LARGE_INTEGER  AllocationSize;
	ULONG  FileAttributes;
	ULONG  FileNameLength;
	ULONG  EaSize;
	CCHAR  ShortNameLength;
	WCHAR  ShortName[12];
	WCHAR  FileName[1];
} FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;

typedef struct _FILE_FULL_DIR_INFORMATION {
	ULONG  NextEntryOffset;
	ULONG  FileIndex;
	LARGE_INTEGER  CreationTime;
	LARGE_INTEGER  LastAccessTime;
	LARGE_INTEGER  LastWriteTime;
	LARGE_INTEGER  ChangeTime;
	LARGE_INTEGER  EndOfFile;
	LARGE_INTEGER  AllocationSize;
	ULONG  FileAttributes;
	ULONG  FileNameLength;
	ULONG  EaSize;
	WCHAR  FileName[1];
} FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;

typedef struct _FILE_NAMES_INFORMATION {
	ULONG  NextEntryOffset;
	ULONG  FileIndex;
	ULONG  FileNameLength;
	WCHAR  FileName[1];
} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;

typedef struct _KPROCESS                        *PKPROCESS;

typedef struct _KAPC_STATE {
	LIST_ENTRY  ApcListHead[2];
	PKPROCESS   Process;
	BOOLEAN     KernelApcInProgress;
	BOOLEAN     KernelApcPending;
	BOOLEAN     UserApcPending;
} KAPC_STATE, *PKAPC_STATE;

typedef struct _MAILSLOT_CREATE_PARAMETERS {
	ULONG           MailslotQuota;
	ULONG           MaximumMessageSize;
	LARGE_INTEGER   ReadTimeout;
	BOOLEAN         TimeoutSpecified;
} MAILSLOT_CREATE_PARAMETERS, *PMAILSLOT_CREATE_PARAMETERS;

typedef struct _NAMED_PIPE_CREATE_PARAMETERS {
	ULONG           NamedPipeType;
	ULONG           ReadMode;
	ULONG           CompletionMode;
	ULONG           MaximumInstances;
	ULONG           InboundQuota;
	ULONG           OutboundQuota;
	LARGE_INTEGER   DefaultTimeout;
	BOOLEAN         TimeoutSpecified;
} NAMED_PIPE_CREATE_PARAMETERS, *PNAMED_PIPE_CREATE_PARAMETERS;

typedef struct _FILE_GET_QUOTA_INFORMATION      *PFILE_GET_QUOTA_INFORMATION;

/*
* When needing these parameters cast your PIO_STACK_LOCATION to
* PEXTENDED_IO_STACK_LOCATION
*/
#include <pshpack4.h>
typedef struct _EXTENDED_IO_STACK_LOCATION {

	/* Included for padding */
	UCHAR MajorFunction;
	UCHAR MinorFunction;
	UCHAR Flags;
	UCHAR Control;

	union {

		struct {
			PIO_SECURITY_CONTEXT              SecurityContext;
			ULONG                             Options;
			USHORT                            Reserved;
			USHORT                            ShareAccess;
			PMAILSLOT_CREATE_PARAMETERS       Parameters;
		} CreateMailslot;

		struct {
			PIO_SECURITY_CONTEXT            SecurityContext;
			ULONG                           Options;
			USHORT                          Reserved;
			USHORT                          ShareAccess;
			PNAMED_PIPE_CREATE_PARAMETERS   Parameters;
		} CreatePipe;

		struct {
			ULONG                           OutputBufferLength;
			ULONG                           InputBufferLength;
			ULONG                           FsControlCode;
			PVOID                           Type3InputBuffer;
		} FileSystemControl;

		struct {
			PLARGE_INTEGER                  Length;
			ULONG                           Key;
			LARGE_INTEGER                   ByteOffset;
		} LockControl;

		struct {
			ULONG                           Length;
			ULONG                           CompletionFilter;
		} NotifyDirectory;

		struct {
			ULONG                           Length;
			PUNICODE_STRING                 FileName;
			FILE_INFORMATION_CLASS          FileInformationClass;
			ULONG                           FileIndex;
		} QueryDirectory;

		struct {
			ULONG                           Length;
			PVOID                           EaList;
			ULONG                           EaListLength;
			ULONG                           EaIndex;
		} QueryEa;

		struct {
			ULONG                           Length;
			PSID                            StartSid;
			PFILE_GET_QUOTA_INFORMATION     SidList;
			ULONG                           SidListLength;
		} QueryQuota;

		struct {
			ULONG                           Length;
		} SetEa;

		struct {
			ULONG                           Length;
		} SetQuota;

		struct {
			ULONG                           Length;
			FS_INFORMATION_CLASS            FsInformationClass;
		} SetVolume;

	} Parameters;

} EXTENDED_IO_STACK_LOCATION, *PEXTENDED_IO_STACK_LOCATION;
#include <poppack.h>

#define FSCTL_TCP_BASE                  FILE_DEVICE_NETWORK

#define _TCP_CTL_CODE(function, method, access) \
CTL_CODE(FSCTL_TCP_BASE, function, method, access)

#define IOCTL_TCP_QUERY_INFORMATION_EX  \
                _TCP_CTL_CODE(0, METHOD_NEITHER, FILE_ANY_ACCESS)

NTKERNELAPI NTSTATUS	PoCallDriver(IN PDEVICE_OBJECT   DeviceObject, IN OUT PIRP Irp);
NTKERNELAPI VOID		PoStartNextPowerIrp(IN PIRP	Irp);
typedef struct _POWERPARAMS
{
	ULONG								SystemContext;
	POWER_STATE_TYPE POINTER_ALIGNMENT	Type;
	POWER_STATE POINTER_ALIGNMENT		State;
	POWER_ACTION POINTER_ALIGNMENT		ShutdownType;
} POWERPARAMS, * PPOWERPARAMS;

typedef struct _IO_WORKITEM*	PIO_WORKITEM;

// Object manipulation support structures (from w2k sources)
#define NUMBER_HASH_BUCKETS 37

typedef struct _OBJECT_DIRECTORY_ENTRY {
    struct _OBJECT_DIRECTORY_ENTRY *ChainLink;
    PVOID Object;
} OBJECT_DIRECTORY_ENTRY, *POBJECT_DIRECTORY_ENTRY;

typedef struct _OBJECT_DIRECTORY {
    struct _OBJECT_DIRECTORY_ENTRY *HashBuckets[ NUMBER_HASH_BUCKETS ];
    struct _OBJECT_DIRECTORY_ENTRY **LookupBucket;
    BOOLEAN LookupFound;
    USHORT SymbolicLinkUsageCount;
    struct _DEVICE_MAP *DeviceMap;
} OBJECT_DIRECTORY, *POBJECT_DIRECTORY;

typedef struct _DEVICE_MAP {
    ULONG ReferenceCount;
    POBJECT_DIRECTORY DosDevicesDirectory;
    ULONG DriveMap;
    UCHAR DriveType[ 32 ];
} DEVICE_MAP, *PDEVICE_MAP;

typedef struct _OBJECT_HEADER_NAME_INFO {
    POBJECT_DIRECTORY Directory;
    UNICODE_STRING Name;
    ULONG Reserved;
} OBJECT_HEADER_NAME_INFO, *POBJECT_HEADER_NAME_INFO;

#define OBJECT_TO_OBJECT_HEADER( o ) \
CONTAINING_RECORD( (o), OBJECT_HEADER, Body )

#define OBJECT_HEADER_TO_NAME_INFO( oh ) ((POBJECT_HEADER_NAME_INFO) \
((oh)->NameInfoOffset == 0 ? NULL : ((PCHAR)(oh) - (oh)->NameInfoOffset)))

NTSTATUS ObOpenObjectByName (IN POBJECT_ATTRIBUTES ObjectAttributes,
							 IN POBJECT_TYPE ObjectType OPTIONAL, IN KPROCESSOR_MODE AccessMode,
							 IN OUT PACCESS_STATE AccessState OPTIONAL, IN ACCESS_MASK DesiredAccess OPTIONAL,
							 IN OUT PVOID ParseContext OPTIONAL, OUT PHANDLE Handle);

NTSYSAPI VOID PsRevertToSelf ( VOID );

NTSYSAPI NTSTATUS PsLookupProcessByProcessId (IN HANDLE ProcessId, OUT PEPROCESS *Process);
NTSYSAPI NTSTATUS PsLookupThreadByThreadId (IN HANDLE ThreadId, OUT PETHREAD *Thread);
NTSYSAPI NTSTATUS PsLookupProcessThreadByCid (IN PCLIENT_ID ClientId, OUT PEPROCESS *Process OPTIONAL, 
											  OUT PETHREAD *Thread);
NTSYSAPI NTSTATUS ZwCreateSection (OUT PHANDLE SectionHandle, IN ACCESS_MASK DesiredAccess,
	IN POBJECT_ATTRIBUTES ObjectAttributes OPTIONAL, IN PLARGE_INTEGER MaximumSize OPTIONAL,
	IN ULONG SectionPageProtection, IN ULONG AllocationAttributes, IN HANDLE FileHandle OPTIONAL);

NTSYSAPI NTSTATUS ZwLoadDriver(PUNICODE_STRING DriverServiceName);
NTSYSAPI NTSTATUS ZwCreateEvent(OUT PHANDLE  EventHandle,IN ACCESS_MASK  DesiredAccess, IN POBJECT_ATTRIBUTES  ObjectAttributes OPTIONAL,
	IN EVENT_TYPE  EventType, IN BOOLEAN  InitialState);
NTSYSAPI NTSTATUS ZwWaitForSingleObject(IN HANDLE  Handle, IN BOOLEAN  Alertable, IN PLARGE_INTEGER  Timeout OPTIONAL);

NTSYSAPI NTSTATUS ZwOpenThread(OUT PHANDLE ThreadHandle, IN ACCESS_MASK AccessMask, 
							   IN POBJECT_ATTRIBUTES ObjectAttributes, IN PCLIENT_ID ClientId ); 
NTSYSAPI NTSTATUS ZwTestAlert(VOID);

NTSYSAPI NTSTATUS SeCreateClientSecurity(IN PETHREAD  ClientThread, IN PSECURITY_QUALITY_OF_SERVICE  ClientSecurityQos,
	IN BOOLEAN  ServerIsRemote, OUT PSECURITY_CLIENT_CONTEXT  ClientContext); 

NTSYSAPI NTSTATUS SeImpersonateClientEx(IN PSECURITY_CLIENT_CONTEXT  ClientContext, IN PETHREAD  ServerThread  OPTIONAL); 

typedef enum _TOKEN_TYPE {
	TokenPrimary = 1,
	TokenImpersonation
} TOKEN_TYPE;
typedef TOKEN_TYPE *PTOKEN_TYPE;

NTKERNELAPI                                     
TOKEN_TYPE                                      
SeTokenType(                                    
			__in PACCESS_TOKEN Token                    
			);                                          

#define PsDereferencePrimaryToken(T) (ObDereferenceObject((T)))
#define PsDereferenceImpersonationToken(T) (ObDereferenceObject((T)))

#define SeDeleteClientSecurity(C)  {                                           \
	if (SeTokenType((C)->ClientToken) == TokenPrimary) {               \
	PsDereferencePrimaryToken( (C)->ClientToken );                 \
	} else {                                                           \
	PsDereferenceImpersonationToken( (C)->ClientToken );           \
	}                                                                  \
}

typedef VOID (*PIO_WORKITEM_ROUTINE) (IN PDEVICE_OBJECT DeviceObject, IN PVOID Context);

#define RtlUnicodeStringToAnsiSize(STRING) (                  \
	NLS_MB_CODE_PAGE_TAG ?                                    \
	RtlxUnicodeStringToAnsiSize(STRING) :                     \
	((STRING)->Length + sizeof(UNICODE_NULL)) / sizeof(WCHAR) \
	)

NTSYSAPI ULONG RtlxUnicodeStringToAnsiSize ( IN PCUNICODE_STRING UnicodeString);

typedef struct _tagPRIVATE_DEVOBJ_EXTENSION {
	WORD Type;
	WORD  Size;
	PDEVICE_OBJECT DeviceObject;
	DWORD PowerFlags;
	DWORD Dope;       
	DWORD ExtensionFlags;
	DWORD DeviceNode;     
	PDEVICE_OBJECT AttachedTo;  
	DWORD StartIoCount;
	DWORD StartIoKey;  
	DWORD StartIoFlags;
	PVPB* Vpb;         
} PRIVATE_DEVOBJ_EXTENSION, *PPRIVATE_DEVOBJ_EXTENSION;

NTSYSAPI NTSTATUS ObOpenObjectByPointer(
					  IN PVOID  Object,
					  IN ULONG  HandleAttributes,
					  IN PACCESS_STATE  PassedAccessState OPTIONAL,
					  IN ACCESS_MASK  DesiredAccess,
					  IN POBJECT_TYPE  ObjectType,
					  IN KPROCESSOR_MODE  AccessMode,
					  OUT PHANDLE  Handle
					  );

#define FlagOn(x, f) ((x) & (f))
typedef VOID (*PDRIVER_FS_NOTIFICATION) (
	IN PDEVICE_OBJECT DeviceObject,
	IN BOOLEAN        DriverActive
	);

NTSYSAPI NTSTATUS IoRegisterFsRegistrationChange(IN PDRIVER_OBJECT  DriverObject, IN PDRIVER_FS_NOTIFICATION  DriverNotificationRoutine); 

NTSYSAPI NTSTATUS ZwOpenSymbolicLinkObject(OUT PHANDLE  LinkHandle, IN ACCESS_MASK  DesiredAccess,
	IN POBJECT_ATTRIBUTES  ObjectAttributes);
NTSYSAPI NTSTATUS ZwQuerySymbolicLinkObject(IN HANDLE LinkHandle, IN OUT PUNICODE_STRING LinkTarget,
	OUT PULONG ReturnedLength OPTIONAL);

NTSYSAPI NTSTATUS ZwQueryDefaultLocale(IN BOOLEAN ThreadOrSystem, OUT PLCID Locale);
NTSYSAPI NTSTATUS ZwQueryDefaultUILanguage(PULONG LanguageId);

NTSYSAPI NTSTATUS ZwQueryInformationToken(IN HANDLE  TokenHandle, 
	IN TOKEN_INFORMATION_CLASS  TokenInformationClass,
    OUT PVOID  TokenInformation,
    IN ULONG  TokenInformationLength,
    OUT PULONG  ReturnLength
    );

NTSYSAPI NTSTATUS ZwOpenProcessTokenEx(
    IN HANDLE  ProcessHandle,
    IN ACCESS_MASK  DesiredAccess,
	IN ULONG HandleAttributes,
    OUT PHANDLE  TokenHandle
    );

NTSYSAPI NTSTATUS ZwOpenProcessToken(
	IN HANDLE ProcessHandle,
	IN ACCESS_MASK DesiredAccess,
	OUT PHANDLE TokenHandle
	);

NTSYSAPI NTSTATUS ZwOpenThreadTokenEx(
	IN HANDLE  ThreadHandle,
    IN ACCESS_MASK  DesiredAccess,
    IN BOOLEAN  OpenAsSelf,
    IN ULONG  HandleAttributes,
    OUT PHANDLE  TokenHandle
    );

NTSYSAPI NTSTATUS RtlConvertSidToUnicodeString(
  PUNICODE_STRING UnicodeString,
  PSID Sid,
  BOOLEAN AllocateDestinationString
);

NTSYSAPI NTSTATUS	ObQueryNameString(IN PVOID Object,
	OUT POBJECT_NAME_INFORMATION ObjectNameInfo, IN ULONG Length, OUT PULONG ReturnLength);  														

NTSYSAPI NTSTATUS ZwDeleteValueKey(IN HANDLE  KeyHandle, IN PUNICODE_STRING  ValueName);

NTSYSAPI NTSTATUS NtWriteFile(IN HANDLE   			FileHandle,
	IN HANDLE Event OPTIONAL, IN PIO_APC_ROUTINE  	ApcRoutine OPTIONAL,
	IN PVOID  ApcContext OPTIONAL, OUT PIO_STATUS_BLOCK	IoStatusBlock,
	IN PVOID  Buffer, IN ULONG   			 Length,
	IN PLARGE_INTEGER  	 ByteOffset OPTIONAL, IN PULONG 			  Key OPTIONAL);

NTSYSAPI NTSTATUS ZwQueryDirectoryFile(IN HANDLE FileHandle,
		IN HANDLE Event OPTIONAL, IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
		IN PVOID ApcContext OPTIONAL, OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FileInformation,
		IN ULONG Length, IN FILE_INFORMATION_CLASS FileInformationClass, IN BOOLEAN ReturnSingleEntry,
		IN PUNICODE_STRING FileName OPTIONAL, IN BOOLEAN RestartScan);

typedef struct _KE_SERVICE_DESCRIPTOR_ENTRY
{
	PULONG		ServiceTableBase;
	PVOID*		CountTable;
	ULONG		NumberofServices;
	PVOID*		ArgumentTableBase;
}KE_SERVICE_DESCRIPTOR_ENTRY, * PKE_SERVICE_DESCRIPTOR_ENTRY;

__declspec(dllimport)	ULONG InitSafeBootMode;		// to handle safeboot

NTSYSAPI NTSTATUS ZwQueryVolumeInformationFile(IN HANDLE FileHandle,
	OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FsInformation, IN ULONG Length,
	IN FS_INFORMATION_CLASS FsInformationClass);

// process stuff
NTSYSAPI NTSTATUS ZwOpenProcess(OUT PHANDLE phProcess, IN ACCESS_MASK AccessMask, 
	IN POBJECT_ATTRIBUTES ObjectAttributes, IN PCLIENT_ID pClientId);

#define PROCESS_TERMINATE   	  (0x0001)  
NTSYSAPI NTSTATUS ZwTerminateProcess(IN HANDLE ProcessHandle OPTIONAL, IN NTSTATUS ExitStatus);

typedef enum _KAPC_ENVIRONMENT {
    OriginalApcEnvironment,
	AttachedApcEnvironment,
	CurrentApcEnvironment
} KAPC_ENVIRONMENT;

NTSYSAPI VOID KeInitializeApc (IN PRKAPC Apc,IN PKTHREAD Thread,IN KAPC_ENVIRONMENT Environment,
	IN PKKERNEL_ROUTINE KernelRoutine, IN PKRUNDOWN_ROUTINE RundownRoutine OPTIONAL, 
	IN PKNORMAL_ROUTINE NormalRoutine OPTIONAL, IN KPROCESSOR_MODE ApcMode, IN PVOID NormalContext);

NTSYSAPI BOOL KeInsertQueueApc( PKAPC Apc, PVOID SystemArgument1,
	PVOID SystemArgument2, UCHAR mode);

NTSYSAPI VOID KeStackAttachProcess (IN PKPROCESS    Process, OUT PKAPC_STATE ApcState);

NTSYSAPI VOID KeUnstackDetachProcess (IN PKAPC_STATE ApcState);

NTSYSAPI BOOLEAN PsIsThreadTerminating (IN PETHREAD Thread);

NTSYSAPI PEPROCESS IoGetRequestorProcess (IN PIRP Irp);

NTSYSAPI PDEVICE_OBJECT IoGetAttachedDevice (IN PDEVICE_OBJECT DeviceObject);

// system information structures
typedef enum _SYSTEM_INFORMATION_CLASS
{
	SystemBasicInformation, // 0 Y N
		SystemProcessorInformation, // 1 Y N
		SystemPerformanceInformation, // 2 Y N
		SystemTimeOfDayInformation, // 3 Y N
		SystemNotImplemented1, // 4 Y N
		SystemProcessesAndThreadsInformation, // 5 Y N
		SystemCallCounts, // 6 Y N
		SystemConfigurationInformation, // 7 Y N
		SystemProcessorTimes, // 8 Y N
		SystemGlobalFlag, // 9 Y Y
		SystemNotImplemented2, // 10 Y N
		SystemModuleInformation, // 11 Y N
		SystemLockInformation, // 12 Y N
		SystemNotImplemented3, // 13 Y N
		SystemNotImplemented4, // 14 Y N
		SystemNotImplemented5, // 15 Y N
		SystemHandleInformation, // 16 Y N
		SystemObjectInformation, // 17 Y N
		SystemPagefileInformation, // 18 Y N
		SystemInstructionEmulationCounts, // 19 Y N
		SystemInvalidInfoClass1, // 20
		SystemCacheInformation, // 21 Y Y
		SystemPoolTagInformation, // 22 Y N
		SystemProcessorStatistics, // 23 Y N
		SystemDpcInformation, // 24 Y Y
		SystemNotImplemented6, // 25 Y N
		SystemLoadImage, // 26 N Y
		SystemUnloadImage, // 27 N Y
		SystemTimeAdjustment, // 28 Y Y
		SystemNotImplemented7, // 29 Y N
		SystemNotImplemented8, // 30 Y N
		SystemNotImplemented9, // 31 Y N
		SystemCrashDumpInformation, // 32 Y N
		SystemExceptionInformation, // 33 Y N
		SystemCrashDumpStateInformation, // 34 Y Y/N
		SystemKernelDebuggerInformation, // 35 Y N
		SystemContextSwitchInformation, // 36 Y N
		SystemRegistryQuotaInformation, // 37 Y Y
		SystemLoadAndCallImage, // 38 N Y
		SystemPrioritySeparation, // 39 N Y
		SystemNotImplemented10, // 40 Y N
		SystemNotImplemented11, // 41 Y N
		SystemInvalidInfoClass2, // 42
		SystemInvalidInfoClass3, // 43
		SystemTimeZoneInformation, // 44 Y N
		SystemLookasideInformation, // 45 Y N
		SystemSetTimeSlipEvent, // 46 N Y
		SystemCreateSession, // 47 N Y
		SystemDeleteSession, // 48 N Y
		SystemInvalidInfoClass4, // 49
		SystemRangeStartInformation, // 50 Y N
		SystemVerifierInformation, // 51 Y Y
		SystemAddVerifier, // 52 N Y
		SystemSessionProcessesInformation // 53 Y N
}	 SYSTEM_INFORMATION_CLASS;

NTSYSAPI NTSTATUS ZwQuerySystemInformation(IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
	IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL);

NTSYSAPI NTSTATUS ZwSetSystemInformation(IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
										 IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength);

typedef struct _LDR_MODULE {
	
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
	PVOID                   BaseAddress;
	PVOID                   EntryPoint;
	ULONG                   SizeOfImage;
	UNICODE_STRING          FullDllName;
	UNICODE_STRING          BaseDllName;
	ULONG                   Flags;
	SHORT                   LoadCount;
	SHORT                   TlsIndex;
	LIST_ENTRY              HashTableEntry;
	ULONG                   TimeDateStamp;
	
} LDR_MODULE, *PLDR_MODULE;

typedef struct _PEB_LDR_DATA {
	ULONG                   Length;
	BOOLEAN                 Initialized;
	PVOID                   SsHandle;
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
} PEB_LDR_DATA, *PPEB_LDR_DATA;

typedef struct _PEB {
	
	BOOLEAN                 InheritedAddressSpace;
	BOOLEAN                 ReadImageFileExecOptions;
	BOOLEAN                 BeingDebugged;
	BOOLEAN                 Spare;
	HANDLE                  Mutant;
	PVOID                   ImageBaseAddress;
	PPEB_LDR_DATA           LoaderData;
	PVOID					ProcessParameters;
	PVOID                   SubSystemData;
	PVOID                   ProcessHeap;
	PVOID                   FastPebLock;
	PVOID					FastPebLockRoutine;
	PVOID			        FastPebUnlockRoutine;
	ULONG                   EnvironmentUpdateCount;
	PVOID*                  KernelCallbackTable;
	PVOID                   EventLogSection;
	PVOID                   EventLog;
	PVOID		            FreeList;
	ULONG                   TlsExpansionCounter;
	PVOID                   TlsBitmap;
	ULONG                   TlsBitmapBits[0x2];
	PVOID                   ReadOnlySharedMemoryBase;
	PVOID                   ReadOnlySharedMemoryHeap;
	PVOID*                  ReadOnlyStaticServerData;
	PVOID                   AnsiCodePageData;
	PVOID                   OemCodePageData;
	PVOID                   UnicodeCaseTableData;
	ULONG                   NumberOfProcessors;
	ULONG                   NtGlobalFlag;
	BYTE                    Spare2[0x4];
	LARGE_INTEGER           CriticalSectionTimeout;
	ULONG                   HeapSegmentReserve;
	ULONG                   HeapSegmentCommit;
	ULONG                   HeapDeCommitTotalFreeThreshold;
	ULONG                   HeapDeCommitFreeBlockThreshold;
	ULONG                   NumberOfHeaps;
	ULONG                   MaximumNumberOfHeaps;
	PVOID*                  *ProcessHeaps;
	PVOID                   GdiSharedHandleTable;
	PVOID                   ProcessStarterHelper;
	PVOID                   GdiDCAttributeList;
	PVOID                   LoaderLock;
	ULONG                   OSMajorVersion;
	ULONG                   OSMinorVersion;
	ULONG                   OSBuildNumber;
	ULONG                   OSPlatformId;
	ULONG                   ImageSubSystem;
	ULONG                   ImageSubSystemMajorVersion;
	ULONG                   ImageSubSystemMinorVersion;
	ULONG                   GdiHandleBuffer[0x22];
	ULONG                   PostProcessInitRoutine;
	ULONG                   TlsExpansionBitmap;
	BYTE                    TlsExpansionBitmapBits[0x80];
	ULONG                   SessionId;
} PEB, *PPEB;

typedef struct _PROCESS_BASIC_INFO {
    NTSTATUS ExitStatus;
    PPEB PebBaseAddress;
    ULONG_PTR AffinityMask;
    KPRIORITY BasePriority;
    ULONG_PTR UniqueProcessId;
    ULONG_PTR InheritedFromUniqueProcessId;
} PROCESS_BASIC_INFO, *PPROCESS_BASIC_INFO;

typedef struct _SYSTEM_PROCESSOR_TIMES { // Information Class 8
	LARGE_INTEGER IdleTime;
	LARGE_INTEGER KernelTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER DpcTime;
	LARGE_INTEGER InterruptTime;
	ULONG InterruptCount;
} SYSTEM_PROCESSOR_TIMES, *PSYSTEM_PROCESSOR_TIMES;

/*
  typedef enum _PROCESSINFOCLASS {
	ProcessBasicInformation, // 0 Y N
	ProcessQuotaLimits, // 1 Y Y
	ProcessIoCounters, // 2 Y N
	ProcessVmCounters, // 3 Y N
	ProcessTimes, // 4 Y N
	ProcessBasePriority, // 5 N Y
	ProcessRaisePriority, // 6 N Y
	ProcessDebugPort, // 7 Y Y
	ProcessExceptionPort, // 8 N Y
	ProcessAccessToken, // 9 N Y
	ProcessLdtInformation, // 10 Y Y
	ProcessLdtSize, // 11 N Y
	ProcessDefaultHardErrorMode, // 12 Y Y
	ProcessIoPortHandlers, // 13 N Y
	ProcessPooledUsageAndLimits, // 14 Y N
	ProcessWorkingSetWatch, // 15 Y Y
	ProcessUserModeIOPL, // 16 N Y
	ProcessEnableAlignmentFaultFixup, // 17 N Y
	ProcessPriorityClass, // 18 N Y
	ProcessWx86Information, // 19 Y N
	ProcessHandleCount, // 20 Y N
	ProcessAffinityMask, // 21 N Y
	ProcessPriorityBoost, // 22 Y Y
	ProcessDeviceMap, // 23 Y Y
    ProcessSessionInformation, // 24 Y Y
	ProcessForegroundInformation, // 25 N Y
	ProcessWow64Information // 26 Y N
} PROCESSINFOCLASS;
*/

NTSYSAPI NTSTATUS ZwQueryInformationProcess(IN HANDLE ProcessHandle,
	IN ULONG ProcessInformationClass, OUT PVOID ProcessInformation, IN ULONG ProcessInformationLength,
	OUT PULONG ReturnLength OPTIONAL);

typedef struct _SYSTEM_HANDLE_INFORMATION { // Information Class 16
	ULONG ProcessId;
	UCHAR ObjectTypeNumber;
	UCHAR Flags; // 0x01 = PROTECT_FROM_CLOSE, 0x02 = INHERIT
	USHORT Handle;
	PVOID Object;
	ACCESS_MASK GrantedAccess;
} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;


#ifndef SID_IDENTIFIER_AUTHORITY_DEFINED
#define SID_IDENTIFIER_AUTHORITY_DEFINED
typedef struct _SID_IDENTIFIER_AUTHORITY {
    UCHAR Value[6];
} SID_IDENTIFIER_AUTHORITY, *PSID_IDENTIFIER_AUTHORITY;
#endif

#ifndef SID_DEFINED
#define SID_DEFINED
typedef struct _SID {
   UCHAR Revision;
   UCHAR SubAuthorityCount;
   SID_IDENTIFIER_AUTHORITY IdentifierAuthority;
#ifdef MIDL_PASS
   [size_is(SubAuthorityCount)] ULONG SubAuthority[*];
#else // MIDL_PASS
   ULONG SubAuthority[ANYSIZE_ARRAY];
#endif // MIDL_PASS
} SID, *PISID;
#endif

#include <WinIoCtl.h>

typedef struct _SYSTEM_MODULE_INFORMATION
{
	// Information Class 11
	ULONG	Reserved[2];			// 0
	PVOID	Base;					// +8
	ULONG	Size;					// +0a
	ULONG	Flags;					// +10
	USHORT	Index;					// +14
	USHORT	Unknown;				// +16
	USHORT	LoadCount;				// +18
	USHORT	ModuleNameOffset;		// +1a
	CHAR	ImageName[256];			// +1c
} SYSTEM_MODULE_INFORMATION, * PSYSTEM_MODULE_INFORMATION;

typedef struct _MODULE_ENTRY
{
	LIST_ENTRY				le_mod;				//+0x000 
	DWORD					unknown[4];			//+0x008 
	DWORD					base;				//+0x018 
	DWORD					driver_start;		//+0x01c 
	DWORD					unk1;				//+0x020 
	UNICODE_STRING			driver_Path;		//+0x024 
	UNICODE_STRING			driver_Name;		//+0x02c 
} MODULE_ENTRY, * PMODULE_ENTRY;

typedef struct _SYSTEM_BASIC_INFORMATION
{
	// Information Class 0
	ULONG	Unknown;
	ULONG	MaximumIncrement;
	ULONG	PhysicalPageSize;
	ULONG	NumberOfPhysicalPages;
	ULONG	LowestPhysicalPage;
	ULONG	HighestPhysicalPage;
	ULONG	AllocationGranularity;
	ULONG	LowestUserAddress;
	ULONG	HighestUserAddress;
	ULONG	ActiveProcessors;
	UCHAR	NumberProcessors;
} SYSTEM_BASIC_INFORMATION, * PSYSTEM_BASIC_INFORMATION;

typedef struct _SYSTEM_TIME_OF_DAY_INFORMATION
{
	// Information Class 3
	LARGE_INTEGER	BootTime;
	LARGE_INTEGER	CurrentTime;
	LARGE_INTEGER	TimeZoneBias;
	ULONG			CurrentTimeZoneId;
} SYSTEM_TIME_OF_DAY_INFORMATION, * PSYSTEM_TIME_OF_DAY_INFORMATION;

//
// Process I/O Counters
//  NtQueryInformationProcess using ProcessIoCounters
//
typedef struct _VIO_COUNTERS
{
	ULONGLONG				ReadOperationCount;
	ULONGLONG				WriteOperationCount;
	ULONGLONG				OtherOperationCount;
	ULONGLONG				ReadTransferCount;
	ULONGLONG				WriteTransferCount;
	ULONGLONG				OtherTransferCount;
} VIO_COUNTERS;
typedef VIO_COUNTERS*	PVIO_COUNTERS;

//
// Process Virtual Memory Counters
//  NtQueryInformationProcess using ProcessVmCounters
//
typedef struct _VVM_COUNTERS
{
	SIZE_T					PeakVirtualSize;
	SIZE_T					VirtualSize;
	ULONG					PageFaultCount;
	SIZE_T					PeakWorkingSetSize;
	SIZE_T					WorkingSetSize;
	SIZE_T					QuotaPeakPagedPoolUsage;
	SIZE_T					QuotaPagedPoolUsage;
	SIZE_T					QuotaPeakNonPagedPoolUsage;
	SIZE_T					QuotaNonPagedPoolUsage;
	SIZE_T					PagefileUsage;
	SIZE_T					PeakPagefileUsage;
} VVM_COUNTERS;
typedef VVM_COUNTERS*	PVVM_COUNTERS;

typedef enum _THREAD_STATE
{
	StateInitialized,
	StateReady,
	StateRunning,
	StateStandby,
	StateTerminated,
	StateWait,
	StateTransition,
	StateUnknown
}	 THREAD_STATE;

typedef struct _SYSTEM_THREADS
{
	LARGE_INTEGER	KernelTime;
	LARGE_INTEGER	UserTime;
	LARGE_INTEGER	CreateTime;
	ULONG			WaitTime;
	PVOID			StartAddress;
	CLIENT_ID		ClientId;
	KPRIORITY		Priority;
	KPRIORITY		BasePriority;
	ULONG			ContextSwitchCount;
	THREAD_STATE	State;
	KWAIT_REASON	WaitReason;
} SYSTEM_THREADS, * PSYSTEM_THREADS;

typedef struct _SYSTEM_PROCESSES
{
	ULONG			NextEntryDelta;
	ULONG			ThreadCount;
	ULONG			Reserved1[6];
	LARGE_INTEGER	CreateTime;
	LARGE_INTEGER	UserTime;
	LARGE_INTEGER	KernelTime;
	UNICODE_STRING	ProcessName;
	KPRIORITY		BasePriority;
	ULONG			ProcessId;
	ULONG			InheritedFromProcessId;
	ULONG			HandleCount;
	ULONG			Reserved2[2];
	VVM_COUNTERS		VmCounters;
	VIO_COUNTERS		IoCounters; // Windows 2000 only
	SYSTEM_THREADS	Threads[1];
} SYSTEM_PROCESSES, * PSYSTEM_PROCESSES;


//  Valid values for the bCommandReg member of IDEREGS.
#define  IDE_ATA_IDENTIFY    0xEC  //  Returns ID sector for ATA.
#define  IDE_ATAPI_IDENTIFY  0xA1  //  Returns ID sector for ATAPI.
#define  FILE_DEVICE_SCSI              0x0000001b
#define  IOCTL_SCSI_MINIPORT 0x0004D008  //  see NTDDSCSI.H for definition
#define  IOCTL_SCSI_MINIPORT_IDENTIFY  ((FILE_DEVICE_SCSI << 16) + 0x0501)

typedef struct _SRB_IO_CONTROL
{
	ULONG HeaderLength;
	UCHAR Signature[8];
	ULONG Timeout;
	ULONG ControlCode;
	ULONG ReturnCode;
	ULONG Length;
} SRB_IO_CONTROL, *PSRB_IO_CONTROL;

#define  SENDIDLENGTH  sizeof (SENDCMDOUTPARAMS) + IDENTIFY_BUFFER_SIZE

// The following struct defines the interesting part of the IDENTIFY
// buffer:
typedef struct _IDSECTOR
{
	USHORT  wGenConfig;
	USHORT  wNumCyls;
	USHORT  wReserved;
	USHORT  wNumHeads;
	USHORT  wBytesPerTrack;
	USHORT  wBytesPerSector;
	USHORT  wSectorsPerTrack;
	USHORT  wVendorUnique[3];
	CHAR    sSerialNumber[20];
	USHORT  wBufferType;
	USHORT  wBufferSize;
	USHORT  wECCSize;
	CHAR    sFirmwareRev[8];
	CHAR    sModelNumber[40];
	USHORT  wMoreVendorUnique;
	USHORT  wDoubleWordIO;
	USHORT  wCapabilities;
	USHORT  wReserved1;
	USHORT  wPIOTiming;
	USHORT  wDMATiming;
	USHORT  wBS;
	USHORT  wNumCurrentCyls;
	USHORT  wNumCurrentHeads;
	USHORT  wNumCurrentSectorsPerTrack;
	ULONG   ulCurrentSectorCapacity;
	USHORT  wMultSectorStuff;
	ULONG   ulTotalAddressableSectors;
	USHORT  wSingleWordDMA;
	USHORT  wMultiWordDMA;
	BYTE    bReserved[128];
} IDSECTOR, *PIDSECTOR;

#define IOCTL_STORAGE_QUERY_PROPERTY   CTL_CODE(IOCTL_STORAGE_BASE, 0x0500, METHOD_BUFFERED, FILE_ANY_ACCESS)


#define STORAGE_DESCRIPTOR_SIZE 10*1024;

// This value should be returned from completion routines to continue
// completing the IRP upwards. Otherwise, STATUS_MORE_PROCESSING_REQUIRED
// should be returned.
#define STATUS_CONTINUE_COMPLETION      STATUS_SUCCESS

//
// An IRPLOCK allows for safe cancellation. The idea is to protect the IRP
// while the canceller is calling IoCancelIrp. This is done by wrapping the
// call in InterlockedExchange(s). The roles are as follows:
//
// Initiator/completion: Cancelable --> IoCallDriver() --> Completed
// Canceller: CancelStarted --> IoCancelIrp() --> CancelCompleted
//
// No cancellation:
//   Cancelable-->Completed
//
// Cancellation, IoCancelIrp returns before completion:
//   Cancelable --> CancelStarted --> CancelCompleted --> Completed
//
// Canceled after completion:
//   Cancelable --> Completed -> CancelStarted
//
// Cancellation, IRP completed during call to IoCancelIrp():
//   Cancelable --> CancelStarted -> Completed --> CancelCompleted
//
//  The transition from CancelStarted to Completed tells the completer to block
//  postprocessing (IRP ownership is transfered to the canceller). Similarly,
//  the canceler learns it owns IRP postprocessing (free, completion, etc)
//  during a Completed->CancelCompleted transition.
//
typedef enum {
	
	IRPLOCK_CANCELABLE,
		IRPLOCK_CANCEL_STARTED,
		IRPLOCK_CANCEL_COMPLETE,
		IRPLOCK_COMPLETED	
} IRPLOCK;

#define IO_IGNORE_SHARE_ACCESS_CHECK    0x0800  // Ignores share access checks on opens.
typedef long (*fnIoCreateFileSpecifyDeviceObjectHint) (void*  FileHandle, unsigned long  DesiredAccess, void* ObjectAttributes, void* IoStatusBlock, 
													 void* AllocationSize, unsigned long FileAttributes, unsigned long ShareAccess, unsigned long Disposition, 
													 unsigned long CreateOptions, void* EaBuffer, unsigned long  EaLength, unsigned long CreateFileType, void* InternalParameters, 
													 unsigned long Options, void* DeviceObject);

#endif // __undoc_h__
