#ifndef __cfgdata_h__
#define __cfgdata_h__

LIST_ENTRY		ListHttpRule;
LIST_ENTRY		ListFilesystemRule;
LIST_ENTRY		ListTdiRule;
LIST_ENTRY		ListPacketRule;
LIST_ENTRY		ListDisableStealthRule;
LIST_ENTRY		ListBypassFsFiltersRule;

//************************************************************************
// exported configuration data and functions                                                                    
//                                                                      
//************************************************************************/

KEVENT			EventReadConfig;
KEVENT			EventUpdateConfigHappened;

NTSTATUS		ConfigCreateDefaultConfig(BOOL AlwaysOverwrite);

NTSTATUS		ConfigCreateUserService(PWCHAR pRegistryMainPath);

NTSTATUS		ConfigParseMainCfgFile();

void			ConfigInitDefaults ();
#pragma alloc_text (INIT,ConfigInitDefaults)

NTSTATUS		ConfigUpdateCfgFile(PDRV_CONFIG pConfig, ULONG size, BOOLEAN bApplyNow);
NTSTATUS		ConfigGetFromHttp();

// driver work directories
WCHAR			BaseDir[MAX_PATH+1];				
WCHAR			DrvDir[MAX_PATH+1];				
WCHAR			CacheDir[MAX_PATH+1];				

// structure which holds driver configuration parameters
DRV_CONFIG		DriverCfg;

// these maps to parameters in cfg
LARGE_INTEGER	CheckIncomingMsgInterval;				
LARGE_INTEGER	CommunicationStopInterval;
LARGE_INTEGER	SysInfoInterval;
LARGE_INTEGER	CheckCacheInterval;
LARGE_INTEGER	SendInterval;
LARGE_INTEGER	SockTimeout;

#endif // __cfgdata_h__