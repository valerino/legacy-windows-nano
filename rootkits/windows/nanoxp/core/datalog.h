#ifndef __datalog_h__
#define __datalog_h__

//************************************************************************
// logging                                                                     
//                                                                      
//************************************************************************/
ULONG					ActualCacheSize;
HANDLE					hBaseDir;
HANDLE					hCacheDir;
PFILE_OBJECT			DrvDirFob;
UNICODE_STRING			BaseDirectoryFullPath;
BOOLEAN					StopLogging;
KEVENT					FileReady;

BOOLEAN					LogCheckForCacheFull (BOOLEAN cycle, ULONG neededsize);
#pragma alloc_text		(PAGEboom, LogCheckForCacheFull)
NTSTATUS				LogOpenRoot(VOID);
#pragma alloc_text		(PAGEboom, LogOpenRoot)
NTSTATUS				LogOpenFile(PHANDLE Handle, PWCHAR Name, BOOL bOverWrite, BOOL KernelHandle);

NTSTATUS				LogGetCurrentCacheSizeAndAdjust(OUT PLARGE_INTEGER CurrentCacheSize,
											   ULONG spaceneeded, PULONG PreviousCacheSize);
#pragma alloc_text(PAGEboom, LogGetCurrentCacheSizeAndAdjust)

NTSTATUS LogCopyFileToBaseDir(HANDLE hFile, IN PUNICODE_STRING FileName, PFILE_INFO pFileInfo, ULONG ulSizeToCopy,
	OPTIONAL ULONG ulFileSize, PUNICODE_STRING DosFileName, OPTIONAL ULONG msgtype, BOOLEAN MoveToCache, BOOLEAN CheckHash);

NTSTATUS LogMoveDataLogToCache(IN ULONG msgType, IN HANDLE LogFileHandle);

NTSTATUS				LogCleanupCacheZeroSizeFiles(HANDLE hDir, ULONG RelativeTo, PULONG CurrentSize, BOOL cleanall);
#pragma alloc_text(PAGEboom, LogCleanupCacheZeroSizeFiles)

NTSTATUS				LogNormalizeCache(ULONG MaxSize, PULONG CurrentSize);
#pragma alloc_text(PAGEboom, LogNormalizeCache)
#endif //__datalog_h__