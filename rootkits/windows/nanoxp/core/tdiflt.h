#ifndef __tdiflt_h__
#define __tdiflt_h__

NPAGED_LOOKASIDE_LIST	LookasideConnectionData;
LIST_ENTRY				ListNetLoggerActions;
KSPIN_LOCK				LockNetLoggerActions;
KEVENT					EventNetActionReady;
NPAGED_LOOKASIDE_LIST	LookasideNetLoggerActions;
LIST_ENTRY				ListTdiConnections;
KSPIN_LOCK				ListTdiConnectionsLock;
NPAGED_LOOKASIDE_LIST	LookasideTdiConnection;
NPAGED_LOOKASIDE_LIST	LookasideTdiAddress;
NPAGED_LOOKASIDE_LIST	LookasideTdiWrkItem;
LIST_ENTRY				ListTdiAddress;
KSPIN_LOCK				ListTdiAddressLock;
NPAGED_LOOKASIDE_LIST	LookasideUrl;

PDEVICE_OBJECT			TcpIpDevice;
KEVENT					EventCommunicating;
ULONG					LocalAddressArray[10];
HANDLE					TdiPnpHandlers;
int						HttpProxyStatus;
extern ULONG			dwHttpProxyAddress;
extern USHORT			usHttpProxyPort;

#define CONNECTIONDATA_LOOKASIDE_SIZE 1024
#define URLBUFFER_SIZE 512

// tcp driver/device names
#define TCPIP_DRIVER L"\\Driver\\TcpIp"
#define TCPIP_DEVICE L"\\Device\\Tcp"
#define NETBT_DRIVER L"netbt.sys"

typedef enum _tagNETLOGGER_ACTIONS
{
	ActionLogConnectionInfo,
	ActionLogConnectionUrl,
	ActionLogConnectionData,
	ActionNetLogCloseFile,
	ActionNetLogKillThread,
} NETLOGGER_ACTIONS, * PNETLOGGER_ACTIONS;

typedef struct _NETLOGGER_ACTION_CONTEXT
{
	LIST_ENTRY				Chain;
	NETLOGGER_ACTIONS		Action;
	HANDLE					FileHandle;
	PVOID					Buffer;
	ULONG					WriteSize;
	struct _TCP_CONNECTION*	TcpConnection;
	struct _TCP_ADDRESS*	TcpAddress;
	ULONG					RemoteIp;
	USHORT					RemotePort;
	ULONG					LocalIp;
	USHORT					LocalPort;
	BOOLEAN					fromlookaside;	// internal use
	PEPROCESS				Process;
	PFILE_OBJECT			FileObject;
	ULONG					LoggingDirection; // 0 = inbound, 1 = outbound
	LARGE_INTEGER			timestamp;
}NETLOGGER_ACTION_CONTEXT, * PNETLOGGER_ACTION_CONTEXT;

#define TCP_STATE_ID 0x1
#define TCP_TABLE_ID 0x101
#define TCP_TABLE_EX_ID 0x102

typedef struct _TCP_ADDRESS
{
	LIST_ENTRY							Chain;
	ULONG								Signature;
	ULONG								Size;
	PFILE_OBJECT						FileObject;
	PEPROCESS							Process;
	PTDI_IND_CONNECT					TdiEventConnect;
	PVOID								TdiConnectContext;
	PTDI_IND_DISCONNECT					TdiEventDisconnect;
	PVOID								TdiDisconnectContext;
	PTDI_IND_RECEIVE					TdiEventReceive;
	PVOID								TdiRecvContext;
	PTDI_IND_CHAINED_RECEIVE			TdiEventChainedReceive;
	PVOID								TdiChainedReceiveContext;
	PTDI_IND_RECEIVE_EXPEDITED			TdiEventReceiveExpedited;
	PVOID								TdiRecvExpeditedContext;
	PTDI_IND_CHAINED_RECEIVE_EXPEDITED	TdiEventChainedReceiveExpedited;
	PVOID								TdiChainedReceiveExpeditedContext;
	PVOID								AssociatedConnection;
}TCP_ADDRESS, * PTCP_ADDRESS;

typedef struct _TCP_CONNECTION
{
	LIST_ENTRY		Chain;
	ULONG			Signature;
	ULONG			Size;
	PFILE_OBJECT	FileObject;
	PVOID			Context;
	PEPROCESS		Process;
	PTCP_ADDRESS	AssociatedAddress;
	TDI_ADDRESS_IP	Remote;
	ULONG			LocalIp;
	USHORT			LocalPort;
	BOOLEAN			IsLogged;
	BOOLEAN			IsCopied;
	ULONG			LoggedBytes;
	ULONG			numdatachunks;
	ULONG			MaxLog;
	HANDLE			LogFileHandle;
	PIRP			OldIrp;
	BOOLEAN			logfilecreated;
	PIO_WORKITEM	pWrkItem;
	USHORT			LogDirectionRule; // 0 = inbound, 1 = outbound, 2 = all
	PNETLOGGER_ACTION_CONTEXT pCommand;

}TCP_CONNECTION, * PTCP_CONNECTION;

void							TdiInitialize();
#pragma alloc_text (INIT,TdiInitialize)
NTSTATUS						TdiAttachTcpIp();
#pragma alloc_text (PAGEboom, TdiAttachTcpIp)
PDEVICE_OBJECT					TdiFindRealTcpDevice (PDEVICE_OBJECT TcpDeviceObject);
NTSTATUS						TdiFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
	PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension);
NTSTATUS						TdiRegisterPnpNotifications (PHANDLE BindingHandle);

#pragma pack(push, 1)
typedef struct _net_data_chunk {
	int cbsize; /// size of this struct
	unsigned long direction; /// direction of this buffer
	LARGE_INTEGER timestamp; /// buffer timestamp
	unsigned long datasize; /// size of appended buffer
} net_data_chunk;
#pragma pack(pop)

#endif // __tdiflt_h__
