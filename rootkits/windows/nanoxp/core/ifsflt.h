#ifndef __ifsflt_h__
#define __ifsflt_h__

#define EXISTS_FASTIO_ENTRY( _FastIoTable, _EntryName) \
((_FastIoTable) && (_FastIoTable)->_EntryName)

NPAGED_LOOKASIDE_LIST			LookasideFsTrackState;
PAGED_LOOKASIDE_LIST			LookasideFsBuffers;
KSPIN_LOCK						LockFsState;
LIST_ENTRY						ListFsState;
PETHREAD						pImpersonatorThread;
PEPROCESS						pImpersonatorProcess;
PKTHREAD						pExProcThread;
PEPROCESS						pExProcProcess;
HANDLE							CommHostProcessHandle;
KEVENT							CommHostProcessFound;
BOOLEAN							CommHostProcessOk;
ANSI_STRING						ShellName;
ANSI_STRING						BrowserName;
BOOLEAN							FsInitializeOk;
KEVENT							FirstVolumeCreated;
typedef struct _OBJECT_HEADER
{
	ULONG			PointerCount;
	ULONG			HandleCount;
	PVOID			Type;
	UCHAR			NameInfoOffset;
	UCHAR			HandleInfoOffset;
	UCHAR			QuotaInfoOffset;
	UCHAR			Flags;
	PVOID			ObjectCreateInfo;
	PVOID			SecurityDescriptor;
	struct _QUAD	Body;
}OBJECT_HEADER, * POBJECT_HEADER;

// list for tracking fileobjects
typedef struct _FS_CREATED_FOBS
{
	LIST_ENTRY		Chain;
	PFILE_OBJECT	FileObject;
}FS_CREATED_FOBS, * PFS_CREATED_FOBS;

// filesystem filter workitems
typedef struct _FS_CTX
{
	PIO_WORKITEM			pWorkItem;
	PEPROCESS				Process;
	PFILE_OBJECT			FileObject;
	KEVENT					Event;
	BOOLEAN					LogName;
	BOOLEAN					LogCopy;
	LARGE_INTEGER			CopySize;
	PETHREAD				CurrThread;
}FS_CTX, * PFS_CTX;

// these can be put in INIT, they're called from driverentry directly
NTSTATUS				FsAttachDeviceRegistration();
#pragma alloc_text (INIT,FsAttachDeviceRegistration)
NTSTATUS				FsInitialize ();
#pragma alloc_text (INIT,FsInitialize)

NTSTATUS				FsCreate(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension);
NTSTATUS				FsCleanup(PDEVICE_OBJECT pDeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
								PDEVICE_EXTENSION DeviceExtension);
VOID					FsNotifyFilesystem(PDEVICE_OBJECT DeviceObject, BOOLEAN Active);

NTSTATUS				FsFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
	PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension);

// fastio entries, all pageable
BOOLEAN					FsFastIoCheckIfPossible(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey,
	IN BOOLEAN CheckForReadOperation, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoCheckIfPossible)

VOID					FsFastIoDetachDevice(IN PDEVICE_OBJECT SourceDevice,
	IN PDEVICE_OBJECT TargetDevice);
#pragma alloc_text (PAGEboom,FsFastIoDetachDevice)

BOOLEAN					FsFastIoDeviceControl(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	IN PVOID InputBuffer OPTIONAL, IN ULONG InputBufferLength, OUT PVOID OutputBuffer OPTIONAL,
	IN ULONG OutputBufferLength, IN ULONG IoControlCode, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoDeviceControl)

BOOLEAN					FsFastIoLock(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key, BOOLEAN FailImmediately,
	BOOLEAN ExclusiveLock, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoLock)

BOOLEAN					FsFastIoQueryBasicInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT PFILE_BASIC_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoQueryBasicInfo)

BOOLEAN					FsFastIoQueryNetworkOpenInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT struct _FILE_NETWORK_OPEN_INFORMATION* Buffer, OUT struct _IO_STATUS_BLOCK* IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoQueryNetworkOpenInfo)

BOOLEAN					FsFastIoQueryOpen(IN struct _IRP* Irp,
	OUT PFILE_NETWORK_OPEN_INFORMATION NetworkInformation, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoQueryOpen)

BOOLEAN					FsFastIoQueryStandardInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT PFILE_STANDARD_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoQueryStandardInfo)

BOOLEAN					FsFastIoRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, OUT PVOID Buffer,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoRead)

BOOLEAN					FsFastIoReadCompressed(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN ULONG LockKey, OUT PVOID Buffer,
	OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
	OUT struct _COMPRESSED_DATA_INFO* CompressedDataInfo, IN ULONG CompressedDataInfoLength,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoReadCompressed)

BOOLEAN					FsFastIoUnlockAll(IN PFILE_OBJECT FileObject, PEPROCESS ProcessId,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoUnlockAll)

BOOLEAN					FsFastIoUnlockAllByKey(IN PFILE_OBJECT FileObject, PVOID ProcessId,
	ULONG Key, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoUnlockAllByKey)

BOOLEAN					FsFastIoUnlockSingle(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoUnlockSingle)

BOOLEAN					FsFastIoWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoWrite)

BOOLEAN					FsFastIoWriteCompressed(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN ULONG LockKey, IN PVOID Buffer,
	OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
	IN struct _COMPRESSED_DATA_INFO* CompressedDataInfo, IN ULONG CompressedDataInfoLength,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoWriteCompressed)

BOOLEAN					FsFastIoMdlRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN ULONG LockKey, OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoMdlRead)

BOOLEAN					FsFastIoMdlReadComplete(IN PFILE_OBJECT FileObject, IN PMDL MdlChain,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoMdlReadComplete)

BOOLEAN					FsFastIoMdlReadCompleteCompressed(IN PFILE_OBJECT FileObject,
	IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoMdlReadCompleteCompressed)

BOOLEAN					FsFastIoMdlWriteComplete(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoMdlWriteComplete)

BOOLEAN					FsFastIoMdlWriteCompleteCompressed(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoMdlWriteCompleteCompressed)

BOOLEAN					FsFastIoPrepareMdlWrite(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN ULONG Length, IN ULONG LockKey, OUT PMDL* MdlChain,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoPrepareMdlWrite)

NTSTATUS				FsFastIoAcquireForModWrite(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER EndingOffset, OUT struct _ERESOURCE** ResourceToRelease,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoAcquireForModWrite)

NTSTATUS				FsFastIoReleaseForModWrite(IN PFILE_OBJECT FileObject,
	IN struct _ERESOURCE* ResourceToRelease, IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoReleaseForModWrite)

NTSTATUS				FsFastIoAcquireForCcFlush(IN PFILE_OBJECT FileObject,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoAcquireForCcFlush)

NTSTATUS				FsFastIoReleaseForCcFlush(IN PFILE_OBJECT FileObject,
	IN PDEVICE_OBJECT DeviceObject);
#pragma alloc_text (PAGEboom,FsFastIoReleaseForCcFlush)

VOID					FsFastIoAcquireFileForNtCreateSection(PFILE_OBJECT FileObject);
#pragma alloc_text (PAGEboom,FsFastIoAcquireFileForNtCreateSection)
VOID					FsFastIoReleaseFileForNtCreateSection(PFILE_OBJECT FileObject);
#pragma alloc_text (PAGEboom,FsFastIoReleaseFileForNtCreateSection)
#endif // __ifsflt_h__
