#ifndef __inputflt_h__
#define __inputflt_h__

PDEVICE_OBJECT					MouseClassDeviceObject;
KEYBOARD_INDICATOR_PARAMETERS	KeyboardState;

NTSTATUS				KeybAttachFilter();
NTSTATUS				KeybAttachFilterWdm(PDEVICE_OBJECT pdo);
NTSTATUS				MouseAttachFilter();
NTSTATUS				MouseAttachFilterWdm(PDEVICE_OBJECT pdo);

NTSTATUS				KeybReadComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context);

NTSTATUS				InputShutdownHandler(PDEVICE_OBJECT DeviceObject, PIRP Irp);
NTSTATUS				InputAddDevice(IN PDRIVER_OBJECT  DriverObject, IN PDEVICE_OBJECT  PhysicalDeviceObject );

NTSTATUS				KeybFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
										   PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension);

NTSTATUS				MouseFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp,
											PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension);
#define INPUT_ATTACH_MOUSE	1
#define INPUT_ATTACH_KBD	2

#endif //#ifndef __inputflt_h__