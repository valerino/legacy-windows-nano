#ifndef __lanflt_h__
#define __lanflt_h__

NTSTATUS LanAttachFilter (PDRIVER_OBJECT pDriverObject);
#pragma alloc_text(PAGEboom,LanAttachFilter)
NTSTATUS LanFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,PDEVICE_EXTENSION DeviceExtension);

#endif // __lanflt_h__