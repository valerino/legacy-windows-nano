#ifndef __evtlog_h__
#define __evtlog_h__

ULONG							EventsCount;
LIST_ENTRY						EventList;
KSPIN_LOCK						LockEventList;
NPAGED_LOOKASIDE_LIST			LookasideEventList;
NPAGED_LOOKASIDE_LIST			LookasideSmallEventList;

typedef struct __tagEVENT_WRK_CONTEXT
{
	PIO_WORKITEM			pWorkItem;
	PDEVICE_OBJECT			AttachedDevice;
	KEVENT					Event;
}EVENT_WRK_CONTEXT, *PEVENT_WRK_CONTEXT;

NPAGED_LOOKASIDE_LIST	LookasideEventsCtx;

VOID	EvtInitialize();
#pragma alloc_text		(INIT,EvtInitialize)

PVOID	EvtAllocateEntry(BOOL allocatefromsmall);
VOID	EvtAddEntry(PNANO_EVENT_MESSAGE Entry);
VOID	EvtFreeEntry(PVOID Entry, BOOL freefromsmall);

NTSTATUS EvtDumpEntries(PKEVENT EventDone);
#endif //__evtlog_h__