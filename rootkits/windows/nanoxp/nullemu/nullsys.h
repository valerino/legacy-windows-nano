#ifndef __nullsys_h__
#define __nullsys_h__

// signal that pico must be installed
#define PICO_INSTALLING_KEY L"{48F7C805-F2E8-41d8-A511-693A9C8DABA2}" 

// the original nullsys is copied into system32 with this name
#define BACKUP_NULLSYS_NAME L"msnlx32d.dll"

//#define NO_NULLSYS_DBGMSG

#endif // #ifndef __nullsys_h__