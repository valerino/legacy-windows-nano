using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace rktool
{
	public enum plugin_type { dropper, kernelmode, usermode };

	/// <summary>
	/// represents a plugin
	/// </summary>
    public class cplugincfg
    {
        [XmlAttribute("name")]
        public string m_name;
        [XmlAttribute("targetname")]
        public string m_targetname;
        [XmlAttribute("desc")]
        public string m_desc;
        [XmlAttribute("type")]
        public plugin_type m_type;
		[XmlAttribute("rk")]
		public string m_rk;
		[XmlAttribute("installer")]
        public string m_installer;
        [XmlAttribute("excludeall")]
        public bool m_excludeall;
        [XmlAttribute("cfg")]
        public bool m_cfg; 
        [XmlAttribute("coinstaller")]
        public bool m_coinstaller;
        [XmlAttribute("apps")]
        public string m_apps;
		[XmlAttribute("deps")]
        public string m_deps;
		[XmlAttribute("needed3264bit")]
		public bool m_needed3264bit;
        [XmlAttribute("include3264bit")]
        public bool m_include3264bit;
        [XmlAttribute("only32bit")]
		public bool m_only32bit;
		[XmlAttribute("only64bit")]
		public bool m_only64bit;
		[XmlAttribute("loadorder")]
		public int m_loadorder;
		[XmlAttribute("usenullsys")]
		public bool m_nullsys;
		[XmlAttribute("legacy")]
		public bool m_legacy;
		[XmlAttribute("selected")]
        public bool m_selected;
        [XmlAttribute("osversion")]
        public string m_osversion;
        
        public bool m_processed; // internal		

		/// <summary>
		/// constructor
		/// </summary>
		/// <returns>a cplugincfg instance</returns>
		public cplugincfg ()
		{

		}
        
	    /// <summary>
	    /// create a cplugincfg instance
	    /// </summary>
	    /// <param name="name">plugin name</param>
		/// <returns>a cplugincfg instance</returns>
	    public cplugincfg(string name)
		{
			m_name = name;
		}

		/// <summary>
		/// create a cplugincfg instance, copying another
		/// </summary>
		/// <param name="item">cplugincfg instance</param>
		/// <returns>a cplugincfg instance</returns>
		public cplugincfg(cplugincfg item)
		{
			m_name = "<newplugin>";
            m_targetname = "<newplugin>";
			m_loadorder = 999;
			m_deps = item.m_deps;
			m_installer = item.m_installer;
			m_legacy = item.m_legacy;
			m_rk = item.m_rk;
			m_type = item.m_type;
			m_only64bit = item.m_only64bit;
			m_only32bit = item.m_only32bit;
			m_apps = "";
			m_cfg = false;
			m_excludeall = false;
			m_needed3264bit = false;
            m_include3264bit = false;
			m_nullsys = false;
            m_osversion = "<minmj,maxmj,minmn,maxmn,minsp,maxsp,condition>";
		}

		/// <summary>
		/// add plugin to specified list
		/// </summary>
		/// <param name="list">list to add the plugin to</param>
		/// <param name="item">plugin to add</param>
		/// <returns>index at which the plugin is added, -1 on error</returns>
		public static int add_plugin(ArrayList list, cplugincfg item)
        {
			try
			{
				return list.Add(item);
			}
			catch
			{
				return -1;
			}
        }

		/// <summary>
		/// get plugin type from string
		/// </summary>
		/// <param name="stringtype">plugintype string (kernelmode, usermode, dropper)</param>
		/// <returns>plugin_type or throws exception</returns>
		public static plugin_type get_type_from_string (string stringtype)
		{
			if (stringtype == "kernelmode")
				return plugin_type.kernelmode;
			else if (stringtype == "usermode")
				return plugin_type.usermode;
			else if (stringtype == "dropper")
				return plugin_type.dropper;
			
			// not found
			throw (new System.Exception());
		}

		/// <summary>
		/// get string from plugin type
		/// </summary>
		/// <param name="plugintype">plugin_type (kernelmode, usermode, dropper)</param>
		/// <returns>string or throws exception</returns>
		public static string get_string_from_type(plugin_type plugintype)
		{
			if (plugintype == plugin_type.kernelmode)
				return "kernelmode";
			else if (plugintype == plugin_type.usermode)
				return "usermode";
			else if (plugintype == plugin_type.dropper)
				return "dropper";

			// not found
			throw (new System.Exception());
		}

		/// <summary>
		/// add plugin to specified list, making a new copy and checking existance
		/// </summary>
		/// <param name="list">list to add the plugin to</param>
		/// <param name="item">plugin to add</param>
		/// <param name="makecopy">true to make a copy</param>
        /// <param name="checkexistance">true to check for existance</param>
		/// <returns>index at which the plugin is added, -1 on error/exists</returns>
        public static int add_plugin(ArrayList list, cplugincfg item, bool makecopy, bool checkexistance)
        {
            bool exists = false;

            if (!checkexistance)
                return add_plugin(list, item, makecopy);

            foreach (cplugincfg plugin in list)
            {
                // exists ?
                if (plugin.m_rk == item.m_rk && plugin.m_name == item.m_name)
                    exists = true;
            }
            if (exists)
                return -1;
            
            // add
            return add_plugin(list, item, makecopy);
        }

        /// <summary>
		/// add plugin to specified list, making a new copy
		/// </summary>
		/// <param name="list">list to add the plugin to</param>
		/// <param name="item">plugin to add</param>
		/// <param name="makecopy">true to make a copy</param>
		/// <returns>index at which the plugin is added, -1 on error</returns>
		public static int add_plugin(ArrayList list, cplugincfg item, bool makecopy)
		{
			try
			{
				if (makecopy)
				{
					cplugincfg newitem = new cplugincfg(item.m_name);
                    newitem.m_targetname = item.m_name;
                    newitem.m_apps = item.m_apps;
					newitem.m_cfg = item.m_cfg;
					newitem.m_coinstaller = item.m_coinstaller;
					newitem.m_deps = item.m_deps;
					newitem.m_desc = item.m_desc;
					newitem.m_excludeall = item.m_excludeall;
					newitem.m_installer = item.m_installer;
					newitem.m_legacy = item.m_legacy;
					newitem.m_loadorder = item.m_loadorder;
					newitem.m_needed3264bit = item.m_needed3264bit;
                    newitem.m_include3264bit = item.m_include3264bit;
					newitem.m_nullsys = item.m_nullsys;
					newitem.m_only32bit = item.m_only32bit;
					newitem.m_only64bit = item.m_only64bit;
					newitem.m_rk = item.m_rk;
					newitem.m_type = item.m_type;
					newitem.m_selected = item.m_selected;
					newitem.m_processed = item.m_processed;
                    newitem.m_osversion = item.m_osversion;
                    return list.Add(newitem);
				}
				return list.Add(item);
			}
			catch
			{
				return -1;
			}
		}

		/// <summary>
		/// get plugin from list
		/// </summary>
		/// <param name="list">list to search in</param>
		/// <param name="plugin_name">plugin name</param>
		/// <returns>a cplugincfg instance</returns>
		public static cplugincfg get_plugin(ArrayList list, string plugin_name)
        {
			foreach (cplugincfg currentitem in list)
			{
				if (currentitem.m_name == plugin_name)						
					return currentitem;
			}

			return null;
        }

		/// <summary>
		/// get plugin from list, specifying rootkit name list
		/// </summary>
		/// <param name="list">list to search in</param>
		/// <param name="plugin_name">plugin name</param>
		/// <param name="rkname">rootkit name</param>
		/// <returns>a cplugincfg instance</returns>
		public static cplugincfg get_plugin(ArrayList list, string plugin_name, string rkname)
		{
			if (string.IsNullOrEmpty(rkname))
				return get_plugin(list, plugin_name);

			foreach (cplugincfg currentitem in list)
			{
				if (!string.IsNullOrEmpty(currentitem.m_rk))
				{
					if (currentitem.m_name == plugin_name && rkname == currentitem.m_rk)
						return currentitem;
				}
				else
				{
					if (currentitem.m_name == plugin_name)
						return currentitem;
				}
			}
			
			return null;
		}

		/// <summary>
		/// remove plugin from list
		/// </summary>
		/// <param name="list">list to search in</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <returns>true if removed</returns>
		public static bool remove_plugin(ArrayList list, cplugincfg plugin)
		{
			try
			{
				list.Remove(plugin);
				return true;
			}
			catch
			{

			}
			return false;

		}

		/// <summary>
        /// remove plugin from list
        /// </summary>
        /// <param name="list">list to search in</param>
        /// <param name="plugin_name">plugin to remove</param>
        /// <returns>true if removed</returns>
        public static bool remove_plugin(ArrayList list, string plugin_name)
        {
			cplugincfg item = get_plugin(list, plugin_name);
			return remove_plugin(list, item);
        }

		/// <summary>
		/// remove plugin from list specifying rootkit name
		/// </summary>
		/// <param name="list">list to search in</param>
		/// <param name="plugin_name">plugin to remove</param>
		/// <param name="rkname">rootkit name</param>
		/// <returns>true if removed</returns>
		public static bool remove_plugin(ArrayList list, string plugin_name, string rkname)
		{
			if (string.IsNullOrEmpty(rkname))
				return remove_plugin(list, plugin_name);

			cplugincfg item = get_plugin(list, plugin_name, rkname);
			return remove_plugin(list, item);
		}

		/// <summary>
		/// set plugin selected
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="name">plugin name</param>
		/// <returns></returns>
		public static void set_selected(ArrayList list, string name)
		{
			set_selected(list,get_plugin(list, name));			
		}

		/// <summary>
		/// select plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <returns></returns>
		public static void set_selected (ArrayList list, cplugincfg plugin)
		{
			if (plugin == null)
				return;
			plugin.m_selected = true;

			// select dependencies too
			if (string.IsNullOrEmpty(plugin.m_deps))
				return;
			string[] deps = plugin.m_deps.Split(',');			
			foreach (string d in deps)
			{
				// recurse
				cplugincfg p = get_plugin(list, d);
				set_selected(list, p);
			}
		}

		/// <summary>
		/// select plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <param name="handledeps">handle dependencies</param>
		/// <returns></returns>
		public static void set_selected(ArrayList list, cplugincfg plugin, bool handledeps)
		{
			if (plugin == null)
				return;
			plugin.m_selected = true;

			// select dependencies too
			if (string.IsNullOrEmpty(plugin.m_deps) || !handledeps)
				return;
			string[] deps = plugin.m_deps.Split(',');
			foreach (string d in deps)
			{
				// recurse
				cplugincfg p = get_plugin(list, d);
				set_selected(list, p);
			}
		}

		/// <summary>
		/// select plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="name">plugin name</param>
		/// <param name="rkname">rootkit name</param>
		/// <returns></returns>
		public static void set_selected(ArrayList list, string name, string rkname)
		{
			if (string.IsNullOrEmpty(rkname))
			{
				set_selected(list, name);
				return;
			}
			
			cplugincfg item = get_plugin(list, name,rkname);
			set_selected(list, item);
		}

		/// <summary>
		/// check if the specified plugin is a dependency among the selected plugins
		/// </summary>
		/// <param name="list">list to work with</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <param name="selectedonly">selected plugins only</param>
		/// <returns>true if its a dependency</returns>
		public static bool is_dependency(ArrayList list, cplugincfg plugin, bool selectedonly)
		{
			// walk list
			foreach (cplugincfg plgtocheck in list)
			{				
				// skip this very plugin
				if (plgtocheck.m_name == plugin.m_name && plgtocheck.m_rk == plugin.m_rk)
					continue;
				
				// if specified, consider selected plugins only
				if (selectedonly && !plgtocheck.m_selected)
					continue;

				if (!(string.IsNullOrEmpty(plugin.m_rk)))
				{
					if ((plugin.m_rk != plgtocheck.m_rk) && plugin.m_type != plugin_type.dropper)
						continue;
				}
				
				// check if name is a dependency
				if (string.IsNullOrEmpty(plgtocheck.m_deps))
					continue;

				string[] deps = plgtocheck.m_deps.Split(',');
				foreach (string d in deps)
				{
					if (plugin.m_name == d)
						return true;
				}
			}
			
			return false;
		}

		/// <summary>
		/// extract a string from csv string
		/// </summary>
		/// <param name="targetstring">csv string</param>
		/// <param name="stringtosearch">string to search</param>
		/// <returns></returns>
		public string get_csv_value (string csvstring, string stringtosearch)
		{
			string[] strings = csvstring.Split(',');
			foreach (string item in strings)
			{
				if (item == stringtosearch)
					return item;
			}
			return null;
		}

		/// <summary>
		/// add a string to csv string
		/// </summary>
		/// <param name="targetstring">csv string</param>
		/// <param name="stringtoadd">string to add</param>
		/// <returns>the new string</returns>
		public static string add_csv_value(string targetstring, string stringtoadd)
		{
			string newstring;

			if (string.IsNullOrEmpty(stringtoadd))
				return targetstring;

			if (string.IsNullOrEmpty(targetstring))
				newstring = stringtoadd;
			else
				newstring = targetstring + "," + stringtoadd;
			return newstring;
		}

		/// <summary>
		/// remove a string from csv string
		/// </summary>
		/// <param name="targetstring">csv string</param>
		/// <param name="stringtoremove">string to remove</param>
		/// <returns>the new string</returns>
		public static string remove_csv_value(string targetstring, string stringtoremove)
		{
			if (string.IsNullOrEmpty(targetstring))
				return targetstring;

			try
			{
				int idx = targetstring.IndexOf(stringtoremove);
				if (idx != -1)
				{
					string newstring = targetstring.Remove(idx, stringtoremove.Length);
					newstring = newstring.Replace(",,", ",");
					newstring = newstring.TrimStart(',');
					newstring = newstring.TrimEnd(',');
					return newstring;
				}
			}
			catch
			{
				
			}
						
			return targetstring;
		}

		/// <summary>
		/// deselect plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <param name="force">force deselect</param>
		/// <returns>true if it can be deselected</returns>
		public static bool set_deselected (ArrayList list, cplugincfg plugin, bool force)
		{
			if (plugin == null)
				return false;

			foreach (cplugincfg item in list)
			{
				// check if this is a dependency of another selected plugin (if force isnt specified)
				if (!force) 
				{
					if (is_dependency(list,plugin,true))
						return false;
				}

				if (plugin.m_name == item.m_name && plugin.m_rk == item.m_rk) 
				{
					item.m_selected = false;
					return true;
				}
			}
			
			return false;
		}

		/// <summary>
		/// deselect plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="name">plugin name</param>
		/// <param name="force">force deselect</param>
		/// <returns>true if it can be deselected</returns>
		public static bool set_deselected(ArrayList list, string name, bool force)
		{
			cplugincfg plugin = get_plugin(list, name);
			return set_deselected(list, plugin, force);
		}

		/// <summary>
		/// deselect plugin
		/// </summary>
		/// <param name="list">list to work in</param>
		/// <param name="name">plugin name</param>
		/// <param name="rkname">rootkit name</param>
		/// <param name="force">force deselect</param>
		/// <returns>true if it can be deselected</returns>
		public static bool set_deselected(ArrayList list, string name, string rkname, bool force)
		{
			cplugincfg plugin = get_plugin(list, name, rkname);
			return set_deselected (list,plugin,force);
		}

		public class cloadordersort : IComparer
		{
			/// <summary>
			/// custom sort with plugins loadorder	
			/// </summary>
			/// <param name="x"></param>
			/// <param name="y"></param>
			/// <returns></returns>
			int IComparer.Compare(Object x, Object y)
			{
				cplugincfg xx = (cplugincfg)x;
				cplugincfg yy = (cplugincfg)y;

				if (xx.m_loadorder == yy.m_loadorder)
					return 0;
				if (xx.m_loadorder > yy.m_loadorder)
					return 1;
				return -1;
			}
		}

		public class cinstallersort : IComparer
		{
			/// <summary>
			/// custom sort with plugins installer
			/// </summary>
			/// <param name="x"></param>
			/// <param name="y"></param>
			/// <returns></returns>
			int IComparer.Compare(Object x, Object y)
			{
				cplugincfg xx = (cplugincfg)x;
				cplugincfg yy = (cplugincfg)y;

				if (string.Compare (xx.m_installer,yy.m_installer) == 0)
					return 0;
				if (string.Compare (xx.m_installer, yy.m_installer) > 0)
					return 1;
				
				return -1;
			}
		}

		/// <summary>
		/// sort plugins list using loadorder
		/// </summary>
		/// <param name="list">list to work with</param>
		/// <returns></returns>
		public static void sort_plugins_with_loadorder(ArrayList list)
		{
			cloadordersort comparer = new cloadordersort();
			try
			{
				list.Sort(comparer);
			}
			catch
			{
			}
		}

		/// <summary>
		/// sort plugins list using loadorder returning new list
		/// </summary>
		/// <param name="list">list to work with</param>
		/// <returns>sorted arraylist</returns>
		public static ArrayList sort_plugins_with_loadorder_return (ArrayList list)
		{
			cloadordersort comparer = new cloadordersort();
			try
			{
				ArrayList newlist = new ArrayList();
				foreach (Object item in list)
					newlist.Add(item);

				newlist.Sort(comparer);
				return newlist;
			}
			catch
			{

			}
			return null;
		}
		
		/// <summary>
		/// sort plugins list using installers
		/// </summary>
		/// <param name="list">list to work with</param>
		/// <returns>sorted arraylist</returns>
		public static ArrayList sort_plugins_with_installer(ArrayList list)
		{
			cinstallersort comparer = new cinstallersort();
			try
			{
				ArrayList newlist = new ArrayList();
				foreach (Object item in list)
					newlist.Add(item);

				newlist.Sort(comparer);
				return newlist;
			}			
			catch
			{

			}
			return null;
		}

	}
}
