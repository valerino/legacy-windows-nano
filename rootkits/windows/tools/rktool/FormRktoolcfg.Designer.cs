namespace rktool
{
	partial class formRktoolcfg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formRktoolcfg));
            this.menuOptionsform = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuAddplugin = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRemoveplugin = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.objlistGlobalplugins = new BrightIdeasSoftware.ObjectListView();
            this.columnLoadorder = new BrightIdeasSoftware.OLVColumn();
            this.columnName = new BrightIdeasSoftware.OLVColumn();
            this.columnTargetName = new BrightIdeasSoftware.OLVColumn();
            this.columnDesc = new BrightIdeasSoftware.OLVColumn();
            this.columnType = new BrightIdeasSoftware.OLVColumn();
            this.columnRk = new BrightIdeasSoftware.OLVColumn();
            this.columnDeps = new BrightIdeasSoftware.OLVColumn();
            this.columnOs = new BrightIdeasSoftware.OLVColumn();
            this.columnInstaller = new BrightIdeasSoftware.OLVColumn();
            this.columnApps = new BrightIdeasSoftware.OLVColumn();
            this.columnCfg = new BrightIdeasSoftware.OLVColumn();
            this.columnCoinstaller = new BrightIdeasSoftware.OLVColumn();
            this.columnExcludeall = new BrightIdeasSoftware.OLVColumn();
            this.column32only = new BrightIdeasSoftware.OLVColumn();
            this.column64only = new BrightIdeasSoftware.OLVColumn();
            this.column64needed = new BrightIdeasSoftware.OLVColumn();
            this.columnBothVersion = new BrightIdeasSoftware.OLVColumn();
            this.columnLegacy = new BrightIdeasSoftware.OLVColumn();
            this.columnNullsys = new BrightIdeasSoftware.OLVColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textToolspath = new System.Windows.Forms.TextBox();
            this.textBinpath = new System.Windows.Forms.TextBox();
            this.textCryptokey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonBrowsetools = new System.Windows.Forms.Button();
            this.buttonBrowsebin = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.tooltipMainCfg = new System.Windows.Forms.ToolTip(this.components);
            this.menuOptionsform.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objlistGlobalplugins)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuOptionsform
            // 
            this.menuOptionsform.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddplugin,
            this.menuRemoveplugin});
            this.menuOptionsform.Name = "menuOptionsform";
            this.menuOptionsform.Size = new System.Drawing.Size(218, 48);
            // 
            // menuAddplugin
            // 
            this.menuAddplugin.Name = "menuAddplugin";
            this.menuAddplugin.Size = new System.Drawing.Size(217, 22);
            this.menuAddplugin.Text = "Nuovo plugin";
            this.menuAddplugin.Click += new System.EventHandler(this.menuAddplugin_Click);
            // 
            // menuRemoveplugin
            // 
            this.menuRemoveplugin.Name = "menuRemoveplugin";
            this.menuRemoveplugin.Size = new System.Drawing.Size(217, 22);
            this.menuRemoveplugin.Text = "Rimuovi plugin selezionato";
            this.menuRemoveplugin.Click += new System.EventHandler(this.menuRemoveplugin_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.objlistGlobalplugins);
            this.groupBox1.Location = new System.Drawing.Point(6, 131);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1058, 194);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "plugins disponibili";
            // 
            // objlistGlobalplugins
            // 
            this.objlistGlobalplugins.AlternateRowBackColor = System.Drawing.Color.Empty;
            this.objlistGlobalplugins.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.objlistGlobalplugins.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.objlistGlobalplugins.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnLoadorder,
            this.columnName,
            this.columnTargetName,
            this.columnDesc,
            this.columnType,
            this.columnRk,
            this.columnDeps,
            this.columnOs,
            this.columnInstaller,
            this.columnApps,
            this.columnCfg,
            this.columnCoinstaller,
            this.columnExcludeall,
            this.column32only,
            this.column64only,
            this.column64needed,
            this.columnBothVersion,
            this.columnLegacy,
            this.columnNullsys});
            this.objlistGlobalplugins.ContextMenuStrip = this.menuOptionsform;
            this.objlistGlobalplugins.FullRowSelect = true;
            this.objlistGlobalplugins.GridLines = true;
            this.objlistGlobalplugins.Location = new System.Drawing.Point(11, 19);
            this.objlistGlobalplugins.Name = "objlistGlobalplugins";
            this.objlistGlobalplugins.OwnerDraw = true;
            this.objlistGlobalplugins.Size = new System.Drawing.Size(1041, 169);
            this.objlistGlobalplugins.TabIndex = 13;
            this.objlistGlobalplugins.UseAlternatingBackColors = true;
            this.objlistGlobalplugins.UseCompatibleStateImageBehavior = false;
            this.objlistGlobalplugins.View = System.Windows.Forms.View.Details;
            // 
            // columnLoadorder
            // 
            this.columnLoadorder.AspectName = null;
            this.columnLoadorder.Text = "loadorder";
            // 
            // columnName
            // 
            this.columnName.AspectName = null;
            this.columnName.Text = "nome";
            this.columnName.Width = 100;
            // 
            // columnTargetName
            // 
            this.columnTargetName.AspectName = null;
            this.columnTargetName.Text = "nome (target)";
            this.columnTargetName.Width = 100;
            // 
            // columnDesc
            // 
            this.columnDesc.AspectName = null;
            this.columnDesc.Text = "descrizione";
            this.columnDesc.Width = 100;
            // 
            // columnType
            // 
            this.columnType.AspectName = null;
            this.columnType.GroupWithItemCountFormat = "";
            this.columnType.GroupWithItemCountSingularFormat = "";
            this.columnType.Text = "tipo";
            this.columnType.Width = 100;
            // 
            // columnRk
            // 
            this.columnRk.AspectName = null;
            this.columnRk.Text = "rootkit";
            this.columnRk.Width = 100;
            // 
            // columnDeps
            // 
            this.columnDeps.AspectName = null;
            this.columnDeps.Text = "dipendenze";
            this.columnDeps.Width = 100;
            // 
            // columnOs
            // 
            this.columnOs.AspectName = null;
            this.columnOs.Text = "versione os";
            this.columnOs.Width = 100;
            // 
            // columnInstaller
            // 
            this.columnInstaller.AspectName = null;
            this.columnInstaller.Text = "installer";
            this.columnInstaller.Width = 100;
            // 
            // columnApps
            // 
            this.columnApps.AspectName = null;
            this.columnApps.Text = "inc. applicazioni";
            this.columnApps.Width = 200;
            // 
            // columnCfg
            // 
            this.columnCfg.AspectName = null;
            this.columnCfg.Text = "inc. modcfg";
            this.columnCfg.Width = 100;
            // 
            // columnCoinstaller
            // 
            this.columnCoinstaller.AspectName = null;
            this.columnCoinstaller.Text = "inc. coinstaller";
            this.columnCoinstaller.Width = 100;
            // 
            // columnExcludeall
            // 
            this.columnExcludeall.AspectName = null;
            this.columnExcludeall.Text = "esclude tutti";
            this.columnExcludeall.Width = 100;
            // 
            // column32only
            // 
            this.column32only.AspectName = null;
            this.column32only.Text = "solo 32bit";
            this.column32only.Width = 100;
            // 
            // column64only
            // 
            this.column64only.AspectName = null;
            this.column64only.Text = "solo 64bit";
            // 
            // column64needed
            // 
            this.column64needed.AspectName = null;
            this.column64needed.Text = "necessario 32/64bit";
            this.column64needed.Width = 100;
            // 
            // columnBothVersion
            // 
            this.columnBothVersion.AspectName = null;
            this.columnBothVersion.Text = "supporta wow";
            this.columnBothVersion.Width = 100;
            // 
            // columnLegacy
            // 
            this.columnLegacy.AspectName = null;
            this.columnLegacy.Text = "legacy";
            this.columnLegacy.Width = 100;
            // 
            // columnNullsys
            // 
            this.columnNullsys.AspectName = null;
            this.columnNullsys.Text = "nullsys";
            this.columnNullsys.Width = 100;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "cartella tools :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "cartella files binari :";
            // 
            // textToolspath
            // 
            this.textToolspath.Location = new System.Drawing.Point(135, 22);
            this.textToolspath.Name = "textToolspath";
            this.textToolspath.Size = new System.Drawing.Size(448, 20);
            this.textToolspath.TabIndex = 4;
            this.textToolspath.TextChanged += new System.EventHandler(this.textToolspath_TextChanged);
            // 
            // textBinpath
            // 
            this.textBinpath.Location = new System.Drawing.Point(135, 52);
            this.textBinpath.Name = "textBinpath";
            this.textBinpath.Size = new System.Drawing.Size(448, 20);
            this.textBinpath.TabIndex = 5;
            this.textBinpath.TextChanged += new System.EventHandler(this.textBinpath_TextChanged);
            // 
            // textCryptokey
            // 
            this.textCryptokey.Location = new System.Drawing.Point(135, 81);
            this.textCryptokey.Name = "textCryptokey";
            this.textCryptokey.Size = new System.Drawing.Size(448, 20);
            this.textCryptokey.TabIndex = 6;
            this.textCryptokey.TextChanged += new System.EventHandler(this.textCryptokey_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "chiave cifratura dropper :";
            // 
            // buttonBrowsetools
            // 
            this.buttonBrowsetools.Location = new System.Drawing.Point(589, 22);
            this.buttonBrowsetools.Name = "buttonBrowsetools";
            this.buttonBrowsetools.Size = new System.Drawing.Size(31, 20);
            this.buttonBrowsetools.TabIndex = 11;
            this.buttonBrowsetools.Text = "...";
            this.buttonBrowsetools.UseVisualStyleBackColor = true;
            this.buttonBrowsetools.Click += new System.EventHandler(this.buttonBrowsetools_Click);
            // 
            // buttonBrowsebin
            // 
            this.buttonBrowsebin.Location = new System.Drawing.Point(589, 52);
            this.buttonBrowsebin.Name = "buttonBrowsebin";
            this.buttonBrowsebin.Size = new System.Drawing.Size(31, 20);
            this.buttonBrowsebin.TabIndex = 12;
            this.buttonBrowsebin.Text = "...";
            this.buttonBrowsebin.UseVisualStyleBackColor = true;
            this.buttonBrowsebin.Click += new System.EventHandler(this.buttonBrowsebin_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.buttonBrowsebin);
            this.groupBox2.Controls.Add(this.textToolspath);
            this.groupBox2.Controls.Add(this.buttonBrowsetools);
            this.groupBox2.Controls.Add(this.textBinpath);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textCryptokey);
            this.groupBox2.Location = new System.Drawing.Point(6, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1058, 113);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "configurazione builder";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::rktool.Properties.Resources.virus1;
            this.pictureBox1.Location = new System.Drawing.Point(626, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(426, 96);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOk.Location = new System.Drawing.Point(6, 331);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 16;
            this.buttonOk.Text = "ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // tooltipMainCfg
            // 
            this.tooltipMainCfg.IsBalloon = true;
            this.tooltipMainCfg.ToolTipTitle = "rktool";
            // 
            // formRktoolcfg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 361);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "formRktoolcfg";
            this.Text = "configurazione";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.formRktoolcfg_Shown);
            this.menuOptionsform.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.objlistGlobalplugins)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textToolspath;
		private System.Windows.Forms.TextBox textBinpath;
		private System.Windows.Forms.TextBox textCryptokey;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonBrowsetools;
		private System.Windows.Forms.ContextMenuStrip menuOptionsform;
		private System.Windows.Forms.ToolStripMenuItem menuAddplugin;
		private System.Windows.Forms.ToolStripMenuItem menuRemoveplugin;
		private System.Windows.Forms.Button buttonBrowsebin;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button buttonOk;
		private BrightIdeasSoftware.ObjectListView objlistGlobalplugins;
		private BrightIdeasSoftware.OLVColumn columnLoadorder;
		private BrightIdeasSoftware.OLVColumn columnName;
		private BrightIdeasSoftware.OLVColumn columnDesc;
		private BrightIdeasSoftware.OLVColumn columnType;
		private BrightIdeasSoftware.OLVColumn columnRk;
		private BrightIdeasSoftware.OLVColumn columnDeps;
		private BrightIdeasSoftware.OLVColumn columnInstaller;
		private BrightIdeasSoftware.OLVColumn columnApps;
		private BrightIdeasSoftware.OLVColumn columnCfg;
		private BrightIdeasSoftware.OLVColumn columnCoinstaller;
		private BrightIdeasSoftware.OLVColumn columnExcludeall;
		private BrightIdeasSoftware.OLVColumn column32only;
		private BrightIdeasSoftware.OLVColumn column64needed;
		private BrightIdeasSoftware.OLVColumn columnLegacy;
		private BrightIdeasSoftware.OLVColumn columnNullsys;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ToolTip tooltipMainCfg;
        private BrightIdeasSoftware.OLVColumn columnTargetName;
        private BrightIdeasSoftware.OLVColumn columnBothVersion;
        private BrightIdeasSoftware.OLVColumn column64only;
        private BrightIdeasSoftware.OLVColumn columnOs;
	}
}

