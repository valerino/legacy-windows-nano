
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;

namespace rktool
{
    /// <summary>
    /// text console with logging capabilities
    /// </summary>
	public class cconsole
    {
        private RichTextBox txtbox;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="ob">a richtext box instance</param>
        /// <returns></returns>
        public cconsole (Object ob)
        {
            txtbox = (RichTextBox)ob;
        }
        
        /*
         *	log text to textbox
         *
         */
        /// <summary>
        /// log text (date - text)
        /// </summary>
        /// <param name="text">text to log</param>
        /// <returns></returns>
        public void log_out_text (string text)
        {
			txtbox.ScrollToCaret(); 
			string txt = DateTime.Now.ToString();
            txt += " - " + text + "\n";
            txtbox.AppendText(txt);
			txtbox.Refresh();
        }

		/// <summary>
		/// log text (date - text) and show messagebox
		/// </summary>
		/// <param name="text">text to log</param>
		/// <returns></returns>
		public void log_out_text_msgbox(string text)
		{
			log_out_text(text);
			MessageBox.Show(text);
		}

		/// <summary>
		/// log text, optionally showing date
		/// </summary>
		/// <param name="text">text to log</param>
		/// <param name="showdate">true to show date</param>
		/// <returns></returns>
		public void log_out_text(string text, bool showdate)
		{
			if (showdate)
			{
				log_out_text(text);
			}
			else
			{
				string txt;
				txt = text + "\n";
				txtbox.AppendText(txt);
				txtbox.Refresh();
			}
		}

		/// <summary>
		/// execute application, returning process instance
		/// </summary>
		/// <param name="cmd">application to be executed</param>
		/// <param name="arguments">parameters</param>
		/// <param name="workingdir">working directory</param>
		/// <param name="process">process instance to be returned</param>
		/// <returns>application exit code</returns>
		public int exec_cmd(string cmd, string arguments, string workingdir, out Process process)
		{
			ProcessStartInfo si = new ProcessStartInfo(cmd);
			si.UseShellExecute = false;
			si.RedirectStandardOutput = true;
			si.WindowStyle = ProcessWindowStyle.Hidden;
			si.WorkingDirectory = workingdir;
			si.Arguments = arguments;
			
			// execute and wait for termination
			try
			{
				Process p = Process.Start(si);
				p.WaitForExit();
				process = p;
			}
			catch
			{
				process = null;
				return -1;
			}

			return process.ExitCode;
		}

		/// <summary>
		/// execute application
		/// </summary>
		/// <param name="cmd">application to be executed</param>
		/// <param name="arguments">parameters</param>
		/// <param name="workingdir">working directory</param>
		/// <returns>application exit code</returns>
		public int exec_cmd(string cmd, string arguments, string workingdir)
		{
			ProcessStartInfo si = new ProcessStartInfo(cmd);
			si.UseShellExecute = false;
			si.RedirectStandardOutput = true;
			si.WindowStyle = ProcessWindowStyle.Hidden;
			si.WorkingDirectory = workingdir;
			si.Arguments = arguments;
			int res = -1;

			// execute and wait for termination
			try
			{
				Process p = Process.Start(si);
				p.WaitForExit();
				res = p.ExitCode;
			}
			catch
			{
				return -1;
			}

			return res;
		}

		/// <summary>
		/// execute application logging output
		/// </summary>
		/// <param name="cmd">application to be executed</param>
		/// <param name="arguments">parameters</param>
		/// <param name="workingdir">working directory</param>
		/// <param name="logoutput">true to log output</param>
		/// <returns>application exit code</returns>
		public int exec_cmd(string cmd, string arguments, string workingdir, bool logoutput)
		{
			// execute process
			Process p = null;
			int res = exec_cmd(cmd, arguments, workingdir, out p);
			if (p == null)
				return -1;

			if (logoutput)
			{
				// log output
				string output = p.StandardOutput.ReadToEnd();
				log_out_text(output);
			}
			return res;
		}

    }
}
