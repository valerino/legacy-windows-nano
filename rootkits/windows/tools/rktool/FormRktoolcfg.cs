using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using BrightIdeasSoftware;

namespace rktool
{
	public partial class formRktoolcfg : Form
	{
		private cconsole console;
		private formMain mainform;

		public formRktoolcfg(formMain f, cconsole c)
		{
			InitializeComponent();
			
			// get console instance and mainform
			console = c;
			mainform = f;

			// initialize tooltips
			initialize_tooltips();

			// initialize listview
			initialize_listview();
		}

		/// <summary>
		/// set tooltips to controls in this form
		/// </summary>
		/// <returns></returns>
		private void initialize_tooltips()
		{
			tooltipMainCfg.SetToolTip(textBinpath, "path della cartella base contenente i files binari dei vari componenti dei rootkits (ex. z:\\deploy).\n\nselezionare il menu 'help' per ulteriori informazioni a riguardo");
			tooltipMainCfg.SetToolTip(textToolspath, "path della cartella contenente i tools esterni necessari per la generazione del dropper (ex. z:\\shared\\bin\\win).\n\nselezionare il menu 'help' per ulteriori informazioni a riguardo");
			tooltipMainCfg.SetToolTip(textCryptokey, "chiave di cifratura AES da usare per le risorse iniettate nel dropper.\n\nla chiave di cifratura deve avere formato di stringa esadecimale e ampiezza 16 (128bit),32(256bit),64 (512bit) caratteri)");
			tooltipMainCfg.SetToolTip(objlistGlobalplugins, "inserire qui i componenti da rendere disponibili a rktool per la generazione del dropper.\n\nclickando con il bottone destro � possibile aggiungere nuovi componenti o rimuovere il componente selezionato.\n(si consiglia di mantenere le impostazioni di default)");
		}

		private void textCryptokey_TextChanged(object sender, EventArgs e)
		{
			cglobalcfg.Instance.cryptokey = ((TextBox)sender).Text;
		}

		private void textBinpath_TextChanged(object sender, EventArgs e)
		{
			cglobalcfg.Instance.path_bin = ((TextBox)sender).Text;
		}

		/// <summary>
		/// add plugin to listview
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void menuAddplugin_Click(object sender, EventArgs e)
		{
			try
			{
				// get the selected plugin
				cplugincfg plugin = (cplugincfg)objlistGlobalplugins.GetSelectedObject();
				
				// copy to a new one and add
				cplugincfg newplugin = new cplugincfg(plugin);
				cglobalcfg.Instance.listplugins.Add(newplugin);

				// update form
				this.refresh_form();
			}
			catch
			{

			}
		}

		/// <summary>
		/// accept and save configuration
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void buttonOk_Click(object sender, EventArgs e)
		{			
			// save cfg
			if (cglobalcfg.Instance.to_file() == 0)
			{
				console.log_out_text_msgbox("configurazione salvata correttamente");
			}
			else
			{
				console.log_out_text_msgbox("errore salvataggio configurazione");
			}
			this.Close();
		}

		private void textToolspath_TextChanged(object sender, EventArgs e)
		{
			cglobalcfg.Instance.path_tools = ((TextBox)sender).Text;
		}

		private void buttonBrowsetools_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog browsedlg = new FolderBrowserDialog();
			browsedlg.ShowNewFolderButton = false;
			DialogResult res = browsedlg.ShowDialog();
			if (res == DialogResult.OK)
			{
				textToolspath.Text = browsedlg.SelectedPath;
			}
		}

		private void buttonBrowsebin_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog browsedlg = new FolderBrowserDialog();
			browsedlg.ShowNewFolderButton = false;
			DialogResult res = browsedlg.ShowDialog();
			if (res == DialogResult.OK)
			{
				textBinpath.Text = browsedlg.SelectedPath;
			}
		}

		/// <summary>
		/// initialize listview
		/// </summary>
		/// <returns></returns>
		private void initialize_listview ()
		{			
			// loadorder
			this.columnLoadorder.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_loadorder; };
			this.columnLoadorder.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_loadorder = (int)newValue; };
			
			// name
			this.columnName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_name; };
			this.columnName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_name = (string)newValue; };

            // targetname
            this.columnTargetName.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_targetname; };
            this.columnTargetName.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_targetname = (string)newValue; };
			
            // desc
			this.columnDesc.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_desc; };
			this.columnDesc.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_desc = (string)newValue; };

			// type
			this.columnType.AspectGetter = delegate(object row) { return (cplugincfg.get_string_from_type(((cplugincfg)row).m_type)); };
			this.columnType.AspectPutter = delegate(object x, object newValue) 
			{ 
				try
				{
					((cplugincfg)x).m_type = cplugincfg.get_type_from_string((string)newValue); 
				}
				catch
				{
					MessageBox.Show("valori possibili : kernelmode,usermode,dropper");
				}
			};
	
			// rootkit
			this.columnRk.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_rk; };
			this.columnRk.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_rk = (string)newValue; };

			// deps
			this.columnDeps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_deps; };
			this.columnDeps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_deps = (string)newValue; };

			// installer
			this.columnInstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_installer; };
			this.columnInstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_installer = (string)newValue; };

			// apps
			this.columnApps.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_apps; };
			this.columnApps.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_apps = (string)newValue; };

			// modcfg
			this.columnCfg.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnCfg.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_cfg; };
			this.columnCfg.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_cfg = (bool)newValue; };

			// coinstaller
			this.columnCoinstaller.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnCoinstaller.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_coinstaller; };
			this.columnCoinstaller.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_coinstaller = (bool)newValue; };

			// excludeall
			this.columnExcludeall.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnExcludeall.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_excludeall; };
			this.columnExcludeall.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_excludeall = (bool)newValue; };

			// 32bitonly
			this.column32only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column32only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only32bit; };
			this.column32only.AspectPutter = delegate(object x, object newValue)
			{
				((cplugincfg)x).m_only32bit = (bool)newValue;
				((cplugincfg)x).m_only64bit = false;
				((cplugincfg)x).m_include3264bit = false;
				((cplugincfg)x).m_needed3264bit = false;
			};

			// 64bitonly
			this.column64only.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column64only.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_only64bit; };
			this.column64only.AspectPutter = delegate(object x, object newValue)
			{
				((cplugincfg)x).m_only64bit = (bool)newValue;
				((cplugincfg)x).m_only32bit = false;
			};

			// 64bitneeded
			this.column64needed.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.column64needed.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_needed3264bit; };
			this.column64needed.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_needed3264bit = (bool)newValue; };

            // include32/64
            this.columnBothVersion.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
            this.columnBothVersion.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_include3264bit; };
            this.columnBothVersion.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_include3264bit = (bool)newValue; };

            // legacy
			this.columnLegacy.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnLegacy.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_legacy; };
			this.columnLegacy.AspectPutter = delegate(object x, object newValue)
			{
				((cplugincfg)x).m_legacy = (bool)newValue;
				if (((cplugincfg)x).m_nullsys == true)
				{
					// force selected if nullsys is selected
					((cplugincfg)x).m_legacy = (bool)true;
				}
			};

			// nullsys
			this.columnNullsys.Renderer = new MappedImageRenderer(new Object[] { false, resources.error, true, resources.ok });
			this.columnNullsys.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_nullsys; };
			this.columnNullsys.AspectPutter = delegate(object x, object newValue) 
			{ 
				((cplugincfg)x).m_nullsys = (bool)newValue;
				if ((bool)newValue == true)
				{
					// select legacy too
					((cplugincfg)x).m_legacy = (bool)newValue;
				}
			};

            /// osversion
            this.columnOs.AspectGetter = delegate(object x) { return ((cplugincfg)x).m_osversion; };
            this.columnOs.AspectPutter = delegate(object x, object newValue) { ((cplugincfg)x).m_osversion = (string)newValue; };

		}
		
		/// <summary>
		/// refresh form
		/// </summary>
		/// <returns></returns>
		private void refresh_form ()
		{
			// fill listview
			cplugincfg.sort_plugins_with_loadorder(cglobalcfg.Instance.listplugins);
			objlistGlobalplugins.SecondarySortColumn = columnLoadorder;
			objlistGlobalplugins.SortGroupItemsByPrimaryColumn = false;
			objlistGlobalplugins.SetObjects(cglobalcfg.Instance.listplugins);
			objlistGlobalplugins.Sort(columnRk);
			
			// fill other form components
			textToolspath.Text = cglobalcfg.Instance.path_tools;
			textBinpath.Text = cglobalcfg.Instance.path_bin;
			textCryptokey.Text = cglobalcfg.Instance.cryptokey;
		}

		private void formRktoolcfg_Shown(object sender, EventArgs e)
		{
			// load configuration from file
			try
			{
				cglobalcfg.Instance.from_file();
			}
			catch (System.Exception ex)
			{
				MessageBox.Show("Impossibile caricare la configurazione\n" + ex.ToString());
			}
			
			// init components
			refresh_form();
		}

		/// <summary>
		/// remove plugin from listview, update global list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		private void menuRemoveplugin_Click(object sender, EventArgs e)
		{
			try
			{
				// get and remove the selected plugin
				cplugincfg plugin = (cplugincfg)objlistGlobalplugins.GetSelectedObject();
				cplugincfg.remove_plugin(cglobalcfg.Instance.listplugins, plugin);

				// update form
				this.refresh_form();
			}
			catch
			{
				
			}
		}
	}
}