namespace rktool
{
	partial class formMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
			this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
			this.menuNewprj = new System.Windows.Forms.ToolStripMenuItem();
			this.menuOpenprj = new System.Windows.Forms.ToolStripMenuItem();
			this.generaInstallerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuConfig = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textLog = new System.Windows.Forms.RichTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.menuMain = new System.Windows.Forms.MenuStrip();
			this.buttonClearLog = new System.Windows.Forms.Button();
			this.tooltipMain = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1.SuspendLayout();
			this.menuMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuFile
			// 
			this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNewprj,
            this.menuOpenprj,
            this.generaInstallerToolStripMenuItem,
            this.menuConfig,
            this.helpToolStripMenuItem,
            this.menuExit});
			this.menuFile.Name = "menuFile";
			this.menuFile.Size = new System.Drawing.Size(37, 20);
			this.menuFile.Text = "File";
			// 
			// menuNewprj
			// 
			this.menuNewprj.Name = "menuNewprj";
			this.menuNewprj.Size = new System.Drawing.Size(157, 22);
			this.menuNewprj.Text = "nuovo progetto";
			this.menuNewprj.Click += new System.EventHandler(this.menuNewproj_Click);
			// 
			// menuOpenprj
			// 
			this.menuOpenprj.Name = "menuOpenprj";
			this.menuOpenprj.Size = new System.Drawing.Size(157, 22);
			this.menuOpenprj.Text = "apri progetto";
			this.menuOpenprj.Click += new System.EventHandler(this.menuOpenproj_Click);
			// 
			// generaInstallerToolStripMenuItem
			// 
			this.generaInstallerToolStripMenuItem.Name = "generaInstallerToolStripMenuItem";
			this.generaInstallerToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
			this.generaInstallerToolStripMenuItem.Text = "genera installer";
			this.generaInstallerToolStripMenuItem.Click += new System.EventHandler(this.generaInstallerToolStripMenuItem_Click);
			// 
			// menuConfig
			// 
			this.menuConfig.Name = "menuConfig";
			this.menuConfig.Size = new System.Drawing.Size(157, 22);
			this.menuConfig.Text = "configurazione";
			this.menuConfig.Click += new System.EventHandler(this.menuConfig_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
			this.helpToolStripMenuItem.Text = "help";
			this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
			// 
			// menuExit
			// 
			this.menuExit.Name = "menuExit";
			this.menuExit.Size = new System.Drawing.Size(157, 22);
			this.menuExit.Text = "esci";
			this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.textLog);
			this.groupBox1.Location = new System.Drawing.Point(12, 27);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(902, 226);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Log";
			// 
			// textLog
			// 
			this.textLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.textLog.Location = new System.Drawing.Point(6, 19);
			this.textLog.Name = "textLog";
			this.textLog.Size = new System.Drawing.Size(890, 201);
			this.textLog.TabIndex = 0;
			this.textLog.Text = "";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(696, 256);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(218, 76);
			this.label1.TabIndex = 2;
			this.label1.Text = "rkTool";
			// 
			// menuMain
			// 
			this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile});
			this.menuMain.Location = new System.Drawing.Point(0, 0);
			this.menuMain.Name = "menuMain";
			this.menuMain.Size = new System.Drawing.Size(926, 24);
			this.menuMain.TabIndex = 0;
			this.menuMain.Text = "menuMain";
			// 
			// buttonClearLog
			// 
			this.buttonClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonClearLog.Location = new System.Drawing.Point(12, 256);
			this.buttonClearLog.Name = "buttonClearLog";
			this.buttonClearLog.Size = new System.Drawing.Size(75, 23);
			this.buttonClearLog.TabIndex = 3;
			this.buttonClearLog.Text = "pulisci log";
			this.buttonClearLog.UseVisualStyleBackColor = true;
			this.buttonClearLog.Click += new System.EventHandler(this.buttonClearLog_Click);
			// 
			// tooltipMain
			// 
			this.tooltipMain.IsBalloon = true;
			this.tooltipMain.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.tooltipMain.ToolTipTitle = "rktool";
			// 
			// formMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(926, 338);
			this.Controls.Add(this.buttonClearLog);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.menuMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuMain;
			this.Name = "formMain";
			this.Text = "rktool";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.formMain_Load);
			this.groupBox1.ResumeLayout(false);
			this.menuMain.ResumeLayout(false);
			this.menuMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStripMenuItem menuFile;
		private System.Windows.Forms.ToolStripMenuItem menuNewprj;
		private System.Windows.Forms.ToolStripMenuItem menuOpenprj;
		private System.Windows.Forms.ToolStripMenuItem menuExit;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RichTextBox textLog;
		private System.Windows.Forms.ToolStripMenuItem menuConfig;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.MenuStrip menuMain;
		private System.Windows.Forms.Button buttonClearLog;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolTip tooltipMain;
		private System.Windows.Forms.ToolStripMenuItem generaInstallerToolStripMenuItem;
	}
}