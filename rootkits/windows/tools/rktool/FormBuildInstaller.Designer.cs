﻿namespace rktool
{
	partial class FormBuildInstaller
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuildInstaller));
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.objlistComponents = new BrightIdeasSoftware.ObjectListView();
			this.columnSelect = new BrightIdeasSoftware.OLVColumn();
			this.columnLoadorder = new BrightIdeasSoftware.OLVColumn();
			this.columnName = new BrightIdeasSoftware.OLVColumn();
			this.columnTargetName = new BrightIdeasSoftware.OLVColumn();
			this.columnDesc = new BrightIdeasSoftware.OLVColumn();
			this.columnType = new BrightIdeasSoftware.OLVColumn();
			this.columnRk = new BrightIdeasSoftware.OLVColumn();
			this.columnDeps = new BrightIdeasSoftware.OLVColumn();
			this.columnInstaller = new BrightIdeasSoftware.OLVColumn();
			this.columnApps = new BrightIdeasSoftware.OLVColumn();
			this.columnCfg = new BrightIdeasSoftware.OLVColumn();
			this.columnCoinstaller = new BrightIdeasSoftware.OLVColumn();
			this.columnExcludeall = new BrightIdeasSoftware.OLVColumn();
			this.column32only = new BrightIdeasSoftware.OLVColumn();
			this.column64only = new BrightIdeasSoftware.OLVColumn();
			this.column64needed = new BrightIdeasSoftware.OLVColumn();
			this.columnBothVersion = new BrightIdeasSoftware.OLVColumn();
			this.columnLegacy = new BrightIdeasSoftware.OLVColumn();
			this.columnNullsys = new BrightIdeasSoftware.OLVColumn();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonBuildInstaller = new System.Windows.Forms.Button();
			this.check64bit = new System.Windows.Forms.CheckBox();
			this.tooltipBuildInstaller = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox10.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.objlistComponents)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox10
			// 
			this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox10.Controls.Add(this.objlistComponents);
			this.groupBox10.Location = new System.Drawing.Point(12, 12);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(1044, 207);
			this.groupBox10.TabIndex = 28;
			this.groupBox10.TabStop = false;
			this.groupBox10.Tag = "";
			this.groupBox10.Text = "componenti";
			// 
			// objlistComponents
			// 
			this.objlistComponents.AlternateRowBackColor = System.Drawing.Color.Empty;
			this.objlistComponents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.objlistComponents.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
			this.objlistComponents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnSelect,
            this.columnLoadorder,
            this.columnName,
            this.columnTargetName,
            this.columnDesc,
            this.columnType,
            this.columnRk,
            this.columnDeps,
            this.columnInstaller,
            this.columnApps,
            this.columnCfg,
            this.columnCoinstaller,
            this.columnExcludeall,
            this.column32only,
            this.column64only,
            this.column64needed,
            this.columnBothVersion,
            this.columnLegacy,
            this.columnNullsys});
			this.objlistComponents.FullRowSelect = true;
			this.objlistComponents.GridLines = true;
			this.objlistComponents.Location = new System.Drawing.Point(6, 19);
			this.objlistComponents.Name = "objlistComponents";
			this.objlistComponents.OwnerDraw = true;
			this.objlistComponents.Size = new System.Drawing.Size(1032, 182);
			this.objlistComponents.TabIndex = 25;
			this.objlistComponents.UseAlternatingBackColors = true;
			this.objlistComponents.UseCompatibleStateImageBehavior = false;
			this.objlistComponents.View = System.Windows.Forms.View.Details;
			// 
			// columnSelect
			// 
			this.columnSelect.AspectName = null;
			this.columnSelect.Text = "selezionato";
			this.columnSelect.Width = 80;
			// 
			// columnLoadorder
			// 
			this.columnLoadorder.AspectName = null;
			this.columnLoadorder.Text = "loadorder";
			this.columnLoadorder.Width = 65;
			// 
			// columnName
			// 
			this.columnName.AspectName = null;
			this.columnName.IsEditable = false;
			this.columnName.Text = "nome";
			this.columnName.Width = 100;
			// 
			// columnTargetName
			// 
			this.columnTargetName.AspectName = null;
			this.columnTargetName.Text = "nome (target)";
			this.columnTargetName.Width = 100;
			// 
			// columnDesc
			// 
			this.columnDesc.AspectName = null;
			this.columnDesc.IsEditable = false;
			this.columnDesc.Text = "descrizione";
			this.columnDesc.Width = 100;
			// 
			// columnType
			// 
			this.columnType.AspectName = null;
			this.columnType.GroupWithItemCountFormat = "";
			this.columnType.GroupWithItemCountSingularFormat = "";
			this.columnType.IsEditable = false;
			this.columnType.Text = "tipo";
			this.columnType.Width = 100;
			// 
			// columnRk
			// 
			this.columnRk.AspectName = null;
			this.columnRk.IsEditable = false;
			this.columnRk.Text = "rootkit";
			this.columnRk.Width = 100;
			// 
			// columnDeps
			// 
			this.columnDeps.AspectName = null;
			this.columnDeps.IsEditable = false;
			this.columnDeps.Text = "dipendenze";
			this.columnDeps.Width = 100;
			// 
			// columnInstaller
			// 
			this.columnInstaller.AspectName = null;
			this.columnInstaller.IsEditable = false;
			this.columnInstaller.Text = "installer";
			this.columnInstaller.Width = 100;
			// 
			// columnApps
			// 
			this.columnApps.AspectName = null;
			this.columnApps.Text = "inc. applicazioni";
			this.columnApps.Width = 200;
			// 
			// columnCfg
			// 
			this.columnCfg.AspectName = null;
			this.columnCfg.IsEditable = false;
			this.columnCfg.Text = "inc. modcfg";
			this.columnCfg.Width = 100;
			// 
			// columnCoinstaller
			// 
			this.columnCoinstaller.AspectName = null;
			this.columnCoinstaller.IsEditable = false;
			this.columnCoinstaller.Text = "inc. coinstaller";
			this.columnCoinstaller.Width = 100;
			// 
			// columnExcludeall
			// 
			this.columnExcludeall.AspectName = null;
			this.columnExcludeall.IsEditable = false;
			this.columnExcludeall.Text = "esclude tutti";
			this.columnExcludeall.Width = 100;
			// 
			// column32only
			// 
			this.column32only.AspectName = null;
			this.column32only.IsEditable = false;
			this.column32only.Text = "solo 32bit";
			this.column32only.Width = 100;
			// 
			// column64only
			// 
			this.column64only.AspectName = null;
			this.column64only.IsEditable = false;
			this.column64only.Text = "solo 64bit";
			this.column64only.Width = 100;
			// 
			// column64needed
			// 
			this.column64needed.AspectName = null;
			this.column64needed.IsEditable = false;
			this.column64needed.Text = "necessario 32/64bit";
			this.column64needed.Width = 100;
			// 
			// columnBothVersion
			// 
			this.columnBothVersion.AspectName = null;
			this.columnBothVersion.IsEditable = false;
			this.columnBothVersion.Text = "supporta wow";
			this.columnBothVersion.Width = 100;
			// 
			// columnLegacy
			// 
			this.columnLegacy.AspectName = null;
			this.columnLegacy.IsEditable = false;
			this.columnLegacy.Text = "legacy";
			this.columnLegacy.Width = 100;
			// 
			// columnNullsys
			// 
			this.columnNullsys.AspectName = null;
			this.columnNullsys.IsEditable = false;
			this.columnNullsys.Text = "nullsys";
			this.columnNullsys.Width = 100;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 232);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(323, 13);
			this.label1.TabIndex = 29;
			this.label1.Text = "selezionare i componenti per i quali si intende generare gli installers.";
			// 
			// buttonBuildInstaller
			// 
			this.buttonBuildInstaller.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBuildInstaller.Location = new System.Drawing.Point(828, 222);
			this.buttonBuildInstaller.Name = "buttonBuildInstaller";
			this.buttonBuildInstaller.Size = new System.Drawing.Size(228, 30);
			this.buttonBuildInstaller.TabIndex = 30;
			this.buttonBuildInstaller.Text = "genera installer";
			this.buttonBuildInstaller.UseVisualStyleBackColor = true;
			this.buttonBuildInstaller.Click += new System.EventHandler(this.buttonBuildInstaller_Click);
			// 
			// check64bit
			// 
			this.check64bit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.check64bit.AutoSize = true;
			this.check64bit.Location = new System.Drawing.Point(750, 230);
			this.check64bit.Name = "check64bit";
			this.check64bit.Size = new System.Drawing.Size(49, 17);
			this.check64bit.TabIndex = 31;
			this.check64bit.Text = "64bit";
			this.check64bit.UseVisualStyleBackColor = true;
			// 
			// tooltipBuildInstaller
			// 
			this.tooltipBuildInstaller.IsBalloon = true;
			this.tooltipBuildInstaller.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.tooltipBuildInstaller.ToolTipTitle = "rktool";
			// 
			// FormBuildInstaller
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1068, 264);
			this.Controls.Add(this.check64bit);
			this.Controls.Add(this.buttonBuildInstaller);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox10);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormBuildInstaller";
			this.Text = "generazione installer";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.groupBox10.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.objlistComponents)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox10;
		private BrightIdeasSoftware.ObjectListView objlistComponents;
		private BrightIdeasSoftware.OLVColumn columnSelect;
		private BrightIdeasSoftware.OLVColumn columnLoadorder;
		private BrightIdeasSoftware.OLVColumn columnName;
		private BrightIdeasSoftware.OLVColumn columnTargetName;
		private BrightIdeasSoftware.OLVColumn columnDesc;
		private BrightIdeasSoftware.OLVColumn columnType;
		private BrightIdeasSoftware.OLVColumn columnRk;
		private BrightIdeasSoftware.OLVColumn columnDeps;
		private BrightIdeasSoftware.OLVColumn columnInstaller;
		private BrightIdeasSoftware.OLVColumn columnApps;
		private BrightIdeasSoftware.OLVColumn columnCfg;
		private BrightIdeasSoftware.OLVColumn columnCoinstaller;
		private BrightIdeasSoftware.OLVColumn columnExcludeall;
		private BrightIdeasSoftware.OLVColumn column32only;
		private BrightIdeasSoftware.OLVColumn column64only;
		private BrightIdeasSoftware.OLVColumn column64needed;
		private BrightIdeasSoftware.OLVColumn columnBothVersion;
		private BrightIdeasSoftware.OLVColumn columnLegacy;
		private BrightIdeasSoftware.OLVColumn columnNullsys;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonBuildInstaller;
		private System.Windows.Forms.CheckBox check64bit;
		private System.Windows.Forms.ToolTip tooltipBuildInstaller;
	}
}