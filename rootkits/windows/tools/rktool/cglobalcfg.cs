using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace rktool
{
	[XmlRootAttribute(ElementName = "rktool_cfg", IsNullable = false)]
	public sealed class cglobalcfg
	{
		private static volatile cglobalcfg instance;
		private static object syncRoot = new Object();
		private ArrayList m_listplugins;

		private cglobalcfg()
		{
			listplugins = new ArrayList();
		}

		public static cglobalcfg Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new cglobalcfg();
					}
				}
				return instance;
			}
		}

		private string m_toolspath;
		private string m_binpath;
		private string m_cryptokey;

		public ArrayList listplugins
		{
			get { return m_listplugins; }
			set { m_listplugins = value; }
		}

		public string path_tools
		{
			get { return m_toolspath; }
			set { m_toolspath = value; }
		}

		public string path_bin
		{
			get { return m_binpath; }
			set { m_binpath = value; }
		}

		public string cryptokey
		{
			get { return m_cryptokey; }
			set { m_cryptokey = value; }
		}
		
		public const string cfgfilename = "rktool_cfg.xml";

		/// <summary>
		/// clear configuration
		/// </summary>
		/// <returns></returns>
		public void clear ()
		{
			m_listplugins.Clear();
			m_cryptokey = "";
			m_binpath = "";
			m_toolspath = "";
		}

		/// <summary>
		/// serialize cfg to file
		/// </summary>
		/// <returns>0 on success</returns>
		public int to_file()
		{
			try
			{
				string cfgpath = Directory.GetCurrentDirectory() + "\\" + cfgfilename;
				XmlSerializer s = new XmlSerializer(typeof(cglobalcfg));
				TextWriter w = new StreamWriter(cfgpath);
				s.Serialize(w, Instance);
				w.Close();
			}
			catch
			{
				return -1;
			}

			return 0;
		}

		/// <summary>
		/// de-serialize cfg from file
		/// </summary>
		/// <returns>0 on success</returns>
		public int from_file()
		{
			try
			{
				string cfgpath = Directory.GetCurrentDirectory() + "\\" + cfgfilename;
				XmlSerializer s = new XmlSerializer(typeof(cglobalcfg));
				TextReader r = new StreamReader(cfgpath);
				instance = (cglobalcfg)s.Deserialize(r);
				r.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return -1;
			}
			return 0;
		}

		[XmlElement("plugin")]
		public cplugincfg[] Items
		{
			get
			{
				cplugincfg[] items = new cplugincfg[m_listplugins.Count];
				m_listplugins.CopyTo(items);
				return items;
			}
			set
			{
				if (value == null)
					return;
				cplugincfg[] items = (cplugincfg[])value;
				m_listplugins.Clear();
				foreach (cplugincfg item in items)
					m_listplugins.Add(item);
			}
		}
	}
}
