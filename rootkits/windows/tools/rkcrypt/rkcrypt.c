/*
 *	this tool is a simple file crypter using the rootkits resource key. it can be used to encrypt files which needs to be read crypted
 *  by the various rootkits (i.e. wmmicro reads the encrypted configuration directly from disk, it isnt embedded in the executable)
 *
 *  -vx-
 */
#include <winsock2.h>
#include <stdio.h>
#include <modrkcore.h>

#ifdef _USE_AES_RC6
#include <aes.h>
#endif
#ifdef _USE_AES_TWOFISH
#include <aes_2fish.h>
#endif

void printusage (char* name)
{
	printf ("usage : %s infilename outfilename -t\n",name);
}

int main (int argc, char** argv)
{
	int res = -1;
	unsigned char* buf = NULL;
	int size = 0;
	int ciphersize = 0;
	keyInstance ki;
	cipherInstance ci;
	FILE* fin = NULL;
	FILE* fout = NULL;
	CHAR asciikey [128] = {0};
	int i = 0;
	int j = 0;
	unsigned char* cipherkey = DROPPER_RSRC_CYPHERKEY;

	if (argc < 3)
	{
		printusage(argv[0]);
		goto __exit;
	}
	
	// open input file
	fin = fopen (argv[1],"rb");
	if (!fin)
	{
		printf (". error : cannot open %s for crypting\n", argv[1]);
		goto __exit;
	}
	fseek (fin,0,SEEK_END);
	size = ftell(fin);
	rewind (fin);
	
	// allocate a 16byte aligned memory block and read the file in
	ciphersize = size;
	while (ciphersize % 16)
		ciphersize++;
	buf = calloc (1,ciphersize + 1);
	if (!buf)
	{
		printf (". error : cannot allocate memory for crypting\n");
		goto __exit;
	}
	if (fread (buf,size,1,fin) != 1)
	{
		printf (". error : cannot read %s for crypting\n", argv[1]);
		goto __exit;
	}

	// encrypt
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf (&asciikey[j],"%.2x",(unsigned char)cipherkey[i]);
		j+=2;
	}
	if (makeKey(&ki,DIR_ENCRYPT,DROPPER_RSRC_CYPHERKEY_BITS,asciikey) != TRUE)
	{
		printf (". error : makeKey\n");
		goto __exit;
	}
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
	{
		printf (". error : cipherInit\n");
		goto __exit;
	}
	if (blockEncrypt(&ci,&ki,buf,ciphersize*8,buf) != ciphersize*8)
	{
		printf (". error : blockEncrypt\n");
		goto __exit;
	}

	// write back outfile
	fout = fopen (argv[2],"wb");
	if (!fout)
	{
		printf (". error : cannot open %s for writing\n", argv[2]);
		goto __exit;
	}
	if (fwrite (buf,ciphersize,1,fout) != 1)
	{
		printf (". error : cannot write %s\n", argv[2]);
		goto __exit;
	}
	
	res = 0;
	printf (". succesfully crypted %s to %s with cypherkey %s\n", argv[1],argv[2], asciikey);

	if (argc == 4)
	{
		if (_stricmp(argv[3],"-t") == 0)
		{
			// test decryption
			if (makeKey(&ki,DIR_DECRYPT,DROPPER_RSRC_CYPHERKEY_BITS,asciikey) != TRUE)
			{
				printf (". error : makeKey\n");
				goto __exit;
			}
			if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
			{
				printf (". error : cipherInit\n");
				goto __exit;
			}
			if (blockDecrypt(&ci,&ki,buf,ciphersize*8,buf) != ciphersize*8)
			{
				printf (". error : blockEncrypt\n");
				goto __exit;
			}
			printf (". showing decrypted content after encryption : \n");
			printf (buf);
		}
	}

__exit:
	if (fin)
		fclose(fin);
	if (fout)
		fclose (fout);
	if (buf)
		free (buf);
	return res;
}