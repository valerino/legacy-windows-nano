#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wdfinstaller.h>

/*
*	generate dummy inf on the fly for kmdf install
*
*/
int kmdf_generate_inf(char* infpath, char* servicename) 
{
	BOOLEAN ret = TRUE;
	CHAR buf [1024];
	int res = -1;
	FILE* f = NULL;

	/// create file
	f = fopen (infpath,"wb");
	if (!f)
		goto __exit;

	/// write
	memset (buf,0,sizeof (buf));
	sprintf_s(buf, sizeof (buf), 
		"[Version]\r\n"								\
		"signature = \"$Windows NT$\"\r\n"			\
		"\r\n"										\
		"[%s.NT.Wdf]\r\n"							\
		"KmdfService = %s, wdfsect\r\n"				\
		"[wdfsect]\r\n"								\
		"KmdfLibraryVersion = 1.0\r\n", 
		servicename, servicename);
	if (fwrite (buf,strlen(buf),1,f) != 1)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (f)
		fclose (f);
	return res;
}

int main (int argc, char** argv)
{
	WCHAR path [MAX_PATH] = {0};
	WCHAR temp [MAX_PATH] = {0};
	WCHAR current [MAX_PATH] = {0};
	HMODULE mod = NULL;
	ULONG_PTR preinst = 0;
	PFN_WDFPREDEVICEINSTALL  WdfPreDeviceInstallPtr = NULL;
	int res = -1;

	/// load coinstaller and get pointers
	printf ("WDF coinstaller installer\n\n");
	mod = LoadLibrary ("wdfcoinstaller.dll");
	if (!mod)
	{
		printf (". ERROR : can't find wdfcoinstaller.dll!\n");
		return -1;
	}

	preinst = (ULONG_PTR)GetProcAddress (mod,"WdfPreDeviceInstall");
	if (!preinst)
		return -1;
	WdfPreDeviceInstallPtr = (PFN_WDFPREDEVICEINSTALL)preinst;

	/// generate inf
	if (kmdf_generate_inf ("wdfinf.inf","test") != 0)
		return -1;

	/// call the coinstaller for pre-install
	GetCurrentDirectoryW (MAX_PATH,current);
	swprintf(path, MAX_PATH, L"%s\\wdfinf.inf",current);
	swprintf(temp, MAX_PATH, L"%s.NT.Wdf", L"test");
	WdfPreDeviceInstallPtr(path, temp);

	DeleteFile ("wdfinf.inf");
	printf (". Reboot is required to complete the installation!\n");
	FreeLibrary(mod);
	return 0;
}