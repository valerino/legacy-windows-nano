#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cfghandle.h"

int cfg_linelen (PCHAR pLine)
{
	PCHAR p = NULL;
	int len = 0;

	// check params;
	if (!pLine)
		goto __exit;

	// calculate size until EOL
	p=pLine;
	while (*p)
	{
		if (*p == '\0' || *p==0x0d || *p==0x0a)
			break;

		// next char
		p++;
		len++;
	}

__exit:
	return len;
}

PCHAR cfg_getsubvalue (PCHAR pBuffer)
{
	PCHAR p = NULL;

	// check params;
	if (!pBuffer)
		return NULL;

	// get to subvalue
	p = pBuffer + strlen (pBuffer) + 1;

	return p;
}

PCHAR cfg_getcontent (PCHAR pBuffer, PCHAR pDest, int Size)
{
	PCHAR p = NULL;
	PCHAR q = NULL;
	int len = 0;

	// check params
	if (!pBuffer || !pDest || !Size)
		return NULL;

	// get to content
	p = strchr (pBuffer,(int)'=');
	if (!p)
		return NULL;
	p++;

	// skip spaces if any
	while (*p == ' ')
		p++; 

	// check size
	len = cfg_linelen (p);
	if (len + 2 > Size)
		return NULL;

	// copy string
	memset (pDest,0,len + 2);
	memcpy (pDest,p,len);

	// scan output string and subdivide in substrings
	q = pDest;
	while (*q)
	{
		if (*q == ',')
			*q = '\0';
		// next char
		q++;
	}

	return p;
}

int cfg_comparekey (PCHAR pBuffer, PCHAR pkeyname)
{
	PCHAR s = pBuffer;
	PCHAR p = pkeyname;
	int matched = 0;
	int keylen = 0;

	// check params
	if (!pBuffer || !pkeyname)
		return 0;
	keylen = (int)strlen (pkeyname);

	// loop while '=' found
	while (*s != '=')
	{
		// EOL ?
		if (!*s || !*p)
			break;

		// skip spaces
		if (*s == ' ')
			goto __nextchar;

		// char matches ?
		if (*s != *p)
			break;

		// char matched
		matched++;

__nextchar:
		// next char
		s++;p++;
	}

	// matched ?
	if (matched == keylen)
		return 1;

	return 0;
}

int cfg_getkey (PCHAR pBuffer, PCHAR pKeyName, int keynamelen)
{
	PCHAR s = pBuffer;
	PCHAR p = pKeyName;
	int count = 0;
	int keylen = 0;

	// check params
	if (!pBuffer || !pKeyName || !keynamelen)
		return 0;
	memset (pKeyName,0,keynamelen);

	// loop while '=' found
	while (*s != '=')
	{
		// EOL ?
		if (!*s)
			break;
		if (*s == ' ')
			goto __nextchar;

		*p=*s;
__nextchar:
		p++;s++;
		count++;
		if (count == keynamelen)
			break;
	}
	return count;
}

void cfg_xor (unsigned char* pBuffer, unsigned long size, unsigned char seed)
{
	// check params
	if (!pBuffer || !size)
		return;

	// encrypt/decrypt buffer byte by byte
	while (size > 0)
	{
		size--; 
		*pBuffer = *pBuffer ^ seed;
		pBuffer++;
	}
}

/*
 *	free cfg list
 *
 */
void cfg_free (PLIST_ENTRY cfg)
{
	void* p;

	while (!IsListEmpty(cfg))
	{
		p = RemoveHeadList (cfg);
		if (p)
			free (p);
	}
}

PCHAR cfg_prsline (PCHAR line, PLIST_ENTRY list)
{
	PCHAR p;
	cfg_entry* entry = NULL;
	int tag = 0;

	// check params
	if (!list || !line)
		return NULL;	
	if (!*line)
		return NULL;

	// check for comment
	if (*line == ';' || *line == ' ' ||  *line == 0x0d || *line == 0x0a)
		goto __exit;

	// add to list
	entry = malloc (sizeof (cfg_entry));
	if (!entry)
		goto __exit;
	memset (entry,0,sizeof (cfg_entry));

	// add tag
	cfg_getkey(line,entry->tag,sizeof (entry->tag));
	// add content
	p = cfg_getcontent (line,entry->value,sizeof (entry->value));
	if (!p)
	{
		free (entry);
		goto __exit;
	}
	_strupr (entry->tag);
	InsertTailList(list,&entry->chain);

__exit:
	// return next line ptr
	p = strchr (line,'\n');
	p++;

	// EOF ?
	if (!*p)
		return NULL;

	return p;
}

/*
*	get int corresponding to tag
*
*/
int cfg_getintfromtag (PLIST_ENTRY list, PCHAR tag)
{
	cfg_entry* pEntry = NULL;
	PLIST_ENTRY CurrentListEntry = NULL;
	CHAR	szName [MAX_PATH];
	int found = 0;

	if (!list || !tag)
		return found;

	// check list
	CurrentListEntry = list->Flink;
	while (TRUE)
	{
		pEntry = (cfg_entry*)CurrentListEntry;
		if (strcmp (tag,pEntry->tag) != 0)
			goto __next;
		strcpy (szName,pEntry->value);
		found = atoi (szName);
		break;

__next:
		// next
		if (CurrentListEntry->Flink == list || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}
	return found;
}

/*
*	get string corresponding to tag
*
*/
int cfg_getstringfromtag (PLIST_ENTRY list, PCHAR tag, PCHAR outstring, int outstringsize)
{
	cfg_entry* pEntry = NULL;
	PLIST_ENTRY CurrentListEntry = NULL;
	int found = -1;

	if (!list || !tag || !outstring || !outstringsize)
		return found;

	// check list
	CurrentListEntry = list->Flink;
	while (TRUE)
	{
		pEntry = (cfg_entry*)CurrentListEntry;
		if (strcmp (tag,pEntry->tag) != 0)
			goto __next;
		strncpy (outstring,pEntry->value,outstringsize);
		found = 0;
		break;

__next:
		// next
		if (CurrentListEntry->Flink == list || CurrentListEntry->Flink == NULL)
			break;
		CurrentListEntry = CurrentListEntry->Flink;
	}
	return found;
}

/*
 *	returns linked list with tag/values (must be freed) from memory (unxored if needed)
 *
 */
int cfg_parse_from_memory (PCHAR p,ULONG len, unsigned char xorseed, LIST_ENTRY* cfglist)
{
	PCHAR line = NULL;

	if (!p || !len)
		return -1;

	// xor if asked
	if (xorseed != 0)
		cfg_xor(p,len,xorseed);
	
	// build linkedlist
	line = p;
	while (1)
	{
		// parse line
		line = cfg_prsline(line,cfglist);
		if (!line)
			break;
	}

	if (p)
		free (p);
	return 0;
}

/*
*	returns linked list with tag/values (must be freed) from txt file (unxored if needed)
*
*/
int cfg_read (PCHAR cfgfilepath, unsigned char xorseed, LIST_ENTRY* cfglist)
{
	FILE* f;
	int len = 0;
	unsigned char* p = NULL;
	int res = -1;

	if (!cfgfilepath || !cfglist)
		return -1;

	// read
	f = fopen (cfgfilepath,"rb");
	if (!f)
		goto __exit;
	fseek (f,0,SEEK_END);
	len = ftell (f);
	rewind (f);
	if (!len)
		goto __exit;
	p = malloc (len + 32);
	if (!p)
		goto __exit;
	memset (p,0,len+32);
	if (fread (p,len,1,f) != 1)
		goto __exit;

	res = cfg_parse_from_memory(p,len,xorseed,cfglist);

__exit:
	if (f)
		fclose(f);
	return res;
}
