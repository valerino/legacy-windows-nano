#ifndef __cfg_handle__
#define __cfg_handle__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#include <windows.h>
#else
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *Flink;
	struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;
#endif

#ifndef InitializeListHead
#define InitializeListHead(ListHead) (\
	(ListHead)->Flink = (ListHead)->Blink = (ListHead))
#endif

#ifndef IsListEmpty
#define IsListEmpty(ListHead) \
	((ListHead)->Flink == (ListHead))
#endif

#ifndef InsertTailList
#define InsertTailList(ListHead,Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_ListHead;\
	_EX_ListHead = (ListHead);\
	_EX_Blink = _EX_ListHead->Blink;\
	(Entry)->Flink = _EX_ListHead;\
	(Entry)->Blink = _EX_Blink;\
	_EX_Blink->Flink = (Entry);\
	_EX_ListHead->Blink = (Entry);\
}
#endif

#ifndef RemoveEntryList
#define RemoveEntryList(Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_Flink;\
	_EX_Flink = (Entry)->Flink;\
	_EX_Blink = (Entry)->Blink;\
	_EX_Blink->Flink = _EX_Flink;\
	_EX_Flink->Blink = _EX_Blink;\
}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

#ifndef RemoveTailList
#define RemoveTailList(ListHead) \
	(ListHead)->Blink;\
{RemoveEntryList((ListHead)->Blink)}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

// cfg entry
typedef struct _cfg_entry {
	LIST_ENTRY chain;
	char tag [64];
	char value [256];
} cfg_entry;


/*
 *	get line len until EOL
 *
 */
int cfg_linelen (PCHAR pLine);

/*
*	get content at beginning of string until ;, return next content ptr on the same buffer if any
*
*/
PCHAR cfg_getsubvalue (PCHAR pBuffer);

/*
 *	compare key (before =) to keyname value
 *
 */
int cfg_comparekey (PCHAR pBuffer, PCHAR pkeyname);

/*
 *	get content after = until EOL
 *
 */
PCHAR cfg_getcontent (PCHAR pBuffer, PCHAR pDest, int Size);

/*
 *	read a configuration file of key=values in a linked list
 *
 */
int cfg_parse_from_memory (PCHAR cfgBuffer,ULONG len, unsigned char xorseed, LIST_ENTRY* cfglist);
int cfg_read (PCHAR cfgfilepath, unsigned char xorseed, LIST_ENTRY* cfglist);

/*
 *	free linked list
 *
 */
void cfg_free (PLIST_ENTRY cfg);

/*
 *	get key part of key=value string
 *
 */
int cfg_getkey (PCHAR pBuffer, PCHAR pKeyName, int keynamelen);

/*
 *	get value of entry "tag" in int format
 *
 */
int cfg_getintfromtag (PLIST_ENTRY list, PCHAR tag);

/*
 *	get value of entry "tag" in string format
 *
 */
int cfg_getstringfromtag (PLIST_ENTRY list, PCHAR tag, PCHAR outstring, int outstringsize);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif