#include "injlib.h"

VOID dbg_out(char* format, ...)
{
#if _DEBUG
	char buf [1024] = {0};
	va_list ap;

	va_start(ap, format);
	vsnprintf(buf,sizeof (buf),format,ap);
	va_end(ap);

	OutputDebugStringA(buf);
#endif
}

/*
*	createremotethread replacement (uses an undocumented function to let it work on vista, calls standard api on xp/2k). *undoc call works on xp/vista32/64*
*
*/
HANDLE proc_createremotethread (HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress,
								LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId)
{
	typedef HANDLE (WINAPI *_CreateRemoteThread)(HANDLE hProcess,
		__in_opt  LPSECURITY_ATTRIBUTES lpThreadAttributes,
		__in      SIZE_T dwStackSize,
		__in      LPTHREAD_START_ROUTINE lpStartAddress,
		__in_opt  LPVOID lpParameter,
		__in      DWORD dwCreationFlags,
		__out_opt LPDWORD lpThreadId
		); 
	
	typedef NTSTATUS (NTAPI *RtlCreateUserThread)(HANDLE ProcessHandle, PSECURITY_DESCRIPTOR SecurityDescriptor, IN BOOLEAN CreateSuspended, 
		IN ULONG StackZeroBits, ULONG StackReserved, ULONG StackCommit, PVOID StartAddress, PVOID StartParameter, PHANDLE ThreadHandle, PCLIENT_ID ClientID ); 

	HANDLE th = NULL;
	HMODULE ntdll = NULL;
	HMODULE kernel32 = NULL;
	OSVERSIONINFO osinfo;
	RtlCreateUserThread pRtlCreateUserThread = NULL;
	_CreateRemoteThread pCreateRemoteThread = NULL;
	BOOLEAN suspended = FALSE;
	NTSTATUS Status = 0;
	PSECURITY_DESCRIPTOR secdesc = NULL;
	CLIENT_ID cid;
	
	ntdll = LoadLibrary ("ntdll.dll");
	kernel32 = LoadLibrary("kernel32.dll");

	/* check os version */
	osinfo.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	if (!GetVersionEx (&osinfo))
		return NULL;
	
	/* use standard api if < vista */
 	if (osinfo.dwMajorVersion < 6)
	{
		pCreateRemoteThread = (_CreateRemoteThread) GetProcAddress(kernel32,"CreateRemoteThread");
 		th = pCreateRemoteThread (hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
		FreeLibrary(kernel32);
		return th;
	}

	/* get function from ntdll */
	pRtlCreateUserThread = (RtlCreateUserThread) GetProcAddress(ntdll,"RtlCreateUserThread");

	/* create thread */
	if (dwCreationFlags & CREATE_SUSPENDED)
		suspended = TRUE;
	if (lpThreadAttributes)
		secdesc = lpThreadAttributes->lpSecurityDescriptor;

	Status = pRtlCreateUserThread(hProcess, secdesc, suspended, 0, 0, (ULONG)dwStackSize, lpStartAddress, lpParameter, &th, &cid);
	
	FreeLibrary (ntdll);
	return th;
}

int inject (PCHAR dllname, ULONG pid)
{
	typedef HANDLE (WINAPI *OPENPROCESS)(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId);
	typedef LPVOID (WINAPI *VIRTUALALLOCEX)(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	typedef BOOL (WINAPI *WRITEPROCESSMEMORY)(HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T *lpNumberOfBytesWritten);
	typedef BOOL (WINAPI *VIRTUALFREEEX) (HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType );
	HANDLE hProcess = NULL;
	HMODULE hLib = NULL;
	unsigned char* pscMem = NULL;
	ULONG written = 0;
	HANDLE hThread = NULL;
	int res = -1;
	HMODULE address = 0;
	PCHAR p = NULL;
	OPENPROCESS pfOpenProcess = NULL;
	VIRTUALALLOCEX pfVirtualAllocEx = NULL;
	WRITEPROCESSMEMORY pfWriteProcessMemory = NULL;
	VIRTUALFREEEX pfVirtualFreeEx = NULL;

	// get function addresses
	hLib = LoadLibrary("kernel32.dll");
	pfOpenProcess = (OPENPROCESS)GetProcAddress(hLib, "OpenProcess");
	pfVirtualAllocEx = (VIRTUALALLOCEX)GetProcAddress(hLib, "VirtualAllocEx");
	pfWriteProcessMemory = (WRITEPROCESSMEMORY)GetProcAddress(hLib, "WriteProcessMemory");
	pfVirtualFreeEx = (VIRTUALFREEEX)GetProcAddress(hLib,"VirtualFreeEx");

	// open process
	hProcess = pfOpenProcess (PROCESS_CREATE_THREAD|PROCESS_QUERY_INFORMATION|PROCESS_VM_OPERATION|PROCESS_VM_WRITE|PROCESS_VM_READ|SYNCHRONIZE, FALSE,pid);
	if (!hProcess)
		goto __exit;
	
	// allocate memory in the target process
	pscMem = (PUCHAR)pfVirtualAllocEx (hProcess, NULL, 4096, MEM_COMMIT, PAGE_READWRITE);
	if (!pscMem)
		goto __exit;

	// copy dll name string and create thread
	if (!pfWriteProcessMemory (hProcess, pscMem, dllname, strlen (dllname) + 1, &written))
		goto __exit;
	hThread = proc_createremotethread(hProcess,NULL,0, (LPTHREAD_START_ROUTINE)(GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA")), pscMem, 0, NULL);
	if (!hThread)
	{
		goto __exit;
	}
	WaitForSingleObject(hThread,INFINITE);
	res = 0;

__exit:
	if (hThread)
		CloseHandle(hThread);
	if (pscMem)
		pfVirtualFreeEx (hProcess,pscMem,4096,MEM_DECOMMIT);
	if (hProcess)
		CloseHandle(hProcess);
	FreeLibrary(hLib);
	return res;
}

/*
 *	free list allocated with proc_getprocesseslist
 *
 */
void proc_freeprocesseslist (IN LIST_ENTRY* processes)
{
	void* p;

	while (!IsListEmpty(processes))
	{
		p = RemoveHeadList (processes);
		if (p)
			free(p);
	}
}

/*
 *	get list of running processes using toolhelp32 functions
 *
 */
int proc_getprocesseslist (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses)
{
	HANDLE	hSnapshot	= INVALID_HANDLE_VALUE;
	procentry* entry = NULL;
	int res = -1;
	int	num = 0;
	typedef HANDLE (WINAPI *CREATETOOLHELP32SNAPSHOT) (DWORD dwFlags, DWORD th32ProcessID);
	typedef BOOL (WINAPI *PROCESS32FIRST) (HANDLE hSnapshot, LPPROCESSENTRY32 lppe);
	typedef BOOL (WINAPI *PROCESS32NEXT) (HANDLE hSnapshot, LPPROCESSENTRY32 lppe);
	CREATETOOLHELP32SNAPSHOT pfCreateToolhelp32Snapshot = NULL;
	PROCESS32FIRST pfProcess32First = NULL;
	PROCESS32NEXT pfProcess32Next = NULL;
	HMODULE hlib = NULL;

	// check params
	if (!processes)
		goto __exit;
	if (numprocesses)
		*numprocesses = 0;

	InitializeListHead(processes);
	hlib = LoadLibrary ("kernel32.dll");
	pfCreateToolhelp32Snapshot = (CREATETOOLHELP32SNAPSHOT)GetProcAddress(hlib,"CreateToolhelp32Snapshot");
	pfProcess32First = (PROCESS32FIRST)GetProcAddress(hlib,"Process32First");
	pfProcess32Next = (PROCESS32NEXT)GetProcAddress(hlib,"Process32Next");

	/// get current sys snapshot
	hSnapshot = pfCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE)
		goto __exit;

	/// walk snapshot
	entry = calloc (1,sizeof (procentry));
	if (!entry)
		goto __exit;

	entry->processentry.dwSize = sizeof (PROCESSENTRY32);
	if (pfProcess32First (hSnapshot,&entry->processentry) == 0)
	{
		free (entry);
		goto __exit;
	}

	while (TRUE)
	{
		/// add to list
		InsertTailList(processes,&entry->chain);
		num++;

		/// allocate and get next
		entry = calloc (1,sizeof (procentry));
		if (!entry)
			break;
		
		// get next
		entry->processentry.dwSize = sizeof (PROCESSENTRY32);
		if (pfProcess32Next (hSnapshot,&entry->processentry) == 0)
		{
			if (GetLastError() == ERROR_NO_MORE_FILES)
			{
				/// ok
				res = 0;
			}
			else
			{
				/// free all
				proc_freeprocesseslist(processes);
			}
			break;
		}
	}
	
	if (numprocesses)
		*numprocesses = num;

__exit:
	if (hSnapshot)
		CloseHandle(hSnapshot);
	FreeLibrary(hlib);

	if (res != 0 && numprocesses)
		*numprocesses = 0;
	return res;
}

/*
 *	returns process pid from name/path
 *
 */
DWORD proc_getpidbyname (IN char* processname, IN BOOL issubstring)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	char name [MAX_PATH] = {0};
	DWORD	dwPid = 0;
	char* p = NULL;
	char* q = NULL;

	if (!processname)
		return 0;

	/// get processes list
	if (proc_getprocesseslist (&processes, NULL) != 0)
		return 0;

	/// walk entries
	strncpy (name,processname,MAX_PATH);
	strupr(name);

	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;
		strupr (entry->processentry.szExeFile);
		/// check processname
		p = strrchr (name,'\\');
		if (p)
			p++;
		else
			p = (char*)&name;
		q = strrchr (entry->processentry.szExeFile,'\\');
		if (q)
			q++;
		else
			q = (char*)&entry->processentry.szExeFile;

		if (issubstring)
		{
			if (strstr (q,p))
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}
		else
		{
			if (_stricmp (p,q) == 0)
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return dwPid;
}
