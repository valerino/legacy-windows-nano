#ifndef __injlib_h__
#define __injlib_h__

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <winsock2.h>
#include <TlHelp32.h>
#include <lists.h>

typedef struct procentry {
	LIST_ENTRY chain;
	PROCESSENTRY32 processentry;
} procentry;

typedef LONG NTSTATUS, *PNTSTATUS;

typedef struct _CLIENT_ID {
	HANDLE UniqueProcess;
	HANDLE UniqueThread;
} CLIENT_ID;
typedef CLIENT_ID *PCLIENT_ID;

HANDLE proc_createremotethread (HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress,
								LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);
int inject (PCHAR dllname, ULONG pid);
void proc_freeprocesseslist (IN LIST_ENTRY* processes);
int proc_getprocesseslist (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses);
DWORD proc_getpidbyname (IN char* processname, IN BOOL issubstring);

VOID dbg_out(char* format, ...);

#ifdef  __cplusplus
}
#endif

#endif