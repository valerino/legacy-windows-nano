#ifndef _NANOEVT_H
#define _NANOEVT_H

#define RK_NANOEVT_SYMLINK_NAME L"{33EA04D2-3B3F-4765-A370-83605A8EA8F2}" /* symbolic link for usermode */

#ifdef NO_EVT_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

typedef enum _EvtActionType
{
	Action_Command = 0,
	Action_Rule,
	Action_Invalid = -1
} EvtActionType;

typedef struct _nanoevt_act_command
{
	ULONG cmdtype;
	void *param1;
	void *param2;
	void *param3;
	void *param4;
} nanoevt_act_command;

typedef struct _nanoevt_action
{
	LIST_ENTRY chain;
	EvtActionType type;
	void *action;
} nanoevt_action;

typedef struct _nanoevt_event
{
	LIST_ENTRY chain;
	ULONG type;
	LIST_ENTRY actions;
	void *params;
} nanoevt_event;

extern PEPROCESS g_pSystemProcess;
extern BOOLEAN g_bEnabled;
#endif 
