#ifndef __plgnbscan_h__
#define __plgnbscan_h__

/*
 * portscan data
 *
 */
#pragma pack(push, 1) 
typedef struct portscan_data {
	unsigned long ip;		/* ip address */
	unsigned short port; /* port */
	char banner [260]; /* banner */
} portscan_data;
#pragma pack(pop)

#define REVERT_PORTSCANDATA(_x_) {	\
	portscan_data* _p_ = (_x_);	\
	_p_->ip = htonl (_p_->ip);	\
	_p_->port = htons (_p_->port);	\
}

/*
 *	netbios data
 *
 */

#define NETBIOS_TYPE_MACHINE	1
#define NETBIOS_TYPE_SHARE		2
#define NETBIOS_TYPE_PRINTER	3

#pragma pack(push, 1) 
typedef struct netbios_data {
	unsigned long ip;					/* machine ip address */
	unsigned short netbios_name [64];	/* machine netbios/share name */
	unsigned short container [64];		/* container (usually workgroup or domain name) */
	int type;							/* netbios type (share,machine,printer)*/
} netbios_data;
#pragma pack(pop)

#define REVERT_NETBIOSDATA(_x_) {	\
	netbios_data* _p_ = (_x_);	\
	_p_->ip = htonl (_p_->ip);	\
	_p_->type = htonl (_p_->type);	\
}

#endif

