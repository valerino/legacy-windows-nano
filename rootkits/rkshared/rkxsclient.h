#ifndef __rkxsclient_h__
#define __rkxsclient_h__

#include <w32_defs.h>
#include <rkclient.h>
#include <rkxspy.h>
#include <tagrtl.h>
#include <aes.h>
#include <minilzo.h>


PRKCLIENT_CTX RkXsClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed);

#endif

