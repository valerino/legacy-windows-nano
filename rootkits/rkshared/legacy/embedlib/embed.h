#ifndef __EMBED_H__
#define __EMBED_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
*	stuff to be embedded
*
*/

// global
#define EMBEDDED_TAG_UID					"werxcnzv347 fs7"
extern unsigned long uniqueid;

#define EMBEDDED_TAG_NANO_CFG				"sdfczvn,zxcmvmf"
extern unsigned char na_cfg_bin[16*1024];

#define SIZEOF_EMBEDDEDSTUFF 3*1024
// embedded stuff (all strings are doublenull terminated)
/* 
 *	nano generic embedded format : build,svcname,binname,basename
 *  null generic embedded format : nanosvcname,nanobinname,nanodisplayname,picoregname,picobinname,picobasename
 *  pico generic embedded format : build,picoregname,picobinname,picodllname,picobasename
 *  plguser generic embedded format : picobinname,picodllname,picobasename
 */
#define EMBEDDED_TAG_GENERIC				"  dfmk58usrg23u"
extern unsigned char embedded_stuff [SIZEOF_EMBEDDEDSTUFF];

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif