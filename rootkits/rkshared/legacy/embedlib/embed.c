// the stuff to be embedded at compile time

#include "embed.h"

/*
 *	global
 *
 */
#ifdef PICO
#pragma data_seg(".SHRD")
#endif
char taguid[] = EMBEDDED_TAG_UID;
unsigned long uniqueid = 0xaaaaaaaa;

char natagcfg[] = EMBEDDED_TAG_NANO_CFG;
unsigned char na_cfg_bin[16*1024] = {"a"};

// 3kbyte for embedded stuff
char taggeneric[] = EMBEDDED_TAG_GENERIC;
unsigned char embedded_stuff [SIZEOF_EMBEDDEDSTUFF] = {"a"};
#ifdef PICO
#pragma data_seg()
#pragma comment(linker, "/section:.SHRD,RWS")
#endif



