#ifndef __plgcrapi_h__
#define __plgcrapi_h__

#pragma pack(push, 1) 
typedef struct cryptoapi_data {
	unsigned short processname [32]; /* processname */
	char sessionid [32];        /* context id */
    int direction;              /* 0=out, 1=in */
	unsigned long size;			/* size of data */
	unsigned char data;			/* data */
} cryptoapi_data;
#pragma pack(pop)

#define REVERT_CRYPTOAPIDATA(_x_) {	\
	cryptoapi_data* _p_ = (_x_);	\
	_p_->size = htonl (_p_->size);	\
    _p_->direction = htonl (_p_->direction);  \
}
#define DIRECTION_OUT 0
#define DIRECTION_IN 1
#endif

