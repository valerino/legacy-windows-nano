/*
 *	this include contains tags (msgtag) for all the produced rootkits (both legacy and modular)
 *
 */

#ifndef __rktags_h__
#define __rktags_h__

/// picomod
#define RK_PICOMOD_MSGTAG	0x02391061

/// newrk (picomod variation)
#define RK_NEWRK_MSGTAG		0x00410042

/// nanomod
#define RK_NANOMOD_MSGTAG	0x41806171

/// nanoxp
#define RK_NANOXP_MSGTAG	0x19211921

/// pico (deprecated)
#define RK_PICO_MSGTAG		0x01010101

/// xspy
#define RK_XSPY_MSGTAG		0x05050505

/// micro (symbian)
#define RK_SYMMICRO_MSGTAG	0x02020202

/// micro (winmobile)
#define RK_WMMICRO_MSGTAG	0x012510a3

/// micro (symbian 3rd edition)
#define RK_SYMMICRO_3RD_MSGTAG	0x02020303

/// ipico (osx)
#define RK_IPICO_MSGTAG     0xaabbccdd

#endif // #ifndef __rktags_h__
