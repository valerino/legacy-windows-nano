/*
 *	stuff common to picomod
 *
 */

#ifndef __pmshared_h__
#define __pmshared_h__

#include <rktypes.h>
#include <modrkcore.h>

/*
*	internal ioctl interface for usermode modules
*
*/

#define EVT_IOCTL "{B28C918B-325A-454a-A431-068F24F3F027}"
#define EVT_IOCTL_PROCESSED "{52A4F511-CE00-4737-ABE5-CC48AE56EE88}"
#define EVT_DATA_READY_TO_COMM "{F23F2534-4140-4d44-A032-48760FFD760F}" // for communication plugins

#define UMCORE_IOCTL_GETLOGSPACE		555		// request log space to core
#define UMCORE_IOCTL_DELETELOGFILE		556			// ask core to delete a file it created

#pragma pack(push, 1) 
typedef struct umcore_ioctlstruct {
	unsigned long ioctl_result;			// result of the ioctl request
	unsigned long ioctl_cmdtype;		// command type
	int reqnotify;						// request notify of command execution
	LARGE_INTEGER cmd_uid;				// command uid
	cmd_data cmd;						// command data
} umcore_ioctl_struct;
#pragma pack(pop)

#endif //// #ifndef __pmshared_h__

