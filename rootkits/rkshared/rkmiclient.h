#ifndef __rkmiclient_h__
#define __rkmiclient_h__

#include <w32_defs.h>
#include <rkclient.h>
#include <rkmicro.h>
#include <tagrtl.h>
#include <aes.h>
#include <minilzo.h>

PRKCLIENT_CTX RkMiClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed);

#endif

