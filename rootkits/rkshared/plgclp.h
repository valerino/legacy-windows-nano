#ifndef __plgclp_h__
#define __plgclp_h__

#pragma pack(push, 1) 
typedef struct clp_data {
	unsigned long type;			/* type of clipboard data */
	unsigned short processname [32]; /* processname */
	unsigned long size;			/* size of data */
	unsigned char data;			/* data */
} clp_data;
#pragma pack(pop)

#define REVERT_CLPDATA(_x_) {	\
	clp_data* _p_ = (_x_);	\
	_p_->type = htonl (_p_->type);	\
	_p_->size = htonl (_p_->size);	\
}

#endif

