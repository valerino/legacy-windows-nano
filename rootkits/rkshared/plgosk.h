#ifndef __plgosk_h__
#define __plgosk_h__

#define KEY_E0_FLAG 0x200

#pragma pack(push, 1) 
typedef struct osk_data {
	unsigned short processname [32]; /* processname */
	unsigned long size;			/* size of data */
	unsigned char data;			/* data */
} osk_data;
#pragma pack(pop)

#define REVERT_OSKDATA(_x_) {	\
	osk_data* _p_ = (_x_);	\
	_p_->size = htonl (_p_->size);	\
}

#endif /// #ifndef __plgosk_h__

