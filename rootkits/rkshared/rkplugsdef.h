#ifndef __rkplugsdef_h__
#define __rkplugsdef_h__

/*
 *	constants for plugins
 *
 */
#define LOGEVT_PLUGINS	9000

#define LOGEVT_COOKIES_FF		LOGEVT_PLUGINS+1
#define LOGEVT_COOKIES_IE		LOGEVT_PLUGINS+2
#define LOGEVT_COOKIES_OP		LOGEVT_PLUGINS+3
#define LOGEVT_BOOKMARKS_IE		LOGEVT_PLUGINS+4
#define LOGEVT_BOOKMARKS_FF		LOGEVT_PLUGINS+5
#define LOGEVT_BOOKMARKS_OP		LOGEVT_PLUGINS+6
#define LOGEVT_ADDRESSBOOK_MSOE	LOGEVT_PLUGINS+7
#define LOGEVT_ADDRESSBOOK_TB	LOGEVT_PLUGINS+8
#define LOGEVT_MAIL_OUTLOOK		LOGEVT_PLUGINS+9
#define LOGEVT_MAIL_OE			LOGEVT_PLUGINS+10
#define LOGEVT_SCREENSHOT_NANO	LOGEVT_PLUGINS+11
#define LOGEVT_VIRTUALKBD		LOGEVT_PLUGINS+12
#define LOGEVT_CLIPBOARD		LOGEVT_PLUGINS+13
#define LOGEVT_NET_OUT			LOGEVT_PLUGINS+14
#define LOGEVT_NET_IN			LOGEVT_PLUGINS+15
#define LOGEVT_NET_INOUT		LOGEVT_PLUGINS+16
#define LOGEVT_NET_CONNECT		LOGEVT_PLUGINS+17
#define LOGEVT_CRYPTOAPI		LOGEVT_PLUGINS+18
#define LOGEVT_AMBIENTSOUND		LOGEVT_PLUGINS+19
#endif // #ifndef __rkplugsdef_h__ 
