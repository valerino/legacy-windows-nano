#ifndef __nmglobal_h__
#define __nmglobal_h__

#include "nmshared.h"
#include "rktypes.h"
#include <plugins/nanofs.h>
#include <plugins/nanonet.h>
#include <plugins/nanoif.h>
#include <plugins/nanondis.h>
#include <plugins/plgcrapi.h>
#include <plugins/plgclp.h>
#include <plugins/plgosk.h>
#include <plugins/plgucmds.h>

#endif // #ifndef __nmglobal_h__

