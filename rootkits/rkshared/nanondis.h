#ifndef __nanondis_h__
#define __nanondis_h__

#ifdef NO_NDIS_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

/*
 *	eth/ip/proto structs
 *
 */
#ifdef _KERNELMODE
#include "network/B2WINET.H"
struct timeval {
	long    tv_sec;         /// seconds
	long    tv_usec;        /// microseconds
};
#endif // #ifdef _KERNELMDE

/*
*	nanondis cfg
*
*/
typedef struct _nanondis_cfg {
	long enabled;			/// ndis log enabled/disabled
	long promiscuous;		/// log in promiscuous mode
	long log_noip;			/// log non-ip packets
	long log_noeth;			/// log non-ethernet packets
	LIST_ENTRY ndisrules;	/// ndis log rules
} nanondis_cfg;

/*
*	ndis rule
*
*/
typedef struct _ndis_rule {
	LIST_ENTRY chain;				/// internal use
	int direction;					/// direction
	int protocol;					/// protocol
	unsigned short dstport;			/// destination port
	unsigned short srcport;			/// source port
	unsigned long srcip;			/// source ip
	unsigned long dstip;			/// destination ip
} ndis_rule;

#define NDIS_PASS_PACKET 0
#define NDIS_LOG_PACKET 1

/*
 *	pcap-style packet header
 *
 */
#pragma pack(push, 1) 
typedef struct _bpf_hdr {
	struct timeval  bh_tstamp;  
	unsigned long   bh_caplen;          
	unsigned long	bh_datalen;         
} bpf_hdr;
#pragma pack(pop)

#define REVERT_BPFHDR(_zx_) {		\
	bpf_hdr* _zp_ = (_zx_);			\
	_zp_->bh_tstamp.tv_sec = htonl (_zp_->bh_tstamp.tv_sec);		\
	_zp_->bh_tstamp.tv_usec = htonl (_zp_->bh_tstamp.tv_usec);		\
	_zp_->bh_caplen = htonl (_zp_->bh_caplen);		\
	_zp_->bh_datalen = htonl (_zp_->bh_datalen);		\
}

#define RK_NANONDIS_SYMLINK_NAME L"{60CEA7DD-B632-48eb-8442-A0E19CAEEAA8}" /* symbolic link for usermode */

#endif // #ifndef __nanondis_h__
