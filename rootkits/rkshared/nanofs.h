/*
 *	nano filesystem filter defs
 *	-vx-
 */
#ifndef __nanofs_h__
#define __nanofs_h__

#ifdef NO_FS_MSG
#ifdef KdPrintEx
#undef KdPrintEx
#define KdPrintEx
#endif
#endif

/// TODO : add large_integer, list_entry, ulong defs for *nix

#define FS_LOG_DATA_FULL -1		/// log info and data for the whole file
#define FS_LOG_INFO		-2		/// log info only

/*
*	filesystem log rule
*
*/
typedef struct _fs_rule {
	LIST_ENTRY chain;				/// internal use
	unsigned short processname[32];	/// processname
	unsigned short extension[10];	/// file extension
	long sizetolog;					/// size to be logged
} fs_rule;


#define RK_NANOFS_SYMLINK_NAME L"{D7B35A74-6531-4363-8146-20A99BF283EB}" /* symbolic link for usermode */

#endif /// #ifndef __nanofs_h__

