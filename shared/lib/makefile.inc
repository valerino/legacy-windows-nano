ifeq ("$(LIBINSTALLDIR)", "")
INSTALLPREFIX = /usr/local
INCINSTALLDIR = $(INSTALLPREFIX)/include
LIBINSTALLDIR = $(INSTALLPREFIX)/lib
endif

INCDIR = -I$(PWD)/../inc -I$(INCINSTALLDIR) -I../../inc

export OSTYPE=$(shell uname -s | tr "[:upper:]" "[:lower:]")

ifneq (, $(findstring darwin, $(OSTYPE)))
  LIBFLAGS+=-dynamiclib
  SHLIB_EXT=dylib
  STLIB_EXT=a
else
  LIBFLAGS+=-shared
  SHLIB_EXT=so
  STLIB_EXT=a
endif

all: shared static

$(OBJECTS): %.o: %.c
	$(CC) $(CFLAGS) $(INCDIR) -fPIC -c $< -o $@
	
shared: $(OBJECTS)
	@if [ "X$(LIBNAME)" != "X" ]; then \
	    $(CC) $(CFLAGS) $(INCDIR) $(LIBFLAGS) $(OBJECTS) -o lib$(LIBNAME).$(SHLIB_EXT); \
	fi

static: $(OBJECTS)
	@if [ "X$(LIBNAME)" != "X" ]; then \
	    $(AR) cru lib$(LIBNAME).$(STLIB_EXT) $(OBJECTS); \
	    $(RANLIB) lib$(LIBNAME).$(STLIB_EXT); \
	fi

clean:
	rm -f $(OBJECTS)
	([ -f lib$(LIBNAME).$(STLIB_EXT) ] && rm -f lib$(LIBNAME).$(STLIB_EXT)) || true
	([ -f lib$(LIBNAME).$(SHLIB_EXT) ] && rm -f lib$(LIBNAME).$(SHLIB_EXT)) || true

install:
	#strip lib$(LIBNAME).$(SHLIB_EXT)
	([ -f lib$(LIBNAME).$(SHLIB_EXT) ] && cp lib$(LIBNAME).$(SHLIB_EXT) $(LIBINSTALLDIR)) || true
	([ -f lib$(LIBNAME).$(STLIB_EXT) ] && cp lib$(LIBNAME).$(STLIB_EXT) $(LIBINSTALLDIR)) || true
	@for i in $(INCLUDES); do \
		cp $$i $(INCINSTALLDIR); \
	done

test:
	#
	# TODO - IMPLEMENT
	#
