#ifndef __dbg_h__
#define __dbg_h__

/*
*	printf style debugging macros (ascii/unicode)
*
*/
#ifndef DBG
#ifdef _DEBUG
#define DBG
#endif
#endif

#if defined (DBG)
void dbg_out(char* format, ...);
#define DBG_OUT(__x__) dbg_out __x__  
void wdbg_out(wchar_t* format, ...);
#define WDBG_OUT(__x__) wdbg_out __x__  
#else  
#define DBG_OUT(__x__)  
#define WDBG_OUT(__x__)
#endif

#endif