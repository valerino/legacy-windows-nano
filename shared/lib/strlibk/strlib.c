/*
*	generic string functions
*	-vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strlib.h"
#include <time.h>
#include <wchar.h>

#ifdef _KERNELMODE
#include <ntifs.h>
#endif

/*
*	a table of values and macro for converting an ASCII hexadecimal digit into a number
*
*/
static const unsigned char digits[256] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

/*
 *	change slashes to path. if usebackslash is specified, \ is used, otherwise / is used
 *
 */
void str_changeslashesw (IN wchar_t* path, IN int usebackslash)
{
	int len = 0;
	int i = 0;
	
	if (!path)
		return;
	
	len = wcslen (path);
	for (i=0; i < len; i ++)
	{
		if (path[i] == (wchar_t)'\\')
		{
			if (!usebackslash)
				path[i] = (wchar_t)'/';
		}
	}
	return;
}

/*
*	change slashes to path. if usebackslash is specified, \ is used, otherwise / is used
*
*/
void str_changeslashes (IN char* path, IN int usebackslash)
{
	int len = 0;
	int i = 0;

	if (!path)
		return;

	len = strlen (path);
	for (i=0; i < len; i ++)
	{
		if (path[i] == (char)'\\')
		{
			if (!usebackslash)
				path[i] = (char)'/';
		}
	}
	return;
}

/*
*	tokenize string (change token to \0). string must be big enough to hold another \0 in the end
*
*/
void str_tokenizew (IN OUT wchar_t* string, IN wchar_t token)
{
	int len = 0;
	int i = 0;

	if (!string)
		return;

	len = wcslen (string);
	for (i=0; i < len; i ++)
	{
		if (string[i] == token)
		{
			string[i] = (wchar_t)'\0';
		}
	}
	string[i] = (wchar_t)'\0';
	string[i+1] = (wchar_t)'\0';
	return;
}

/*
*	tokenize string (change token to \0). string must be big enough to hold another \0 in the end
*
*/
void str_tokenize (IN OUT char* string, IN char token)
{
	int len = 0;
	int i = 0;

	if (!string)
		return;

	len = strlen (string);
	for (i=0; i < len; i ++)
	{
		if (string[i] == token)
		{
			string[i] = '\0';
		}
	}
	string[i] = '\0';
	string[i+1] = '\0';
	return;
}

/*
 *	terminate string on char occurrence. returns TRUE if succeeded.
 *
 */
int str_terminateoncharw (IN wchar_t* string, IN wchar_t terminateon)
{
	wchar_t* pTerm = NULL;

	// check params
	if (!string || !terminateon)
		return 0;

	// check char
	pTerm = wcsrchr(string,terminateon);
	if (!pTerm)
		return 0;

	// terminate
	*pTerm = (wchar_t)'\0';
	return 1;
}

/*
*	terminate string on char occurrence. returns TRUE if succeeded.
*
*/
int str_terminateonchar (IN char* string, IN char terminateon)
{
	char* pTerm = NULL;

	// check params
	if (!string || !terminateon)
		return 0;

	// check char
	pTerm = strchr(string,terminateon);
	if (!pTerm)
		return 0;

	// terminate
	*pTerm = '\0';
	return 1;
}


/*
 *	unenclose string from enclosingchar delimiters. returns TRUE if succeeded
 *
 */
int str_unenclosew (IN wchar_t* string, IN wchar_t enclosingchar)
{
	wchar_t* pBuffer = NULL;

	// check params
	if (!string || !enclosingchar)
		return 0;

	// allocate memory
#ifdef _KERNELMODE
	pBuffer = ExAllocatePoolWithTag(PagedPool,wcslen(string)*sizeof(wchar_t) + sizeof (wchar_t),'lrts');
	if (pBuffer)
		memset (pBuffer,0,wcslen(string)*sizeof(wchar_t) + sizeof (wchar_t));
#else
	pBuffer = (wchar_t*)calloc (1,(wcslen(string)*sizeof(wchar_t)) + sizeof (wchar_t));
#endif // #ifdef _KERNELMODE
	if (!pBuffer)
		return 0;

	// unenclose string
	wcscpy (pBuffer, (string + 1));
	pBuffer[wcslen(pBuffer) - 1] = (wchar_t)'\0';
	wcscpy (string,pBuffer);

#ifdef _KERNELMODE
	ExFreePool (pBuffer);
#else
	free (pBuffer);
#endif

	return 1;
}

/*
*	unenclose string from enclosingchar delimiters. returns TRUE if succeeded
*
*/
int str_unenclose (IN char* string, IN char enclosingchar)
{
	char* pBuffer = NULL;

	// check params
	if (!string || !enclosingchar)
		return 0;

	// allocate memory
#ifdef _KERNELMODE
	pBuffer = ExAllocatePoolWithTag (PagedPool,strlen(string) + sizeof (char),'lrts');
	if (pBuffer)
		memset (pBuffer,0,strlen(string) + sizeof (char));
#else
	pBuffer = (char*)calloc (1,(strlen(string) + sizeof (char)));
#endif // #ifdef _KERNELMODE
	if (!pBuffer)
		return 0;

	// unenclose string
	strcpy (pBuffer, (string + 1));
	pBuffer[strlen(pBuffer) - 1] = (char)'\0';
	strcpy (string,pBuffer);

#ifdef _KERNELMODE
	ExFreePool (pBuffer);
#else
	free (pBuffer);
#endif
	return 1;
}

/*
*	return hexadecimal corresponding to char (0-9,a-f), or 0xff if no mapping
*
*/
unsigned char digit_to_number (unsigned char digit)
{
	unsigned char c = digits[digit];
	return c;
}

/*
 *	remove trailing slash from string if present (returns TRUE if so)
 *
 */
int str_remove_trailing_slash (IN char* pString)
{
	int res = 0;
	int len = 0;

	// check params
	if (!pString)
		goto __exit;

	// check if it ends with '\'
	len = (int)strlen (pString);
	if (pString[len - 1] == '\\' || pString[len - 1] == '/')
	{
		// remove slash
		pString[len-1] = '\0';
		res = 1;
	}

__exit:	
	return res;
}

/*
*	remove trailing slash from unicode string if present (returns TRUE if so)
*
*/
int str_remove_trailing_slashw (IN wchar_t* pString)
{
	int res = 0;
	int len = 0;

	// check params
	if (!pString)
		goto __exit;

	// check if it ends with '\' or '/'
	len = (int)wcslen (pString);
	if (pString[len - 1] == (wchar_t)'\\' || pString[len - 1] == (wchar_t)'/')
	{
		// remove slash
		pString[len-1] = (wchar_t)'\0';
		res = 1;
	}

__exit:	
	return res;
}

/*
 *	find buffer into another (like strstr)
 *
 */
void* find_buffer_in_buffer(IN void* srcbuf, IN void* buftosearch, IN unsigned long srcbufsize, IN unsigned long searchsize, OPTIONAL IN int casesensitive)
{
	unsigned long current = 0;
	unsigned long matched  = 0;
	unsigned char* src = (unsigned char*)srcbuf;
	unsigned char* tgt = (unsigned char*)buftosearch;
	unsigned char cs;
	unsigned char ct;
	unsigned long stopsize = srcbufsize;

	if (!srcbuf || !buftosearch || !srcbufsize || !searchsize)
		return NULL;

	while (current < stopsize)
	{
		cs = *src;
		ct = *tgt;

		/// convert all to uppercase to perform case insensitive comparison if specified
		if (!casesensitive)
		{
			if (cs >= (char) 'a' && cs <= (char) 'z')
				cs -= 0x20;

			if (ct >= (char) 'a' && ct <= (char) 'z')
				ct -= 0x20;
		}

		if (cs == ct)
		{
			matched += sizeof(char);
			if (matched == searchsize)
				return src - (matched / sizeof(char)) + sizeof (char);
			tgt++;
		}
		else
		{
			tgt = (unsigned char*)buftosearch;
			matched = 0;
		}
		src++;
		current += sizeof(char);
	}

	return NULL;
}

/* 
*	get filename from full path (unicode). returns size of filename in wchars. path is limited to 260 chars
*
*/
int str_getnamefrompathw (IN wchar_t* path, OUT wchar_t* outfilename, IN int outfilenamewsize)
{
	wchar_t* pRelativeName = NULL;
	wchar_t* pRelativeNameAlt = NULL;
	int len = 0;

	// check params
	if (!path || !outfilename || !outfilenamewsize)
		return 0;

	// get relative name
	pRelativeName = wcsrchr (path,(wchar_t)'\\');
	pRelativeNameAlt = wcsrchr (path,(wchar_t)'/');
	
	// its an unix path ?
	if (pRelativeNameAlt)
		pRelativeName = pRelativeNameAlt;

	if (pRelativeName)
		pRelativeName++;
	else
		// already relative
		pRelativeName = path;

	len = (int)wcslen (pRelativeName);

	// check size
	if (len > outfilenamewsize)
		return 0;

	// ok
	wcscpy (outfilename,pRelativeName);
	return len;
}

/* 
*	get filename from full path. returns size of filename in chars. path is limited to 512 chars
*
*/
int str_getnamefrompath (IN char* path, OUT char* outfilename, IN int outfilenamesize)
{
	int len = 0;
	wchar_t wpath [513];
	wchar_t woutfilename [261];

	// check params
	if (!path || !outfilename || !outfilenamesize)
		return 0;

	mbstowcs (wpath,path,512);
	len = str_getnamefrompathw(wpath,woutfilename,260);
	if (len == 0)
		return len;
	
	wcstombs (outfilename,woutfilename,outfilenamesize);
	
	return (int)strlen (outfilename);
}

/*
 *	same as strcpy, but ends at eol. returns size copied
 *
 */
int str_copyeol (IN char* dst, IN int dstsize, IN char* src)
{
	int count = 0;
	char* p = NULL;
	char* q = NULL;

	if (!dst || !src || !dstsize)
		return 0;
	
	// copy until eol
	memset (dst,0,dstsize);
	p = src;
	q = dst;
	
	while (1)
	{
		if (*p == '\0' || *p == '\r' || *p == '\n')
			break;
		*q = *p;
		q++;p++;
		count++;
		
		if (count == (dstsize - 1))
			break;
	}	
	
	return (int)strlen (dst);
}

/*
 *	enclose string in EnclosingChar characters
 *
 */
int str_enclose (IN char* pString, IN char EnclosingChar)
{
	int res = -1;
	char szC [2];
	char* pBuffer = NULL;

	// check params
	if (!pString || !EnclosingChar)
		goto __exit;

	// allocate memory
#ifdef _KERNELMODE
	pBuffer = ExAllocatePoolWithTag (PagedPool,strlen (pString) + 4,'lrts');
	if (pBuffer)
		memset (pBuffer,0,strlen (pString) + 4);
#else
	pBuffer = (char*)calloc (1,strlen (pString) + 4);
#endif
	if (!pBuffer)
		goto __exit;

	// enclose string
	pBuffer[0] = EnclosingChar;
	strcpy (pBuffer + 1, pString);

	szC[0] = EnclosingChar;	
	szC[1] = '\0';
	strcat (pBuffer,szC);

	strcpy (pString,pBuffer);

	// ok
	res = 0;

__exit:
#ifdef _KERNELMODE
	ExFreePool (pBuffer);
#else
	free (pBuffer);
#endif
	return res;
}

/*
*	enclose string in EnclosingChar characters (unicode)
*
*/
int str_enclosew (IN wchar_t* pString, IN wchar_t EnclosingChar)
{
	int res = -1;
	wchar_t szC [2];
	wchar_t* pBuffer = NULL;

	// check params
	if (!pString || !EnclosingChar)
		goto __exit;

	// allocate memory
#ifdef _KERNELMODE
	pBuffer = ExAllocatePoolWithTag(PagedPool, wcslen (pString) * sizeof (wchar_t) + 4*sizeof (wchar_t),'lrts');
	if (pBuffer)
		memset (pBuffer,0,wcslen (pString) * sizeof (wchar_t) + 4*sizeof (wchar_t));
#else
	pBuffer = (wchar_t*)calloc (1,(wcslen (pString) * sizeof (wchar_t)) + 4*sizeof (wchar_t));
#endif
	if (!pBuffer)
		goto __exit;

	// enclose string
	pBuffer[0] = EnclosingChar;
	wcscpy (pBuffer + 1, pString);

	szC[0] = EnclosingChar;	
	szC[1] = (wchar_t)'\0';
	wcscat (pBuffer,szC);

	wcscpy (pString,pBuffer);

	// ok
	res = 0;

__exit:
#ifdef _KERNELMODE
	ExFreePool (pBuffer);
#else
	free (pBuffer);
#endif
	return res;
}

/*
 *	append string to path, handling (back)slashes. source buffer must be big enough to hold the resulting string
 *	
 */
int str_appendpathw (IN OUT wchar_t* source, IN wchar_t* toappend)
{
	if (!source || !toappend)	
		return -1;
	
	// ensure string isnt slash/backslash terminated
	str_remove_trailing_slashw(source);

	// append slash and string
#if defined(WIN32) || defined(WINCE) || defined (_KERNELMODE)
	wcscat (source,L"\\");
#else
	wcscat (source,L"//");
#endif
	wcscat (source,toappend);
	return 0;
}

/*
*	append string to path, handling (back)slashes. source buffer must be big enough to hold the resulting string
*	
*/
int str_appendpath (IN OUT char* source, IN char* toappend)
{
	if (!source || !toappend)	
		return -1;

	// ensure string isnt slash/backslash terminated
	str_remove_trailing_slash(source);

	// append slash and string
#if defined(WIN32) || defined(WINCE) || defined (_KERNELMODE)
	strcat (source,"\\");
#else
	strcat (source,"//");
#endif
	strcat (source,toappend);
	return 0;
}

/*
*	same as strcpy but ends at any character included in stopstring (included \0)
*
*/
unsigned long str_cpy_stopat (OUT char* outstring, IN unsigned long outstrsize, IN char* instring, IN char* stopstring)
{
	char* p = instring;
	char* q = outstring;
	unsigned long size = 0;

	if (!outstring || !outstrsize || !instring || !stopstring)
		return 0;
	memset (outstring,0,outstrsize);
	while (1)
	{
		if (strchr (stopstring,*p) || *p == '\0')
			break;
		*q=*p;
		q++;p++;
		size++;
		if (size == (outstrsize - 1))
			break;
	}
	return size;
}

/*
 *	return bytes length of a wchar string
 *
 */
unsigned long str_lenbytesw (IN wchar_t* string)
{
	unsigned long size = 0;
	if (!string)
		return 0;
	size = wcslen(string);
	return (size * sizeof (wchar_t));
}

/*
*	convert large integer to systemtime (usermode only). if timestr is supplied, the time is copied as a string there (must be big enough)
*	timeisunixtime is ignored on unix.
*/
#ifndef _KERNELMODE
int str_largeinteger_to_systemtime (IN LARGE_INTEGER* time, OUT SYSTEMTIME* systime, OPTIONAL IN int timeisunixtime, OPTIONAL OUT char* timestr)
{
	if (!time || !systime)
		return -1;

#if defined (WIN32) || defined (WINCE)
	
	/// check if time is unix time (comes from a unix source)
	if (timeisunixtime)
	{
		/// convert to windows time
		time->QuadPart = (time->QuadPart + 11644473600) * 10000000;
	}
	FileTimeToSystemTime((FILETIME*)time,systime);		
#else
	struct tm* plt = NULL;
	time_t uxtime = 0;
	unsigned short milliseconds = 0;
	unsigned long long msecs = 0;

	/* get millisecs from 1970 */
	msecs = (unsigned long long)(time->QuadPart/10000) - 11644473600000LL; //(31556926000*(1970-1601)); 
	
	/// get seconds from 1970
	uxtime = (msecs/1000);
	plt = gmtime (&uxtime);
	milliseconds = msecs - (uxtime*1000);

	/// convert to win32 structure
	systime->wYear = plt->tm_year+1900;
	systime->wMonth = plt->tm_mon+1;
	systime->wDayOfWeek = plt->tm_wday;
	systime->wDay = plt->tm_mday;
	systime->wHour = plt->tm_hour;
	systime->wMinute = plt->tm_min;
	systime->wSecond = plt->tm_sec;
	systime->wMilliseconds = milliseconds;
#endif
	
	/// convert if asked
	if (timestr)
	{
		sprintf (timestr,"%.04d%.02d%.02d-%.02d-%.02d-%.02d-%.04d", systime->wYear, systime->wMonth, systime->wDay,
			systime->wHour, systime->wMinute, systime->wSecond, systime->wMilliseconds);
	}
	
	return 0;
}
#endif