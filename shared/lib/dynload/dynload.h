#ifndef __dynload_h__
#define __dynload_h__

#ifdef __cplusplus
extern "C"
{
#endif

#if (defined (WIN32) || defined (WINCE))
#include <winsock2.h>
#else
#include <w32_defs.h>
#endif

/*
*	load dynamic library at dllpath, return dll handle. flags is ignored on windows. returns dll handle
*	for unix, if flags is set to 0 RTLD_NOW is assumed
*/
void* dyn_loadlibraryw (IN wchar_t* dllpath, OPTIONAL IN int flags);

/*
*	load dynamic library at dllpath, return dll handle. flags is ignored on windows. returns dll handle
*	for unix, if flags is set to 0 RTLD_NOW is assumed
*/
void* dyn_loadlibrary (IN char* dllpath, OPTIONAL IN int flags);

/*
*	free library loaded with dyn_loadlibrary. returns 0 on success
*
*/
int dyn_freelibrary (IN void* dllhandle);

/*
*	get function address in dll by name
*
*/
void* dyn_getprocaddress (IN VOID* dllhandle, IN char* name);

/*
*	get function address in dll by name
*
*/
void* dyn_getprocaddressw (IN VOID* dllhandle, IN wchar_t* name);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __dynload_h__

