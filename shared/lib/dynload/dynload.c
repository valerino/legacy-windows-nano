/*
 *	library to handle dynamic link libraries functions (loading,freeing,getprocaddress,...) in a portable way
 *	-vx-
 */

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#if (defined (WIN32) || defined (WINCE))
#include <winsock2.h>
#else
#include <w32_defs.h>
#endif

/*
 *	load dynamic library at dllpath, return dll handle. flags is ignored on windows. returns dll handle
 *	for unix, if flags is set to 0 RTLD_NOW is assumed
 */
void* dyn_loadlibraryw (IN wchar_t* dllpath, OPTIONAL IN int flags)
{
	void* mod = NULL;
	char ansipath [1024] = {0};

	if (!dllpath)
		return NULL;

#if defined (WIN32) && defined (WINCE)
	mod = LoadLibrary (dllpath);
#endif

#if defined (WIN32) && !defined (WINCE)
	mod = LoadLibraryW (dllpath);
#endif

#if !defined (WIN32) && !defined (WINCE)
	wcstombs(ansipath,dllpath,sizeof (ansipath));
	if (flags == 0)
		flags = RTLD_NOW;
	mod = dlopen (ansipath,flags);
#endif
	return mod;
}	

/*
*	load dynamic library at dllpath, return dll handle. flags is ignored on windows. returns dll handle
*	for unix, if flags is set to 0 RTLD_NOW is assumed
*/
void* dyn_loadlibrary (IN char* dllpath, OPTIONAL IN int flags)
{
	wchar_t wcharpath [1024] = {0};
	
	if (!dllpath)
		return NULL;

	mbstowcs(wcharpath,dllpath,1024);
	return dyn_loadlibraryw(wcharpath,flags);
}

/*
 *	free library loaded with dyn_loadlibrary. returns 0 on success
 *
 */
int dyn_freelibrary (IN void* dllhandle)
{
	int res = -1;
#if (defined (WIN32) || defined (WINCE))
	if (FreeLibrary(dllhandle))
		res = 0;
#else
	if (dlclose (dllhandle) == 0)
		res = 0;
#endif
	return res;
}

/*
*	get function address in dll by name
*
*/
void* dyn_getprocaddressw (IN VOID* dllhandle, IN wchar_t* name)
{
	void* addr = NULL;
	char ansiname [128] = {0};

	if (!name || !dllhandle)
		return NULL;

#if defined (WIN32) && defined (WINCE)
	addr = GetProcAddress (dllhandle, name);
#endif

#if defined (WIN32) && !defined (WINCE)
	wcstombs(ansiname,name,sizeof (ansiname));
	addr = GetProcAddress (dllhandle, ansiname);
#endif

#if !defined (WIN32) && !defined (WINCE)
	wcstombs(ansiname,name,sizeof (ansiname));
	addr = dlsym (dllhandle, ansiname);
#endif
	return addr;
}

/*
 *	get function address in dll by name
 *
 */
void* dyn_getprocaddress (IN VOID* dllhandle, IN char* name)
{
	wchar_t wcharname [128] = {0};

	if (!dllhandle || !name)
		return NULL;

	mbstowcs(wcharname,name,128);
	return dyn_getprocaddressw(dllhandle,wcharname);
}

