#ifndef __strlib_h__
#define __strlib_h__

#ifdef __cplusplus
extern "C"
{
#endif

#if !defined (_KERNELMODE)
#if (defined (WIN32) || defined (WINCE))
#include <winsock2.h>
#else
#include <w32_defs.h>
#endif
#endif // #if !defined (_KERNELMODE)

#ifndef IN
#define IN
#endif
#ifndef OUT
#define OUT
#endif
#ifndef OPTIONAL
#define OPTIONAL
#endif


/*
*	change slashes to path. if usebackslash is specified, \ is used, otherwise / is used
*
*/
void str_changeslashesw (IN wchar_t* path, IN int usebackslash);

/*
*	change slashes to path. if usebackslash is specified, \ is used, otherwise / is used
*
*/
void str_changeslashes (IN char* path, IN int usebackslash);

/*
*	unenclose string from enclosingchar delimiters. returns TRUE if succeeded
*
*/
int str_unenclosew (IN wchar_t* string, IN wchar_t enclosingchar);

/*
*	unenclose string from enclosingchar delimiters. returns TRUE if succeeded
*
*/
int str_unenclose (IN char* string, IN char enclosingchar);

/*
*	terminate string on char occurrence. returns TRUE if succeeded.
*
*/
int str_terminateonchar (IN char* string, IN char terminateon);

/*
*	terminate string on char occurrence. returns TRUE if succeeded.
*
*/
int str_terminateoncharw (IN wchar_t* string, IN wchar_t terminateon);

/*
*	return hexadecimal corresponding to char (0-9,a-f), or 0xff if no mapping
*
*/
unsigned char digit_to_number (unsigned char digit);

/*
 *	find buffer in buffer (as strstr, but generic)
 *
 */
void* find_buffer_in_buffer(IN void* srcbuf, IN void* buftosearch, IN unsigned long srcbufsize, IN unsigned long searchsize, OPTIONAL IN int casesensitive);

/*
*	remove trailing slash from string if present (returns TRUE if so)
*
*/
int str_remove_trailing_slash (IN char* pString);

/*
*	remove trailing slash from unicode string if present (returns TRUE if so)
*
*/
int str_remove_trailing_slashw (IN wchar_t* pString);

/* 
*	get filename from full path (unicode) . returns size of filename in wchars. path is limited to 512 chars
*
*/
int str_getnamefrompathw (IN wchar_t* path, OUT wchar_t* outfilename, IN int outfilenamewsize);

/* 
*	get filename from full path. returns size of filename in chars. path is limited to 512 chars
*
*/
int str_getnamefrompath (IN char* path, OUT char* outfilename, IN int outfilenamesize);

/*
*	enclose string in EnclosingChar characters (unicode)
*
*/
int str_enclosew (IN wchar_t* pString, IN wchar_t EnclosingChar);

/*
*	enclose string in EnclosingChar characters
*
*/
int str_enclose (IN char* pString, IN char EnclosingChar);

/*
*	same as strcpy, but ends at eol. returns size copied
*
*/
int str_copyeol (IN char* dst, IN int dstsize, IN char* src);

/*
*	append string to path, handling (back)slashes. source buffer must be big enough to hold the resulting string
*	
*/
int str_appendpathw (IN OUT wchar_t* source, IN wchar_t* toappend);

/*
*	append string to path, handling (back)slashes. source buffer must be big enough to hold the resulting string
*	
*/
int str_appendpath (IN OUT char* source, IN char* toappend);

/*
*	same as strcpy but ends at any character included in stopstring (included \0)
*
*/
unsigned long str_cpy_stopat (OUT char* outstring, IN unsigned long outstrsize, IN char* instring, IN char* stopstring);

/*
*	tokenize string (change token to \0). string must be big enough to hold another \0 in the end
*
*/
void str_tokenizew (IN OUT wchar_t* string, IN wchar_t token);

/*
*	tokenize string (change token to \0). string must be big enough to hold another \0 in the end
*
*/
void str_tokenize (IN OUT char* string, IN char token);

/*
*	return bytes length of a wchar string
*
*/
unsigned long str_lenbytesw (IN wchar_t* string);

#if !defined (_KERNELMODE)
/*
*	convert large integer to systemtime (usermode only). if timestr is supplied, the time is copied as a string there (must be big enough)
*	timeisunixtime is ignored on unix.
*/
int str_largeinteger_to_systemtime (IN LARGE_INTEGER* time, OUT SYSTEMTIME* systime, OPTIONAL IN int timeisunixtime, OPTIONAL OUT char* timestr);
#endif /// #if !defined (_KERNELMODE)

#ifdef __cplusplus
}
#endif

#endif /// #ifnded __strlib_h__
