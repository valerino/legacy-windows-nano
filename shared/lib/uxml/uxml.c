/*
XML Reader
Minimal error checking on xml format, must be validated from tera.
Multi-thread unsafe, must be fixed for that, currently not needed anyway
This module is expected to run at PASSIVE_LEVEL (file operations, pagedpool allocs) if used in windows kernelmode
Encoding: UTF-8
*/

#include "uxml.h"
#include <dbg.h>

#define SKIP_BLANKS(__p) \
	while((*__p==' ' || *__p=='\t' || *__p=='\r' || *__p == '\n') && *__p!=0) __p++; \

#define END_OF_TAG(__p) \
	while(*__p != '>') __p++; \
	if(*__p == 0) break;

#define ADVANCE_ELEMENT(__p) \
	while(*__p!='>' && *__p!=' ' && *__p!='\t' && *__p!='\r' && *__p != '\n' && *__p!=0) __p++; \

#define ADVANCE_TO_ATTR_VALUE(__p) \
	while(*__p!='=' && *__p!=' ' && *__p!='\t' && *__p!='\r' && *__p != '\n' && *__p!=0) __p++;\
	SKIP_BLANKS(__p);

#define UXML_DECLARATION "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>"

int ParseBuffer(IN char* Buffer, IN UXml *NewXml);

#if defined _KERNELMODE
#define KXML_POOL_TAG 'lmxK'
#pragma alloc_text(PAGE, UXmlCreate)
#pragma alloc_text(PAGE, UXmlDestroy)
#pragma alloc_text(PAGE, UXmlCreateNode)
#pragma alloc_text(PAGE, UXmlCreateAttribute)
#pragma alloc_text(PAGE, UXmlDestroyNode)
#pragma alloc_text(PAGE, UXmlDestroyAttribute)
#pragma alloc_text(PAGE, UXmlAddNode)
#pragma alloc_text(PAGE, UXmlAddAttribute)
#pragma alloc_text(PAGE, UXmlSetNodeValue)
#pragma alloc_text(PAGE, UXmlGetNextNode)
#pragma alloc_text(PAGE, UXmlGetChildNode)
#pragma alloc_text(PAGE, UXmlGetFirstAttribute)
#pragma alloc_text(PAGE, UXmlGetNextAttribute)
#pragma alloc_text(PAGE, UXmlGetAttributeByName)
#pragma alloc_text(PAGE, UXmlSetRoot)
#pragma alloc_text(PAGE, UXmlFromFile)
#pragma alloc_text(PAGE, ParseBuffer)
#endif /// #if defined _KERNELMODE

/*
 *	parse the xml buffer
 *
 */
int ParseBuffer(IN char* Buffer, IN UXml *NewXml)
{
	char* pBuf = Buffer;
	int res = -1;
	char* pMarker = NULL;
	char* pName = NULL;
	char* pValue = NULL;
	ULONG_PTR nameLen = 0;
	UXmlNode *curParent = NULL;
	UXmlNode *newNode = NULL;

	if(!Buffer || !NewXml)
		return -1;

	SKIP_BLANKS(pBuf);
	if(strncmp(pBuf, UXML_DECLARATION, strlen(UXML_DECLARATION)) != 0)
	{
		DBG_OUT(("UXml: Not a valid uxml file.\n"));
		return -1;
	}

	pBuf += strlen(UXML_DECLARATION);
	while(*pBuf != 0)
	{
		SKIP_BLANKS(pBuf);
		if(*pBuf++ == '<')
		{
			SKIP_BLANKS(pBuf);
			if(strncmp(pBuf, "!--", 3) == 0)
			{
				pBuf += 3;
				//can't use END_OF_TAG macro, since comments may contain '>' and similar
				pBuf = strstr(pBuf, "-->");
				pBuf += 3;
			}
			else if(*pBuf == '/')
			{
				if(curParent != NULL)
					curParent = curParent->Parent;
				END_OF_TAG(pBuf);
				pBuf++;
			}
			else
			{
				pMarker = pBuf;
				ADVANCE_ELEMENT(pBuf);
				nameLen = pBuf - pMarker;

#if defined _KERNELMODE
				pName = ExAllocatePoolWithTag(PagedPool, nameLen+1, KXML_POOL_TAG);
				if (pName)
					memset (pName,0,nameLen + 1);
#else
				pName = calloc(1,nameLen + 1);
#endif
				if(!pName)
				{
					DBG_OUT(("UXml: ExAllocatePoolWithTag failed. nameLen: %d, Line: %d.\n", nameLen, __LINE__));
					return -1;
				}

				strncpy(pName, pMarker, nameLen);
				pName[nameLen] = '\0';

				res = UXmlCreateNode(NULL, pName, NULL, &newNode);
				if(res != 0)
				{
					DBG_OUT(("UXml: UXmlCreateNode failed. pName: %08x, Line: %d.\n", pName, __LINE__));
					if(pName)
#if defined _KERNELMODE
						ExFreePoolWithTag(pName, KXML_POOL_TAG);
#else
						free (pName);
#endif
					return -1;
				}

#if defined _KERNELMODE
				ExFreePoolWithTag(pName, KXML_POOL_TAG);
#else
				free (pName);
#endif
				pName = NULL;

				SKIP_BLANKS(pBuf);
				while(*pBuf != '>')
				{
					pMarker = pBuf;
					ADVANCE_TO_ATTR_VALUE(pBuf);
					nameLen = pBuf - pMarker;

#if defined _KERNELMODE
					pName = ExAllocatePoolWithTag(PagedPool, nameLen+1, KXML_POOL_TAG);
					if (pName)
						memset (pName,0,nameLen + 1);
#else
					pName = calloc(1,nameLen + 1);
#endif
					if(!pName)
					{
						DBG_OUT(("UXml: ExAllocatePoolWithTag failed. nameLen: %d, Line: %d.\n", nameLen, __LINE__));
						return -1;
					}

					strncpy(pName, pMarker, nameLen);
					pName[nameLen] = '\0';

					pBuf++;
					if(*pBuf == '"' || *pBuf == '\'')
					{
						char cQuote = *pBuf++;
						pMarker = pBuf;
						while(*pBuf != cQuote && *pBuf != '\0')
							pBuf++;
						if(*pBuf == cQuote)
						{
							nameLen = pBuf - pMarker;

#if defined _KERNELMODE
							pValue = ExAllocatePoolWithTag(PagedPool, nameLen+1, KXML_POOL_TAG);
							if (pValue)
								memset (pValue,0,nameLen + 1);
#else
							pValue = calloc(1,nameLen + 1);
#endif
							if(!pValue)
							{
								DBG_OUT(("UXml: ExAllocatePoolWithTag failed. nameLen: %d, Line: %d\n", nameLen, __LINE__));
								if(pName)
#if defined _KERNELMODE
									ExFreePoolWithTag(pName, KXML_POOL_TAG);
#else
									free (pName);
#endif
								return -1;
							}

							strncpy(pValue, pMarker, nameLen);
							pValue[nameLen] = '\0';
							pBuf++;
						}
					}
					
					if(pName && pValue)
					{
						res = UXmlCreateAttribute(newNode, pName, pValue, NULL);
						if(res != 0)
						{
							DBG_OUT(("UXml: could not create new attribute. pName: %x, pValue: %x\n", pName, pValue));
							if(pName)
#if defined _KERNELMODE
								ExFreePoolWithTag(pName, KXML_POOL_TAG);
#else
								free (pName);
#endif
							if(pValue)
#if defined _KERNELMODE
								ExFreePoolWithTag(pValue, KXML_POOL_TAG);
#else
								free (pValue);
#endif
							return -1;
						}
						
#if defined _KERNELMODE
						ExFreePoolWithTag(pName, KXML_POOL_TAG);
#else
						free (pName);
#endif
						pName = NULL;
#if defined _KERNELMODE
						ExFreePoolWithTag(pValue, KXML_POOL_TAG);
#else
						free (pValue);
#endif
						pValue = NULL;
					}
					
					SKIP_BLANKS(pBuf);
					if(*pBuf == '/')
						pBuf++;
				}

				if(curParent)
					UXmlAddNode(curParent, newNode);
				else
					UXmlSetRoot(NewXml, newNode);

				if(*(pBuf - 1) != '/')
					curParent = newNode;
				pBuf++;

				SKIP_BLANKS(pBuf);
				if(*pBuf != '<')
				{
					pMarker = pBuf;
					while(*pBuf != '<' && *pBuf != '\0')
						pBuf++;
					if(*pBuf == '<')
					{
						nameLen = pBuf - pMarker;
#if defined _KERNELMODE
						pValue = ExAllocatePoolWithTag(PagedPool, nameLen+1, KXML_POOL_TAG);
						if (pValue)
							memset (pValue,0,nameLen + 1);
#else
						pValue = calloc (1,nameLen + 1);
#endif
						if(!pValue)
						{
							DBG_OUT(("UXml: ExAllocatePoolWithTag failed. nameLen: %d, Line: %d\n", nameLen, __LINE__));
							return -1;
						}

						strncpy(pValue, pMarker, nameLen);
						pValue[nameLen] = '\0';
						UXmlSetNodeValue(newNode, pValue);
						
#if defined _KERNELMODE
						ExFreePoolWithTag(pValue, KXML_POOL_TAG);
#else
						free (pValue);
#endif
						pValue = NULL;
					}
				}
			}
		}
	}

	return 0;
}

/*
*	create an xml structure
*
*/
int UXmlCreate(IN char* Name, OUT UXml **NewXml)
{
	UXml *pNewXml = NULL;

	if(!Name || !NewXml)
		return -1;
	if(Name[0] == '\0')
		return -1;

#if defined _KERNELMODE
	pNewXml = ExAllocatePoolWithTag(PagedPool, sizeof(UXml), KXML_POOL_TAG);
	if (pNewXml)
		memset (pNewXml,0,sizeof (UXml));
#else
	pNewXml = calloc (1,sizeof (UXml));
#endif
	if(!pNewXml)
		return -1;

	pNewXml->Root = NULL;
#if defined _KERNELMODE
	pNewXml->Name = ExAllocatePoolWithTag(PagedPool, strlen(Name)+1, KXML_POOL_TAG);
	if (pNewXml->Name)
		memset (pNewXml->Name,0,strlen(Name) + 1);
#else
	pNewXml->Name = calloc (1,strlen(Name)+1);
#endif
	if(!pNewXml->Name)
	{
#if defined _KERNELMODE
		ExFreePoolWithTag(pNewXml, KXML_POOL_TAG);
#else
		free (pNewXml);
#endif
		return -1;
	}

	/// done
	strncpy(pNewXml->Name, Name, strlen(Name));
	*NewXml = pNewXml;
	return 0;
}

/*
*	destroy an xml structure
*
*/
int UXmlDestroy(IN UXml *Xml)
{

	if(!Xml)
		return -1;

	if(Xml->Root)
		UXmlDestroyNode(Xml->Root);

	if(Xml->Name)
	{	
#if defined _KERNELMODE
		ExFreePoolWithTag(Xml->Name, KXML_POOL_TAG);
#else
		free (Xml->Name);
#endif
	}

#if defined _KERNELMODE
	ExFreePoolWithTag(Xml, KXML_POOL_TAG);
#else
	free (Xml);
#endif

	return 0;
}

/*
*	create an xml node
*
*/
int UXmlCreateNode(OPTIONAL IN UXmlNode *Parent, IN char* Name, IN char* Value, OPTIONAL OUT UXmlNode **NewNode)
{
	UXmlNode *pNewNode = NULL;
	int res = -1;

	if(!Name)
		return -1;

	if(!Parent && !NewNode)
		return -1;

	/// allocate a node
#if defined _KERNELMODE
	pNewNode = (UXmlNode *)ExAllocatePoolWithTag(PagedPool, sizeof(UXmlNode), KXML_POOL_TAG);
	if (pNewNode)
		memset (pNewNode,0,sizeof (UXmlNode));
#else
	pNewNode = calloc (1,sizeof (UXmlNode));
#endif

	if(!pNewNode)
		return -1;

	InitializeListHead(&pNewNode->NodeListHead);
	InitializeListHead(&pNewNode->AttributeListHead);

#if defined _KERNELMODE
	pNewNode->Name = ExAllocatePoolWithTag(PagedPool, strlen(Name)+1, KXML_POOL_TAG);
	if (pNewNode->Name)
		memset (pNewNode->Name,0,strlen(Name) + 1);
#else
	pNewNode->Name = calloc(1,strlen(Name)+1);
#endif
	if(!pNewNode->Name)
	{
		res = -1;
		goto __exit;
	}
	
	strncpy(pNewNode->Name, Name, strlen(Name));
	pNewNode->Name[strlen(Name)] = '\0';

	if(Value)
	{
#if defined _KERNELMODE
		pNewNode->Value = (PUCHAR)ExAllocatePoolWithTag(PagedPool, strlen(Value)+1, KXML_POOL_TAG);
		if (pNewNode->Value)
			memset (pNewNode->Value,0,strlen(Value) + 1);
#else
		pNewNode->Value = calloc (1,strlen (Value) + 1);
#endif
		if(!pNewNode->Value)
		{
			res = -1;
			goto __exit;
		}
		strncpy(pNewNode->Value, Value, strlen(Value));
		pNewNode->Value[strlen(Value)] = '\0';
	}

	/// ok
	res = 0;

__exit:
	if(res != 0)
	{
		/// cleanup and exit
		if(pNewNode->Name)
#if defined _KERNELMODE
			ExFreePoolWithTag(pNewNode->Name, KXML_POOL_TAG);
#else
			free (pNewNode->Name);
#endif
		if(pNewNode)
#if defined _KERNELMODE
			ExFreePoolWithTag(pNewNode, KXML_POOL_TAG);
#else
			free (pNewNode);
#endif
		return -1;
	}

	/// return values
	if(Parent)
	{
		InsertTailList(&Parent->NodeListHead, &pNewNode->NodeList);
		pNewNode->Parent = Parent;
	}
	
	if(NewNode)
		*NewNode = pNewNode;
	
	return 0;
}

/*
*	create an xml attribute
*
*/
int UXmlCreateAttribute(OPTIONAL IN UXmlNode *Node, IN char* Name, IN char* Value, OPTIONAL OUT UXmlAttribute **NewAttribute)
{
	UXmlAttribute *pNewAttribute = NULL;
	int res = -1;

	if(!Name || !Value)
		return -1;

	if(!Node && !NewAttribute)
		return -1;

#if defined _KERNELMODE
	pNewAttribute = (UXmlAttribute *)ExAllocatePoolWithTag(PagedPool, sizeof(UXmlAttribute), KXML_POOL_TAG);
	if (pNewAttribute)
		memset (pNewAttribute,0,sizeof (UXmlAttribute));
#else
	pNewAttribute = calloc(1,sizeof (UXmlAttribute));
#endif

	if(!pNewAttribute)
		return -1;

#if defined _KERNELMODE
	pNewAttribute->Name = (PUCHAR)ExAllocatePoolWithTag(PagedPool, strlen(Name)+1, KXML_POOL_TAG);
	if (pNewAttribute->Name)
		memset (pNewAttribute->Name,0,strlen(Name) + 1);
#else
	pNewAttribute->Name = calloc (1,strlen(Name)+1);
#endif
		if(!pNewAttribute->Name)
		{
			res = -1;
			goto __exit;
		}
		
		strncpy(pNewAttribute->Name, Name, strlen(Name));
		pNewAttribute->Name[strlen(Name)] = '\0';

#if defined _KERNELMODE
		pNewAttribute->Value = (PUCHAR)ExAllocatePoolWithTag(PagedPool, strlen(Value)+1, KXML_POOL_TAG);
		if (pNewAttribute->Value)
			memset (pNewAttribute->Value,0,strlen(Value) + 1);
#else
		pNewAttribute->Value = calloc (1,strlen(Value) + 1);
#endif
		if(!pNewAttribute->Value)
		{
			res = -1;
			goto __exit;
		}
		strncpy(pNewAttribute->Value, Value, strlen(Value));
		pNewAttribute->Value[strlen(Value)] = '\0';
	
	/// ok
	res = 0;

__exit:
	if(res != 0)
	{
		/// cleanup and exit
		if(pNewAttribute->Name)
#if defined _KERNELMODE
			ExFreePoolWithTag(pNewAttribute->Name, KXML_POOL_TAG);
#else
			free (pNewAttribute->Name);
#endif
		if(pNewAttribute)
#if defined _KERNELMODE
			ExFreePoolWithTag(pNewAttribute, KXML_POOL_TAG);
#else
			free (pNewAttribute);
#endif
		return -1;
	}

	/// return values
	if(Node)
		InsertTailList(&Node->AttributeListHead, &pNewAttribute->AttributeList);
	if(NewAttribute)
		*NewAttribute = pNewAttribute;

	return 0;
}

/*
*	destroy an xml node
*
*/
int UXmlDestroyNode(IN UXmlNode *Node)
{
	UXmlNode *pNode = NULL;
	UXmlAttribute *pAttribute = NULL;
	int res = -1;

	if(!Node)
		return -1;

	for( pAttribute = (UXmlAttribute *)Node->AttributeListHead.Flink;!IsListEmpty(&Node->AttributeListHead); 
		pAttribute = (UXmlAttribute *)Node->AttributeListHead.Flink)
	{
		if(pAttribute)
		{
			RemoveEntryList(&pAttribute->AttributeList);
			res = UXmlDestroyAttribute(pAttribute);			
		}
	}

	for(pNode = (UXmlNode *)Node->NodeListHead.Flink; !IsListEmpty(&Node->NodeListHead); pNode = (UXmlNode *)Node->NodeListHead.Flink)
	{
		if(pNode)
		{
			RemoveEntryList(&pNode->NodeList);
			res = UXmlDestroyNode(pNode);
		}
	}

	if(Node->Name)
#if defined _KERNELMODE
		ExFreePoolWithTag(Node->Name, KXML_POOL_TAG);
#else
		free (Node->Name);
#endif
	if(Node->Value)
#if defined _KERNELMODE
		ExFreePoolWithTag(Node->Value, KXML_POOL_TAG);
#else
		free (Node->Value);
#endif

#if defined _KERNELMODE
	ExFreePoolWithTag(Node, KXML_POOL_TAG);
#else
	free (Node);
#endif

	return res;
}

/*
*	destroy an xml attribute
*
*/
int UXmlDestroyAttribute(IN UXmlAttribute *Attribute)
{
	if(!Attribute)
		return -1;

	if(Attribute->Name)
#if defined _KERNELMODE
		ExFreePoolWithTag(Attribute->Name, KXML_POOL_TAG);
#else
		free (Attribute->Name);
#endif

	if(Attribute->Value)
#if defined _KERNELMODE
		ExFreePoolWithTag(Attribute->Value, KXML_POOL_TAG);
#else
		free (Attribute->Value);
#endif

#if defined _KERNELMODE
	ExFreePoolWithTag(Attribute, KXML_POOL_TAG);
#else
	free (Attribute);
#endif

	return 0;
}

/*
*	add a node
*
*/
int UXmlAddNode(IN UXmlNode *Parent, IN UXmlNode *Child)
{
	if(!Parent || !Child)
		return -1;

	InsertTailList(&Parent->NodeListHead, &Child->NodeList);

	Child->Parent = Parent;

	return 0;
}

/*
*	add an attribute
*
*/
int UXmlAddAttribute(IN UXmlNode *Node, IN UXmlAttribute *Attribute)
{
	if(!Node || !Attribute)
		return -1;

	InsertTailList(&Node->AttributeListHead, &Attribute->AttributeList);

	return 0;
}

/*
*	set node value, replacing the other if present
*
*/
int UXmlSetNodeValue(IN UXmlNode *Node, IN char* Value)

{
	if(!Node || !Value)
		return -1;

	if(Node->Value)
	{
#if defined _KERNELMODE
		ExFreePoolWithTag(Node->Value, KXML_POOL_TAG);
		Node->Value = (PUCHAR)ExAllocatePoolWithTag(PagedPool, strlen(Value)+1, KXML_POOL_TAG);
		if (Node->Value)
			memset (Node->Value,0,strlen (Value) + 1);
#else
		free (Node->Value);
		Node->Value = calloc (1,strlen (Value + 1));
#endif
		if(!Node->Value)
			return -1;
	}

	strncpy(Node->Value, Value, strlen(Value));
	Node->Value[strlen(Value)] = '\0';

	return 0;
}

/*
*	get the next node 
*
*/
UXmlNode *UXmlGetNextNode(IN UXmlNode *Node)
{
	if(!Node)
		return NULL;
	if(!Node->Parent)
		return NULL;

	if(Node->NodeList.Flink != &Node->Parent->NodeListHead)
		return (UXmlNode *)Node->NodeList.Flink;

	return NULL;
}

/*
*	get the child node
*
*/
UXmlNode *UXmlGetChildNode(IN UXmlNode *Node)
{
	if(!Node)
		return NULL;

	if(IsListEmpty(&Node->NodeListHead))
		return NULL;

	return (UXmlNode *)Node->NodeListHead.Flink;
}

/*
*	get the first attribute
*
*/
UXmlAttribute *UXmlGetFirstAttribute(IN UXmlNode *Node)
{
	if(!Node)
		return NULL;

	if(IsListEmpty(&Node->AttributeListHead))
		return NULL;

	return (UXmlAttribute *)Node->AttributeListHead.Flink;
}

/*
*	get the next attribute
*
*/
UXmlAttribute *UXmlGetNextAttribute(IN UXmlNode *Node, IN UXmlAttribute *Attribute)
{
	if(!Attribute || !Node)
		return NULL;

	if(Attribute->AttributeList.Flink != &Node->AttributeListHead)
		return (UXmlAttribute *)Attribute->AttributeList.Flink;

	return NULL;
}

/*
*	get attribute by name
*
*/
UXmlAttribute *UXmlGetAttributeByName(IN UXmlNode *Node, IN char* Name)
{
	UXmlAttribute *pAttribute = NULL;

	if(!Node)
		return NULL;
	if(IsListEmpty(&Node->AttributeListHead) || (Node->AttributeListHead.Flink == NULL))
		return NULL;

	pAttribute = (UXmlAttribute *)Node->AttributeListHead.Flink;
	while(1)
	{
		if(strcmp(pAttribute->Name, Name) == 0)
			return pAttribute;

		if(pAttribute->AttributeList.Flink == &Node->AttributeListHead || pAttribute->AttributeList.Flink == NULL)
			break;

		pAttribute = (UXmlAttribute *)pAttribute->AttributeList.Flink;
	}
	return NULL;
}

/*
*	set the root node
*
*/
int UXmlSetRoot(IN UXml *Xml, IN UXmlNode *RootNode)
{
	if(!Xml || !RootNode)
		return -1;

	if(Xml->Root != NULL)
		return -1;

	Xml->Root = RootNode;

	return 0;
}

/*
*	create an xml struct from buffer
*
*/
int UXmlFromBuffer(IN char* Buffer, IN OUT UXml **NewXml)
{
	UXml *pNewXml = NULL;

	if(!Buffer || !NewXml)
		return -1;

	if (UXmlCreate("BufXml", &pNewXml) != 0)
	{
		DBG_OUT (("UXml: Could not create xml structure\n"));
		return -1;
	}

	if (ParseBuffer(Buffer, pNewXml) != 0)
	{
		DBG_OUT(("UXml: ParseBuffer failed\n"));
		UXmlDestroy(pNewXml);
		return -1;
	}

	*NewXml = pNewXml;
	return 0;
}

/*
*	create an xml struct from file
*
*/
#if defined _KERNELMODE
int UXmlFromFile(IN PUNICODE_STRING FileName, IN OUT UXml **NewXml)
{
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	OBJECT_ATTRIBUTES objAttr = {0};
	IO_STATUS_BLOCK iosb = {0};
	FILE_STANDARD_INFORMATION fileStandardInfo = {0};
	char* pBuffer = NULL;
	HANDLE fileHandle = NULL;
	UXml *newXml = NULL;

	if(!FileName || !NewXml)
		return -1;
	*NewXml = NULL;

	InitializeObjectAttributes(&objAttr, FileName, OBJ_CASE_INSENSITIVE, NULL, NULL);
	status = ZwCreateFile(&fileHandle, GENERIC_READ, &objAttr, &iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_ALERT, NULL, 0);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT(("UXml: Could not open %wZ\n", FileName));
		return -1;
	}

	status = ZwQueryInformationFile(fileHandle, &iosb, &fileStandardInfo, sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT(("UXml: Could not get file size.\n"));
		goto __exit;
	}

	pBuffer = ExAllocatePoolWithTag(PagedPool, fileStandardInfo.EndOfFile.LowPart+32, KXML_POOL_TAG);
	if(!pBuffer)
	{
		DBG_OUT(("UXml: Could not allocate buffer.\n"));
		goto __exit;
	}
	memset (pBuffer,0,fileStandardInfo.EndOfFile.LowPart + 32);

	status = ZwReadFile(fileHandle, NULL, NULL, NULL, &iosb, pBuffer, fileStandardInfo.EndOfFile.LowPart, 0, NULL);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT(("UXml: Could not read file.\n"));
		goto __exit;
	}
	pBuffer[fileStandardInfo.EndOfFile.LowPart] = '\0';
	ZwClose(fileHandle);
	fileHandle = NULL;

	status = (NTSTATUS)UXmlCreate("test", &newXml);
	if(status != 0)
	{
		DBG_OUT(("UXml: Could not initialize xml structure.\n"));
		goto __exit;
	}
	
	// ok
	status = 0;

__exit:
	if(status != 0)
	{
		if(fileHandle)
			ZwClose(fileHandle);
		if(pBuffer)
			ExFreePoolWithTag(pBuffer, KXML_POOL_TAG);
		return -1;
	}
	else
	{
		status = ParseBuffer(pBuffer, newXml);
	}

	if(status == 0)
		*NewXml = newXml;

	if(pBuffer)
		ExFreePoolWithTag(pBuffer, KXML_POOL_TAG);

	return status;
}
#else
int UXmlFromFile(IN char* FileName, IN OUT UXml **NewXml)
{
	int res = -1;
	char* pBuffer = NULL;
	unsigned long dwFileSize = 0;
	FILE* f = NULL;
	UXml *newXml = NULL;

	if(!FileName || !NewXml)
		return -1;
	*NewXml = NULL;

	f = fopen (FileName, "rb");
	if (!f)
	{
		DBG_OUT(("UXml: Could not open %s\n", FileName));
		return -1;
	}

	fseek (f,0,SEEK_END);
	dwFileSize = ftell (f);
	fseek (f,0,SEEK_SET);
	if (dwFileSize == 0)
		goto __exit;

	pBuffer = calloc(1,dwFileSize + 32);
	if(!pBuffer)
	{
		DBG_OUT(("UXml: Could not allocate buffer.\n"));
		res = -1;
		goto __exit;
	}

	
	if(fread (pBuffer,dwFileSize,1,f) != 1)
	{
		DBG_OUT(("UXml: Could not read file.\n"));
		res = -1;
		goto __exit;
	}
	
	pBuffer[dwFileSize] = '\0';
	fclose (f);
	f = NULL;

	res = UXmlCreate("test", &newXml);
	if(res != 0)
	{
		DBG_OUT(("UXml: Could not initialize xml structure.\n"));
		goto __exit;
	}
	
	/// ok
	res = 0;

__exit:
	if (res != 0)
	{
		if(f != NULL)
			fclose (f);
		if(pBuffer)
			free(pBuffer);
	}
	else
	{
		res = ParseBuffer(pBuffer, newXml);
	}

	if(res == 0)
		*NewXml = newXml;

	if(pBuffer)
		free(pBuffer);

	return res;
}

int UXmlFromFilew(IN wchar_t* FileName, IN OUT UXml **NewXml)
{
	char path [1024] = {0};
	
	if (!FileName || !NewXml)
		return -1;
	
	wcstombs (path,FileName,1024);
	return UXmlFromFile(path,NewXml);
}
#endif