/*
*	this include is valid for both the usermode and kernelmode uxml implementation
*
*/
#ifndef __uxml_h__
#define __uxml_h__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>

#if (defined (WIN32) || defined (WINCE)) && !defined (_KERNELMODE)
#include <winsock2.h>
#endif

#if defined (_KERNELMODE)
#include <ntifs.h>
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
#include <w32_defs.h>
#endif

#include <lists.h>

/*
 *	defines an xml node
 *
 */
typedef struct _UXmlNode
{
	LIST_ENTRY NodeList;				
	LIST_ENTRY NodeListHead;
	LIST_ENTRY AttributeListHead;
	struct _UXmlNode *Parent;
	char* Name;
	char* Value;
} UXmlNode;

/*
 *	defines an xml attribute
 *
 */
typedef struct _UXmlAttribute
{
	LIST_ENTRY AttributeList;
	char* Name;
	char* Value;
} UXmlAttribute;

/*
 *	defines the xml structure
 *
 */
typedef struct _UXml
{
	UXmlNode *Root;
	char* Name;
} UXml;

/*
 *	create an xml structure
 *
 */
int UXmlCreate(IN char* Name, OUT UXml **NewXml);

/*
 *	destroy an xml structure
 *
 */
int UXmlDestroy(IN UXml *Xml);

/*
 *	create an xml node
 *
 */
int UXmlCreateNode(OPTIONAL IN UXmlNode *Parent, IN char* Name, IN char* Value, OPTIONAL OUT UXmlNode **NewNode);

/*
 *	create an xml attribute
 *
 */
int UXmlCreateAttribute(OPTIONAL IN UXmlNode *Node, IN char* Name, IN char* Value, OPTIONAL OUT UXmlAttribute **NewAttribute);

/*
 *	destroy an xml node
 *
 */
int UXmlDestroyNode(IN UXmlNode *Node);

/*
*	destroy an xml attribute
*
*/
int UXmlDestroyAttribute(IN UXmlAttribute *Attribute);

/*
 *	add a node
 *
 */
int UXmlAddNode(IN UXmlNode *Parent, IN UXmlNode *Child);

/*
 *	add an attribute
 *
 */
int UXmlAddAttribute(IN UXmlNode *Node, IN UXmlAttribute *Attribute);

/*
 *	set node value, replacing the other if present
 *
 */
int UXmlSetNodeValue(IN UXmlNode *Node, IN char* Value);

/*
 *	get the next node 
 *
 */
UXmlNode *UXmlGetNextNode(IN UXmlNode *Node);

/*
 *	get the child node
 *
 */
UXmlNode *UXmlGetChildNode(IN UXmlNode *Node);

/*
 *	get the first attribute
 *
 */
UXmlAttribute *UXmlGetFirstAttribute(IN UXmlNode *Node);

/*
*	get the next attribute
*
*/
UXmlAttribute *UXmlGetNextAttribute(IN UXmlNode *Node, IN UXmlAttribute *Attribute);

/*
*	get attribute by name
*
*/
UXmlAttribute *UXmlGetAttributeByName(IN UXmlNode *Node, IN char* Name);

/*
 *	set the root node
 *
 */
int UXmlSetRoot(IN UXml *Xml, IN UXmlNode *RootNode);

/*
 *	create an xml struct from buffer
 *
 */
int UXmlFromBuffer(IN char* Buffer, IN OUT UXml **NewXml);

/*
 *	create an xml struct from file
 *
 */
#if defined _KERNELMODE
int UXmlFromFile(IN PUNICODE_STRING FileName, IN OUT UXml **NewXml);
#else
int UXmlFromFile(IN char* FileName, IN OUT UXml **NewXml);
int UXmlFromFilew (IN wchar_t* FileName, IN OUT UXml **NewXml);
#endif

#ifdef __cplusplus
}
#endif

#endif  // #ifndef __uxml_h__