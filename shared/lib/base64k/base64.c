//************************************************************************
// base64.c
//
// utility functions
// -vx-
// 
// Base64 encoding/decoding algorithms
//************************************************************************

#include "base64.h"
#ifdef _KERNELMODE
#include <ntddk.h>
#endif

unsigned char decoder[256];
unsigned char inalphabet[256];
static unsigned char b64alphabet[65]	= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//************************************************************************
// char* Base64AllocateEncodeBuffer(unsigned long insize, unsigned long* poutsize)
//
// allocate a proper buffer for Base64 encoding, aligned to 16 bytes. 
//
// returns buffer pointer or NULL. 
//************************************************************************/
char* Base64AllocateEncodeBuffer(unsigned long insize, unsigned long* poutsize)
{
	unsigned long i = 0;
	char* neededencbuffer = NULL;
	
	// check params
	if (!insize || !poutsize)
		return NULL;
	*poutsize = 0;
	
	// allocate buffer of needed size
	i = ((insize + 2) / 3) * 4;
	i += 2 * ((i / BASE64_MAX_LINE_LENGTH) + 1);
	while (i % 128)
		i++;

#ifdef _KERNELMODE
	neededencbuffer = ExAllocatePoolWithTag (PagedPool,i+32+1,'bs64');
#else
	neededencbuffer = (char*)malloc (i + 32 + 1);
#endif
	if (neededencbuffer)
	{
		memset(neededencbuffer, 0, i + 1);
		*poutsize = i;
	}
	
	return neededencbuffer;
}

//************************************************************************
// void* Base64AllocateDecodeBuffer(char* inbuffer, unsigned long* outsize)
//
// allocate a proper buffer for Base64 decoding, aligned to 16 bytes.
//
// returns buffer pointer or NULL. 
//************************************************************************/
void* Base64AllocateDecodeBuffer(char* inbuffer, unsigned long* outsize)
{
	unsigned long BufferLength = 0;
	unsigned long	size	= 0;
	char*	outbuf	= NULL;

	// check params
	if (!inbuffer || !outsize)
		return NULL;
	*outsize = 0;

	// get buffer length
	BufferLength = (unsigned long)strlen(inbuffer);
	if (!BufferLength)
		return NULL;

	// allocate memory of the needed size
	size = 4 + ((BufferLength * 3) / 4);
	while (size % 128)
		size ++;

#ifdef _KERNELMODE
	outbuf = ExAllocatePoolWithTag(PagedPool,size+32+1,'bs64');
#else
	outbuf = (char*)malloc (size + 32 + 1);
#endif

	if (outbuf)
	{
		*outsize = size;
		memset(outbuf, 0, size + 1);
	}

	return outbuf;
}

//************************************************************************
// void* Base64Decode(char* src, unsigned long* len)
//
// Decode a supplied base64 buffer. The resulting buffer must be freed by the caller
//
// returns decoded buffer pointer or NULL
//************************************************************************/
void* Base64Decode(char* src, unsigned long* len)
{
	unsigned char* dst = NULL;
	unsigned char* d = NULL;
	unsigned char* s = NULL;
	unsigned long	outlen	= 0;
	int		bits = 0;
	int		c = 0;
	int		char_count = 0;
	int	err	= 0;

	// check params
	if (!src || !len)
		goto __exit;
        if (len)
            *len = 0;
	
	// allocate buffer
	dst = (unsigned char*)Base64AllocateDecodeBuffer(src, &outlen);
	if (!dst)
		return NULL;
        if (len)
            *len = outlen;

	// decode
	char_count = 0;
	bits = 0;
	d = dst;
	s = (unsigned char*)src;

	while ((c = *s++) != 0)
	{
		if (c == '=')
			break;

		if (c > 255 || !inalphabet[c])
			continue;

		bits += decoder[c];
		char_count++;

		if (char_count == 4)
		{
			*d++ = (bits >> 16);
			*d++ = ((bits >> 8) & 0xff);
			*d++ = ((bits & 0xff));
			bits = 0;
			char_count = 0;
		}
		else
		{
			bits <<= 6;
		}
	}

	if (c == 0)
	{
		if (char_count)
		{
			// error while decoding
			err = 1;
		}
	}
	else
	{
		/* c == '=' */
		switch (char_count)
		{
			case 1:
				// error while decoding
				err = 1;
				break;

			case 2:
				*d++ = (bits >> 10);
				break;

			case 3:
				*d++ = (bits >> 16);
				*d++ = ((bits >> 8) & 0xff);
				break;
		}
	}

__exit:
	if (err)
	{
		// cleanup on error
#ifdef _KERNELMODE
		ExFreePoolWithTag(dst,'bs64');
#else		
		free (dst);
#endif
                if (len)
                    *len = 0;
		dst = NULL;
	}

	return dst;
}

//************************************************************************
// char* Base64Encode(void* src, unsigned long srcl, unsigned long* len)
//
// Encode a buffer with BASE64. The resulting buffer must be freed by the caller
//
// returns encoded buffer or NULL
//************************************************************************/
char* Base64Encode(void* src, unsigned long srcl, unsigned long* len)
{
	unsigned char* dest = NULL;
	unsigned long outlen = 0;
	unsigned char* s = NULL;
	unsigned char* d = NULL;
	int cols = 0;
	int bits = 0;
	int c = 0;
	int char_count = 0;

	// check params
	if (!src || !srcl)
		goto __exit;
        if (len)
            *len = 0;

	// allocate proper buffer for encoding
	dest = (unsigned char*)Base64AllocateEncodeBuffer(srcl, &outlen);
	if (!dest)
		return NULL;
        if (len)
            *len = outlen;
	
	// encode
	s = (unsigned char*)src;
	d = dest;
	char_count = 0;
	bits = 0;
	cols = 0;
	
	while (srcl > 0)
	{
		c = *s++;
		srcl--;
		if (c > 255)
		{
			// error in base64 encoding
#ifdef _KERNELMODE
			ExFreePoolWithTag(dest,'bs64');
#else
			free (dest);
#endif
			dest = NULL;
                        if (len)
                            *len = 0;
			goto __exit;
		}
		
		bits += c;
		char_count++;
		
		if (char_count == 3)
		{
			*d++ = b64alphabet[bits >> 18];
			*d++ = b64alphabet[(bits >> 12) & 0x3f];
			*d++ = b64alphabet[(bits >> 6) & 0x3f];
			*d++ = b64alphabet[bits & 0x3f];
			
			cols += 4;
			
			if (cols == BASE64_MAX_LINE_LENGTH)
			{
				*d++ = '\r';
				*d++ = '\n';
				cols = 0;
			}
			
			bits = 0;
			char_count = 0;
		}
		else
		{
			bits <<= 8;
		}
	}
	
	if (char_count != 0)
	{
		bits <<= 16 - (8 * char_count);
		
		*d++ = b64alphabet[bits >> 18];
		*d++ = b64alphabet[(bits >> 12) & 0x3f];
		
		if (char_count == 1)
		{
			*d++ = '=';
			*d++ = '=';
		}
		else
		{
			*d++ = b64alphabet[(bits >> 6) & 0x3f];
			*d++ = '=';
		}
		if (cols > 0)
		{
			*d++ = '\r';
			*d++ = '\n';
		}
	}

__exit:	
	return (char*)dest;
}

//***********************************************************************
// void Base64Initialize ()                                                                     
//
// Initialize base64 encoder/decoder
//
//************************************************************************/
void Base64Initialize ()
{
	int i = 0;

	// initialize b64 stuff
	for (i = (sizeof(b64alphabet)) - 1; i >= 0 ; i--)
	{
		inalphabet[b64alphabet[i]] = 1;
		decoder[b64alphabet[i]] = (unsigned char) i;
	}
}
