/*
 *	routines to handle com (serial) ports in synchronous mode (non-overlapped)
 *  -vx-
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <io.h>
#include <fcntl.h>
#include "comport.h"
#ifdef WIN32
#include <winsock2.h>
#endif

/*
*	check for "OK" on com port. if checkstring is specified, com port is checked for that instead of OK
*
*/
int com_checkok (IN int comfd, OPTIONAL IN char* checkstring)
{
	char line [1024] = {0};
	char comparestr [] = "OK";
	char* p = NULL;
	int res = -1;

	if (!comfd)
		return -1;

	/// read line
	res = com_readline(comfd, line, 1024, NULL);
	if (res != 0)
		return -1;
	
	/// compare
	if (checkstring)
		p = checkstring;
	else
		p = comparestr;
	
	strupr (line);
	if (strstr (line,p) == (char*)line)
		return 0;

	return -1;
}

/*
*	print a formatted string to com port
*
*/
int com_printf (IN int comfd, OPTIONAL OUT unsigned long* sent, IN char* formatstring, ...)
{
	int res = -1;
	va_list ap;
	char buf [1024] = {0};
	unsigned long len = 0;

	if (!comfd || !formatstring)
		return -1;

	/// print formatted buffer
	va_start (ap,formatstring);
	vsnprintf(buf, sizeof (buf), formatstring, ap); 
	va_end (ap);

	/// send buffer
	len = (unsigned long)strlen (buf);
	res = com_write (comfd,buf,len,sent);
	
	return res;
}

/*
*	read a line (until EOL) from com port
*
*/
int com_readline (IN int comfd, IN void* outbuffer, IN unsigned long outbufsize, OPTIONAL OUT unsigned long* received)
{
	int res = -1;
	char* p = NULL;
	int ok = 0;
	unsigned long recvlen = 0;
	unsigned long bytesread = 0;

	if (!comfd || !outbuffer || !outbufsize)
		return -1;
	memset (outbuffer,0,outbufsize);
	if(received)
		*received = 0;

	/// receive data until EOL
	p=outbuffer;
	while (recvlen < outbufsize)
	{
		/// read char by char
		if (com_read (comfd,p,1,&bytesread) != 0)
			break;
		if (bytesread == 0)
			break;
		recvlen+=bytesread;

		// check for end-of-line
		if (*p == '\r' || *p == '\n')
		{
			if (*p == '\n')
			{
				// finish on \n
				ok = 1;
				break;
			}
		}

		// read next chars
		p+=bytesread;
	}

	// check errors
	if (!ok)
		return -1;

	if(received)
		*received = recvlen;
	return 0;
}

/*
*	read data from com port. outbuffer must be big enough to hold at least bytestoread bytes 
*
*/
int com_read (IN int comfd, IN void* outbuffer, IN unsigned long bytestoread, OPTIONAL OUT unsigned long* received)
{
	unsigned long readbytes  = 0;

	if (!comfd || !outbuffer)
		return -1;
	if (bytestoread == 0)
		return 0;

	if (received)
		*received = 0;

#if defined (WIN32) || defined (WINCE)
	if (!ReadFile((HANDLE)comfd,outbuffer,bytestoread,&readbytes,NULL))
		return -1;
#else
	/// unix code goes here
	readbytes = read(comfd,outbuffer,bytestoread);
	if ((int)readbytes == -1)	
		return -1;
#endif

	if (received)
		*received = readbytes;
	return 0;
}

/*
*	write data to com port
*
*/
int com_write (IN int comfd, IN void* buffer, IN unsigned long size, OPTIONAL OUT unsigned long* sent)
{
	unsigned long written  = 0;

	if (!comfd || !buffer)
		return -1;
	if (size == 0)
		return 0;
	
	if (sent)
		*sent = 0;
	
#if defined (WIN32) || defined (WINCE)
	if (!WriteFile((HANDLE)comfd,buffer,size,&written,NULL))
		return -1;
#else
	/// unix code goes here
	written = write(comfd,p,size);
	if ((int)written == -1)	
		return -1;
#endif

	if (sent)
		*sent = written;
	return 0;
}

/*
*	open com port
*
*/
int com_open (OUT int* comfd, IN char* portname, IN int baudrate, IN int parity, IN int bytesize, IN int stopbits)
{
	int fd = 0;
	char name [260] = {0};
#if defined (WIN32) || defined (WINCE)
	DCB dcbcfg = {0};
	COMMTIMEOUTS commtimeout = {0};
#endif

	if (!comfd || !portname || !baudrate)
		return -1;
	*comfd = 0;

#if defined (WIN32) || defined (WINCE)
	/// open com port
	snprintf (name,260,"\\\\.\\%s",portname);
	fd = (int)CreateFile(name, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	if (fd == (int)INVALID_HANDLE_VALUE)
		return -1;
	
	/// configure port
	dcbcfg.BaudRate = (unsigned long) baudrate;
	dcbcfg.StopBits = stopbits;
	dcbcfg.Parity = parity;
	dcbcfg.ByteSize = bytesize;
	if(!SetCommState((HANDLE)fd,&dcbcfg))
	{
		com_close(fd);
		return -1;
	}
	
	/// set timeouts
	commtimeout.ReadIntervalTimeout = 3;
	commtimeout.ReadTotalTimeoutMultiplier = 3;
	commtimeout.ReadTotalTimeoutConstant = 2;
	commtimeout.WriteTotalTimeoutMultiplier = 3;
	commtimeout.WriteTotalTimeoutConstant = 2;	
	if(!SetCommTimeouts((HANDLE)fd,&commtimeout))
	{
		com_close(fd);
		return -1;
	}
#else
	/// unix code goes here
	strncpy (name, portname, 260);
	fd = open (name, O_RDWR|O_BINARY);
	if (!fd)
		return -1;
#endif

	*comfd = fd;
	return 0;
}

/*
*	close com port
*
*/
int com_close (IN int comfd)
{
	if (!comfd)
		return -1;

#if defined (WIN32) || defined (WINCE)
	CloseHandle((HANDLE)comfd);
#else
	/// close port
	close (comfd);
#endif
	return 0;
}

/*
*	flush data on com port
*
*/
int com_flush (IN int comfd)
{
	if (!comfd)
		return -1;

#if defined (WIN32) || defined (WINCE)
	PurgeComm((HANDLE)comfd,PURGE_RXCLEAR | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_TXABORT);
#else
	if (commit (comfd) == -1)
		return -1;
#endif

	return 0;
}

/*
*	enable or disable echo on com port
*
*/
int com_setecho (IN int comfd, IN int enabled)
{
	int res = -1;
	unsigned long sent = 0;
	char command [260]={0};

	if (enabled)
		strcpy (command,"ATE1\r\n");
	else
		strcpy (command,"ATE0\r\n");

	/// send command
	res = com_printf(comfd,&sent,command);
	if (sent == strlen (command))
		return 0;

	return -1;
}
