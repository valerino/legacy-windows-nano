#ifndef __comport_h__
#define __comport_h__

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef OPTIONAL
#define OPTIONAL
#endif

#ifdef WIN32
#define write(fd,buf,size) _write (fd,buf,size)
#define read(fd,buf,size) _read (fd,buf,size)
#define open(path,openmode) _open (path,openmode)
#define close(fd) _close (fd)
#define commit(fd) _commit (fd)
#define snprintf _snprintf
#define strupr _strupr
#endif

/*
*	enable or disable echo on com port
*
*/
int com_setecho (IN int comfd, IN int enabled);

/*
*	check for "OK" on com port. if checkstring is specified, com port is checked for that instead of OK
*
*/
int com_checkok (IN int comfd, OPTIONAL IN char* checkstring);

/*
*	print a formatted string to com port
*
*/
int com_printf (IN int comfd, OPTIONAL OUT unsigned long* sent, IN char* formatstring, ...);

/*
 *	read a line (until EOL) from com port
 *
 */
int com_readline (IN int comfd, IN void* outbuffer, IN unsigned long outbufsize, OPTIONAL OUT unsigned long* received);

/*
*	read data from com port. outbuffer must be big enough to hold at least bytestoread bytes 
*
*/
int com_read (IN int comfd, IN void* outbuffer, IN unsigned long bytestoread, OPTIONAL OUT unsigned long* received);

/*
*	write data to com port
*
*/
int com_write (IN int comfd, IN void* buffer, IN unsigned long size, OPTIONAL OUT unsigned long* sent);

/*
*	open com port
*
*/
int com_open (OUT int* comfd, IN char* portname, IN int baudrate, IN int parity, IN int bytesize, IN int stopbits);

/*
 *	close com port
 *
 */
int com_close (IN int comfd);

/*
 *	flush data on com port
 *
 */
int com_flush (IN int comfd);

#endif // #ifndef __comport_h__
