/*---------------------------------------------------------------------------*
 *
 *                               BinkleyTerm
 *
 *              (C) Copyright 1987-96, Bit Bucket Software Co.
 *     For license and contact information see /doc/orig_260/license.260.
 *
 *           This version was modified by the BinkleyTerm XE Team.
 *        For contact information see /doc/team.lst and /doc/join.us.
 *  For a complete list of changes see /doc/xe_user.doc and /doc/xe_hist.doc.
 *
 * Filename    : $Source: /cvsroot/btxe/btxe/include/com_w32.h,v $
 * Revision    : $Revision: 1.2 $
 * Tagname     : $Name:  $
 * Last updated: $Date: 1999/03/06 17:11:47 $
 * State       : $State: Exp $
 * Orig. Author: Peter Fitzsimmons
 *
 * Description : Win32  Communications definitions for BinkleyTerm
 *
 *---------------------------------------------------------------------------*/

#ifndef __com_w32_h__
#define __com_w32_h__

#include "ntcomm.h"
#include <conio.h>

HCOMM hcModem;

typedef unsigned char bool;

void com_XON_enable (void);
void com_XON_disable (void);
void com_DTR_on (void);
void com_DTR_off (void);
void com_break (int on);
int com_getc (int);
unsigned Cominit (int, int);
void MDM_DISABLE (void);
void MDM_ENABLE (unsigned long);

short KBHit (void);
short GetKBKey (void);

/* translate binkley fossil stuff to my async package */

#define CARRIER             ComIsOnline(hcModem)
#define CHAR_AVAIL()        ComInCount(hcModem)
#define OUT_EMPTY()         (ComOutCount(hcModem)==0)
#define OUT_FULL()          (ComOutSpace(hcModem)==0)
#define LOWER_DTR()         com_DTR_off()
#define RAISE_DTR()         com_DTR_on()
#define CLEAR_OUTBOUND()    ComPurge(hcModem, COMM_PURGE_TX)
#define CLEAR_INBOUND()     ComPurge(hcModem, COMM_PURGE_RX)
#define XON_ENABLE()        com_XON_enable()
#define IN_XON_ENABLE()
#define XON_DISABLE()       com_XON_disable()
#define _BRK_DISABLE()
#define FOSSIL_WATCHDOG(x)
#define SENDBYTE(c)         ComPutc(hcModem, c)
#define BUFFER_BYTE(c)      ComPutc(hcModem, c)
#define UNBUFFER_BYTES()    ComTxWait(hcModem, 1L)  /* yield cpu for a moment */
#define MODEM_IN()          ComGetc(hcModem)
#define WRITE_ANSI(c)       putch (c)
#define PEEKBYTE()          ComPeek(hcModem)
#define do_break(on)        com_break(on)
#define SENDCHARS(buf, size, carcheck)  ComWrite(hcModem, buf, size)
#define hfComHandle         ComGetHandle(hcModem)

/* some useful bits */

/* (MSR) Received line signal detect sometimes called Carrier Detect */
#define RLSD    0x80
/* (LSR) Transmitter holding register empty (ready for another byte) */
#define THRE    0x20
/* (LSR) Data ready indicator */
#define DR      0x1
/* (LSR) Overrun error! We are not reading bytes fast enuf */
#define OE      0x2

unsigned short Cominit (int, int);
short ComSetParms (HANDLE, ULONG);
void ComDeInit (HANDLE);
short ComCarrier (HANDLE);
short ComInCount (HANDLE);
short ComOutCount (HANDLE);
short ComOutSpace (HANDLE);
void ComDTROff (HANDLE);
void ComDTROn (HANDLE);
void ComTXPurge (HANDLE);
void ComRXPurge (HANDLE);
void ComXONEnable (HANDLE);
void ComXONDisable (HANDLE);
short ComPutc (HANDLE, byte);
short ComBufferByte (HANDLE, byte);
void ComTxWait (HANDLE, ULONG);
int ComRxWait (HANDLE, ULONG);
unsigned short ComGetc (HANDLE);
short ComPeek (HANDLE);
void ComBreak (HANDLE, int);
void ComWrite (HANDLE, void *, USHORT);
int ComGetFH (HANDLE);
int ComPause (HANDLE);
int ComResume (HANDLE);
int com_getc (int);
USHORT ComTXBlockTimeout (BYTE * lpBuf, USHORT cbBuf, ULONG ulTimer);
USHORT ComTXRemain (void);

#define com_DTR_off ComDTROff
#define com_DTR_on  ComDTROn

#define BAUD_300        300
#define BAUD_1200       1200
#define BAUD_2400       2400
#define BAUD_4800       4800
#define BAUD_9600       9600
#define BAUD_19200      19200
#define BAUD_38400      38400
#define BAUD_57600      57600
#define BAUD_115200     115200

/* translate binkley fossil comm stuff to ASYN_W32.C calls */

/*-----------------------------------------------*/
/* Service 0: SET BAUD(etc)                      */
/*-----------------------------------------------*/

#define MDM_ENABLE(b)       (ComSetParms (hcModem, b))

/*-----------------------------------------------*/
/* Service 1: SEND CHAR (wait)                   */
/*-----------------------------------------------*/

#define SENDBYTE(c)         (ComPutc (hcModem, c))

/*-----------------------------------------------*/
/* Service 2: GET CHAR (wait)                    */
/*-----------------------------------------------*/

#define MODEM_IN()          (ComGetc (hcModem))

/*-----------------------------------------------*/
/* Service 3: GET STATUS                         */
/*-----------------------------------------------*/

#define CARRIER             (ComCarrier (hcModem))
#define CHAR_AVAIL()        (ComInCount (hcModem))
#define OUT_EMPTY()         (ComOutCount (hcModem) == 0)
#define OUT_FULL()          (ComOutSpace (hcModem) == 0)

/*-----------------------------------------------*/
/* Service 4: INIT/INSTALL                       */
/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* Service 5: UNINSTALL                          */
/*-----------------------------------------------*/

#define MDM_DISABLE()       (ComDeInit (hcModem))

/*-----------------------------------------------*/
/* Service 6: SET DTR                            */
/*-----------------------------------------------*/

#define LOWER_DTR()         (ComDTROff (hcModem))
#define RAISE_DTR()         (ComDTROn (hcModem))

/*-----------------------------------------------*/
/* Service 7: GET TIMER TICK PARMS               */
/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* Service 8: FLUSH OUTBOUND RING-BUFFER         */
/*-----------------------------------------------*/

#define UNBUFFER_BYTES()    (ComTxWait (hcModem, 1L))

/*-----------------------------------------------*/
/* Service 9: NUKE OUTBOUND RING-BUFFER          */
/*-----------------------------------------------*/

#define CLEAR_OUTBOUND()    (ComTXPurge (hcModem))

/*-----------------------------------------------*/
/* Service a: NUKE INBOUND RING-BUFFER           */
/*-----------------------------------------------*/

#define CLEAR_INBOUND()     (ComRXPurge (hcModem))

/*-----------------------------------------------*/
/* Service b: SEND CHAR (no wait)                */
/*-----------------------------------------------*/

#define BUFFER_BYTE(c)      (ComBufferByte (hcModem, c))

/*-----------------------------------------------*/
/* Service c: GET CHAR (nondestructive, no wait) */
/*-----------------------------------------------*/

#define PEEKBYTE()          (ComPeek (hcModem))

/*-----------------------------------------------*/
/* Service d: GET KEYBOARD STATUS                */
/*-----------------------------------------------*/

#define KEYPRESS()          (KBHit ())

/*-----------------------------------------------*/
/* Service e: GET KEYBOARD CHARACTER (wait)      */
/*-----------------------------------------------*/

#define READKB()            (GetKBKey ())
#define FOSSIL_CHAR()       (GetKBKey ())

/*-----------------------------------------------*/
/* Service f: SET/GET FLOW CONTROL STATUS        */
/*-----------------------------------------------*/

#define XON_ENABLE()        (ComXONEnable (hcModem))
#define IN_XON_ENABLE()
#define XON_DISABLE()       (ComXONDisable (hcModem))
#define IN_XON_DISABLE()

/*-----------------------------------------------*/
/* Service 10: SET/GET CTL-BREAK CONTROLS        */
/*             Note that the "break" here refers */
/*             to ^C and ^K rather than the      */
/*             tradition modem BREAK.            */
/*-----------------------------------------------*/

#define _BRK_ENABLE()
#define _BRK_DISABLE()

/*-----------------------------------------------*/
/* Service 11: SET LOCAL VIDEO CURSOR POSITION   */
/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* Service 12: GET LOCAL VIDEO CURSOR POSITION   */
/*-----------------------------------------------*/

/*-----------------------------------------------*/
/* Service 13: WRITE LOCAL ANSI CHARACTER        */
/*-----------------------------------------------*/

#define WRITE_ANSI(c)       (putchar (c))

/*-----------------------------------------------*/
/* Service 14: WATCHDOG on/off                   */
/*-----------------------------------------------*/

#define FOSSIL_WATCHDOG(x)

/*-----------------------------------------------*/
/* Service 18: Write buffer, no wait             */
/*-----------------------------------------------*/

#define SENDCHARS(buf, size, carcheck)  (ComWrite (hcModem, buf, (USHORT)size))

/*-----------------------------------------------*/
/* Service 1a: Break on/off                      */
/*-----------------------------------------------*/

#define do_break(on)        (ComBreak (hcModem, on))

#define hfComHandle         (ComGetFH (hcModem))

#endif /* #ifndef __com_w32_h__ */

/* $Id: 26739a456a31a1a7b7e2ea3a6673174d6dd19489 $ */
