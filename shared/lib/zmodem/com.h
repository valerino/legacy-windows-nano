/*---------------------------------------------------------------------------*
 *
 *                               BinkleyTerm
 *
 *              (C) Copyright 1987-96, Bit Bucket Software Co.
 *     For license and contact information see /doc/orig_260/license.260.
 *
 *           This version was modified by the BinkleyTerm XE Team.
 *        For contact information see /doc/team.lst and /doc/join.us.
 *  For a complete list of changes see /doc/xe_user.doc and /doc/xe_hist.doc.
 *
 * Filename    : $Source: /cvsroot/btxe/btnt/sources/com.h,v $
 * Revision    : $Revision: 1.1 $
 * Tagname     : $Name:  $
 * Last updated: $Date: 2005/01/01 15:18:12 $
 * State       : $State: Exp $
 *
 * Description : Communications definitions for BinkleyTerm
 *
 *---------------------------------------------------------------------------*/

#ifndef __com_h__
#define __com_h__

#define BITS_7          0x02
#define BITS_8          0x03
#define STOP_1          0x00
#define STOP_2          0x04
#define ODD_PARITY      0x08
#define EVEN_PARITY     0x18
#define NO_PARITY       0x00

/* Bit definitions for the driver flags */

#define USE_XON         0x01
#define USE_CTS         0x02
#define USE_DSR         0x04
#define OTHER_XON       0x08

#define BRK             0x01
#define MDM             0x02

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
#include "com_lnx.h"
#else
#include "com_w32.h"
#endif

#endif //#ifndef __com_h__

/* $Id: 67186d503c9d29f3ff90b4d88f739c7daebabedc $ */
