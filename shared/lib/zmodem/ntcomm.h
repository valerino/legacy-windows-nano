/*
 * Maximus Version 3.02
 * Copyright 1989, 2002 by Lanius Corporation.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef __NTCOMM_H_DEFINED
#define __NTCOMM_H_DEFINED

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include "comqueue.h"

//#define COMMAPI  NTstdcall  /* Standard NT API calling convention */
/*#define DEFAULT_COMM_MASK   (EV_ERR | EV_RLSD | EV_RXFLAG)*/
#define DEFAULT_COMM_MASK   (EV_ERR | EV_RLSD)

typedef struct
{
  HANDLE h;                 /* Handle of the physical com port file */

  COMQUEUE cqTx;            /* Transmit queue */
  COMQUEUE cqRx;            /* Receive queue */

  HANDLE hRx, hTx, hMn;     /* Handles for read and write threads */
  HANDLE hevTx, hevRx;      /* Semaphores for the tx/rx threads */
  HANDLE hevTxDone;         /* Pending transmit has completed */
  HANDLE hevRxWait, hevTxWait;  /* Waiting for input/output buf to clear */
  HANDLE hevRxPause, hevTxPause;  /* Stop transmitter for compause/resume */
  HANDLE hevRxDone;         /* Pending receive has completed */
  HANDLE hevMonDone;        /* Pending monitor has completed */

  BOOL fDCD;                /* Current status of DCD */
  volatile BOOL fDie;       /* True if we are trying to kill threads */
  DWORD dwCtrlC;            /* How many ^C's have we received from user? */
  volatile DWORD cThreads;  /* Number of active threads */

  COMMTIMEOUTS ct;          /* Timeout values */
} HCOMMOBJ, *HCOMM;

#define COMM_PURGE_RX 1
#define COMM_PURGE_TX 2
#define COMM_PURGE_ALL  (COMM_PURGE_RX | COMM_PURGE_TX)

BOOL ComOpenHandle(HANDLE hfComm, HCOMM *phc, DWORD dwRxBuf, DWORD dwTxBuf);
BOOL ComOpen(LPTSTR pszDevice, HCOMM *phc, DWORD dwRxBuf, DWORD dwTxBuf);
BOOL ComClose(HCOMM hc);
bool ComIsOnline(HCOMM hc);
BOOL ComWrite(HCOMM hc, PVOID pvBuf, DWORD dwCount);
BOOL ComRead(HCOMM hc, PVOID pvBuf, DWORD dwBytesToRead, PDWORD pdwBytesRead);
int ComGetc(HCOMM hc);
int ComPeek(HCOMM hc);
BOOL ComPutc(HCOMM hc, int c);
BOOL ComRxWait(HCOMM hc, DWORD dwTimeOut);
BOOL ComTxWait(HCOMM hc, DWORD dwTimeOut);
DWORD ComInCount(HCOMM hc);
DWORD ComOutCount(HCOMM hc);
DWORD ComOutSpace(HCOMM hc);
BOOL ComPurge(HCOMM hc, DWORD fBuffer);
HANDLE ComGetHandle(HCOMM hc);
BOOL ComGetDCB(HCOMM hc, LPDCB pdcb);
USHORT ComSetDCB(HCOMM hc, LPDCB pdcb);
BOOL ComSetBaudRate(HCOMM hc, DWORD dwBps, BYTE bParity, BYTE bDataBits, BYTE bStopBits);
BOOL ComPause(HCOMM hc);
BOOL ComResume(HCOMM hc);
BOOL ComWatchDog(HCOMM hc, BOOL fEnable, DWORD ulTimeOut);

#endif /* __NTCOMM_H_DEFINED */

