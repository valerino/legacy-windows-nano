/*---------------------------------------------------------------------------*
 *
 *                               BinkleyTerm
 *
 *              (C) Copyright 1987-96, Bit Bucket Software Co.
 *     For license and contact information see /doc/orig_260/license.260.
 *
 *           This version was modified by the BinkleyTerm XE Team.
 *        For contact information see /doc/team.lst and /doc/join.us.
 *  For a complete list of changes see /doc/xe_user.doc and /doc/xe_hist.doc.
 *
 * Filename    : $Source: /cvsroot/btxe/btxe/src/asyn_w32.c,v $
 * Revision    : $Revision: 1.4 $
 * Tagname     : $Name:  $
 * Last updated: $Date: 2000/06/09 16:01:18 $
 * State       : $State: Exp $
 *
 * Description : async module for Win32
 *
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include "com.h"

/*
 * adopted to BTXE/Win32 and ntcomm.dll by alexander sanda, 97-02-22
 * thanks to scott for providing the necessary information about
 * ntcomm.dll
 */

// Rewrote parts of the async routines for ntcomm.dll and made it work
// finally also under Win95 - HJK 98/2/7

char ascii_comport[] = "COM";

HCOMM hcModem = 0;              /* comm.dll handle */

#define RBSIZE  8200
#define TBSIZE  8200

DWORD rBufsize = RBSIZE;
DWORD tBufsize = TBSIZE;

// HJK 99/01/25 - an attempt to make Bink work with CFos

void com_DTR_on (void)
{
  DCB dcb;
  DWORD dwIgnore;

  if (ComGetDCB (hcModem, &dcb))
  {
	dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;  /* raise DTR */
    ComSetDCB (hcModem, &dcb);
  }
  else
  {
	  exit (3);
  }
}

void com_DTR_off (void)
{
  DCB dcb;
  DWORD dwIgnore;

    if (ComGetDCB (hcModem, &dcb))
    {
      dcb.fDtrControl = DTR_CONTROL_DISABLE;  /* lower DTR */
      ComSetDCB (hcModem, &dcb);
      com_kick ();
    }
    else
    {
      exit (3);
    }
  }
}

void com_XON_disable (void)
{
  DCB dcb;

  if (ComGetDCB (hcModem, &dcb))
  {
    // disable auto Xmit and recv flow control

    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;
    ComSetDCB (hcModem, &dcb);
    com_kick ();
  }
  else
  {
    exit (3);
  }
}

void com_XON_enable (void)
{
  DCB dcb;

  if (ComGetDCB (hcModem, &dcb))
  {
    // enable auto Xmit and recv flow control

    dcb.fOutX = TRUE;
    dcb.fInX = TRUE;
    ComSetDCB (hcModem, &dcb);
  }
  else
  {
    exit (3);
  }
}

int com_getc (int t)
{

  if (t)
    ComRxWait (hcModem, t * 1000L);
  return ComGetc (hcModem);
}

/* com_break() : start break if on==TRUE, stop break if on==FALSE */

void com_break (int on)
{
  DWORD dwIgnore;

  if (on)
    SetCommBreak (ComGetHandle (hcModem));
  else
    ClearCommBreak (ComGetHandle (hcModem));

  ClearCommError (ComGetHandle (hcModem), &dwIgnore, NULL);
}

void MDM_ENABLE (unsigned long rate)
{
  BYTE _parity;
  BYTE databits;
  BYTE stopbits;

  if (!hcModem)
    return;

  databits = 8;
  stopbits = ONESTOPBIT;

  switch (parity)
  {
	  case NO_PARITY:
		_parity = NOPARITY;
		break;
	  case ODD_PARITY:
		_parity = ODDPARITY;
		break;
	  case EVEN_PARITY:
		_parity = EVENPARITY;
		break;
	  default:
		_parity = NOPARITY;
  }
  ComSetBaudRate (hcModem, rate, _parity, databits, stopbits);
}

void MDM_DISABLE (void)
{
  if (hcModem)
  {
    ComClose (hcModem);
    hcModem = 0;
  }
}

unsigned short Com_init (int port)
{
  USHORT rc;
  char *s;
  char dev[256];
  char* port_device;
  DCB dcb;

  port_device = ascii_comport;

  sprintf (dev, "%s%d", port_device, port + 1);

  printf ("Attempting to open async device %s\r\n", dev);

  // this should also work under Win32, so I didn't see any reason for removing it - alex

  s = getenv ("RBUF");          /* MB 94-01-02 */
  if (s)
    rBufsize = (USHORT) min (32000, atoi (s));
  else
    rBufsize = RBSIZE;

  s = getenv ("TBUF");
  if (s)
    tBufsize = (USHORT) min (32000, atoi (s));
  else
    tBufsize = TBSIZE;

  // alex, 97-02-22: Instead of returning a error code, Win32 APIs always
  // return TRUE if everything went ok, FALSE otherwise.
  // You have to explicitely obtain the error code with GetLastError()

  rc = (!ComOpen ((LPTSTR) dev, &hcModem, rBufsize, tBufsize));
  if (rc)
  {
    return (rc);
  }

  // HJK - 98/3/23 - Solves a bug with Windows 95 which caused Bink to
  // hang at exit. Fixed in new NTCOMM.DLL (98/05/11) but Com needs to be
  // closed at exit now.

  atexit (MDM_DISABLE);

  // Some specific COM settings, because MODE.COM from Windows95
  // has not all features which NT has - HJK, 98/6/12

  if (ComGetDCB (hcModem, &dcb))
  {
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fOutxCtsFlow = TRUE;
    dcb.fDsrSensitivity = TRUE;
    dcb.fDtrControl = DTR_CONTROL_ENABLE;
    ComSetDCB (hcModem, &dcb);
  }
  else
  {
    exit (3);
  }

  return (rc);
}

unsigned short Cominit (int port, int failsafe)
{
  int ret = 0x1954;

  failsafe = failsafe;

  if (!hcModem)
  {
    if (Com_init (port))
    {
      ret = 0;
    }
  }
  else
    ComResume (hcModem);

  return ret;
}

/* force transmitter to go */

void com_kick (void)
{
	ComPurge (hcModem, COMM_PURGE_TX);  // was: COMM_PURGE_RX; NS290400 
}

/* $Id: 86c5394fdd38569f93b67f712f41897c03fe77c2 $ */
