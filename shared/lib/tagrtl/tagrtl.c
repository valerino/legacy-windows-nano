/************************************************************************
// Tagged values routines. 
// 
// All *Create* routines assume a properly sized TaggedValue buffer already allocated (size of buffer + 
//	sizeof (TAGGED_VALUE) + sizeof (TCHAR)
// 
// -vx-
// 
/ ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
	#include <winsock2.h>
#endif
#include "tagrtl.h"

/************************************************************************
// unsigned long TaggedGetUlong (PTAGGED_VALUE pBuffer)
// 
// get ulong from TAGGED_VALUE buffer
// 
/ ************************************************************************/
unsigned long TaggedGetUlong (PTAGGED_VALUE pBuffer)
{
	unsigned long value = 0;

	if (!pBuffer)
		goto __exit;

	// get value
	memcpy (&value,(unsigned char*)&pBuffer->value,sizeof (unsigned long));

__exit:
	return value;
}

/************************************************************************
// void* TaggedGetValue (PTAGGED_VALUE pBuffer)
// 
// get value from TAGGED_VALUE buffer
// 
/ ************************************************************************/
void* TaggedGetValue (PTAGGED_VALUE pBuffer)
{
	unsigned char* p = NULL;

	if (!pBuffer)
		goto __exit;

	// get value
	p = (unsigned char*)&pBuffer->value;

__exit:
	return p;
}

/************************************************************************
// PTAGGED_VALUE TaggedFindValueById (PTAGGED_VALUE pBuffer, unsigned long BufferSize, unsigned long id)
// 
// search value by id in a tagged value array buffer, returning TAGGED_VALUE
// 
/ ************************************************************************/
PTAGGED_VALUE TaggedFindValueById (PTAGGED_VALUE pBuffer, unsigned long BufferSize, unsigned long id)
{
	PTAGGED_VALUE q = NULL;
	unsigned char* p = NULL;

	if (!BufferSize || !pBuffer)
		return NULL;
	
	// search
	p = (unsigned char*)pBuffer;
	while ((int)BufferSize > 0)
	{
		q = (PTAGGED_VALUE)p;
		
		// search id
		if (q->tagid == id)
			return q;

		// next
		BufferSize-=(q->size + sizeof (TAGGED_VALUE));
		p+=(q->size + sizeof (TAGGED_VALUE));
	}

	return NULL;
}

/*
 *	reverse ulong value if needed
 *
 */
void TaggedReverseUlong (PTAGGED_VALUE pTaggedValue)
{
	int value = 0;
	memcpy (&value,&pTaggedValue->value,sizeof (unsigned long));
#ifdef _KERNELMODE
	value = _byteswap_ulong(value);
#else
	value = htonl (value);
#endif
	memcpy (&pTaggedValue->value,&value,sizeof (unsigned long));
}

/************************************************************************
// unsigned long TaggedCreateUlong (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned long value, int allocate)
// 
// create tagged value from ulong value
// 
/ ************************************************************************/
unsigned long TaggedCreateUlong (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned long value)
{
	unsigned long val = value;
	unsigned long len = sizeof (unsigned long) + sizeof (TAGGED_VALUE);

	memset ((unsigned char*)pTaggedValue,0,len);

	// build value
	pTaggedValue->tagid = id;
	pTaggedValue->size = sizeof (unsigned long);
	memcpy (&pTaggedValue->value,&val,sizeof (unsigned long));

	return (sizeof (TAGGED_VALUE) + pTaggedValue->size); 
}

/************************************************************************
// unsigned long TaggedCreateBuffer (PTAGGED_VALUE pTaggedValue, unsigned long id, void* pBuffer, unsigned long size)
// 
// create tagged value from sized buffer
// 
/ ************************************************************************/
unsigned long TaggedCreateBuffer (PTAGGED_VALUE pTaggedValue, unsigned long id, void* pBuffer, unsigned long size)
{
	unsigned long len = size + sizeof (TAGGED_VALUE);
	
	memset ((unsigned char*)pTaggedValue,0,len);

	// build value
	pTaggedValue->tagid = id;
	pTaggedValue->size = size;
	memcpy (&pTaggedValue->value,pBuffer,size);

	return (sizeof (TAGGED_VALUE) + pTaggedValue->size); 
}

/************************************************************************
// unsigned long TaggedCreateUlongFromString (PTAGGED_VALUE pTaggedValue, unsigned long id, char* str)
// 
// create tagged value from ulong value (as string)
// 
/ ************************************************************************/
unsigned long TaggedCreateUlongFromString (PTAGGED_VALUE pTaggedValue, unsigned long id, char* str)
{
	unsigned long val = atol(str);
	
	return TaggedCreateUlong(pTaggedValue,id,val);
}

/************************************************************************
// unsigned long TaggedCreateString (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned char* str)
// 
// create tagged value from string
// 
/ ************************************************************************/
unsigned long TaggedCreateString (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned char* str)
{
	char *p = (char*)&pTaggedValue->value;
	char *dst = NULL;
	char *src = NULL;
	unsigned long len = 0;

	len = (unsigned long)strlen((char *)str) + sizeof (TAGGED_VALUE) + 1;
	memset ((unsigned char*)pTaggedValue,0,len);

	// build value
	pTaggedValue->tagid = id;
	dst = p;
	src = (char *)str;

	// strip terminators
	while (*src != 0x0d && *src != 0x0a && *src != 0x00)
	{
		*dst = *src;
		dst++;src++;
	}

	// zeroterminate
	*dst = 0x00;

	pTaggedValue->size = (unsigned long)strlen (p) + 1;

	return (sizeof (TAGGED_VALUE) + pTaggedValue->size); 
}

/************************************************************************
// unsigned long TaggedCreateStringW (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned short* str)
// 
// create tagged value from unicode UCS2 string
// 
/ ************************************************************************/
unsigned long TaggedCreateStringW (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned short* str)
{
	unsigned short* p = (unsigned short*)&pTaggedValue->value;
	unsigned short* dst = NULL;
	unsigned short* src = NULL;
	unsigned long len = 0;
#ifdef WIN32
	len = ((unsigned long)wcslen(str)*sizeof (unsigned short)) + sizeof (TAGGED_VALUE) + sizeof (unsigned short);
#else
	src = str;
	while(*src)
		src++;
	len = (unsigned long)(((src-str) * sizeof(unsigned short)) + sizeof (TAGGED_VALUE) + sizeof (unsigned short));
	src = NULL;
#endif
	memset ((unsigned char*)pTaggedValue,0,len);

	// build value
	pTaggedValue->tagid = id;
	dst = p;
	src = str;
	
	// strip terminators
	while (*src != (unsigned short)0x0d && *src != (unsigned short)0x0a && *src != (unsigned short)0x00)
	{
		*dst = *src;
		dst++;src++;
	}
	
	// zeroterminate
	*dst = (unsigned short)0x00;
#ifdef WIN32
	pTaggedValue->size = (unsigned long)(wcslen (p)*sizeof (unsigned short)) + sizeof (unsigned short);
#else
	dst = p;
	while(*dst)
		dst++;
	pTaggedValue->size = (unsigned long)(((dst-p)*sizeof(unsigned short)) + sizeof(unsigned short));
#endif
	return (sizeof (TAGGED_VALUE) + pTaggedValue->size); 
}
