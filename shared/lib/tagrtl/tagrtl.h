#ifndef __tagrtl_h__
#define __tagrtl_h__

// All *Create* routines assume a properly sized TaggedValue buffer already allocated (size of buffer + 
// sizeof (TAGGED_VALUE) + sizeof (TCHAR)


// generic tagged value struct
typedef struct __tagTAGGED_VALUE {
	unsigned long	tagid;
	unsigned long	size;
	unsigned long	value[0];
}TAGGED_VALUE, *PTAGGED_VALUE;	

// reverse ulong taggedvalue if needed
void TaggedReverseUlong (PTAGGED_VALUE pTaggedValue);

// get ulong from TAGGED_VALUE buffer, returns ulong value
unsigned long TaggedGetUlong (PTAGGED_VALUE pBuffer);

// get value from TAGGED_VALUE buffer, returns ptr
void* TaggedGetValue (PTAGGED_VALUE pBuffer);

// search value by id in a tagged value array buffer, returning PTAGGED_VALUE
PTAGGED_VALUE TaggedFindValueById (PTAGGED_VALUE pBuffer, unsigned long BufferSize, unsigned long id);

// create tagged value from ulong value, returns whole size
unsigned long TaggedCreateUlong (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned long value);

// create tagged value from sized buffer, returns whole size
unsigned long TaggedCreateBuffer (PTAGGED_VALUE pTaggedValue, unsigned long id, void* pBuffer, unsigned long size);

// create tagged value from ulong value (as string), returns whole size
unsigned long TaggedCreateUlongFromString (PTAGGED_VALUE pTaggedValue, unsigned long id, char* str);

// create tagged value from string, returns whole size
unsigned long TaggedCreateString (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned char* str);

// create tagged value from unicode UCS2 string, returns whole size
unsigned long TaggedCreateStringW (PTAGGED_VALUE pTaggedValue, unsigned long id, unsigned short* str);

#endif // #ifndef __tagrtl_h__
