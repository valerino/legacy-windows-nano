#ifndef __rkcfghandle_h__
#define  __rkcfghandle_h__

#ifdef __cplusplus
extern "C"
{
#endif

#include <uxml.h>

/*
*	read configuration file, optionally encrypted
*
*/
#if defined _KERNELMODE
int RkCfgRead (IN PUNICODE_STRING cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN OPTIONAL int asciicipherkey, OUT UXml **ConfigXml);
#else
int RkCfgRead (IN char* cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml);
int RkCfgReadW (IN WCHAR* cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml);
#endif // #if defined _KERNELMODE

/*
*	returns an Uxml describing the configuration in cfgmem.
*
*/
int RkCfgReadFromMemory (IN void* cfgmem, IN unsigned long size, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml);

/*
*	get core configuration
*
*/
int RkCfgGetCoreConfig( IN UXml *ConfigXml, OUT UXmlNode **ConfigNode);

/*
*	get configuration for the specified module
*
*/
int RkCfgGetModuleConfig(IN UXml *ConfigXml, IN char* ModuleName, IN char* ModuleType, OUT UXmlNode **ConfigNode);

/*
*	get specifed option and value for the input xml node
*
*/
int RkCfgGetModuleOption(IN UXmlNode *Node, OUT char** OptionName, OUT char** OptionValue);

/*
*	get specified parameter and value for the input node
*
*/
int RkCfgGetOptionParameter(IN UXmlNode *Node, OUT char** ParameterName, OUT char** ParameterValue);

#ifdef __cplusplus
}
#endif

#endif ///#ifndef __rkcfghandle_h__