#ifndef __base64_h__
#define __base64_h__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BASE64_MAX_LINE_LENGTH 76

void	Base64Initialize ();
char*	Base64Encode(void* src, unsigned long srcl, unsigned long* len);
void*	Base64Decode(char* src, unsigned long* len);

#endif // #ifndef __base64_h__

