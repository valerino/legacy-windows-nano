/*
 *  xmltest.c
 *  libtera
 *
 *  Created by xant on 2/18/06.
 *  Copyright 2006 T.E.4I. - All rights reserved.
 *
 */

#include "txml.h"

int main (int argc, char **argv)
{
	TXml *xml;
	//XmlNode *cfg;
	xml = XmlCreateContext();
	if(argv[1]) 
	{
		if(XmlParseFile(xml,argv[1]) == XML_NOERR)
		{
			char *dump = XmlDump(xml);
			printf("%s\n", dump);
			free(dump);
			/*
			cfg = XmlGetNode(xml,"/clientinfo");
			if(cfg)
				printf("%s",XmlDumpBranch(xml,cfg,0));
			*/
		}
	}
	XmlDestroyContext(xml);
	exit(0);
}
