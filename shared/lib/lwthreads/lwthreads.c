/* $Id: b2dc2decb02fd22a2c396d76c1600eebb6e82e8a $
 *
 * Threads management routines for lwids.
 * This 'module' wants to introduce a set of routines to handle threads inside lwids.
 * 
 * coded by xant <xant@xant.net>
 *
 */
#include <time.h>
#include <errno.h>
#include "lwthreads.h"

inspectionThread thList[MAX_INSPECTION_THREADS];
int thCount = 0;
pthread_t thMonitor; // TODO - use a mutex to access it
static pthread_cond_t tmcond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t tmlock = PTHREAD_MUTEX_INITIALIZER;
/*
 * Initialize the thread engine.
 * One monitor thread is started to let manage expiry and notify routines 
 */
int initThreadManager() {
    int i;
    memset(thList, 0, sizeof(thList));
    for(i = 0 ; i< MAX_INSPECTION_THREADS; i++) {
        pthread_mutex_init(&thList[i].stateLock, NULL);
        pthread_mutex_init(&thList[i].sharedLock, NULL);
    }
    if(pthread_create(&thMonitor, NULL, monitorThreads, NULL) != 0) {
        ERROR("Can't start thread monitor!!");
        return(-1);
    }
    NOTICE("Thread subsystem started successfully");
    return(0);
}

/*
 * This routine stops the engine and free memory.
 * Thread's table is scanned against running threads to let them stop and join resources.
 */
void freeThreadManager() {
    int i, rc;
    DEBUG2("Made cancellation request to the Thread Manager.");
    if(pthread_cancel(thMonitor) != 0) {
        ERROR("Can't stop the Thread Manager (pthread_cancel() failure)");
    }
    if (pthread_self() != thMonitor) {
        rc = pthread_join(thMonitor, NULL);
        if(rc != 0) {
            ERROR("Can't join resources for the Thread Manager (%s)", strerror(rc));
        }
    }
    DEBUG2("Cleaning thread table...");
    for(i = 0 ; i< MAX_INSPECTION_THREADS; i++) {
        pthread_mutex_lock(&thList[i].stateLock);
        if(thList[i].thHandler && thList[i].state == TH_RUN) 
            pthread_cancel(thList[i].thHandler);
        pthread_mutex_unlock(&thList[i].stateLock);
    }
    //sleep(2);
    for(i = 0 ; i< MAX_INSPECTION_THREADS; i++) {
        pthread_mutex_lock(&thList[i].stateLock);
        if(thList[i].thHandler && thList[i].state != TH_CLEAN) {
            pthread_mutex_unlock(&thList[i].stateLock);
            rc = pthread_join(thList[i].thHandler, &thList[i].returnVal);
            if(rc != 0)
                ERROR("Error joining resources for thread %d (%s)", i, strerror(rc));
            if(thList[i].notify != NULL) {
                DEBUG3("Executing notify routine for thread %d", i);
                thList[i].notify(thList[i].returnVal, thList[i].notifyArg);
            }
            if(thList[i].sharedVars)
                clearShared(i);
            thList[i].state = TH_CLEAN;
            thCount--;
        }
        else {
            pthread_mutex_unlock(&thList[i].stateLock);
        }
        pthread_mutex_destroy(&thList[i].stateLock);
        pthread_mutex_destroy(&thList[i].sharedLock);
    }
    DEBUG2(("Thread table cleaned successfully "));
    NOTICE("Thread subsystem has been stopped successfully");
}

void exitMonitor(void *arg) {
    DEBUG2("Thread Manager successfully stopped.");
}

/* this is the main thread that periodically (THREAD_MONITOR_INTERVAL) checks 
 * for ended threads, executes the related notify routine if present and finally clean resources.
 */
void *monitorThreads(void *arg) {
    int i;
    time_t now;
    struct timespec sleep_time;
    pthread_cleanup_push(exitMonitor, NULL);
    for(;;) {
        sleep_time.tv_sec = time(NULL)+THREAD_MONITOR_INTERVAL;
        sleep_time.tv_nsec = 0;
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
        DEBUG3("monitorThreads cycle starts");
        for (i=0;i<MAX_INSPECTION_THREADS;i++) {
            inspectionThread *th = &thList[i];
            pthread_mutex_lock(&th->stateLock);
            switch(th->state) {
                case TH_DEFINED:
                    now = time(NULL);
                    if(th->timeout && th->timeout <= now) {
                        /* XXX - Error messages needed */
                        th->timeout = 0;
                    }
                    pthread_mutex_unlock(&th->stateLock);
                    break;
                case TH_RUN:
                    now = time(NULL);
                    if(th->timeout && th->timeout <= now) {
                        /* XXX - Notify messages needed */
                        pthread_mutex_unlock(&th->stateLock); /* XXX - mmmmm */
                        stopThread(i);
                    }
                    pthread_mutex_unlock(&th->stateLock); /* XXX - mmmmm */
                    break;
                case TH_OK:
                case TH_ERR:
                    DEBUG3("Thread %d finished ", i);
                    pthread_mutex_unlock(&th->stateLock);
                    int rc = pthread_join(th->thHandler, &th->returnVal);
                    if(rc != 0) {
                        ERROR("Error joining resources for thread %d (%s)", i, strerror(rc));
                    }
                    if(th->notify) {
                        DEBUG3("Executing notify routine for thread %d", i);
                        th->notify(th->returnVal, th->notifyArg);
                    }
                    /*
                    if(th->returnVal)
                        free(th->returnVal); // free returnVal if present
                    */
                    if(th->sharedVars)
                        clearShared(i);
                    /* XXX - i don't think it's necessary to destroy and reinit mutexes */
                    pthread_mutex_destroy(&th->stateLock);
                    pthread_mutex_destroy(&th->sharedLock);
                    memset(th, 0, sizeof(inspectionThread));
                    th->state = TH_CLEAN;
                    pthread_mutex_init(&th->stateLock, NULL);
                    pthread_mutex_init(&th->sharedLock, NULL);
                    thCount--;
                    break;
                default:
                    pthread_mutex_unlock(&th->stateLock);
            }
        }
        DEBUG3("monitorThreads cycle done");
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        int err = 0;
        err = pthread_mutex_lock(&tmlock);
        err = pthread_cond_timedwait(&tmcond, &tmlock, &sleep_time);
        if (err != ETIMEDOUT) {
            WARNING("monitorThreads exiting for signal on tmcond");
            pthread_exit(0);
        }
        err = pthread_mutex_unlock(&tmlock);
    }
    pthread_cleanup_pop(1);
}

/* 
 * All-in-one routine to instance a new thread... allocates needed resources and 
 * starts it immediately.
 */
threadId startThread(void *(*thRoutine)(), void *thArg,
    void (*notifyRoutine)(), void *notifyArg, time_t timeout)
{
    threadId id = instanceThread(thRoutine, thArg);
    if(id < 0)
        return(id);
    DEBUG2("Starting new thread with id %d", id);
    thList[id].notify = notifyRoutine;
    thList[id].notifyArg = notifyArg;
    thList[id].timeout = timeout;
    execThread(id);
    return(id);
}

/*
 * Allocates resources for a new thread and returns its id (to be used with other thread-related routines).
 * Thread is not executed but just istantiated. User can still change parameters and register a notify routine 
 * or a timeout * because he has to explicitly execute the thread using the execThread routine
 * (it's not started automatically).
 */
threadId instanceThread(void *(*thRoutine)(), void *thArg) {
    int i, idx;
    for(i = 0;i < MAX_INSPECTION_THREADS;i++) {
        pthread_mutex_lock(&thList[i].stateLock);
        if(thList[i].state == TH_CLEAN) {
            thList[i].state = TH_DEFINED;
            thList[i].routine = thRoutine;
            thList[i].routineArg.priv = thArg;
            thList[i].routineArg.id = i;
            thCount++;
            pthread_mutex_unlock(&thList[i].stateLock);
            return(i);
        }
        pthread_mutex_unlock(&thList[i].stateLock);
    }
    /* XXX - Error message needed */
    ERROR("Can't instanziate new thread... max reached?");
    return(-1);
}

/*
 * Execute the thread pointed by id.
 */
int execThread(threadId id) {
    pthread_mutex_lock(&thList[id].stateLock);
    if(thList[id].state != TH_DEFINED) {
        ERROR("Requested execution for undefined thread id %d", id);
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    thList[id].state = TH_RUN;
    if(pthread_create(&thList[id].thHandler, NULL, 
        thList[id].routine, (void *)&thList[id].routineArg) != 0)
    {
        thList[id].state = TH_ERR;
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    pthread_mutex_unlock(&thList[id].stateLock);
    return(0);
}

/*
 * Define a notify routine to be called when thread pointed by id has finished its execution.
 */
int setNotify (threadId id, void (*routine)(), void *arg) {
    pthread_mutex_lock(&thList[id].stateLock);
    if(thList[id].state != TH_DEFINED) {
        ERROR("called setNotify() for an undefined thread id %d", id);
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    thList[id].notify = routine;
    thList[id].notifyArg  = arg;
    pthread_mutex_unlock(&thList[id].stateLock);
    return(0);
}

int setNotifyArg(threadId id, void *arg) {
    pthread_mutex_lock(&thList[id].stateLock);
    if(thList[id].state != TH_DEFINED && thList[id].state != TH_RUN) {
        ERROR("called setNotify() for an undefined thread id %d", id);
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    thList[id].notifyArg  = arg;
    pthread_mutex_unlock(&thList[id].stateLock);
    return(0);
}

int setTimeout(threadId id, time_t timeStamp) {
    time_t now;
    pthread_mutex_lock(&thList[id].stateLock);
    if(thList[id].state != TH_DEFINED || thList[id].state != TH_RUN) {
        ERROR("called setTimeout() for an undefined thread id %d", id);
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    now = time(NULL);
    if(timeStamp <= now) {
        ERROR("requested timeout for thread %d is already expired", id);
        /* XXX - ERROR MESSAGE NEEDED */
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    thList[id].timeout = timeStamp;
    pthread_mutex_unlock(&thList[id].stateLock);
    return(0);
}

int stopThread(threadId id) {
    int err;
    pthread_mutex_lock(&thList[id].stateLock);
    if(thList[id].state != TH_RUN) {
        ERROR("called stopThread() for a not running thread id %d", id);
        pthread_mutex_unlock(&thList[id].stateLock);
        return(-1);
    }
    err = pthread_cancel(thList[id].thHandler);
    if (err != 0) {
        ERROR("Can't cancel thread %d : %s\n", id, strerror(err));
    }
    err = pthread_join(thList[id].thHandler, &thList[id].returnVal);
    if (err != 0) {
        ERROR("Can't join thread %d : %s\n", id, strerror(err));
    }
    thList[id].thHandler = 0;
    thList[id].state = TH_CLEAN;
    pthread_mutex_unlock(&thList[id].stateLock);
    thCount--;
    return(0);
}

thVar *selectShared(threadId id, const char *name) {
    thVar *var = thList[id].sharedVars;
    pthread_mutex_lock(&thList[id].sharedLock);
    while(var) {
        if(strcasecmp(name, var->Name) == 0) {
            pthread_mutex_unlock(&thList[id].sharedLock);
            return(var);
        }
        var = var->next;
    }
    pthread_mutex_unlock(&thList[id].sharedLock);
    return NULL;
}

int addShared(threadId id, const char *name, void *var, void (*clean)()) {
    thVar *head = thList[id].sharedVars;
    thVar *prev, *new;
    pthread_mutex_lock(&thList[id].sharedLock);
    while(head) {
        prev = head;
        if(strcasecmp(name, head->Name) == 0) {
            /* XXX - Warning message here */
            pthread_mutex_unlock(&thList[id].sharedLock);
            return(-1);
        }
        head = head->next;
    }
    new = (thVar *)malloc(sizeof(thVar));
    new->next = NULL;
    new->Name = name;
    new->Var = var;
    new->cleanUp = clean;
    pthread_mutex_init(&new->Lock, NULL);
    prev->next = new;
    pthread_mutex_unlock(&thList[id].sharedLock);
    return(0);
}

int lockShared(threadId id, const char *name) {
    thVar *var = selectShared(id, name);
    if(var == NULL)
        return(-1);
    if(pthread_mutex_lock(&var->Lock) != 0) {
        /* XXX - Errors here */
        return(-1);
    }
    return(0);
}

int unlockShared(threadId id, const char *name) {
    thVar *var = selectShared(id, name);
    if(var == NULL)
        return(-1);
    if(pthread_mutex_unlock(&var->Lock) != 0) {
        /* XXX - Errors here */
        return(-1);
    }
    return(0);
}

void *getShared(threadId id, const char *name) {
    thVar *var = selectShared(id, name);
    if(var == NULL)
        return(NULL);
    return(var->Var);
}

int removeShared(threadId id, const char *name) {
    thVar *var = selectShared(id, name);
    thVar *head = thList[id].sharedVars;
    if(var == NULL || head == NULL)
        return(-1);
    pthread_mutex_lock(&thList[id].sharedLock);
    if(var == head) {
        thList[id].sharedVars = var->next;
        pthread_mutex_unlock(&thList[id].sharedLock);
        return(freeShared(var));
    }
    while(head->next != var)
        head = head->next;
    head->next = var->next;
    pthread_mutex_unlock(&thList[id].sharedLock);
    return(freeShared(var));
}

int freeShared(thVar *var) {
    if(var == NULL)
        return(-1);
    pthread_mutex_destroy(&var->Lock);
    if(var->cleanUp)
        var->cleanUp(var->Var);
    free(var);
    return(0);
}

int clearShared(threadId id) {
    thVar *list = thList[id].sharedVars;
    thVar *p;
    while(list != NULL) {
        p = list;
        list = list->next;    
        freeShared(p);
    }
    thList[id].sharedVars = NULL;
    return(0);
}

void setState(threadId id, unsigned char state) {
    pthread_mutex_lock(&thList[id].stateLock);
    thList[id].state = state;
    pthread_mutex_unlock(&thList[id].stateLock);
}

int isActiveThread(threadId id) {
    return (thList[id].state == TH_RUN)?1:0;
}
