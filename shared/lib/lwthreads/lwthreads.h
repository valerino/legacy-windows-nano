/* $Id: eff297d88b512508b377dd4318f1145aec3240a1 $
 *
 */
#ifndef __THREADS_H__
#define __THREADS_H__

#include <pthread.h>
#include <lwlog.h>

#ifndef MAX_INSPECTION_THREADS
#define MAX_INSPECTION_THREADS 2048
#endif

#ifndef THREAD_MONITOR_INTERVAL
#define THREAD_MONITOR_INTERVAL	5  /* interval (in secs) between cycles of the threadManager */
#endif

#define TH_NOERR         0
#define TH_ERR_GENERIC  -1

#define threadId long

typedef struct thVar {
	const char *Name;
	void *Var;
	void (*cleanUp)(void *); // callback routine called to free resources used by Var
	pthread_mutex_t Lock;
	struct thVar *next;
} thVar;

typedef struct thArg {
	threadId id;
	void *priv;
} thArg;
typedef struct inspectionThread {
	pthread_t thHandler;
	time_t startTime;
	pthread_mutex_t sharedLock;
	thVar *sharedVars;  /* NULL terminated array of thVar */
	pthread_mutex_t stateLock;
	unsigned char state;
#define TH_CLEAN 0 
#define TH_DEFINED 1
#define TH_RUN 2
#define TH_OK 3
#define TH_ERR 4
	//struct inspectionThread *next;
	time_t timeout;
	void *(*routine)();
	thArg routineArg;
	void (*notify)();
	void *notifyArg;
	void *returnVal;
} inspectionThread;


#define CAN_STOP_THREAD(__bool) \
{\
    if (__bool) {\
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);\
        pthread_testcancel();\
    } else {\
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);\
    }\
}

#define START_THREAD_NOARG(__arg) \
	threadId id = __arg?((thArg *)__arg)->id:0;

#define START_THREAD(__arg,__prv) \
	threadId id = ((thArg *)__arg)->id;\
	__prv = ((thArg *)__arg)->priv;
	
#define END_THREAD(s,r)  \
	setState(id,s);\
	pthread_exit((void *)r);

int initThreadManager();
void freeThreadManager();
threadId startThread (void *(*thRoutine)(),void *thArg,void (*notifyRoutine)(),void *notifyArg,time_t timeout);
threadId instanceThread(void *(*thRoutine)(),void *thArg);
int execThread(threadId id);
int setNotify(threadId id, void (*routine)(),void *arg);
int setTimeout(threadId id,time_t timeStamp);
int stopThread(threadId id);
void *monitorThreads(void *);
void setState(threadId id,unsigned char state);
int isActiveThread(threadId id);

thVar *selectShared(threadId id,const char *name);
int addShared(threadId id,const char *name,void *var,void (*clean)());
int lockShared(threadId id,const char *name);
int unlockShared(threadId id,const char *name);
int removeShared(threadId id,const char *name);
int clearShared(threadId id);
int freeShared(thVar *var);

#endif
