/* $Id: 5a8b03b7c8ea399e9bf66b33cc2972637b7f0f1b $
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "emldissect.h"

extern int errno;

int main (int argc,char **argv)
{
	FILE *in;
	char *buf;
	struct stat st;
	int bytes;
	EmlMsg *msg;
	int i;
	
	if(argc < 2) 
	{
		printf("Usage: %s <file.eml> \n",argv[0]);
		exit(1);
	}
    if(stat(argv[1],&st) != 0) 
	{
	}

	in = fopen(argv[1],"rb");	
	if(!in) 
	{
		printf("Can't open input file: %s \n",strerror(errno));
		exit(1);
	}
	buf = (char *)calloc(1,st.st_size + 32);
	
	bytes = fread(buf,1,st.st_size,in);
	if(bytes)
	{
		msg = EmlScanMessage(buf,bytes);
		printf("NUM PARTS : %lu \n",msg->pnum);
		for(i=0;i<(int)msg->pnum;i++) 
		{
			printf("\t%s \n",msg->parts[i]->type);
		}
		EmlFreeMessage(msg);
	}
	fclose(in);
	free(buf);
	exit(0);
}
