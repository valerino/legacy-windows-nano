/*
 *  C Implementation: libemldissect
 *
 * Description: 
 *
 *
 * Author: xant <xant@xant.net>, (C) 2005
 *
 * Copyright: See COPYING file that comes with this distribution
 *
 */

#include "emldissect.h"
#include <stdio.h>
#ifdef WIN32
#include <uxdefs.h>
#endif

int EmlNextLine(char *buf,int len) {
	int off = 0;
	while(off+2 <= len) {
		if(strncmp(buf,"\r\n",2) == 0) 
			return(off+2);
		buf++;
		off++;
	}
	return(-1);
}

char *EmlExtractAddress(char *mailString,int sLen) {
	int i;
	int o = 0;
	int state = 0;
	char *buf;
	for (i=0; i< sLen;i++) {
		if(mailString[i] == '<') {
			o = i;
		}
		else if(mailString[i] == '>') {
			state = 2;
			buf = malloc(i-o+1);
			if(!buf) return NULL;
			memcpy(buf,mailString+o,i-o);
			buf[i-o] = 0;
			return(buf);
		}
	}
	buf = malloc(sLen+3);
	if(buf)
	{
		buf[0] = '<';
		memcpy(buf+1,mailString,sLen);
		buf[sLen+1] = '<';
		buf[sLen+2] = 0;
	}
	return(buf);
}

int EmlMakeHeaders(char *buff,unsigned long blen,char **headers,int hnum,char ***dests)
{
	int ll = 0; /* line length */
	unsigned long rb = blen; /* remaining bytes */
	int i;
	unsigned long hLen;
	char *p = buff;
	char **lastHeader = NULL;
	while(rb > 3 && p && strncmp(p,".\r\n",3) != 0) {
		if((ll = EmlNextLine(p,rb))) {
			for(i=0;i<hnum;i++)
			{
				hLen = (unsigned long)strlen(headers[i]);
				if(strncasecmp(p,headers[i],hLen) == 0) 
				{
					*dests[i] = (char *)calloc(1, ll-hLen+1);
					if(*dests[i]) 
					{
						memcpy(*dests[i],p+hLen,ll-hLen-2);
						lastHeader = dests[i];
					}
					break;
				}
			}
			if(*p == '\t' || *p == ' ')
			{
				if(lastHeader)
				{
					hLen = (unsigned long)(strlen(*lastHeader)+ll);
					*lastHeader = realloc(*lastHeader,hLen+1);
					strncat(*lastHeader,p,ll);
					(*lastHeader)[hLen] = 0;
				}
			}
			if(strncasecmp(p,"\r\n",2) == 0)
				return blen-rb;
		}
		else {
			/* mmm... no more lines...we should never arrive here */
#ifdef DEBUG
			printf("(POP3) EmlScanMessage reached the end of message buffer!!\n");
#endif
			p = NULL;
		}
		p+=ll; /* point to next line */
		rb-=ll; /* decrement remaining bytes */
	}
	return 0;
}

EmlMsg *EmlScanMessage(char *msgData,unsigned long msgLen) {
	EmlMsg *msg = NULL;
	unsigned long off;
	unsigned long dlen;
	char *headers[8] = 
		{ "Delivered-To: ","From: ", "To: ", "Subject: ","Cc: ", "Bcc: ","Content-Type: ","Date: " };
	char **dests[8];
	
	if (!msgData || !msgLen)
		return NULL;

	msg = (EmlMsg *)calloc(1,sizeof(EmlMsg));
	if (!msg)
		return NULL;

	dests[0] = &msg->delivered;
	dests[1] = &msg->from;
	dests[2] = &msg->to;
	dests[3] = &msg->subject;
	dests[4] = &msg->cc;
	dests[5] = &msg->bcc;
	dests[6] = &msg->ctype;
	dests[7] = &msg->date;
	
	off = EmlMakeHeaders(msgData,msgLen,headers,8,dests);
	msg->header = malloc(off+1);
	if (!msg->header)
	{
		free (msg);
		return NULL;
	}	

	strncpy(msg->header,msgData,off);
	msg->header[off] = 0;
	if(!off)
		goto bad_msg;
	dlen = msgLen-off;
	msg->data = malloc(dlen+1);
	if (!msg->data)
	{
		free (msg);
		return NULL;
	}

	memcpy(msg->data,msgData+off,dlen);
	msg->data[dlen] = 0; /* null-terminate msg string */
	msg->dataLen = dlen;
	EmlDissectData(msg);
	return msg;

bad_msg:			
	/* if here, some weird error occurred */
	free(msg);
	return NULL;
}

void EmlDissectData(EmlMsg *msg)
{
	char *p;
	char *start;
	char *tag;
	unsigned long tLen;
	
	if(!msg->ctype) 
		return;
		
	if(strncasecmp(msg->ctype,"multipart/",10) != 0)
		return;

	start = strstr(msg->ctype,"boundary=");
	if(!start)
		return;
	start+=9;
	if(*start == '"') start++;
	tLen = 0;
	p = start;
	while(*p != '"' && *p != 0)
	{
		tLen++;
		p++;
	}	
	if(!tLen) return;
	
	tLen += 2;
	tag = (char *)malloc(tLen+1);
	if (!tag)
		return;

	memcpy(tag,"--",2);
	memcpy(tag+2,start,tLen-2);
	tag[tLen] = 0;
	
	EmlWalkParts(msg->data,msg->dataLen,tag,&msg->parts,&msg->pnum);
	
	free(tag);
	
}

int EmlWalkParts(char *data,unsigned long len,char *tag,MsgPart ***newParts,unsigned long *numParts)
{
	char *headers[3] = 
	{ "Content-Type: ","Content-Disposition: ","Content-Transfer-Encoding: " };
	char **dests[3];
	MsgPart **pList;
	MsgPart *newPart;
	char *name = NULL;
	unsigned long i;
	unsigned long off;
	unsigned long pLen;
	unsigned long tLen = (unsigned long)strlen(tag);
	char *p = data;
	char *start;
	
	if(*numParts && *newParts)
	{
		pList = *newParts;
	}
	else
	{
		pList = NULL;
		*numParts = 0;
	}
	newPart = NULL;
	while(*p != 0) 
	{
		while(*p != 0 && strncmp(p,tag,tLen) != 0)
			p++;
			
		if(*p == 0)	break;
		p += tLen; /* skip also \r\n after tag */

		if(strncmp(p,"--",2) == 0)
			p+=2;
		if(strncmp(p,"\r\n",2) == 0)
			p+=2;
		if(*p == 0 || strncmp(p,"\r\n",2) == 0) break;

		newPart = (MsgPart *)calloc(1,sizeof(MsgPart));
		if(!newPart)
			break;
		
		dests[0] = &newPart->type;
		dests[1] = &newPart->name;
		dests[2] = &newPart->encoding;
	
		off = EmlMakeHeaders(p,len-(p-data),headers,3,dests);
		if(!off)
			break;
		newPart->tag = strdup(tag);

		if(newPart->name)
		{
			name = strstr(newPart->name,"name=");
		}
		else if(newPart->type) 
		{
			name = strstr(newPart->type,"name=");
			if(name) 
				newPart->name = (char *)malloc(strlen(newPart->type));
		}
		
		if(name)
		{
			name += 5;
			if(*name == '"') name++;
			i=0;
			while(*name != '"' && *name != 0)
			{
				memcpy(newPart->name+i,name,1);
				i++;
				name++;
			}
			*(newPart->name+i) = 0;
			name = NULL;
		}

		
		newPart->data = p+off;
		p+=off;
		
		pLen = 0;
		while(*p != 0 && strncmp(p,tag,tLen) != 0)
		{	
			p++;
			pLen++;
		}
		newPart->len = pLen;
		if(newPart->type && strncmp(newPart->type,"multipart/",10) == 0)
		{
			start = strstr(newPart->type,"boundary=");
			if(!start)
				return 0;
			start+=9;
			if(*start == '"') start++;
			tLen = 0;
			p = start;
			while(*p != '"' && *p != 0)
			{
				tLen++;
				p++;
			}	
			if(!tLen) return 0;
			
			tLen += 2;
			tag = (char *)malloc(tLen+1);
			if(tag) 
			{
				memcpy(tag,"--",2);
				memcpy(tag+2,start,tLen-2);
				tag[tLen] = 0;
				EmlWalkParts(newPart->data,newPart->len,tag,&newPart->parts,&newPart->pnum);
				free(tag);
			}
		}
		pList = (MsgPart **)realloc(pList,((*numParts)+1)*sizeof(MsgPart *));
		pList[*numParts] = newPart; 
		(*numParts)++;
		*newParts = pList;
	}
	return 1;
}

void EmlFreePart(MsgPart *part)
{
	int i;
	int children;
	if(part)
	{
		if(part->pnum && part->parts)
		{
			children = part->pnum;
			for(i=0;i<children;i++)
				EmlFreePart(part->parts[i]);
			free(part->parts);
		}
		if(part->tag) free(part->tag);
		if(part->name) free(part->name);
		if(part->type) free(part->type);
		if(part->encoding) free(part->encoding);
	}
	free(part);
}

void EmlFreeMessage(EmlMsg *msg)
{
	int i;
	if(msg) 
	{
		if(msg->pnum && msg->parts)
		{
			for(i=0;i<(int)msg->pnum;i++)
				EmlFreePart(msg->parts[i]);
			free(msg->parts);
		}
		if(msg->ctype) free(msg->ctype);
		if(msg->from) free(msg->from);
		if(msg->to) free(msg->to);
		if(msg->delivered) free(msg->delivered);
		if(msg->cc) free(msg->cc);
		if(msg->bcc) free(msg->bcc);
		if(msg->subject) free(msg->subject);	
		if(msg->header) free(msg->header);
		if(msg->data) free(msg->data);
	}
}
