//
// C Interface: emldissect
//
// Description: 
//
//
// Author: xant <xant@xant.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
//
// C Interface: libemldissect
//
// Description: 
//
//
// Author: xant <xant@xant.net>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
// "$Id: 1be9632be27cf495a0b61487447b31dc12972f33 $"
//

#ifndef __EMLDISSECT_H__
#define __EMLDISSECT_H__

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

typedef struct __MsgPart 
{
	char *tag;
	char *name;
	char *type;
	char *encoding;
	char *data;
	unsigned long len;
	struct __MsgPart **parts;
	unsigned long pnum; 
} MsgPart;

typedef struct __EmlMsg 
{
	char *ctype;
	char *from;
	char *to;
	char *delivered;
	char *cc;
	char *bcc;
	char *subject;
	char *header;
	char *date;
	char *data;
	MsgPart **parts;
	unsigned long pnum;
	unsigned long dataLen;
} EmlMsg;

EmlMsg *EmlScanMessage(char *msgData,unsigned long msgLen);
void EmlFreeMessage(EmlMsg *msg);

int EmlNextLine(char *buf,int len);
void EmlDissectData(EmlMsg *msg);
int EmlWalkParts(char *data,unsigned long len,char *tag,MsgPart ***newParts,unsigned long *numParts);

char *EmlExtractAddress(char *mailString,int sLen);

#endif
