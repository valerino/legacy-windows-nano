#ifndef __LWLOG_H__
#define __LWLOG_H__

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

int lwlog_init(int loglevel);
unsigned int lwlog_level();
void lwlog(int prio, int dbglevel, char *fmt, ...);

#define ERROR(fmt,args...)      do { lwlog(LOG_ERR,     0, fmt, ## args); } while (0)
#define WARNING(fmt,args...)    do { lwlog(LOG_WARNING, 0, fmt, ## args); } while (0)
#define NOTICE(fmt,args...)     do { lwlog(LOG_NOTICE,  0, fmt, ## args); } while (0)
#define INFO(fmt,args...)       do { lwlog(LOG_INFO,    0, fmt, ## args); } while (0)
#define DEBUG(fmt,args...)      do { if (lwlog_level() >= 1)  lwlog(LOG_DEBUG,   1, fmt, ## args); } while (0)
#define DEBUG2(fmt,args...)     do { if (lwlog_level() >= 2)  lwlog(LOG_DEBUG,   2, fmt, ## args); } while (0)
#define DEBUG3(fmt,args...)     do { if (lwlog_level() >= 3)  lwlog(LOG_DEBUG,   3, fmt, ## args); } while (0)
#define DEBUG4(fmt,args...)     do { if (lwlog_level() >= 4)  lwlog(LOG_DEBUG,   4, fmt, ## args); } while (0)

#endif

