//
// C Interface: teralog
//
// Description: 
//
//
// Author: xant <xant@xant.net>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
// $Id: 4e4a33c414da97a387155a56a6d8db9241f7171d $
//

#ifndef __TERALOG_H__
#define __TERALOG_H__

#include <xdbaccess.h>
#ifdef WIN32
#include "syslog_w32.h"
#include <w32_pthread.h>
#else
#include <syslog.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

/* Log descriptor handler */
typedef struct __TLogHandler {
	DBHandler *dbh;
	int syslog;
	int donotlog;
	FILE *fd;
	char *component;
	char *context;
	int debug;
	pthread_mutex_t lock;
} TLogHandler;

/* Creates and initializes a new descriptor. The returned descriptor must
 * be used in all subsequent calls to TeraLog API */
TLogHandler *TLogInit(char *component, char *context, DBHandler *dbh, 
	int syslog, char *logPath,int debug);
/* Destroy a TeraLog context releasing all used resources */
void TLogDestroy(TLogHandler *log);
/* Log a debug message */
void TLogDebug(TLogHandler *log,const char *format, ...);
/* Log a warning message */
void TLogWarning(TLogHandler *log,const char *format, ...);
/* Log a notify message. Theese will be broadcasted to all tera interfaces */
void TLogNotify(TLogHandler *log,const char *format, ...);
void TLogError(TLogHandler *log,const char *format, ...);
void TLogEvent(TLogHandler *log,const char *format, ...);

#endif
