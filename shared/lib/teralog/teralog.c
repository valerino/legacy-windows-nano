/*
 *  C Implementation: teralog
 *
 * Description: 
 *
 *
 * Author: xant <xant@xant.net>, (C) 2006
 *
 * Copyright: See COPYING file that comes with this distribution
 *
 * $Id: c2ef6d22927d7a9f8aaae8d8998134143147f67b $
 */
#include <stdio.h>
#include <stdarg.h>
#include "teralog.h"
#ifdef WIN32
#include <uxdefs.h>
#endif

#define PREFIX_MAXLEN 48
TLogHandler *TLogInit(char *component, char *context, DBHandler *dbh, 
	int syslog, char *logPath, int debug)
{
	TLogHandler *newHandler = NULL;
	
	if(!component) 
		return NULL;
	newHandler = (TLogHandler *)calloc(1, sizeof(TLogHandler));

	if(!newHandler) 
		return NULL;

	newHandler->component = strdup(component);
	newHandler->dbh = dbh;
	newHandler->debug = debug;
	if(syslog)
	{
		newHandler->syslog = syslog;
		openlog(component, LOG_NDELAY, LOG_USER);
	}
	if(logPath)
	{
		newHandler->fd = fopen(logPath, "w+");
		if(!newHandler->fd)
		{ 
			fprintf(stderr, "Can't open logfile %s\n", logPath);
			fprintf(stderr, "Logging to local filesystem disabled! \n");
		}
	}
	if(context)
		newHandler->context = strdup(context);
	pthread_mutex_init(&newHandler->lock, NULL);
	return newHandler;
}

void TLogDestroy(TLogHandler * log)
{
	if(log)
	{
		if(log->fd)
			fclose(log->fd);
		if(log->syslog)
			closelog();
		if(log->component)
			free(log->component);
		pthread_mutex_destroy(&log->lock);
		free(log);
	}
}

int TLogDB(TLogHandler *log, char *type, char *msg)
{
	LinkedList *iList;
	int res;
	
	if(!log || !type || !msg)
		return 0;
	iList = CreateList();
	if(!iList)
		return 0;
	PushTaggedValue(iList, CreateTaggedValue("type", type, 0));
	PushTaggedValue(iList, CreateTaggedValue("msg", msg, 0));
	PushTaggedValue(iList, CreateTaggedValue("source", log->component, 0));
	PushTaggedValue(iList, CreateTaggedValue("context", log->context, 0));
	PushTaggedValue(iList, CreateTaggedValue("timestamp", DB_NOQUOTE(now_msec()), 0));
	res = DBInsert(log->dbh, "Messages", iList);
	DestroyList(iList);
	return res;
}

void TLogPutMessage(TLogHandler *log, char *msg, int facility, char *type, char *pre)
{
	pthread_mutex_lock(&log->lock);
	if(log->fd) 
	{ 
		fprintf(log->fd, "%s %s\n", pre, msg);
		fflush(log->fd); 
	}
	
	// avoid logging on db if this is specified (mostly for debugging)
	if (!log->donotlog)
	{
		if(log->dbh)
			TLogDB(log, type, msg);
		if(log->syslog)
			syslog(facility, msg);
	}

	fprintf(stderr, "%s %s\n", pre, msg);
	pthread_mutex_unlock(&log->lock);
}

char *FixLlu (const char* ppString)
{
	char* p = NULL;
#ifdef WIN32
	char* q = NULL;
	unsigned long pLen;

	if(!ppString)
		return NULL;

	pLen = (unsigned long)strlen(ppString)+1;
	p = calloc (1, pLen);
	if (!p)
		return NULL;
	q = strstr(ppString, "%llu");
	while(q)
	{
		pLen++;
		p = realloc(p, pLen);
		strncat(p, ppString, q-ppString);
		strcat(p, "%I64d");
		ppString = q+4;
		q = strstr(ppString, "%llu");
	}
	strcat (p, ppString);
#else
	p = strdup(ppString);
#endif
	return p;
}

void TLogDebug(TLogHandler *log, const char *format, ...) 
{
	char msg[255];
	char *rformat = NULL;
	char prefix[PREFIX_MAXLEN];

	
	if(log->debug)
	{
		va_list arg;
		va_start(arg, format);
		rformat = FixLlu(format);
		if(!rformat)
			return;
		snprintf(prefix, sizeof(prefix), "** %28s **[D] - ", log->context);
		vsnprintf(msg, 254, format, arg);
		TLogPutMessage(log, msg, LOG_DEBUG, "debug", prefix);
		if(rformat)
			free(rformat);
		va_end(arg);
	}
}

void TLogNotify(TLogHandler *log, const char *format, ...) 
{
	char msg[255];
	char *rformat = NULL;
	char prefix[PREFIX_MAXLEN];
	va_list arg;
	
	va_start(arg, format);
	rformat = FixLlu(format);
	if(!rformat)
		return;
	vsnprintf(msg, 254, rformat, arg);
	if(rformat)
		free(rformat);
	snprintf(prefix, sizeof(prefix), "** %28s **[N] - ", log->context);
	TLogPutMessage(log, msg, LOG_NOTICE, "notify", prefix);
	va_end(arg);
}

void TLogError(TLogHandler *log, const char *format, ...) 
{
	char msg[255];
	char *rformat = NULL;
	char prefix[PREFIX_MAXLEN];
	va_list arg;
	
	va_start(arg, format);
	rformat = FixLlu(format);
	if(!rformat)
		return;
	vsnprintf(msg, 254, rformat, arg);
	if(rformat)
		free(rformat);
	snprintf(prefix, sizeof(prefix), "** %28s **[E] - ", log->context);
	TLogPutMessage(log, msg, LOG_ERR, "error", prefix);
	va_end(arg);
}

void TLogEvent(TLogHandler *log, const char *format, ...)
{
	char msg[255];
	char *rformat = NULL;
	char prefix[PREFIX_MAXLEN];
	va_list arg;
	
	va_start(arg, format);
	rformat = FixLlu(format);
	if(!rformat)
		return;
	vsnprintf(msg, 254, rformat, arg);
	if(rformat)
		free(rformat);
	snprintf(prefix, sizeof(prefix), "** %28s **[EVT] - ", log->context);
	TLogPutMessage(log, msg, LOG_NOTICE, "event", prefix);
	va_end(arg);
}
void TLogWarning(TLogHandler *log, const char *format, ...) 
{
	char msg[255];
	char prefix[PREFIX_MAXLEN];
	char *rformat = NULL;
	va_list arg;

	va_start(arg, format);
	rformat = FixLlu(format);
	vsnprintf(msg, 254, rformat, arg);
	if(rformat)
		free(rformat);
	snprintf(prefix, sizeof(prefix), "** %28s **[W] - ", log->context);
	TLogPutMessage(log, msg, LOG_WARNING, "warning", prefix);
	va_end(arg);
}

