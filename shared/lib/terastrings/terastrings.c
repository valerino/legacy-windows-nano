/*
 *  stringops.c
 *  libteradb
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  "$Id: 3c6d92afdd9b8ac2a75210d5d335abe26ab27f73 $"
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iconv.h>

/************************************************************************/
// void	TokenizeW32Path (char* pszInPath, char* pszDevice, char* pszPath, char* pszName);
// 
// Tokenize path in device,path,name strings. All output strings are optional.
// 
/************************************************************************/
unsigned long Ucs2Len(unsigned short *str)
{
	size_t cnt = 0;
	while (str[cnt] != 0) 
		cnt++;
	return (unsigned long)cnt;
}

void TokenizeW32Path (char* pszInPath, char* pszDevice, char* pszPath, char* pszName)
{
#ifdef WIN32
	char szext [260];
	if (!pszInPath)
		return;

	if (pszDevice)
		_splitpath(pszInPath,pszDevice,NULL,NULL,NULL);
	if (pszPath)
		_splitpath(pszInPath,NULL,pszPath,NULL,NULL);
	if (pszName)
	{
		_splitpath(pszInPath,NULL,NULL,pszName,NULL);
		// get extension too
		_splitpath(pszInPath,NULL,NULL,NULL,szext);
		strcat (pszName,szext);
	}
#else
	ssize_t inLen = 0;
	char *start = NULL;
	char *end = NULL;
	//ssize_t off = 0;
	ssize_t outLen = 0;

	inLen = strlen(pszInPath);
	if(pszDevice)
	{
		start = pszInPath;
		end = start;
		/* skip heading whitespaces if present */
		while(*start == ' ' && *start != 0)
			start++;
		if(*start != 0)
		{
			while(*end != ':')
			{
				if(*end == 0)
				{
					end = NULL;
					break;
				}
				end++;
			}
			if(end)
			{
				outLen = end-start;
				memcpy(pszDevice,start,outLen);
				pszDevice[outLen] = 0;
			}
		}
	}
	if(pszName)
	{
		start=pszInPath+inLen;
		/* skip trailing whitespaces if present */
		if(*start == ' ')
		{
			while(*start == ' ' && start > pszInPath)
				start--;
		}
		if(*start == '\\')
			start--;
		outLen = 0;
		while(start > pszInPath)
		{
			if(*start == '\\')
				break;
			start--;
			outLen++;
		}
		if(outLen)
		{
			memcpy(pszName,start+1,outLen-1);
			pszName[outLen-1] = 0;
		}
	}
	if(pszPath)
	{
		start=pszInPath;
		while(*start != '\\')
		{
			if(*start == 0)
			{
				start = NULL;
				break;
			}
			start++;
		}
		if(start)
		{
			end=pszInPath+inLen;
			if(*end == '\\')
				end--;
			while(end != start)
			{
				if(*end == '\\')
					break;
				end--;
			}
			outLen = end-start;
			memcpy(pszPath,start,outLen+1);
			pszPath[outLen+1] = 0;
		}
	}
#endif
}

//***********************************************************************
// int Ucs2ToUtf8 (char* pszOutstring, unsigned short* pwszString, int OutStringLen)
// 
// convert ucs2 string to utf8.
// 
// returns number of bytes converted
//***********************************************************************
int Ucs2ToUtf8 (char* pszOutstring, unsigned short* pwszString, int OutStringLen)
{
	size_t bytesin = 0;
	size_t bytesout = 0;
	char* pIn = NULL;
	char* pOut = NULL;
	iconv_t cd = NULL;

	if (!pszOutstring || !OutStringLen)
		goto __exit;
	memset (pszOutstring,0,OutStringLen);

	bytesin = Ucs2Len (pwszString) * sizeof (unsigned short);
	if (!bytesin)
		goto __exit;
	cd = iconv_open("UTF-8","UCS-2LE");
	if (!cd)
		goto __exit;

	bytesout = OutStringLen;
	pIn = (char*)pwszString;
	pOut = (char*)pszOutstring;

	iconv (cd, &pIn, &bytesin, &pOut, &bytesout);

__exit:	
	if (cd)
		iconv_close (cd);
	return (int)(OutStringLen - bytesout);
}

//***********************************************************************
// int Utf8ToUcs2 (unsigned short* pwszOutstring, char* pszString, int OutStringLen)
// 
// convert utf8 string to ucs2 
// 
// returns number of bytes converted
//***********************************************************************
int Utf8ToUcs2 (unsigned short* pwszOutstring, char* pszString, int OutStringLen)
{
	size_t bytesin = 0;
	size_t bytesout = 0;
	char* pIn = NULL;
	char* pOut = NULL;
	iconv_t cd = NULL;

	if (!pwszOutstring || !OutStringLen)
		goto __exit;
	memset (pwszOutstring,0,OutStringLen);

	bytesin = strlen (pszString);
	if (!bytesin)
		goto __exit;
	cd = iconv_open("UCS-2LE","UTF-8");
	if (!cd)
		goto __exit;

	bytesout = OutStringLen;
	pIn = (char*)pszString;
	pOut = (char*)pwszOutstring;

	iconv (cd, &pIn, &bytesin, &pOut, &bytesout);

__exit:	
	if (cd)
		iconv_close (cd);
	return (int)(OutStringLen - bytesout);
}

//***********************************************************************
// int AsciiHexToByte (unsigned char* pByteDest, int ByteDestSize, char* pSrc, int swap)
// 
// convert ascii hex string to bytes. return number of bytes converted, or 0 on error. Swap bytes if requested
// 
// 
//***********************************************************************
int AsciiHexToByte (unsigned char* pByteDest, int ByteDestSize, char* pSrc, int swap)
{
	int bl = 0;
	unsigned char	s[3];
	unsigned char* p = NULL;
	unsigned char* stop = NULL;
	int i = 0;
	int checkendian = 1;

	if (!pByteDest || !ByteDestSize || !pSrc)
		return 0;

	// get byte len
	bl = (int)strlen(pSrc) / 2;
	if (bl > ByteDestSize)
		return 0;

	// convert (check for bigendian and do not swap in case)
	p=(char*)&checkendian;
	//if (p[0]!=1)
	//	swap = FALSE;

	if (!swap)
	{
		p = pSrc;i=0;
		while (i < bl)
		{
			memset (s,0,sizeof (s));
			strncpy(s,p,2);
			*pByteDest=(unsigned char)strtol(s,(char **)&stop,16);
			p+=2;i++;pByteDest++;
		}
	}
	else
	{
		// swapped (for ulongs conversion)
		p = (pSrc+(bl*2))-2;i=0;
		while (i < bl)
		{
			memset (s,0,sizeof (s));
			strncpy(s,p,2);
			*pByteDest=(unsigned char)strtol(s,(char **)&stop,16);
			p-=2;i++;pByteDest++;
		}
	}

	return bl;
}

/*
*	transform a sephar separated string in a \0 terminated strings buffer
*
*/
void TokenizeString (char* string, char sepchar)
{
	char* q = string;

	while (*q != '\0')
	{
		if (*q == sepchar)
			*q = '\0';
		q++;
	}
}

