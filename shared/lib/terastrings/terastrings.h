/*
 *  stringops.h
 *  libteradb
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  "$Id: 2f142760796f7154ed6fc1825011b05db8f75c12 $"
 */
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

int Ucs2ToUtf8 (char* pszOutstring, unsigned short* pwszString, int OutStringLen);
int Utf8ToUcs2 (unsigned short* pwszOutstring, char* pszString, int OutStringLen);
void TokenizeW32Path (char* pszInPath, char* pszDevice, char* pszPath, char* pszName);
int AsciiHexToByte (unsigned char* pByteDest, int ByteDestSize, char* pSrc, int swap);

/*
*	transform a sephar separated string in a \0 terminated strings buffer
*
*/
void TokenizeString (char* string, char sepchar);
