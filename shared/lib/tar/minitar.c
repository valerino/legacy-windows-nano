//***********************************************************************
// minitar.c
// 
// mini tar creator. supports path up to 255 chars. TODO : unix support (findfirst/findnext)
// -vx-
// 
// 
//***********************************************************************
#include <stdio.h>
#include <stdarg.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include "minitar.h"

//***********************************************************************
// int put_octal(char *cp, int len, long value)
// 
// Put an octal string into the specified buffer.
// The number is zero and space padded and possibly null padded.
// Returns TRUE if successful.
//***********************************************************************
int put_octal(char *cp, int len, long value)
{
	int tempLength;
	char tempBuffer[32];
	char *tempString = tempBuffer;

	/* Create a string of the specified length with an initial space,
	* leading zeroes and the octal number, and a trailing null.  */
	sprintf(tempString, "%0*lo", len - 1, value);

	/* If the string is too large, suppress the leading space.  */
	tempLength = (int) strlen(tempString) + 1;
	if (tempLength > len) {
		tempLength--;
		tempString++;
	}

	/* If the string is still too large, suppress the trailing null.  */
	if (tempLength > len)
		tempLength--;

	/* If the string is still too large, fail.  */
	if (tempLength > len)
		return FALSE;

	/* Copy the string to the field.  */
	memcpy(cp, tempString, len);

	return TRUE;
}

//***********************************************************************
// int split_long_name (const char *name, size_t length)
// 
// return last slash position
// 
// 
//***********************************************************************
int split_long_name (const char *name, int length)
{
	int i = 0;

	if (length > PREFIX_SIZE)
		length = PREFIX_SIZE+2;
	for (i = length - 1; i > 0; i--)
		if ((name[i]) == '\\' || name[i] == '/')
			break;
	return i;
}

//***********************************************************************
// int write_ustar_long_name (TarHeader Header, char *name)
// 
// write header filename (max 255 chars, if needed split into name and prefix)
// 
// returns 0 on success
//***********************************************************************
int write_ustar_long_name (TarHeader* Header, char *name)
{
	char* pdots = NULL;
	int length = 0;
	int i = 0;
	
	// skip drive
	pdots = strchr (name,':');
	if (pdots)
		pdots+=2;
	else
		pdots = name;

	// do not support names above 255 chars
	length = (int)strlen (pdots);
	if (length > PREFIX_SIZE + NAME_SIZE + 1)
		return -1;

	i = split_long_name (pdots, length);
	if (i == 0 || length - i - 1 > NAME_SIZE)
		return -1;
	
	memcpy (Header->prefix, pdots, i);
	memcpy (Header->name, pdots + i + 1, length - i - 1);
	
	// change slashes
	for (i=0; i < (int)strlen(Header->prefix);i++)
	{
		if (Header->prefix[i]=='\\')
			Header->prefix[i]='/';
	}
	for (i=0; i < (int)strlen(Header->name);i++)
	{
		if (Header->name[i]=='\\')
			Header->name[i]='/';
	}

	return 0;
}

/*
 *	add file to tar archive
 *
 */
int tar_add_file (IN FILE* fTar, IN char* filename)
{
	int res = -1;
	TarHeader* pHeader = NULL;
	char* pBuffer = NULL;
	char* cp = NULL;
	long chksum = 0;
	int sizewrite = 0;
	int size = 0;
	struct _stat finfo;
	FILE* fIn = NULL;

	if (!fTar || !filename)
		goto __exit;
	pBuffer = malloc (32768 + 1);
	if (!pBuffer)
		goto __exit;
	memset (pBuffer,0,32768);
	pHeader = (TarHeader*)pBuffer;

	// get file info
	if (_stat (filename,&finfo) != 0)
		goto __exit;

	// build header
	if (write_ustar_long_name(pHeader,filename) != 0)
		goto __exit;

	put_octal(pHeader->mode, sizeof(pHeader->mode), finfo.st_mode);
	put_octal(pHeader->uid, sizeof(pHeader->uid), finfo.st_uid);
	put_octal(pHeader->gid, sizeof(pHeader->gid), finfo.st_gid);
	put_octal(pHeader->size, sizeof(pHeader->size), finfo.st_size);
	put_octal(pHeader->mtime, sizeof(pHeader->mtime), (long)finfo.st_mtime);
	strcpy(pHeader->magic, "ustar");
	memcpy (pHeader->version,"  ",sizeof (pHeader->version));
	strcpy(pHeader->uname, "user");
	strcpy(pHeader->gname, "group");
	if (finfo.st_mode & S_IFDIR)
	{
		strcat (pHeader->name,"/");
		pHeader->typeflag = '5';
	}
	else if (finfo.st_mode & S_IFREG)
	{
		pHeader->typeflag = '0';
	}

	/* Calculate and store the checksum (i.e., the sum of all of the bytes of
	* the header).  The checksum field must be filled with blanks for the
	* calculation.  The checksum field is formatted differently from the
	* other fields: it has [6] digits, a null, then a space -- rather than
	* digits, followed by a null like the other fields... */
	memset(pHeader->chksum, ' ', sizeof(pHeader->chksum));
	size = sizeof (struct TarHeader);
	cp = pBuffer;
	while (size)
	{
		chksum += *cp;
		cp++;size--;
	}
	put_octal(pHeader->chksum, 7, chksum);

	// write header
	if (fwrite(pBuffer,512,1,fTar) != 1)
		goto __exit;
	if (finfo.st_mode & S_IFDIR)
	{
		// directory, return ok
		res = 0;
		goto __exit;
	}

	// write file into tar
	size = finfo.st_size;
	fIn = fopen (filename,"rb");
	if (!fIn)
		goto __exit;

	while (TRUE)
	{
		if (size >= 32768)
			sizewrite = 32768;
		else
			sizewrite = size;

		if (fread (pBuffer,sizewrite,1,fIn) != 1)
			break;
		if (fwrite (pBuffer,sizewrite,1,fTar) != 1)
			break;
		size-=sizewrite;
		if (size == 0)
		{
			memset(pBuffer,0,32768);
			// done, pad file at 512
			size = sizewrite;
			while (size % 512)
				size++;
			sizewrite = size-sizewrite;
			if (fwrite (pBuffer,sizewrite,1,fTar) != 1)
				break;
			res = 0;
			break;
		}
	}

__exit:
	if (pBuffer)
		free (pBuffer);
	if (fIn)
		fclose (fIn);
	return res;
}

/*
 *	add dir to tar archive, optionally specify a mask (.doc)
 *
 */
int tar_add_dir (IN FILE* fTar, IN char* dirname, OPTIONAL IN char* includemask)
{
	int res = -1;
	struct _finddata_t fdata;
	char pathname [255];
	char fullname [255];
	int hfile = 0;
	
	// search files
	sprintf (pathname,"%s\\*.*",dirname);
	hfile = (int)_findfirst (pathname,&fdata);
	if (hfile == -1)
		goto __exit;

	do
	{
		if (fdata.attrib & _A_SUBDIR)
		{
			if (fdata.name[0] == '.')
				continue;

			// check mask
			if (includemask)
			{
				if (!strstr (fdata.name,includemask))
					continue;
			}

			sprintf (fullname,"%s\\%s",dirname,fdata.name);
			
			// add dir
			res = tar_add_file (fTar,fullname);
			if (res != 0)
				goto __exit;
			
			// recurse
			res = tar_add_dir (fTar,fullname,includemask);
			if (res != 0)
				goto __exit;
		}
		else
		{
			// check mask
			if (includemask)
			{
				if (!strstr (fdata.name,includemask))
					continue;
			}

			// add this file
			sprintf (fullname,"%s\\%s",dirname,fdata.name);
			res = tar_add_file(fTar,fullname);
		}
	}
	while (_findnext ((int)hfile, &fdata) == 0);
	
	// check error
	if (errno == ENOENT)
		res = 0;
	
__exit:
	if (hfile)
		_findclose((int)hfile);
	return res;
}