#ifndef __minitar_h__
#define __minitar_h__

#include <stdio.h>
#include <stdarg.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef OPTIONAL
#define OPTIONAL
#endif

#ifndef FALSE
#define FALSE 0
#define TRUE 1
#endif

/* POSIX tar Header Block, from POSIX 1003.1-1990  */
#define NAME_SIZE			100
#define PREFIX_SIZE 155

struct TarHeader {		/* byte offset */
	char name[NAME_SIZE];	/*   0-99 */
	char mode[8];		/* 100-107 */
	char uid[8];		/* 108-115 */
	char gid[8];		/* 116-123 */
	char size[12];		/* 124-135 */
	char mtime[12];		/* 136-147 */
	char chksum[8];		/* 148-155 */
	char typeflag;		/* 156-156 */
	char linkname[NAME_SIZE];	/* 157-256 */
	char magic[6];		/* 257-262 */
	char version[2];	/* 263-264 */
	char uname[32];		/* 265-296 */
	char gname[32];		/* 297-328 */
	char devmajor[8];	/* 329-336 */
	char devminor[8];	/* 337-344 */
	char prefix[155];	/* 345-499 */
	char padding[12];	/* 500-512 (pad to exactly the TAR_BLOCK_SIZE) */
};
typedef struct TarHeader TarHeader;

/*
*	add file to tar archive
*
*/
int tar_add_file (IN FILE* fTar, IN char* filename);

/*
*	add dir to tar archive, optionally specify a mask (.doc)
*
*/
int tar_add_dir (IN FILE* fTar, IN char* dirname, OPTIONAL IN char* includemask);

#endif // #ifndef __minitar_h__