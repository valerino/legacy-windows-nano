/*
 *	configuration file handler, handles encrypted txt files (tag=value) 
 *  -xv-
 */

#pragma warning( disable : 4995 )

#include <stdio.h>
#include <stdlib.h>

#if (defined (WIN32) || defined (WINCE)) && !defined (_KERNELMODE)
#include <winsock2.h>
#endif

#if defined (_KERNELMODE)
#include <ntifs.h>
#include <ntstrsafe.h>
#define POOL_TAG 'gfck'
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
#include <w32_defs.h>
#endif


#ifdef _USE_AES_RC6
#ifdef WINCE
#include <aes-noopt.h>
#else
#include <aes.h>
#endif
#endif

#ifdef _USE_AES_TWOFISH
#include <aes_2fish.h>
#endif

#include <uxml.h>
#include <dbg.h>
#include "rkcfghandle.h"


/*
*	returns an Uxml describing the configuration in cfgmem.
*
*/
int RkCfgReadFromMemory (IN void* cfgmem, IN unsigned long size, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml)
{
	UXml *pNewXml = NULL;
	keyInstance    ki = {0};
	cipherInstance ci = {0};
	unsigned long ciphersize = 0;
	char asciikey [128] = {0};
	int res = -1;
	int i = 0;
	int j = 0;	
	unsigned char* buf = NULL;

	if (!cfgmem || !ConfigXml || !size)
		goto __exit;

	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}

	ciphersize = size;
	while (ciphersize % 16)
		ciphersize++;
#if defined _KERNELMODE
	buf = ExAllocatePoolWithTag(PagedPool, ciphersize + 32, POOL_TAG);
	if(buf)
		memset (buf,0,ciphersize + 32);
#else
	buf = calloc (1,ciphersize + 32);
#endif

	if (!buf)
		goto __exit;
	memcpy (buf,cfgmem,size);

	/// decrypt
	if (cipherkey)
	{
		if (!asciicipherkey)
		{
			/// turn cipherkey to ascii
			for (i=0;i < bits / 8; i++)
			{
				sprintf (&asciikey[j],"%.2x",(unsigned char)cipherkey[i]);
				j+=2;
			}
			cipherkey = asciikey;
		}

		if (makeKey(&ki,DIR_DECRYPT,bits,cipherkey) != TRUE)
			goto __exit;
		if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
			goto __exit;
		if (blockDecrypt(&ci,&ki,buf,ciphersize*8,buf) != ciphersize*8)
			goto __exit;
	}

	res = UXmlFromBuffer(buf, &pNewXml);
	if(res != 0)
	{
		res = -1;
		goto __exit;
	}

	*ConfigXml = pNewXml;
	
	/// ok
	res = 0;

__exit:
	if (buf)
#if defined _KERNELMODE
	ExFreePoolWithTag(buf, POOL_TAG);
#else
	free (buf);
#endif

	return res;
}

/*
 *	read configuration file, optionally encrypted
 *
 */
#if defined _KERNELMODE
int RkCfgRead (IN PUNICODE_STRING cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN OPTIONAL int asciicipherkey, OUT UXml **ConfigXml)
{
	NTSTATUS status = STATUS_UNSUCCESSFUL;
	HANDLE hFile = NULL;
	OBJECT_ATTRIBUTES objAttr = {0};
	IO_STATUS_BLOCK iosb = {0};
	FILE_STANDARD_INFORMATION fileStandardInfo = {0};
	unsigned long ciphersize = 0;
	char* buf = NULL;
	
	if(!cfgpath)
		return -1;

	if(cipherkey)
	{
		if(bits != 64 && bits != 128 && bits != 256)
			return -1;
	}

	InitializeObjectAttributes(&objAttr, cfgpath, OBJ_CASE_INSENSITIVE, NULL, NULL);
	status = ZwCreateFile(&hFile, GENERIC_READ, &objAttr, &iosb, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_ALERT, NULL, 0);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT(("Could not open %wZ\n", cfgpath));
		return -1;
	}

	status = ZwQueryInformationFile(hFile, &iosb, &fileStandardInfo, sizeof(FILE_STANDARD_INFORMATION), FileStandardInformation);
	if(!NT_SUCCESS(status))
	{
		DBG_OUT(("Could not get file size.\n"));
		goto __exit;
	}

	ciphersize = fileStandardInfo.EndOfFile.LowPart;
	while (ciphersize % 16)
		ciphersize++;
	buf = ExAllocatePoolWithTag(PagedPool, ciphersize + 32, POOL_TAG);
	if(!buf)
	{
		status = -1;
		goto __exit;
	}
	memset(buf, 0, ciphersize + 32);

	status = ZwReadFile(hFile, NULL, NULL, NULL, &iosb, buf, fileStandardInfo.EndOfFile.LowPart, 0, NULL);
	if(!NT_SUCCESS(status))
		goto __exit;

	/// read cfg from memory
	status = RkCfgReadFromMemory(buf,fileStandardInfo.EndOfFile.LowPart,cipherkey,bits,asciicipherkey,ConfigXml);

__exit:	
	if (status != 0)
		status = -1;

	if(hFile)
		ZwClose (hFile);
	if(buf)
		ExFreePoolWithTag(buf, POOL_TAG);

	return status;
}
#else
int RkCfgReadW (IN WCHAR* cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml)
{
	unsigned long filesize = 0;
	FILE* f = NULL;
	unsigned char* buf = NULL;
	unsigned long ciphersize = 0;
	int res = -1;

	if (!cfgpath || !ConfigXml)
		goto __exit;
	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}

	/// read cfg file
	f = _wfopen (cfgpath,L"rb");
	if (!f)
	{
		WDBG_OUT ((L"cfgread can't find cfgfile %s\n",cfgpath));
		goto __exit;
	}

	fseek (f,0,SEEK_END);
	filesize = ftell (f);
	fseek (f,0,SEEK_SET);
	if (!filesize)
		goto __exit;

	ciphersize = filesize;
	while (ciphersize % 16)
		ciphersize++;
	buf = calloc (1,ciphersize + 32);
	if (!buf)
		goto __exit;
	if (fread (buf,filesize,1,f) != 1)
		goto __exit;

	/// read cfg from memory
	res = RkCfgReadFromMemory(buf,filesize,cipherkey,bits,asciicipherkey,ConfigXml);

__exit:
	if (f)
		fclose(f);
	if (buf)
		free (buf);

	return res;
}

int RkCfgRead (IN char* cfgpath, IN OPTIONAL unsigned char* cipherkey, IN OPTIONAL int bits, IN int asciicipherkey, OUT UXml **ConfigXml)
{
	WCHAR tmppath [MAX_PATH];
	size_t numchar = 0;
	
	if (!cfgpath)
		return -1;

	mbstowcs(tmppath,cfgpath,MAX_PATH);
	
	return RkCfgReadW(tmppath,cipherkey,bits,asciicipherkey,ConfigXml);
}

#endif // #if defined _KERNELMODE

/*
*	get configuration for the specified module
*
*/
int RkCfgGetModuleConfig(IN UXml *ConfigXml, IN char* ModuleName, IN char* ModuleType, OUT UXmlNode **ConfigNode)
{
	UXmlNode *pNode = NULL;
	int res = -1;

	if(!ConfigXml || !ModuleName || !ConfigNode || !ModuleType)
		return -1;

	pNode = UXmlGetChildNode(ConfigXml->Root);
	while(pNode != NULL)
	{
		if(strcmp(pNode->Name, "module") == 0)
		{
			UXmlAttribute *pNameAttr = UXmlGetAttributeByName(pNode, "name");
			UXmlAttribute *pTypeAttr = UXmlGetAttributeByName(pNode, "type");
			if(pNameAttr && pTypeAttr)
			{
				if((strcmp(pNameAttr->Value, ModuleName) == 0) && (strcmp(pTypeAttr->Value, ModuleType) == 0))
				{
					*ConfigNode = pNode;
					res = 0;
					break;
				}
			}
		}
		pNode = UXmlGetNextNode(pNode);
	}

	return res;
}

/*
*	get core configuration
*
*/
int RkCfgGetCoreConfig( IN UXml *ConfigXml, OUT UXmlNode **ConfigNode)
{
	UXmlNode *pNode = NULL;
	int res = -1;
	
	if(!ConfigXml || !ConfigNode)
		return -1;
		
	//search for core node "<core>"
	pNode = UXmlGetChildNode(ConfigXml->Root);
	while(pNode != NULL)
	{
		if(strcmp(pNode->Name, "core") == 0)
		{
			res = 0;
			*ConfigNode = pNode;
			break;
		}
		
		pNode = UXmlGetNextNode(pNode);
	}
	
	return res;
}
	
/*
*	get specifed option and value for the input xml node
*
*/
int RkCfgGetModuleOption(IN UXmlNode *Node, OUT char** OptionName, OUT char** OptionValue)
{
	UXmlAttribute *pAttribute = NULL;

	if(!Node || !OptionName)
		return -1;

	if(strncmp(Node->Name, "option", strlen("option")) != 0)
		return -1;

	pAttribute = UXmlGetAttributeByName(Node, "name");
	if(pAttribute != NULL)
		*OptionName = pAttribute->Value;

	pAttribute = UXmlGetAttributeByName(Node, "value");
	if(pAttribute != NULL)
		*OptionValue = pAttribute->Value;

	return 0;
}

/*
*	get specified parameter and value for the input node
*
*/
int RkCfgGetOptionParameter(IN UXmlNode *Node, OUT char** ParameterName, OUT char** ParameterValue)
{
	UXmlAttribute *pAttribute = NULL;

	if(!Node || !ParameterName || !ParameterValue)
		return -1;

	if(strncmp(Node->Name, "param", strlen("param")) != 0)
		return -1;

	pAttribute = UXmlGetAttributeByName(Node, "name");
	if(pAttribute != NULL)
		*ParameterName = pAttribute->Value;

	pAttribute = UXmlGetAttributeByName(Node, "value");
	if(pAttribute != NULL)
		*ParameterValue = pAttribute->Value;

	return 0;
}