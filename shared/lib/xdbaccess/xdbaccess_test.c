/* libteradbtest.c
 * libteradb
 * example program for libteradb.
 * This program is both a starting point for programming with libteradb 
 * and a testing suite to check functionalities of the db framework
 *
 * "$Id: 77f8094c5e7febaabcb938f468e8bb0594486ce8 $"
 */

#include <stdio.h>
#include <stdlib.h>
//#include <wchar.h>
#include <locale.h>

/* includes needed to use libteradb */
#include <xdbaccess.h>

#define FIELD_NUM 3
#define ENTRY_NUM 3

/* TEST ROUTINES */

int TestCreateTables();
int TestPing();
int TestInsertSimple();
int TestInsert();
int TestInsertID();
int TestGetAllRecords();
int TestUpdateRecords();
int TestGetAllRecordsFiltered();
int TestGetRecordsFiltered();
int TestCustomQuery();
int TestDropTables();
int TestUnicode();
int TestBlob();

/*****************
 *    GLOBALS    *
 *****************/
/* the tables we will use for our tests */
char *tables[3] = { "test1","test2","testunicode" };
#define NUM_TABLES 3

/* fields in table "test" */
char *field[FIELD_NUM] = { "name", "descr", "num" };

/* the rows we are going to insert */
char *insertEntry[ENTRY_NUM][FIELD_NUM] = 
{
	{ "pippo","test1","1" },
	{ "pluto","test con \"'\" (apici)","2" },
	{ "paperino","test3 con '\\' e % ","3"}
};


DBHandler *dbh;
	
#define TEST(__chk,__test) \
	if(!__chk) \
	{\
		printf("%s FAILED !!\n",__test);\
		res = 0;\
		goto end;\
	}\
	else\
	{\
		printf("%s OK \n",__test);\
		res = 1;\
	} 
	
/* the main test program */
int main(int argc, char *argv[])
{
	int res = 1;
	
	if(argc < 5) 
	{
		printf("Usage: %s <dbHost> <dbName> <dbUser> <dbPass>\n",argv[0]);
		exit(1);
	}
	
	dbh = DBInit(MODE_SQLITE, argv[1],argv[2],argv[3],argv[4]);
	if(!dbh) {
		printf("Can't init DBHandler !! \n");
		exit(1);
	}
	
	printf("Connected to database \n");
	
	/* PING DATABASE */
	TEST(TestPing(),"TestPing()");
	/* CREATE TEST TABLES */
	TEST(TestCreateTables(),"TestCreateTables()");
	/* POPULATE TABLES */
	TEST(TestInsertSimple(),"TestInsertSimple()");
	TEST(TestInsert(),"TestInsert()");
	TEST(TestInsertID(),"TestInsertID()");
	/* READ TABLES */	
	TEST(TestGetAllRecords(),"TestGetAllRecords()");
	/* MODIFY ONE ENTRY */
	TEST(TestUpdateRecords(),"TestUpdateRecords()");
	/* READ AGAIN */
	TEST(TestGetAllRecordsFiltered(),"TestGetAllRecordsFiltered()");
	TEST(TestGetRecordsFiltered(),"TestGetRecordsFiltered()");
	TEST(TestUnicode(),"TestUnicode()");
	// TEST(TestBlob(),"TestBlob()");
	/* DROP TEST TABLES */
	TEST(TestDropTables(),"TestDropTables()");
end:
	/* stop db operations */
	DBDestroy(dbh);
  
	if(res) return EXIT_SUCCESS;
	else return -1;
}

/********************************************************************
 *                          TEST ROUTINES  
 ********************************************************************/
 
/* Exec custom queries to create temporary tables used in tests */
int TestCreateTables() 
{
	char sql[255];
	sprintf(sql,"CREATE TABLE %s ( name varchar(30) NOT NULL, \
		descr varchar(30) NOT NULL, num bigint(30) NOT NULL )",tables[0]);

	if( !DBExec(dbh,sql,(unsigned long)strlen(sql)) ) return 0;
	
	sprintf(sql,"CREATE TABLE %s ( id integer PRIMARY KEY AUTOINCREMENT, name varchar(30) NOT NULL, \
		descr varchar(30) NOT NULL, num bigint(30) NOT NULL)",tables[1]);

	if(! DBExec(dbh,sql,(unsigned long)strlen(sql)) ) return 0;
	
	sprintf(sql,"CREATE TABLE %s ( id integer PRIMARY KEY AUTOINCREMENT, \
		string varchar(30) NOT NULL )",tables[2]);
	
	if(! DBExec(dbh,sql,(unsigned long)strlen(sql)) ) return 0;
	
	return 1;
}

int TestInsertSimple() 
{
	LinkedList *iList;
	int ret;
	
	iList = CreateList();
	/* prepare the new row we want to insert */
	PushTaggedValue(iList, CreateTaggedValue("name","antonio",0) );
	PushTaggedValue(iList, CreateTaggedValue("descr","insert_simple entry",0) );
	PushTaggedValue(iList, CreateTaggedValue("num","666",0) );
	
	/* insert row */
	ret = DBInsert(dbh,"test1",iList);
	
	/* release resources for both list and tagged values */
	DestroyList(iList);
	return ret;
}

#define __MODE_INSERT 0
#define __MODE_INSERTID 1
/* common routine used for both test cases TestInsert and TestInsertID. */
int TestInsertCommon(char *table,int mode) 
{
	TaggedValue *taggedEntry;
	LinkedList *insertList;
	int i;
	int n;
	unsigned long long id; /* used if InsertID is called */
	
	/* create a new list to store filter tagged values */
	insertList = CreateList();
	/* cycle on entries (rows) we are going to insert */
	for(i=0;i<ENTRY_NUM;i++) {
		
		/* first build the insert-row cycling on fields we want to define */
		for(n=0;n<FIELD_NUM;n++) {
			/* create a tagged value */
			taggedEntry = CreateTaggedValue(field[n],insertEntry[i][n],0);
			/* push it into the list using tagged-based api */
			PushTaggedValue(insertList,taggedEntry);
		}		
		
		/* entry built,now we try to insert row represented by our LinkedList */
		switch (mode) 
		{
			case __MODE_INSERT:
				if(!DBInsert(dbh,table,insertList)) {
					printf("(XXX) Insert test failed!! \n");
					return 0;
				}
				break;
			case __MODE_INSERTID:
				id = DBInsertID(dbh,table,insertList);
				if(!id) {
					printf("(XXX) Insert test failed!! \n");
					return 0;
				}
				printf("(InsertID) returned id %llu \n",id);
				break;
			default:
				break;
		}

		/* ClearList free memory for the TaggedValues we have created */ 
		ClearList(insertList);
	}
	/* release resources allocated for our filter list */
	DestroyList(insertList);
	return 1;
}

int TestInsert() 
{
	return TestInsertCommon(tables[0],__MODE_INSERT);
}

int TestInsertID() 
{
	return TestInsertCommon(tables[1],__MODE_INSERTID);
}

int TestDropTables() 
{
	char sql[255];
	int i;
	for (i=0; i<NUM_TABLES; i++)
	{
		sprintf(sql,"DROP TABLE %s",tables[i]);
		if( !DBExec(dbh,sql,(unsigned long)strlen(sql)) ) return 0;
	}
	return 1;
}

int TestPing() 
{
	return DBPing(dbh);
}

void TestPrintResults(DBResult *res)
{
	LinkedList *row;
	TaggedValue *val;
	int cnt=0;
	
	printf("Results from Get operation cointains %lu rows : \n",DBCountRows(res));
	while(row = DBFetchRow(res)) 
	{
		cnt++;
		printf("ROW %d VALUES: \n",cnt);
		while(val = ShiftTaggedValue(row))
			printf("\t{ %s => %s } \n",val->tag,(char *)val->value);
	}
}

int TestGetAllRecords() 
{
	DBResult *res;
	
	res = DBGetAllRecords(dbh,tables[0],NULL,0,NULL);
	if(!res) return 0;
	
	/* do something with results */
	TestPrintResults(res);
	
	/* done with result, let's release its resources */
	DBFreeResult(res);
	return 1;
}

int TestGetRecordsFiltered()
{
	DBResult *res;
	LinkedList *fields;
	LinkedList *filter;
	TaggedValue *filterEntry;
	
	/* create fields list */
	fields = CreateList();
	PushValue(fields,field[0]);
	PushValue(fields,field[1]);
	
	/* create filter list */
	filter = CreateList();
	filterEntry = CreateTaggedValue(field[2],"3",0);
	PushTaggedValue(filter,filterEntry);
	
	/* DBGetRecords() return a DBResult, we have to release its resource when finished */
	res = DBGetRecords(dbh,tables[0],fields,filter,0,NULL);
	if(!res) return 0;
	
	/* do something with results */
	TestPrintResults(res);
	
	/* release resources reserved for result data */
	DBFreeResult(res);
	DestroyList(filter);
	DestroyList(fields);
	
	return 1;
}

int TestUpdateRecords()
{
	LinkedList *set;
	LinkedList *filter;
	TaggedValue *tVal;
	unsigned long ret = 0;
	
	set = CreateList();
	filter = CreateList();
	
	tVal = CreateTaggedValue(field[1],"updated field",0);
	PushTaggedValue(set,tVal);
	
	tVal = CreateTaggedValue(field[2],"3",0);
	PushTaggedValue(filter,tVal);
	
	ret = DBUpdate(dbh,tables[0],set,filter,FILTER_AND);
	printf("(TestUpdateRecords) Updated %lu rows \n",ret);
	DestroyList(set);
	DestroyList(filter);
	return ret;
}

int TestGetAllRecordsFiltered()
{
	DBResult *res;
	LinkedList *filter;
	TaggedValue *filterEntry;
	
	/* create filter list */
	filter = CreateList();
	filterEntry = CreateTaggedValue(field[2],"3",0);
	PushTaggedValue(filter,filterEntry);
	
	/* DBGetRecords() return a DBResult, we have to release its resource when finished */
	res = DBGetAllRecords(dbh,tables[1],filter,0,NULL);
	if(!res) return 0;
	
	TestPrintResults(res);
	
	/* release resources reserved for lists and result */
	DBFreeResult(res);
	DestroyList(filter);
	
	return 1;
}

int TestUnicode()
{
	LinkedList *iList;
	//char string[256];
	char string1[15] = {0xd8,0xa8,0xd8,0xa7,0xd9,0x83,0xd8,0xb3,0xd8,0xaa,0xd8,0xa7,0xd9,0x86,0x00};
	char string2[20] = {0xc3,0xa8,0x20,0x53,0x63,0x68,0xc3,0xb6,0x6e,0x65,0x20,0x47,0x72,0xc3,0xbc,0xc3,0x9f,0x65,0x00};
	//char string2[3] = { 0xc3, 0xa8 , 0x00} ;
	TaggedValue *tval;
	DBResult *res;
	LinkedList *row;
	iList = CreateList();
	if(!iList) 
	{
		printf("(TestUnicode) Can't create the linkedlist used for insertion!\n");
		return 0;
	}
	//printf("Current locale: %s\n",setlocale(LC_CTYPE,NULL));
	//if (!setlocale(LC_CTYPE, "")) { /*"en_US.UTF-8"*/
    //  fprintf(stderr, "Can't set the specified locale! "
    //          "Check LANG, LC_CTYPE, LC_ALL.\n");
    //  return 0;
    //}
	printf("now locale is: %s\n",setlocale(LC_CTYPE,NULL));
	
	PushTaggedValue(iList,CreateTaggedValue("string",string1,0));
	if(!DBInsert(dbh,tables[2],iList))
	{
		printf("(TestUnicode) Can't insert unicode value!! \n");
		return 0;
	}
	printf("Inserted unicode strings: '%s' \n",string1);
	ClearList(iList);
	PushTaggedValue(iList,CreateTaggedValue("string",string2,0));
	if(!DBInsert(dbh,tables[2],iList))
	{
		printf("(TestUnicode) Can't insert unicode value!! \n");
		return 0;
	}
	printf("Inserted unicode strings: '%s' \n",string2);
	ClearList(iList);
	PushTaggedValue(iList,CreateTaggedValue("string",string1,0));
	PushTaggedValue(iList,CreateTaggedValue("string",string2,0));
	res = DBGetAllRecords(dbh,tables[2],iList,FILTER_OR,NULL);
	if(!res)
	{
		printf("(TestUnicode) Can't get unicode record! \n");
		return 0;
	}
	while(row = DBFetchRow(res))
	{
		tval = PickValue(row,2);
		if(!tval)
		{
			printf("(TestUnicode) Can't get unicode value! \n");
			return 0;
		}
		printf("Returned unicode string: '%s' \n",(char *)tval->value);
	}
	DBFreeResult(res);
	DestroyList(iList);
	return 1;
}

int TestBlob() 
{
	DBResult *res;
	LinkedList *fields;
	LinkedList *row;
	TaggedValue *blob;
	FILE *out;
	
	fields = CreateList();
	PushValue(fields,"field");
	res = DBGetRecords(dbh,"test",fields,NULL,FILTER_AND,NULL);
	DestroyList(fields);
	if(!res)
		return 0;
	row = DBFetchRow(res);
	if(!row) 
		return 0;
	blob = PickValue(row,1);
	out = fopen("./out","w");
	fwrite(blob->value,1,blob->vLen,out);
	fclose(out);
	DBFreeResult(res);
	return 1;
}
