/*
 *  sqlite_access.c
 *  libteradb
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  libteradb module built on top of libsqlite to 
 *  provide access to sqlite database trough libteradb API
 *
 */
 
#include <stdio.h>

#include "sqlite_access.h"

typedef struct sqlite_access_s {
    struct sqlite3 *sqlite;
    LinkedList *resList;    
} sqlite_access_t;

static void now_msec(sqlite3_context *context, int argc, sqlite3_value **argv) {
    struct timeval tv;
    struct tm* ptm;
    char time_string[20]; /* e.g. "2006-04-27 17:10:52" */
    long milliseconds;
    static char msec_time_string[24]; // XXX - not thread safe 
    time_t t;

    /* Obtain the time of day, and convert it to a tm struct. */
    gettimeofday (&tv, NULL);

    t = (time_t)tv.tv_sec;
    ptm = localtime (&t);   /* ptm = localtime (&tv.tv_sec); */

    /* Format the date and time, down to a single second.  */
    strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);

    /* Compute milliseconds from microseconds. */
    milliseconds = tv.tv_usec / 1000;

    /* Print the formatted time, in seconds, followed by a decimal point
    *      and the milliseconds.  */
    sprintf(msec_time_string, "%s.%03ld\n", time_string, milliseconds);

    sqlite3_result_text(context, msec_time_string, -1, NULL);

}

void *
SQLiteInitModule(DBModule *mod) 
{
    sqlite_access_t *dbh;
    
    dbh = calloc(1, sizeof(sqlite_access_t));
    if(!dbh)
        return NULL;
    
    //dbh->options.max_allowed_packet = 100000000;
    mod->connect = SQLiteConnect;
    mod->execQuery = SQLiteExec;
    mod->getResult = SQLiteGetResult;
    mod->insertId = SQLiteInsertId;
    mod->disconnect = SQLiteDisconnect;;
    mod->destroyModule = SQLiteDestroyModule;
    return (void *)dbh;
}

void 
SQLiteDestroyModule(void *mHandler) 
{
    sqlite_access_t *dbh = (sqlite_access_t *) mHandler;
    sqlite3_close(dbh->sqlite);
    free(dbh);
}

int 
SQLiteConnect(void *mHandler, char *host, char *db, char *user, char *pass) 
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    char *error = NULL;
    if (sqlite3_open(db, &dbh->sqlite) != SQLITE_OK)
    {
        fprintf(stderr, "Error: Can't connect do sqlite database: %s\n", db);
        return 0;
    } 

    // register now_msec() function so it will be available in sql queries within libteradb context
    sqlite3_create_function(dbh->sqlite, "now_msec", 0, SQLITE_UTF8, NULL, now_msec, NULL, NULL);
#ifdef DEBUG
    printf("\nConnected to: %s\n", db);
#endif
    return 1;
}

static int
SQLiteCallback(void *mHandler, int argc, char **argv, char **columnNames)
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    int i;
    LinkedList *newRow = CreateList();

    for (i = 0; i < argc; i++) 
    {
        TaggedValue *val;
        char *valstr = argv[i];
        if (!valstr)
            valstr = "(null)";
        val = CreateTaggedValue(columnNames[i], valstr, 0);
        PushTaggedValue(newRow, val);
    }
    PushValue(dbh->resList, newRow);

    return SQLITE_OK;
}

int 
SQLiteExec(void *mHandler, char *sql, unsigned long sqlLen) 
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    char *error = NULL;
    if (!dbh->resList)
        dbh->resList = CreateList();
    if (sqlite3_exec(dbh->sqlite, sql, SQLiteCallback, dbh, &error) != SQLITE_OK) {
        fprintf(stderr, "Warning: Can't execute query %s: \n\t%s\n", sql, error);
        sqlite3_free(error);
        return 0;
    }
    return 1;
}

unsigned long 
SQLiteGetResult(void *mHandler, LinkedList *resList, LinkedList *alloc) 
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    unsigned long nRows = 0;
    LinkedList *row;
    int i;
    TaggedValue *val;
    LinkedList *newRow;
    DBAlloc *newAlloc;
    
    if(!resList) 
            return 0;

    if (dbh->resList) {
	while(row = ShiftValue(dbh->resList)) {
            if(alloc) 
            {
                newAlloc = (DBAlloc *)malloc(sizeof(DBAlloc));
                if(newAlloc)
                {
                    newAlloc->type = __DBLIST;
                    newAlloc->obj = row;
                    PushValue(alloc, newAlloc);
                }
                for (i = 1; i <= ListLength(row); i++) 
                {
                    val = PickTaggedValue(row, i);
                    newAlloc = (DBAlloc *)malloc(sizeof(DBAlloc));
                    if(newAlloc)
                    {
                        newAlloc->type = __DBTVAL;
                        newAlloc->obj = val;
                        PushValue(alloc, newAlloc);
                    }
                }
            }
            PushValue(resList, row);
            nRows++;
	}
        DestroyList(dbh->resList);
        dbh->resList = NULL;
    }
    return nRows;
}	

unsigned long long 
SQLiteInsertId(void *mHandler) 
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    char *error = NULL;
    char **result = NULL;
    int nrows = 0;
    int ncolumns = 0;
    unsigned long long id = 0;
    if (sqlite3_get_table(dbh->sqlite, "SELECT last_insert_rowid()", &result, &nrows, &ncolumns, &error) != SQLITE_OK) {
        // TODO - error messages
        sqlite3_free(error);
    } else {
        if (nrows == 1 && ncolumns == 1)
            id = result[0][0];
        sqlite3_free_table(result);
    }
    return id;
}

void 
SQLiteDisconnect(void *mHandler) 
{
    sqlite_access_t *dbh = (sqlite_access_t *)mHandler;
    sqlite3_close(dbh->sqlite);
    dbh->sqlite = NULL;
}

