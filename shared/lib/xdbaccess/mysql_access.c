/*
 *  mysql_access.c
 *  libteradb
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  $Id: 4a68045135a792628b9333be118fe47710611d0f $
 *
 *  libteradb module built on top of libmysqlclient to 
 *  provide access to mysql database trough libteradb API
 *
 */
 
#include <stdio.h>

#include "mysql_access.h"

void *MysqlInitModule(DBModule *mod) 
{
	MYSQL *dbh;
	
	dbh = malloc(sizeof(MYSQL));
	if(!dbh) return NULL;
	mysql_init(dbh);
	
	//dbh->options.max_allowed_packet = 100000000;
	mod->connect = MysqlConnect;
	mod->execQuery = MysqlExec;
	mod->getResult = MysqlGetResult;
	mod->insertId = MysqlInsertId;
	mod->disconnect = MysqlDisconnect;
	mod->ping = MysqlPing;
	mod->destroyModule = MysqlDestroyModule;
	return (void *)dbh;
}

void MysqlDestroyModule(void *mHandler) 
{
	mysql_close((MYSQL *)mHandler);
	free(mHandler);
}

int MysqlConnect(void *mHandler, char *host, char *db, char *user, char *pass) 
{
	MYSQL *mysql = (MYSQL *)mHandler;
	mysql_options(mysql, MYSQL_READ_DEFAULT_GROUP, "tera");
    if(!mysql_real_connect(mysql, host, user, pass, db, 0, NULL, 0) &&
            mysql_ping(mysql) != 0)
    {
        fprintf(stderr, "Error: Can't connect do mysql database: %s\n", mysql_error(mysql));
		return 0;
    } 
#ifdef DEBUG
    printf("\nConnected to: %s\nMySQL Server Version: %s\nStats: %s\n\n",
        mysql_get_host_info(mysql), mysql_get_server_info(mysql), mysql_stat(mysql));
#endif
	return 1;
}

int MysqlExec(void *mHandler, char *sql, unsigned long sqlLen) 
{
	MYSQL *mysql = (MYSQL *)mHandler;
	if(mysql_real_query(mysql, sql, sqlLen) != 0) 
	{
		fprintf(stderr, "Warning: Can't execute query %s: \n\t%s\n", sql, mysql_error(mysql));
		return 0;
	}
	return 1;
}

unsigned long MysqlGetResult(void *mHandler, LinkedList *resList, LinkedList *alloc) 
{
	MYSQL *mysql = (MYSQL *)mHandler;
	MYSQL_RES *res;
	MYSQL_ROW row;
	MYSQL_FIELD *fields;
	unsigned int nFields;
	unsigned long *fLengths; 
	unsigned long nRows = 0;
	int i;
	TaggedValue *val;
	LinkedList *newRow;
	DBAlloc *newAlloc;
	
	if(!resList) 
		return 0;
	res = mysql_use_result(mysql); 
	if(!res) goto skip_res;
	
	nFields = mysql_num_fields(res);
	fields = mysql_fetch_fields(res);
	while(row = mysql_fetch_row(res)) 
	{
		newRow = CreateList();
		if(alloc) 
		{
			newAlloc = (DBAlloc *)malloc(sizeof(DBAlloc));
			if(newAlloc)
			{
				newAlloc->type = __DBLIST;
				newAlloc->obj = newRow;
				PushValue(alloc, newAlloc);
			}
		}
		fLengths = mysql_fetch_lengths(res);
		for (i = 0; i < (int)nFields; i++) 
		{
			val = CreateTaggedValue(fields[i].name, row[i], fLengths[i]);
			PushTaggedValue(newRow, val);
			if(alloc) 
			{
				newAlloc = (DBAlloc *)malloc(sizeof(DBAlloc));
				if(newAlloc)
				{
					newAlloc->type = __DBTVAL;
					newAlloc->obj = val;
					PushValue(alloc, newAlloc);
				}
			}
		}
		PushValue(resList, newRow);
		nRows++;
	}
	mysql_free_result(res);
skip_res:
	//nRows = (unsigned long)(mysql_affected_rows(mysql));  
	return nRows;
}	

unsigned long long MysqlInsertId(void *mHandler) 
{
	return (unsigned long)(mysql_insert_id((MYSQL *)mHandler));
}

void MysqlDisconnect(void *mHandler) 
{
	mysql_close((MYSQL *)mHandler);
}

/* returns 1 if success 0 otherwise */
int MysqlPing(void *mHandler) 
{
	if(mysql_ping((MYSQL *)mHandler) == 0) return 1;
	return 0;
}

