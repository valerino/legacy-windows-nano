/*
 *  xdbaccess.h
 *  libxdbaccess
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2009 T.E.4I. . All rights reserved.
 *
 *  "$Id: 6933240440f081b7ba7f54ea97e78694e8719eae $"
 */
 
#ifndef __TERADB_H__
#define __TERADB_H__

#include <linklist.h>
#include <terastrings.h>

#define __QUOTE__(_s) #_s
#define NOQUOTE_TAG "_$NQ$_"
#define NOQUOTE_TAGLEN 6
#define DB_NOQUOTE(_s) __QUOTE__(_$NQ$_##_s)
/* Opaque structures. User will never access theese structures directly */
typedef struct DBResult {
	LinkedList *rows; /* LinkedList of LinkedLists */
	LinkedList *allocs;
	unsigned long nRows;
} DBResult;

typedef struct DBAlloc {
	int type;
#define __DBTVAL 0
#define __DBLIST 1
#define __DBSTRING 2
	void *obj;
} DBAlloc;

/* accessor to specific module routines. 
 * User will never access this structure directly but libteradb API 
 * should be used instead
 */
typedef struct DBModule {
	/* initialize db subsystem and returns a pointer to a new db context i
	 * (needed for further operations) */
	int (*connect)(void *mHandler,char *host,char *db,char *username,char *password);
	/* executes an sql query on the database associated to dbh */
	int (*execQuery)(void *mHandler,char *sql,unsigned long sqlLen);
	/* 
	 * fills res (LinkedList)  with one LinkedList for each row.
	 * Each row (LinkedList) contains TaggedValues.
	 * allocs will be filled with pointers to allocated memory, so the caller 
	 * can free resources when finished using them. 
	 * returns the number of rows affected by last operation on database */
	unsigned long (*getResult)(void *mHandler,LinkedList *res,LinkedList *allocs);
	/* returns the automatic id for the last insert operation */
	unsigned long long (*insertId)(void *mHandler); /* XXX - not sure */
	int (*ping)(void *mHandler);
	void (*disconnect)(void *mHandler);
	void (*destroyModule)(void *mHandler);
} DBModule;

/* opaque structure used internally by libteradb */
typedef struct DBHandler {
	void *mHandler;
	char *dbHost;
	char *dbName;
	char *username;
	char *password;
	int donotstore;
	int getfromstorepath;
	DBModule dbModule;
	int mode;
	pthread_mutex_t lock;
} DBHandler;

#define MODE_MYSQL 0
#define MODE_MSSQL 1
#define MODE_SQLITE 2

/* This is the first routine that must be called before starting any db operations 
 * It creates a new database handler initializing the correct underlying module 
 * mode can be MODE_MYSQL or MODE_MSSQL.
 * Return a DBHandler pointer that caller must use for any further operation.
 * When all is done with database, DBDestroy should be used on the DBHandler to
 * release all resources that have been allocated internally to provide db operations
 */
DBHandler *DBInit(int mode,char *host,char *db,char *user,char *pass);

/* checks the connection with the database. 
 * Returns 1 if success 0 otherwise */
int DBPing(DBHandler *dbh);

/* release resources associated to dbh */
void DBDestroy(DBHandler *dbh);

/* executes an sql query. return 1 if success 0 otherwise
 * WARNING: this routine doen't handle locking by its own ...
 * beware if using in a multithreaded environment...use DBLock() and DBUnlock()
 * to explicitly lock DBHandler */
int DBExec(DBHandler *dbh,char *sql,unsigned long sqlLen);

/* store result in our internal memory , for later use. 
 * A pointer to DBResult structure is returned. 
 * This is an opaque structure and caller must
 * access rows stored in DBResult trough the DBFetchRow() routine 
 */
DBResult *DBStoreResult(DBHandler *dbh);

/* returns the number of rows stored in DBResult *res */
unsigned long DBCountRows(DBResult *res);

 /* Returns a LinkedList of TaggedValues (aka a tagged linkedlist)
 * representing 1 row of db data.
 * Caller MUST not free resources for returned LinkedLists and TaggedValues directly,
 * at end of operations on data that comes with a result, he should simply call DBFreeResult() 
 * to release all resources associated to the specific result (and the DBResult itself)
 */ 
LinkedList *DBFetchRow(DBResult *res);

/* free resources for stored result and all associated data (LinkedList, TaggedValues , strings) */
void DBFreeResult(DBResult *res);

/* filterOp can be : */
#define FILTER_AND 0
#define FILTER_OR 1
#define FILTER_GT 2
#define FILTER_LT 3

/* Executes a "SELECT *" query from specified table. 
 * taggedFilterList is a tagged-LinkedList used to build the WHERE statement.
 * The built statement will be of the form tag=value and filterOp defines
 * if statements must be joined with an "AND" or "OR" operation.
 * For example, a list containing 2 TaggedValues should be rendered as :
 * WHERE tag1=value1 <filterOp> tag2=value2.
 * if filterOp is an AND operation:
 * WHERE tag1=value1 AND tag2=value2
 */
DBResult *DBGetAllRecords(DBHandler *dbh,char *table,
	LinkedList *taggedFilterList,int filterOp,char *extra);

/*
 * works exacly like DBGetAllRecords but let caller define which fields should go in the select 
 * statement trough a simple value-based LinkedList (the "fields" arg).
 * The built statement will be of the form :
 * SELECT field1,field2,field3 FROM table ...
 * if you need to select from multiple tables simply pass as argument a char *table 
 * containing a comma separated list of tables. Field can obviously be of the form <table.field>.
 * For example :
 *  char *table == "table1,table2"
 *  LinkedList *fields contains 2 fields from table1 and 1 field from table2, each in the form "<table.field>"
 *  the rendered statement will be:
 *    SELECT table1.field1,table1.field2,table2.field1 FROM table1,table2 ....
 *
 * check description of DBGetAllRecords() for an explanation about the taggedFilterList param 
 */
DBResult *DBGetRecords(DBHandler *dbh,char *table,LinkedList *fields,
	LinkedList *taggedFilterList,int filterOp,char *extra);

/* insert values in "table". returns the number of rows inserted 
 * taggedValueList contains the row that will be inserted. TaggedValues inside
 * this list represents the "field => value" tuple used to build the sql statement 
 * The built statement will be of the form:
 * INSERT INTO table (field1,field2,...,fieldN) VALUES (value1,value2,...,valueN)
 * returns 1 if success 0 otherwise
 */
int DBInsert(DBHandler *dbh,char *table,LinkedList *taggedValueList);

/* This routine works exactly like the DBInsert() one, but in addition 
 * it returns the automatic id assigned on insert by the DB
 */
unsigned long long DBInsertID(DBHandler *dbh,char *table,LinkedList *taggedValueList);

/* Update "table" modifying fields as specified in taggedSet and selecting items to modify
 * trough taggedFilter (and filterOp) in the same way of DBGetAllRecords()
 */
unsigned long DBUpdate(DBHandler *dbh,char *table,LinkedList *taggedSet,LinkedList *taggedFilter,int filterOp);

/* Removes entries from "table" using taggedFilter to select rows to remove.
 * returns the number of rows removed from db
 */
unsigned long DBDelete(DBHandler *dbh,char *table,LinkedList *taggedFilter,int filterOp);

/* theese routines should be used when calling DBExec() directly ... 
 * (remember that it doesn't handle locking by its own) */
void DBLock(DBHandler *dbh);

void DBUnlock(DBHandler *dbh);
#endif
