/*
 *  mysql_access.h
 *  libxdbaccess
 *
 *  Created by xant on 12/1/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  "$Id: dc8940b83c1b7746f53b1991a22d5ebd45ca35f5 $"
 */

#ifndef __MYSQL_ACCESS_H__
#define __MYSQL_ACCESS_H__

#ifdef WIN32
#ifndef SOCKET
typedef int SOCKET;
#endif
#endif

#include <mysql.h>
#include <linklist.h>
#include "xdbaccess.h"

void *MysqlInitModule(DBModule *mod);
void MysqlDestroyModule(void *mHandler);
int MysqlConnect(void *mHandler,char *host,char *db,char *user,char *pass);
int MysqlExec(void *mHandler,char *sql,unsigned long sqlLen);
unsigned long MysqlGetResult(void *mHandler,LinkedList *res,LinkedList *alloc);
unsigned long long MysqlInsertId(void *mHandler);
void MysqlDisconnect(void *mHandler);
int MysqlPing(void *mHandler);


#endif

