/*
 *  xdbaccess.c
 *  xdbaccess.c
 *
 *  Created by xant on 12/3/05.
 *  Copyright 2009 T.E.4I. . All rights reserved.
 *
 *  "$Id: 9d790bb1b90518c22bf67af5f0dcf685ef796924 $"
 *
 */

#ifdef HAVE_MYSQL
#include "mysql_access.h"
#endif

#ifdef HAVE_MSSQL
#include "tds/tds_access.h"
#endif

#include <stdio.h>
#include <escaping.h>
#include <linklist.h>
#include <fbuf.h>
#include "xdbaccess.h"

/* create a new database handler initializing the correct underlying module 
 * mode can be MODE_MYSQL or MODE_MSSQL */
DBHandler *DBInit(int mode, char *host, char *db, char *user, char *pass) 
{
    DBHandler *newHandler;
    newHandler = (DBHandler *)calloc(1, sizeof(DBHandler));
    switch(mode) 
    {
#ifdef HAVE_MYSQL
        case MODE_MYSQL:
            newHandler->mHandler = MysqlInitModule(&newHandler->dbModule);
        break;
#endif
#ifdef HAVE_MSSQL
        case MODE_MSSQL:
            newHandler->mHandler = TdsInitModule(&newHandler->dbModule);
        break;
#endif
#ifdef HAVE_SQLITE
        case MODE_SQLITE:
            newHandler->mHandler = SQLiteInitModule(&newHandler->dbModule);
#endif
        break;
        default:
            printf("Unsupported backend mode %d\n", mode);
            goto init_err;
    }
    newHandler->mode = mode;
    if(host)
        newHandler->dbHost = strdup(host);
    if(db)
        newHandler->dbName = strdup(db);
    if(user)
        newHandler->username = strdup(user);
    if(pass)
        newHandler->password = strdup(pass);
    
    /* try connect to database */
    if(!newHandler->dbModule.connect(newHandler->mHandler, host, db, user, pass))
        goto init_err;
    pthread_mutex_init(&newHandler->lock, NULL);
    return newHandler;
init_err:
    if(newHandler) free(newHandler);
    return NULL;
}

int DBPing(DBHandler *dbh) 
{
    if (dbh->dbModule.ping)
    return dbh->dbModule.ping(dbh->mHandler);
    else // if module doesn't implement ping we assume connection can't be lost
        return 1;
}

/* release resources associated to dbh */
void DBDestroy(DBHandler *dbh) 
{
    dbh->dbModule.destroyModule(dbh->mHandler);
    if(dbh->dbHost) free(dbh->dbHost);
    if(dbh->dbName) free(dbh->dbName);
    if(dbh->username) free(dbh->username);
    if(dbh->password) free(dbh->password);
    pthread_mutex_destroy(&dbh->lock);
    free(dbh);
}

/* WARNING !!! NO LOCKING HERE... USE CAREFULLY */
int DBExec(DBHandler *dbh, char *sql, unsigned long sqlLen) 
{
#ifdef DEBUG
    printf("Executing : %s \n", sql);
#endif
    return dbh->dbModule.execQuery(dbh->mHandler, sql, sqlLen);
}

/*
int
DBExecXml(DBHAndler *dbh, TXml *xquery)
{
}
*/

/* store result in our internal memory , for later use. 
 * A pointer to DBResult structure is returned. 
 * check header file for more details
 */
DBResult *DBStoreResult(DBHandler *dbh) 
{
    DBResult *res = (DBResult *)calloc(1, sizeof(DBResult));
    res->rows = CreateList();
    res->allocs = CreateList();
    res->nRows = dbh->dbModule.getResult(dbh->mHandler, res->rows, res->allocs);
    return res;
}

/* returns the number of rows stored in DBResult *res */
unsigned long DBCountRows(DBResult *res) 
{
    return ListLength(res->rows);
}

/* Returns a LinkedList of TaggedValues (aka a tagged linkedlist)
 * representing 1 row of db data.
 * check header file for memory-related details 
*/ 
LinkedList *DBFetchRow(DBResult *res) 
{
    return ShiftValue(res->rows);
}

/* free resources for stored result and all associated data (LinkedList, TaggedValues , strings) */
void DBFreeResult(DBResult *res) 
{
    DBAlloc *alloc;
    if(!res) return; /* just for safety ... but maybe too paranoid */
    if(res->rows) DestroyList(res->rows);
    if(res->allocs) 
    {
        while((alloc=(DBAlloc *)ShiftValue(res->allocs)) != NULL) 
        {
            switch(alloc->type) 
            {
                case __DBSTRING:
                    free(alloc->obj);
                    break;
                case __DBLIST:
                    while(ListLength((LinkedList *)alloc->obj) > 0) 
                        ShiftValue((LinkedList *)alloc->obj);
                    DestroyList((LinkedList *)alloc->obj);
                    break;
                case __DBTVAL:
                    DestroyTaggedValue((TaggedValue *)alloc->obj);
                    break;
                default:
                    break;
            }
            free(alloc);
        }
        DestroyList(res->allocs);
    }
    free(res);
}    

/* builds an sql query */
char *DBBuildQuery(char *preamble, char *table,
    LinkedList *taggedFilterList, int filterOp, char *extra) 
{
    unsigned long i;
    TaggedValue *filterEntry;
    fbuf_t sql = FBUF_STATIC_INITIALIZER;
    char *valTmp = NULL;
    unsigned long valLen;
    char *flag;
    int noQuote;
        
    fbuf_printf(&sql, "%s %s ", preamble, table);
    
    if(taggedFilterList && ListLength(taggedFilterList) > 0) 
    {
        fbuf_add(&sql, "WHERE ");
        for(i=1;i<=ListLength(taggedFilterList);i++) 
        {
            noQuote = 0;
            filterEntry = PickTaggedValue(taggedFilterList, i);
            if(!filterEntry) 
                break;
            if(!filterEntry->tag || !filterEntry->value)
                return NULL;
            if(*((char *)filterEntry->value) == 0)
                continue;
            /* escape single quotes */
            flag = (char *)filterEntry->value;
            if(strncmp(flag, NOQUOTE_TAG, NOQUOTE_TAGLEN) == 0)
            {
                noQuote = 1;
                valLen = filterEntry->vLen-NOQUOTE_TAGLEN;
                valTmp = malloc(valLen+1);
                if(valTmp)
                {
                    strncpy(valTmp, (char *)filterEntry->value+NOQUOTE_TAGLEN, valLen);
                    valTmp[valLen] = 0;
                }
            }
            else {
                byte_escape('\'', '\'', (char *)filterEntry->value,
                    filterEntry->vLen, &valTmp, &valLen);
            }
            if(i != 1) 
            {
                if(filterOp == FILTER_AND)         
                    fbuf_add(&sql, "AND ");
                else 
                    fbuf_add(&sql, "OR ");
            }
            if(valTmp) {
                fbuf_add(&sql, filterEntry->tag);

                if(!noQuote)
                    fbuf_add(&sql, "='");
                else
                    fbuf_add(&sql, "=");
                fbuf_add_binary(&sql, valTmp, valLen);

                if(!noQuote) 
                    fbuf_add(&sql, "' ");
                else
                    fbuf_add(&sql, " ");

                free(valTmp);
                valTmp = NULL;
            }
        }
    }
    return fbuf_data(&sql);
}


DBResult *DBGetAllRecords(DBHandler *dbh, char *table,
    LinkedList *taggedFilterList, int filterOp, char *extra) 
{
    char *sql;
    DBResult *res = NULL;
    
    char *select = "SELECT * FROM";
    
    if(!table) return NULL;
    
    sql = DBBuildQuery(select, table, taggedFilterList, filterOp, extra);

    pthread_mutex_lock(&dbh->lock);
    if(DBExec(dbh, sql, (unsigned long)strlen(sql))) 
        res = DBStoreResult(dbh);
    pthread_mutex_unlock(&dbh->lock);
    free(sql);
    return res;
}

/* build sql query using LinkedLists values and try to execute it.
 * If any result is present a DBResult pointer is returned back to caller,
 * otherwise NULL is returned.
 */
DBResult *DBGetRecords(DBHandler *dbh, char *table, LinkedList *fields,
    LinkedList *taggedFilterList, int filterOp, char *extra) 
{
    char *sql;
    DBResult *res = NULL;
    /* 22 = strlen("SELECT DISTINCT ") + strlen("FROM") + 2 whitespaces + tralinig \0 */
    int selectLen = 22;
    unsigned long i;
    char *field;
    char *select = malloc(selectLen);

    if(!select) return NULL;

    strcpy(select, "SELECT DISTINCT ");
    for (i=1;i<=ListLength(fields);i++) 
    {
        field = PickValue(fields, i);
        if(!field) 
            break;
        selectLen += (unsigned long)strlen(field);
        if(i != 1) 
            selectLen += 1;
        select = realloc(select, selectLen);
        if(i != 1) 
            strcat(select, ", ");
        strcat(select, field);
    }
    strcat(select, " FROM");
    sql = DBBuildQuery(select, table, taggedFilterList, filterOp, extra);
    pthread_mutex_lock(&dbh->lock);
    if(DBExec(dbh, sql, (unsigned long)strlen(sql))) 
        res = DBStoreResult(dbh);
    pthread_mutex_unlock(&dbh->lock);
    free(select);
    if(sql)
        free(sql);
    return res;
}

static int DBInsertCommon(DBHandler *dbh, char *table, LinkedList *taggedValueList)
{
    int res = 0;
    unsigned long i;
    TaggedValue *entry;
    fbuf_t *sql;
    fbuf_t *values;
    fbuf_t *fields;
    char *valTmp = NULL;
    int valLen;

    unsigned long off;
    int noQuote;
    char *flag;

    if(!table || !taggedValueList) 
        return 0;

    sql = fbuf_create(FBUF_MAXLEN_NONE);
    if(!sql) 
        return 0;
    fbuf_printf(sql, "INSERT INTO %s ", table);

    fields = fbuf_create(FBUF_MAXLEN_NONE);
    values = fbuf_create(FBUF_MAXLEN_NONE);
    fbuf_add(fields, "(");
    fbuf_add(values, "(");
    /* cycle on the list containing TaggedValues 
    * with entries we want to insert on db */
    for(i=1;i<=ListLength(taggedValueList);i++) 
    {
        noQuote = 0;
        /* don't modify source list, just access items read-only 
        * (without removing them from list)*/
        entry = PickTaggedValue(taggedValueList, i);
        if(!entry || !entry->value || !entry->vLen)
            continue;
        
        flag = (char *)entry->value;
        if(strncmp(flag, NOQUOTE_TAG, strlen(NOQUOTE_TAG)) == 0) {
            noQuote = 1;
            byte_escape('\'', '\'', (char *)entry->value+NOQUOTE_TAGLEN,
                entry->vLen-NOQUOTE_TAGLEN, &valTmp, (unsigned long *)&valLen);
        } else {
            byte_escape('\'', '\'', (char *)entry->value,
                entry->vLen, &valTmp, (unsigned long *)&valLen);
        }
        if(i != 1) {
            fbuf_add(fields, ", ");
            fbuf_add(values, ", ");
        }
        fbuf_add(fields, entry->tag);

        if(!noQuote)
            fbuf_add(values, "'");

        if(valTmp) {
            fbuf_add_binary(values, (const char *)valTmp, valLen);
            free(valTmp);
            valTmp = NULL;
        }

        if(!noQuote)
            fbuf_add(values, "'");
    }
    fbuf_add(fields, ")");
    fbuf_add(values, ")");
    fbuf_printf(sql, "%s VALUES ", fbuf_data(fields));
    fbuf_add_binary(sql, fbuf_data(values), fbuf_used(values));

    res = DBExec(dbh, fbuf_data(sql), fbuf_used(sql)); 

    if(fields)
            fbuf_free(fields);
    if(values)
            fbuf_free(values);
    if(sql)
            fbuf_free(sql);
    return res;
}

/* insert specified values in "table" and fetch the automatic assigned id */
unsigned long long DBInsertID(DBHandler *dbh, char *table, LinkedList *taggedValueList) 
{
    int res = 0;

    if (dbh->donotstore)
            return 1;

    pthread_mutex_lock(&dbh->lock);
    res = DBInsertCommon(dbh, table, taggedValueList);
    if(res) 
            res = (int)dbh->dbModule.insertId(dbh->mHandler);
    pthread_mutex_unlock(&dbh->lock);
    return res;
}

/* insert one row in "table". returns 1 if success 0 otherwise */
int DBInsert(DBHandler *dbh, char *table, LinkedList *taggedValueList) 
{
    int res = 0;

    if (dbh->donotstore)
        return 1;

    pthread_mutex_lock(&dbh->lock);
    res = DBInsertCommon(dbh, table, taggedValueList);
    pthread_mutex_unlock(&dbh->lock);
    return res;
}

/* Modify entries in a table */
unsigned long DBUpdate(DBHandler *dbh, char *table, LinkedList *taggedSet, LinkedList *taggedFilter, int filterOp)
{
    int i;
    int ret;
    char *set;
    char *sql = NULL;
    char *preamble;
    TaggedValue *tVal;
    char *escape;
    char *flag;
    int noQuote;
    unsigned long escapeLen;
    unsigned long off;
    
    if(!table || !taggedSet) 
        return 0;
        
    ret = 0;
    
    set = strdup(" SET ");
    preamble = malloc(strlen("UPDATE ")+strlen(table)+1);
    strcpy(preamble, "UPDATE ");
    strcat(preamble, table);
    
    for(i=1;i<=(int)ListLength(taggedSet);i++)
    {
        noQuote = 0;
        tVal = PickTaggedValue(taggedSet, i);
        if(!tVal)
            break;
        
        flag = tVal->value;
        if(strncmp(flag, NOQUOTE_TAG, strlen(NOQUOTE_TAG)) == 0)
        {
            noQuote = 1;
            byte_escape('\'', '\'', (char *)tVal->value+NOQUOTE_TAGLEN,
                tVal->vLen-NOQUOTE_TAGLEN, &escape, &escapeLen);
        }
        else 
        {
            byte_escape('\'', '\'', (char *)tVal->value,
                tVal->vLen, &escape, &escapeLen);
        }

        set = realloc(set, strlen(set)+strlen(tVal->tag)+escapeLen+5);
        if(i!=1)
            strcat(set, ", ");
        
        strcat(set, tVal->tag);
        if(noQuote) 
            strcat(set, "=");
        else 
            strcat(set, "='");
        off = (unsigned long)strlen(set);
        memcpy(set+off, escape, escapeLen);
        *(set+off+escapeLen) = 0;
        if(!noQuote) 
            strcat(set, "'");
        free(escape);
    }
    preamble = malloc(strlen(set)+strlen(table)+9);
    sprintf(preamble, "UPDATE %s %s", table, set);
    sql = DBBuildQuery(preamble, "", taggedFilter, filterOp, NULL);
    
    pthread_mutex_lock(&dbh->lock);
    ret = DBExec(dbh, sql, (unsigned long)strlen(sql)); 
    pthread_mutex_unlock(&dbh->lock);

    if(preamble) free(preamble);
    if(set)
        free(set);
    if(sql)
        free(sql);
    return ret;
}

/* Delete rows from a table */
unsigned long DBDelete(DBHandler *dbh, char *table, LinkedList *taggedFilter, int filterOp)
{
    char *sql;
    DBResult *res;
    int ret = 0;
    
    sql = DBBuildQuery("DELETE FROM ", table, taggedFilter, filterOp, NULL);
    if(!sql)
        return ret;
    
    pthread_mutex_lock(&dbh->lock);
    if( DBExec(dbh, sql, (unsigned long)strlen(sql)) ) 
    {
        res = DBStoreResult(dbh);
        ret = res->nRows;
        DBFreeResult(res);
    }
    pthread_mutex_unlock(&dbh->lock);

    free(sql);
    return ret;
}

/* theese routines should be used when calling DBExec() directly ... 
 * (remember that it doesn't handle locking by its own) */
void DBLock(DBHandler *dbh)
{
    pthread_mutex_lock(&dbh->lock);
}

void DBUnlock(DBHandler *dbh)
{
    pthread_mutex_unlock(&dbh->lock);
}
