/*
 *  SQLite_access.h
 *  libxdbaccess
 *
 *  Created by xant on 12/1/05.
 *  Copyright 2005 T.E.4I. . All rights reserved.
 *
 *  "$Id: 650ec45623ca50800af4006c74628f2b5263675f $"
 */

#ifndef __SQLite_ACCESS_H__
#define __SQLite_ACCESS_H__

#ifdef WIN32
#ifndef SOCKET
typedef int SOCKET;
#endif
#endif

#include <sqlite3.h>
#include <linklist.h>
#include "xdbaccess.h"

void *SQLiteInitModule(DBModule *mod);
void SQLiteDestroyModule(void *mHandler);
int SQLiteConnect(void *mHandler,char *host,char *db,char *user,char *pass);
int SQLiteExec(void *mHandler,char *sql,unsigned long sqlLen);
unsigned long SQLiteGetResult(void *mHandler,LinkedList *res,LinkedList *alloc);
unsigned long long SQLiteInsertId(void *mHandler);
void SQLiteDisconnect(void *mHandler);
int SQLitePing(void *mHandler);


#endif

