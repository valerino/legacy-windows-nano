// Copyright 2009, TE4I

const char * hex_escape(const unsigned char *buf, int inlen);
const  char * ascii_escape(const unsigned char *buf, int inlen);
const unsigned char *shell_varname(const unsigned char *str);
const unsigned char *shell_escape(const unsigned char *str);
unsigned long byte_escape(char ch, char esc, char *buffer, unsigned long len, char **dest, unsigned long *newlen);
