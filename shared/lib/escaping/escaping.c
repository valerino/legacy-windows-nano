// Copyright 2009, TE4I

#include "escaping.h"
#include <fbuf.h>
#include <string.h>

const char *
hex_escape(const unsigned char *buf, int inlen)
{
    static fbuf_t *fbuf = NULL;
    int i;

    if (!fbuf) {
	fbuf = fbuf_create(FBUF_MAXLEN_NONE);
	if (!fbuf) 
	    return NULL;
    } else {
	fbuf_clear(fbuf);
    }

    for (i = 0; i < inlen; i++)
        fbuf_printf(fbuf, "%s%02x", (i>0? " ":""), buf[i]);

    return fbuf_data(fbuf);

}

const char *
ascii_escape(const unsigned char *buf, int inlen)
{
    static fbuf_t *fbuf = NULL;
    int i;

    if (!fbuf) {
	fbuf = fbuf_create(FBUF_MAXLEN_NONE);
	if (!fbuf) 
	    return NULL;
    } else {
	fbuf_clear(fbuf);
    }

    for (i = 0; i < inlen; i++) {
        switch(buf[i]) {
            case '\b':
                fbuf_add(fbuf, "\\b");
                break;
            case '\t':
                fbuf_add(fbuf, "\\t");
                break;
            case '\n':
                fbuf_add(fbuf, "\\n");
                break;
            case '\r':
                fbuf_add(fbuf, "\\r");
                break;
            case ';':
                fbuf_add(fbuf, "\\x3b");
                break;
            default:
                if (buf[i] <= 0x1f || buf[i] >= 0x7f) 
                    fbuf_printf(fbuf, "\\x%02x", buf[i]);
                else 
                    fbuf_printf(fbuf, "%c", buf[i]);
        }
    }

    return fbuf_data(fbuf);
}

/**
 * \brief Escape a string for use as a shell variable name in sh.
 * \param str string to escape
 * \returns static string
 */
const unsigned char *
shell_varname(const unsigned char *str)
{
    static unsigned char buffer[130];
    unsigned int i = 0, j = 0;

    if (!str)
	return str;

    for (; str[i] != '\0' && j < sizeof(buffer)-2; i++) {
	if ((str[i] >= 'a' && str[i] <= 'z') ||
	    (str[i] >= 'A' && str[i] <= 'Z')) {
	    buffer[j++] = str[i];
	} else if (str[i] >= '0' && str[i] <= '9') {
	    if (i == 0)
		buffer[j++] = '_';
	    buffer[j++] = str[i];
	} else {
	    buffer[j++] = '_';
	}
    }
    
    buffer[j++] = '\0';

    return buffer;
}

/**
 * \brief Escape a string for use in sh.
 * \param str string to escape
 * \returns static string
 */
const unsigned char *
shell_escape(const unsigned char *str)
{
    static unsigned char buffer[1024];
    unsigned int i = 0;
    unsigned int j = 0;
    int do_quote = 0;

    if (!str)
	return str;

    buffer[i++] = '"';
    while (str[j] != '\0' && i < sizeof(buffer)-4-2) {
	if ((str[j] < 0x20 && str[j] != '\r' && str[j] != '\n' && str[j] != '\b' && str[j] != '\t')	
	    || str[j] >= 0x80) {			// High ASCII
	    buffer[i++] = '\\';				// Display as octal
	    buffer[i++] = ((str[j]>>6)&03)+'0';
	    buffer[i++] = ((str[j]>>3)&07)+'0';
	    buffer[i++] = ((str[j]>>0)&07)+'0';
	    do_quote = 1;				// We've got special characters
	} else if (str[j] == '"') {			// "'"
	    buffer[i++] = '\\';
	    buffer[i++] = '"';
	    do_quote = 1;				// We've got special characters
	} else {					// Anything else is passed on
	    if (!((str[j] >= '0' && str[j] <= '9')
		  || (str[j] == '.' || str[j] == ',')
		  || (str[j] >= 'a' && str[j] <= 'z')
		  || (str[j] >= 'A' && str[j] <= 'Z'))) {
		do_quote = 1;
	    }
	    buffer[i++] = str[j];
	}
	j++;
    }

    buffer[i++] = '"';
    buffer[i++] = '\0';

    if (do_quote)
	return buffer;		// we've had to escape stuff
    else
	return str;		// return pointer to original string
}

unsigned long
byte_escape(char ch, char esc, char *buffer, unsigned long len, char **dest, unsigned long *newlen)
{
	char *newbuf;
	unsigned long buflen;
	unsigned long i;
	unsigned long cnt;
	int escape;
	char *p;
	unsigned long off;
	
	if(len == 0) 
		return 0;
			
	newbuf = (char *)malloc(len);
	if(!newbuf)
		return 0;
	buflen = len;
	p = buffer;
	off = 0;
	cnt = 0;
	for(i=0;i<len;i++)
	{
		escape = 0;
		if(*p == ch) 
			cnt ++;
		
		if(*p == ch || *p == esc) 
			escape = 1;
			
		if(escape) 
		{
			buflen++;
			newbuf = (char *)realloc(newbuf, buflen+1);
			memcpy(newbuf+off, &esc, 1);
			off++;
		}
		memcpy(newbuf+off, p, 1);
		p++;
		off++;
	}
	*dest = newbuf;
	*newlen = buflen;
	return cnt;
}

