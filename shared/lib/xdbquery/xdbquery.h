#include <txml.h>
/*
    per esempio potremmo creare una query tipo WHERE ( a = pippo and b != pluto ) or ( c = cazzo )
        facendo:

        a = dbEq("a", "pippo");
        b = dbNeq("b", "pluto");
        c = dbEq("c", "cazzo");
        expr dbOr(dbAnd(a, b), c);
        1/15/09 3:05 PM
         
        expr =
        o meglio ancora :
        SqlStart();
        char *test = 
            SqlOr(2,
                SqlAnd(2,
                    SqlEq("a", "pippo", 1),
                    SqlNeq("b", "pluto", 1)
                ), 
                SqlEq("c", "cazzo", 1)
            );
        SqlEnd();

*/

void SqlStart(void);
void SqlEnd(void);
char *SqlOr(int count, ...);
char *SqlAnd(int count, ...);
char *SqlEq(char *field, char *value, int quote);
char *SqlNeq(char *field, char *value, int quote);
char *SqlLike(char *field, char *mask);
char *Xml2Sql(TXml *xml);

