#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fbuf.h>
#include <linklist.h>
#include <escaping.h>
#include "xdbquery.h"


#define SQL_SKIP_QUOTING(__n, __nq) \
    if (__n)  \
        if (__n->value)  \
            if (strlen(__n->value) == 1 && *__n->value == '1')  \
                __nq = 1;

LinkedList *alloc = NULL;

void
SqlStart(void)
{
    alloc = CreateList();
}

void
SqlEnd(void)
{
    if (alloc) {
        fbuf_t *v = ShiftValue(alloc);
        while(v) {
            fbuf_free(v);
            v = ShiftValue(alloc);
        }
        DestroyList(alloc);
        alloc = NULL;
    }
}

static fbuf_t *
Xml2Sql_ExpandNode(XmlNode *node)
{
    XmlNode *child = NULL;
    int count;
    fbuf_t *out = fbuf_create(FBUF_MAXLEN_NONE);
    if (strcasecmp(node->name, "and") == 0 ||
        strcasecmp(node->name, "or") == 0 ) 
    {
        fbuf_t *tmp = fbuf_create(FBUF_MAXLEN_NONE);
        for (count = 1; count <= XmlCountChildren(node); count++) {
            child = XmlGetChildNode(node, count);
            fbuf_t *sub = Xml2Sql_ExpandNode(child); // recursion here
            if (fbuf_used(tmp))
                fbuf_printf(tmp, " %s ", node->name);
            fbuf_printf(tmp, "(%s)", fbuf_data(sub));
            fbuf_free(sub);
        }
        fbuf_concat(out, tmp);
        fbuf_free(tmp);
    } else if (strcasecmp(node->name, "eq") == 0) {
        if (XmlCountAttributes(node) >= 1) {
            XmlNodeAttribute *field = XmlGetAttributeByName(node, "field");
            XmlNodeAttribute *noquote = XmlGetAttributeByName(node, "noquote");
            if (field) {
                int nq = 0;
                fbuf_printf(out, "%s = ", field->value);

                SQL_SKIP_QUOTING(noquote, nq);

                if (nq)
                    fbuf_add(out, node->value);
                else
                    fbuf_printf(out, "'%s'", node->value);
            } else {
                // TODO - error messages 
            }
        } else {
            // TODO - error messages 
        }
    } else if (strcasecmp(node->name, "neq") == 0) {
        if (XmlCountAttributes(node) >= 1) {
            XmlNodeAttribute *field = XmlGetAttributeByName(node, "field");
            XmlNodeAttribute *noquote = XmlGetAttributeByName(node, "noquote");
            if (field) {
                int nq = 0;
                fbuf_printf(out, "%s != ", field->value);

                SQL_SKIP_QUOTING(noquote, nq);

                if (nq)
                    fbuf_add(out, node->value);
                else
                    fbuf_printf(out, "'%s'", node->value);
            } else {
                // TODO - error messages 
            }
        } else {
            // TODO - error messages 
        }
    } else if (strcasecmp(node->name, "like") == 0) {
        if (XmlCountAttributes(node) == 2) {
            XmlNodeAttribute *field = XmlGetAttributeByName(node, "field");
            XmlNodeAttribute *value = XmlGetAttributeByName(node, "value");
            if (field && value) {
                fbuf_printf(out, "%s like %s", field->value, value->value);
            } else {
                // TODO - error messages 
            }
        }
    } else {
        /* TODO - Error message */
    }
    return out;
}

static void
Xml2Sql_Where(XmlNode *query, XmlNode *where, fbuf_t *out)
{
    XmlNode *node = XmlGetChildNode(where, 1);
    if (node) {
        fbuf_t *buf = Xml2Sql_ExpandNode(node);
        if (buf) {
            fbuf_add(out, " where ");
            fbuf_concat(out, buf);
            fbuf_free(buf);
        }
    }
}

static void
Xml2Sql_Select(XmlNode *query, XmlNode *op, fbuf_t *out)
{
    int i;
    XmlNode *from = NULL;
    XmlNode *where = NULL;
    fbuf_t *fbuf = fbuf_create(FBUF_MAXLEN_NONE);

    for (i = 1; i <= XmlCountChildren(op); i++) {
        XmlNode *field = XmlGetChildNode(op, i);
        if (strcasecmp(field->name, "field") == 0) {
            XmlNodeAttribute *name = XmlGetAttributeByName(field, "name");
            if (name) {
                if (fbuf_used(fbuf))
                    fbuf_add(fbuf, ",");
                fbuf_add(fbuf, name->value);
            } else {
                // TODO - Error Messages
            }
        } else {
            // TODO - Error Messages
        }
    }
    fbuf_printf(out, "%s ", op->name);
    fbuf_concat(out, fbuf);
    fbuf_clear(fbuf);
    from = XmlGetChildNodeByName(query, "from");
    if (from) {
        for (i = 1; i <= XmlCountChildren(from); i++) {
            XmlNode *table = XmlGetChildNode(from, i);
            if (strcasecmp(table->name, "table") == 0) {
                if (fbuf_used(fbuf))
                    fbuf_add(fbuf, ",");
                fbuf_add(fbuf, table->value);
            } else if (strcasecmp(table->name, "sqlquery")) {
                // TODO - handle nested subqueries
            }else {
                // TODO - Error Messages
            }
        }
        fbuf_printf(out, " from ");
        fbuf_concat(out, fbuf);
    }
    fbuf_free(fbuf);
    where = XmlGetChildNodeByName(query, "where");
    if (where)
        Xml2Sql_Where(query, where, out);
}

static void
Xml2Sql_Update(XmlNode *query, XmlNode *op, fbuf_t *out)
{
    int i;
    fbuf_t *fbuf = fbuf_create(FBUF_MAXLEN_NONE);
    XmlNode *set = NULL;
    XmlNode *where = NULL;

    for (i = 1; i <= XmlCountChildren(op); i++) {
        XmlNode *table = XmlGetChildNode(op, i);
        if (strcasecmp(table->name, "table") == 0) {
            if (fbuf_used(fbuf))
                fbuf_add(fbuf, ",");
            fbuf_add(fbuf, table->value);
        } else {
            // TODO - Error Messages
        }
    }
    fbuf_printf(out, "%s ", op->name);
    fbuf_concat(out, fbuf);
    fbuf_clear(fbuf);

    set = XmlGetChildNodeByName(query, "set");
    if (set) {
        for (i = 1; i <= XmlCountChildren(set); i++) {
            XmlNode *field = XmlGetChildNode(set, i);
            if (strcasecmp(field->name, "field") == 0) {
                XmlNodeAttribute *name = XmlGetAttributeByName(field, "name");
                if (name) {
                    XmlNodeAttribute *noquote = XmlGetAttributeByName(field, "noquote");
                    int nq = 0;

                    if (fbuf_used(fbuf))
                        fbuf_add(fbuf, ",");

                    SQL_SKIP_QUOTING(noquote, nq);

                    if (nq) {
                        fbuf_printf(fbuf, "%s=%s", name->value, field->value);
                    } else {
                        char *value = NULL;
                        unsigned long len = 0;
                        byte_escape('\'', '\'', field->value, strlen(field->value), &value, &len);
                        if (value) {
                            fbuf_printf(fbuf, "%s='%s'", name->value, value);
                            free(value);
                        } else {
                            // TODO - Error Messages
                        }
                    }
                } else {
                    // TODO - Error Messages
                }
            } else {
                // TODO - Error Messages
            }
        }
        fbuf_printf(out, " set ");
        fbuf_concat(out, fbuf);
    }
    fbuf_free(fbuf);
    where = XmlGetChildNodeByName(query, "where");
    if (where)
        Xml2Sql_Where(query, where, out);
}

static void
Xml2Sql_Delete(XmlNode *query, XmlNode *op, fbuf_t *out)
{
    int i;
    XmlNode *where = NULL;
    fbuf_t *fbuf = fbuf_create(FBUF_MAXLEN_NONE);

    for (i = 1; i <= XmlCountChildren(op); i++) {
        XmlNode *table = XmlGetChildNode(op, i);
        if (strcasecmp(table->name, "table") == 0) {
            if (fbuf_used(fbuf))
                fbuf_add(fbuf, ",");
            fbuf_add(fbuf, table->value);
        } else {
            // TODO - Error Messages
        }
    }
    fbuf_printf(out, "%s from %s", op->name, fbuf_data(fbuf));
    fbuf_free(fbuf);

    where = XmlGetChildNodeByName(query, "where");
    if (where)
        Xml2Sql_Where(query, where, out);
}

static void
Xml2Sql_Insert(XmlNode *query, XmlNode *op, fbuf_t *out)
{
    int i;
    fbuf_t *fbuf = fbuf_create(FBUF_MAXLEN_NONE);
    XmlNode *set = NULL;

    for (i = 1; i <= XmlCountChildren(op); i++) {
        XmlNode *table = XmlGetChildNode(op, i);
        if (strcasecmp(table->name, "table") == 0) {
            if (fbuf_used(fbuf))
                fbuf_add(fbuf, ",");
            fbuf_add(fbuf, table->value);
        } else {
            // TODO - Error Messages
        }
    }
    fbuf_printf(out, "%s into %s ", op->name, fbuf_data(fbuf));
    fbuf_clear(fbuf);

    set = XmlGetChildNodeByName(query, "set");
    if (set) {
        fbuf_t *values = fbuf_create(FBUF_MAXLEN_NONE);
        for (i = 1; i <= XmlCountChildren(set); i++) {
            XmlNode *field = XmlGetChildNode(set, i);
            if (strcasecmp(field->name, "field") == 0) {
                XmlNodeAttribute *name = XmlGetAttributeByName(field, "name");
                if (name) {
                    int nq = 0;
                    XmlNodeAttribute *noquote = XmlGetAttributeByName(field, "noquote");

                    if (fbuf_used(fbuf))
                        fbuf_add(fbuf, ",");

                    fbuf_printf(fbuf, "%s", name->value);

                    if (fbuf_used(values))
                        fbuf_add(values, ",");
                    
                    SQL_SKIP_QUOTING(noquote, nq);

                    if (nq) {
                        fbuf_add(out, field->value);
                    } else {
                        char *value = NULL;
                        unsigned long len = 0;
                        byte_escape('\'', '\'', field->value, strlen(field->value), &value, &len);
                        if (value) {
                            fbuf_printf(values, "'%s'", value);
                            free(value);
                        } else {
                            // TODO - Error Messages
                        }
                    }
                } else {
                    // TODO - Error Messages
                }
            } else {
                // TODO - Error Messages
            }
        }
        fbuf_printf(out, " (%s) values (%s)", fbuf_data(fbuf), fbuf_data(values));
        fbuf_free(values);
    }
    fbuf_free(fbuf);
}

char *
Xml2Sql(TXml *xml)
{
    fbuf_t *out = NULL;
    int i;

    for (i = 1; i <= XmlCountBranches(xml); i++) {
        XmlNode *query = XmlGetBranch(xml, i);
        if (!query) {
            // TODO - Error Messages
            return NULL;
        }
        if (strcasecmp(query->name, "sqlquery") == 0) {

            if (!out)
                out = fbuf_create(FBUF_MAXLEN_NONE);
            else
                fbuf_add(out, ";\n");

            XmlNode *op = XmlGetChildNode(query, 1);
            if (strcasecmp(op->name, "select") == 0) 
                Xml2Sql_Select(query, op, out);
            else if (strcasecmp(op->name, "update") == 0) 
                Xml2Sql_Update(query, op, out);
            else if (strcasecmp(op->name, "delete") == 0) 
                Xml2Sql_Delete(query, op, out);
            else if (strcasecmp(op->name, "insert") == 0) 
                Xml2Sql_Insert(query, op, out);
        }
    }
    if (out) {
        PushValue(alloc, out);
        return fbuf_data(out);
    }
    return NULL;
}

static char *SqlExpr(char *op, int count, va_list args)
{
    fbuf_t *query = fbuf_create(FBUF_MAXLEN_NONE);
        

    for (; count > 0; count--) {
        char *subexpr = va_arg(args, char *);
        if (subexpr) {
            if (fbuf_used(query))
                fbuf_printf(query, " %s ", op);
            fbuf_printf(query, "(%s)", subexpr);
        }
    }


    PushValue(alloc, query);
    return fbuf_data(query);
}

char *
SqlOr(int count, ...)
{
    va_list args;
    char *sql = NULL;

    va_start(args, count);

    sql = SqlExpr("OR", count, args);

    va_end(args);

    return sql;
}

char *
SqlAnd(int count, ...)
{
    va_list args;
    char *sql = NULL;

    va_start(args, count);

    sql = SqlExpr("AND", count, args);

    va_end(args);

    return sql;
}

static char *SqlCmp(char *op, char *field, char *value, int quote)
{
    va_list args;
    char *escaped = NULL;
    unsigned long len = 0;
    fbuf_t *query = fbuf_create(FBUF_MAXLEN_NONE);

    if (quote) {
        byte_escape('\'', '\'', value, strlen(value), &escaped, &len);

        if (escaped) {
            fbuf_printf(query, "%s %s '%s'", field, op, escaped);
            free(escaped);
        }
    } else {
        fbuf_printf(query, "%s %s %s", field, op, escaped);
    }

    va_end(args);
    PushValue(alloc, query);
    return fbuf_data(query);

}

char *
SqlEq(char *field, char *value, int quote)
{
    va_list args;
    char *sql = NULL;

    sql = SqlCmp("=", field, value, quote);

    return sql;
}

char *
SqlNeq(char *field, char *value, int quote)
{
    va_list args;
    char *sql = NULL;

    sql = SqlCmp("!=", field, value, quote);

    return sql;
}

char *
SqlLike(char *field, char *value)
{
    va_list args;
    char *sql = NULL;

    sql = SqlCmp("like", field, value, 1);

    return sql;
}

