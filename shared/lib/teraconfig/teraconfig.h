//
// C Interface: teraconfig
//
// Description: 
//
//
// Author: xant <xant@xant.net>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
// $Id: f3c6882e0779dada70d90dcba271d0f9db3922c9 $
//

#ifndef __TERACONFIG_H__
#define __TERACONFIG_H__

#include <stdio.h>
#include <xdbaccess.h>
#include <teralog.h>
#include <linklist.h>
#include <txml.h>

// client configuration
typedef struct __tagTERA_CLIENT_CFG {
	short	sType;					// db type
	char	szHost [32];			// db host
	char	szName [64];			// db name
	char	szUser [64];			// db user
	char	szPassword [64];		// db password
} TERA_CLIENT_CFG, *PTERA_CLIENT_CFG;

typedef struct __TConfigHandler
{
	TERA_CLIENT_CFG clientCfg;
	char *cid;
	LinkedList *cfgEntries;
	DBHandler *dbh;
	char *program;
	char *descr;
	char *targetId;
	char *operationId;
	unsigned long long dbId;
	TLogHandler *log;
	int debug;
} TConfigHandler;

int TCfgGetDBConfig(TConfigHandler *cfg);
char *TCfgGetValue(TConfigHandler *cfg,char *option);
void TCfgDestroy(TConfigHandler *config);
TConfigHandler *TCfgInit (char *pCfgFile, char *pClientId);
//************************************************************************
// int GetNextSv (char *pszCsvString, char sepchar, char *pszOutValue, int OutValueBufLen, char **ppszNext)
// 
// Get substring (in pszValue) from separated value string (str1,str2,str3,...)
// 
// Returns number of chars copied in the outbuffer, and pointer to next value if any in ppszNext
//************************************************************************
int TCfgGetNextSv (char *pszCsvString, char sepchar, char *pszOutValue, int OutValueBufLen, char **ppszNext);

#endif
