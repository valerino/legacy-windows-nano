/*
*  C Implementation: teraconfig
*
* Description: 
*
*
* Author: xant <xant@xant.net>, (C) 2006
*
* Copyright: See COPYING file that comes with this distribution
*
* $Id: 384a01b1bfac5e64a694c64edc1a95925125580a $
*/

#include <stdio.h>
#include "teraconfig.h"
#ifdef WIN32
#include <uxdefs.h>
#endif

int TCfgGetNextSv (char* pszCsvString, char sepchar, char* pszOutValue, int OutValueBufLen, char** ppszNext)
{
	char* pStr = NULL;
	char* pSep = NULL;
	unsigned long copylen = 0;

	// check params
	if (!pszCsvString || !pszOutValue || !OutValueBufLen || !ppszNext)
		goto __exit;
	*ppszNext = NULL;

	// check eol or null
	if (*pszCsvString == 0x0d || *pszCsvString == 0x0a || *pszCsvString == 0x0)
		goto __exit;

	// return the whole string in case no separation char found
	pSep = strchr (pszCsvString,sepchar);
	if (!pSep)
	{
		strncpy (pszOutValue,pszCsvString,OutValueBufLen);
		pStr = pszCsvString;
		copylen = (int)strlen (pszOutValue);
		*ppszNext = NULL;
		goto __exit;
	}
	
	pStr = pszCsvString;
	copylen = (unsigned long)(pSep - pszCsvString);
	memcpy (pszOutValue,pszCsvString,copylen);
	
	*(pszOutValue + copylen) = '\0';
	
	// return next value ptr
	pSep++;
	pStr = pSep;
	*ppszNext = pStr;

__exit:
	return copylen;
}

TConfigHandler *TCfgInit (char *pCfgFile, char *pClientId)
{
	TConfigHandler *cfg = NULL;
	TERA_CLIENT_CFG *pCfgStruct = NULL;
	TXml *xml = NULL;
	char *val;
	char *opt;
	XmlNode *cCfg = NULL;
	XmlNode *cNode = NULL;
	unsigned int i;
	int donotstore = 0;
	int donotlog = 0;
	int getfromstorepath = 0;

	xml = XmlCreateContext();
        xml->allowMultipleRootNodes = 1;
	
	// check param
	if (!pCfgFile || !pClientId)
		goto __exit;

	cfg = calloc(1,sizeof(TConfigHandler));
	if(!cfg)
		goto __exit;
	
	pCfgStruct = &cfg->clientCfg;
	
	// set client id
	cfg->cid = strdup(pClientId);

	// load configuration from file
	if(XmlParseFile(xml,pCfgFile) != XML_NOERR)
		goto __exit;

	// select dbinfo node
	cCfg = XmlGetNode (xml,"/tera/clientinfo"); 
	if (!cCfg)
		goto __exit;

	// get values
	for(i = 1; i <= XmlCountChildren(cCfg); i++)
	{
		cNode = XmlGetChildNode(cCfg,i);
		if (!cNode)
			break;
		
		opt = cNode->name;
		val = cNode->value;
		
		/* skip comments */
		if(cNode->type == XML_NODETYPE_COMMENT)
			continue;
			
		if (strcasecmp (opt,"name") == 0)
			strncpy (pCfgStruct->szName,val,sizeof (pCfgStruct->szName));
		else if (strcasecmp (opt,"type") == 0)
		{
			if (strcasecmp(val,"mssql") == 0)
				pCfgStruct->sType = MODE_MSSQL;
			else if (strcasecmp(val,"mysql") == 0)
				pCfgStruct->sType = MODE_MYSQL;
		}
		else if (strcasecmp (opt,"host") == 0)
			strncpy (pCfgStruct->szHost,val,sizeof (pCfgStruct->szHost));			
		else if (strcasecmp (opt,"username") == 0)
		{
			strncpy (pCfgStruct->szUser,val,sizeof (pCfgStruct->szUser));			
		}
		else if (strcasecmp (opt,"password") == 0)
		{
			strncpy (pCfgStruct->szPassword,val,sizeof (pCfgStruct->szPassword));			
		}
		else if(strcasecmp (opt,"debug") == 0)
		{
			if(strcasecmp(val,"1") == 0 || strcasecmp(val,"Y") == 0)
				cfg->debug = 1;
		}
		else if(strcasecmp (opt,"donotstoreondb") == 0)
		{
			if(strcasecmp(val,"1") == 0 || strcasecmp(val,"Y") == 0)
			{
				donotstore = 1;
				donotlog = 1;
			}
		}
		else if(strcasecmp (opt,"getfromstorepath") == 0)
		{
			if(strcasecmp(val,"1") == 0 || strcasecmp(val,"Y") == 0)
				getfromstorepath = 1;
		}
	}

	if (strlen (pCfgStruct->szUser) == 0 || strlen (pCfgStruct->szPassword) == 0 || 
		strlen (pCfgStruct->szName) == 0 || strlen (pCfgStruct->szHost) == 0)	
	{
		fprintf(stderr,"Error in XML file, aborting! \n");
		goto __exit;
	}

	cfg->dbh = DBInit(MODE_MYSQL,pCfgStruct->szHost,pCfgStruct->szName, pCfgStruct->szUser,pCfgStruct->szPassword);
	if(cfg->dbh)
	{
		
		// these 2 are mostly for debugging ......
		if (donotstore)
			cfg->dbh->donotstore = 1;
		if (getfromstorepath)
			cfg->dbh->getfromstorepath = 1;

		if(TCfgGetDBConfig(cfg) == 0)
		{
			cfg->log = TLogInit(cfg->program,cfg->cid,cfg->dbh,1,NULL,cfg->debug);
			if(!cfg->log) 
			{
				fprintf(stderr,"Can't create the log handler, aborting! \n");
				goto __exit;
			}
			
			// this is mostly for debugging too....
			if (donotlog)
				cfg->log->donotlog = 1;
			return cfg;
		}
		else
			fprintf(stderr,"Can't read configuration from database \n");
	}
	else
		fprintf(stderr,"Can't connect to database!!\n");
	
__exit:
	// release configuration
	if (xml)
		XmlDestroyContext(xml);
	if(cfg) 
		TCfgDestroy(cfg);
	return NULL;
}

int TCfgGetDBConfig(TConfigHandler *cfg)
{
	DBResult *res = NULL;
	LinkedList *dbFilter;
	LinkedList *cConfig;
	LinkedList *cRow;
	char *cfgId = NULL;
	TaggedValue *tv;
	TaggedValue *tv2;
	char *programId = NULL;
	DBHandler *dbh;
	unsigned long i;
	
	if(!cfg || !cfg->dbh || !cfg->cid)
		return -1;
	
	dbh = cfg->dbh;
	dbFilter = CreateList();
	PushTaggedValue(dbFilter,CreateTaggedValue("cid",cfg->cid,0));
	res = DBGetAllRecords(dbh,"Configurations",dbFilter,FILTER_AND,NULL);
	if(!res)
		goto _init_err;
	cConfig = DBFetchRow(res);
	if(!cConfig) 
		goto _init_err;

	/* get preliminary info */
	for(i=1; i<=ListLength(cConfig); i++) 
	{
		tv = PickTaggedValue(cConfig,i);
		if(strcmp(tv->tag,"program") == 0) 
			programId = strdup(tv->value);
		else if(strcmp(tv->tag,"descr") == 0) 
			cfg->descr = strdup(tv->value);
		else if(strcmp(tv->tag,"id") == 0) 
			cfgId = strdup(tv->value);
		else if(strcmp(tv->tag,"target") == 0) 
			cfg->targetId = strdup(tv->value);
		else if(strcmp(tv->tag,"operation") == 0) 
			cfg->operationId = strdup(tv->value);
	}
	if(!(cfg->descr && cfg->targetId && cfg->operationId && cfgId && programId))
		goto _init_err;
	DBFreeResult(res);
	
	/* get the program name */
	ClearList(dbFilter);
	PushTaggedValue(dbFilter,CreateTaggedValue("id",programId,0));
	res = DBGetAllRecords(dbh,"Programs",dbFilter,FILTER_AND,NULL);
	cConfig = DBFetchRow(res);
	if(!cConfig) 
		goto _init_err;
	tv = GetTaggedValue(cConfig,"name");
	if(!tv) goto _init_err;
	cfg->program = strdup(tv->value);
	free(programId);
	programId = NULL; /* set to NULL in case of jumps to _init_err */
	DBFreeResult(res);
	
	/* and finally fetch all configuration entries */
	if(cfgId) 
	{
		ClearList(dbFilter);
		sscanf(cfgId,"%llu",&cfg->dbId);
		PushTaggedValue(dbFilter,CreateTaggedValue("cfg",cfgId,0));
		res = DBGetAllRecords(dbh,"ConfigurationEntries",dbFilter,FILTER_AND,NULL);
		if(!res)
			goto _init_err;
		cfg->cfgEntries = CreateList();
		while(cRow = DBFetchRow(res))
		{
			tv = GetTaggedValue(cRow,"name");
			tv2 = GetTaggedValue(cRow,"value");
			if(tv && tv2)
				PushTaggedValue(cfg->cfgEntries,
					CreateTaggedValue(tv->value,tv2->value,0));
		}
		DBFreeResult(res);
		free(cfgId);
	}

	DestroyList(dbFilter);
	return 0;
	
_init_err:
	DestroyList(dbFilter);
	if(res) 
		DBFreeResult(res);
	if(cfgId) 
		free(cfgId);
	if(programId)
		free(programId);
	
	return -1;
}

char *TCfgGetValue(TConfigHandler *cfg,char *option)
{
	char *val = NULL;
	TaggedValue *tv;
	tv = GetTaggedValue(cfg->cfgEntries,option);
	if(tv)
		val = tv->value;
	return val;
}

void TCfgDestroy(TConfigHandler *cfg)
{
	if(cfg)
	{
		if(cfg->program)
			free(cfg->program);
		if(cfg->descr)
			free(cfg->descr);
		if(cfg->cfgEntries)
			DestroyList(cfg->cfgEntries);
		if(cfg->cid)
			free(cfg->cid);
		if(cfg->dbh)
			DBDestroy(cfg->dbh);
		if(cfg->log)
			TLogDestroy(cfg->log);
		free(cfg);
	}
}
