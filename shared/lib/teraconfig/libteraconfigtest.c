/*
 *  libteraconfigtest.c
 *  libtera
 *
 *  Created by xant on 1/21/06.
 *  Copyright 2006 T.E.4I. - All rights reserved.
 *
 * $Id: da09dfee1a7f5b12696f689cba299312aa61c04e $
 */

#include "libteraconfigtest.h"

void EntryHandler(void *entry,u_long idx,void *user)
{
	TaggedValue *cfgEntry = (TaggedValue *)entry;
	if(cfgEntry)
		printf("\t %lu - %s  =  %s\n",idx,cfgEntry->tag,(char *)cfgEntry->value);
	else
		printf("Null Entry at pos %lu \n",idx);
}


int main (int argc, char **argv)
{
	TConfigHandler *cfg;
	
	if(argc < 3) 
	{
		printf("Usage: %s <cfgfile> <cid>\n",argv[0]);
		exit(1);
	}
	
	cfg = TCfgInit(argv[1],argv[2]);
	if(!cfg)
	{
		printf("Can't obtain the configuration handler!! \n");
		exit(1);
	}
	printf("CfgHandler obtained! \n");
	printf("Tera Client Configuration: \n dbName: '%s' \n dbHost: '%s'\n"
		" dbUser: '%s' \n dbPass: '%s' \n cid:    '%s' \n\n",
		cfg->clientCfg.szName,cfg->clientCfg.szHost,cfg->clientCfg.szUser,
		cfg->clientCfg.szPassword,cfg->collectorId);
	
	printf("Configuration Name: %s \n",cfg->name);
	printf("Program: %s \n",cfg->program);
	printf("Configuration Entries: %lu \n",ListLength(cfg->cfgEntries));
	
	ForEachListValue(cfg->cfgEntries,EntryHandler,NULL);
	
	TCfgDestroy(cfg);
	exit(0);
}