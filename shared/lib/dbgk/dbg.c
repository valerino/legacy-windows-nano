/*
*	debugging function to be used with WDBG_OUT and DBG_OUT macros
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#if (defined (WIN32) || defined (WINCE)) && !defined (_KERNELMODE)
#include <winsock2.h>
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
#include <w32_defs.h>
#endif

/// windows kernelmode only
#if defined _KERNELMODE
#include <ntifs.h>
#endif // ifdef _KERNELMODE

#include "dbg.h"

/*
*	print debug string
*
*/
void dbg_out(char* format, ...)
{

#ifdef DBG
	char buf [1024] = {0};
	wchar_t wbuf [1024] = {0};
	va_list ap;
	size_t numchar = 0;

	va_start(ap, format);
	_vsnprintf(buf, sizeof (buf), format, ap);
	va_end(ap);

#if defined (WINCE) && defined (WIN32) && !defined (_KERNELMODE)
	mbstowcs(wbuf,buf,1024);
	OutputDebugString(wbuf);
#endif

#if defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
	OutputDebugStringA(buf);
#endif

#if defined (WIN32) && defined (_KERNELMODE)
#if (NTDDI_VERSION >= NTDDI_LONGHORN)
	KdPrintEx ((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL,buf));
#else
	KdPrint ((buf));
#endif /// #if (NTDDI_VERSION >= NTDDI_LONGHORN)
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
	fprintf (stderr,buf);
#endif

#endif
}

/*
*	print debug string (unicode)
*
*/
void wdbg_out(wchar_t* format, ...)
{
#ifdef DBG
	wchar_t buf [1024] = {0};
	char abuf [1024] = {0};
	va_list ap;

	va_start(ap, format);
	_vsnwprintf(buf, 1024,format, ap);
	va_end(ap);

#if defined (WINCE) && defined (WIN32)
	OutputDebugString(buf);
#endif

#if defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
	OutputDebugStringW(buf);
#endif

#if defined (WIN32) && defined (_KERNELMODE)
	wcstombs(abuf,buf,1024);
	KdPrintEx ((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL,abuf));
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
	wcstombs(abuf,buf,1024);
	fprintf (stderr,abuf);
#endif

#endif //#ifdef DBG
}
