/*
 *	sockets library, vista implementation using WSK
 *	-vx-
 */

#include <ntifs.h>
#include <wsk.h>
#include <stdio.h>
#include <stdarg.h>
#include <gensock.h>
#include <dbg.h>

#define POOL_TAG 'vkst'

/*
 *	globals
 *
 */

/// client-level callback table
WSK_CLIENT_DISPATCH clientdispatch = {
	MAKE_WSK_VERSION(1, 0),
	0,
	NULL
};

WSK_REGISTRATION wskreg;	/// wsk registration object
WSK_CLIENT_NPI ksocknpi;	/// our client NPI
WSK_PROVIDER_NPI providernpi;	/// provider NPI
KEVENT ksockinitialized;	/// signal initialization succesfull
NPAGED_LOOKASIDE_LIST ls_sock;	/// sockets ls
PAGED_LOOKASIDE_LIST ls_sockmem;	/// ls used by the sockprintf function

/*
 *	completion routine for sockets operations
 *
 */
NTSTATUS KSocketsComplete(PDEVICE_OBJECT DeviceObject, PIRP Irp, PVOID Context)
{
	ksocket* sock = (ksocket*)Context;

	/// set completion status
	sock->iosb.Status = Irp->IoStatus.Status;
	sock->iosb.Information = Irp->IoStatus.Information;

	/// free any associated mdl
	if (sock->mdl)
	{
		MmUnlockPages(sock->mdl);
		IoFreeMdl(sock->mdl);
		sock->mdl = NULL;
	}

	/// if the socket is closing, we free the irp too
	if (sock->closing)
	{
		IoFreeIrp(sock->irp);
		sock->irp = NULL;
	}

	/// signal event
	KeSetEvent(&sock->evt,IO_NETWORK_INCREMENT,FALSE);
	
	return STATUS_MORE_PROCESSING_REQUIRED;
}


/*
*	initialize kernel sockets
*
*/
NTSTATUS KSocketsInitialize ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	KeInitializeEvent(&ksockinitialized,NotificationEvent,FALSE);
	ExInitializeNPagedLookasideList (&ls_sock,NULL,NULL,0,sizeof (ksocket),'kcsk',0);
	ExInitializePagedLookasideList(&ls_sockmem, NULL, NULL, 0, SOCKET_MEM_SIZE, 'mcsk', 0);
	
	/// register with WSK
	ksocknpi.ClientContext = NULL;
	ksocknpi.Dispatch = &clientdispatch;
	Status = WskRegister(&ksocknpi, &wskreg);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// capture provider NPI
	Status = WskCaptureProviderNPI (&wskreg,WSK_INFINITE_WAIT,&providernpi);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// ok
	KeSetEvent(&ksockinitialized,IO_NO_INCREMENT,FALSE);

__exit:
	DBG_OUT (("KSocketInitialize status = %x\n",Status));
	return Status;
}

/*
*	create a socket object
*
*/
NTSTATUS KSocketsCreate(IN OUT ksocket** sock)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ksocket* s = NULL;
	PWSK_PROVIDER_CONNECTION_DISPATCH dispatch = NULL;
	SOCKADDR_IN bindaddr;

	if (!sock)
		return STATUS_INVALID_PARAMETER;
	*sock = NULL;

	if (!KeReadStateEvent(&ksockinitialized))
		goto __exit;
	
	/// allocate socket from our lookaside list
	s = ExAllocateFromNPagedLookasideList(&ls_sock);
	if (!s)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (s,0,sizeof (ksocket));

	/// allocate irp
	s->irp = IoAllocateIrp(1,FALSE);
	if (!s->irp)
	{
		ExFreeToNPagedLookasideList(&ls_sock,s);
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	
	/// set completion routine
	KeInitializeEvent(&s->evt,NotificationEvent,FALSE);
	IoSetCompletionRoutine(s->irp,KSocketsComplete,s,TRUE,TRUE,TRUE);

	/// create socket via wsk
	Status = providernpi.Dispatch->WskSocket (providernpi.Client,AF_INET,SOCK_STREAM,IPPROTO_TCP,WSK_FLAG_CONNECTION_SOCKET,
		NULL,NULL,NULL,NULL,NULL,s->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&s->evt, Executive, KernelMode, FALSE, NULL);
		Status = s->iosb.Status;
		if (!NT_SUCCESS(Status))
			goto __exit;
	}
	
	/// bind the socket to a local address
	s->s = (PWSK_SOCKET)s->iosb.Information;
	dispatch = (PWSK_PROVIDER_CONNECTION_DISPATCH)s->s->Dispatch;
	IoReuseIrp(s->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&s->evt);
	IoSetCompletionRoutine(s->irp,KSocketsComplete,s,TRUE,TRUE,TRUE);
	
	memset (&bindaddr,0,sizeof (SOCKADDR_IN));
	bindaddr.sin_family = AF_INET;
	Status = dispatch->WskBind(s->s,(PSOCKADDR)&bindaddr,0,s->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&s->evt, Executive, KernelMode, FALSE, NULL);
		Status = s->iosb.Status;
		if (!NT_SUCCESS(Status))
			goto __exit;
	}
	
	/// ok
	*sock = s;

__exit:
	if (!NT_SUCCESS (Status))
	{
		/// free socket and irp
		if (s)
		{
			if (s->irp)
				IoFreeIrp(s->irp);
			ExFreeToNPagedLookasideList(&ls_sock,s);
		}
	}	
	return Status;
}

/*
*	connect socket to address:port (both -NON- nbo)
*
*/
NTSTATUS KSocketsConnect(IN ksocket* sock, IN unsigned long Address, IN unsigned short Port)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	SOCKADDR_IN remoteaddr;
	PWSK_PROVIDER_CONNECTION_DISPATCH dispatch = NULL;
	
	if (!sock)
		return STATUS_INVALID_PARAMETER;
	if (sock->connected)
		return STATUS_SUCCESS;

	/// setup irp
	dispatch = (PWSK_PROVIDER_CONNECTION_DISPATCH)sock->s->Dispatch;
	IoReuseIrp(sock->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&sock->evt);
	IoSetCompletionRoutine(sock->irp,KSocketsComplete,sock,TRUE,TRUE,TRUE);

	/// connect socket
	memset (&remoteaddr,0,sizeof (SOCKADDR_IN));
	remoteaddr.sin_family = AF_INET;
	remoteaddr.sin_port = htons (Port);
	remoteaddr.sin_addr.s_addr = htonl (Address);
	Status = dispatch->WskConnect (sock->s,(PSOCKADDR)&remoteaddr,0,sock->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&sock->evt, Executive, KernelMode, FALSE, NULL);
		Status = sock->iosb.Status;
	}
	
	if (NT_SUCCESS (Status))
		sock->connected = TRUE;
	return Status;
}

/*
*	disconnect socket
*
*/
NTSTATUS KSocketsDisconnect(IN ksocket* sock)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PWSK_PROVIDER_CONNECTION_DISPATCH dispatch = NULL;

	if (!sock)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;

	/// setup irp
	dispatch = (PWSK_PROVIDER_CONNECTION_DISPATCH)sock->s->Dispatch;
	IoReuseIrp(sock->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&sock->evt);
	IoSetCompletionRoutine(sock->irp,KSocketsComplete,sock,TRUE,TRUE,TRUE);

	/// disconnect socket
	Status = dispatch->WskDisconnect (sock->s,NULL,0,sock->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&sock->evt, Executive, KernelMode, FALSE, NULL);
		Status = sock->iosb.Status;
	}

	if (NT_SUCCESS (Status))
		sock->connected = FALSE;
	return Status;
}

/*
*	sends a buffer thru socket
*
*/
NTSTATUS KSocketsSend(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesSent)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PWSK_PROVIDER_CONNECTION_DISPATCH dispatch = NULL;
	WSK_BUF buf;
	
	if (!sock || !Buffer)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (BytesSent)
		*BytesSent = 0;

	/// setup irp
	dispatch = (PWSK_PROVIDER_CONNECTION_DISPATCH)sock->s->Dispatch;
	IoReuseIrp(sock->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&sock->evt);
	IoSetCompletionRoutine(sock->irp,KSocketsComplete,sock,TRUE,TRUE,TRUE);

	/// setup buffer
	memset (&buf,0,sizeof (WSK_BUF));
	buf.Length = SizeBuffer;
	buf.Mdl = IoAllocateMdl(Buffer,SizeBuffer,FALSE,FALSE,NULL);
	if (!buf.Mdl)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	__try 
	{
		MmProbeAndLockPages (buf.Mdl, KernelMode, IoReadAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		Status = STATUS_UNSUCCESSFUL;
		IoFreeMdl(buf.Mdl);
		goto __exit;
	}
	sock->mdl = buf.Mdl;

	/// send data
	Status = dispatch->WskSend(sock->s, &buf, 0, sock->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&sock->evt, Executive, KernelMode, FALSE, NULL);
		Status = sock->iosb.Status;
		if (BytesSent)
			*BytesSent = (ULONG)sock->iosb.Information;
		if (sock->iosb.Information != SizeBuffer)
			Status = STATUS_CONNECTION_ABORTED;
	}

__exit:
	return Status;
}

/*
*	receive a buffer thru socket
*
*/
NTSTATUS KSocketsReceive(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesReceived)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PWSK_PROVIDER_CONNECTION_DISPATCH dispatch = NULL;
	WSK_BUF buf;

	if (!sock || !Buffer || !SizeBuffer)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (BytesReceived)
		*BytesReceived = 0;

	/// setup irp
	dispatch = (PWSK_PROVIDER_CONNECTION_DISPATCH)sock->s->Dispatch;
	IoReuseIrp(sock->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&sock->evt);
	IoSetCompletionRoutine(sock->irp,KSocketsComplete,sock,TRUE,TRUE,TRUE);

	/// setup buffer
	memset (&buf,0,sizeof (WSK_BUF));
	buf.Length = SizeBuffer;
	buf.Mdl = IoAllocateMdl(Buffer,SizeBuffer,FALSE,FALSE,NULL);
	if (!buf.Mdl)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	__try 
	{
		MmProbeAndLockPages (buf.Mdl, KernelMode, IoWriteAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		Status = STATUS_UNSUCCESSFUL;
		IoFreeMdl(buf.Mdl);
		goto __exit;
	}
	sock->mdl = buf.Mdl;

	/// receive data
	Status = dispatch->WskReceive(sock->s, &buf, 0, sock->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&sock->evt, Executive, KernelMode, FALSE, NULL);
		Status = sock->iosb.Status;
		if (BytesReceived)
			*BytesReceived = (ULONG)sock->iosb.Information;
	}

__exit:
	return Status;
}

/*
*	print a formatted string to socket. Max string length is SOCKET_MEM_SIZE
*
*/
NTSTATUS KSocketsPrintf (IN ksocket* sock, OPTIONAL OUT unsigned long* SentBytes, IN const char* format, ...)
{
	va_list ap = NULL;
	char* buf = NULL;
	ULONG len = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	if (!sock || !format)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	
	/// allocate memory
	buf = ExAllocateFromPagedLookasideList(&ls_sockmem);
	if (!buf)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (buf,0,SOCKET_MEM_SIZE);

	/// build line
	va_start(ap, format);
	_vsnprintf(buf, SOCKET_MEM_SIZE, format, ap);
	va_end(ap);
	len = strlen(buf);

	/// send
	Status = KSocketsSend(sock, buf, len, SentBytes);

	/// free buffer
	ExFreeToPagedLookasideList (&ls_sockmem,buf);

	return Status;
}

/*
*	read an ascii line (until EOL) from socket
*
*/
NTSTATUS KSocketsReadLn(IN ksocket* sock, IN char* buf, IN unsigned long buflen, OPTIONAL OUT unsigned long* ReceivedBytes)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UCHAR c = 0;
	ULONG i = 0;
	ULONG received = 0;

	/// check params
	if (!sock || !buf || !buflen)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (ReceivedBytes)
		*ReceivedBytes = 0;

	/// read line char by char, and stop at EOL
	memset (buf, 0, buflen);
	while (TRUE)
	{
		if (i == buflen)
			break;

		/// get char from socket
		Status = KSocketsReceive (sock,&c,1,&received);
		if (!NT_SUCCESS (Status) || received == 0)
			break;

		/// write char into buffer and advance
		*buf = c;
		buf++;
		i++;

		/// check for EOL
		if (c == '\n')
		{
			if (ReceivedBytes)
				*ReceivedBytes = i;
			break;
		}
	}

	/// treat 0 size received as error
	if (received == 0)
		Status = STATUS_NO_DATA_DETECTED;

	return Status;
}

/*
*	close a socket opened with KSocketsCreate
*
*/
NTSTATUS KSocketsClose(IN ksocket* sock)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PWSK_PROVIDER_BASIC_DISPATCH dispatch = NULL;

	if (!sock)
		return STATUS_UNSUCCESSFUL;

	/// setup irp
	dispatch = (PWSK_PROVIDER_BASIC_DISPATCH)sock->s->Dispatch;
	IoReuseIrp(sock->irp,STATUS_UNSUCCESSFUL);
	KeClearEvent(&sock->evt);
	IoSetCompletionRoutine(sock->irp,KSocketsComplete,sock,TRUE,TRUE,TRUE);

	/// close socket
	sock->closing = TRUE;
	Status = dispatch->WskCloseSocket (sock->s,sock->irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&sock->evt, Executive, KernelMode, FALSE, NULL);
		Status = sock->iosb.Status;
	}
	
	/// free socket
	ExFreeToNPagedLookasideList(&ls_sock,sock);
	return STATUS_SUCCESS;
}
