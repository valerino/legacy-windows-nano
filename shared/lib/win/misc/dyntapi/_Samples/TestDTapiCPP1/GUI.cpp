#include "stdafx.h"

RECT	g_rt,			//main window rect
		g_rcMenuBar;	//menubar rect

extern HINSTANCE		g_hInst;			// current instance
extern HWND				g_hWndMenuBar,		// menu bar handle
						g_hWnd,				// main window handle
						g_hWndTab,			// tab handle
						g_hWndLB;				// listbox handle
extern int				g_nTabPage;

int		sp = DRA::SCALEX(5), BUTW = DRA::SCALEX(70), BUTH = DRA::SCALEY(20),
		WndW, WndH, MenuW, MenuH;

struct GUIPage
{
	HWND wnds[20];
	int	ccount;
} GUIPgs[10];



BOOL CreateControls(HWND hWnd) 
{
	// Window size	
	GetClientRect(hWnd, &g_rt);
	WndW = g_rt.right-g_rt.left; 
	WndH = g_rt.bottom - g_rt.top;

	// Resize window to fit menu
    GetWindowRect(g_hWndMenuBar, &g_rcMenuBar);
	MenuW = g_rcMenuBar.right - g_rcMenuBar.left;
	MenuH = g_rcMenuBar.bottom - g_rcMenuBar.top;
    //MoveWindow(g_hWnd, g_rt.left, g_rt.top, WndW, WndH - MenuH , FALSE);
	
	// Create TABS
	TCITEM tie;
	g_hWndTab = CreateWindow( WC_TABCONTROL, TEXT(""),
        WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | TCS_BOTTOM | TCS_FLATBUTTONS ,
		g_rt.left, WndH - 2*MenuH , WndW, MenuH, hWnd, NULL, g_hInst, NULL );
	
	if (!g_hWndTab) return FALSE;
	tie.mask = TCIF_TEXT | TCIF_IMAGE; 
    tie.iImage = -1; 
	tie.pszText = TEXT("Hardware");
	TabCtrl_InsertItem(g_hWndTab, 0, &tie);

	tie.pszText = TEXT("Network");
	TabCtrl_InsertItem(g_hWndTab, 1, &tie);

	tie.pszText = TEXT("Calls");
	TabCtrl_InsertItem(g_hWndTab, 2, &tie);
		
	tie.pszText = TEXT("Data");
	TabCtrl_InsertItem(g_hWndTab, 3, &tie);
	
	g_hWndLB = CreateWindow(TEXT("listbox"),TEXT(""),WS_VISIBLE|WS_CHILD|WS_VSCROLL|WS_BORDER|LBS_NOTIFY ,
		g_rt.left + sp,g_rt.top + sp, WndW - 2*sp, WndH / 2, hWnd,(HMENU)900,g_hInst, NULL);
	SendMessage(g_hWndLB,LB_ADDSTRING,0,(LPARAM)TEXT("DynTAPI sample startup."));

	CreatePageControls(hWnd, g_nTabPage);

	return TRUE;
}

void DestroyControls()
{
	DestroyWindow(g_hWndLB);
	DestroyWindow(g_hWndTab);
	DestroyWindow(g_hWndMenuBar);
}

BOOL CreatePageControls(HWND hWnd, int nPage) 
{
	switch (nPage)
	{
		case 0: //Hardware
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Radio On"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Radio Off"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Check"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Mute On"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Mute Off"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Check Mute"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Info"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			break;
		case 1: //Network
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Register"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Unregister"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Check Reg."), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Operator"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Signal"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			break;
		case 2: //Calls
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Dial"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Answer"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Drop"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Hold"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Swap"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Unhold"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("DMTF"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Transfer"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("SPKPHONE"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			break;
		case 3: //Data
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Dial Data"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Answer"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Drop"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Send Data"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			/*
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Send Data"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Unhold"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*3 + 2*BUTW, WndH / 2 + 2*sp + sp + BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("DMTF"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );
			GUIPgs[nPage].wnds[GUIPgs[nPage].ccount++] = CreateWindow(TEXT("button"), TEXT("Transfer"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
				sp*2 + BUTW, WndH / 2 + 2*sp + 2*sp + 2*BUTH, BUTW, BUTH,
				hWnd, (HMENU)(1000*(1+nPage) + GUIPgs[nPage].ccount), g_hInst, NULL );*/
			break;
			default: break;
	}
	return 1;
}

void DestroyPageControls(int nPage)
{
	for (int i=0; i< GUIPgs[nPage].ccount;i++)
		DestroyWindow(GUIPgs[nPage].wnds[i]);
	GUIPgs[nPage].ccount = 0;
}

void LBMessage(TCHAR *szMessage,...)
{
	TCHAR szBuffer[1024]; //in this buffer we form the message
    const size_t NUMCHARS = sizeof(szBuffer) / sizeof(szBuffer[0]);
    const int LASTCHAR = NUMCHARS - 1;
    va_list pArgs;
    va_start(pArgs, szMessage);
    _vsntprintf(szBuffer, NUMCHARS - 1, szMessage, pArgs);
    va_end(pArgs);
	SendMessage(g_hWndLB,LB_ADDSTRING,0,(LPARAM)szBuffer);
	int ic = SendMessage(g_hWndLB,LB_GETCOUNT,0,0);
	SendMessage(g_hWndLB,LB_SETCURSEL,  (WPARAM)ic-1,0);
}