// TestDTapi.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TestDTapi.h"
#include <windows.h>
#include <commctrl.h>
#include "DynTapi.h"

#define SZRES(x) (x==ERROR_SUCCESS?TEXT("OK"):TEXT("ERROR"))

// Global Variables for GUI
HINSTANCE			g_hInst;			// current instance
HWND				g_hWndMenuBar,		// menu bar handle
					g_hWnd,				// main window handle
					g_hWndTab,			// tab handle
					g_hWndLB;				// listbox handle
TCHAR				g_szWindowClass[255]= TEXT("phonetest.wnd"),	// main window class
					g_szWindowTitle[255]= TEXT("DynTAPI Sample"),	// main window title
					g_szRVText1[255] = {0}, g_szRVText2[255] = {0}, g_szRVParam1[255] = {0}, g_szRVParam2[255] = {0}; //read value dialog
DWORD				res;
int					g_nTabPage = 0;

// Our phone callback func
void PhoneCallback(DWORD dwEvent,LPVOID lpvParam,DWORD cbParam)
{
	switch (dwEvent)
	{
		case NETWORK_EVENT_CALL_IN: 
			{
				TCHAR szNumber[255]={0};
				for (unsigned int i=0;i<cbParam;i++) szNumber[i] = (TCHAR)(((char *)lpvParam)[i]);
				LBMessage(TEXT("Event(CALLIN): [%s]"),szNumber);
			}
			break;
		case NETWORK_EVENT_CALL_OUT: 
			{
				BYTE *pMediaMode = (BYTE *)lpvParam;
				if (*pMediaMode == 1)
					LBMessage(TEXT("Event(VOICE CALLOUT)"));
				else if (*pMediaMode == 2)
					LBMessage(TEXT("Event(DATA CALLOUT)"));
				else
					LBMessage(TEXT("Event(UNKNOWN CALLOUT)"));
			}
			break;
		case NETWORK_EVENT_CALL_CONNECT: 
			{
				BYTE *pMediaMode = (BYTE *)lpvParam;
				if (*pMediaMode == 1)
					LBMessage(TEXT("Event(VOICE CALL CONNECTED)"));
				else if (*pMediaMode == 2)
					LBMessage(TEXT("Event(DATA CALL CONNECTED)"));
				else
					LBMessage(TEXT("Event(UNKNOWN CALL CONNECTED)"));
			}
			break;
		case NETWORK_EVENT_CALL_DISCONNECT: 
			LBMessage(TEXT("Event(CALL DISCONNECTED)"));
			break;
		case NETWORK_EVENT_CALL_REJECT: 
			LBMessage(TEXT("Event(CALL REJECTED)"));
			break;
		case NETWORK_EVENT_CALL_INFO: 
			{
				TCHAR szNumber[255]={0};
				for (unsigned int i=0;i<cbParam;i++) szNumber[i] = (TCHAR)(((char *)lpvParam)[i]);
				LBMessage(TEXT("Event(CALLINFO): [%s]"),szNumber);
			}
			break;
		case NETWORK_EVENT_CALL_BUSY: 
			LBMessage(TEXT("Event(CALL BUSY)"));
			break;
		case NETWORK_EVENT_RING: 
			LBMessage(TEXT("Event(CALL RING)"));
			break;
		case NETWORK_EVENT_DTMFDIGIT:
			{
				BYTE *pDigit = (BYTE *)lpvParam;
				LBMessage(TEXT("Event(CALL DTMF): [%d]"),*pDigit);
			}
			break;
		case HARDWARE_EVENT_RADIOSTATUS:
			LBMessage(TEXT("HWEvent(RADIOSTATUS): [%d]"), *((BYTE *)lpvParam));
		break;
		case HARDWARE_EVENT_REGISTERSTATUS:
			LBMessage(TEXT("HWEvent(REGISTERSTATUS): [%d]"), *((BYTE *)lpvParam));
			break;
		case NETWORK_EVENT_FAILED: 
			LBMessage(TEXT("Event(CALL EVENT FAILED)"));
			break;
		case DATA_EVENT_READ:
			{
				LBMessage(TEXT("Event(DATA READ): %d bytes"), cbParam);
				FILE *f = _tfopen(TEXT("dataread.dat"), TEXT("ab"));
				fwrite((BYTE *)lpvParam, cbParam,1, f);
				fclose(f);
			}
			break;
		default: 
			//_debug(L"ERROR: Unhandled event");
			break;
	}
}




// Forward declarations of main window proc
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
// Message handler for value reader box.
INT_PTR CALLBACK RVProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

// Application entry point
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)
{

	// Load the TAPI Library
	if (LoadDynTapiDll(TEXT("\\DynTapiDll.dll")) != ERROR_SUCCESS) {
		MessageBox(0, TEXT("Cannot load DynTapiDll.dll . Try to reinstall this sample."), TEXT(""), MB_ICONERROR);
		return 0;
	}
	DebugMode(1);
	// Init the Library network component
	if (NetworkInit(hInstance) != ERROR_SUCCESS) {
		UnloadDynTapiDll();
		MessageBox(0, TEXT("Error using the network, does this device have a phone module?"), TEXT(""), MB_ICONERROR);
		return 0;
	}

	// Set callback for events
	SetNetworkEventCallback((tpfnCallback)PhoneCallback);

	// Store instance handle in our global variable
	g_hInst = hInstance; 

	// Check previous instance
	HWND hWnd = FindWindow(g_szWindowClass, g_szWindowTitle);	
    if (hWnd) 
    {
		SetForegroundWindow((HWND)((ULONG) hWnd | 0x00000001));
        return 0;
    } 
	
	// Register main window class
	WNDCLASS wc = {0};
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTDTAPI));
	wc.hCursor       = 0;
	wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName  = 0;
	wc.lpszClassName = g_szWindowClass;
	if (!RegisterClass(&wc)) return 0;

	// Create main application window
	SHInitExtraControls();
	g_hWnd = CreateWindow(g_szWindowClass, g_szWindowTitle, WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
    if (!g_hWnd||!g_hWndMenuBar) return FALSE;

	if (!CreateControls(g_hWnd)) return FALSE;

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);
	
	// Message loop
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	NetworkDeinit();
	UnloadDynTapiDll();

	return (int) msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;

    static SHACTIVATEINFO s_sai;
	
    switch (message) 
    {
		case WM_COMMAND: 
		{
            wmId    = LOWORD(wParam); 
            wmEvent = HIWORD(wParam); 
			//
            // Parse the menu selections:
            switch (wmId)
            {
				case 900: //double tap on listbox item shows content in messagebox
					if (wmEvent==LBN_DBLCLK) {
						TCHAR szTmp[512] = {0};
						int ic = SendMessage(g_hWndLB, LB_GETCURSEL,0,0);
						SendMessage(g_hWndLB, LB_GETTEXT ,ic,(LPARAM)szTmp);
						MessageBox(hWnd,szTmp,TEXT("Details"),MB_ICONINFORMATION);
					}
				break;
				//page 0 : hardware
				case 1000: //radio on button
					{
						DWORD dwRes = SetRadioStatus(1);
						LBMessage(TEXT("CMD Radio On, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 1001: //radio off button
					{
						DWORD dwRes = SetRadioStatus(0);
						LBMessage(TEXT("CMD Radio Off, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 1002: //check button
					{
						DWORD dwStatus = 0;
						DWORD dwRes = GetRadioStatus(&dwStatus);
						LBMessage(TEXT("Info(Radio Status): %d, Fn_res:%s"),dwStatus, SZRES(dwRes));
					}
					break;
				case 1003: //mute on
					{
						DWORD dwRes = SetMuteStatus(1);
						LBMessage(TEXT("CMD Mute On, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 1004: //mute off button
					{
						DWORD dwRes = SetMuteStatus(0);
						LBMessage(TEXT("CMD Mute Off, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 1005: //check button
					{
						DWORD dwStatus = 0;
						DWORD dwRes = GetMuteStatus(&dwStatus);
						LBMessage(TEXT("Info(Mute Status): %d, Fn_res:%s"),dwStatus, SZRES(dwRes));
					}
					break;
				case 1006: //info button
					{
						DWORD dwRes = 0;
						TCHAR szResult[255] = {0};
						dwRes = GetGeneralInfo(GI_MANUFACTURER,szResult,255);
						LBMessage(TEXT("Info(MANUFACTURER): [%s], Fn_rest:%s"),szResult, SZRES(dwRes));
						dwRes = GetGeneralInfo(GI_MODEL,szResult,255);
						LBMessage(TEXT("Info(MODEL): [%s], Fn_rest:%s"),szResult, SZRES(dwRes));
						dwRes = GetGeneralInfo(GI_REVISION,szResult,255);
						LBMessage(TEXT("Info(REVISION): [%s], Fn_rest:%s"),szResult, SZRES(dwRes));
						dwRes = GetGeneralInfo(GI_SERIALNUMBER,szResult,255);
						LBMessage(TEXT("Info(IMEI): [%s], Fn_rest:%s"),szResult, SZRES(dwRes));
						dwRes = GetGeneralInfo(GI_SUBSNUMBER,szResult,255);
						LBMessage(TEXT("Info(SUBSNUMBER): [%s], Fn_rest:%s"),szResult, SZRES(dwRes));

					}
					break;
				//page 1 : network
				case 2000: //register button
					{
						DWORD dwRes = SetRegisterStatus(1);
						LBMessage(TEXT("CMD Register, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 2001: //unregister button
					{
						DWORD dwRes = SetRegisterStatus(0);
						LBMessage(TEXT("CMD Unregister, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 2002: //check registration
					{
						REGISTRATIONINFO dwStatus ;
						DWORD dwRes = GetRegisterStatus(&dwStatus);
						LBMessage(TEXT("Info(Reg Status): %d, Fn_res:%s"),dwStatus, SZRES(dwRes));
					}
					break;
				case 2003: //operator name button
					{
						TCHAR wszOPName[MAX_LENGTH_OPERATOR_LONG] = {0};
						DWORD dwRes = GetNetworkOperator(wszOPName);
						LBMessage(TEXT("Info(Operator): [%s], Fn_res:%s"),wszOPName, SZRES(dwRes));
					}
					break;
				case 2004: //signal button
					{
						DWORD dwSignal = 0;
						DWORD dwRes = GetNetworkSignal(&dwSignal);
						LBMessage(TEXT("Info(Signal): %d%%, Fn_res:%s"),dwSignal, SZRES(dwRes));
					}
					break;
				//page 2 : calls
				case 3000: //Dial button
					{
						//ask number
						_tcscpy(g_szRVText1,TEXT("Enter phone number:"));
						DialogBox(g_hInst, (LPCTSTR)IDD_DIALOG1, hWnd, RVProc);
						if (_tcslen(g_szRVParam1) > 0) { //user entered a number to dial 
							DWORD dwRes = NetworkDialNumber(g_szRVParam1);
							LBMessage(TEXT("CMD Call(%s), Fn_res:%s"),g_szRVParam1, SZRES(dwRes));
						}
					}
					break;
				case 3001: //Answer
					{
						DWORD dwRes = NetworkAnswerCall();
						LBMessage(TEXT("CMD Answer, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 3002: //Drop
					{
						DWORD dwRes = NetworkDropCall(NETWORK_FLAGS_DROP_ALL);
						LBMessage(TEXT("CMD Drop(All), Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 3003: //Hold
					{
						DWORD dwRes = NetworkHoldCall();
						LBMessage(TEXT("CMD Hold, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 3004: //Swap
					{
						DWORD dwRes = NetworkSwapCall();
						LBMessage(TEXT("CMD Swap, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 3005: //Unhold
					{
						DWORD dwRes = NetworkUnholdCall();
						LBMessage(TEXT("CMD Unhold, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 3006: //DTmF
					{
						//ask DTMF sequence
						_tcscpy(g_szRVText1,TEXT("Enter DTMF sequence:"));
						DialogBox(g_hInst, (LPCTSTR)IDD_DIALOG1, hWnd, RVProc);
						if (_tcslen(g_szRVParam1) > 0) { //user entered a DTMF sequence
							DWORD dwRes = NetworkTransmitDTMF(g_szRVParam1);
							LBMessage(TEXT("CMD DTMF(%s), Fn_res:%s"),g_szRVParam1, SZRES(dwRes));
						}
					}
					break;
				case 3007: //Transfer
					{
						//ask number
						_tcscpy(g_szRVText1,TEXT("Enter phone number:"));
						DialogBox(g_hInst, (LPCTSTR)IDD_DIALOG1, hWnd, RVProc);
						if (_tcslen(g_szRVParam1) > 0) { //user entered a number to dial 
							DWORD dwRes = NetworkTransferCall(g_szRVParam1);
							LBMessage(TEXT("CMD Transfer(%s), Fn_res:%s"),g_szRVParam1, SZRES(dwRes));
						}
					}
					break;
				case 3008: //Speakerphone toggle
					{
						DWORD dwRes = ToggleSpeakerphone();
						LBMessage(TEXT("CMD SPKPHONE(), Fn_res:%s"),SZRES(dwRes));
					}
					break;
				//page 3 : data
				case 4000: //Dial Data button
					{
						//ask number
						_tcscpy(g_szRVText1,TEXT("Enter phone(modem) number:"));
						DialogBox(g_hInst, (LPCTSTR)IDD_DIALOG1, hWnd, RVProc);
						if (_tcslen(g_szRVParam1) > 0) { //user entered a number to dial 
							DWORD dwRes = NetworkDialData(g_szRVParam1);
							LBMessage(TEXT("CMD CallData(%s), Fn_res:%s"),g_szRVParam1, SZRES(dwRes));
						}
					}
					break;
				case 4001: //Answer
					{
						DWORD dwRes = NetworkAnswerCall();
						LBMessage(TEXT("CMD Answer, Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 4002: //Drop
					{
						DWORD dwRes = NetworkDropCall(NETWORK_FLAGS_DROP_ALL);
						LBMessage(TEXT("CMD Drop(All), Fn_res:%s"), SZRES(dwRes));
					}
					break;
				case 4003: //Send Data
					{
						_tcscpy(g_szRVText1,TEXT("Enter data to send:"));
						DialogBox(g_hInst, (LPCTSTR)IDD_DIALOG1, hWnd, RVProc);
						if (_tcslen(g_szRVParam1) > 0) { //user entered data to send
							BYTE data[1024] = {0};
							DWORD dwSize = _tcslen(g_szRVParam1), dwSent = 0; 
							for (DWORD i = 0; i< dwSize;i++) data[i] = (BYTE)g_szRVParam1[i];

							DWORD dwRes = SendData(data, dwSize, &dwSent);
							LBMessage(TEXT("CMD SendData(%s), Sent:%d, Res:%d"),g_szRVParam1, dwSent,dwRes);

						}
					}
					break;
                case IDM_HELP_ABOUT:
                   // DialogBox(g_hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, About);
                    break;
                case IDM_OK:
                    SendMessage (hWnd, WM_CLOSE, 0, 0);				
                    break;
                default:
                    return DefWindowProc(hWnd, message, wParam, lParam);
            }
            break;
		}
		case WM_NOTIFY:
		{
			int idCtrl = (int)wParam;
			LPNMHDR pnmh = (LPNMHDR)lParam;
			if (pnmh->code ==  TCN_SELCHANGE)
			{
				DestroyPageControls(g_nTabPage);
				g_nTabPage = TabCtrl_GetCurSel(pnmh->hwndFrom);
				CreatePageControls(hWnd,g_nTabPage);
			}
		}
		break;
        case WM_CREATE:
            SHMENUBARINFO mbi;

            memset(&mbi, 0, sizeof(SHMENUBARINFO));
            mbi.cbSize     = sizeof(SHMENUBARINFO);
            mbi.hwndParent = hWnd;
            mbi.nToolBarId = IDR_MENU;
            mbi.hInstRes   = g_hInst;

            if (!SHCreateMenuBar(&mbi)) 
            {
                g_hWndMenuBar = NULL;
            }
            else
            {
                g_hWndMenuBar = mbi.hwndMB;
            }

            // Initialize the shell activate info structure
            memset(&s_sai, 0, sizeof (s_sai));
            s_sai.cbSize = sizeof (s_sai);
            break;
        case WM_PAINT:
            hdc = BeginPaint(hWnd, &ps);
            
            // TODO: Add any drawing code here...
            
            EndPaint(hWnd, &ps);
            break;
        case WM_DESTROY:
            DestroyControls();
            PostQuitMessage(0);
            break;

        case WM_ACTIVATE:
            // Notify shell of our activate message
            SHHandleWMActivate(hWnd, wParam, lParam, &s_sai, FALSE);
            break;
        case WM_SETTINGCHANGE:
            SHHandleWMSettingChange(hWnd, wParam, lParam, &s_sai);
            break;

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for value reader box.
INT_PTR CALLBACK RVProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_INITDIALOG:
            {
                // Create a Done button and size it.  
                SHINITDLGINFO shidi;
                shidi.dwMask = SHIDIM_FLAGS;
                shidi.dwFlags = SHIDIF_DONEBUTTON | SHIDIF_SIPDOWN | SHIDIF_SIZEDLGFULLSCREEN | SHIDIF_EMPTYMENU;
                shidi.hDlg = hDlg;
                SHInitDialog(&shidi);
				
				SetWindowText(hDlg, TEXT("Input"));
				RECT rt;GetClientRect(hDlg, &rt);
				CreateWindow(TEXT("edit"), g_szRVText1, WS_VISIBLE | WS_CHILD | ES_READONLY | ES_CENTER,
					DRA::SCALEX(5),0,rt.right-rt.left-DRA::SCALEX(10),DRA::SCALEY(20), hDlg, (HMENU)1000, g_hInst,0);
				HWND ed1 = CreateWindow(TEXT("edit"), TEXT(""), WS_VISIBLE | WS_CHILD  | WS_BORDER | ES_CENTER,
					DRA::SCALEX(5),DRA::SCALEY(20),rt.right-rt.left-DRA::SCALEX(10),DRA::SCALEY(20), hDlg, (HMENU)1001, g_hInst,0);
				SetFocus(ed1);
				CreateWindow(TEXT("edit"), g_szRVText2,  WS_CHILD  | ES_READONLY | ES_CENTER,
					DRA::SCALEX(5),DRA::SCALEY(40),rt.right-rt.left-DRA::SCALEX(10),DRA::SCALEY(20), hDlg, (HMENU)1002, g_hInst,0);
				HWND ed2 = CreateWindow(TEXT("edit"), TEXT(""),  WS_CHILD  | WS_BORDER | ES_CENTER,
					DRA::SCALEX(5),DRA::SCALEY(60),rt.right-rt.left-DRA::SCALEX(10),DRA::SCALEY(20), hDlg, (HMENU)1003, g_hInst,0);
				if (_tcslen(g_szRVText1) == 0) EnableWindow(ed1, 0);
				if (_tcslen(g_szRVText2) == 0) EnableWindow(ed2, 0);
            }
			
            return (INT_PTR)TRUE;

        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK)
            {
				GetWindowText(GetDlgItem(hDlg, 1001),g_szRVParam1,255);
				GetWindowText(GetDlgItem(hDlg, 1003),g_szRVParam2,255);
                EndDialog(hDlg, LOWORD(wParam));
                return TRUE;
            }
            break;

        case WM_CLOSE:
            EndDialog(hDlg, message);
            return TRUE;

    }
    return (INT_PTR)FALSE;
}