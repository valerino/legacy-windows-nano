#pragma once

BOOL CreateControls(HWND hWnd) ;

void DestroyControls();

BOOL CreatePageControls(HWND hWnd, int nPage) ;

void DestroyPageControls(int nPage);

void LBMessage(TCHAR *szMessage,...);