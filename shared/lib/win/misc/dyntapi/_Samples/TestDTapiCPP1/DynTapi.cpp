#include "stdafx.h"
#include "DynTapi.h"

//--------------------------------------------//
//  private section                            
//--------------------------------------------//
typedef DWORD	(*tpfnNetworkInit)(HINSTANCE hInstance);
typedef void	(*tpfnNetworkDeinit)(void);
typedef void	(*tpfnDebugMode)(bool state);
typedef void	(*tpfnSetNetworkEventCallback)(tpfnCallback pfn);
typedef DWORD	(*tpfnGetOSMajorVersion)(void);
typedef DWORD	(*tpfnToggleSpeakerphone)(void);
//-----
typedef DWORD	(*tpfnNetworkDialNumber)(LPWSTR pwszNumber);
typedef DWORD	(*tpfnNetworkDialData)(LPWSTR pwszNumber);
typedef DWORD	(*tpfnNetworkDropCall)(DWORD dwFlags);
typedef DWORD	(*tpfnNetworkAnswerCall)(void);
typedef DWORD	(*tpfnNetworkHoldCall)(void);
typedef DWORD	(*tpfnNetworkSwapCall)(void);
typedef DWORD	(*tpfnNetworkDropSwapCall)(void);
typedef DWORD	(*tpfnNetworkUnholdCall)(void);
typedef DWORD	(*tpfnNetworkTransmitDTMF)(LPWSTR pwszDTMF);
typedef DWORD	(*tpfnNetworkGetCallState)(PDWORD pdwFlags);
typedef DWORD	(*tpfnNetworkRedirectCall)(LPWSTR pwszNumber);
typedef DWORD	(*tpfnNetworkTransferCall)(LPWSTR pwszNumber);
typedef BOOL	(*tpfnSendData)(BYTE *lpData, DWORD cbSize, LPDWORD dwNumBytesWritten);
//-----
typedef DWORD	(*tpfnGetRadioStatus)(PDWORD pdwStatus);
typedef DWORD	(*tpfnSetRadioStatus)(DWORD dwStatus);
typedef DWORD	(*tpfnGetNetworkSignal)(PDWORD pdwSignal);
typedef DWORD	(*tpfnGetNetworkOperator)(TCHAR pwszOPName[MAX_LENGTH_OPERATOR_LONG]);
typedef DWORD	(*tpfnGetGeneralInfo)(GENERALINFO InfoType, TCHAR *pwszInfo, int ccInfo);
typedef DWORD	(*tpfnGetMuteStatus)(PDWORD pdwStatus);
typedef DWORD	(*tpfnSetMuteStatus)(DWORD dwStatus);
typedef DWORD	(*tpfnGetRegisterStatus)(REGISTRATIONINFO *pdwStatus);
typedef DWORD	(*tpfnSetRegisterStatus)(DWORD dwStatus);
//-----

tpfnNetworkInit			pfnNetworkInit = NULL;
tpfnNetworkDeinit		pfnNetworkDeinit = NULL;
tpfnDebugMode			pfnDebugMode = NULL;
tpfnSetNetworkEventCallback	pfnSetNetworkEventCallback = NULL;
tpfnGetOSMajorVersion	pfnGetOSMajorVersion = NULL;
tpfnToggleSpeakerphone	pfnToggleSpeakerphone = NULL;
//
tpfnNetworkDialNumber	pfnNetworkDialNumber = NULL;
tpfnNetworkDialData		pfnNetworkDialData = NULL;
tpfnNetworkDropCall		pfnNetworkDropCall = NULL;
tpfnNetworkAnswerCall	pfnNetworkAnswerCall = NULL;
tpfnNetworkHoldCall		pfnNetworkHoldCall = NULL;
tpfnNetworkSwapCall		pfnNetworkSwapCall = NULL;
tpfnNetworkDropSwapCall	pfnNetworkDropSwapCall = NULL;
tpfnNetworkUnholdCall	pfnNetworkUnholdCall = NULL;
tpfnNetworkTransmitDTMF	pfnNetworkTransmitDTMF = NULL;
tpfnNetworkGetCallState	pfnNetworkGetCallState = NULL;
tpfnNetworkRedirectCall pfnNetworkRedirectCall = NULL;
tpfnNetworkTransferCall pfnNetworkTransferCall = NULL;
tpfnSendData			pfnSendData = NULL;
//
tpfnGetRadioStatus		pfnGetRadioStatus = NULL;
tpfnSetRadioStatus		pfnSetRadioStatus = NULL;
tpfnGetNetworkSignal	pfnGetNetworkSignal = NULL;
tpfnGetNetworkOperator	pfnGetNetworkOperator = NULL;
tpfnGetGeneralInfo		pfnGetGeneralInfo = NULL;
tpfnGetMuteStatus		pfnGetMuteStatus = NULL;
tpfnSetMuteStatus		pfnSetMuteStatus = NULL;
tpfnGetRegisterStatus	pfnGetRegisterStatus = NULL;
tpfnSetRegisterStatus	pfnSetRegisterStatus = NULL;
//
HINSTANCE hDllInst = NULL;

//----------LIBRARY INTERFACE-----------------
// This would load the library DLL
DWORD LoadDynTapiDll(TCHAR *szPath)
{
	//load dll
	hDllInst = LoadLibrary(szPath);
	if (!hDllInst) return -1;
	//_debug(TEXT("LoadDynTapiDll(%s) hDllInst=%X"),szPath, hDllInst);
//----------LIBRARY INTERFACE-----------------
	//load fn procs
	pfnNetworkInit = (tpfnNetworkInit)GetProcAddress(hDllInst, TEXT("NetworkInit"));
	//_debug(TEXT("pfnNetworkInit=%X"), pfnNetworkInit);
	if (!pfnNetworkInit) goto exit;

	pfnNetworkDeinit = (tpfnNetworkDeinit)GetProcAddress(hDllInst, TEXT("NetworkDeinit"));
	//_debug(TEXT("pfnNetworkDeinit=%X"), pfnNetworkDeinit);
	if (!pfnNetworkDeinit) goto exit;
	
	pfnDebugMode = (tpfnDebugMode)GetProcAddress(hDllInst, TEXT("DebugMode"));
	//_debug(TEXT("pfnDebugMode=%X"), pfnDebugMode);
	if (!pfnDebugMode) goto exit;
	
	pfnSetNetworkEventCallback = (tpfnSetNetworkEventCallback)GetProcAddress(hDllInst, TEXT("SetNetworkEventCallback"));
	//_debug(TEXT("pfnSetNetworkEventCallback=%X"), pfnSetNetworkEventCallback);
	if (!pfnSetNetworkEventCallback) goto exit;

	pfnGetOSMajorVersion = (tpfnGetOSMajorVersion)GetProcAddress(hDllInst, TEXT("GetOSMajorVersion"));
	//_debug(TEXT("pfnGetOSMajorVersion=%X"), pfnGetOSMajorVersion);
	if (!pfnGetOSMajorVersion) goto exit;

	pfnToggleSpeakerphone = (tpfnToggleSpeakerphone)GetProcAddress(hDllInst, TEXT("ToggleSpeakerphone"));
	//_debug(TEXT("pfnToggleSpeakerphone=%X"), pfnToggleSpeakerphone);
	if (!pfnToggleSpeakerphone) goto exit;

//----------CALLS INTERFACE-----------------
	pfnNetworkDialNumber= (tpfnNetworkDialNumber)GetProcAddress(hDllInst, TEXT("NetworkDialNumber"));
	//_debug(TEXT("pfnNetworkDialNumber=%X"), pfnNetworkDialNumber);
	if (!pfnNetworkDialNumber) goto exit;

	pfnNetworkDialData= (tpfnNetworkDialData)GetProcAddress(hDllInst, TEXT("NetworkDialData"));
	//_debug(TEXT("pfnNetworkDialData=%X"), pfnNetworkDialData);
	if (!pfnNetworkDialData) goto exit;

	pfnNetworkDropCall = (tpfnNetworkDropCall)GetProcAddress(hDllInst, TEXT("NetworkDropCall"));
	//_debug(TEXT("pfnNetworkDropCall=%X"), pfnNetworkDropCall);
	if (!pfnNetworkDropCall) goto exit;
	
	pfnNetworkAnswerCall = (tpfnNetworkAnswerCall)GetProcAddress(hDllInst, TEXT("NetworkAnswerCall"));
	//_debug(TEXT("pfnNetworkAnswerCall=%X"), pfnNetworkAnswerCall);
	if (!pfnNetworkAnswerCall) goto exit;
	
	pfnNetworkHoldCall = (tpfnNetworkHoldCall)GetProcAddress(hDllInst, TEXT("NetworkHoldCall"));
	//_debug(TEXT("pfnNetworkHoldCall=%X"), pfnNetworkHoldCall);
	if (!pfnNetworkHoldCall) goto exit;
	
	pfnNetworkSwapCall = (tpfnNetworkSwapCall)GetProcAddress(hDllInst, TEXT("NetworkSwapCall"));
	//_debug(TEXT("pfnNetworkSwapCall=%X"), pfnNetworkSwapCall);
	if (!pfnNetworkSwapCall) goto exit;
	
	pfnNetworkDropSwapCall = (tpfnNetworkDropSwapCall)GetProcAddress(hDllInst, TEXT("NetworkDropSwapCall"));
	//_debug(TEXT("pfnNetworkDropSwapCall=%X"), pfnNetworkDropSwapCall);
	if (!pfnNetworkDropSwapCall) goto exit;
	
	pfnNetworkUnholdCall = (tpfnNetworkUnholdCall)GetProcAddress(hDllInst, TEXT("NetworkUnholdCall"));
	//_debug(TEXT("pfnNetworkUnholdCall=%X"), pfnNetworkUnholdCall);
	if (!pfnNetworkUnholdCall) goto exit;
	
	pfnNetworkTransmitDTMF = (tpfnNetworkTransmitDTMF)GetProcAddress(hDllInst, TEXT("NetworkTransmitDTMF"));
	//_debug(TEXT("pfnNetworkTransmitDTMF=%X"), pfnNetworkTransmitDTMF);
	if (!pfnNetworkTransmitDTMF) goto exit;
	
	pfnNetworkGetCallState = (tpfnNetworkGetCallState)GetProcAddress(hDllInst, TEXT("NetworkGetCallState"));
	//_debug(TEXT("pfnNetworkGetCallState=%X"), pfnNetworkGetCallState);
	if (!pfnNetworkGetCallState) goto exit;

	pfnNetworkRedirectCall = (tpfnNetworkRedirectCall)GetProcAddress(hDllInst, TEXT("NetworkRedirectCall"));
	//_debug(TEXT("pfnNetworkRedirectCall=%X"), pfnNetworkRedirectCall);
	if (!pfnNetworkRedirectCall) goto exit;

	pfnNetworkTransferCall = (tpfnNetworkTransferCall)GetProcAddress(hDllInst, TEXT("NetworkTransferCall"));
	//_debug(TEXT("pfnNetworkTransferCall=%X"), pfnNetworkTransferCall);
	if (!pfnNetworkTransferCall) goto exit;

	pfnSendData = (tpfnSendData)GetProcAddress(hDllInst, TEXT("SendData"));
	//_debug(TEXT("pfnSendData=%X"), pfnSendData);
	if (!pfnSendData) goto exit;


//----------HARDWARE/NETWORK INTERFACE-----------------
	pfnGetRadioStatus = (tpfnGetRadioStatus)GetProcAddress(hDllInst, TEXT("GetRadioStatus"));
	//_debug(TEXT("pfnGetRadioStatus=%X"), pfnGetRadioStatus);
	if (!pfnGetRadioStatus) goto exit;

	pfnSetRadioStatus = (tpfnSetRadioStatus)GetProcAddress(hDllInst, TEXT("SetRadioStatus"));
	//_debug(TEXT("pfnSetRadioStatus=%X"), pfnSetRadioStatus);
	if (!pfnSetRadioStatus) goto exit;

	pfnGetNetworkSignal = (tpfnGetNetworkSignal)GetProcAddress(hDllInst, TEXT("GetNetworkSignal"));
	//_debug(TEXT("pfnGetNetworkSignal=%X"), pfnGetNetworkSignal);
	if (!pfnGetNetworkSignal) goto exit;

	#ifdef USE_EXTAPI
	pfnGetNetworkOperator = (tpfnGetNetworkOperator)GetProcAddress(hDllInst, TEXT("GetNetworkOperator"));
	//_debug(TEXT("pfnGetNetworkOperator=%X"), pfnGetNetworkOperator);
	if (!pfnGetNetworkOperator) goto exit;

	pfnGetGeneralInfo = (tpfnGetGeneralInfo)GetProcAddress(hDllInst, TEXT("GetGeneralInfo"));
	//_debug(TEXT("pfnGetGeneralInfo=%X"), pfnGetGeneralInfo);
	if (!pfnGetGeneralInfo) goto exit;

	pfnGetMuteStatus = (tpfnGetMuteStatus)GetProcAddress(hDllInst, TEXT("GetMuteStatus"));
	//_debug(TEXT("pfnGetMuteStatus=%X"), pfnGetMuteStatus);
	if (!pfnGetMuteStatus) goto exit;

	pfnSetMuteStatus = (tpfnSetMuteStatus)GetProcAddress(hDllInst, TEXT("SetMuteStatus"));
	//_debug(TEXT("pfnSetMuteStatus=%X"), pfnSetMuteStatus);
	if (!pfnSetMuteStatus) goto exit;

	pfnGetRegisterStatus = (tpfnGetRegisterStatus)GetProcAddress(hDllInst, TEXT("GetRegisterStatus"));
	//_debug(TEXT("pfnGetRegisterStatus=%X"), pfnGetRegisterStatus);
	if (!pfnGetRegisterStatus) goto exit;

	pfnSetRegisterStatus = (tpfnSetRegisterStatus)GetProcAddress(hDllInst, TEXT("SetRegisterStatus"));
	//_debug(TEXT("pfnSetRegisterStatus=%X"), pfnSetRegisterStatus);
	if (!pfnSetRegisterStatus) goto exit;

	#endif
	
	//_debug(TEXT("LoadDynTapiDll all OK."));
	return ERROR_SUCCESS;
exit:
	//_debug(TEXT("LoadDynTapiDll Fatal error."));
	UnloadDynTapiDll();
	return -1;
}
void UnloadDynTapiDll()
{
	if (hDllInst) FreeLibrary(hDllInst);
	hDllInst = NULL;
}
// This function is called to initialize the Network Component.
DWORD NetworkInit(HINSTANCE hInstance)
{
	if (pfnNetworkInit) return pfnNetworkInit(hInstance); else return -1;
}
// This function deinitializes the Network Component.
void NetworkDeinit(void)
{
	if (pfnNetworkDeinit) pfnNetworkDeinit(); else return;
}
void DebugMode(bool state)
{
	if (pfnDebugMode) pfnDebugMode(state); else return;
}
void SetNetworkEventCallback(tpfnCallback pfn)
{
	if (pfnSetNetworkEventCallback) return pfnSetNetworkEventCallback(pfn); else return;
}
// This returns the OS Major version (eg. Pocket PC 2003SE -> 4, WinCE5.0 -> 5, etc)
DWORD GetOSMajorVersion()
{
	if (pfnGetOSMajorVersion) return pfnGetOSMajorVersion(); else return -1;
}
// This toggles speakerphone on/off while in a phone call
DWORD ToggleSpeakerphone()
{
	if (pfnToggleSpeakerphone) return pfnToggleSpeakerphone(); else return -1;
}

//----------CALLS INTERFACE-----------------
// This function dials a number through TAPI.
DWORD NetworkDialNumber(LPWSTR pwszNumber)
{
	if (pfnNetworkDialNumber) return pfnNetworkDialNumber(pwszNumber); else return -1;
}
// This function dials a number through TAPI, for a data connection
DWORD NetworkDialData(LPWSTR pwszNumber)
{
	if (pfnNetworkDialData) return pfnNetworkDialData(pwszNumber); else return -1;
}
// This function hangs up one or more calls in the specified states
DWORD NetworkDropCall(DWORD dwFlags)
{
	if (pfnNetworkDropCall) return pfnNetworkDropCall(dwFlags); else return -1;
}
// This function answers the current incoming call.
DWORD NetworkAnswerCall(void)
{
	if (pfnNetworkAnswerCall) return pfnNetworkAnswerCall(); else return -1;
}
// This function puts the active call on hold
DWORD NetworkHoldCall(void)
{
	if (pfnNetworkHoldCall) return pfnNetworkHoldCall(); else return -1;
}
// This function puts the active call on hold (if it exists) and puts another call (offering or held) in
// the active state.
DWORD NetworkSwapCall(void)
{
	if (pfnNetworkSwapCall) return pfnNetworkSwapCall(); else return -1;
}
DWORD NetworkDropSwapCall(void)
{
	if (pfnNetworkDropSwapCall) return pfnNetworkDropSwapCall(); else return -1;
}
DWORD NetworkUnholdCall(void)
{
	if (pfnNetworkUnholdCall) return pfnNetworkUnholdCall(); else return -1;
}
DWORD NetworkTransmitDTMF(LPWSTR pwszDTMF)
{
	if (pfnNetworkTransmitDTMF) return pfnNetworkTransmitDTMF(pwszDTMF); else return -1;
}
DWORD NetworkGetCallState(PDWORD pdwFlags)
{
	if (pfnNetworkGetCallState) return pfnNetworkGetCallState(pdwFlags); else return -1;
}
// This function can be used to redirect the offering call to the specified number
DWORD NetworkRedirectCall(LPWSTR pwszNumber)
{
	if (pfnNetworkRedirectCall) return pfnNetworkRedirectCall(pwszNumber); else return -1;
}
// This function can be used to transfer the active call to the specified number
DWORD NetworkTransferCall(LPWSTR pwszNumber)
{
	if (pfnNetworkTransferCall) return pfnNetworkTransferCall(pwszNumber); else return -1;
}
BOOL SendData(BYTE *lpData, DWORD cbSize, LPDWORD dwNumBytesWritten)
{
	if (pfnSendData) return pfnSendData(lpData, cbSize, dwNumBytesWritten); else return -1;
}

//----------HARDWARE/NETWORK INTERFACE-----------------
// The radio hardware status, is returned in pdwStatus, 1 radio is on, 0 for radio is off or unknown
DWORD GetRadioStatus(PDWORD pdwStatus)
{
	if (pfnGetRadioStatus) return pfnGetRadioStatus(pdwStatus); else return -1;
}
// Sets the radio hardware status, dwStatus must be 1 for radio on, 0 for radio is off
DWORD SetRadioStatus(DWORD dwStatus)
{
	if (pfnSetRadioStatus) return pfnSetRadioStatus(dwStatus); else return -1;
}
DWORD GetNetworkSignal(PDWORD pdwSignal)
{
	if (pfnGetNetworkSignal) return pfnGetNetworkSignal(pdwSignal); else return -1;
}
DWORD GetNetworkOperator(TCHAR pwszOPName[MAX_LENGTH_OPERATOR_LONG])
{
	if (pfnGetNetworkOperator) return pfnGetNetworkOperator(pwszOPName); else return -1;
}
// Returns Manufacturer,Model,Revision,IMEI or subscrieber number
DWORD GetGeneralInfo(GENERALINFO InfoType, TCHAR *pwszInfo, int ccInfo)
{
	if (pfnGetGeneralInfo) return pfnGetGeneralInfo(InfoType, pwszInfo, ccInfo); else return -1;
}
// Retrieves the mute state of the device
DWORD GetMuteStatus(PDWORD pdwStatus)
{
	if (pfnGetMuteStatus) return pfnGetMuteStatus(pdwStatus); else return -1;
}
// Sets the mute state of the device.
DWORD SetMuteStatus(DWORD dwStatus)
{
	if (pfnSetMuteStatus) return pfnSetMuteStatus(dwStatus); else return -1;
}
// Retrieves the network registration state
DWORD GetRegisterStatus(REGISTRATIONINFO *pdwStatus)
{
	if (pfnGetRegisterStatus) return pfnGetRegisterStatus(pdwStatus); else return -1;
}
// Sets the network registration state, dwStatus 1 registers to network, 0 unregisters from network
DWORD SetRegisterStatus(DWORD dwStatus)
{
	if (pfnSetRegisterStatus) return pfnSetRegisterStatus(dwStatus); else return -1;
}