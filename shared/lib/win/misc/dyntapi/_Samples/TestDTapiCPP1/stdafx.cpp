// stdafx.cpp : source file that includes just the standard includes
// TestDTapi.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

void _debug(TCHAR *szFormat, ...)
{
	TCHAR szBuffer[10240]; //in this buffer we form the message
    const size_t NUMCHARS = sizeof(szBuffer) / sizeof(szBuffer[0]);
    const int LASTCHAR = NUMCHARS - 1;
    va_list pArgs;
    va_start(pArgs, szFormat);
    _vsntprintf(szBuffer, NUMCHARS - 1, szFormat, pArgs);
    va_end(pArgs);
	SYSTEMTIME pSystemTime; 
	GetLocalTime( &pSystemTime ); 
	FILE *f = _tfopen(TEXT("\\tapitest.txt"),TEXT("a"));
	_ftprintf(f, TEXT("[%2d:%2d:%2d] %s\n"),
									pSystemTime.wHour,
									pSystemTime.wMinute,
									pSystemTime.wSecond,szBuffer);
	fclose(f);
}