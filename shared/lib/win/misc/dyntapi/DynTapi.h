#pragma once

#define USE_EXTAPI	//allows EX functions: GetNetworkOperator, GetGeneralInfo

#define MAX_LENGTH_OPERATOR_LONG        32 //defined in extapi.h

//--------------------------------------------//
//  public section                            
//--------------------------------------------//
// callback type for network/phone/call events;
typedef void (*tpfnCallback)(DWORD dwEvent,LPVOID lpvParam,DWORD cbParam); 

// basic phone/call events
#define NETWORK_EVENT_CALL_IN           0x00	//provided by tpfnCallback dwEvent for an incoming call
#define NETWORK_EVENT_CALL_OUT          0x01	//provided by tpfnCallback dwEvent for an outgoing call
#define NETWORK_EVENT_CALL_CONNECT      0x02	//provided by tpfnCallback dwEvent when a call is connected
#define NETWORK_EVENT_CALL_DISCONNECT   0x03	//provided by tpfnCallback dwEvent when a call is disconnected
#define NETWORK_EVENT_CALL_REJECT       0x04	//provided by tpfnCallback dwEvent when a call is rejected
#define NETWORK_EVENT_CALL_INFO         0x05	//provided by tpfnCallback dwEvent when a call info is available
#define NETWORK_EVENT_CALL_BUSY         0x06	//provided by tpfnCallback dwEvent when a call comes as busy
#define NETWORK_EVENT_RING              0x07	//provided by tpfnCallback dwEvent when the phone is ringing
#define NETWORK_EVENT_DTMFDIGIT			0x08	//provided by tpfnCallback dwEvent when a digit has been received
#define	HARDWARE_EVENT_RADIOSTATUS		0x09	// Radio State changed
#define HARDWARE_EVENT_REGISTERSTATUS	0x0A	// Network Registration changed
#define DATA_EVENT_READ					0x0D	// 1 byte received from data call
#define NETWORK_EVENT_FAILED            0xff	

// Network Drop Call Flags, for use with NetworkDropCall
#define NETWORK_FLAGS_DROP_ACTIVE		0x01
#define NETWORK_FLAGS_DROP_HOLD			0x02
#define NETWORK_FLAGS_DROP_OFFERING		0x04
#define NETWORK_FLAGS_DROP_OUTGOING		0x08
#define NETWORK_FLAGS_DROP_ALL			0x0f

// Call status Flags, for use with NetworkGetCallState
#define NETWORK_FLAGS_STATE_ACTIVE      0x01
#define NETWORK_FLAGS_STATE_HOLD        0x02
#define NETWORK_FLAGS_STATE_OFFERING    0x04
#define NETWORK_FLAGS_STATE_OUTGOING    0x08

// defined to retrieve data sent with NETWORK_EVENT_FAILED
typedef struct _NetworkCallFailedInfo 			// for NETWORK_EVENT_FAILED
{
	USHORT usCallType;							// Type of call (see NETWORK_CALL_TYPE enum
	DWORD dwStatus;								// Status of the call
} NetworkCallFailedInfo;

// defined for use with GetGeneralInfo
typedef enum _GENERALINFO {						// for GetGeneralInfo
    GI_MANUFACTURER = 0x00,						// manufacturer string
    GI_MODEL = 0x01,							// model string
    GI_REVISION = 0x02,							// revision string
    GI_SERIALNUMBER = 0x03,						// IMEI serial number
    GI_SUBSNUMBER = 0x04						// subscrieber number
} GENERALINFO;

// network registration info. Use with GetRegisterStatus, 
typedef enum _REGISTRATIONINFO {
	REGSTATUS_UNKNOWN = 0x0,					// Registration is unknown. 
	REGSTATUS_DENIED = 0x1,						// Registration denied. 
	REGSTATUS_UNREGISTERED = 0x02,				// Not registered. 
	REGSTATUS_ATTEMPTING = 0x3,					// Attempting to register. 
	REGSTATUS_HOME = 0x4,						// Registered at home. 
	REGSTATUS_ROAM = 0x5,						// Registered for roam. 
	REGSTATUS_DIGITAL = 0x6,					// Registered for digital service. 
	REGSTATUS_ANALOG = 0x7,						// Registered for analog service. 
} REGISTRATIONINFO;

//----------LIBRARY INTERFACE-----------------
// This would load the library DLL
DWORD LoadDynTapiDll(TCHAR *szPath);
// This functions unloads the library DLL
void UnloadDynTapiDll();
// This function is called to initialize the Network Component.
DWORD NetworkInit(HINSTANCE hInstance);
// This function deinitializes the Network Component.
void NetworkDeinit(void);
// This function enables or disables the log-to-file debug mode, \\dyntapidll.txt
void DebugMode(bool state);
// This functions configures the callback function ptr
void SetNetworkEventCallback(tpfnCallback pfn);
// This returns the OS Major version (eg. Pocket PC 2003SE -> 4, WinCE5.0 -> 5, etc)
DWORD GetOSMajorVersion();

//----------CALLS INTERFACE-----------------
// This function dials a number through TAPI.
DWORD NetworkDialNumber(LPWSTR pwszNumber);
// This function dials a number through TAPI, for a data connection
DWORD NetworkDialData(LPWSTR pwszNumber);
// This function hangs up one or more calls in the specified states
DWORD NetworkDropCall(DWORD dwFlags);
// This function answers the current incoming call.
DWORD NetworkAnswerCall(void);
// This function puts the active call on hold
DWORD NetworkHoldCall(void);
// This function puts the active call on hold (if it exists) and puts another call (offering or held) in the active state.
DWORD NetworkSwapCall(void);
// This function drops the held call (if it exists)
DWORD NetworkDropSwapCall(void);
// This function activates the held call (if it exists)
DWORD NetworkUnholdCall(void);
// This function can be used to send DTMF tones
DWORD NetworkTransmitDTMF(LPWSTR pwszDTMF);
// This function can be used to return the call status.
DWORD NetworkGetCallState(PDWORD pdwFlags);
// This function can be used to redirect the offering call to the specified number
DWORD NetworkRedirectCall(LPWSTR pwszNumber);
// This function can be used to transfer the active call to the specified number
DWORD NetworkTransferCall(LPWSTR pwszNumber);
// Sends data over a data call
BOOL SendData(BYTE *lpData, DWORD cbSize, LPDWORD dwNumBytesWritten);

//----------HARDWARE/NETWORK INTERFACE-----------------
// The radio hardware status, is returned in pdwStatus, 1 radio is on, 0 for radio is off or unknown
DWORD GetRadioStatus(PDWORD pdwStatus);
// Sets the radio hardware status, dwStatus must be 1 for radio on, 0 for radio is off
DWORD SetRadioStatus(DWORD dwStatus);
// The network signal strength is returned in pdwSignal, as a percent 0..100%
DWORD GetNetworkSignal(PDWORD pdwSignal);
// The long operator name the phone is connected to, is returned in pwszOPName
DWORD GetNetworkOperator(TCHAR pwszOPName[MAX_LENGTH_OPERATOR_LONG]);
// Returns Manufacturer,Model,Revision,IMEI or subscrieber number
DWORD GetGeneralInfo(GENERALINFO InfoType, TCHAR *pwszInfo, int ccInfo);
// Retrieves the mute state of the device.
DWORD GetMuteStatus(PDWORD pdwStatus);
// Sets the mute state of the device.
DWORD SetMuteStatus(DWORD dwStatus);
// Retrieves the network registration state
DWORD GetRegisterStatus(REGISTRATIONINFO *pdwStatus);
// Sets the network registration state, dwStatus 1 registers to network, 0 unregisters from network
DWORD SetRegisterStatus(DWORD dwStatus);
// This toggles speakerphone on/off while in a phone call
DWORD ToggleSpeakerphone();
