#ifndef __wzcwrap_h__
#define __wzcwrap_h__

#ifdef __cplusplus
extern "C"
{
#endif

/*
 *	wrappers to simplify work with wireless zero configuration api. all the functions accepting cardname as parameters will use the first available wireless card if cardname is NULL.
 *
 */

#include <pm.h>
#include <eap.h>
#include <eapol.h>
#include <wzcsapi.h>
#include <wifistructs.h>

/*
 *	get powerstate for device. devicename must be comprehensive of class name (classguid\\name)
 *
 */
int wzc_get_device_power (IN PWCHAR devicename, OUT CEDEVICE_POWER_STATE* state);

/*
*	set powerstate for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_set_device_power (IN PWCHAR devicename, IN CEDEVICE_POWER_STATE newstate, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate);

/*
*	set power ON for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_turn_device_on (IN PWCHAR devicename, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate);

/*
*	set power OFF for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_turn_device_off (IN PWCHAR devicename, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate);

/*
*	enable or disable wzc on specified network card
*
*/
int wzc_DoEnableDisableWzcSvc (OPTIONAL IN PWCHAR cardname, IN int enable);

/*
*	enable wzc on specified network card
*
*/
int wzc_EnableWzcSvc (OPTIONAL IN PWCHAR cardname);

/*
*	disable wzc on specified network card
*
*/
int wzc_DisableWzcSvc (OPTIONAL IN PWCHAR cardname);

/*
*	release wzc dll resources
*
*/
void wzc_finalize ();

/*
*	initialize wzc pointers from dll
*
*/
int wzc_initialize ();

/*
*	get the first available network card
*
*/
int wzc_GetFirstWirelessNetworkCard (OUT PWCHAR detectedcard, IN int detetedcardsize);

/*
*	force wzc to refresh (reconnect to preferred network)
*
*/
int wzc_DoRefreshWzc (OPTIONAL IN PWCHAR cardname);

/*
*	reset the preferred networks (wireless will be disconnected)
*
*/
int wzc_ResetPreferredList (OPTIONAL IN PWCHAR cardname);

/*
*	add ssid to preferred networks on the specified card, thus making wzc automagically connect to it (if its an open ap, or if the password is cached)
*
*/
int wzc_AddToPreferredNetworkList (OPTIONAL IN PWCHAR cardname, IN PWZC_WLAN_CONFIG wzcConfig, IN PWCHAR szSsidToConnect);

/*
*	query adapter and return an array of BSSIDInfo containing information about the visible ap. bssdinfoarray must be freed by the caller.
*
*/
int wzc_DoQuery (OPTIONAL IN PWCHAR cardname, OUT BSSIDInfo** bssidinfoarray, OUT int* count);

/*
*	tell wzc to connect the specified card to the specified open network
*
*/
int wzc_DoConnectToOpenNetwork (IN PWCHAR cardname, IN PWCHAR sssid);

#ifdef __cplusplus
}
#endif

#endif /// #ifndef __wzcwrap_h__
