/*
 *	wzc api wrapper
 *	-vx-
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <pm.h>
#include "wzcwrap.h"

/*
 *	globals
 *
 */
HMODULE wzcmod = NULL;

WCHAR* g_szAuthenticationMode[] =
{
	L"Ndis802_11AuthModeOpen",
	L"Ndis802_11AuthModeShared",
	L"Ndis802_11AuthModeAutoSwitch",
	L"Ndis802_11AuthModeWPA",
	L"Ndis802_11AuthModeWPAPSK",
	L"Ndis802_11AuthModeWPANone",
	L"Ndis802_11AuthModeWPA2",
	L"Ndis802_11AuthModeWPA2PSK"
};

WCHAR* g_szcPrivacyMode[] =
{
	TEXT("Ndis802_11WEPEnabled"),                                                         
	TEXT("Ndis802_11WEPDisabled"),
	TEXT("Ndis802_11WEPKeyAbsent"),
	TEXT("Ndis802_11WEPNotSupported"),
	TEXT("Ndis802_11Encryption2Enabled"),
	TEXT("Ndis802_11Encryption2KeyAbsent"),
	TEXT("Ndis802_11Encryption3Enabled"),
	TEXT("Ndis802_11Encryption3KeyAbsent")
};

typedef DWORD (__stdcall *tpWZCEnumInterfaces) (LPWSTR pSrvAddr, PINTFS_KEY_TABLE pIntfs);
typedef DWORD (__stdcall *tpWZCQueryInterface) (LPWSTR pSrvAddr, DWORD dwInFlags, PINTF_ENTRY pIntf, LPDWORD pdwOutFlags);
typedef DWORD (__stdcall *tpWZCSetInterface) (LPWSTR pSrvAddr, DWORD dwInFlags, PINTF_ENTRY pIntf, LPDWORD pdwOutFlags);
typedef VOID (__stdcall *tpWZCDeleteIntfObj) (PINTF_ENTRY pIntf);
typedef DWORD (__stdcall *tpWZCRefreshInterface) (LPWSTR pSrvAddr, DWORD dwInFlags, PINTF_ENTRY pIntf,LPDWORD pdwOutFlags);

tpWZCEnumInterfaces pWZCEnumInterfaces = NULL;
tpWZCQueryInterface pWZCQueryInterface = NULL;
tpWZCDeleteIntfObj pWZCDeleteIntfObj = NULL;
tpWZCSetInterface pWZCSetInterface = NULL;
tpWZCRefreshInterface pWZCRefreshInterface = NULL;

/*
*	set powerstate for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_set_device_power (IN PWCHAR devicename, IN CEDEVICE_POWER_STATE newstate, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate)
{
	CEDEVICE_POWER_STATE oldpowerstate = (CEDEVICE_POWER_STATE)-1;

	if (!devicename)
		return -1;

	/// get the previous powerstate and force poweron the device
	if (GetDevicePower(devicename, POWER_NAME | POWER_FORCE, &oldpowerstate) != ERROR_SUCCESS)
		return -1;
	if (SetDevicePower(devicename, POWER_NAME | POWER_FORCE, newstate) != ERROR_SUCCESS)
		return -1;
	if (oldstate)
		*oldstate = oldpowerstate;

	return 0;
}

/*
*	get powerstate for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_get_device_power (IN PWCHAR devicename, OUT CEDEVICE_POWER_STATE* state)
{
	CEDEVICE_POWER_STATE oldpowerstate = (CEDEVICE_POWER_STATE)-1;

	if (!devicename)
		return -1;

	/// get the previous powerstate and force poweron the device
	if (GetDevicePower(devicename, POWER_NAME | POWER_FORCE, &oldpowerstate) != ERROR_SUCCESS)
		return -1;
	
	*state = oldpowerstate;

	return 0;
}

/*
 *	set power ON for device. devicename must be comprehensive of class name (classguid\\name)
 *
 */
int wzc_turn_device_on (IN PWCHAR devicename, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate)
{
	return wzc_set_device_power(devicename,D0,oldstate);
}

/*
*	set power OFF for device. devicename must be comprehensive of class name (classguid\\name)
*
*/
int wzc_turn_device_off (IN PWCHAR devicename, OPTIONAL OUT CEDEVICE_POWER_STATE* oldstate)
{
	return wzc_set_device_power(devicename,D4,oldstate);
}

/*
 *	get the first available network card
 *
 */
int wzc_GetFirstWirelessNetworkCard (OUT PWCHAR detectedcard, IN int detetedcardsize)
{
	INTFS_KEY_TABLE IntfsTable = {0};
	IntfsTable.dwNumIntfs = 0;
	IntfsTable.pIntfs = NULL;

	if (!detetedcardsize || !detetedcardsize)
		return -1;

	/// enum interfaces
	DWORD dwStatus = pWZCEnumInterfaces(NULL, &IntfsTable);
	if(dwStatus != ERROR_SUCCESS)
		return -1;
	
	if(!IntfsTable.dwNumIntfs)
	{
		/// no network card
		return -1;
	}

	wcsncpy(detectedcard, IntfsTable.pIntfs[0].wszGuid, detetedcardsize);
	LocalFree(IntfsTable.pIntfs);
	return 0;
}

/*
 *	release wzc dll resources
 *
 */
void wzc_finalize ()
{
	if (wzcmod)
	{
		FreeLibrary(wzcmod);
		wzcmod = NULL;
	}
}

/*
 *	enable or disable wzc on specified network card
 *
 */
int wzc_DoEnableDisableWzcSvc (IN PWCHAR cardname, IN int enable)
{
	WCHAR szWiFiCard [MAX_PATH] = {0};

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	DWORD dwInFlags = 0;
	INTF_ENTRY Intf = {0};
	
	Intf.wszGuid = szWiFiCard;
	if(enable)
		Intf.dwCtlFlags |= INTFCTL_ENABLED;
	else
		Intf.dwCtlFlags &= ~INTFCTL_ENABLED;

	DWORD dwStatus = pWZCSetInterface(NULL, INTF_ENABLED, &Intf, &dwInFlags);
	if(dwStatus != ERROR_SUCCESS)
		return -1;
	
	return 0;
}

/*
 *	enable wzc on specified network card
 *
 */
int wzc_EnableWzcSvc (IN PWCHAR cardname)
{
	return wzc_DoEnableDisableWzcSvc(cardname, TRUE);
}

/*
*	disable wzc on specified network card
*
*/
int wzc_DisableWzcSvc (IN PWCHAR cardname)
{
	return wzc_DoEnableDisableWzcSvc(cardname, FALSE);
}

/*
 *	force wzc to refresh (reconnect to preferred network)
 *
 */
int wzc_DoRefreshWzc (OPTIONAL IN PWCHAR cardname)
{
	WCHAR szWiFiCard [MAX_PATH] = {0};

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	INTF_ENTRY Intf = {0};
	Intf.wszGuid = szWiFiCard;
	
	DWORD dwOutFlags = 0;
	DWORD dwStatus = pWZCSetInterface(NULL, INTF_ALL, &Intf, &dwOutFlags);
	
	if (dwStatus != ERROR_SUCCESS)
		return -1;
	return 0;
}

/*
 *	reset the preferred networks (wireless will be disconnected)
 *
 */
int wzc_ResetPreferredList (OPTIONAL IN PWCHAR cardname)
{
	WCHAR szWiFiCard [MAX_PATH] = {0};

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	DWORD dwInFlags = 0;
	INTF_ENTRY Intf = {0};
	
	Intf.wszGuid = szWiFiCard;
	DWORD dwStatus = pWZCSetInterface(NULL, INTF_PREFLIST, &Intf, &dwInFlags);
	if(dwStatus != ERROR_SUCCESS)
		return -1;

	return 0;
}

/*
 *	add ssid to preferred networks on the specified card, thus making wzc automagically connect to it (if its an open ap, or if the password is cached)
 *
 */
int wzc_AddToPreferredNetworkList (OPTIONAL IN PWCHAR cardname, IN PWZC_WLAN_CONFIG wzcConfig, IN PWCHAR szSsidToConnect)
{
	DWORD dwOutFlags = 0;
	INTF_ENTRY Intf = {0};
	WCHAR szWiFiCard [MAX_PATH] = {0};
	
	if (!wzcConfig || !szSsidToConnect)
		return -1;

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	Intf.wszGuid = szWiFiCard;
	DWORD dwStatus = pWZCQueryInterface(NULL, INTF_ALL, &Intf, &dwOutFlags);
	if(dwStatus != ERROR_SUCCESS)
	{
		pWZCDeleteIntfObj(&Intf);
		return -1;
	}

	WZC_802_11_CONFIG_LIST *pConfigList = (PWZC_802_11_CONFIG_LIST)Intf.rdStSSIDList.pData;
	if(!pConfigList)   // empty [Preferred Networks] list case
	{
		DWORD dwDataLen = sizeof(WZC_802_11_CONFIG_LIST);
		WZC_802_11_CONFIG_LIST *pNewConfigList = (WZC_802_11_CONFIG_LIST *)LocalAlloc(LPTR, dwDataLen);
		if (!pNewConfigList)
		{
			pWZCDeleteIntfObj(&Intf);
			return -1;
		}
		pNewConfigList->NumberOfItems = 1;
		pNewConfigList->Index = 0;
		memcpy(pNewConfigList->Config, wzcConfig, sizeof(WZC_WLAN_CONFIG));
		Intf.rdStSSIDList.pData = (BYTE*)pNewConfigList;
		Intf.rdStSSIDList.dwDataLen = dwDataLen;
	}
	else
	{
		/// add to list in the first slot
		ULONG uiNumberOfItems = pConfigList->NumberOfItems;
		for(UINT i=0; i<uiNumberOfItems; i++)
		{
			if(memcmp(&wzcConfig->Ssid, &pConfigList->Config[i].Ssid, sizeof(NDIS_802_11_SSID)) == 0)
			{
				/// already in the preferred networks list
				pWZCDeleteIntfObj(&Intf);
				return 0;
			}
		}
	
		DWORD dwDataLen = sizeof(WZC_802_11_CONFIG_LIST) + (uiNumberOfItems+1)*sizeof(WZC_WLAN_CONFIG);
		WZC_802_11_CONFIG_LIST *pNewConfigList = (WZC_802_11_CONFIG_LIST *)LocalAlloc(LPTR, dwDataLen);
		if (!pNewConfigList)
		{
			pWZCDeleteIntfObj(&Intf);
			return -1;
		}

		pNewConfigList->NumberOfItems = uiNumberOfItems + 1;
		pNewConfigList->Index = 0;

		memcpy(pNewConfigList->Config, wzcConfig, sizeof(WZC_WLAN_CONFIG));
		if(pConfigList->NumberOfItems)
		{
			pNewConfigList->Index = pConfigList->Index;
			memcpy(pNewConfigList->Config+1, pConfigList->Config, (uiNumberOfItems)*sizeof(WZC_WLAN_CONFIG));
			LocalFree(pConfigList);
			pConfigList = NULL;
		}

		Intf.rdStSSIDList.pData = (BYTE*)pNewConfigList;
		Intf.rdStSSIDList.dwDataLen = dwDataLen;
	}

	dwStatus = pWZCSetInterface(NULL, INTF_PREFLIST, &Intf, &dwOutFlags);
	if(dwStatus != ERROR_SUCCESS)
	{
		pWZCDeleteIntfObj(&Intf);
		return -1;
	}

	pWZCDeleteIntfObj(&Intf);
	return 0;
}   

/*
 *	internal routine for querying accespoints informations
 *
 */
int wzc_DoQueryInternal (IN PRAW_DATA prdBSSIDList, OUT BSSIDInfo** bssidinfoarray, OUT int* count)
{
	if (!prdBSSIDList || !bssidinfoarray || !count)
		return -1;
	
	*count = 0;
	*bssidinfoarray = NULL;
	if (prdBSSIDList->dwDataLen == 0)
		return -1;

	PWZC_802_11_CONFIG_LIST pConfigList = (PWZC_802_11_CONFIG_LIST)prdBSSIDList->pData;
	if (!pConfigList->NumberOfItems)
	{
		/// no available aps
		return 0;
	}
	
	/// allocate enough structures
	BSSIDInfo* bssdi = NULL;
	BSSIDInfo* entry = NULL;
	WCHAR tmpbuf [64] = {0};

	bssdi = (BSSIDInfo*)calloc (1,pConfigList->NumberOfItems * sizeof (BSSIDInfo));
	if (!bssdi)
		return -1;
	entry = bssdi;
	*count = pConfigList->NumberOfItems;
	*bssidinfoarray = bssdi;

	/// fill structures
	for (unsigned long i = 0; i < pConfigList->NumberOfItems; i++)
	{
		PWZC_WLAN_CONFIG pConfig = &(pConfigList->Config[i]);
		
		/// bssid (mac address)
		swprintf(tmpbuf,L"%02X:%02X:%02X:%02X:%02X:%02X", pConfig->MacAddress[0], pConfig->MacAddress[1],
				pConfig->MacAddress[2], pConfig->MacAddress[3], pConfig->MacAddress[4], pConfig->MacAddress[5]);
		wcsncpy((PWCHAR)&entry->BSSID,tmpbuf,32);		
		
		/// ssid
		memset (tmpbuf,0,64*sizeof (WCHAR));
		if (pConfig->Ssid.Ssid && pConfig->Ssid.SsidLength)
		{
			for (UINT q = 0; q < pConfig->Ssid.SsidLength; q++)
				tmpbuf[q] = (WCHAR)pConfig->Ssid.Ssid[q];
		}
		wcsncpy((PWCHAR)&entry->SSID,tmpbuf,64);		

		/// signal strength
		entry->strength = pConfig->Rssi;
		
		/// channel
		ULONG ulFrequency_MHz = pConfig->Configuration.DSConfig;
		if((2412<=ulFrequency_MHz) && (ulFrequency_MHz<2484))
			entry->channel = ((ulFrequency_MHz-2412)/5)+1;
		else if(ulFrequency_MHz==2484)
			entry->channel = 14;
		
		/// infrastructure mode
		entry->infrastructure = pConfig->InfrastructureMode;
		
		/// authentication mode
		entry->auth = pConfig->AuthenticationMode;
		
		/// next entry
		entry++;
	}
	return 0;
}

/*
 *	query adapter and return an array of BSSIDInfo containing information about the visible ap. bssdinfoarray must be freed by the caller.
 *
 */
int wzc_DoQuery (OPTIONAL IN PWCHAR cardname, OUT BSSIDInfo** bssidinfoarray, OUT int* count)
{
	WCHAR szWiFiCard [MAX_PATH] = {0};
	BSSIDInfo* bssdi = NULL;
	int numap = 0;

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	INTF_ENTRY Intf = {0};
	DWORD dwOutFlags = 0;
	
	Intf.wszGuid = szWiFiCard;
	DWORD dwStatus = pWZCQueryInterface(NULL, INTF_ALL, &Intf, &dwOutFlags);
	if (dwStatus != ERROR_SUCCESS)
		return -1;

	/// call internal routine
	if (wzc_DoQueryInternal (&Intf.rdBSSIDList,&bssdi,&numap) != 0)
	{
		pWZCDeleteIntfObj(&Intf);
		return -1;
	}
	if (numap == 0)
	{
		pWZCDeleteIntfObj(&Intf);
		*bssidinfoarray = NULL;
		*count = 0;
		return 0;
	}

	/// ok
	pWZCDeleteIntfObj(&Intf);
	*bssidinfoarray = bssdi;
	*count = numap;
	return 0;
}

/*
 *	tell wzc to connect the specified card to the specified open network
 *
 */
int wzc_DoConnectToOpenNetwork (IN PWCHAR cardname, IN PWCHAR sssid)
{
	WCHAR szWiFiCard [MAX_PATH] = {0};

	if (!sssid)
		return -1;

	if (!cardname)
	{
		/// get first available card
		if (wzc_GetFirstWirelessNetworkCard(szWiFiCard,MAX_PATH) != 0)
			return -1;
	}
	else
	{
		wcsncpy(szWiFiCard,cardname,MAX_PATH);
	}

	WZC_WLAN_CONFIG wzcConfig1 = {0};
	wzcConfig1.Length = sizeof(WZC_WLAN_CONFIG);
	wzcConfig1.dwCtlFlags = 0;

	// SSID
	wzcConfig1.Ssid.SsidLength = wcslen(sssid);
	for(UINT i=0; i<wzcConfig1.Ssid.SsidLength; i++)
		wzcConfig1.Ssid.Ssid[i] = (char)sssid[i];

	// set infrastructure mode
	wzcConfig1.InfrastructureMode = Ndis802_11Infrastructure;
	
	// set no authentication
	wzcConfig1.AuthenticationMode = Ndis802_11AuthModeOpen;

	// set no encryption
	wzcConfig1.Privacy = Ndis802_11WEPDisabled;
	
	/// done, add to preferred network list resulting in connection
	return wzc_AddToPreferredNetworkList(szWiFiCard, &wzcConfig1, sssid);
}

/*
 *	initialize wzc pointers from dll
 *
 */
int wzc_initialize ()
{
	int res = -1;

	/// load module
	wzcmod = LoadLibrary (L"wzcsapi.dll");
	if (!wzcmod)
		return -1;

	/// get function pointers
	pWZCDeleteIntfObj = (tpWZCDeleteIntfObj)GetProcAddress(wzcmod,L"WZCDeleteIntfObj");
	if (!pWZCDeleteIntfObj)
		goto __exit;
	
	pWZCEnumInterfaces = (tpWZCEnumInterfaces)GetProcAddress (wzcmod, L"WZCEnumInterfaces");
	if (!pWZCEnumInterfaces)
		goto __exit;

	pWZCSetInterface = (tpWZCSetInterface)GetProcAddress (wzcmod, L"WZCSetInterface");
	if (!pWZCSetInterface)
		goto __exit;
	
	pWZCQueryInterface = (tpWZCQueryInterface)GetProcAddress (wzcmod, L"WZCQueryInterface");
	if (!pWZCQueryInterface)
		goto __exit;

	pWZCRefreshInterface = (tpWZCRefreshInterface)GetProcAddress (wzcmod, L"WZCRefreshInterface");
	if (!pWZCRefreshInterface)
		goto __exit;

	res = 0;

__exit:
	if (res != 0)
	{
		FreeLibrary(wzcmod);
		wzcmod = NULL;
	}

	return res;
}