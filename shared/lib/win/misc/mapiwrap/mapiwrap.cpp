/*
 *	mapi wrapper (tested on windows mobile only, probably needs some (few) adaptions for plain win32)
 *	-vx-
 */

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include "mapiwrap.h"

/*
*	returns a list with of mapi_folder_entry from a message store. list must be initialized prior to calling with initializelisthead, 
*   childfolder and childstable are reserved and must be NULL. list must then be freed calling mapi_free_folders
*
*/
int mapi_get_folders (IN OUT LIST_ENTRY* list, IN IMsgStore* msgstore, OPTIONAL IN IMAPIFolder* childfolder, OPTIONAL IN IMAPITable* childstable)
{
	LPSPropValue rgStoreProps = NULL;
	ULONG cStoreValues = 0;
	IMAPIFolder* pRootFolder = NULL;
	IMAPIFolder* child = NULL;
	SizedSPropTagArray(2, rgStoreTags) = {2,{PR_IPM_SUBTREE_ENTRYID, PR_OBJECT_TYPE}};
	SizedSPropTagArray(2, tblColumns) = {2,{PR_DISPLAY_NAME, PR_ENTRYID}};
	HRESULT longres = 0;
	IMAPITable* foldertable = NULL;
	IMAPITable* childfolderstable = NULL;
	SRowSet* row = NULL;
	int res = -1;
	mapi_folder_entry* folderentry = NULL;

	if (!list || !msgstore)
		return -1;
	
	if (!childstable && !childfolder)
	{
		/// get the root folder
		longres = msgstore->GetProps((LPSPropTagArray)&rgStoreTags, MAPI_UNICODE, &cStoreValues, &rgStoreProps);
		if(FAILED(longres))
			return -1;
		longres = msgstore->OpenEntry(rgStoreProps[0].Value.bin.cb, (LPENTRYID)rgStoreProps[0].Value.bin.lpb, NULL, MAPI_MODIFY, NULL, (LPUNKNOWN*)&pRootFolder);
		if(FAILED(longres))
			goto __exit;

		/// get the hierarchy table
		longres = pRootFolder->GetHierarchyTable(0, &foldertable);
		if(FAILED(longres))
			goto __exit;
	}
	else
	{
		/// only in recursion
		foldertable = childstable;
		pRootFolder = childfolder;
	}

	/// store rows in list
	while(TRUE)
	{
		foldertable->SetColumns((LPSPropTagArray)&tblColumns, 0);
		longres = foldertable->QueryRows(1, 0, &row);
		if ((longres != S_OK) || (row == NULL) || (row->cRows == 0))
			break;
		
		/// allocate
		folderentry = (mapi_folder_entry*)calloc (1,sizeof (mapi_folder_entry));
		if (folderentry)
		{
			folderentry->row = row;
			InsertTailList (list,&folderentry->chain);
		}
		
		/// get subfolders and its hierarchy table
		longres = pRootFolder->OpenEntry(row[0].aRow->lpProps[1].Value.bin.cb, (LPENTRYID)row[0].aRow->lpProps[1].Value.bin.lpb, NULL, MAPI_MODIFY, NULL, (LPUNKNOWN*)&child);
		if (child && longres == S_OK)
		{
			longres = child->GetHierarchyTable(0,&childfolderstable);
			if (longres == S_OK && childfolderstable)
			{
				/// recurse
				mapi_get_folders(list,msgstore,child,childfolderstable);
				childfolderstable->Release();
				childfolderstable = NULL;
			}
			child->Release();
			child = NULL;
		}
	}

	/// ok
	res = 0;

__exit:
	if (rgStoreProps)
		MAPIFreeBuffer(rgStoreProps);
	if (pRootFolder)
		pRootFolder->Release();
	if (foldertable)
		foldertable->Release();
	return res;
}

/*
*	free list allocated with mapi_get_folders
*
*/
void mapi_free_folders (IN LIST_ENTRY* list)
{
	mapi_folder_entry* p = NULL;

	if (!list)
		return;

	while (!IsListEmpty(list))
	{
		p = (mapi_folder_entry*)RemoveHeadList (list);
		if (p)
		{
			if (p->row)
			{
				FreeProws(p->row);
			}
			free(p);
		}
	}
}


/*
*	get mapi table by tag (sorted from newer to older). table must be released by the caller. if getinbox is specified, tag is ignored
*
*/
int mapi_get_table_by_tag (OUT IMAPITable** table, IN IMsgStore* msgstore, IN SPropTagArray* propertiestoretrieve, OPTIONAL IN ULONG tag, OPTIONAL IN int getinbox)
{
	int res = -1;
	SPropValue* propvalue = NULL;
	SizedSPropTagArray (1,foldertag) = {1,tag};
	SizedSSortOrderSet(1, sortorder) = { 1, 0, 0, { PR_MESSAGE_DELIVERY_TIME, TABLE_SORT_DESCEND } };
	ULONG values = 0;
	HRESULT longres = 0;
	IMAPIFolder* imapifolder = NULL;
	ULONG objtype = 0;
	IMAPITable* imapitable = NULL;
	ENTRYID* entryid = NULL;

	if (!table || !msgstore || !propertiestoretrieve)
		return -1;
	*table = NULL;

	/// get folder
	if (!getinbox)
	{
		longres = msgstore->GetProps((LPSPropTagArray)&foldertag,MAPI_UNICODE,&values,&propvalue);
		if (longres != S_OK)
			goto __exit;
		longres = msgstore->OpenEntry(propvalue->Value.bin.cb, (LPENTRYID)propvalue->Value.bin.lpb, NULL, 0, &objtype, (LPUNKNOWN*)&imapifolder);
		if (longres != S_OK)
			goto __exit;
	}
	else
	{
		/// inbox		
		longres = msgstore->GetReceiveFolder(NULL, MAPI_UNICODE, &values, &entryid, NULL);
		if (longres != S_OK)
			goto __exit;
		longres = msgstore->OpenEntry(values, entryid, NULL, 0, &objtype, (LPUNKNOWN*)&imapifolder);
		if (longres != S_OK)
			goto __exit;
	}

	/// get table
	longres = imapifolder->GetContentsTable(0, &imapitable);
	if (longres != S_OK)
		goto __exit;
	longres = imapitable->SortTable((SSortOrderSet*)&sortorder, 0);
	if (longres != S_OK)
		goto __exit;
	longres = imapitable->SetColumns (propertiestoretrieve, 0);
	if (longres != S_OK)
		goto __exit;

	/// ok
	*table = imapitable;
	res = 0;

__exit:
	if (imapifolder)
		imapifolder->Release();
	if (entryid)
		MAPIFreeBuffer(entryid);
	if (propvalue)
		MAPIFreeBuffer(propvalue);
	if (res != 0)
	{
		if (imapitable)
			imapitable->Release();
	}
	return res;
}

/*
*	get mapi table by entryid (sorted from newer to older). table must be released by the caller.
*
*/
int mapi_get_table_by_entryid (OUT IMAPITable** table, IN IMsgStore* msgstore, IN SPropTagArray* propertiestoretrieve, IN ULONG entryidsize, IN ENTRYID* entryid)
{
	int res = -1;
	SizedSSortOrderSet(1, sortorder) = { 1, 0, 0, { PR_MESSAGE_DELIVERY_TIME, TABLE_SORT_DESCEND } };
	ULONG values = 0;
	HRESULT longres = 0;
	IMAPIFolder* imapifolder = NULL;
	ULONG objtype = 0;
	IMAPITable* imapitable = NULL;

	if (!table || !msgstore || !propertiestoretrieve || !entryid || !entryidsize)
		return -1;
	*table = NULL;

	/// get folder
	longres = msgstore->OpenEntry(entryidsize, entryid, NULL, 0, &objtype, (LPUNKNOWN*)&imapifolder);
	if (longres != S_OK)
		goto __exit;

	/// get table
	longres = imapifolder->GetContentsTable(0, &imapitable);
	if (longres != S_OK)
		goto __exit;
	longres = imapitable->SortTable((SSortOrderSet*)&sortorder, 0);
	if (longres != S_OK)
		goto __exit;
	longres = imapitable->SetColumns (propertiestoretrieve, 0);
	if (longres != S_OK)
		goto __exit;

	/// ok
	*table = imapitable;
	res = 0;

__exit:
	if (imapifolder)
		imapifolder->Release();
	if (res != 0)
	{
		if (imapitable)
			imapitable->Release();
	}
	return res;
}

/*
*	get message store by name. store must be released by the caller
*
*/
int mapi_getmessagestore_by_name (IN  ICEMAPISession* session, IN PWCHAR name, OUT IMsgStore** store)
{
	int res = -1;
	SizedSPropTagArray (2, propstag) = { 2, PR_DISPLAY_NAME, PR_ENTRYID };
	SRowSet* rowset = NULL;
	HRESULT longres = 0;
	IMAPITable* mapitable = NULL;
	SPropValue* propvalue = NULL;
	IMsgStore* msgstore = NULL;

	if (!session || !name || !store)
		return -1;
	*store = NULL;

	/// get msgstore
	longres = session->GetMsgStoresTable(0,&mapitable);
	if (longres != S_OK)
		goto __exit;
	longres = mapitable->SetColumns((SPropTagArray *)&propstag, 0);
	if (longres != S_OK)
		goto __exit;
	while (TRUE)
	{
		FreeProws(rowset);
		rowset = NULL;

		longres = mapitable->QueryRows (1, 0, &rowset);
		if ((longres != S_OK) || (rowset == NULL) || (rowset->cRows == 0))
			break;
		propvalue = rowset->aRow[0].lpProps;

		/// check if it matches the name
		if (_wcsicmp(propvalue[0].Value.lpszW, name) == 0)
		{
			/// open msg store
			longres = session->OpenMsgStore(0, propvalue[1].Value.bin.cb, (LPENTRYID)propvalue[1].Value.bin.lpb, 0, 0, &msgstore);
			if (longres != S_OK)
				break;
			*store = msgstore;
			res = 0;
			break;
		}
	}

__exit:
	if (rowset)
		FreeProws(rowset);
	if (mapitable)
		mapitable->Release();
	return res;
}

/*
*	get messages stats. accountnames (wsz csv) must be freed by the caller. if sms is specified, only sms account is considered. email stats are for standard folders only
*
*/
int mapi_get_messages_stats (IN ICEMAPISession* session, OPTIONAL IN int sms, OPTIONAL OUT PWCHAR* accountnames, OPTIONAL OUT int* numaccounts, OUT int* numinbox, OUT int* numoutbox, OUT int* numsent, OUT int* numdrafts, OUT int* numdeleted)
{
	int res = -1;
	int longres = 0;
	IMsgStore* msgstore = NULL;
	IMAPITable* msgtable = NULL;
	PWCHAR namesarray [64] = {0};
	PWCHAR namescsv = NULL;
	SizedSPropTagArray (1, props) = { 1, PR_ENTRYID};
	int i = 0;
	int numfolders = 0;
	unsigned long inbox = 0;
	unsigned long outbox = 0;
	unsigned long sent = 0;
	unsigned long drafts = 0;
	unsigned long deleted = 0;
	unsigned long totalinbox = 0;
	unsigned long totaloutbox = 0;
	unsigned long totalsent = 0;
	unsigned long totaldrafts = 0;
	unsigned long totaldeleted = 0;
	int size = 0;

	if (!session || !numinbox || !numoutbox || !numsent || !numdrafts || !numdeleted)
		return -1;
	if (accountnames)
		*accountnames = NULL;
	if (numaccounts)
		*numaccounts = 0;
	*numinbox = 0;
	*numoutbox = 0;
	*numsent = 0;
	*numdrafts = 0;
	*numdeleted = 0;

	/// allocate char array for names
	for (i = 0; i < 64; i ++)
	{
		namesarray[i] = (PWCHAR)calloc (1,128);
		if (namesarray[i] == NULL)
			goto __exit;
	}

	/// get all account names
	if (mapi_getmessagestore_names(session,namesarray, &numfolders) != 0)
		goto __exit;

	/// get names size
	for (i = 0; i < 64; i ++)
	{
		if (namesarray[i])
			size += (int)(wcslen (namesarray[i]) * sizeof(WCHAR)) + 32;
	}

	if (accountnames)
	{
		/// allocate buffer for result
		namescsv = (PWCHAR)calloc (1,size);
		if (!namescsv)
			goto __exit;
	}

	/// get all messages from all accounts
	for (i=0; i < numfolders; i++)
	{
		if (sms)
		{
			if (_wcsicmp(namesarray[i],L"SMS") != 0)
				continue;
		}
		else
		{
			/// skip sms
			if (_wcsicmp(namesarray[i],L"SMS") == 0)
				continue;
		}

		if (accountnames)
		{
			/// copy name
			wcscat(namescsv,namesarray[i]);
			wcscat(namescsv,L",");
		}

		/// get msgstore for this account
		if (mapi_getmessagestore_by_name(session,namesarray[i],&msgstore) != 0)
			continue;

		/// sent messages
		if (mapi_get_table_by_tag(&msgtable,msgstore,(SPropTagArray*)&props,PR_IPM_SENTMAIL_ENTRYID,FALSE) == 0)
		{
			msgtable->GetRowCount(0,&sent);
			msgtable->Release();
			totalsent+=sent;
		}	

		/// outgoing messages		
		if (mapi_get_table_by_tag(&msgtable,msgstore,(SPropTagArray*)&props,PR_IPM_OUTBOX_ENTRYID,FALSE) == 0)
		{
			msgtable->GetRowCount(0,&outbox);
			msgtable->Release();
			totaloutbox+=outbox;
		}		

		/// incoming messages
		if (mapi_get_table_by_tag(&msgtable,msgstore,(SPropTagArray*)&props,0,TRUE) == 0)
		{
			msgtable->GetRowCount(0,&inbox);
			msgtable->Release();
			totalinbox+=inbox;
		}		

		/// draft messages
		if (mapi_get_table_by_tag(&msgtable,msgstore,(SPropTagArray*)&props,PR_CE_IPM_DRAFTS_ENTRYID,FALSE) == 0)
		{
			msgtable->GetRowCount(0,&drafts);
			msgtable->Release();
			totaldrafts+=drafts;
		}		

		/// deleted messages
		if (mapi_get_table_by_tag(&msgtable,msgstore,(SPropTagArray*)&props,PR_IPM_WASTEBASKET_ENTRYID,FALSE) == 0)
		{
			msgtable->GetRowCount(0,&deleted);
			msgtable->Release();
			totaldeleted+=deleted;
		}		

		/// release this msgstore
		msgstore->Release();
	}

	/// ok
	res = 0;
	*numinbox = (int)totalinbox;
	*numoutbox = (int)totaloutbox;
	*numdrafts = (int)totaldrafts;
	*numsent = (int)totalsent;
	*numdeleted = (int)totaldeleted;

	if (numaccounts)
	{
		*numaccounts = (int)numfolders;
	}
	if (accountnames)
	{
		namescsv[wcslen(namescsv) - 1] = (WCHAR)'\0';
		*accountnames = namescsv;
	}

__exit:
	for (i = 0; i < 64; i ++)
	{
		if (namesarray[i] != NULL)
			free (namesarray[i]);
	}

	return res;
}

/*
*	get message store names, returns an array of names (must be reasonably big to hold account names, each string must be at least 64 wchars)
*
*/
int mapi_getmessagestore_names (IN  ICEMAPISession* session, OUT PWCHAR* names, OUT int* namesarrayelements)
{
	int res = -1;
	SizedSPropTagArray (2, propstag) = { 2, PR_DISPLAY_NAME, PR_ENTRYID };
	SRowSet* rowset = NULL;
	HRESULT longres = 0;
	IMAPITable* mapitable = NULL;
	SPropValue* propvalue = NULL;
	IMsgStore* msgstore = NULL;
	int idx = 0;

	if (!session || !names || !namesarrayelements)
		return -1;
	*namesarrayelements = 0;

	/// get msgstore
	longres = session->GetMsgStoresTable(0,&mapitable);
	if (longres != S_OK)
		goto __exit;
	longres = mapitable->SetColumns((SPropTagArray *)&propstag, 0);
	if (longres != S_OK)
		goto __exit;
	while (TRUE)
	{
		FreeProws(rowset);
		rowset = NULL;

		longres = mapitable->QueryRows (1, 0, &rowset);
		if ((longres != S_OK) || (rowset == NULL) || (rowset->cRows == 0))
			break;
		propvalue = rowset->aRow[0].lpProps;

		/// copy the name
		if (propvalue[0].ulPropTag == PR_DISPLAY_NAME)
		{
			if (wcslen(propvalue[0].Value.lpszW) != 0)
			{
				wcsncpy(names[idx],propvalue[0].Value.lpszW, 64);		
				idx++;
			}
		}
	}

	// ok
	res = 0;

__exit:
	*namesarrayelements = idx;
	if (rowset)
		FreeProws(rowset);
	if (mapitable)
		mapitable->Release();
	return res;
}

/*
*	get recipients for message. output is an array of numrecipients recipients
*	
*/
int mapi_get_recipients (IN IMessage* message, OUT recipient** recipientsarray, OUT int* numrecipients)
{
	int res = -1;
	HRESULT longres = 0;
	IMAPITable* reciptable = NULL;
	SRowSet* rowset = NULL;
	SPropValue* propvalue = NULL;
	int i = 0;
	unsigned long count = 0;
	recipient* buffer = NULL;
	int k = 0;

	if (!message || !recipientsarray || !numrecipients)
		return -1;

	*recipientsarray = NULL;
	*numrecipients = 0;

	/// get table and count
	longres = message->GetRecipientTable(0,&reciptable);
	if (longres != S_OK)
		goto __exit;
	longres = reciptable->GetRowCount(0,&count);
	if (longres != S_OK)
		goto __exit;
	if (count == 0)
	{
		/// no recipients
		res = 0;
		goto __exit;
	}
	
	/// allocate a buffer big enough
	buffer = (recipient*)calloc (1,count * sizeof (recipient));
	if (!buffer)
		goto __exit;

	while (TRUE)
	{
		FreeProws(rowset);
		rowset = NULL;

		longres = reciptable->QueryRows (1, 0, &rowset);
		if ((longres != S_OK) || (rowset == NULL) || (rowset->cRows == 0))
			break;
		propvalue = rowset->aRow[0].lpProps;

		for (i=0;i < (int)rowset->aRow[0].cValues;i++)
		{
			/// name
			if (propvalue[i].ulPropTag == PR_DISPLAY_NAME)
			{
				wcsncpy ((PWCHAR)buffer[k].namenumber,propvalue[i].Value.lpszW,256);
			}

			/// email address
			if (propvalue[i].ulPropTag == PR_EMAIL_ADDRESS)
			{
				wcsncpy ((PWCHAR)buffer[k].emailnumber,propvalue[i].Value.lpszW,256);
			}
		}		
		k++;
	}

	/// ok
	*recipientsarray = buffer;
	*numrecipients = count;
	res = 0;

__exit:
	if (reciptable)
		reciptable->Release();
	return res;
}

/*
*	get attachments from imessage (if any). output is an array of attachment structures
*  
*  attachmentbuffer must be freed by the caller
*/
int mapi_get_attachments (IN IMessage* message, OUT attachment** attachmentsarray, OUT unsigned long* attachmentssize, OUT int* numattachments)
{
	SizedSPropTagArray(1,g_sptMsgProps) = {1,PR_HASATTACH};
	SizedSPropTagArray(2,sptCols) = {2,PR_ATTACH_FILENAME, PR_ATTACH_NUM};
	IMAPITable* pAttTbl = NULL;
	SRowSet* pRows = NULL;
	SPropValue* pProps = NULL;
	HRESULT hRes = 0;
	ULONG cVals = 0;
	int res = -1;
	attachment* buffer = NULL;
	attachment* newbuffer = NULL;
	unsigned long size = 0;
	unsigned long newsize = 0;
	unsigned long offset = 0;
	int num = 0;
	unsigned long i = 0;
	IAttach* lpAttach = NULL;
	IStream* pStrmSrc = NULL;
	STATSTG StatInfo = {0};

	if (!message || !attachmentsarray || !attachmentssize || !numattachments)
		return -1;

	*attachmentsarray = NULL;
	*attachmentssize = 0;
	*numattachments = 0;

	/// check if there's attachments
	hRes = message->GetProps((LPSPropTagArray) &g_sptMsgProps, 0, &cVals, &pProps);
	if (hRes != S_OK)
		goto __exit;
	if (PR_HASATTACH != pProps[0].ulPropTag)
		goto __exit;
	if (pProps->Value.b == 0)
	{
		/// no attachments
		goto __exit;
	}

	/// get attachments table
	hRes = message->GetAttachmentTable (0,&pAttTbl);
	if (hRes != S_OK)
		goto __exit;

	/// set attachment columns properties
	hRes = pAttTbl -> SetColumns((LPSPropTagArray)&sptCols,0);
	if (hRes != S_OK)
		goto __exit;

	/// query rows in attachment tables
	hRes = pAttTbl->QueryRows(1,0,&pRows);
	if (hRes != S_OK)
		goto __exit;

	/// browse all attachments found
	for (i = 0; i < pRows->cRows; i++)
	{
		/// Verify we received a filename from GetProps
		if (PR_ATTACH_FILENAME != pRows->aRow[i].lpProps[0].ulPropTag)
			goto __exit;

		/// Verify we received an Attachment Index from GetProps
		if (PR_ATTACH_NUM != pRows->aRow[i].lpProps[1].ulPropTag)
			goto __exit;

		/// Open the attachment
		hRes = message->OpenAttach(pRows->aRow[i].lpProps[1].Value.l, NULL, MAPI_BEST_ACCESS, &lpAttach);
		if (hRes != S_OK)
			goto __exit;

		/// Open the property of the attachment containing the file data
		hRes = lpAttach->OpenProperty(PR_ATTACH_DATA_BIN, (LPIID)&IID_IStream, 0, MAPI_MODIFY,(LPUNKNOWN *)&pStrmSrc);
		if (hRes != S_OK)
			goto __exit;

		/// get stream stats
		hRes = pStrmSrc->Stat(&StatInfo, STATFLAG_NONAME);
		if (hRes != S_OK)
			goto __exit;

		/// allocate buffer
		newsize = sizeof (attachment) + StatInfo.cbSize.LowPart;
		size += newsize;
		buffer = (attachment*)realloc ((attachment*)buffer, size + 1);
		if (!buffer)
			goto __exit;
		newbuffer = (attachment*)((unsigned char*)buffer + offset);
		memset ((unsigned char*)newbuffer, 0, newsize);

		/// build buffer
		wcsncpy ((PWCHAR)&newbuffer->name,pRows->aRow[i].lpProps[0].Value.lpszW,256);
		newbuffer->sizedata = htonl (StatInfo.cbSize.LowPart);
		hRes = pStrmSrc->Read((unsigned char*)&newbuffer->data,StatInfo.cbSize.LowPart,NULL);
		if (hRes != S_OK)
		{
			free (buffer);
			goto __exit;
		}

		/// release stream
		pStrmSrc->Release();
		pStrmSrc = NULL;

		// Release the attachment
		lpAttach->Release();
		lpAttach = NULL;

		/// inc offsets
		num++;
		offset += newsize;
	}

	/// ok
	FreeProws(pRows);
	pRows = NULL;

	/// return stuff
	*attachmentsarray = buffer;
	*numattachments = num;
	*attachmentssize = size;
	res = 0;

__exit:
	if (pRows)
		FreeProws(pRows);
	if (pStrmSrc)
		pStrmSrc->Release();
	if (lpAttach)
		lpAttach->Release();
	if (pProps)
		MAPIFreeBuffer(pProps);
	if (pAttTbl)
		pAttTbl->Release();
	return res;
}

/*
*	get message body. bodybuffer must be freed by the caller
*
*/
int mapi_get_body (IN IMessage* message, OUT unsigned char** bodybuffer, OUT unsigned long* bodysize)
{
	IStream* stream = NULL;
	STATSTG streamstat = {0};
	int res = -1;
	HRESULT longres = 0;
	unsigned long size = 0;
	unsigned char* buffer = NULL;

	if (!message || !bodybuffer || !bodysize)
		return -1;

	*bodybuffer = NULL;
	*bodysize = 0;

	/// check if there's a body
	longres = message->OpenProperty(PR_CE_MIME_TEXT,&IID_IStream, STGM_READ, 0, (IUnknown**)&stream); 
	if (longres != S_OK)
	{
		longres = message->OpenProperty(PR_BODY,&IID_IStream, STGM_READ, 0, (IUnknown**)&stream); 
		if (longres != S_OK)
		{
			longres = message->OpenProperty(PR_BODY_HTML,&IID_IStream, STGM_READ, 0, (IUnknown**)&stream); 
			if (longres != S_OK)
			{
				longres = message->OpenProperty(PR_BODY_HTML_A,&IID_IStream, STGM_READ, 0, (IUnknown**)&stream); 
				if (longres != S_OK)
				{
					/// tried everything .... fail!
					goto __exit;
				}
			}
		}
	}

	/// get size
	longres = stream->Stat(&streamstat,STATFLAG_NONAME|STATFLAG_NOOPEN);
	if (longres != S_OK)
		goto __exit;
	size = streamstat.cbSize.LowPart;

	/// allocate memory
	buffer = (unsigned char*)calloc (1, size + 32);
	if (!buffer)
		goto __exit;

	/// copy stream to buffer
	longres = stream->Read(buffer,size,NULL);
	if (longres != S_OK)
		goto __exit;

	/// ok
	*bodybuffer = buffer;
	*bodysize = size;
	res = 0;

__exit:
	if (stream)
		stream->Release();
	return res;
}
