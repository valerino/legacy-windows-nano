#ifndef __mapiwrap_h__
#define __mapiwrap_h__

#ifdef __cplusplus
extern "C"
{
#endif

#include <lists.h>

/// mapi
#include <mapix.h>
#include <cemapi.h>
#include <mapiutil.h>

#ifndef PR_BODY_HTML
#define PR_BODY_HTML PROP_TAG(PT_TSTRING,0x1013)
#endif

#ifndef PR_BODY_HTML_A
#define PR_BODY_HTML_A PROP_TAG(PT_STRING8,0x1013)
#endif

/*
*	get message store names, returns an array of names (must be reasonably big to hold account names, each string must be at least 64 wchars)
*
*/
int mapi_getmessagestore_names (IN  ICEMAPISession* session, OUT PWCHAR* names, OUT int* namesarrayelements);

/*
*	get mapi table by tag (sorted from newer to older). table must be released by the caller. if getinbox is specified, tag is ignored
*
*/
int mapi_get_table_by_tag (OUT IMAPITable** table, IN IMsgStore* msgstore, IN SPropTagArray* propertiestoretrieve, OPTIONAL IN ULONG tag, OPTIONAL IN int getinbox);

/*
*	get mapi table by entryid (sorted from newer to older). table must be released by the caller.
*
*/
int mapi_get_table_by_entryid (OUT IMAPITable** table, IN IMsgStore* msgstore, IN SPropTagArray* propertiestoretrieve, IN ULONG entryidsize, IN ENTRYID* entryid);

/*
*	get message store by name. store must be released by the caller
*
*/
int mapi_getmessagestore_by_name (IN ICEMAPISession* session, IN PWCHAR name, OUT IMsgStore** store);

/*
*	get recipients for message. output is an array of numrecipients recipients
*	
*/
typedef struct _recipient {
	unsigned short namenumber [256];		/// name or phonenumber
	unsigned short emailnumber [256];		/// email or phonenumber
} recipient;
int mapi_get_recipients (IN IMessage* message, OUT recipient** recipientsarray, OUT int* numrecipients);

/*
*	get attachments from imessage (if any). output is an array of attachment structures
*  
*  attachmentbuffer must be freed by the caller
*/
typedef struct _attachment {
	unsigned short name [256];	/// attachment filename
	unsigned long sizedata;		/// size of attachment (nbo)
	unsigned char data;			/// data
} attachment;
int mapi_get_attachments (IN IMessage* message, OUT attachment** attachmentsarray, OUT unsigned long* attachmentssize, OUT int* numattachments);

/*
*	get message body. bodybuffer must be freed by the caller
*
*/
int mapi_get_body (IN IMessage* message, OUT unsigned char** bodybuffer, OUT unsigned long* bodysize);

/*
*	get messages stats. accountnames (wsz csv) must be freed by the caller. if sms is specified, only sms account is considered. email stats are for standard folders only
*
*/
int mapi_get_messages_stats (IN ICEMAPISession* session, OPTIONAL IN int sms, OPTIONAL OUT PWCHAR* accountnames, OPTIONAL OUT int* numaccounts, OUT int* numinbox, OUT int* numoutbox, OUT int* numsent, OUT int* numdrafts, OUT int* numdeleted);

typedef struct _mapi_folder_entry {
	LIST_ENTRY chain;
	SRowSet* row;			/// foldername, entry_id
} mapi_folder_entry;

/*
*	returns a list with of mapi_folder_entry from a message store. list must be initialized prior to calling with initializelisthead, 
*   childfolder and childstable are reserved and must be NULL. list must then be freed calling mapi_free_folders
*
*/
int mapi_get_folders (IN OUT LIST_ENTRY* list, IN IMsgStore* msgstore, OPTIONAL IN IMAPIFolder* childfolder, OPTIONAL IN IMAPITable* childstable);

/*
*	free list allocated with mapi_get_folders
*
*/
void mapi_free_folders (IN LIST_ENTRY* list);

#ifdef __cplusplus
}
#endif

#endif // #ifndef __mapiwrap_h__