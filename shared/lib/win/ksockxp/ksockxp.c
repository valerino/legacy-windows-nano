/*
 *	kernel sockets (xp version,basic support for tcp only)
 *  -vx-
 */
#include <ntifs.h>
#include <stdio.h>
#include <stdarg.h>
#include <gensock.h>
#include <dbg.h>
#include <tdikrnl.h>

#define POOL_TAG 'xpks'

/*
 *	globals
 *
 */
PDEVICE_OBJECT tcpdev;  /// \\device\\tcp pointer
PAGED_LOOKASIDE_LIST	ls_sockets;			/// lookaside list for sockets
PAGED_LOOKASIDE_LIST	ls_sockmem;			/// used by sockets library to allocate buffers of fixed size

/*
*	initialize kernel sockets
*
*/
NTSTATUS KSocketsInitializeWithDeviceObject (IN void* tcpdeviceobject)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name = {0};

	if (!tcpdeviceobject)
		return STATUS_INVALID_PARAMETER;

	tcpdev = (PDEVICE_OBJECT)tcpdeviceobject;
	DBG_OUT (("KSocketsInitializeWithDeviceObject tcpdevice %x, drivername : %S\n", tcpdev, tcpdev->DriverObject->DriverName.Buffer));

	/// initialize the lookaside lists
	ExInitializePagedLookasideList(&ls_sockets,NULL,NULL,0,sizeof (ksocket) + 256,POOL_TAG,0);
	ExInitializePagedLookasideList(&ls_sockmem, NULL, NULL, 0, SOCKET_MEM_SIZE, POOL_TAG, 0);

	return STATUS_SUCCESS;
}

/*
*	connect socket to address:port (both -NON- nbo)
*
*/
NTSTATUS KSocketsConnect(IN ksocket* sock, IN unsigned long Address, IN unsigned short Port)
{
	PIRP	Irp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	TA_IP_ADDRESS RemoteAddress = {0};
	IO_STATUS_BLOCK iosb = {0};
	KEVENT event;
	TDI_CONNECTION_INFORMATION  reqconnection = {0};
	TDI_CONNECTION_INFORMATION  returnreq  = {0};
	unsigned char buffer[256] = {0};
	PTRANSPORT_ADDRESS pTransportAddress =(PTRANSPORT_ADDRESS)&buffer;
	PTDI_ADDRESS_IP pTdiAddressIp = NULL;

	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;

	/// setup request
	reqconnection.RemoteAddress = (PVOID)pTransportAddress;
	reqconnection.RemoteAddressLength = sizeof(PTRANSPORT_ADDRESS) + sizeof(TDI_ADDRESS_IP); 
	pTransportAddress->TAAddressCount = 1;
	pTransportAddress->Address[0].AddressType = TDI_ADDRESS_TYPE_IP;
	pTransportAddress->Address[0].AddressLength = sizeof(TDI_ADDRESS_IP);
	pTdiAddressIp = (TDI_ADDRESS_IP*)&pTransportAddress->Address[0].Address;
	pTdiAddressIp->sin_port = htons (Port);   
	pTdiAddressIp->in_addr  = htonl (Address);    
	
	/// build irp
	KeInitializeEvent(&event, NotificationEvent, FALSE);	
	Irp = TdiBuildInternalDeviceControlIrp (TDI_CONNECT,tcpdev,sock->conn,&event,&iosb);
	if (!Irp)
		return STATUS_INSUFFICIENT_RESOURCES;
	TdiBuildConnect (Irp, tcpdev, sock->conn, NULL, NULL, NULL, &reqconnection, &returnreq);
	
	/// call tcpip
	Status = IoCallDriver(tcpdev, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
		Status = iosb.Status;
	}
	
	if (NT_SUCCESS (Status))
		sock->connected = TRUE;

	return Status;
}

/*
 *	associate address and connection on socket
 *
 */
NTSTATUS KSocketsAssociate(IN ksocket* sock)
{
	PIRP Irp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK iosb;
	KEVENT event;

	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	if (!sock->conn || !sock->transporthandle)
		return STATUS_INVALID_PARAMETER;
	
	/// build irp
	KeInitializeEvent(&event, NotificationEvent, FALSE);	
	Irp = TdiBuildInternalDeviceControlIrp(TDI_ASSOCIATE_ADDRESS,tcpdev,sock->conn,&event,&iosb);
	if (!Irp)
		return STATUS_INSUFFICIENT_RESOURCES;
	TdiBuildAssociateAddress(Irp, tcpdev, sock->conn, NULL, NULL, sock->transporthandle);

	/// call tcpip
	Status = IoCallDriver(tcpdev, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
		Status = iosb.Status;
	}

	return Status;
}

/*
*	disconnect socket
*
*/
NTSTATUS KSocketsDisconnect(IN ksocket* sock)
{
	PIRP Irp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	TDI_CONNECTION_INFORMATION	ReqDisconnect = {0};
	IO_STATUS_BLOCK iosb;
	KEVENT event;
	
	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	
	/// build irp
	KeInitializeEvent(&event, NotificationEvent, FALSE);	
	Irp = TdiBuildInternalDeviceControlIrp(TDI_DISCONNECT,tcpdev,sock->conn,&event,&iosb);
	if (!Irp)
		return STATUS_INSUFFICIENT_RESOURCES;
	TdiBuildDisconnect (Irp, tcpdev, sock->conn, NULL, NULL, NULL, TDI_DISCONNECT_ABORT, &ReqDisconnect, &ReqDisconnect);

	/// call tcpip
	Status = IoCallDriver(tcpdev, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
		Status = iosb.Status;
	}

	if (NT_SUCCESS (Status))
		sock->connected = FALSE;

	return Status;
}

/*
*	sends a buffer thru socket
*
*/
NTSTATUS KSocketsSend(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesSent)
{
	PIRP Irp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK iosb;
	KEVENT event;
	PMDL Mdl = NULL;

	if (!sock || !Buffer || !tcpdev)
		return STATUS_UNSUCCESSFUL;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (BytesSent)
		*BytesSent = 0;

	/// allocate mdl
	Mdl = IoAllocateMdl(Buffer, SizeBuffer, FALSE, FALSE, NULL);
	if (!Mdl)
		return STATUS_INSUFFICIENT_RESOURCES;

	/// and probe for read
	__try
	{
		MmProbeAndLockPages(Mdl, KernelMode, IoModifyAccess);
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		IoFreeMdl (Mdl);
		return STATUS_UNSUCCESSFUL;
	}
	
	/// build irp
	KeInitializeEvent(&event, NotificationEvent, FALSE);	
	Irp = TdiBuildInternalDeviceControlIrp (TDI_SEND,tcpdev,sock->conn,&event,&iosb);
	if (!Irp)
	{
		IoFreeMdl(Mdl);
		return STATUS_INSUFFICIENT_RESOURCES;
	}
	TdiBuildSend (Irp, tcpdev, sock->conn, NULL, NULL, Mdl, 0, SizeBuffer);

	/// call tcp
	Status = IoCallDriver(tcpdev, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
		Status = iosb.Status;
	}
	if (BytesSent)
		*BytesSent = iosb.Information;

	/// check transferred bytes
	if (iosb.Information != SizeBuffer)
		Status = STATUS_CONNECTION_ABORTED;

	return Status;
}

/*
*	receive a buffer thru socket
*
*/
NTSTATUS KSocketsReceive(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesReceived)
{
	PIRP Irp = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PMDL Mdl = NULL;
	IO_STATUS_BLOCK iosb;
	KEVENT event;

	if (!sock || !tcpdev || !Buffer || !SizeBuffer)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (BytesReceived)
		*BytesReceived = 0;

	/// allocate mdl
	Mdl = IoAllocateMdl(Buffer, SizeBuffer, FALSE, FALSE, NULL);
	if (!Mdl)
		return STATUS_INSUFFICIENT_RESOURCES;

	/// and probe for write
	__try
	{
		MmProbeAndLockPages(Mdl, KernelMode, IoModifyAccess);
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		IoFreeMdl (Mdl);
		return STATUS_UNSUCCESSFUL;
	}

	/// build irp
	KeInitializeEvent(&event, NotificationEvent, FALSE);	
	Irp = TdiBuildInternalDeviceControlIrp (TDI_RECEIVE,tcpdev,sock->conn,&event,&iosb);
	if (!Irp)
	{
		IoFreeMdl(Mdl);
		return STATUS_INSUFFICIENT_RESOURCES;
	}
	TdiBuildReceive (Irp, tcpdev, sock->conn, NULL, NULL, Mdl, TDI_RECEIVE_NORMAL, SizeBuffer);

	/// call tcp
	Status = IoCallDriver(tcpdev, Irp);
	if (Status == STATUS_PENDING)
	{
		KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
		Status = iosb.Status;
	}
	if (BytesReceived)
		*BytesReceived = iosb.Information;

	return Status;
}

/*
*	print a formatted string to socket. Max string length is SOCKET_MEM_SIZE
*
*/
NTSTATUS KSocketsPrintf (IN ksocket* sock, OPTIONAL OUT unsigned long* SentBytes, IN const char* format, ...)
{
	va_list ap = NULL;
	char* buf = NULL;
	ULONG len = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	if (!sock || !format || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;

	/// allocate memory
	buf = ExAllocateFromPagedLookasideList(&ls_sockmem);
	if (!buf)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (buf,0,SOCKET_MEM_SIZE);

	/// build line
	va_start(ap, format);
	_vsnprintf(buf, SOCKET_MEM_SIZE, format, ap);
	va_end(ap);
	len = strlen(buf);

	/// send
	Status = KSocketsSend(sock, buf, len, SentBytes);

	/// free buffer
	ExFreeToPagedLookasideList (&ls_sockmem,buf);

	return Status;
}

/*
*	read an ascii line (until EOL) from socket
*
*/
NTSTATUS KSocketsReadLn(IN ksocket* sock, IN char* buf, IN ULONG buflen, OPTIONAL OUT unsigned long* ReceivedBytes)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UCHAR c = 0;
	ULONG i = 0;
	ULONG received = 0;

	/// check params
	if (!tcpdev || !sock || !buf || !buflen)
		return STATUS_INVALID_PARAMETER;
	if (!sock->connected)
		return STATUS_ALREADY_DISCONNECTED;
	if (ReceivedBytes)
		*ReceivedBytes = 0;

	/// read line char by char, and stop at EOL
	memset (buf, 0, buflen);
	while (TRUE)
	{
		if (i == buflen)
			break;

		/// get char from socket
		Status = KSocketsReceive (sock,&c,1,&received);
		if (!NT_SUCCESS (Status) || received == 0)
			break;

		/// write char into buffer and advance
		*buf = c;
		buf++;
		i++;

		/// check for EOL
		if (c == '\n')
		{
			if (ReceivedBytes)
				*ReceivedBytes = i;
			break;
		}
	}

	/// treat 0 size received as error
	if (received == 0)
		Status = STATUS_NO_DATA_DETECTED;

	return Status;
}

/*
*	close a socket opened with KSocketsCreate
*
*/
NTSTATUS KSocketsClose(ksocket* sock)
{
	if (!sock)
		return STATUS_INVALID_PARAMETER;
	if (sock->transport)
		ObDereferenceObject(sock->transport);
	if (sock->transporthandle)
		ZwClose(sock->transporthandle);
	if (sock->conn)
		ObDereferenceObject(sock->conn);
	if (sock->connhandle)
		ZwClose(sock->connhandle);

	ExFreeToPagedLookasideList (&ls_sockets,sock);
	return STATUS_SUCCESS;
}

/*
 *	open connection endpoint
 *
 */
NTSTATUS KSocketsOpenConnection (IN ksocket* sock)
{
	NTSTATUS Status = STATUS_INSUFFICIENT_RESOURCES;
	UNICODE_STRING usTdiDriverNameString;
	OBJECT_ATTRIBUTES oaTdiDriverNameAttributes;
	IO_STATUS_BLOCK IoStatusBlock;
	char DataBlob[sizeof(FILE_FULL_EA_INFORMATION) + 
		TDI_CONNECTION_CONTEXT_LENGTH + 300] = {0};
	PFILE_FULL_EA_INFORMATION pExtendedAttributesInformation = 
		(PFILE_FULL_EA_INFORMATION)&DataBlob;
	unsigned long dwEASize = 0;
	HANDLE h = NULL;
	PFILE_OBJECT obj = NULL;

	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	sock->conn = NULL;
	sock->connhandle = NULL;

	/// open tcpip device, initializing extended attributes
	RtlInitUnicodeString(&usTdiDriverNameString, L"\\Device\\Tcp");
	InitializeObjectAttributes(&oaTdiDriverNameAttributes, &usTdiDriverNameString, 
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	memcpy (&pExtendedAttributesInformation->EaName, TdiConnectionContext, TDI_CONNECTION_CONTEXT_LENGTH);
	pExtendedAttributesInformation->EaNameLength = TDI_CONNECTION_CONTEXT_LENGTH;
	pExtendedAttributesInformation->EaValueLength = TDI_CONNECTION_CONTEXT_LENGTH; 
	
	dwEASize = sizeof(DataBlob);
	Status = ZwCreateFile(&h, FILE_READ_EA | FILE_WRITE_EA, &oaTdiDriverNameAttributes,
		&IoStatusBlock, NULL, FILE_ATTRIBUTE_NORMAL, 0, FILE_OPEN_IF, 0, pExtendedAttributesInformation, dwEASize);
	if(NT_SUCCESS(Status))
	{
		/// get connection fileobject
		Status = ObReferenceObjectByHandle(h, GENERIC_READ | GENERIC_WRITE, NULL, KernelMode, &obj, NULL);      
		if(!NT_SUCCESS(Status))
		{
			ZwClose(h);
			return Status;
		}

		/// ok
		sock->conn = obj;
		sock->connhandle = h;
	}

	return Status;
}

/*
 *	open transport address
 *
 */
NTSTATUS KSocketsOpenAddress (IN ksocket* sock)
{
	NTSTATUS Status = STATUS_INSUFFICIENT_RESOURCES;
	UNICODE_STRING usTdiDriverNameString;
	OBJECT_ATTRIBUTES oaTdiDriverNameAttributes;
	IO_STATUS_BLOCK IoStatusBlock;
	char DataBlob[sizeof(FILE_FULL_EA_INFORMATION) + TDI_TRANSPORT_ADDRESS_LENGTH + 300] = {0};
	PFILE_FULL_EA_INFORMATION pExtendedAttributesInformation = (PFILE_FULL_EA_INFORMATION)&DataBlob;
	unsigned long dwEASize = 0;
	PTRANSPORT_ADDRESS pTransportAddress = NULL;
	PTDI_ADDRESS_IP pTdiAddressIp = NULL;
	HANDLE h = NULL;
	PFILE_OBJECT obj = NULL;

	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	sock->transport = NULL;
	sock->transporthandle = NULL;

	/// open tcpip device, initializing extended attributes
	RtlInitUnicodeString(&usTdiDriverNameString, L"\\Device\\Tcp");
	InitializeObjectAttributes(&oaTdiDriverNameAttributes, &usTdiDriverNameString, 
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, NULL, NULL);

	memcpy(&pExtendedAttributesInformation->EaName, TdiTransportAddress, TDI_TRANSPORT_ADDRESS_LENGTH);

	pExtendedAttributesInformation->EaNameLength = TDI_TRANSPORT_ADDRESS_LENGTH;
	pExtendedAttributesInformation->EaValueLength = TDI_TRANSPORT_ADDRESS_LENGTH + sizeof(TRANSPORT_ADDRESS) + sizeof(TDI_ADDRESS_IP);
	pTransportAddress = (PTRANSPORT_ADDRESS)(&pExtendedAttributesInformation->EaName + TDI_TRANSPORT_ADDRESS_LENGTH + 1);

	/// initialize transport address
	pTransportAddress->TAAddressCount = 1;
	pTransportAddress->Address[0].AddressType    = TDI_ADDRESS_TYPE_IP;
	pTransportAddress->Address[0].AddressLength  = sizeof(TDI_ADDRESS_IP);
	pTdiAddressIp = (TDI_ADDRESS_IP *)&pTransportAddress->Address[0].Address;

	memset (pTdiAddressIp, 0,sizeof(TDI_ADDRESS_IP));
	dwEASize = sizeof(DataBlob);

	Status = ZwCreateFile(&h, FILE_READ_EA | FILE_WRITE_EA, &oaTdiDriverNameAttributes, &IoStatusBlock, NULL, 
		FILE_ATTRIBUTE_NORMAL, 0, FILE_OPEN_IF, 0, pExtendedAttributesInformation, dwEASize);
	if(NT_SUCCESS(Status))
	{
		/// get transport address fileobject
		Status = ObReferenceObjectByHandle(h,GENERIC_READ | GENERIC_WRITE, NULL, KernelMode, &obj, NULL);      
		if(!NT_SUCCESS(Status))
		{
			ZwClose(h);
			return Status;
		}
		
		/// ok
		sock->transport = obj;
		sock->transporthandle = h;
	}

	return Status;
}

/*
*	create a socket object
*
*/
NTSTATUS KSocketsCreate(IN OUT ksocket** sock)
{
	NTSTATUS Status	= STATUS_UNSUCCESSFUL;
	ksocket* s = NULL;

	if (!sock || !tcpdev)
		return STATUS_INVALID_PARAMETER;
	*sock = NULL;

	/// allocate memory for a new socket
	s = ExAllocateFromPagedLookasideList(&ls_sockets);
	if (!s)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}
	memset (s,0,sizeof (ksocket));

	/// open address and connection
	Status = KSocketsOpenAddress(s);
	if (!NT_SUCCESS(Status))
		goto __exit;
	Status = KSocketsOpenConnection (s);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// associate address with connection
	Status = KSocketsAssociate (s);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// ok
	*sock = s;

__exit:
	if (!NT_SUCCESS(Status))
	{
		if (s)
			KSocketsClose(s);
	}

	return Status;
}

