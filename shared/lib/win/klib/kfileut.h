/*
 *	kernel file utility functions, support filter manager API too
 *
 */

#ifndef __kfileut_h__
#define __kfileut_h__

#include <fltKernel.h>

/*
 *	many of these functions accept file both as handle and name
 *
 */

/*
*	calls ZwCreateFile to perform file operation. Accept the same parameters of ZwCreateFile in combination
*  to the object attributes parameters to InitializeObjectAttributes.
*  resulting handle and fileobject must be closed with KFileClose
*/
NTSTATUS KCreateOpenFile (IN OPTIONAL PFLT_FILTER flt, IN OPTIONAL PFLT_INSTANCE fltinst, OUT HANDLE* filehandle, OUT PFILE_OBJECT* fileobj, IN PUNICODE_STRING filename, IN ULONG objattributes, OPTIONAL IN HANDLE objroot, OPTIONAL IN PSECURITY_DESCRIPTOR secdesc, 
						  IN ACCESS_MASK access, OPTIONAL IN PLARGE_INTEGER allocsize, IN ULONG fileattributes, IN ULONG shareacess, IN ULONG createdisposition, 
						  IN ULONG createoptions, OPTIONAL IN PVOID eabuffer, IN ULONG ealength);
/*
*	returns length of file in size
*
*/
NTSTATUS	KGetFileSize(OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT PLARGE_INTEGER size);

/*
 *	return size of a folder (only base path, no subdirectories)
 *
 */
NTSTATUS	KGetDirectorySize (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PUNICODE_STRING name, OPTIONAL IN HANDLE dirhandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT PLARGE_INTEGER size);

/*
*	delete file (or empty directory, if specified) regardless of attributes
*  
*/
NTSTATUS	KDeleteFile(OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, IN BOOLEAN directory);

/*
*	copy file specifying full source and destination paths
*
*/
NTSTATUS	KCopyFile (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, IN PUNICODE_STRING destpath, IN PUNICODE_STRING srcpath, IN OPTIONAL BOOLEAN replaceattr);

/*
 *	same as ZwReadFile, with the standard parameters (no callbacks)
 *
 */
NTSTATUS	KReadFile (OPTIONAL IN PFLT_INSTANCE fltinst, IN HANDLE hfile, OPTIONAL IN PFILE_OBJECT fileobj, IN PVOID buffer, IN ULONG sizetoread, IN OPTIONAL PLARGE_INTEGER offset);

/*
*	same as ZwWriteFile, with the standard parameters (no callbacks)
*
*/
NTSTATUS	KWriteFile (OPTIONAL IN PFLT_INSTANCE fltinst, IN HANDLE hfile, OPTIONAL IN PFILE_OBJECT fileobj, IN PVOID buffer, IN ULONG sizetowrite, IN OPTIONAL PLARGE_INTEGER offset);

/*
 *	same as ZwQueryInformationFile
 *
 */
NTSTATUS	KQueryFileInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize);

/*
*	same as ZwSetInformationFile
*
*/
NTSTATUS	KSetFileInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize);

/*
 *	same as ZwQueryDirectoryFile (synchronous call)
 *
 */
NTSTATUS KQueryDirectoryInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize, OPTIONAL PUNICODE_STRING filemask, BOOLEAN returnsingle, BOOLEAN restartscan);

/*
*	close file 
*
*/
NTSTATUS KFileClose (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PFILE_OBJECT fobj,IN HANDLE hfile);

/*
*	delete directory tree. startdir must be \\??\\letter:\\... 
*  
*/
NTSTATUS KDeleteDirTree (IN PWCHAR startdir, OPTIONAL IN PWCHAR mask, OPTIONAL IN BOOLEAN removedirs);

#endif /// #ifndef __kfileut_h__
