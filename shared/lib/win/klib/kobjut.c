/*
*	kernel utilities : objects
*  -xv-
*/

#include <ntifs.h>
#include <stdio.h>
#include "kobjut.h"
#include "kgenut.h"

#define POOL_TAG 'tuok'

NTSTATUS KWaitObjectByHandle (IN HANDLE obj, IN BOOLEAN alertable, IN OPTIONAL PLARGE_INTEGER timeout)
{
	if (!obj)
		return STATUS_INVALID_PARAMETER;
	
	/// wait
	return ZwWaitForSingleObject(obj,alertable,timeout);
}

NTSTATUS KGetProcessFullPathByHandle (IN HANDLE processhandle, OUT PUNICODE_STRING* name)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PEPROCESS process = NULL;
	ULONG sizename = 0;
	PVOID buffer = NULL;
	UNICODE_STRING n;

	if (!processhandle || !name)
		return STATUS_INVALID_PARAMETER;
	*name = NULL;

	/// get process name by handle
	Status = ZwQueryInformationProcess (processhandle,ProcessImageFileName,&n,sizeof (UNICODE_STRING),&sizename);
	if (Status == STATUS_INFO_LENGTH_MISMATCH)
	{
		/// allocate large enough buffer
		buffer = ExAllocatePoolWithTag(PagedPool,sizename + 3*sizeof (WCHAR),POOL_TAG);
		if (!buffer)
			return STATUS_INSUFFICIENT_RESOURCES;
		memset (buffer,0,sizename+(3*sizeof (WCHAR)));

		/// reissue
		Status = ZwQueryInformationProcess (processhandle,ProcessImageFileName,buffer,sizename,&sizename);
		if (!NT_SUCCESS (Status))
			ExFreePool(buffer);
		else
			*name = buffer;
		
		return Status;
	}	

	/// can't get a proper name ..... (system process,probably)
	return STATUS_NOT_SUPPORTED;
}

HANDLE KGetProcessHandleByPid (IN HANDLE pid, IN ULONG attribs)
{
	PEPROCESS process = NULL;
	HANDLE processhandle = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if (!pid)
		return NULL;

	/// get process object from pid
	Status = PsLookupProcessByProcessId(pid,&process);
	if (Status != STATUS_SUCCESS)
		return NULL;
	ObDereferenceObject(process);

	/// get handle 
	Status = ObOpenObjectByPointer(process,attribs,NULL,0,NULL,KernelMode,&processhandle);
	if (!NT_SUCCESS (Status))
		return NULL;
	
	return processhandle;
}

NTSTATUS KGetProcessFullPathByPid (IN HANDLE pid, OUT PUNICODE_STRING* name)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE processhandle = NULL;

	if (!pid || !name)
		return STATUS_INVALID_PARAMETER;

	/// get process handle from pid
	processhandle = KGetProcessHandleByPid(pid,OBJ_KERNEL_HANDLE);
	if (!processhandle != STATUS_SUCCESS)
		return STATUS_OBJECTID_NOT_FOUND;
	
	/// get name
	Status = KGetProcessFullPathByHandle(processhandle,name);

	ZwClose(processhandle);
	return Status;
}

NTSTATUS KGetProcessFullPathByEProcess (IN PEPROCESS process, OUT PUNICODE_STRING* name)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE processhandle = NULL;

	if (!process || !name)
		return STATUS_INVALID_PARAMETER;

	/// get handle 
	Status = ObOpenObjectByPointer(process,0,NULL,0,NULL,KernelMode,&processhandle);
	if (!NT_SUCCESS (Status))
		return Status;

	/// get name
	Status = KGetProcessFullPathByHandle(processhandle,name);
	if (!NT_SUCCESS (Status))
	{
		ZwClose(processhandle);	
		return Status;
	}

	ZwClose(processhandle);
	return Status;
}

PVOID KWaitForObjectPresent (IN PUNICODE_STRING objname, BOOLEAN testonly)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PDEVICE_OBJECT devobj = NULL;
	PFILE_OBJECT fileobj = NULL;

	/// check params
	if (!objname)
		return NULL;

	/// loop until object appears.....
	while (TRUE)
	{
		Status = IoGetDeviceObjectPointer(objname, FILE_READ_ATTRIBUTES, &fileobj, &devobj);
		if (NT_SUCCESS(Status))
		{
			/// this must be dereferenced
			ObDereferenceObject(fileobj);
			break;
		}
		
		/// if this is specified, we just test if the object exists
		if (testonly)
			break;
		KStallThread(RELATIVE(SECONDS(1)),KernelMode,FALSE);
	}
	return devobj;
}

/*
 *	get user sid providing an object (opened in the user context). string must be freed with RtlFreeUnicodeString
 *
 */
NTSTATUS KGetUserSidByObject (IN PVOID object, OPTIONAL IN POBJECT_TYPE objtype, OUT PUNICODE_STRING usersidstring)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	HANDLE ht = NULL;
	PTOKEN_USER tokuser = NULL;
	ULONG len = 0;

	if (!object || !usersidstring)
		return STATUS_INVALID_PARAMETER;

	tokuser = ExAllocatePoolWithTag(PagedPool,sizeof (TOKEN_USER) + 256*sizeof (WCHAR), POOL_TAG);
	if (!tokuser)
		return STATUS_INSUFFICIENT_RESOURCES;

	/// open object
	Status = ObOpenObjectByPointer(object,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, 
		NULL, GENERIC_READ, objtype, KernelMode, &h);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// open token
	Status = ZwOpenProcessTokenEx(h, GENERIC_READ, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, &ht);
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// get token informations
	Status = ZwQueryInformationToken (ht,TokenUser,tokuser,sizeof (TOKEN_USER) + 256*sizeof (WCHAR),&len);
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = RtlConvertSidToUnicodeString(usersidstring, tokuser->User.Sid, TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// ok
__exit:
	if (tokuser)
		ExFreePool(tokuser);
	if (h)
		ZwClose(h);
	if (ht)
		ZwClose(ht);
	return Status;
}