#ifndef __klib_h__
#define __klib_h__

// klib strings
#include "kstrut.h"
#include <strlib.h>
// klib objects
#include "kobjut.h"
// klib registry
#include "kregut.h"
// klib files
#include "kfileut.h"
// klib generic kernel
#include "kgenut.h"
// generic sockets
#include <gensock.h>
/// debug
#include <dbg.h>
#endif // #ifndef __klib_h__

