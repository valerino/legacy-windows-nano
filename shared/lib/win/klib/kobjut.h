#ifndef __kobjut_h__
#define __kobjut_h__

#include <WinDef.h>

/*
*	undoc
*
*/
NTSYSAPI NTSTATUS ZwWaitForSingleObject(__in HANDLE Handle, __in BOOLEAN Alertable, __in_opt PLARGE_INTEGER Timeout);

NTSYSAPI NTSTATUS ZwQueryInformationProcess(IN HANDLE ProcessHandle,
	IN ULONG ProcessInformationClass, OUT PVOID ProcessInformation, IN ULONG ProcessInformationLength,
	OUT PULONG ReturnLength OPTIONAL);

NTSYSAPI NTSTATUS RtlConvertSidToUnicodeString(PUNICODE_STRING UnicodeString, PSID Sid, BOOLEAN AllocateDestinationString);

/*
*	get user sid providing an object (opened in the user context). string must be freed with RtlFreeUnicodeString
*
*/
NTSTATUS KGetUserSidByObject (IN PVOID object, OPTIONAL IN POBJECT_TYPE objtype, OUT PUNICODE_STRING usersidstring);

/*
 *	wait on event object by handle (same as call to ZwWaitForSingleObject)
 *
 */
NTSTATUS KWaitObjectByHandle (IN HANDLE obj, IN BOOLEAN alertable, IN OPTIONAL PLARGE_INTEGER timeout);

/*
*	loops (beware!) until an object with the specified name appears, and returns its pointer
*   to avoid looping and just test if the object is present, specify testonly.
*/
PVOID KWaitForObjectPresent (IN PUNICODE_STRING objname, BOOLEAN testonly);

/*
 * get process full path from process pid. The resulting unicode string must be freed with ExFreePool
 *
 */
NTSTATUS KGetProcessFullPathByPid (IN HANDLE pid, OUT PUNICODE_STRING* name);

/*
 * get process full path from process handle. The resulting unicode string must be freed with ExFreePool
 *
 */
NTSTATUS KGetProcessFullPathByHandle (IN HANDLE processhandle, OUT PUNICODE_STRING* name);

/*
 * get process full path from EPROCESS. The resulting unicode string must be freed with ExFreePool
 *
 */
NTSTATUS KGetProcessFullPathByEProcess (IN PEPROCESS process, OUT PUNICODE_STRING* name);

/*
 *	get process handle from pid. attribs are attributes of the returned handle (see ObOpenObjectByPointer).
 *  handle must be closed with ZwClose
 *
 */
HANDLE KGetProcessHandleByPid (IN HANDLE pid, IN ULONG attribs);

#endif /// #ifndef __kobjut_h__