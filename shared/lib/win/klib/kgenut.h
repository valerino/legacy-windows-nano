#ifndef __kgenut_h__
#define __kgenut_h__

/*
*	network macros
*
*/
#ifndef htonl
#define htonl(l)			\
	((((l)&0xFF000000L)>> 24) | \
	(((l)&0x00FF0000L)>> 8) |   \
	(((l)&0x0000FF00L)<< 8) |   \
	(((l)&0x000000FFL)<< 24))
#endif

#ifndef htons
#define htons(s)			\
	((((s)&0xFF00)>>8) |		\
	(((s)&0x00FF)<<8))
#endif

#define UC(b) (((int)b)&0xff)

/*
*	time macros
*
*/
#define ABSOLUTE(wait) (wait)

#define RELATIVE(wait) (-(wait))

#define NANOSECONDS(nanos)   \
	(((signed __int64)(nanos)) / 100L)

#define MICROSECONDS(micros) \
	(((signed __int64)(micros)) * NANOSECONDS(1000L))

#define MILLISECONDS(milli)  \
	(((signed __int64)(milli)) * MICROSECONDS(1000L))

#define SECONDS(seconds)	 \
	(((signed __int64)(seconds)) * MILLISECONDS(1000L))

#define MINUTES(minutes)	 \
	(((signed __int64)(minutes)) * SECONDS(60L))

#define HOURS(hours)		 \

/*
*	copy irp to a newly allocated one, optionally setting up completion routine
*
*/
PIRP KAllocateCopyIrp(IN PIRP pSourceIrp, IN CCHAR StackSize, OPTIONAL PVOID pCompletionRoutine, OPTIONAL PVOID pContext);


/*
*	stalls a thread for the specified time (use time macros here....)
*   parameters are the same as KeDelayExecutionThread
*/
void KStallThread (IN signed __int64 timeout, IN KPROCESSOR_MODE WaitMode, IN BOOLEAN alertable);

/*
 *	enforce memory protection by setting a key in the registry.
 *  beware, this function causes reboot if the key is not set, ensuring its 
 *  set on the reboot right after!
 */
NTSTATUS KEnforceMemoryProtectionDisabledRegistry ();

/*
*	free linked list which members were allocated by exallocatepool
*
*/
void KFreeList (IN PLIST_ENTRY l);

/*
*	allocate paged or nonpaged memory depending on current IRQL. PagedPool is allocated only at PASSIVE_LEVEL
*
*/
PVOID KAllocatePagedOrNonPaged (IN ULONG size, IN ULONG tag);

/*
 *	allocate memory from lookaside list or pool depending on size
 *
 */
PVOID KAllocateFromLsOrPool (IN ULONG size, IN ULONG type, IN PVOID lookaside, IN ULONG maxls_size, IN ULONG tag, OUT PBOOLEAN allocatedfromlookaside);

/*
*	free from memory or pool
*
*/
void KFreeFromLsOrPool (IN PVOID buffer, OPTIONAL IN PVOID lookaside, OPTIONAL IN ULONG type);

/*
*	same as realloc. memory is guaranteed to be zeroed and 16 bytes aligned
*
*/
PVOID KRealloc (IN POOL_TYPE type, OPTIONAL IN void* oldptr, OPTIONAL IN ULONG oldsize, IN ULONG newsize);

/*
*	returns unsigned long (nbo) ip from ascii string (x.x.x.x)
*
*/
unsigned long Kinet_addr(const char *src);

/*
*	get ascii string (x.x.x.x) from ip (nbo)
*
*/
char* KIpToAscii(unsigned long in, char* out, int sizeout);

#endif /// #ifndef __kgenut_h__