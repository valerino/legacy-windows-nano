/*
 *	kernel utilities : files
 *  -xv-
 */

#include <ntifs.h>
#include "kgenut.h"
#include "kobjut.h"
#include "kfileut.h"
#include "kstrut.h"
#include <strlib.h>
#include <ntstrsafe.h>
#include <dbg.h>

#define POOL_TAG 'tufk'

/*
 *	close file (if fileobject is passed, its dereferenced too) 
 *
 */
NTSTATUS KFileClose (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PFILE_OBJECT fobj,IN HANDLE hfile)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	
	if (fobj)
		ObDereferenceObject(fobj);

	if (hfile)
	{
		if (fltinst || flt)
			Status = FltClose(hfile);
		else
			Status = ZwClose(hfile);
	}
	return Status;
}

NTSTATUS KQueryFileInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK iosb;

	if (!buffer || !bufsize || (!fileobj && !handle))
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	if (!fltinst)
		Status = ZwQueryInformationFile(handle, &iosb, buffer, bufsize, infoclass);
	else
		Status = FltQueryInformationFile (fltinst,fileobj,buffer,bufsize,infoclass,NULL);

	return Status;
}

NTSTATUS KSetFileInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK iosb;

	if (!buffer || !bufsize || (!fileobj && !handle))
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	if (!fltinst)
		Status = ZwSetInformationFile(handle, &iosb, buffer, bufsize, infoclass);
	else
		Status = FltSetInformationFile(fltinst,fileobj,buffer,bufsize,infoclass);

	return Status;
}

NTSTATUS KQueryDirectoryInformation (OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN HANDLE handle, OPTIONAL IN PFILE_OBJECT fileobj, IN FILE_INFORMATION_CLASS infoclass, IN PVOID buffer, IN ULONG bufsize, OPTIONAL PUNICODE_STRING filemask, BOOLEAN returnsingle, BOOLEAN restartscan)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK iosb;

	if (!buffer || !bufsize || (!fileobj && !handle))
		return STATUS_INVALID_PARAMETER;

#if NTDDI_VERSION < NTDDI_LONGHORN
	fltinst = NULL;
	if (!handle)
		return STATUS_INVALID_PARAMETER;
	Status = ZwQueryDirectoryFile(handle,NULL,NULL,NULL,&iosb,buffer,bufsize,
		infoclass, returnsingle, filemask, restartscan);
	if (Status == STATUS_PENDING)
	{
		/// wait for completion on filehandle
		KWaitObjectByHandle(handle,FALSE,NULL);
		Status = iosb.Status;
	}
#else
	if (!fltinst)
	{
		Status = ZwQueryDirectoryFile(handle,NULL,NULL,NULL,&iosb,buffer,bufsize,
			infoclass, returnsingle, filemask, restartscan);
		if (Status == STATUS_PENDING)
		{
			/// wait for completion on filehandle
			KWaitObjectByHandle(handle,FALSE,NULL);
			Status = iosb.Status;
		}
	}
	else
	{
		if (!fileobj)
			return STATUS_INVALID_PARAMETER;

		Status = FltQueryDirectoryFile (fltinst,fileobj,buffer,bufsize,infoclass,returnsingle,filemask,
			restartscan,NULL);
	}
#endif // #if NTDDI_VERSION < NTDDI_LONGHORN

	return Status;
}

NTSTATUS KGetDirectorySize (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PUNICODE_STRING name, OPTIONAL IN HANDLE dirhandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT PLARGE_INTEGER size)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PFILE_DIRECTORY_INFORMATION info = NULL;
	HANDLE h = NULL;
	BOOLEAN restartscan = TRUE;
	PFILE_OBJECT f = NULL;

	if (!size)
		return STATUS_INVALID_PARAMETER;
	if ((!name && (!dirhandle && !fileobj)))
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	size->QuadPart = 0;

	/// check if handle is provided
	if (dirhandle || fileobj)
	{
		h=dirhandle;
		f=fileobj;
		goto __handleprovided;
	}

	Status = KCreateOpenFile(flt,fltinst,&h,&f,name,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ|FILE_LIST_DIRECTORY|SYNCHRONIZE,NULL,
		FILE_ATTRIBUTE_DIRECTORY,FILE_SHARE_READ, FILE_OPEN, FILE_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	info = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION),POOL_TAG);
	if (!info)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	while (TRUE)
	{
		/// get directory info
		Status = KQueryDirectoryInformation(fltinst,h,f,FileDirectoryInformation,
			info,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION),NULL,TRUE,restartscan);
		if (!NT_SUCCESS (Status))
		{
			/// if its' empty, return success
			if (Status == STATUS_NO_MORE_FILES)
				Status = STATUS_SUCCESS;
			break;
		}

		/// increment size
		size->QuadPart+=info->EndOfFile.QuadPart;
		restartscan = FALSE;
	}

__exit:
	if (info)
		ExFreePool(info);
	if (f && !fileobj)
		ObDereferenceObject (f);
	if (h && !dirhandle)
		KFileClose(flt,fltinst,NULL,h);
	return Status;
}

NTSTATUS KCreateOpenFile (IN OPTIONAL PFLT_FILTER flt, IN OPTIONAL PFLT_INSTANCE fltinst, OUT HANDLE* filehandle, OUT PFILE_OBJECT* fileobj, IN PUNICODE_STRING filename, IN ULONG objattributes, OPTIONAL IN HANDLE objroot, OPTIONAL IN PSECURITY_DESCRIPTOR secdesc, 
				  IN ACCESS_MASK access, OPTIONAL IN PLARGE_INTEGER allocsize, IN ULONG fileattributes, IN ULONG shareacess, IN ULONG createdisposition, 
				  IN ULONG createoptions, OPTIONAL IN PVOID eabuffer, IN ULONG ealength)
{
	OBJECT_ATTRIBUTES ObjectAttributes;
	HANDLE h = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	IO_STATUS_BLOCK  iosb;
	PFILE_OBJECT f = NULL;

	/// check params
	if (!filename || !filehandle || !fileobj)
		return STATUS_INVALID_PARAMETER;
	
	/// initialize object
	InitializeObjectAttributes(&ObjectAttributes, filename, objattributes, objroot, secdesc);
	
	/// open file
	*filehandle = NULL;
	*fileobj = NULL;
	if (!flt)
		Status = ZwCreateFile(&h,access,&ObjectAttributes,&iosb,allocsize,fileattributes, shareacess, createdisposition,
			createoptions,eabuffer,ealength);
	else
	{
#if NTDDI_VERSION >= NTDDI_LONGHORN
		Status = FltCreateFileEx (flt,fltinst,&h, &f, access,&ObjectAttributes,&iosb, allocsize, fileattributes,
			shareacess, createdisposition, createoptions, eabuffer, ealength, IO_IGNORE_SHARE_ACCESS_CHECK);
#else
		Status = FltCreateFile (flt,fltinst,&h,access, &ObjectAttributes ,&iosb, allocsize, fileattributes,
			shareacess, createdisposition, createoptions, eabuffer, ealength, IO_IGNORE_SHARE_ACCESS_CHECK);
#endif // #if NTDDI_VERSION >= NTDDI_LONGHORN
	}
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	// get fileobject (if we used standard api or ddi < longhorn)
	if (!f)
	{
		Status = ObReferenceObjectByHandle(h,0,*IoFileObjectType,KernelMode,&f,NULL);
		if (!NT_SUCCESS (Status))
		{
			KFileClose(flt,fltinst,f,h);
			h=NULL;
			f=NULL;
		}
	}

__exit:
	if (NT_SUCCESS (Status))
	{
		*filehandle = h;
		*fileobj = f;
	}
	return Status;
}

NTSTATUS KGetFileSize(OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, OUT PLARGE_INTEGER size)
{
	FILE_STANDARD_INFORMATION	FileStandardInfo;
	NTSTATUS Status	= STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	PFILE_OBJECT f = NULL;

	/// check params
	if (!filename && (!filehandle && !fileobj))
		return STATUS_INVALID_PARAMETER;
	if (!size)
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	/// check if handle is provided
	if (filehandle || fileobj)
	{
		h = filehandle;
		f = fileobj;
		goto __handleprovided;
	}

	Status = KCreateOpenFile(flt,fltinst,&h,&f, filename, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,GENERIC_READ,NULL,FILE_ATTRIBUTE_NORMAL,
			FILE_SHARE_READ|SYNCHRONIZE, FILE_OPEN, FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT,NULL,0);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	size->QuadPart = 0;
	
	/// query file informations and get size
	Status = KQueryFileInformation(fltinst,h,f,FileStandardInformation,&FileStandardInfo, sizeof(FILE_STANDARD_INFORMATION));
	if (!NT_SUCCESS(Status))
		goto __exit;
	size->QuadPart = FileStandardInfo.EndOfFile.QuadPart;

__exit :
	if (f && !fileobj)
		ObDereferenceObject(f);

	if (h && !filehandle)
		KFileClose(flt,fltinst,NULL,h);

	return Status;
}

NTSTATUS KDeleteFile(OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, OPTIONAL IN PUNICODE_STRING filename, OPTIONAL IN HANDLE filehandle, OPTIONAL IN PFILE_OBJECT fileobj, IN BOOLEAN directory)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE h = NULL;
	FILE_BASIC_INFORMATION FileinfoBasic;
	FILE_DISPOSITION_INFORMATION FileinfoDisposition;
	ULONG CreateOptions	= FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT;
	ULONG DesiredAccess	= GENERIC_READ | GENERIC_WRITE | DELETE | SYNCHRONIZE;
	PFILE_OBJECT f = NULL;

	if (!filename && (!filehandle && !fileobj))
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	/// check if handle is provided
	if (filehandle || fileobj)
	{
		h = filehandle;
		f = fileobj;
		goto __HandleProvided;
	}
	
	/// open file/directory
	if (directory)
	{
		CreateOptions = FILE_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT;
		DesiredAccess = FILE_LIST_DIRECTORY | DELETE;
	}
	Status = KCreateOpenFile(flt,fltinst,&h,&f,filename,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL,DesiredAccess,NULL,
		FILE_ATTRIBUTE_NORMAL,FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,FILE_OPEN,CreateOptions,NULL,0);
	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
			Status = STATUS_SUCCESS;
		goto __exit;
	}
	
__HandleProvided:		
	/// query fileinfo
	Status = KQueryFileInformation(fltinst,h,f,FileBasicInformation, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION));
	if (!NT_SUCCESS(Status))
	{
		if (Status == STATUS_DELETE_PENDING)
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// reset attributes
	if (directory)
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_DIRECTORY;
	else
		FileinfoBasic.FileAttributes = FILE_ATTRIBUTE_NORMAL;

	Status = KSetFileInformation(fltinst,h,f,FileBasicInformation,&FileinfoBasic,sizeof (FILE_BASIC_INFORMATION));
	if (!NT_SUCCESS(Status))
		goto __exit;
	
	/// mark file for deletion
	FileinfoDisposition.DeleteFile = TRUE;
	Status = KSetFileInformation(fltinst,h,f,FileDispositionInformation,&FileinfoDisposition,sizeof (FILE_DISPOSITION_INFORMATION));

__exit:
	if (f && !fileobj)
		ObDereferenceObject(f);

	if (h && !filehandle)
		KFileClose(flt,fltinst,NULL,h);
	return Status;
}

NTSTATUS KWriteFile (OPTIONAL IN PFLT_INSTANCE fltinst, IN HANDLE hfile, OPTIONAL IN PFILE_OBJECT fileobj, IN PVOID buffer, IN ULONG sizetowrite, IN OPTIONAL PLARGE_INTEGER offset)
{
	IO_STATUS_BLOCK iosb;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if ((!hfile && !fileobj) || !buffer)
		return STATUS_INVALID_PARAMETER;
	if (sizetowrite == 0)
		return STATUS_SUCCESS;	
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;

	/// read file
	if (!fltinst)
		Status = ZwWriteFile(hfile, NULL, NULL, NULL, &iosb, buffer, sizetowrite, offset, NULL);
	else
		Status = FltWriteFile(fltinst,fileobj,offset,sizetowrite,buffer,0,NULL,NULL,NULL);

	return Status;
}

NTSTATUS KReadFile (OPTIONAL IN PFLT_INSTANCE fltinst, IN HANDLE hfile, OPTIONAL IN PFILE_OBJECT fileobj, IN PVOID buffer, IN ULONG sizetoread, IN OPTIONAL PLARGE_INTEGER offset)
{
	IO_STATUS_BLOCK iosb;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	if ((!hfile && !fileobj) || !buffer)
		return STATUS_INVALID_PARAMETER;
	if (!fltinst && !hfile)
		return STATUS_INVALID_PARAMETER;
	if (!fltinst)
		fileobj = NULL;
	if (!fileobj)
		fltinst = NULL;
	if (sizetoread == 0)
		return STATUS_SUCCESS;	
	
	/// read file
	if (!fltinst)
		Status = ZwReadFile(hfile, NULL, NULL, NULL, &iosb, buffer, sizetoread, offset, NULL);
	else
		Status = FltReadFile(fltinst,fileobj,offset,sizetoread,buffer,0,NULL,NULL,NULL);
	
	return Status;
}

NTSTATUS KCopyFile (OPTIONAL IN PFLT_FILTER flt, OPTIONAL IN PFLT_INSTANCE fltinst, IN PUNICODE_STRING destpath, IN PUNICODE_STRING srcpath, IN OPTIONAL BOOLEAN replaceattr)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE inFile = NULL;
	HANDLE outFile = NULL;
	ULONG fileLen = 0;
	ULONG NextTransferSize	= 0;
	LARGE_INTEGER FileSize;
	LARGE_INTEGER Offset;
	PUCHAR	buffer = NULL;
	ULONG CopySize = 64*1024;
	FILE_BASIC_INFORMATION FileinfoBasic;
	PFILE_OBJECT srcobj = NULL;
	PFILE_OBJECT dstobj = NULL;

	if (!srcpath || !destpath)
		return STATUS_INVALID_PARAMETER;

	/// open source file
	Status = KCreateOpenFile(flt,fltinst,&inFile,&srcobj,srcpath,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE,NULL,NULL, GENERIC_READ|SYNCHRONIZE,
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_OPEN, 
		FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);

	if (!NT_SUCCESS(Status))
		goto __exit;

	/// open destination file
	Status = KCreateOpenFile(flt,fltinst,&outFile,&dstobj,destpath,OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL, GENERIC_READ|GENERIC_WRITE|SYNCHRONIZE,
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_SUPERSEDE, 
		FILE_NON_DIRECTORY_FILE|FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0);
	if (!NT_SUCCESS(Status))
		goto __exit;

	/// get sourcefile size
	Status = KGetFileSize(flt,fltinst,NULL,inFile,srcobj,&FileSize);
	if (!NT_SUCCESS(Status))
		goto __exit;
	fileLen = FileSize.LowPart;
	if (fileLen == 0)
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// allocate buffer for copying
	buffer = ExAllocatePoolWithTag (PagedPool,CopySize,POOL_TAG);
	if (buffer == NULL)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	/// copy file
	Offset.QuadPart = 0;
	while (fileLen)
	{
		if (fileLen < CopySize)
			NextTransferSize = fileLen;
		else
			NextTransferSize = CopySize;

		Status = KReadFile(fltinst,inFile,srcobj, buffer, NextTransferSize, &Offset);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Status = KWriteFile(fltinst,outFile, dstobj,buffer,NextTransferSize,&Offset);
		if (!NT_SUCCESS(Status))
			goto __exit;

		Offset.QuadPart += NextTransferSize;
		fileLen -= NextTransferSize;
	}

	/// done copying, now replace attributes if needed
	if (!replaceattr)
		goto __exit;

	/// replace new file attributes with the source ones
	Status = KQueryFileInformation(fltinst,inFile,srcobj,FileBasicInformation, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION));
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = KSetFileInformation(fltinst,outFile,dstobj,FileBasicInformation, &FileinfoBasic, sizeof(FILE_BASIC_INFORMATION));

__exit:
	/// free memory and handles
	if (inFile)
		KFileClose(flt,fltinst,srcobj,inFile);
	
	if (outFile)
		KFileClose(flt,fltinst,dstobj,outFile);

	if (buffer)
		ExFreePool (buffer);
	return Status;
}

/*
*	delete directory tree. startdir must be \\??\\letter:\\... 
*  
*/
NTSTATUS KDeleteDirTree (IN PWCHAR startdir, OPTIONAL IN PWCHAR mask, OPTIONAL IN BOOLEAN removedirs)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	static int numrecursion;
	static PFILE_DIRECTORY_INFORMATION	filedirinfo;
	static PWCHAR newname;
	PWCHAR newstartdir = NULL;
	UNICODE_STRING name;
	UNICODE_STRING filename;
	HANDLE hdir = NULL;
	PFILE_OBJECT fdir = NULL;
	BOOLEAN restartscan = TRUE;
	ULONG len = 0;

	DBG_OUT (("KDeleteDirTree : startdir: %S\n", startdir));

	/// initialization on recursion 0
	if (numrecursion == 0)
	{
		/// allocate all the static buffers
		filedirinfo = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (FILE_DIRECTORY_INFORMATION) + 1,POOL_TAG);
		if (!filedirinfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		newname = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
		if (!newname)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
	}

	/// open directory
	newstartdir = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!newstartdir)
	{
		/// do not fail here
		Status = STATUS_SUCCESS;
		goto __exit;
	}
	RtlStringCbCopyW(newstartdir,1024*sizeof (WCHAR),startdir);
	RtlInitUnicodeString(&name,newstartdir);
	Status = KCreateOpenFile (NULL,NULL,&hdir,&fdir,&name, OBJ_CASE_INSENSITIVE|OBJ_KERNEL_HANDLE, NULL, NULL, FILE_LIST_DIRECTORY|DELETE, 
		NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, FILE_OPEN, FILE_DIRECTORY_FILE, NULL, 0);
	if (!NT_SUCCESS(Status))
	{
		/// prevent open errors to stop scanning
		if (!NT_SUCCESS(Status))
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	while (TRUE)
	{
		/// query directory
		Status = KQueryDirectoryInformation(NULL,hdir,fdir,FileDirectoryInformation,filedirinfo,sizeof (FILE_DIRECTORY_INFORMATION) + (1024*sizeof(WCHAR)),NULL,TRUE,restartscan);
		if (!NT_SUCCESS(Status))
		{
			if (Status == STATUS_NO_MORE_FILES)
			{
				/// delete directory
				if (removedirs)
					Status = KDeleteFile(NULL,NULL,&name,NULL,NULL,TRUE);
				Status = STATUS_SUCCESS;
			}
			break;
		}

		/// check if its the parent entry
		if (*filedirinfo->FileName == (WCHAR) '.')
			goto __next;

		/// build name
		if (numrecursion == 0)
			RtlStringCbCopyW (newname,1024*sizeof (WCHAR),newstartdir);
		else
			RtlStringCbPrintfW(newname,1024*sizeof (WCHAR),L"%s\\",newstartdir);
		RtlStringCbCatNW(newname,1024*sizeof(WCHAR),filedirinfo->FileName,filedirinfo->FileNameLength);

		DBG_OUT (("KDeleteDirTree : entry: %S\n", newname));

		/// check if its a directory
		if ((filedirinfo->FileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			/// recurse
			numrecursion++;
			Status = KDeleteDirTree(newname,mask,removedirs);
			numrecursion--;
			if (!NT_SUCCESS (Status))
				break;
		}
		else
		{
			/// check mask
			if (mask)
			{
				if (!find_buffer_in_buffer(filedirinfo->FileName,mask,filedirinfo->FileNameLength,str_lenbytesw(mask),FALSE))
					goto __next;
			}

			/// delete file
			RtlInitUnicodeString(&filename,newname);
			Status = KDeleteFile(NULL,NULL,&filename,NULL,NULL,FALSE);
			if (!NT_SUCCESS (Status))
				break;
		}

__next:
		/// continue scan
		restartscan = FALSE;
	} /// end while TRUE

__exit:
	if (hdir || fdir)
		KFileClose(NULL,NULL,fdir,hdir);
	if (newstartdir)
		ExFreePool(newstartdir);
	if (numrecursion == 0)
	{
		/// free the static buffers
		if (filedirinfo)
			ExFreePool(filedirinfo);
		if (newname)
			ExFreePool(newname);
	}
	return Status;
}

