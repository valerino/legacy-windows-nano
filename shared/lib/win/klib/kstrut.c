/*
*	kernel utilities : strings
*  -xv-
*/

#include <ntddk.h>
#include "kstrut.h"
#include <stdio.h>

/*
*	convert an unicode string to WCHAR
*
*/
unsigned long KUnicodeStringToCharW (OUT wchar_t* dststring, IN unsigned long dstsize, IN PUNICODE_STRING srcstring)
{
	/// check params
	if (!dststring || !srcstring || !dstsize)
		return 0;
	if (dstsize < srcstring->Length)
		return 0;

	/// copy string
	memset ((char*)dststring,0,dstsize);
	memcpy (dststring,srcstring->Buffer,srcstring->Length);
	return srcstring->Length;
}

/*
*	same as wcsupper
*
*/
void KUpcaseCharW(IN OUT wchar_t* srcstring, IN unsigned long srclen)
{
	unsigned long len = srclen;
	wchar_t* pstr = srcstring;

	if(!srcstring)
		return;

	while(srclen)
	{
		if(*pstr != L'\0')
		{
			*pstr = RtlUpcaseUnicodeChar(*pstr);
			pstr++;
		}
		else
			break;

		srclen--;
	}
}
/*
*	same as strtol
*
*/
unsigned long KStrtol(const char *s, char **endp, int base)
{
	int negate;
	long digit;
	long max,min;
	long d;

	d=0;
	/* Skip leading whitespace */
	while(isspace(*s))
		s++;

	/* Check if negative */
	if(*s=='-')
		negate=1,s++;
	else
		negate=0;

	/* Set base depending on string */
	if(base==0)
	{
		if(*s=='0')
		{
			s++;
			if(*s=='x' || *s=='X')
			{
				s++;
				base = 16;
			}
			else
				base = 8;
		}
		else
			base = 10;
	}

	/* Handle 'x' in base 16 */
	if(base==16)
	{
		if(*s=='0')
		{
			s++;
			if(*s=='x' || *s=='X')
				s++;
		}
	}

	/* Handle illegal bases */
	if(base<2 || base>36)
	{
		if(endp) 
			*endp = (char *)s;
		return(0);
	}

	/* Set up limits for overflow on multiplication */
	max = LONG_MAX/(base-1);
	min = LONG_MIN/(base-1);

	/* Convert number */
	while(*s)
	{
		if(*s>='0' && *s<='9')
			digit = *s-'0';
		else if(*s>='a' && *s<='z')
			digit = *s-'a'+10;
		else if(*s>='A' && *s<='Z')
			digit = *s-'A'+10;
		else
			break;

		if(digit>=base) 
			break;

		if(max<=d)
		{
			d=LONG_MAX;
			break;
		}
		else if(min>=d)
		{
			d=LONG_MIN;
			break;
		}

		d = d*base;
		if(negate)
		{
			if(LONG_MIN+digit>d)
				d=LONG_MIN;
			else
				d-=digit;
		}
		else
		{
			if(LONG_MAX-digit<d)
				d=LONG_MAX;
			else
				d+=digit;
		}

		s++;
	}

	/* Return pointer to rest of string */
	if(endp) 
		*endp = (char *)s;
	return(d); 
}

/*
*	same as atol
*
*/
long KAtol (const char *nptr)
{
	int c;
	long total;
	int sign;

	while (isspace((int)(unsigned char) *nptr)) ++nptr;

	c = (int)(unsigned char) *nptr++;
	sign = c;
	if (c == '-' || c == '+') c = (int)(unsigned char) *nptr++;

	total = 0;
	while (isdigit(c)) 
	{
		total = 10 * total + (c - '0');
		c = (int)(unsigned char) *nptr++;
	}

	if (sign == '-')
		return -total;
	else
		return total;
}

