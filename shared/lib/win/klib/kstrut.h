#ifndef __kstrut_h__
#define __kstrut_h__

#ifndef LONG_MAX
#define LONG_MAX      2147483647L   /* maximum (signed) long value */
#endif

#ifndef LONG_MIN
#define LONG_MIN    (-2147483647L - 1) /* minimum (signed) long value */
#endif

/*
*	convert an unicode string to WCHAR
*
*/
unsigned long KUnicodeStringToCharW (OUT wchar_t* dststring, IN unsigned long dstsize, IN PUNICODE_STRING srcstring);

/*
*	same as wcsupper
*
*/
void KUpcaseCharW(IN OUT wchar_t* srcstring, IN unsigned long srclen);

/*
*	same as strtol
*
*/
unsigned long KStrtol(const char *s, char **endp, int base);

/*
*	same as atol
*
*/
long KAtol (const char *nptr);

#endif /// #ifndef __kstrut_h__