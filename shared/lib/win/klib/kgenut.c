/*
*	kernel utilities : generic
*   -xv-
*/
#include <ntddk.h>
#include <kstrut.h>
#include <kregut.h>
#include <kgenut.h>
#include <ntstrsafe.h>

#define POOL_TAG 'tugk'


/*
*	free from memory or pool
*
*/
void KFreeFromLsOrPool (IN PVOID buffer, OPTIONAL IN PVOID lookaside, OPTIONAL IN POOL_TYPE type)
{
	if (!buffer)
		return;
	
	if (!lookaside)
	{
		// free from pool
		ExFreePool (buffer);
		return;
	}	

	// free from lookaside
	if (type == PagedPool)
		ExFreeToPagedLookasideList ((PPAGED_LOOKASIDE_LIST)lookaside,buffer);
	else
		ExFreeToNPagedLookasideList((PNPAGED_LOOKASIDE_LIST)lookaside,buffer);
	return;
}

/*
*	allocate memory from lookaside list or pool depending on size
*
*/
PVOID KAllocateFromLsOrPool (IN ULONG size, IN POOL_TYPE type, IN PVOID lookaside, IN ULONG maxls_size, IN ULONG tag, OUT PBOOLEAN allocatedfromlookaside)
{
	*allocatedfromlookaside = FALSE;

	/// check if it exceeds ls size
	if (size > maxls_size)
		return ExAllocatePoolWithTag(type,size,tag);

	*allocatedfromlookaside = TRUE;

	/// allocate from paged pool
	if (type == PagedPool)
		return ExAllocateFromPagedLookasideList((PPAGED_LOOKASIDE_LIST)lookaside);

	/// allocate from nonpaged pool
	return ExAllocateFromNPagedLookasideList((PNPAGED_LOOKASIDE_LIST)lookaside);
}

/*
*	same as realloc. memory is guaranteed to be zeroed and 16 bytes aligned
*
*/
PVOID KRealloc (IN POOL_TYPE type, OPTIONAL IN void* oldptr, OPTIONAL IN ULONG oldsize, IN ULONG newsize)
{
	PVOID p = NULL;
	
	if (!newsize)
		return NULL;
	
	/// if oldsize or ptr aren't provided, its a normal allocation
	if (!oldptr || !oldsize)
	{
		p = ExAllocatePoolWithTag(type,newsize + 32,POOL_TAG);
		if (p)
			memset (p,0,newsize + 32);
		return p;
	}

	/// if not, we must allocate a new block and copy

	/// if the sizes overlaps, we return the old pointer directly (no free)
	if (newsize <= oldsize)
		return oldptr;

	p = ExAllocatePoolWithTag(type,newsize + 32,POOL_TAG);
	if (!p)
		return NULL;
	memset (p,0,newsize + 32);

	/// copy and free the old block, return new pointer
	memcpy (p,oldptr,oldsize);
	ExFreePool(oldptr);
	return p;
}

/*
*	allocate paged or nonpaged memory depending on current IRQL. PagedPool is allocated only at PASSIVE_LEVEL
*
*/
PVOID KAllocatePagedOrNonPaged (IN ULONG size, IN ULONG tag)
{
	if (KeGetCurrentIrql () == PASSIVE_LEVEL)
		return ExAllocatePoolWithTag(PagedPool,size,tag);
	else
		return ExAllocatePoolWithTag(NonPagedPool,size,tag);
}

/*
*	stalls a thread for the specified time (use time macros here....)
*   parameters are the same as KeDelayExecutionThread
*/
void KStallThread (IN signed __int64 timeout, IN KPROCESSOR_MODE WaitMode, IN BOOLEAN alertable)
{
	LARGE_INTEGER t;

	t.QuadPart = timeout;
	KeDelayExecutionThread (KernelMode,FALSE,&t);
}


/*
 *	get ascii string (x.x.x.x) from ip (nbo)
 *
 */
char* KIpToAscii(unsigned long in, char* out, int sizeout)
{
	register char * p;

	p = (char*)&in;
	RtlStringCbPrintfA (out,sizeout,"%d.%d.%d.%d",UC(p[0]),UC(p[1]),UC(p[2]),UC(p[3]));
	
	return (out);
}

/*
*	returns unsigned long (nbo) ip from ascii string (x.x.x.x)
*
*/
unsigned long Kinet_addr(const char *src)
{
	unsigned long val;
	int base, n;
	unsigned char c;
	unsigned long parts[4];
	unsigned long *pp = parts;

	c = *src;
	for (;;) {
		/*
		* Collect number up to ``.''.
		* Values are specified as for C:
		* 0x=hex, 0=octal, isdigit=decimal.
		*/
		if (!isdigit(c))
			return (0);
		val = 0; base = 10;
		if (c == '0') {
			c = *++src;
			if (toupper(c) == 'X')
				base = 16, c = *++src;
			else
				base = 8;
		}
		for (;;) {
			if (isdigit(c)) {
				val = (val * base) + (c - '0');
				c = *++src;
			} else if (base == 16 && isxdigit(toupper(c))) {
				val = (val << 4) |
					(toupper(c) + 10 - 'A');
				c = *++src;
			} else
				break;
		}
		if (c == '.') {
			/*
			* Internet format:
			*      a.b.c.d
			*      a.b.c   (with c treated as 16 bits)
			*      a.b     (with b treated as 24 bits)
			*/
			if (pp >= parts + 3)
				return (0);
			*pp++ = val;
			c = *++src;
		} else
			break;
	}
	/*
	* Check for trailing characters.
	*/
	if (c != '\0' && !isspace(c))
		return (0);
	/*
	* Concact the address according to
	* the number of parts specified.
	*/
	n = (int)(pp - parts + 1);
	switch (n) {

		case 0:
			return (0);             /* initial nondigit */

		case 1:                         /* a -- 32 bits */
			break;

		case 2:                         /* a.b -- 8.24 bits */
			if (val > 0xffffff)
				return (0);
			val |= parts[0] << 24;
			break;

		case 3:                         /* a.b.c -- 8.8.16 bits */
			if (val > 0xffff)
				return (0);
			val |= (parts[0] << 24) | (parts[1] << 16);
			break;

		case 4:                         /* a.b.c.d -- 8.8.8.8 bits */
			if (val > 0xff)
				return (0);
			val |= (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8);
			break;
	}

	val = htonl(val);
	return (val);
}

/*
 *	free linked list which members were allocated by exallocatepool
 *
 */
void KFreeList (IN PLIST_ENTRY l)
{
	void* p;

	while (!IsListEmpty(l))
	{
		p = RemoveHeadList (l);
		if (p)
			ExFreePool(p);
	}
}

NTSTATUS KEnforceMemoryProtectionDisabledRegistry ()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PKEY_VALUE_PARTIAL_INFORMATION	pValueInfo = NULL;
	UNICODE_STRING valuename;
	HANDLE key = NULL;
	ULONG valueread = 0;

	/// open key
	RtlInitUnicodeString(&valuename,L"\\REGISTRY\\MACHINE\\SYSTEM\\ControlSet001\\Control\\Session Manager\\Memory Management");
	Status = KCreateOpenKey(&valuename, NULL, OBJ_CASE_INSENSITIVE, &key, KEY_READ|KEY_WRITE,FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;
	
	/// read key value
	RtlInitUnicodeString(&valuename,L"EnforceWriteProtection");
	Status = KQueryValueKey(NULL,key,&valuename,&pValueInfo,NULL);
	if (!NT_SUCCESS (Status))
	{
		/// not present, set and reboot
		valueread = 0;
		Status = KSetValueKey(NULL,key,&valuename,&valueread,REG_DWORD,sizeof (ULONG));
		if (NT_SUCCESS (Status))
		{
			ZwFlushKey (key);
			ZwClose(key);
			/// BOOM!
			KeBugCheck(POWER_FAILURE_SIMULATE);
		}
	}

	/// check if its enabled
	memcpy (&valueread,&pValueInfo->Data,sizeof (ULONG));
	if (valueread != 0)
	{
		/// disable and reboot
		Status = KSetValueKey(NULL,key,&valuename,&valueread,REG_DWORD,sizeof (ULONG));
		if (NT_SUCCESS (Status))
		{
			ZwFlushKey (key);
			ZwClose(key);
			/// BOOM!
			KeBugCheck(POWER_FAILURE_SIMULATE);
		}
	}

__exit:
	/// if its enabled we can exit ....
	if (key)
		ZwClose(key);
	if (pValueInfo)
		ExFreePool(pValueInfo);

	return Status;

}

PIRP KAllocateCopyIrp(IN PIRP pSourceIrp, IN CCHAR StackSize, OPTIONAL PVOID pCompletionRoutine, OPTIONAL PVOID pContext)
{
	PIRP NewIrp = NULL;
	PIO_STACK_LOCATION	NewIrpNextSp = NULL;
	PIO_STACK_LOCATION	IrpSp = IoGetCurrentIrpStackLocation(pSourceIrp);

	if (!pSourceIrp || !StackSize)
		return NULL;

	/// allocate new irp
	NewIrp = IoAllocateIrp(StackSize + 1, FALSE);
	if (!NewIrp)
		return NULL;
	NewIrpNextSp = IoGetNextIrpStackLocation(NewIrp);

	/// set new completion routine
	if (pCompletionRoutine)
	{
		NewIrpNextSp->CompletionRoutine = pCompletionRoutine;
		NewIrpNextSp->Context = pContext;
	}

	/// initialize the new irp
	NewIrpNextSp->Control = IrpSp->Control;
	NewIrpNextSp->DeviceObject = IrpSp->DeviceObject;
	NewIrpNextSp->FileObject = IrpSp->FileObject;
	NewIrpNextSp->Flags = IrpSp->Flags;
	NewIrpNextSp->MajorFunction = IrpSp->MajorFunction;
	NewIrpNextSp->MinorFunction = IrpSp->MinorFunction;
	NewIrpNextSp->Parameters = IrpSp->Parameters;

	NewIrp->UserIosb = pSourceIrp->UserIosb;
	NewIrp->UserEvent = pSourceIrp->UserEvent;
	NewIrp->UserBuffer = pSourceIrp->UserBuffer;
	NewIrp->Flags = pSourceIrp->Flags;
	NewIrp->AssociatedIrp = pSourceIrp->AssociatedIrp;
	NewIrp->RequestorMode = pSourceIrp->RequestorMode;
	NewIrp->MdlAddress = pSourceIrp->MdlAddress;
	NewIrp->Tail.Overlay.Thread = pSourceIrp->Tail.Overlay.Thread;

	return NewIrp;
}
