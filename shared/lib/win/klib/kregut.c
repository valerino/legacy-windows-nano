/*
*	kernel utilities : registry
*  -xv-
*/

#include <ntddk.h>
#include <kstrut.h>
#include <kregut.h>
#include <strlib.h>
#include <ntstrsafe.h>

#define POOL_TAG 'turk'

/*
*	create or open a registry key
*
*/
NTSTATUS KCreateOpenKey (IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE parenthandle, OPTIONAL IN ULONG attributes, IN OUT HANDLE* keyhandle, IN ACCESS_MASK desiredaccess, IN BOOLEAN Create)
{
	HANDLE hRegKey = NULL;
	OBJECT_ATTRIBUTES	ObjectAttributes;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG disp = 0;

	if (!keyname || !keyhandle)
		return STATUS_INVALID_PARAMETER;
	*keyhandle = NULL;

	/// create or open key
	InitializeObjectAttributes(&ObjectAttributes, keyname, attributes, parenthandle, NULL);
	if (Create)
		Status = ZwCreateKey (&hRegKey,desiredaccess,&ObjectAttributes,0,NULL,REG_OPTION_NON_VOLATILE, &disp);
	else
		Status = ZwOpenKey(&hRegKey, desiredaccess, &ObjectAttributes);
	if (!NT_SUCCESS(Status))
		return Status;
	*keyhandle = hRegKey;
	
	return Status;
}

/*
*	returns a KEY_VALUE_PARTIAL_INFORMATION filled with key value. output buffer must be freed by ExFreePool
*
*/
NTSTATUS KQueryValueKey (OPTIONAL IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE keyhandle, IN PUNICODE_STRING valuename,
						 IN OUT PKEY_VALUE_PARTIAL_INFORMATION* buffer, OUT OPTIONAL PULONG outsize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	ULONG size = 0;
	HANDLE k = NULL;
	PKEY_VALUE_PARTIAL_INFORMATION buf = NULL;
	UNICODE_STRING defaultvalue;
	
	/// check params
	if (!keyname && !keyhandle)
		return STATUS_INVALID_PARAMETER;
	if (!buffer)
		return STATUS_INVALID_PARAMETER;
	*buffer = NULL;
	
	if (keyhandle)
	{
		k = keyhandle;
		goto __handleprovided;
	}

	/// open key
	Status = KCreateOpenKey(keyname, NULL, OBJ_CASE_INSENSITIVE, &k, KEY_READ, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	if (!valuename)
	{
		RtlInitUnicodeString(&defaultvalue,NULL);
		valuename = &defaultvalue;
	}
	/// query value
	Status = ZwQueryValueKey(k, valuename, KeyValuePartialInformation, buf, size, &size);
	if (Status == STATUS_BUFFER_OVERFLOW || Status == STATUS_BUFFER_TOO_SMALL)
	{
		/// allocate memory and reissue
		buf = ExAllocatePoolWithTag(PagedPool,size + 1,POOL_TAG);
		if (!buf)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		Status = ZwQueryValueKey(k, valuename, KeyValuePartialInformation, buf, size, &size);
		if (!NT_SUCCESS (Status))
		{
			ExFreePool(buf);
			goto __exit;
		}

		/// ok, return buffer and size
		*buffer = buf;
		if (outsize)
			*outsize = size;
	}

__exit:
	if (k && !keyhandle)
		ZwClose (k);

	return Status;
}

/*
*  set key value according to valuetype (same params as ZwSetValueKey)
*
*/
NTSTATUS KSetValueKey(OPTIONAL IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE keyhandle, IN PUNICODE_STRING valuename, 
					  IN void* valuedata, IN ULONG valuetype, IN ULONG datasize)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING defaultvalue;
	HANDLE k = NULL;

	/// check params
	if (!keyname && !keyhandle)
		return STATUS_INVALID_PARAMETER;
	if (!valuedata || !datasize)
		return STATUS_INVALID_PARAMETER;
	if (datasize == 0)
		return STATUS_INVALID_PARAMETER;

	if (keyhandle)
	{
		k = keyhandle;
		goto __handleprovided;
	}

	/// open key
	Status = KCreateOpenKey(keyname, NULL, OBJ_CASE_INSENSITIVE, &k, KEY_WRITE, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	if (!valuename)
	{
		RtlInitUnicodeString(&defaultvalue,NULL);
		valuename = &defaultvalue;
	}
	Status = ZwSetValueKey(k,valuename,0,valuetype,valuedata,datasize);

__exit:
	if (k && !keyhandle)
		ZwClose (k);
	return Status;
}

/*
*	delete a registry key (must be empty)
*
*/
NTSTATUS KDeleteKey (IN OPTIONAL PUNICODE_STRING keyname, IN OPTIONAL HANDLE keyhandle)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	HANDLE k = NULL;

	/// check if handle is provided
	if (keyhandle)
	{
		k = keyhandle;
		goto __handleprovided;
	}

	/// open key
	Status = KCreateOpenKey(keyname, NULL, OBJ_CASE_INSENSITIVE, &k, KEY_WRITE|DELETE, FALSE);
	if (!NT_SUCCESS(Status))
		goto __exit;

__handleprovided:
	/// delete key
	Status = ZwDeleteKey(k);

__exit:
	if (k && !keyhandle)
		ZwClose(k);
	return Status;
}

/*
*	delete a registry tree 
*
*/
NTSTATUS KDeleteKeyTree (IN PUNICODE_STRING startkey, IN OPTIONAL HANDLE parentkey)
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	static int numrecursion;
	static PKEY_BASIC_INFORMATION keybasicinfo;
	static PKEY_VALUE_BASIC_INFORMATION keyvalueinfo;
	ULONG idxvalue = 0;
	ULONG idx = 0;
	UNICODE_STRING newname;
	HANDLE k = NULL;
	ULONG len = 0;

	/// initialization on recursion 0
	if (numrecursion == 0)
	{
		keybasicinfo = ExAllocatePoolWithTag (PagedPool,1024*sizeof (WCHAR) + sizeof (KEY_BASIC_INFORMATION) + 1,POOL_TAG);
		if (!keybasicinfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
		keyvalueinfo = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR)+sizeof (KEY_VALUE_BASIC_INFORMATION) + 1, POOL_TAG);
		if (!keyvalueinfo)
		{
			Status = STATUS_INSUFFICIENT_RESOURCES;
			goto __exit;
		}
	}

	/// open key
	Status = KCreateOpenKey(startkey, parentkey, OBJ_CASE_INSENSITIVE, &k, KEY_ENUMERATE_SUB_KEYS|KEY_WRITE|DELETE, FALSE);
	if (!NT_SUCCESS(Status))
	{
		/// prevent open errors to stop scanning
		if (!NT_SUCCESS(Status))
			Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// enumerate subkeys
	while (Status == STATUS_SUCCESS)
	{
		Status = ZwEnumerateKey (k,idx,KeyBasicInformation,keybasicinfo,1024*sizeof (WCHAR) + sizeof (KEY_BASIC_INFORMATION),&len);
		if (!NT_SUCCESS (Status))
		{
			if (Status == STATUS_NO_MORE_ENTRIES)
			{
				/// no subkeys, delete all values in this key
				Status = STATUS_SUCCESS;
				while (Status == STATUS_SUCCESS)
				{
					Status = ZwEnumerateValueKey(k,idxvalue,KeyValueBasicInformation,keyvalueinfo,1024*sizeof (WCHAR)+sizeof (KEY_VALUE_BASIC_INFORMATION),&len);
					if (!NT_SUCCESS (Status))
					{
						/// no more entries ?
						if (Status == STATUS_NO_MORE_ENTRIES)
						{
							/// delete key
							Status = KDeleteKey(NULL,k);
							Status = STATUS_SUCCESS;
						}
						break;
					}

					/// build name and delete value
					newname.Buffer = keyvalueinfo->Name;
					newname.Length = (USHORT)keyvalueinfo->NameLength;
					newname.MaximumLength = (USHORT)keyvalueinfo->NameLength;
					Status = ZwDeleteValueKey(k,&newname);
					if (!NT_SUCCESS (Status))
						break;

					idxvalue++;
				}
			}
			break;
		}
		/// recurse
		numrecursion++;
		newname.Buffer = keybasicinfo->Name;
		newname.Length = (USHORT)keybasicinfo->NameLength;
		newname.MaximumLength = (USHORT)keybasicinfo->NameLength;
		Status = KDeleteKeyTree (&newname,k);
		numrecursion--;
		idx++;
	}
	
__exit:
	if (k)
		ZwClose(k);
	if (numrecursion == 0)
	{
		/// free the static buffers
		if (keybasicinfo)
			ExFreePool(keybasicinfo);
		if (keyvalueinfo)
			ExFreePool(keyvalueinfo);
	}
	return Status;
}

/*
*	add a specified class filter under the specified class name GUID(for WDM filters)
*  
*/
NTSTATUS KAddClassFilter (IN PWCHAR classname, IN PWCHAR filtername, IN ULONG filtertype)
{
	HANDLE hKey = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING valuename;
	UNICODE_STRING fullname;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyInfo = NULL;
	PWCHAR tmp = NULL;
	PWCHAR newbuf = NULL;
	WCHAR buffer [1024];
	ULONG newsize = 0;
	
	/// check params
	if (!classname || !filtername)
		return STATUS_INVALID_PARAMETER;
	if ((filtertype != UPPER_FILTER) && (filtertype != LOWER_FILTER))
		return STATUS_INVALID_PARAMETER;

	/// open key
	fullname.Buffer = buffer;
	fullname.Length = 0;
	fullname.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf(&fullname,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Class\\%s",classname);
	Status = KCreateOpenKey (&fullname, NULL, OBJ_CASE_INSENSITIVE, &hKey, KEY_READ|KEY_WRITE, FALSE);
	if (!hKey)
		goto __exit;

	/// read values for filter type
	if (filtertype == UPPER_FILTER)
		RtlInitUnicodeString(&valuename,L"UpperFilters");
	else
		RtlInitUnicodeString(&valuename,L"LowerFilters");

	Status = KQueryValueKey(NULL,hKey,&valuename,&pKeyInfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// is our value already in ?
	if (find_buffer_in_buffer (pKeyInfo->Data, filtername, pKeyInfo->DataLength, str_lenbytesw(filtername), FALSE))
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// no, allocate memory for the new buffer
	newsize = pKeyInfo->DataLength + str_lenbytesw(filtername) + sizeof (WCHAR);
	newbuf = ExAllocatePoolWithTag(PagedPool,newsize,POOL_TAG);
	if (!newbuf)
		goto __exit;
	memset (newbuf,0,newsize);
	memcpy (newbuf,pKeyInfo->Data,pKeyInfo->DataLength);

	/// add our filter in the end
	tmp = newbuf;
	while (*tmp)
		tmp += (wcslen (tmp) + 1);
	RtlStringCbCopyW (tmp,newsize,filtername);

	/// write value back
	Status = KSetValueKey(NULL,hKey,&valuename, newbuf, REG_MULTI_SZ, newsize);
	
__exit:
	/// cleanup
	if (pKeyInfo)
		ExFreePool (pKeyInfo);
	if (newbuf)
		ExFreePool(newbuf);
	if (hKey)
		ZwClose (hKey);
	return Status;
}

/*
*  remove a specified class filter under the specified class name GUID (for WDM filters)
*  
*/
NTSTATUS KRemoveClassFilter (IN PWCHAR classname, IN PWCHAR filtername, IN ULONG filtertype)
{
	HANDLE hKey = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING valuename;
	UNICODE_STRING fullname;
	PKEY_VALUE_PARTIAL_INFORMATION pKeyInfo = NULL;
	PWCHAR tmpsrc = NULL;
	PWCHAR tmpdst = NULL;
	PWCHAR newbuf = NULL;
	WCHAR buffer [1024];
	ULONG newsize = 0;
	ULONG size = 0;
	
	/// check params
	if (!classname || !filtername)
		return STATUS_INVALID_PARAMETER;
	if ((filtertype != UPPER_FILTER) && (filtertype != LOWER_FILTER))
		return STATUS_INVALID_PARAMETER;

	/// open key
	fullname.Buffer = buffer;
	fullname.Length = 0;
	fullname.MaximumLength = sizeof (buffer);
	RtlUnicodeStringPrintf(&fullname,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Class\\%s",classname);
	Status = KCreateOpenKey (&fullname, NULL, OBJ_CASE_INSENSITIVE, &hKey, KEY_READ|KEY_WRITE, FALSE);
	if (!hKey)
		goto __exit;

	/// read values for filter type
	if (filtertype == UPPER_FILTER)
		RtlInitUnicodeString(&valuename,L"UpperFilters");
	else
		RtlInitUnicodeString(&valuename,L"LowerFilters");

	Status = KQueryValueKey(NULL,hKey,&valuename,&pKeyInfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// is our value in ?
	if (!find_buffer_in_buffer (pKeyInfo->Data, filtername, pKeyInfo->DataLength, str_lenbytesw(filtername), FALSE))
	{
		Status = STATUS_SUCCESS;
		goto __exit;
	}

	/// yes, allocate memory for the new buffer
	newbuf = ExAllocatePoolWithTag(PagedPool,pKeyInfo->DataLength,POOL_TAG);
	if (!newbuf)
		goto __exit;
	memset (newbuf,0,pKeyInfo->DataLength);
	tmpsrc = (PWCHAR)pKeyInfo->Data;
	tmpdst = newbuf;
	newsize = 0;
	
	/// copy all values except our
	while (*tmpsrc)
	{
		size = wcslen(tmpsrc) + 1;
		if (_wcsnicmp (tmpsrc,filtername,wcslen(filtername) != 0))
		{
			/// copy this value
			RtlStringCbCopyW (tmpdst,pKeyInfo->DataLength,tmpsrc);
			tmpdst = tmpdst + size;
			newsize+=(size*sizeof (WCHAR));
		}
		tmpsrc = tmpsrc + size;
	}	

	/// write value back
	Status = KSetValueKey(NULL,hKey,&valuename, newbuf, REG_MULTI_SZ, newsize);

__exit:
	/// cleanup
	if (pKeyInfo)
		ExFreePool (pKeyInfo);
	if (newbuf)
		ExFreePool(newbuf);
	if (hKey)
		ZwClose (hKey);

	return Status;
}

/*
*	add protocol binding
*
*/
NTSTATUS KAddProtocolBinding (IN PWCHAR drivername)
{
	HANDLE hadapters = NULL;
	HANDLE h = NULL;
	HANDLE htcp = NULL;
	HANDLE hfilter = NULL;
	ULONG idx = 0;
	PWCHAR p = NULL;
	PWCHAR q = NULL;
	PWCHAR buffer = NULL;
	PWCHAR copybuffer = NULL;
	ULONG len = 0;
	BOOLEAN found = FALSE;
	ULONG offset = 0;
	ULONG offsetpre1 = 0;
	ULONG offsetpre2 = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PKEY_VALUE_PARTIAL_INFORMATION keyinfo = NULL;
	PKEY_BASIC_INFORMATION keybasicinfo = NULL;

	/// allocate memory
	buffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
	copybuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
	keybasicinfo = ExAllocatePoolWithTag(PagedPool, sizeof (KEY_BASIC_INFORMATION) + 1024*sizeof(WCHAR) + 1,POOL_TAG);
	if (!copybuffer || !buffer || !keybasicinfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	/// copy bindings from tcpip driver
	RtlInitUnicodeString(&name,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Linkage");
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &htcp, KEY_READ, FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = 1024*sizeof (WCHAR);
	RtlUnicodeStringPrintf(&name,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services\\%s\\Linkage",drivername);
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &hfilter, KEY_READ|KEY_WRITE, TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	RtlInitUnicodeString(&name,L"Bind");
	Status = KQueryValueKey(NULL,htcp,&name,&keyinfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = KSetValueKey(NULL,hfilter,&name,keyinfo->Data,REG_MULTI_SZ,keyinfo->DataLength);
	if (!NT_SUCCESS (Status))
		goto __exit;
	ExFreePool(keyinfo);keyinfo = NULL;

	RtlInitUnicodeString(&name,L"Route");
	Status = KQueryValueKey(NULL,htcp,&name,&keyinfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;
	Status = KSetValueKey(NULL,hfilter,&name,keyinfo->Data,REG_MULTI_SZ,keyinfo->DataLength);
	if (!NT_SUCCESS (Status))
		goto __exit;
	ExFreePool(keyinfo);keyinfo = NULL;

	RtlInitUnicodeString(&name,L"Export");
	Status = KQueryValueKey(NULL,htcp,&name,&keyinfo,NULL);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// generate unique name for the devices
	memset (copybuffer,0,1024*sizeof (WCHAR));
	p = (PWCHAR)keyinfo->Data;
	q = (PWCHAR)copybuffer;
	RtlStringCbPrintfW (copybuffer,1024*sizeof (WCHAR), L"\\Device\\%S_" ,drivername);
	offsetpre1 = wcslen(L"\\Device\\tcpip_");
	offsetpre2 = wcslen(copybuffer);
	offset = 0;
	len = 0;
	while (*p != (WCHAR)'\0')
	{
		memcpy (q + offsetpre2,p + offsetpre1,str_lenbytesw (p + offsetpre1));
		p+=(wcslen (p) + 1);
		q+=(wcslen (q) + 1);
		len += (str_lenbytesw(q)+sizeof(WCHAR));
		RtlStringCbPrintfW (q,(1024*sizeof (WCHAR) - len), L"\\Device\\%S_" ,drivername);
	}
	len+=sizeof (WCHAR);
	RtlInitUnicodeString(&name,L"Export");
	Status = KSetValueKey(NULL,hfilter,&name,copybuffer,REG_MULTI_SZ,len);
	if (!NT_SUCCESS (Status))
		goto __exit;
	ExFreePool(keyinfo);keyinfo = NULL;

	/// add protocol to the adapter bindings
	RtlInitUnicodeString(&name,L"\\REGISTRY\\MACHINE\\SYSTEM\\SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}");
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &hadapters, KEY_READ, FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = ZwEnumerateKey (hadapters, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 1024 * sizeof (WCHAR), &len);
	if (Status != STATUS_SUCCESS)
		goto __exit;
	while (Status == STATUS_SUCCESS)
	{
		/// get linkage subkey
		memset (buffer,0,1024*sizeof (WCHAR));
		memcpy (buffer,keybasicinfo->Name,keybasicinfo->NameLength);
		RtlStringCbCatW(buffer,1024*sizeof (WCHAR),L"\\Linkage");
		name.Buffer = buffer;
		name.Length = (USHORT)str_lenbytesw(buffer);
		name.MaximumLength = 1024*sizeof (WCHAR);
		h=NULL;
		Status = KCreateOpenKey(&name, hadapters, OBJ_CASE_INSENSITIVE, &h, KEY_READ|KEY_WRITE, FALSE);
		if (!NT_SUCCESS (Status))
		{
			Status = STATUS_SUCCESS;
			goto __next;
		}
	
		RtlInitUnicodeString(&name,L"UpperBind");
		Status = KQueryValueKey(NULL,h,&name,&keyinfo,NULL);
		if (!NT_SUCCESS (Status))
		{
			Status = STATUS_SUCCESS;
			goto __next;
		}

		/// check if its the right adapter to bind on, and check that we're not bound yet
		p = (PWCHAR)keyinfo->Data;
		found = FALSE;
		while (*p != (WCHAR)'\0')
		{
			if (_wcsicmp (p,drivername) == 0)
			{
				found = TRUE;
				break;
			}
			p+=(wcslen (p) + 1);
		}
		if (found)
			goto __next;

		p = (PWCHAR)keyinfo->Data;
		found = FALSE;
		while (*p != (WCHAR)'\0')
		{
			if (_wcsicmp (p,L"ndisuio") == 0)
			{
				found = TRUE;
				break;
			}
			p+=(wcslen (p) + 1);
		}

		/// copy our driver name in case, and update value
		if (found)
		{
			memset (copybuffer,0,1024*sizeof (WCHAR));
			RtlStringCbCopyW (copybuffer,1024*sizeof (WCHAR),drivername);
			p = copybuffer + wcslen (copybuffer) + 1;
			memcpy (p,keyinfo->Data,keyinfo->DataLength);
			len = keyinfo->DataLength + str_lenbytesw(drivername) + sizeof (WCHAR);
			Status = KSetValueKey(NULL,h,&name,copybuffer,REG_MULTI_SZ,len);
		}

__next:
		if (h)
			ZwClose(h);
		if (keyinfo)
		{
			ExFreePool(keyinfo);
			keyinfo = NULL;
		}
		idx++;
	}

__exit:
	if (hfilter)
		ZwClose(hfilter);
	if (htcp)
		ZwClose(htcp);
	if (hadapters)
		ZwClose(hadapters);
	if (keybasicinfo)
		ExFreePool(keybasicinfo);
	if (keyinfo)
		ExFreePool(keyinfo);
	if (buffer)
		ExFreePool(buffer);
	if (copybuffer)
		ExFreePool(copybuffer);
	return Status;
}

/*
*	remove protocol binding
*
*/
NTSTATUS KRemoveProtocolBinding (IN PWCHAR drivername)
{
	HANDLE hadapters = NULL;
	HANDLE h = NULL;
	ULONG idx = 0;
	PWCHAR p = NULL;
	PWCHAR q = NULL;
	PWCHAR buffer = NULL;
	PWCHAR copybuffer = NULL;
	ULONG len = 0;
	BOOLEAN found = FALSE;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	UNICODE_STRING name;
	PKEY_VALUE_PARTIAL_INFORMATION keyinfo = NULL;
	PKEY_BASIC_INFORMATION keybasicinfo = NULL;

	/// allocate memory
	buffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
	copybuffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1,POOL_TAG);
	keybasicinfo = ExAllocatePoolWithTag(PagedPool, sizeof (KEY_BASIC_INFORMATION) + 1024*sizeof(WCHAR) + 1,POOL_TAG);
	if (!copybuffer || !buffer || !keybasicinfo)
	{
		Status = STATUS_INSUFFICIENT_RESOURCES;
		goto __exit;
	}

	RtlInitUnicodeString(&name,L"\\REGISTRY\\MACHINE\\SYSTEM\\SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}");
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &hadapters, KEY_READ, FALSE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	Status = ZwEnumerateKey (hadapters, idx, KeyBasicInformation, keybasicinfo, sizeof (KEY_BASIC_INFORMATION) + 1024 * sizeof (WCHAR), &len);
	if (Status != STATUS_SUCCESS)
		goto __exit;
	while (Status == STATUS_SUCCESS)
	{
		/// get linkage subkey
		memset (buffer,0,1024*sizeof (WCHAR));
		memcpy (buffer,keybasicinfo->Name,keybasicinfo->NameLength);
		RtlStringCbCatW(buffer,1024*sizeof (WCHAR),L"\\Linkage");
		name.Buffer = buffer;
		name.Length = (USHORT)str_lenbytesw(buffer);
		name.MaximumLength = 1024*sizeof (WCHAR);
		h=NULL;
		Status = KCreateOpenKey(&name, hadapters, OBJ_CASE_INSENSITIVE, &h, KEY_READ|KEY_WRITE, FALSE);
		if (!NT_SUCCESS (Status))
		{
			Status = STATUS_SUCCESS;
			goto __next;
		}

		RtlInitUnicodeString(&name,L"UpperBind");
		Status = KQueryValueKey(NULL,h,&name,&keyinfo,NULL);
		if (!NT_SUCCESS (Status))
		{
			Status = STATUS_SUCCESS;
			goto __next;
		}

		/// check if we find our driver
		p = (PWCHAR)keyinfo->Data;
		q = (PWCHAR)copybuffer;
		memset (copybuffer,0,1024*sizeof (WCHAR));
		len = 0;
		found = FALSE;
		while (*p != (WCHAR)'\0')
		{
			if (_wcsicmp (p,drivername) != 0)
			{
				q+=wcslen (p) + 1;
				len +=wcslen (p) + 1;
				found = TRUE;
			}
			p+=(wcslen (p) + 1);
		}

		/// update value if found
		if (found)
		{
			len+=sizeof(WCHAR);
			Status = KSetValueKey(NULL,h,&name,copybuffer,REG_MULTI_SZ,len);
		}

__next:
		if (h)
			ZwClose(h);
		if (keyinfo)
		{
			ExFreePool(keyinfo);
			keyinfo = NULL;
		}
		idx++;
	}

__exit:
	if (hadapters)
		ZwClose(hadapters);
	if (keybasicinfo)
		ExFreePool(keybasicinfo);
	if (keyinfo)
		ExFreePool(keyinfo);
	if (buffer)
		ExFreePool(buffer);
	if (copybuffer)
		ExFreePool(copybuffer);
	return Status;
}

/*
*	add minifilter
*
*/
NTSTATUS KAddMiniFilter (IN PWCHAR drivername, IN PWCHAR altitude)
{
	HANDLE hkey = NULL;
	HANDLE hsubkey = NULL;
	UNICODE_STRING name;
	PWCHAR buffer = NULL;
	WCHAR instancename [64];
	ULONG value = 0;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;

	buffer = ExAllocatePoolWithTag(PagedPool,1024*sizeof (WCHAR) + 1, POOL_TAG);
	if (!buffer)
		return STATUS_INSUFFICIENT_RESOURCES;
	memset (buffer,0,1024*sizeof (WCHAR));

	/// create instances key
	name.Buffer = buffer;
	name.Length = 0;
	name.MaximumLength = 1024*sizeof (WCHAR);
	RtlUnicodeStringPrintf(&name,L"\\REGISTRY\\MACHINE\\SYSTEM\\CurrentControlSet\\Services\\%s\\Instances",drivername);
	Status = KCreateOpenKey(&name, NULL, OBJ_CASE_INSENSITIVE, &hkey, KEY_READ|KEY_WRITE, TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// set default instance
	RtlStringCbPrintfW(instancename,sizeof (instancename),L"%S_Instance",drivername);
	RtlInitUnicodeString(&name,L"DefaultInstance");
	Status = KSetValueKey(NULL,hkey,&name,instancename,REG_SZ,str_lenbytesw(instancename) + sizeof (WCHAR));
	if (!NT_SUCCESS (Status))
		goto __exit;

	/// create subkey
	RtlInitUnicodeString(&name,instancename);
	Status = KCreateOpenKey(&name, hkey, OBJ_CASE_INSENSITIVE, &hsubkey, KEY_READ|KEY_WRITE, TRUE);
	if (!NT_SUCCESS (Status))
		goto __exit;

	value = 0;
	RtlInitUnicodeString(&name,L"Flags");
	Status = KSetValueKey(NULL,hsubkey,&name,&value,REG_DWORD,sizeof (ULONG));
	if (!NT_SUCCESS (Status))
		goto __exit;

	RtlInitUnicodeString(&name,L"Altitude");
	Status = KSetValueKey(NULL,hsubkey,&name,altitude,REG_SZ,str_lenbytesw(altitude) + sizeof (WCHAR));
	
__exit:
	if (hkey)
		ZwClose(hkey);
	if (hsubkey)
		ZwClose(hsubkey);
	if (buffer)
		ExFreePool(buffer);
	return Status;
}
