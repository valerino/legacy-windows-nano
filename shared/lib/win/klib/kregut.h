#ifndef __kregut_h__
#define __kregut_h__

/*
*	many of these functions accept registry key both as handle and name
*
*/

/*
 *	create or open a registry key
 *
 */
NTSTATUS KCreateOpenKey (IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE parenthandle, OPTIONAL IN ULONG attributes, IN OUT HANDLE* keyhandle, IN ACCESS_MASK desiredaccess, IN BOOLEAN Create);

/*
 *	returns a KEY_VALUE_PARTIAL_INFORMATION filled with key value. output buffer must be freed by ExFreePool
 *
 */
NTSTATUS KQueryValueKey (OPTIONAL IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE keyhandle, IN PUNICODE_STRING valuename,
						 IN OUT PKEY_VALUE_PARTIAL_INFORMATION* buffer, OUT OPTIONAL PULONG outsize);


/*
 *  set key value according to valuetype (same params as ZwSetValueKey)
 *
 */
NTSTATUS KSetValueKey(OPTIONAL IN PUNICODE_STRING keyname, OPTIONAL IN HANDLE keyhandle, IN PUNICODE_STRING valuename, 
					  IN void* valuedata, IN ULONG valuetype, IN ULONG datasize);

/*
 *	delete a registry key (must be empty)
 *
 */
NTSTATUS KDeleteKey (IN OPTIONAL PUNICODE_STRING keyname, IN OPTIONAL HANDLE keyhandle);

/*
*	delete a registry tree 
*
*/
NTSTATUS KDeleteKeyTree (IN PUNICODE_STRING startkey, IN OPTIONAL HANDLE parentkey);

#define UPPER_FILTER	1
#define LOWER_FILTER	2

/*
 *	add a specified class filter under the specified class name GUID(for WDM filters)
 *  
 */
NTSTATUS KAddClassFilter (IN PWCHAR classname, IN PWCHAR filtername, IN ULONG filtertype);

/*
*  remove a specified class filter under the specified class name GUID (for WDM filters)
*  
*/
NTSTATUS KRemoveClassFilter (IN PWCHAR classname, IN PWCHAR filtername, IN ULONG filtertype);

/*
*	add protocol binding
*
*/
NTSTATUS KAddProtocolBinding (IN PWCHAR drivername);

/*
*	delete protocol binding
*
*/
NTSTATUS KRemoveProtocolBinding (IN PWCHAR drivername);

/*
*	add minifilter
*
*/
NTSTATUS KAddMiniFilter (IN PWCHAR drivername, IN PWCHAR altitude);

#endif /// #ifndef __kregut_h__