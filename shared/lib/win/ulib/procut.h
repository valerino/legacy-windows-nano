#ifndef __procut_h__
#define __procut_h__

#include <winsock2.h>
#include <lists.h>
#include <TlHelp32.h>

/*
 *	from psapi.h
 *
 */
BOOL
WINAPI
EnumProcessModules(
				   __in  HANDLE hProcess,
				   __out_bcount(cb) HMODULE *lphModule,
				   __in  DWORD cb,
				   __out LPDWORD lpcbNeeded
				   );
DWORD
WINAPI
GetModuleFileNameExW(
					 __in HANDLE hProcess,
					 __in_opt HMODULE hModule,
					 __out_ecount(nSize) LPWSTR lpFilename,
					 __in DWORD nSize
					 );

typedef struct procentry {
	LIST_ENTRY chain;
	PROCESSENTRY32 processentry;
} procentry;

typedef LONG NTSTATUS, *PNTSTATUS;

typedef struct _CLIENT_ID {
	HANDLE UniqueProcess;
	HANDLE UniqueThread;
} CLIENT_ID;
typedef CLIENT_ID *PCLIENT_ID;

/*
*	set privileges on the current process
*
*/
int proc_setcurrentprocess_privileges(IN LPCTSTR privilegename);

/*
*	detach console attachment done with proc_win32consoleoutput_attach
*
*/
void proc_win32consoleoutput_detach();

/*
*	permits to a win32 application to output on console if called from cmd.exe. returns 0 if succeeds
*
*/
int proc_win32consoleoutput_attach();

/*
*	missing implementation from the win32api .... pass GetCommandLine() for the 1st param, returns argv. returned array must be freed with globalfree
*
*/
PCHAR* proc_CommandLineToArgvA(PCHAR CmdLine, int* _argc);

/*
*	check if the current process has administrator privileges, either on vista or xp (on vista, UAC presence is handled)
*
*/
BOOL proc_hasadminprivileges ();

/*
*	returns elevation type of the current process. always return -1 on non-vista
*
*/
int proc_getelevationtype (OUT TOKEN_ELEVATION_TYPE* ptet);

/*
*	returns TRUE if running windows xp or later
*
*/
BOOL proc_iswxpos ();

/*
*	returns TRUE if running on vista or later
*
*/
BOOL proc_isvistaos ();

/*
*	returns TRUE if the user belongs to the administrators groups. BEWARE : on vista + UAC, process running with admin user != process elevated!
*  -1 is retuned on error
*/
BOOL proc_isuseradmin ();

/*
*	returns TRUE if the os is windows vista AND the process is elevated. optionally, it returns TRUE in isvista if process is running on vista
*  returns -1 on error
*/
int proc_isvistaelevated (OPTIONAL OUT PBOOL isvista);

/*
*	verify if the current os meets the requirements (major/minor version, major/minor servicepack version). normally the check succeeds if running os
*	version is greater-than specified. returns TRUE if check is met.
*/
BOOL proc_verifyosversion(IN int majorversion, OPTIONAL IN int minorversion, OPTIONAL IN int spmajor, OPTIONAL IN int spminor, OPTIONAL IN int condition);

/*
*	free list allocated with proc_getprocesseslist
*
*/
void proc_freeprocesseslist (IN LIST_ENTRY* processes);

/*
*	get list of running processes using toolhelp32 functions
*
*/
int proc_getprocesseslist (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses);

/*
*	returns process pid from name/path
*
*/
DWORD proc_getpidbyname (IN char* processname, IN BOOL issubstring);

/*
*	set/unset specified privilege on current token
*
*/
int proc_setprivilege (IN HANDLE hToken, IN LPCTSTR lpszPrivilege, IN BOOL enable);

/*
*	createremotethread replacement (uses an undocumented function to let it work on vista, calls standard api on xp/2k. *experimental*
*
*/
HANDLE proc_createremotethread (HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress,
								LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);

/*
*	inject dll (ansi) into process via CreateRemoteThread/LoadLibrary calls. if timeout is specified, timeout (seconds) is waited before injection
*
*/
int proc_inject_dll (IN ULONG_PTR pid, IN char* dllpath, IN int timeout);

/*
*	inject dll (unicode) into process via CreateRemoteThread/LoadLibrary calls. if timeout is specified, timeout (seconds) is waited before injection
*
*/
int proc_inject_dllw (IN ULONG_PTR pid, IN PWCHAR dllpath, IN int timeout);

/*
*	set null (everyone) dacl on security attributes and descriptor
*
*/
int proc_initializenulldacl (IN PSECURITY_ATTRIBUTES attribs, IN PSECURITY_DESCRIPTOR desc);

/* 
*	get pModuleName full path (pass NULL for current process) (unicode). returns number of wchars copied.
*
*/
int proc_wgetmodulepath (OPTIONAL IN PWCHAR pModuleName, OUT PWCHAR outpath, IN int outpathwsize);

/* 
*	get pModuleName full path (pass NULL for current process). returns number of chars copied.
*
*/
int proc_getmodulepath (OPTIONAL IN PCHAR pModuleName, OUT PCHAR outpath, IN int outpathsize);

/* 
*	get pModuleName name (just bare name) (pass NULL for current process). returns number of chars copied.
*
*/
int proc_getmodulename (OPTIONAL IN PCHAR pModuleName, OUT PCHAR outname, IN int outnamesize);

/* 
*	get pModuleName name (just bare name) (pass NULL for current process) (unicode). returns number of wchars copied.
*
*/
int proc_wgetmodulename (OPTIONAL IN PWCHAR pModuleName, OUT PWCHAR outname, IN int outnamewsize);

/*
*	execute process specified by path, optionally waits for termination returns. exitcode if wait is specified
*
*/
ULONG proc_execw (IN PWCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide);

/*
*	execute process specified by path, optionally waits for termination (ansi). returns exitcode if wait is specified
*
*/
ULONG proc_exec (IN PCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide);

/*
*	execute process specified by path in the current user session, optionally waits for termination. returns exitcode if wait is specified
*
*/
ULONG proc_exec_as_logged_userw (IN PWCHAR path, OPTIONAL IN int wait, OPTIONAL int hide);

/*
*	execute process specified by path in the current user session, optionally waits for termination(ansi). returns exitcode if wait is specified
*
*/
ULONG  proc_exec_as_logged_user (IN PCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide);

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_namew (IN PWCHAR processname, IN BOOL killallinstances);

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_name (IN PCHAR processname, IN BOOL killallinstances);

/*
*	kill process knowing pid. may fail if the process can't be opened for termination
*
*/
int proc_kill_by_pid (IN ULONG pid);

/*
*	detect if we're running on a full 64bit process.
*
*/
BOOL proc_is64bit();

/*
*	detect if we're running on a WOW64 process (32bit process on 64bit OS). if it returns false, its a 64bit application running on 64bit OS
*
*/
BOOL proc_is64bitWOW(IN HANDLE process);

/*
*	detect if the specified process is a WOW64 process (32bit process on 64bit OS) by pid. if it returns false, its a 64bit process running on 64bit OS
*
*/
BOOL proc_is64bitWOWbypid(IN ULONG_PTR pid);

/*
*	detect if we're running on a 64bit os
*
*/
BOOL proc_is64bitOS();

/*
*	get function address in specified library (unicode). phModule must be freed with FreeLibrary by the caller if != NULL on return
*
*/
PVOID proc_getprocaddressw (IN PWCHAR pDllName, IN PCHAR pFunctionName, OUT HMODULE* phModule);

/*
*	get function address in specified library. phModule must be freed with FreeLibrary by the caller if != NULL on return
*
*/
PVOID proc_getprocaddress (IN PCHAR pDllName, IN PCHAR pFunctionName, OUT HMODULE* phModule);

/* 
*	get name (bare name only) of the module which has focus. returns number of wchars copied.
*
*/
int proc_getmodulename_foregroundw (OUT PWCHAR outname, IN int outnamesize);

/* 
*	get name (bare name only) of the module which has focus. returns number of chars copied.
*
*/
int proc_getmodulename_foreground (OUT PCHAR outname, IN int outnamesize);

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted. unicode
*
*/
int proc_getnamebypidw (IN DWORD pid, OUT PWCHAR processname, IN int namelen, IN int nameonly);

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted
*
*/
int proc_getnamebypid (IN DWORD pid, OUT PCHAR processname, IN int namelen, IN int nameonly);

/*
*	check if a module is present in the specified process. module name can be barename or fullpath
*
*/
int proc_is_module_presentw (IN HANDLE process, IN PWCHAR modulename);

/*
*	check if a module is present in the specified process. module name can be barename or fullpath
*
*/
int proc_is_module_present (IN HANDLE process, IN PCHAR modulename);

/*
*	returns number of injected processes
*
*/
int proc_injectprocesses (IN char* processname, IN BOOL issubstring, IN char* dllpath, OPTIONAL IN int timeout);

#endif