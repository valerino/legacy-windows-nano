/*
 *	generic utilities
 *	-vx-
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strsafe.h"
#include <dbg.h>

/*
 *	wait for specified timeout on object, and returns true when signaled
 *
 */
BOOL obj_wait (IN HANDLE hObject, IN ULONG Timeout)
{
	// check params
	if (!hObject)
		return FALSE;

	// wait for the specified timeout
	if (WaitForSingleObject(hObject,Timeout) == WAIT_OBJECT_0)
		return TRUE;
	return FALSE;

}

/*
 *	returns true if the object is signaled
 *
 */
BOOL obj_testsignaled (IN HANDLE hObject)
{
	// check params
	if (!hObject)
		return FALSE;

	// test signaled
	return obj_wait(hObject,0);
}

/*
*	query a registry value
*
*/
int reg_queryvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size)
{
	HKEY key = NULL;
	int res = -1;
	ULONG valuetype = 0;
	ULONG sizebuffer = size;

	if (!parentkey || !valuename || !outbuffer || !size || !subkey)
		return -1;

	if (RegOpenKeyExW ((HKEY)parentkey, subkey, 0, KEY_QUERY_VALUE, &key) != ERROR_SUCCESS)
		return -1;
	
	if (RegQueryValueExW(key,valuename,0,&valuetype,outbuffer,&sizebuffer) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	query a registry value
*
*/
int reg_queryvalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size)
{
	size_t numchar = 0;
	WCHAR wsubkey [1024];
	WCHAR wvaluename[MAX_PATH];

	if (!parentkey || !valuename || !outbuffer || !size || !subkey)
		return -1;
	mbstowcs_s(&numchar,wsubkey,1024,subkey,_TRUNCATE);
	mbstowcs_s(&numchar,wvaluename,MAX_PATH,valuename,_TRUNCATE);
	return reg_queryvaluew(parentkey,wsubkey,wvaluename,outbuffer,size);
}

/*
 *	set a registry value
 *
 */
int reg_setvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size)
{
	HKEY key = NULL;
	int res = -1;

	if (!parentkey || valuetype || !buffer || !size || !subkey)
		return -1;

	if (RegOpenKeyExW ((HKEY)parentkey, subkey, 0, KEY_SET_VALUE, &key) != ERROR_SUCCESS)
		return -1;
	if (RegSetValueExW (key,valuename,0,valuetype,buffer,size) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	set a registry value
*
*/
int reg_setvalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size)
{
	size_t numchar = 0;
	WCHAR wsubkey [1024];
	WCHAR wvaluename[MAX_PATH];
	
	if (!parentkey || valuetype || !buffer || !size || !subkey)
		return -1;
	mbstowcs_s(&numchar,wsubkey,1024,subkey,_TRUNCATE);
	mbstowcs_s(&numchar,wvaluename,MAX_PATH,valuename,_TRUNCATE);
	return reg_setvaluew(parentkey,wsubkey,wvaluename,valuetype,buffer,size);
}

/*
*	delete a registry value
*
*/
int reg_deletevaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename)
{
	HKEY key = NULL;
	int res = -1;

	if (!parentkey  || !subkey)
		return -1;

	if (RegOpenKeyExW ((HKEY)parentkey, subkey, 0, KEY_SET_VALUE, &key) != ERROR_SUCCESS)
		return -1;
	if (RegDeleteValueW (key,valuename) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	delete a registry value
*
*/
int reg_deletevalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename)
{
	size_t numchar = 0;
	WCHAR wsubkey [1024];
	WCHAR wvaluename[MAX_PATH];

	if (!parentkey || !subkey)
		return -1;
	mbstowcs_s(&numchar,wsubkey,1024,subkey,_TRUNCATE);
	mbstowcs_s(&numchar,wvaluename,MAX_PATH,valuename,_TRUNCATE);
	return reg_deletevaluew(parentkey,wsubkey,wvaluename);
}

/*
*	get localtime into a large integer
*
*/
void get_localtime_as_largeinteger (OUT PLARGE_INTEGER localtime)
{
	FILETIME ft;
	FILETIME localft;

	GetSystemTimeAsFileTime(&ft);
	FileTimeToLocalFileTime(&ft,&localft);
	
	if (localtime)
	{
		localtime->HighPart = (long)localft.dwHighDateTime;
		localtime->LowPart = localft.dwLowDateTime;
	}	
	
	return ;
}
