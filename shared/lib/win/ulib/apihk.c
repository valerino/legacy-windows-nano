//************************************************************************
// apihk.c
//
// generic win32/x64 usermode apihook implementation
// -vx-
// 
// generic apihook functions using distorm disassembler and trampolines. 
// note : currently not working for hooking functions with relative jmps and calls in the beginning (needs relocation) 
//************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include "apihk.h"
#include "procut.h"
#include <dbg.h>
#include <distorm.h>

/*
 *	Skip over jumps that lead to the real function. Gets around import, jump tables, etc.
 *
 */
PBYTE SkipJumps(PBYTE pbCode, int is32bit) 
{
	if (pbCode[0] == 0xff && pbCode[1] == 0x25) 
	{
		if (is32bit)
		{
			// on x86 we have an absolute pointer...
			PBYTE pbTarget = *(PBYTE *)&pbCode[2];
			// ... that shows us an absolute pointer.
			return SkipJumps(*(PBYTE *)pbTarget, is32bit);
		}
		else
		{
			// on x64 we have a 32-bit offset...
			INT32 lOffset = *(INT32 *)&pbCode[2];
			// ... that shows us an absolute pointer
			return SkipJumps(*(PBYTE*)(pbCode + 6 + lOffset),is32bit);
		}
	} 
	else if (pbCode[0] == 0xe9)
	{
		// here the behavior is identical, we have...
		// ...a 32-bit offset to the destination.
		return SkipJumps(pbCode + 5 + *(INT32 *)&pbCode[1],is32bit);
	} 
	else if (pbCode[0] == 0xeb) 
	{
		// and finally an 8-bit offset to the destination
		return SkipJumps(pbCode + 2 + *(CHAR *)&pbCode[1],is32bit);
	}
	
	return pbCode;
}

/*
 *	Writes code at Code that jumps to To. Will attempt to do this
 *	in as few bytes as possible. Important on x64 where the long jump
 *	(0xff 0x25 ....) can take up 14 bytes.
 */
BOOL ApiHkGenJmp (ULONG_PTR Code, ULONG_PTR To, int jmpsize, int isproc32bit)
{
	DWORD dwOldProtect = 0;
	DWORD dwOldProtect2 = 0;
	ULONG_PTR JumpFrom = Code + 5;
	ULONG_PTR Diff = JumpFrom > To ? JumpFrom - To : To - JumpFrom;
	unsigned char* pCode = (unsigned char*)Code;

	// unprotect memory
	if (!VirtualProtect((void*)Code, jmpsize, PAGE_EXECUTE_READWRITE, &dwOldProtect))
		return FALSE;

	if (Diff <= 0x7fff0000) 
	{
		pCode[0] = 0xe9;
		Code += 1;
		*((PDWORD)Code) = (DWORD)(DWORD_PTR)(To - JumpFrom);
		Code += sizeof(DWORD);
	} 
	else 
	{
		pCode[0] = 0xff;
		pCode[1] = 0x25;
		Code += 2;

		if (isproc32bit)
			// on x86 we write an absolute address (just behind the instruction)
			*((PDWORD)Code) = (DWORD)(DWORD_PTR)(Code + sizeof(DWORD));
		else
			// on x64 we write the relative address of the same location
			*((PDWORD)Code) = (DWORD)0;

		Code += sizeof(DWORD);
		*((PDWORD_PTR)Code) = (DWORD_PTR)(To);
		Code += sizeof(DWORD_PTR);
	}
	
	// restore memory protection
	VirtualProtect((void*)Code, jmpsize, dwOldProtect, &dwOldProtect2);
	return TRUE;
}

//************************************************************************/
// BOOL ApiHkGenJmp(DWORD To, DWORD From)
//
// Generates a jump to address "To", from address "From", 32-64bit aware
//************************************************************************/
BOOL ApiHkGenJmp2(ULONG_PTR From, ULONG_PTR To, int jmpsize, int isproc32bit)
{
	BYTE *pCur = (BYTE *) From;
	DWORD dwOldProtect = 0;
	DWORD dwOldProtect2 = 0;
	DWORD RelAddr = 0;
	
	// unprotect memory
	if (!VirtualProtect((void*)From, jmpsize, PAGE_EXECUTE_READWRITE, &dwOldProtect))
		return FALSE;

	if (isproc32bit)
	{
		*pCur = 0xE9; // jmp
		RelAddr = (DWORD)(To - From) - jmpsize;
		memcpy(++pCur, &RelAddr, sizeof (DWORD));
	}
	else
	{
		*pCur = 0x49;		// mov r15, ...
		*(++pCur) = 0xBF;
		memcpy(++pCur, &To, sizeof (ULONG_PTR));
		pCur += sizeof (ULONG_PTR);
		*pCur = 0x41;		// jmp r15
		*(++pCur) = 0xFF;
		*(++pCur) = 0xE7;
	}
	// restore memory protection
	VirtualProtect((void*)From, jmpsize, dwOldProtect, &dwOldProtect2);
	return TRUE;
}

//************************************************************************/
// BOOL ApiHkForgeHook(DWORD pOriginalAddress, DWORD pAddrToJump, PAPIHK_HOOK_STRUCT pHookStruct)
// 
// build hook 
//
// returns TRUE on success
//************************************************************************/
BOOL ApiHkForgeHook(ULONG_PTR pOriginalAddress, ULONG_PTR pAddrToJump, PAPIHK_HOOK_STRUCT pHookStruct, BOOL isproc32bit)
{
	ULONG_PTR trampolinesize=0;
	DWORD OldProtect=0;
	unsigned char* pTrampoline = NULL;
	int jmpsize = 0;
	_DecodeResult res = DECRES_INPUTERR;
	_DecodeType dt = Decode32Bits;
	_OffsetType offset = 0;
	_DecodedInst* decodedInstructions = NULL;
	unsigned int count = 0;
	unsigned int i = 0;

	// Check parameters
	if(!pOriginalAddress || !pAddrToJump || !pHookStruct)
		return FALSE;
	
	decodedInstructions = calloc (1,sizeof (_DecodedInst) * 128);
	if (!decodedInstructions)
		return FALSE;

	// on 64bit, we need a bigger jmp
	if (isproc32bit)
		jmpsize = 5;
	else
	{
		dt = Decode64Bits;
		jmpsize = 15;
	}
	
	// disasm
	res = distorm_decode(offset,(unsigned char*)pOriginalAddress, 50, dt, decodedInstructions, 128, &count);
	if (res == DECRES_INPUTERR)
	{
		free (decodedInstructions);	
		return FALSE;
	}

	// check how much space we collected
	for (i=0; i < count; i++)
	{		
		trampolinesize+=decodedInstructions[i].size;
		if (trampolinesize >= (ULONG_PTR)jmpsize)
			break;
	}

	// allocate memory for trampoline
	pTrampoline = VirtualAlloc (NULL,trampolinesize + jmpsize,MEM_COMMIT,PAGE_EXECUTE_READWRITE);
	if (!pTrampoline)
	{
		free (decodedInstructions);	
		return FALSE;
	}

	// copy collected instructions into trampoline, first clearing with NOPs
	memcpy (pTrampoline, (void*)pOriginalAddress, trampolinesize);	

	// into trampoline, add jump-back to original instruction flow (jmp pOriginalAddress+jmpsize)
	if (!ApiHkGenJmp((ULONG_PTR)(pTrampoline + trampolinesize), pOriginalAddress + trampolinesize, jmpsize, isproc32bit))
	{
		VirtualFree (pTrampoline,0,MEM_RELEASE);
		free (decodedInstructions);	
		return FALSE;
	}

	// put jump to trampoline at original function address : trampoline will execute collected code then
	// will jump back to original code after the "hook" jump
	if (!ApiHkGenJmp(pOriginalAddress,pAddrToJump,jmpsize,isproc32bit))
	{
		VirtualFree (pTrampoline,0,MEM_RELEASE);
		free (decodedInstructions);	
		return FALSE;
	}

	// store trampoline informations
	pHookStruct->FunctionAddress = pOriginalAddress;
	pHookStruct->TrampolineAddress = (ULONG_PTR)pTrampoline;
	pHookStruct->trampolinesize = trampolinesize;

#ifdef _WIN64
	DBG_OUT (("apihkforgehook sizejmp : %d, functionaddress = %I64d, trampolineaddress=%I64d, trampolinesize=%I64d\n", jmpsize, pOriginalAddress,
		pTrampoline, trampolinesize));
#else
	DBG_OUT (("apihkforgehook sizejmp : %d, functionaddress = %d, trampolineaddress=%d, trampolinesize=%d\n", jmpsize, pOriginalAddress,
		pTrampoline, trampolinesize));
#endif

	free (decodedInstructions);	
	return TRUE;
}

/*
 *	install ReplacementFunc api hook on the specified FuncName in DllName, returning a hookstructure (must be freed by the caller)
 *
 */
int apihk_installhook (IN char* DllName, IN char* FuncName, void* ReplacementFunc, IN OUT APIHK_HOOK_STRUCT* hk)
{
	int res = -1;
	void* pOriginalFunction = NULL;
	HANDLE hThisModule = NULL;
	PAPIHK_HOOK_STRUCT pHookStruct = hk;
	HMODULE hmod = NULL;
	BOOL isproc32bit = TRUE;

	// check params
	if (!DllName || !FuncName || !ReplacementFunc || !pHookStruct)
		goto __exit;
	memset ((PUCHAR)pHookStruct,0,sizeof (APIHK_HOOK_STRUCT));
	
	if (proc_is64bitOS())
	{
		// check for 64bit (check for 32bit process running on wow, returns false on true 64bit process)
		isproc32bit = proc_is64bitWOW(GetCurrentProcess());
	}
	
	DBG_OUT (("apihk_installhook is32bitproc = %d\n",isproc32bit));
	
	// get proc address of the target function
	pOriginalFunction = (void*) GetProcAddress(GetModuleHandleA(DllName), FuncName);
	if (!pOriginalFunction)
		goto __exit;

	// build and install hook, skipping eventually jumptables
	pOriginalFunction = SkipJumps(pOriginalFunction,isproc32bit);
	ReplacementFunc = SkipJumps(ReplacementFunc,isproc32bit);
	if(!ApiHkForgeHook((ULONG_PTR)pOriginalFunction, (ULONG_PTR)ReplacementFunc, pHookStruct, isproc32bit))
		goto __exit;
	
	res = 0;

__exit:
	return res;
}

/*
 *	uninstall hook previously set with apihk_uninstallhook and free apihook structure
 *
 */
int apihk_uninstallhook (APIHK_HOOK_STRUCT* hk)
{
	int res = -1;
	PBYTE pTargetAddress = NULL;
	PBYTE pSourceAddress = NULL;
	ULONG OldProtect = 0;
	PAPIHK_HOOK_STRUCT pHookStruct = hk;

	// check params
	if (!pHookStruct)
		goto __exit;
	if (!pHookStruct->FunctionAddress || !pHookStruct->TrampolineAddress || !pHookStruct->trampolinesize)
		goto __exit;

	pTargetAddress = (PBYTE)pHookStruct->FunctionAddress;
	pSourceAddress = (PBYTE)pHookStruct->TrampolineAddress;
	
	// change page attributes at targetaddress
	if (!VirtualProtect((void*)pTargetAddress, pHookStruct->trampolinesize, PAGE_EXECUTE_READWRITE, &OldProtect))
	{
		// can't deprotect, will leak memory but prevent crash.......
		return -1;
	}

	// copy back trampoline's collected space to function address, thus
	// regenerating the correct instruction flow
	memcpy (pTargetAddress, pSourceAddress, pHookStruct->trampolinesize);

	// restore back target original page attributes
	VirtualProtect((void*)pTargetAddress, pHookStruct->trampolinesize, OldProtect, &OldProtect);

	// free trampoline
	VirtualFree((unsigned char*)pHookStruct->TrampolineAddress,0,MEM_RELEASE);
	pHookStruct->FunctionAddress = 0;
	pHookStruct->TrampolineAddress = 0;
	pHookStruct->trampolinesize = 0;
	res = 0;

__exit:
	return res;
}
