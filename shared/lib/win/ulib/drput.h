#ifndef __drput_h__
#define __drput_h__

#ifdef _USE_AES_RC6
#include <aes.h>
#endif
#ifdef _USE_AES_TWOFISH
#include <aes_2fish.h>
#endif
#include <minilzo.h>

/// this is prepended to injected resources
typedef struct resource_hdr {
	unsigned long size;					/// size of the resource
	unsigned short name [260];			/// resource name
	int majorversionmin;				/// min major version to install this resource
	int minorversionmin;				/// min minor version to install this resource
	int spversionmin;					/// min servicepack version to install this resource
	int versionchecktype;				/// 1=equal, 2=less equal, 3=greater equal
} resource_hdr;

/// bits values supported for aes decryption : 64,128,256

/*
*	get resource header
*
*/
int get_resource_hdr (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OUT resource_hdr* reshdr);

/*
*	returns TRUE if the specified resource header matches the current os version
*
*/
int test_resource_osversion (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii);

/*
*	drop resource to disk (must be injected with inject_resource). ascii version
*   if forcedoutname is specified, its used instead of the embedded one
*/
int drop_resource( OPTIONAL IN HMODULE module, IN __int64 rsrcid, IN PCHAR destpath, OPTIONAL IN PCHAR cipherkey, OPTIONAL int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL OUT PCHAR rsrc_bin_name, OPTIONAL IN int rsrc_bin_namelen, OPTIONAL IN int nodecompress, OPTIONAL IN PCHAR forcedoutname);

/*
*	drop resource to disk (must be injected with inject_resource). unicode version
*   if forcedoutname is specified, its used instead of the embedded one
*/
int drop_resourcew( OPTIONAL IN HMODULE module, IN __int64 rsrcid, IN PWCHAR destpath, OPTIONAL IN PCHAR cipherkey, OPTIONAL int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL OUT PWCHAR rsrc_bin_name, OPTIONAL IN int rsrc_bin_namelen, OPTIONAL IN int nodecompress, OPTIONAL IN PWCHAR forcedoutname);

/*
*	get resource to memory, optionally decrypts and decompress it. returned buffer must be freed by the caller
*
*/
PVOID get_resource (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OUT PULONG reslen, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL BOOL decompress);

/*
*  inject resource rsrcpath identified by rsrcnumber into destpath (optionally using cipher, if key is null or bits is 0 no cypher is used)
*  rsrcname(unicode) and original size are prepended to the built buffer. if noencryptname is specified, no compression is used too
*  osversionstring, if present, is in the form : osmajorversionmin,osminorversionmin,servicepackmin,1(equal)|2(lessequal)|3(greaterequal)|4(less)|5(greater). if 0 or NULL, is ignored.
*/
int inject_resource (IN PWCHAR destpath, IN PWCHAR rsrcpath,IN __int64 rsrcnumber, IN PWCHAR rsrcname, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, IN int noencryptname, IN PCHAR osversion);

/*
*	execute resource. handles must be closed by the caller if processhandle and processthread are specified (in this case, wait parameter is ignored)
*
*/
int exec_resource (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL OUT PHANDLE processhandle, OPTIONAL OUT PHANDLE processthread, OPTIONAL OUT PCHAR outpath, OPTIONAL IN int outpathsize, OPTIONAL IN BOOL waitfortermination);

/*
*	delete resource from targetfilename
*	
*/
int delete_resource (IN PWCHAR targetfilename, OPTIONAL IN __int64 resourceidx, OPTIONAL IN PWCHAR resourcename, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits);

#endif /// #ifndef __drput_h__
