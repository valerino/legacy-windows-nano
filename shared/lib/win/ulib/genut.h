#ifndef __genut_h__
#define __genut_h__

#include <winsock2.h>

/*
 *	get localtime into a large integer
 *
 */
void get_localtime_as_largeinteger (OUT PLARGE_INTEGER localtime);

/*
*	wait for specified timeout on object, and returns true when signaled
*
*/
BOOL obj_wait (IN HANDLE hObject, IN ULONG Timeout);

/*
*	returns true if the object is signaled
*
*/
BOOL obj_testsignaled (IN HANDLE hObject);

/*
*	query a registry value
*
*/
int reg_queryvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size);

/*
*	query a registry value
*
*/
int reg_queryvalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size);

/*
*	set a registry value
*
*/
int reg_setvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size);

/*
*	set a registry value
*
*/
int reg_setvalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size);

/*
*	delete a registry value
*
*/
int reg_deletevalue (IN HANDLE parentkey, IN PCHAR subkey, IN PCHAR valuename);

/*
*	delete a registry value
*
*/
int reg_deletevaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename);

#endif // __genut_h__