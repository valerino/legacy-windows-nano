#ifndef __filesut_h__
#define __filesut_h__

#include <shellapi.h>
#include <shlwapi.h>

/*
*	get filesize by name (winapi)
*
*/
int file_getsizebynamew (IN PWCHAR filepath, OUT LARGE_INTEGER* size);

/*
*	get filesize by name (winapi)
*
*/
int file_getsizebynamew (IN PWCHAR filepath, OUT LARGE_INTEGER* size);


/*
*	get directory size ondisk
*
*/
int file_getdirsizew (IN PWCHAR path, OUT PLARGE_INTEGER dirsize);

/*
*	get directory size ondisk
*
*/
int file_getdirsize (IN PCHAR path, OUT PLARGE_INTEGER dirsize);

/*
*	delete directory tree
*
*/
int dir_delete_tree (IN PCHAR pPath, OPTIONAL IN PCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp);

/*
*	delete directory tree
*
*/
int dir_delete_treew (IN PWCHAR pPath, OPTIONAL IN PWCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp);

/*
*	get filesize from FILE (libc)
*
*/
ULONG file_libc_getsize (IN FILE* f);

/*
*	read file to buffer (unicode). returning pointer must be freed by the caller
*
*/
void* file_readtobufferw (OPTIONAL IN HANDLE hfile, OPTIONAL IN PWCHAR filepath, OUT PULONG filesize);

/*
*	read file to buffer. returning pointer must be freed by the caller
*
*/
void* file_readtobuffer (OPTIONAL IN HANDLE hfile, OPTIONAL IN PCHAR filepath, OUT PULONG filesize);

/*
*	create a file from buffer (unicode)
*
*/
int file_createfrombufferw (IN PWCHAR filepath, IN void* buffer, IN ULONG bufsize);

/*
*	create a file from buffer
*
*/
int file_createfrombuffer (IN PCHAR filepath, IN void* buffer, IN ULONG bufsize);

/*
*	get file attributes (unicode)
*
*/
int file_get_attributesw (IN PWCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes);

/*
*	get file attributes
*
*/
int file_get_attributes (IN PCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes);

/*
*	set file attributes (unicode)
*
*/
int file_set_attributesw (IN PWCHAR dstfile, OPTIONAL IN FILETIME* creationtime, OPTIONAL IN FILETIME* lastaccesstime, OPTIONAL IN FILETIME* lastwritetime, OPTIONAL IN DWORD attributes);

/*
*	get file attributes
*
*/
int file_set_attributes (IN PCHAR dstfile, OPTIONAL IN FILETIME* creationtime, OPTIONAL IN FILETIME* lastaccesstime, OPTIONAL IN FILETIME* lastwritetime, OPTIONAL IN DWORD attributes);

/*
*	revert wow64 filesystem redirector to previous state
*
*/
BOOL wow64_revert_fsredirector (IN PVOID redirectorhandle); 

/*
*	disable filesystem redirector on wow64. returned handle can be used with wow64_enable_fsredirector
*
*/
PVOID wow64_disable_fsredirector ();

#endif 