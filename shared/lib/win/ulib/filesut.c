/*
 *	file related utilities
 *  -vx-
 */
#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <strlib.h>
#include "filesut.h"
#include "procut.h"
#include <dbg.h>

/*
 *	revert wow64 filesystem redirector to previous state
 *
 */
BOOL wow64_revert_fsredirector (IN PVOID redirectorhandle)
{
	HMODULE mod = NULL;
	typedef BOOL (WINAPI *LPFN_REVERTWOW64FSREDIRECTION) (PVOID);
	LPFN_REVERTWOW64FSREDIRECTION prevertwow = NULL;
	BOOL res = FALSE;

	if (redirectorhandle == NULL)
		return FALSE;

	// get function
	prevertwow = proc_getprocaddress("kernel32.dll","Wow64RevertWow64FsRedirection",&mod);
	if (!prevertwow)	
		return FALSE;
	
	res = prevertwow (redirectorhandle);
	if (mod)
		FreeLibrary(mod);
	return res;
}

/*
*	disable filesystem redirector on wow64. returned handle can be used with wow64_enable_fsredirector
*
*/
PVOID wow64_disable_fsredirector ()
{
	HMODULE mod = NULL;
	typedef BOOL (WINAPI *LPFN_DISABLEWOW64FSREDIRECTION) (PVOID*);
	LPFN_DISABLEWOW64FSREDIRECTION pdisablewow = NULL;
	PVOID disablehandle = NULL;

	// get function
	pdisablewow = proc_getprocaddress("kernel32.dll","Wow64DisableWow64FsRedirection",&mod);
	if (!pdisablewow)	
		return NULL;

	pdisablewow(&disablehandle);
	if (mod)
		FreeLibrary(mod);
	return disablehandle;
}

/*
 *	get directory size ondisk
 *
 */
int file_getdirsizew (IN PWCHAR path, OUT PLARGE_INTEGER dirsize)
{
	WIN32_FIND_DATAW FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WCHAR pathtoscan [MAX_PATH];

	if (!path || !dirsize)
		return -1;

	dirsize->QuadPart = 0;
	wcscpy_s (pathtoscan,MAX_PATH,path);
	PathAppendW (pathtoscan,L"*.*");
	
	// start scan
	hFind = FindFirstFileW(pathtoscan, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
		return 0;
	
	while (TRUE)
	{
		// add sizes
		dirsize->LowPart += FindFileData.nFileSizeLow;
		dirsize->HighPart += FindFileData.nFileSizeHigh;
		
		// keep on until error
		if (!FindNextFileW (hFind,&FindFileData))
			break;
	}
	
	if (hFind != INVALID_HANDLE_VALUE)
		FindClose(hFind);
	return 0;
}

/*
*	get directory size ondisk
*
*/
int file_getdirsize (IN PCHAR path, OUT PLARGE_INTEGER dirsize)
{
	WCHAR wpath [MAX_PATH];
	size_t numchars = 0;

	if (!path || !dirsize)
		return -1;

	mbstowcs_s(&numchars,wpath,MAX_PATH,path,_TRUNCATE);
	return file_getdirsizew(wpath,dirsize);
}

/*
*	delete directory tree
*
*/
int dir_delete_treew (IN PWCHAR pPath, OPTIONAL IN PWCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp)
{
	int res = -1;
	WIN32_FIND_DATAW FindData;
	HANDLE hFindHandle = INVALID_HANDLE_VALUE;
	WCHAR wszPath[MAX_PATH];
	WCHAR tmpdir [MAX_PATH];
	WCHAR tmpfilename [MAX_PATH];
	static int numrecursion;
	static WCHAR wszStartPath [MAX_PATH] = {0};
	
	if (numrecursion == 0)
	{
		// initialize startdir
		wcscpy_s(wszStartPath,MAX_PATH,pPath);
	}

	// start the search
	wcscpy_s(wszPath,MAX_PATH,wszStartPath);
	str_remove_trailing_slashw (wszPath);
	PathAppendW(wszPath,L"*.*");
	hFindHandle = FindFirstFileW (wszPath,&FindData);
	if (hFindHandle == INVALID_HANDLE_VALUE)
	{
		
		// try to remove directory
		if (deleteemptydirs)
		{
			wcscpy_s(wszPath,MAX_PATH,wszStartPath);
			str_remove_trailing_slashw (wszPath);
			RemoveDirectoryW(wszPath);
		}

		// check for access denied, if so return success
		if (GetLastError () == ERROR_ACCESS_DENIED)
		{
			res = 0;
		}
		goto __exit;
	}

	// search loop
	while (TRUE)
	{
		// check if it's the parent dir entry
		if (FindData.cFileName[0] == (WCHAR)'.')
			goto __next;

		// check if its a directory
		if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// build new path
			PathAppendW(wszStartPath,FindData.cFileName);

			// recurse (and exit on error)
			numrecursion++;
			res = dir_delete_treew(wszStartPath,mask,deleteemptydirs,movetotemp);
			numrecursion--;
			if (res != 0)
				goto __exit;
		}
		else
		{
			// check mask if any
			if (mask)
			{
				if (!find_buffer_in_buffer(FindData.cFileName,mask,(unsigned long)wcslen(FindData.cFileName)*sizeof(WCHAR),(unsigned long)wcslen(mask)*sizeof(WCHAR),FALSE))
					goto __next;
			}

			wcscpy_s(wszPath,MAX_PATH,wszStartPath);
			PathAppendW(wszPath,FindData.cFileName);
			WDBG_OUT ((L"dir_delete_treew : %s\n", wszPath));

			// delete file
			if (movetotemp)
			{
				GetTempPathW (MAX_PATH,tmpdir);
				GetTempFileNameW(tmpdir,L"~sd",GetTickCount(),tmpfilename);
				MoveFileW (wszPath,tmpfilename);
				DeleteFileW(tmpfilename);
			}
			else
			{
				DeleteFileW(wszPath);
			}
		}
__next:
		// next entry
		if (!FindNextFileW(hFindHandle,&FindData))
		{
			if (deleteemptydirs)
			{
				// remove empty directory
				wcscpy_s(wszPath,MAX_PATH,wszStartPath);
				str_remove_trailing_slashw (wszPath);
				RemoveDirectoryW(wszPath);
			}
			break;
		}
	}

	// ok
	res = 0;

__exit:
	if (numrecursion == 0)
	{
		if (deleteemptydirs)
		{
			wcscpy_s(wszPath,MAX_PATH,wszStartPath);
			str_remove_trailing_slashw (wszPath);
			RemoveDirectoryW(wszPath);
		}
		DBG_OUT (("dir_delete_treew last recursion, res = %d\n", res));
		memset (wszStartPath,0,MAX_PATH * sizeof (WCHAR));
	}

	if (hFindHandle != INVALID_HANDLE_VALUE)
		FindClose(hFindHandle);
	return res;
}

/*
*	delete directory tree
*
*/
int dir_delete_tree (IN PCHAR pPath, OPTIONAL IN PCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp)
{
	WCHAR wpath [MAX_PATH];
	WCHAR wmask [MAX_PATH];
	size_t numchar = 0;

	if (!pPath)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH,pPath,_TRUNCATE);
	mbstowcs_s (&numchar,wmask,MAX_PATH,mask,_TRUNCATE);
	return dir_delete_treew(wpath,wmask,deleteemptydirs,movetotemp);
}

/*
 *	get filesize from FILE (libc)
 *
 */
ULONG file_libc_getsize (IN FILE* f)
{
	ULONG size = 0;
	
	if (!f)
		return 0;

	fseek (f,0,SEEK_END);
	size = ftell (f);
	rewind (f);

	return size;
}

/*
 *	read file to buffer (unicode). returning pointer must be freed by the caller
 *
 */
void* file_readtobufferw (OPTIONAL IN HANDLE hfile, IN PWCHAR filepath, OUT PULONG filesize)
{
	void* buf = NULL;
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG readbytes = 0;

	if (!filesize || (!hfile && !filepath))
		return NULL;
	*filesize = 0;

	if (hfile)
	{
		// provided by the caller
		f = hfile;
	}
	else
	{
		if (!filepath)
			return NULL;

		/// open file
		f = CreateFileW (filepath,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (f == INVALID_HANDLE_VALUE)
			return NULL;
	}

	/// get size and allocate memory
	size = GetFileSize (f,NULL);
	if (size == 0)
		goto __exit;
	buf = malloc (size + 32);
	if (!buf)
		goto __exit;
	memset (buf,0,size + 32);

	/// read file to buffer
	if (!ReadFile (f,buf,size,&readbytes,NULL))
	{
		free (buf);
		buf = NULL;
		goto __exit;
	}
	
	/// ok
	*filesize = size;

__exit:
	if (!hfile)
	{
		if (f != INVALID_HANDLE_VALUE)
			CloseHandle(f);
	}
	return buf;
}

/*
*	read file to buffer. returning pointer must be freed by the caller
*
*/
void* file_readtobuffer (OPTIONAL IN HANDLE hfile, OPTIONAL IN PCHAR filepath, OUT PULONG filesize)
{
	WCHAR wpath [MAX_PATH];
	size_t numchar = 0;

	if (!filesize || (!hfile && !filepath))
		return NULL;

	mbstowcs_s (&numchar,wpath,MAX_PATH,filepath,_TRUNCATE);
	return file_readtobufferw(hfile, wpath,filesize);
}

/*
 *	create a file from buffer (unicode)
 *
 */
int file_createfrombufferw (IN PWCHAR filepath, IN void* buffer, IN ULONG bufsize)
{
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG writebytes = 0;
	int res = -1;

	if (!filepath || !buffer || !bufsize)
		return -1;

	/// create file
	f = CreateFileW (filepath,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (f == INVALID_HANDLE_VALUE)
		goto __exit;

	/// read file to buffer
	if (!WriteFile(f,buffer,bufsize,&writebytes,NULL))
		goto __exit;
	
	/// ok
	res = 0;

__exit:
	if (res != 0)
	{
		WDBG_OUT ((L"file_createfrombufferw file %s getlasterror = %x\n", filepath, GetLastError()));
	}

	if (f != INVALID_HANDLE_VALUE)
		CloseHandle(f);
	return res;
}

/*
*	create a file from buffer
*
*/
int file_createfrombuffer (IN PCHAR filepath, IN void* buffer, IN ULONG bufsize)
{
	WCHAR wpath [MAX_PATH];
	size_t numchar = 0;

	if (!filepath || !buffer || !bufsize)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH,filepath,_TRUNCATE);
	return file_createfrombufferw(wpath,buffer,bufsize);
}

/*
*	get file attributes (unicode)
*
*/
int file_get_attributesw (IN PWCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes)
{
	HANDLE hsrc = INVALID_HANDLE_VALUE;
	FILETIME ctime;
	FILETIME latime;
	FILETIME lwtime;
	DWORD attrs = 0;
	int res = -1;

	if (!srcfile)
		return -1;
	if (!creationtime && !lastwritetime && !lastaccesstime && !attributes)
		return -1;

	/// open src file
	hsrc = CreateFileW (srcfile,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hsrc == INVALID_HANDLE_VALUE)
		goto __exit;

	/// get file attributes
	if (attributes)
	{
		attrs = GetFileAttributesW (srcfile);
		if (attrs == INVALID_FILE_ATTRIBUTES)
			goto __exit;
		*attributes = attrs;

	}

	/// get file times
	if (creationtime || lastaccesstime || lastwritetime)
	{
		if (!GetFileTime (hsrc,&ctime,&latime,&lwtime))
			goto __exit;
		if (creationtime)
			memcpy (creationtime,&ctime,sizeof (FILETIME));
		if (lastaccesstime)
			memcpy (lastaccesstime,&latime,sizeof (FILETIME));
		if (lastwritetime)
			memcpy (lastwritetime,&lwtime,sizeof (FILETIME));
	}

	/// ok
	res = 0;

__exit:
	if (hsrc != INVALID_HANDLE_VALUE)
		CloseHandle(hsrc);
	return res;
}

/*
*	get file attributes
*
*/
int file_get_attributes (IN PCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes)
{
	WCHAR wpath [MAX_PATH];
	size_t numchar = 0;

	if (!srcfile)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH,srcfile,_TRUNCATE);
	return file_get_attributesw(wpath,creationtime,lastaccesstime,lastwritetime,attributes);
}

/*
 *	get filesize by name (winapi)
 *
 */
int file_getsizebynamew (IN PWCHAR filepath, OUT LARGE_INTEGER* size)
{
	HANDLE h = INVALID_HANDLE_VALUE;
	ULONG filesizelo = 0;
	ULONG filesizehi = 0;

	if (!size || !filepath)
		return -1;
	
	size->QuadPart = 0;

	// open file
	h = CreateFileW(filepath,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (h == INVALID_HANDLE_VALUE)
		return -1;
	
	// get size
	filesizelo = GetFileSize(h,&filesizehi);
	size->LowPart = filesizelo;
	size->HighPart = filesizehi;
	CloseHandle(h);
	return 0;
}	

/*
*	get filesize by name (winapi)
*
*/
int file_getsizebyname (IN PCHAR filepath, OUT LARGE_INTEGER* size)
{
	size_t numchar = 0;
	WCHAR wpath [MAX_PATH];

	if (!size || !filepath)
		return -1;
	
	mbstowcs_s (&numchar,wpath,MAX_PATH,filepath,_TRUNCATE);
	return file_getsizebynamew(wpath,size);
}

/*
*	set file attributes (unicode)
*
*/
int file_set_attributesw (IN PWCHAR dstfile, OPTIONAL IN FILETIME* creationtime, OPTIONAL IN FILETIME* lastaccesstime, OPTIONAL IN FILETIME* lastwritetime, OPTIONAL IN DWORD attributes)
{
	HANDLE hdst = INVALID_HANDLE_VALUE;
	int res = -1;

	if (!dstfile)
		return -1;
	if (!creationtime && !lastwritetime && !lastaccesstime && !attributes)
		return -1;

	/// set file attributes
	if (attributes)
	{
		if (!SetFileAttributesW (dstfile,attributes))
			goto __exit;
	}

	/// open dst file
	hdst = CreateFileW (dstfile,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hdst == INVALID_HANDLE_VALUE)
		goto __exit;

	/// set file times
	if (creationtime || lastaccesstime || lastwritetime)
	{
		if (!SetFileTime (hdst,creationtime,lastaccesstime,lastwritetime))
			goto __exit;
	}

	/// ok
	res = 0;

__exit:
	if (hdst != INVALID_HANDLE_VALUE)
		CloseHandle(hdst);
	return res;
}

/*
*	set file attributes
*
*/
int file_set_attributes (IN PCHAR dstfile, OPTIONAL IN FILETIME* creationtime, OPTIONAL IN FILETIME* lastaccesstime, OPTIONAL IN FILETIME* lastwritetime, OPTIONAL IN DWORD attributes)
{
	WCHAR wpath [MAX_PATH];
	size_t numchar = 0;

	if (!dstfile)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH,dstfile,_TRUNCATE);
	return file_set_attributesw(wpath,creationtime,lastaccesstime,lastwritetime,attributes);
}