/*
 *	services utility functions
 *
 */

#include <winsock2.h>
#include <stdio.h>
#include <string.h>
#include "svcut.h"
#include <wdfinstaller.h>
#include "drput.h"
#include "procut.h"
#include "strsafe.h"
#include <shlwapi.h>

/*
*	globals
*
*/
static PFN_WDFPREDEVICEINSTALL  WdfPreDeviceInstallPtr = NULL;
static PFN_WDFPOSTDEVICEINSTALL WdfPostDeviceInstallPtr = NULL;
static PFN_WDFPREDEVICEREMOVE  WdfPreDeviceRemovePtr = NULL;
static PFN_WDFPOSTDEVICEREMOVE WdfPostDeviceRemovePtr = NULL;

/*
 *	check if the named device is present and running (tries to open \\\\.\\devicename). returns true if found
 *
 */
int svc_isdevicepresentw (IN PWCHAR devicename)
{
	WCHAR name [MAX_PATH];
	HANDLE drvhandle = INVALID_HANDLE_VALUE;

	StringCbPrintfW (name,sizeof (name), L"\\\\.\\%s", devicename);
	drvhandle = CreateFileW (name, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (drvhandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(drvhandle);
		return TRUE;
	}
	return FALSE;
}

/*
*	set service to svchost hosted (must be service dll)
*
*/
int svc_set_to_svchost (IN char* svc_name, OPTIONAL IN char* plgdir, IN char* svchost_group)
{
	HKEY hkey = NULL;
	HKEY hkeyparams = NULL;
	HKEY hkeysvchost = NULL;
	int res = -1;
	char path [1024];
	ULONG size = 0;
	char windir [MAX_PATH];

	GetWindowsDirectory(windir,sizeof (windir));
	
	/// open service key
	sprintf_s (path,sizeof (path),"SYSTEM\\CurrentControlSet\\Services\\%s",svc_name);
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,path,0,KEY_WRITE|KEY_READ,&hkey);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// set service path to point to svchost
	sprintf_s (path,sizeof (path),"%s\\system32\\svchost.exe -k %s",windir,svchost_group);
	res = RegSetValueEx (hkey,"ImagePath",0,REG_EXPAND_SZ,(BYTE*)path,(DWORD)strlen (path) + sizeof (CHAR));
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// set servicedll name
	sprintf_s (path,sizeof (path),"SYSTEM\\CurrentControlSet\\Services\\%s\\Parameters", svc_name);
	if (RegCreateKeyEx (HKEY_LOCAL_MACHINE,path,0,NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hkeyparams, NULL) != ERROR_SUCCESS)
		goto __exit;
	if (plgdir)
		sprintf_s (path,sizeof (path),"%s\\System32\\%s\\%s.dll",windir,plgdir,svc_name);
	else
		sprintf_s (path,sizeof (path),"%s\\System32\\%s.dll",windir,svc_name);
	res = RegSetValueEx (hkeyparams,"ServiceDll",0,REG_EXPAND_SZ,(BYTE*)path,(DWORD)strlen (path) + 1);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// open svchost key
	sprintf_s (path,sizeof (path),"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\SvcHost");
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,path,0,KEY_WRITE|KEY_READ,&hkeysvchost);
	if (res != ERROR_SUCCESS)
		goto __exit;
	
	/// set svchost group
	memset (path,0,sizeof (path));
	strcpy_s (path,sizeof (path),svc_name);
	res = RegSetValueEx (hkeysvchost,svchost_group,0,REG_MULTI_SZ,(BYTE*)path,(DWORD)strlen (path) + 2*sizeof (CHAR));

__exit:
	if (hkey)
		RegCloseKey(hkey);
	if (hkeyparams)
		RegCloseKey(hkeyparams);
	if (hkeysvchost)
		RegCloseKey(hkeysvchost);
	return res;
}

/*
*	set a service display name, optionally set description
*
*/
int svc_set_displayname (char* svc_name, char* displayname, OPTIONAL char* description)
{
	HKEY hkey = NULL;
	int res = -1;
	char path [1024];
	
	if (!svc_name)
		return -1;

	/// open service key
	sprintf_s (path,sizeof (path),"SYSTEM\\CurrentControlSet\\Services\\%s",svc_name);
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,path,0,KEY_WRITE|KEY_READ,&hkey);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// set name
	memset (path,0,sizeof (path));
	strcpy_s (path,sizeof (path),displayname);
	res = RegSetValueEx (hkey,"DisplayName",0,REG_SZ,(BYTE*)path,(DWORD)strlen (path) + sizeof (CHAR));
	if (res != ERROR_SUCCESS)
		goto __exit;
	
	if (!description)
	{
		res = 0;
		goto __exit;
	}

	/// set description
	memset (path,0,sizeof (path));
	strcpy_s (path,sizeof (path),description);
	res = RegSetValueEx (hkey,"Description",0,REG_SZ,(BYTE*)path,(DWORD)strlen (path) + sizeof (CHAR));

__exit:
	if (hkey)
		RegCloseKey(hkey);
	return res;
}

/*
*	remove servicegroup from svchost hosted (removes all in the group)
*
*/
int svc_remove_grp_from_svchost (char* svchost_group)
{
	HKEY hkeysvchost = NULL;
	int res = -1;
	char path [1024];

	/// open svchost key
	sprintf_s (path,sizeof (path),"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\SvcHost");
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,path,0,KEY_WRITE|KEY_READ,&hkeysvchost);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// delete value
	RegDeleteValue (hkeysvchost,svchost_group);

__exit:
	if (hkeysvchost)
		RegCloseKey(hkeysvchost);

	return res;
}

/*
 *	load coinstaller and returns pointer to pre/post install functions
 *
 */
int kmdf_load_coinstaller (char* coinstallerpath, OUT HMODULE* coinstmod, OPTIONAL OUT PULONG_PTR preinstallptr, OPTIONAL OUT PULONG_PTR postinstallptr, OPTIONAL OUT PULONG_PTR preremoveptr, OPTIONAL OUT PULONG_PTR postremoveptr)
{
	HMODULE mod = NULL;
	ULONG_PTR preinst = 0;
	ULONG_PTR postinst = 0;
	ULONG_PTR preremv = 0;
	ULONG_PTR postremv = 0;
	int res = -1;

	/// load coinstaller and get pointers
	mod = LoadLibrary (coinstallerpath);
	if (!mod)
		goto __exit;
	
	preinst = (ULONG_PTR)GetProcAddress (mod,"WdfPreDeviceInstall");
	postinst = (ULONG_PTR)GetProcAddress (mod,"WdfPostDeviceInstall");
	preremv = (ULONG_PTR)GetProcAddress (mod,"WdfPreDeviceRemove");		
	postremv = (ULONG_PTR)GetProcAddress (mod,"WdfPostDeviceRemove");		
	if (preinstallptr)
	{
		if (!preinst)
			goto __exit;
		else
			*preinstallptr = preinst;
	}
	if (postinstallptr)
	{
		if (!postinst)
			goto __exit;
		else
			*postinstallptr = postinst;
	}
	if (preremoveptr)
	{
		if (!preremv)
			goto __exit;
		else
			*preremoveptr = preremv;
	}
	if (postremoveptr)
	{
		if (!postremv)
			goto __exit;
		else 
			*postremoveptr = postremv;
	}
	
	/// ok
	res = 0;
	*coinstmod = mod;

__exit:
	if (res != 0)
	{
		if (mod)
			FreeLibrary(mod);
	}
	return res;
}

/*
 *	generate dummy inf on the fly for kmdf install
 *
 */
int kmdf_generate_inf(char* infpath, char* servicename) 
{
	BOOLEAN ret = TRUE;
	CHAR buf [1024];
	int res = -1;
	FILE* f = NULL;

	/// create file
	f = fopen (infpath,"wb");
	if (!f)
		goto __exit;

	/// write
	memset (buf,0,sizeof (buf));
	sprintf_s(buf, sizeof (buf), 
		"[Version]\r\n"								\
		"signature = \"$Windows NT$\"\r\n"			\
		"\r\n"										\
		"[%s.NT.Wdf]\r\n"							\
		"KmdfService = %s, wdfsect\r\n"				\
		"[wdfsect]\r\n"								\
		"KmdfLibraryVersion = 1.0\r\n", 
		servicename, servicename);
	if (fwrite (buf,strlen(buf),1,f) != 1)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (f)
		fclose (f);
	return res;
}

/*
 *	open service
 *
 */
ULONG service_open (OPTIONAL SC_HANDLE scmhandle, char* servicename, ULONG access, OUT SC_HANDLE* svchandle)
{
	ULONG res = -1;
	SC_HANDLE scm = NULL;
	SC_HANDLE svc = NULL;

	if (!servicename || !svchandle)
		goto __exit;
	*svchandle = NULL;

	/// open scm if handle not provided
	scm = scmhandle;
	if (!scm)
	{
		scm = OpenSCManager (NULL,NULL,SC_MANAGER_ALL_ACCESS);
		if (!scm)
		{
			res = GetLastError();
			goto __exit;
		}
	}

	/// open service
	svc = OpenService (scm,servicename,access);
	if (!svc)
	{
		res = GetLastError();
		goto __exit;
	}
	*svchandle = svc;
	res = 0;

__exit:
	if (scm && !scmhandle)
		CloseServiceHandle(scm);
	return res;
}

/*
 *	control service (send stop/pause/etc...)
 *
 */
ULONG service_control (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char* servicename, ULONG ctlcode, OPTIONAL OUT SERVICE_STATUS* svcstatus)
{
	ULONG res = -1;
	SC_HANDLE svc = NULL;
	SERVICE_STATUS status;

	if (svcstatus)
		memset (svcstatus,0,sizeof (SERVICE_STATUS));
	svc=svchandle;
	if (!svc && !servicename)
		goto __exit;

	/// open service
	if (!svc)
	{
		res = service_open(scmhandle,servicename,SERVICE_ALL_ACCESS,&svc);
		if (res != 0)
			goto __exit;
	}

	/// send ctlcode
	if (!ControlService(svc,ctlcode,&status))
	{
		res = GetLastError();
		goto __exit;
	}

	/// ok
	res = 0;
	if (svcstatus)
		memcpy(svcstatus,&status,sizeof (SERVICE_STATUS));

__exit:
	if (svc && !svchandle)
		CloseServiceHandle(svc);
	return res;
}

/*
 *	start service
 *
 */
int service_start (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char*servicename, OPTIONAL int numargs, OPTIONAL char** args, int restart)
{
	int res = -1;
	SERVICE_STATUS status;
	SC_HANDLE svc = NULL;

	svc = svchandle;
	if (!svc)
	{
		/// get svc handle
		res = service_open(scmhandle,servicename,SERVICE_ALL_ACCESS,&svc);
		if (res != 0)
			goto __exit;
	}

	if (restart)
	{
		/// stop service
		res = service_control (scmhandle,svc,NULL,SERVICE_CONTROL_STOP,&status);
		if (res != 0)
			goto __exit;

		/// wait until its stopped
		while (TRUE)
		{
			if (!QueryServiceStatus(svc,&status))
				break;
			if (status.dwCurrentState == SERVICE_STOPPED)
				break;
			Sleep (1000);
		}
	}
	
	/// start
	if (!StartService (svc,0,NULL))
	{
		res = GetLastError();
		goto __exit;
	}

	/// ok
	res = 0;

__exit:
	if (svc && !svchandle)
		CloseServiceHandle(svc);
	return res;
}

/*
 *	uninstall service
 *
 */
ULONG service_uninstall (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char* servicename)
{
	ULONG res = -1;
	SC_HANDLE svc = NULL;

	svc=svchandle;
	if (!svc && !servicename)
		goto __exit;

	/// open service
	if (!svc)
	{
		res = service_open(scmhandle,servicename,SERVICE_ALL_ACCESS|DELETE,&svc);
		if (res != 0)
			goto __exit;
	}

	/// remove service
	if (!DeleteService(svc))
	{
		res = GetLastError();
		goto __exit;
	}

	/// ok
	res = 0;

__exit:
	if (svc && !svchandle)
		CloseServiceHandle(svc);
	return res;
}

/*
*	install service alternative (provides separate path and binary name)
*
*/
int service_install2 (IN PCHAR destdir, IN PCHAR svcname, IN PCHAR dispname, OPTIONAL IN PCHAR groupname, IN PCHAR binname, IN ULONG type, IN ULONG starttype)
{
	int res = -1;
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};

	/// install service
	sprintf_s(path,sizeof (path),"%s\\%s",destdir,binname);
	if (groupname)
	{
		/// install using the specified group name
		res = service_install (NULL,svcname,dispname,groupname,path,type,starttype,
			SERVICE_ERROR_IGNORE,NULL,NULL,NULL,NULL);
	}
	else
	{
		/// no group name provided
		res = service_install (NULL,svcname,dispname,NULL,path,type,starttype,
			SERVICE_ERROR_IGNORE,NULL,NULL,NULL,NULL);
	}

	if (res != 0)
	{
		if (res != ERROR_SERVICE_EXISTS)
			goto __exit;
		res = 0;
	}

	/// ok
	res = 0;

__exit:
	return res;
}

/*
 *	install service
 *
 */
ULONG service_install (OPTIONAL HANDLE scmhandle, char* svcname, char* svcdisplayname, char* svcgroup, char* svcpath, ULONG type, ULONG starttype, ULONG errortype, PULONG tag, char* dependencies, char* user, char* pwd)
{
	ULONG res = -1;
	SC_HANDLE scm = NULL;
	SC_HANDLE svc = NULL;
	
	/// open scm if handle not provided
	scm = scmhandle;
	if (!scm)
	{
		scm = OpenSCManager (NULL,NULL,SC_MANAGER_CREATE_SERVICE);
		if (!scm)
		{
			res = GetLastError();
			goto __exit;
		}
	}
	
	/// install service
	svc = CreateService (scm,svcname,svcdisplayname,SC_MANAGER_CREATE_SERVICE,type,
		starttype, errortype, svcpath, svcgroup, tag, dependencies, user, pwd);
	if (!svc)
	{
		res = GetLastError();
		goto __exit;
	}
	res = 0;

__exit:
	if (scm && !scmhandle)
		CloseServiceHandle(scm);
	if (svc)
		CloseServiceHandle(svc);
	return res;
}

/*
 *	add wdm class filter
 *
 */
int add_class_filter_registry (PCHAR drivername, PCHAR guid, PCHAR filtertype)
{
	HKEY hkey = NULL;
	int res = -1;
	char filters [512] = {0};
	char newfilters [512] = {0};
	ULONG size = 0;
	char* p = NULL;
	ULONG type = 0;

	/// open class key
	sprintf_s (filters,sizeof (filters),"SYSTEM\\CurrentControlSet\\Control\\Class\\%s",guid);
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,filters,0,KEY_WRITE|KEY_READ,&hkey);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// read key value
	size = sizeof (filters);
	memset (filters,0,size);
	res = RegQueryValueEx (hkey,filtertype,NULL,&type,filters,&size);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// append our filter if it isn't already there
	p = find_buffer_in_buffer (filters,drivername,sizeof (filters),(ULONG)strlen(drivername),FALSE);
	if (p)
	{
		res = 0;
		goto __exit;
	}
	strcpy_s (newfilters,sizeof (newfilters),drivername);
	p = newfilters + strlen (newfilters) + 1;
	memcpy (p,filters,size);
	size+=strlen (drivername) + 1;

	res = RegSetValueEx (hkey,filtertype,0,REG_MULTI_SZ,newfilters,size);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (hkey)
		RegCloseKey(hkey);
	return res;
}

/*
 *	remove wdm class filter
 *
 */
int del_class_filter_registry (PCHAR drivername, PCHAR guid, PCHAR filtertype)
{
	HKEY hkey = NULL;
	int res = -1;
	char path [1024];
	char copybuf [1024];
	ULONG size = 0;
	char* p = NULL;
	char* q = NULL;
	ULONG type = 0;

	/// open class key
	sprintf_s (path,sizeof (path),"SYSTEM\\CurrentControlSet\\Control\\Class\\%s",guid);
	res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,path,0,KEY_WRITE|KEY_READ,&hkey);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// read key value
	memset (path,0,sizeof (path));
	size = sizeof (path);
	res = RegQueryValueEx (hkey,filtertype,NULL,&type,path,&size);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// search for our filter
	p = find_buffer_in_buffer (path,drivername,sizeof (path),(ULONG)strlen(drivername),FALSE);
	if (!p)
	{
		res = 0;
		goto __exit;
	}
	p=path;
	q=copybuf;
	size = 0;
	memset(copybuf,0,sizeof (copybuf));
	while (TRUE)
	{
		/// if its not our driver,copy
		if (strcmp ((p),drivername) != 0)
		{
			strncpy(q,p,strlen(p));
			size+=(ULONG)(strlen(p)+sizeof (CHAR));
			q+=size;
			p+=size;
		}
		else
		{
			p+=(ULONG)(strlen(p)+sizeof (CHAR));
		}
		if (*p)
			continue;
		/// end
		size+=sizeof (CHAR);
		break;
	}

	res = RegSetValueEx (hkey,filtertype,0,REG_MULTI_SZ,copybuf,size);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (hkey)
		RegCloseKey(hkey);
	return res;
}

/*
 *	add protocol binding
 *
 */
int add_protocol_binding_registry (IN PCHAR drivername)
{
	int res = -1;
	HKEY hadapters = NULL;
	HKEY h = NULL;
	HKEY htcp = NULL;
	HKEY hfilter = NULL;
	ULONG idx = 0;
	PCHAR p = NULL;
	PCHAR q = NULL;
	CHAR buffer [1024];
	CHAR copybuffer [1024];
	CHAR keyname [64];
	ULONG sizename = 0;
	ULONG type = 0;
	ULONG len = 0;
	BOOLEAN found = FALSE;
	ULONG offset = 0;
	ULONG offsetpre1 = 0;
	ULONG offsetpre2 = 0;

	/// copy bindings from tcpip driver
	if (RegOpenKeyEx (HKEY_LOCAL_MACHINE,"SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Linkage",0,KEY_READ,&htcp) != ERROR_SUCCESS)
		goto __exit;
	sprintf_s (buffer,sizeof (buffer),"SYSTEM\\CurrentControlSet\\Services\\%s\\Linkage", drivername);
	if (RegCreateKeyEx (HKEY_LOCAL_MACHINE,buffer,0,NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hfilter, NULL) != ERROR_SUCCESS)
		goto __exit;

	len = sizeof (buffer);
	if (RegQueryValueEx(htcp,"Bind",NULL,&type,(PBYTE)buffer,&len) != ERROR_SUCCESS)
		goto __exit;
	if (RegSetValueEx(hfilter,"Bind",0,REG_MULTI_SZ,(PBYTE)buffer,len) != ERROR_SUCCESS)
		goto __exit;

	len = sizeof (buffer);
	if (RegQueryValueEx(htcp,"Route",NULL,&type,(PBYTE)buffer,&len) != ERROR_SUCCESS)
		goto __exit;
	if (RegSetValueEx(hfilter,"Route",0,REG_MULTI_SZ,(PBYTE)buffer,len) != ERROR_SUCCESS)
		goto __exit;

	len = sizeof (buffer);
	if (RegQueryValueEx(htcp,"Export",NULL,&type,(PBYTE)buffer,&len) != ERROR_SUCCESS)
		goto __exit;

	/// generate unique name for the devices
	memset (copybuffer,0,sizeof (copybuffer));
	p = (char*)buffer;
	q = (char*)copybuffer;
	sprintf_s (copybuffer,sizeof (copybuffer),"\\Device\\%s_",drivername);
	offsetpre1 = (ULONG)strlen("\\Device\\tcpip_");
	offsetpre2 = (ULONG)strlen(copybuffer);
	offset = 0;
	len = 0;
	while (*p != '\0')
	{
		sprintf_s (q,sizeof (copybuffer),"\\Device\\%s_",drivername);
		memcpy (q + offsetpre2,p + offsetpre1,strlen (p + offsetpre1));
		p+=((ULONG)strlen (p) + sizeof (CHAR));
		len += (ULONG)strlen (q)+sizeof(CHAR);
		q+=((ULONG)strlen (q) + sizeof (CHAR));
		
	}
	len+=sizeof (CHAR);
	if (RegSetValueEx(hfilter,"Export",0,REG_MULTI_SZ,(PBYTE)copybuffer,len) != ERROR_SUCCESS)
		goto __exit;

	/// add protocol to the adapter bindings
	if (RegOpenKeyEx (HKEY_LOCAL_MACHINE,"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}",0,KEY_READ,&hadapters) != ERROR_SUCCESS)
		goto __exit;
	
	while (TRUE)
	{
		/// get subkey
		sizename = sizeof (keyname);
		res = RegEnumKeyEx(hadapters,idx,keyname,&sizename,NULL,NULL,NULL,0);
		if (res != ERROR_SUCCESS)
		{
			if (res != ERROR_NO_MORE_ITEMS)
				res = -1;
			else
				res = 0;
			break;
		}
		
		/// get linkage subkey
		sprintf_s (buffer,sizeof (buffer),"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\%s\\Linkage",keyname);
		h=NULL;
		res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,buffer,0,KEY_READ|KEY_WRITE,&h);
		if (res != ERROR_SUCCESS)
		{
			res = 0;
			goto __next;
		}
		len = sizeof (buffer);
		memset (buffer,0,sizeof (buffer));
		res = RegQueryValueEx(h,"UpperBind",NULL,&type,(PBYTE)buffer,&len);
		if (res != ERROR_SUCCESS)
		{
			res = 0;
			goto __next;
		}
		
		/// check if its the right adapter to bind on, and check that we're not bound yet
		p = (char*)buffer;
		found = FALSE;
		while (*p != '\0')
		{
			if (_stricmp (p,drivername) == 0)
			{
				found = TRUE;
				break;
			}
			p+=(strlen (p) + sizeof (CHAR));
		}
		if (found)
			goto __next;
		
		p = (char*)buffer;
		found = FALSE;
		while (*p != '\0')
		{
			if (_stricmp (p,"ndisuio") == 0)
			{
				found = TRUE;
				break;
			}
			p+=(strlen (p) + sizeof (CHAR));
		}
		
		/// copy our driver name in case, and update value
		if (found)
		{
			memset (copybuffer,0,sizeof (copybuffer));
			strcpy_s (copybuffer,sizeof (copybuffer),drivername);
			p = copybuffer + strlen (copybuffer) + 1;
			memcpy (p,buffer,len);
			len+=(ULONG)strlen (drivername) + sizeof (CHAR);
			res = RegSetValueEx(h,"UpperBind",0,REG_MULTI_SZ,(PBYTE)copybuffer,len);
			if (res != ERROR_SUCCESS)
				res = -1;
		}
__next:
		if (h)
			RegCloseKey(h);
		if (res != 0)
			break;
		idx++;
	}

__exit:
	if (hfilter)
		RegCloseKey(hfilter);
	if (htcp)
		RegCloseKey(htcp);
	if (hadapters)
		RegCloseKey(hadapters);
	return res;
}

/*
*	remove protocol binding
*
*/
int del_protocol_binding_registry (IN PCHAR drivername)
{
	int res = -1;
	HKEY hadapters = NULL;
	HKEY h = NULL;
	ULONG idx = 0;
	PCHAR p = NULL;
	PCHAR q = NULL;
	CHAR buffer [1024];
	CHAR copybuffer [1024];
	CHAR keyname [64];
	ULONG sizename = 0;
	ULONG type = 0;
	ULONG len = 0;
	BOOLEAN found = FALSE;

	if (RegOpenKeyEx (HKEY_LOCAL_MACHINE,"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}",0,KEY_READ,&hadapters) != ERROR_SUCCESS)
		goto __exit;

	while (TRUE)
	{
		/// get subkey
		sizename = sizeof (keyname);
		res = RegEnumKeyEx(hadapters,idx,keyname,&sizename,NULL,NULL,NULL,0);
		if (res != ERROR_SUCCESS)
		{
			if (res != ERROR_NO_MORE_ITEMS)
				res = -1;
			else
				res = 0;
			break;
		}

		/// get linkage subkey
		sprintf_s (buffer,sizeof (buffer),"SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\%s\\Linkage",keyname);
		h=NULL;
		res = RegOpenKeyEx (HKEY_LOCAL_MACHINE,buffer,0,KEY_READ|KEY_WRITE,&h);
		if (res != ERROR_SUCCESS)
			goto __next;
		len = sizeof (buffer);
		memset (buffer,0,sizeof (buffer));
		res = RegQueryValueEx(h,"UpperBind",NULL,&type,(PBYTE)buffer,&len);
		if (res != ERROR_SUCCESS)
			goto __next;

		/// check if we find our driver name
		p = (char*)buffer;
		q = (char*)copybuffer;
		memset (copybuffer,0,sizeof (copybuffer));
		len = 0;
		found = FALSE;
		while (*p != '\0')
		{
			if (_stricmp (p,drivername) != 0)
			{
				memcpy (q,p,strlen (p));
				q+=(ULONG)strlen (p) + sizeof (CHAR);
				len +=(ULONG)strlen (p) + sizeof (CHAR);
				found = TRUE;
			}
			p+=((ULONG)strlen (p) + sizeof (CHAR));
		}

		/// update value
		if (found)
		{
			len += sizeof (CHAR);
			res = RegSetValueEx(h,"UpperBind",0,REG_MULTI_SZ,(PBYTE)copybuffer,len);
			if (res != ERROR_SUCCESS)
				res = -1;
		}
__next:
		if (h)
			RegCloseKey(h);
		if (res != 0)
			break;
		idx++;
	}

__exit:
	if (hadapters)
		RegCloseKey(hadapters);
	return res;
}

/*
 *	add minifilter
 *
 */
int add_minifilter_registry (char* drivername, char* instancename, char* altitude)
{
	HKEY hkey = NULL;
	HKEY hsubkey = NULL;
	LONG err = ERROR_SUCCESS;
	int res = -1;
	char path [1024];
	ULONG value = 0;

	/// create instances key
	sprintf_s (path,sizeof (path),"SYSTEM\\CurrentControlSet\\Services\\%s\\Instances",drivername);
	res = RegCreateKeyEx (HKEY_LOCAL_MACHINE,path,0,NULL,REG_OPTION_NON_VOLATILE,KEY_WRITE|KEY_READ,NULL,&hkey,NULL);
	if (res != ERROR_SUCCESS)
		goto __exit;

	res = RegSetValueEx (hkey,"DefaultInstance",0,REG_SZ,instancename,(DWORD)strlen(instancename)+1);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// create subkey of instances
	res = RegCreateKeyEx (hkey,instancename,0,NULL,REG_OPTION_NON_VOLATILE,KEY_WRITE|KEY_READ,NULL,&hsubkey,NULL);
	if (res != ERROR_SUCCESS)
		goto __exit;
	value = 2;
	res = RegSetValueEx (hsubkey,"Flags",0,REG_DWORD,(BYTE*)&value,sizeof (ULONG));
	if (res != ERROR_SUCCESS)
		goto __exit;
	res = RegSetValueEx (hsubkey,"Altitude",0,REG_SZ,altitude,(DWORD)strlen(altitude)+1);
	if (res != ERROR_SUCCESS)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (hkey)
		RegCloseKey(hkey);
	if (hsubkey)
		RegCloseKey(hsubkey);
	return res;
}

/*
*	install kmdf driver
*
*/
int install_kmdf (IN PCHAR svcname, OPTIONAL IN PCHAR groupname, IN PCHAR binname, IN ULONG starttype)
{
	CHAR temp [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	WCHAR wpath [MAX_PATH] = {0};
	WCHAR wbuf [64] = {0};
	CHAR windir[MAX_PATH] = {0};

	int res = -1;

	/// generate inf
	GetTempPath(MAX_PATH,temp);
	sprintf_s(path,sizeof (path),"%swdfinf.inf",temp);
	if (kmdf_generate_inf (path,svcname) != 0)
		goto __exit;

	/// call the coinstaller for pre-install
	swprintf_s(wpath,MAX_PATH, L"%Swdfinf.inf",temp);
	swprintf_s(wbuf, 64, L"%S.NT.Wdf", svcname);
	if (WdfPreDeviceInstallPtr(wpath, wbuf) != ERROR_SUCCESS)
		goto __exit;

	/// install service
	GetWindowsDirectory(windir,MAX_PATH);
	sprintf_s(path,sizeof (path),"%s\\system32\\drivers",windir);
	if (service_install2 (path,svcname,svcname,groupname,binname,SERVICE_KERNEL_DRIVER,starttype) != 0)
		goto __exit;

	/// call the coinstaller for post install
	if (WdfPostDeviceInstallPtr(wpath, wbuf) != ERROR_SUCCESS)
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (res != 0)
	{
		/// remove
		swprintf_s(wpath,MAX_PATH,L"%Swdfinf.inf",temp);
		swprintf_s(wbuf, 64, L"%S.NT.Wdf", svcname);
		WdfPreDeviceRemovePtr(wpath,wbuf);
		service_uninstall (NULL,NULL,svcname);
		WdfPostDeviceInstallPtr(wpath,wbuf);
	}

	/// delete files
	sprintf_s(path,sizeof (path),"%swdfinf.inf",temp);
	DeleteFile (path);
	return res;
}

/*
*	generic routine to drop and install a kmdf driver in \systemroot\system32\drivers (resource must be in the current process, added with drput functions)
*
*/
int service_kmdf_generic_install_from_rsrc (HMODULE module, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
								 OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG starttype, OPTIONAL OUT PCHAR bin_namefromsrsrc, OPTIONAL IN int bin_namefromrsrclen, OPTIONAL OUT PCHAR svc_namefromsrsrc, OPTIONAL IN int svc_namefromrsrclen)
{
	CHAR windir [MAX_PATH] = {0};
	CHAR path [MAX_PATH] = {0};
	CHAR svcname [32] = {0};
	CHAR svcbin [32] = {0};
	PCHAR p = NULL;
	SC_HANDLE h = NULL;
	int res = 0;
	
	/// extract driver in \\windows\\system32\\drivers
	GetWindowsDirectory(windir,sizeof (windir));
	strcat_s (windir,sizeof (windir),"\\system32\\drivers");
	res = drop_resource(module,resourceid,windir,enckey, bits, FALSE,svcbin,sizeof (svcbin),FALSE,NULL);
	if (res != 0)
		goto __exit;

	if (bin_namefromrsrclen && bin_namefromsrsrc)
		strcpy_s (bin_namefromsrsrc,bin_namefromrsrclen,svcbin);

	/// get svcname from bin name
	strcpy_s (svcname,sizeof (svcname),svcbin);
	p = strrchr (svcname,'.');
	if (p)
	{
		*p = '\0';
		if (svc_namefromsrsrc && svc_namefromrsrclen)
			strcpy_s (svc_namefromsrsrc,svc_namefromrsrclen,svcname);
	}

	/// check if service exists
	res = service_open (NULL,svcname,SERVICE_ALL_ACCESS,&h);
	if (res == 0)
	{
		CloseServiceHandle(h);
		goto __exit;
	}

	/// install driver
	res = install_kmdf(svcname, group, svcbin, starttype);

__exit:
	return res;
}

/*
*	generic routine to drop and install a win32 service in basepath
*
*/
int service_win32_generic_install_from_rsrc (OPTIONAL IN HMODULE module, IN PCHAR basepath, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
								   OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG servicetype, IN ULONG starttype, OPTIONAL OUT PCHAR bin_namefromsrsrc, OPTIONAL IN int bin_namefromrsrclen, OPTIONAL OUT PCHAR svc_namefromsrsrc, OPTIONAL IN int svc_namefromrsrclen)
{
	CHAR svcname [32] = {0};
	CHAR svcbin [32] = {0};
	PCHAR p = NULL;
	SC_HANDLE h = NULL;
	int res = 0;
	
	res = drop_resource(module,resourceid,basepath,enckey, bits, FALSE,svcbin,sizeof (svcbin),FALSE,NULL);
	if (res != 0)
		goto __exit;

	if (bin_namefromrsrclen && bin_namefromsrsrc)
		strcpy_s (bin_namefromsrsrc,bin_namefromrsrclen,svcbin);

	/// get svcname from bin name
	strcpy_s (svcname,sizeof (svcname),svcbin);
	p = strrchr (svcname,'.');
	if (p)
	{
		*p = '\0';
		if (svc_namefromsrsrc && svc_namefromrsrclen)
			strcpy_s (svc_namefromsrsrc,svc_namefromrsrclen,svcname);
	}

	/// check if service exists
	res = service_open (NULL,svcname,SERVICE_ALL_ACCESS,&h);
	if (res == 0)
	{
		CloseServiceHandle(h);
		goto __exit;
	}

	/// install service
	res = service_install2 (basepath,svcname,svcname,group,svcbin,servicetype,starttype);

__exit:
	return res;
}

/*
*	generic routine to drop and install a win32 service in basepath, specifying all
*
*/
int service_win32_generic_install_from_rsrc2 (OPTIONAL IN HMODULE module, IN PCHAR basepath, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
											 OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG servicetype, IN ULONG starttype, IN PCHAR bin_name, IN PCHAR svc_name, IN PCHAR disp_name)
{
	PCHAR p = NULL;
	SC_HANDLE h = NULL;
	int res = 0;

	if (!bin_name || !svc_name || !disp_name)
		return -1;
	
	// drop
	res = drop_resource(module,resourceid,basepath,enckey, bits, FALSE,NULL,0,FALSE,bin_name);
	if (res != 0)
		goto __exit;

	/// check if service exists
	res = service_open (NULL,svc_name,SERVICE_ALL_ACCESS,&h);
	if (res == 0)
	{
		CloseServiceHandle(h);
		goto __exit;
	}

	/// install service
	res = service_install2 (basepath,svc_name,disp_name, group,bin_name,servicetype,starttype);

__exit:
	return res;
}

/*
*	generic routine to cleanup a failed service installation, removing service file too. destdir must be the directory where servicebin binname is.
*
*/
void service_generic_cleanup (IN PCHAR destdir, IN PCHAR svcname, IN PCHAR binname)
{
	CHAR path [MAX_PATH] = {0};

	/// uninstall service
	service_uninstall (NULL,NULL,svcname);

	/// delete file
	sprintf_s(path,sizeof (path),"%s\\%s",destdir,binname);
	DeleteFile(path);
}

/*
*	extract kmdf engine to temp directory and return path. if forcedoutname is specified, it will be used instead of the one from the extracted resource
*   
*/
int kmdf_extract_from_rsrc (OPTIONAL IN HMODULE module, IN int coinstallerrsrc, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, OPTIONAL IN char* forcedoutname, OUT char* coinstallerpath, IN int coinstallerpathsize)
{
	char tmpdir [MAX_PATH] = {0};
	char origpath[MAX_PATH];
	char coinstallername [64] = {0};
	char path[MAX_PATH] = {0};
	HMODULE coinstmod = NULL;

	if (!coinstallerrsrc || !coinstallerpathsize || !coinstallerpath)
		return -1;

	if (FindResource (module,MAKEINTRESOURCE(coinstallerrsrc),RT_RCDATA))
	{
		/// extract the wdf coinstaller
		GetSystemDirectoryA(tmpdir,MAX_PATH);
		if (drop_resource(module,coinstallerrsrc,tmpdir,cipherkey, bits, FALSE, coinstallername,sizeof (coinstallername),FALSE, NULL) != 0)
			return -1;
	}
	
	/// copy back path
	GetSystemDirectoryA(tmpdir,MAX_PATH);
	if (forcedoutname && _stricmp (coinstallername,forcedoutname) != 0)
	{
		strcpy_s(origpath,sizeof (origpath),tmpdir);
		PathAppendA(origpath,coinstallername);
		PathAppendA(tmpdir,forcedoutname);
		DeleteFile(tmpdir);
		MoveFile (origpath,tmpdir);
	}
	else
		strcat_s(tmpdir,sizeof (tmpdir),coinstallername);

	if (coinstallerpathsize < (int)(strlen (tmpdir) + 1))
	{
		DeleteFile (tmpdir);
		return -1;
	}
	strcpy_s (coinstallerpath,coinstallerpathsize,tmpdir);
	return 0;
}

/*
*	extract to %temp% and load the coinstaller, decrypt and get pointers. cipherkey must be binary. module must be freed with freelibrary
*   if coinstallerrsrc is null, it attempts to load the coinstaller from temp directory. coinstallerrsrc and forcedname are incompatible.
*/
HMODULE kmdf_setup_from_rsrc (OPTIONAL IN HMODULE module, OPTIONAL IN int coinstallerrsrc, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, OPTIONAL IN char* forcedname)
{
	char windir [MAX_PATH] = {0};
	char coinstallername [64] = {0};
	char path[MAX_PATH] = {0};
	HMODULE coinstmod = NULL;

	/// check if we have the coinstaller (on vista its already present, on late xp usually, so no need to include)
	if (coinstallerrsrc)
	{
		if (FindResource (module,MAKEINTRESOURCE(coinstallerrsrc),RT_RCDATA))
		{
			/// extract the wdf coinstaller
			GetTempPath(MAX_PATH,windir);
			if (drop_resource(module,coinstallerrsrc,windir,cipherkey, bits, FALSE, coinstallername,sizeof (coinstallername),FALSE,NULL) != 0)
				return NULL;
		}
	}

	/// load coinstaller and get pointers	
	GetSystemDirectory(windir,MAX_PATH);
	if (forcedname && _stricmp (forcedname,coinstallername) != 0)
		PathAppendA(windir,forcedname);
	else
		PathAppendA(windir,coinstallername);
	if (kmdf_load_coinstaller(windir,&coinstmod,(PULONG_PTR)&WdfPreDeviceInstallPtr,(PULONG_PTR)&WdfPostDeviceInstallPtr,(PULONG_PTR)&WdfPreDeviceRemovePtr,(PULONG_PTR)&WdfPostDeviceRemovePtr) != 0)
		return NULL;
	return coinstmod;
}

