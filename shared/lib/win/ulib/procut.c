/*
 *	process related utility functions
 *	-vx-
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include "procut.h"
#include <TlHelp32.h>
#include <sddl.h>
#include <strlib.h>
#include <wtsapi32.h>
#include <userenv.h>
#include <dbg.h>
#include <fcntl.h>
#include <io.h>

/*
 *	get function address in specified library (unicode). phModule must be freed with FreeLibrary by the caller if != NULL on return
 *
 */
PVOID proc_getprocaddressw (IN PWCHAR pDllName, IN PCHAR pFunctionName, OUT HMODULE* phModule)
{
	HMODULE hMod = NULL;
	PVOID pFunctionAddress = NULL;
	
	// check params
	if (!pDllName || !pFunctionName || !phModule)
		return NULL;
	*phModule = NULL;

	// try and see if the library is already mapped
	pFunctionAddress = (PVOID)GetProcAddress((GetModuleHandleW (pDllName)), pFunctionName);
	if (pFunctionAddress)
		return pFunctionAddress;

	// map library
	hMod = LoadLibraryW (pDllName);
	if (!hMod)
		return NULL;

	// get function address
	pFunctionAddress = GetProcAddress(hMod, pFunctionName);
	if (!pFunctionAddress)
	{
		// error ....
		FreeLibrary (hMod);	
		return NULL;
	}

	// ok
	*phModule = hMod;
	return pFunctionAddress;
}

/*
*	get function address in specified library. phModule must be freed with FreeLibrary by the caller if != NULL on return
*
*/
PVOID proc_getprocaddress (IN PCHAR pDllName, IN PCHAR pFunctionName, OUT HMODULE* phModule)
{
	WCHAR wpath [MAX_PATH];
	size_t numchar = 0;

	mbstowcs_s (&numchar,wpath,MAX_PATH,pDllName,_TRUNCATE);
	return proc_getprocaddressw(wpath,pFunctionName,phModule);
}

/*
*	detect if we're running on a full 64bit process.
*
*/
BOOL proc_is64bit()
{
#ifdef _WIN64
	return TRUE;
#endif
	return FALSE;
}

/*
 *	detect if we're running on a 64bit os
 *
 */
BOOL proc_is64bitOS()
{
	typedef UINT (WINAPI* LPFN_GETSYSTEMWOW64DIRECTORYW) (LPWSTR, UINT);
	LPFN_GETSYSTEMWOW64DIRECTORYW pgetsystemwow64directoryw = NULL;
	WCHAR sysdir [MAX_PATH];
	HMODULE mod = NULL;
	UINT res = 0;

	// get function (exists on xp+ only)
	pgetsystemwow64directoryw= proc_getprocaddress("kernel32.dll","GetSystemWow64DirectoryW",&mod);
	if (!pgetsystemwow64directoryw)	
		return FALSE;

	// call
	res = pgetsystemwow64directoryw(sysdir,MAX_PATH);
	if (mod)
		FreeLibrary(mod);

	if (res == 0)
	{
		if (GetLastError() == ERROR_CALL_NOT_IMPLEMENTED)
			return FALSE;
		return TRUE;
	}

	return TRUE;
}

/*
 *	returns TRUE if the user belongs to the administrators groups. BEWARE : on vista + UAC, process running with admin user != process elevated!
 *  -1 is retuned on error
 */
BOOL proc_isuseradmin ()
{
	BOOL b;
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup = NULL; 
	
	// create a sid
	if (!AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup))
		return -1;

	// check if the current user belongs to the group
	if (!CheckTokenMembership( NULL, AdministratorsGroup, &b)) 
	{
		FreeSid(AdministratorsGroup); 
		return -1;
	}
	
	FreeSid(AdministratorsGroup);
	return b;
}

/*
 *	returns TRUE if running on vista or later
 *
 */
BOOL proc_isvistaos ()
{
	// get os version
	BOOL isvista = proc_verifyosversion(6,0,0,0,VER_GREATER_EQUAL);
	if (isvista)
		return TRUE;
	return FALSE;

}

/*
 *	returns TRUE if running windows xp or later
 *
 */
BOOL proc_iswxpos ()
{
	// get os version
	BOOL iswxp = proc_verifyosversion(5,1,0,0,VER_GREATER_EQUAL);
	if (iswxp)
		return TRUE;
	return FALSE;
}

/*
 *	returns elevation type of the current process. always return -1 on non-vista
 *
 */
int proc_getelevationtype (OUT TOKEN_ELEVATION_TYPE* ptet)
{
	HANDLE hToken	= NULL;
	DWORD dwReturnLength = 0;

	if (!ptet || !proc_isvistaos())
		return -1;

	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken ))
	{
		DBG_OUT (("proc_getelevationtype openprocesstoken getlasterror=%x\n",GetLastError()));
		return -1;
	}

	if (!GetTokenInformation(hToken, TokenElevationType, ptet, sizeof(TOKEN_ELEVATION_TYPE), &dwReturnLength ))
	{
		DBG_OUT (("proc_getelevationtype gettokeninformation getlasterror=%x\n",GetLastError()));
		CloseHandle(hToken);
		return -1;
	}

	CloseHandle(hToken);
	return 0;
}

/*
*	check if the current process has administrator privileges, either on vista or xp (on vista, UAC presence is handled)
*
*/
BOOL proc_hasadminprivileges ()
{
	TOKEN_ELEVATION_TYPE tevtype;

	// handle non-vista case
	if (!proc_isvistaos())
	{
		if (!proc_isuseradmin())
			return FALSE;
		else
			return TRUE;
	}

	// on vista, we check elevation type handling UAC too
	if (proc_getelevationtype(&tevtype) != 0)
		return FALSE;

	switch (tevtype)
	{
		// token isn't split, either UAC is disabled or user isnt admin. so we check if the user belongs to admin group
		case TokenElevationTypeDefault:
			if (!proc_isuseradmin())
				return FALSE;
			else
				return TRUE;
		break;

		// token is a split token, we have privileges
		case TokenElevationTypeFull:
			return TRUE;
		break;

		// UAC is on and process isn't elevated
		case TokenElevationTypeLimited:
			return FALSE;
		break;

	default:
		break;
	}

	return FALSE;
}

/*
 *	returns TRUE if the os is windows vista AND the process is elevated. optionally, it returns TRUE in isvista if process is running on vista
 *  returns -1 on error
 */
int proc_isvistaelevated (OPTIONAL OUT PBOOL isvista)
{
	BOOL vistaos = FALSE;
	HRESULT hResult = E_FAIL;
	HANDLE hToken	= NULL;
	TOKEN_ELEVATION te = { 0 };
	DWORD dwReturnLength = 0;
	int res = -1;

	// get os version
	vistaos = proc_isvistaos();
	if (isvista)
		*isvista = vistaos;
	
	// return false if we're not running vista
	if (!vistaos)
		return FALSE;
	
	// check elevation
	if (!OpenProcessToken(GetCurrentProcess(),TOKEN_QUERY,&hToken))
		return -1;

	if (!GetTokenInformation(hToken, TokenElevation, &te, sizeof( te ), &dwReturnLength ))
		goto __exit;

	if (te.TokenIsElevated)
		res = TRUE;
	else
		res = FALSE;

__exit:
	if (hToken)
		CloseHandle(hToken);
	return res;
}

/*
 *	verify if the current os meets the requirements (major/minor version, major/minor servicepack version). normally the check succeeds if running os
 *  version is greater-than specified.
 */
BOOL proc_verifyosversion(IN int majorversion, OPTIONAL IN int minorversion, OPTIONAL IN int spmajor, OPTIONAL IN int spminor, OPTIONAL IN int condition)
{
	OSVERSIONINFOEX versioninfoex;
	DWORDLONG cond = 0;
	BYTE op = (BYTE)condition;

	if (!condition)
		op = VER_GREATER_EQUAL;

	// set desired values
	memset (&versioninfoex,0,sizeof (OSVERSIONINFOEX));
	versioninfoex.dwOSVersionInfoSize = sizeof (OSVERSIONINFOEX);
	versioninfoex.dwMajorVersion = majorversion;
	versioninfoex.dwMinorVersion = minorversion;
	versioninfoex.wServicePackMajor = (USHORT)spmajor;
	versioninfoex.wServicePackMinor = (USHORT)spminor;
	
	// set conditions
	VER_SET_CONDITION( cond, VER_MAJORVERSION, op );
	VER_SET_CONDITION( cond, VER_MINORVERSION, op );
	VER_SET_CONDITION( cond, VER_SERVICEPACKMAJOR, op );
	VER_SET_CONDITION( cond, VER_SERVICEPACKMINOR, op );

	// check
	return VerifyVersionInfo(&versioninfoex, VER_MAJORVERSION|VER_MINORVERSION|VER_SERVICEPACKMAJOR|VER_SERVICEPACKMINOR,cond);
}

/*
*	detect if the specified process is a WOW64 process (32bit process on 64bit OS). if it returns false, its a 64bit process running on 64bit OS
*
*/
BOOL proc_is64bitWOW(IN HANDLE process)
{
	BOOL iswow64 = FALSE;
	BOOL res = FALSE;
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
	LPFN_ISWOW64PROCESS  pIsWow64Process = NULL;
	HMODULE mod = NULL;

	// get function
	pIsWow64Process = proc_getprocaddress("kernel32.dll","IsWow64Process",&mod);
	if (!pIsWow64Process)
		return FALSE;

	pIsWow64Process(process,&iswow64);
	if (iswow64)
		res = TRUE;

	if (mod)
		FreeLibrary(mod);
	return res;
}

/*
*	detect if the specified process is a WOW64 process (32bit process on 64bit OS) by pid. if it returns false, its a 64bit process running on 64bit OS
*
*/
BOOL proc_is64bitWOWbypid(IN ULONG_PTR pid)
{
	HANDLE hp = NULL;
	BOOL res = FALSE;

	// open process
	hp = OpenProcess (PROCESS_QUERY_INFORMATION, FALSE, (DWORD)pid);
	if (!hp)
		return FALSE;
	
	// detect 64bit wow
	res = proc_is64bitWOW(hp);
	CloseHandle(hp);
	return res;
}

/*
 *	set null (everyone) dacl on security attributes and descriptor
 *
 */
int proc_initializenulldacl (IN PSECURITY_ATTRIBUTES attribs, IN PSECURITY_DESCRIPTOR desc)
{
	PSECURITY_DESCRIPTOR pSD = NULL;
	PACL pSacl = NULL;                  // not allocated
	BOOL fSaclPresent = FALSE;
	BOOL fSaclDefaulted = FALSE;
	int res = -1;

	if (!attribs || !desc)
		return -1;

	/* create a security descriptor for everyone */
	InitializeSecurityDescriptor(desc, SECURITY_DESCRIPTOR_REVISION);
	if (!SetSecurityDescriptorDacl(desc, TRUE, NULL, FALSE))
		return -1;

	attribs->nLength = sizeof(SECURITY_ATTRIBUTES);
	attribs->bInheritHandle = FALSE;
	attribs->lpSecurityDescriptor = desc;

	if (proc_isvistaos())
	{
		/* set low-integrity on its sacl (this is needed on vista) */
		if (!ConvertStringSecurityDescriptorToSecurityDescriptor("S:(ML;;NW;;;LW)", SDDL_REVISION_1, &pSD, NULL))
			return -1;
		if (!GetSecurityDescriptorSacl(pSD, &fSaclPresent, &pSacl, &fSaclDefaulted))
			goto __exit;
		if (!SetSecurityDescriptorSacl(attribs->lpSecurityDescriptor, TRUE, pSacl, FALSE))
			goto __exit;
	}

	/* ok */
	res = 0;

__exit:
	if (pSD)
		LocalFree(pSD);

	return res;
}

/*
*	createremotethread replacement (uses an undocumented function to let it work on vista, calls standard api on xp/2k). *undoc call works on xp/vista32/64*
*
*/
HANDLE proc_createremotethread (HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress,
								LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId)
{
	typedef HANDLE (WINAPI *_CreateRemoteThread)(HANDLE hProcess,
		__in_opt  LPSECURITY_ATTRIBUTES lpThreadAttributes,
		__in      SIZE_T dwStackSize,
		__in      LPTHREAD_START_ROUTINE lpStartAddress,
		__in_opt  LPVOID lpParameter,
		__in      DWORD dwCreationFlags,
		__out_opt LPDWORD lpThreadId
		); 
	
	typedef NTSTATUS (NTAPI *RtlCreateUserThread)(HANDLE ProcessHandle, PSECURITY_DESCRIPTOR SecurityDescriptor, IN BOOLEAN CreateSuspended, 
		IN ULONG StackZeroBits, ULONG StackReserved, ULONG StackCommit, PVOID StartAddress, PVOID StartParameter, PHANDLE ThreadHandle, PCLIENT_ID ClientID ); 

	HANDLE th = NULL;
	HMODULE ntdll = NULL;
	HMODULE kernel32 = NULL;
	OSVERSIONINFO osinfo;
	RtlCreateUserThread pRtlCreateUserThread = NULL;
	_CreateRemoteThread pCreateRemoteThread = NULL;
	BOOLEAN suspended = FALSE;
	NTSTATUS Status = 0;
	PSECURITY_DESCRIPTOR secdesc = NULL;
	CLIENT_ID cid;

	/* check os version */
	osinfo.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	if (!GetVersionEx (&osinfo))
		return NULL;
	
	/* use standard api if < vista */
 	if (osinfo.dwMajorVersion < 6)
	{
		pCreateRemoteThread = (_CreateRemoteThread) proc_getprocaddress("kernel32.dll","CreateRemoteThread",&kernel32);
		if (!pCreateRemoteThread)
			return NULL;

 		th = pCreateRemoteThread (hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
		if (kernel32)
			FreeLibrary(kernel32);
		return th;
	}

	/* get function from ntdll */
	pRtlCreateUserThread = (RtlCreateUserThread) proc_getprocaddress("ntdll.dll","RtlCreateUserThread",&ntdll);
	if (!pRtlCreateUserThread)
		return NULL;
	
	/* create thread */
	if (dwCreationFlags & CREATE_SUSPENDED)
		suspended = TRUE;
	if (lpThreadAttributes)
		secdesc = lpThreadAttributes->lpSecurityDescriptor;

	Status = pRtlCreateUserThread(hProcess, secdesc, suspended, 0, 0, (ULONG)dwStackSize, lpStartAddress, lpParameter, &th, &cid);
	
	if (ntdll)
		FreeLibrary (ntdll);
	return th;
}

/*
*	inject dll (ansi) into process via CreateRemoteThread/LoadLibrary calls. if timeout is specified, timeout (seconds) is waited before injection
*
*/
int proc_inject_dll (IN ULONG_PTR pid, IN char* dllpath, IN int timeout)
{
	WCHAR wname [MAX_PATH*2];
	size_t numchar = 0;

	if (!pid || !dllpath)
		return -1;

	mbstowcs_s (&numchar,wname,MAX_PATH*2,dllpath,_TRUNCATE);
	return proc_inject_dllw(pid,wname,timeout);
}

/*
 *	check if a module is present in the specified process. module name can be barename or fullpath
 *
 */
int proc_is_module_presentw (IN HANDLE process, IN PWCHAR modulename)
{
	PWCHAR barename = NULL;
	PWCHAR modbarename = NULL;
	HMODULE modules [1024] = {0}; // enough
	ULONG cbneeded = 0;
	WCHAR modname [MAX_PATH] = {0};
	int i = 0;
	int num = 0;
	if (!process || !modulename)
		return 0;
	
	// get to the barename if its a full path
	barename = wcsrchr(modulename,(WCHAR)'\\');
	if (barename)
		barename++;
	else
		barename = modulename;

	// enumerate modules
	if (EnumProcessModules(process,modules,sizeof(modules),&cbneeded) == 0)
		return 0;
	
	// check if the module is present
	num = (int) (cbneeded / sizeof (HMODULE));
	for ( i = 0; i < num; i++)
	{
		// get path
		if (GetModuleFileNameExW(process,modules[i],modname,MAX_PATH))
		{
			modbarename = wcsrchr(modname,(WCHAR)'\\');
			if (modbarename)
				modbarename++;
			else
				modbarename = (PWCHAR)modname;
			
			// check if its the same as our modulename
			if (_wcsicmp(modbarename,barename) == 0)
				return 1;
		}
	}
	
	return 0;
}

/*
*	check if a module is present in the specified process. module name can be barename or fullpath
*
*/
int proc_is_module_present (IN HANDLE process, IN PCHAR modulename)
{
	WCHAR wname [MAX_PATH];
	size_t numchar = 0;

	if (!process || !modulename)
		return -1;

	mbstowcs_s (&numchar,wname,MAX_PATH,modulename,_TRUNCATE);
	return proc_is_module_presentw(process,wname);
}

/*
*	inject dll (unicode) into process via CreateRemoteThread/LoadLibrary calls. if timeout is specified, timeout (seconds) is waited before injection
*
*/
int proc_inject_dllw (IN ULONG_PTR pid, IN PWCHAR dllpath, IN int timeout)
{
	HANDLE hp = NULL;
	unsigned char* ctxmem = NULL;
	SIZE_T written = 0;
	HANDLE ht = NULL;
	ULONG tid = 0;
	int res = -1;
	HANDLE hf = INVALID_HANDLE_VALUE;
	int len = 0;
	HMODULE krnl32 = NULL;
	void* loadlibfunc = NULL;
	static void* loadlibfuncstatic;
	typedef BOOL (WINAPI *WRITEPROCESSMEMORY)(HANDLE hProcess,LPVOID lpBaseAddress,LPCVOID lpBuffer,SIZE_T nSize,SIZE_T *lpNumberOfBytesWritten);
	WRITEPROCESSMEMORY pWriteProcessMemory;

	if (!pid || !dllpath)
		return -1;

	// check if dll exists on disk
	hf = CreateFileW (dllpath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hf == INVALID_HANDLE_VALUE)
	{
		// file do not exist
		DBG_OUT(("procut : %S can't be opened (getlasterror=%x)\n",dllpath,GetLastError()));
		return -1;
	}
	CloseHandle(hf);

	/// set debug privileges
	if (proc_setcurrentprocess_privileges(SE_DEBUG_NAME) != 0)
		return -1;

	// open process
	hp = OpenProcess (PROCESS_CREATE_THREAD|PROCESS_QUERY_INFORMATION|PROCESS_VM_WRITE|PROCESS_VM_READ|PROCESS_VM_OPERATION, FALSE, (DWORD)pid);
	if (!hp)
	{
		DBG_OUT (("proc_injectdllw openprocess failed, getlasterror=%x\n",GetLastError()));
		goto __exit;
	}

	// check if module is already present in the target process
	if (proc_is_module_presentw(hp,dllpath))
	{
		DBG_OUT (("proc_injectdllw module already present in the target process\n"));
		goto __exit;
	}

	/// allocate memory in the target process
	len = (int)wcslen(dllpath) * sizeof (WCHAR);
	ctxmem = (PUCHAR)VirtualAllocEx (hp, NULL, len + 32, MEM_COMMIT, PAGE_READWRITE);
	if (!ctxmem)
	{
		DBG_OUT (("proc_injectdllw virtualallocex failed, getlasterror=%x\n",GetLastError()));
		goto __exit;
	}

	// copy dllname in target process
	pWriteProcessMemory = (WRITEPROCESSMEMORY) proc_getprocaddress("kernel32.dll","WriteProcessMemory",&krnl32);
	if (!pWriteProcessMemory)
	{
		DBG_OUT (("proc_injectdllw failed getprocaddress, getlasterror=%x\n",GetLastError()));
		goto __exit;
	}
	if (!pWriteProcessMemory (hp, ctxmem, (PBYTE)dllpath, len, &written))
	{
		DBG_OUT (("proc_injectdllw writeprocessmemory, getlasterror=%x\n",GetLastError()));
		goto __exit;
	}
	if (krnl32)
		FreeLibrary(krnl32);
	
	// wait timeout before injection
	if (timeout)
		Sleep (timeout*1000);

	if (!loadlibfuncstatic)
	{
		// we use a static vars because of shim.dll apparently gets loaded sometimes on w2k, screwing all.
		// getting the function pointer at the beginning seems to solve the issue.
		loadlibfuncstatic = proc_getprocaddress("kernel32.dll","LoadLibraryW",&krnl32);
		if (!loadlibfuncstatic)
		{
			DBG_OUT (("proc_injectdllw getprocaddress loadlibraryw failed, getlasterror=%x\n",GetLastError()));
			goto __exit;
		}
		if (krnl32)
			FreeLibrary(krnl32);
	}

	// inject thread
	ht = proc_createremotethread (hp, NULL, 0, (LPTHREAD_START_ROUTINE)loadlibfuncstatic, ctxmem, 0, &tid);
	if (!ht)
		goto __exit;

	// wait for thread to terminate
	WaitForSingleObject(ht,5*1000);

	/// ok
	res = 0;

__exit:
	DBG_OUT(("proc_injectdllw : injecting %S in PID %x, res %d, getlasterror %x\n", dllpath,pid,res,GetLastError()));

	if (ht)
		CloseHandle (ht);

	if (ctxmem)
		VirtualFreeEx (hp, ctxmem, 0, MEM_RELEASE);

	if (hp)
		CloseHandle(hp);
	return res;
}

/*
 *	free list allocated with proc_getprocesseslist
 *
 */
void proc_freeprocesseslist (IN LIST_ENTRY* processes)
{
	void* p;

	while (!IsListEmpty(processes))
	{
		p = RemoveHeadList (processes);
		if (p)
			free(p);
	}
}

/*
 *	get list of running processes using toolhelp32 functions
 *
 */
int proc_getprocesseslist (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses)
{
	HANDLE	hSnapshot	= INVALID_HANDLE_VALUE;
	procentry* entry = NULL;
	int res = -1;
	int	num = 0;

	// check params
	if (!processes)
		goto __exit;
	if (numprocesses)
		*numprocesses = 0;

	InitializeListHead(processes);

	/// get current sys snapshot
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE)
		return res;

	/// walk snapshot
	entry = calloc (1,sizeof (procentry));
	if (!entry)
		goto __exit;
	
	entry->processentry.dwSize = sizeof (PROCESSENTRY32);
	if (Process32First (hSnapshot,&entry->processentry) == 0)
	{
		free (entry);
		goto __exit;
	}

	while (TRUE)
	{
		/// add to list
		InsertTailList(processes,&entry->chain);
		num++;

		/// allocate and get next
		entry = calloc (1,sizeof (procentry));
		if (!entry)
			break;
		
		// get next
		entry->processentry.dwSize = sizeof (PROCESSENTRY32);
		if (Process32Next (hSnapshot,&entry->processentry) == 0)
		{
			if (GetLastError() == ERROR_NO_MORE_FILES)
			{
				/// ok
				res = 0;
			}
			else
			{
				/// free all
				proc_freeprocesseslist(processes);
			}
			break;
		}
	}
	
	if (numprocesses)
		*numprocesses = num;

__exit:
	if (hSnapshot)
		CloseHandle(hSnapshot);

	if (res != 0 && numprocesses)
		*numprocesses = 0;
	return res;
}

/*
 *	returns process pid from name/path
 *
 */
DWORD proc_getpidbyname (IN char* processname, IN BOOL issubstring)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	char name [MAX_PATH];
	DWORD	dwPid = 0;
	char* p = NULL;
	char* q = NULL;

	if (!processname)
		return 0;

	/// get processes list
	if (proc_getprocesseslist (&processes, NULL) != 0)
		return 0;

	/// walk entries
	strcpy_s (name,sizeof (name),processname);
	
	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;
	
		/// check processname
		p = strrchr (name,'\\');
		if (p)
			p++;
		else
			p = (char*)&name;
		q = strrchr (entry->processentry.szExeFile,'\\');
		if (q)
			q++;
		else
			q = (char*)&entry->processentry.szExeFile;

		if (issubstring)
		{
			if (strstr (q,p))
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}
		else
		{
			if (_stricmp (p,q) == 0)
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return dwPid;
}

/*
*	returns number of injected processes
*
*/
int proc_injectprocesses (IN char* processname, IN BOOL issubstring, IN char* dllpath, OPTIONAL IN int timeout)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	char name [MAX_PATH];
	DWORD	dwPid = 0;
	char* p = NULL;
	char* q = NULL;
	int count = -1;

	if (!processname || !dllpath)
		return -1;

	/// get processes list
	if (proc_getprocesseslist (&processes, NULL) != 0)
		return -1;

	/// walk entries
	strcpy_s (name,sizeof (name),processname);
	count = 0;
	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		/// check processname
		p = strrchr (name,'\\');
		if (p)
			p++;
		else
			p = (char*)&name;
		q = strrchr (entry->processentry.szExeFile,'\\');
		if (q)
			q++;
		else
			q = (char*)&entry->processentry.szExeFile;

		if (issubstring)
		{
			if (strstr (q,p))
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				if (proc_inject_dll(dwPid,dllpath,timeout) == 0)
					count++;
			}
		}
		else
		{
			if (_stricmp (p,q) == 0)
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				if (proc_inject_dll(dwPid,dllpath,timeout) == 0)
					count++;
			}
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return count;
}

/*
 *	set/unset specified privilege on current token
 *
 */
int proc_setprivilege (IN HANDLE hToken, IN LPCTSTR lpszPrivilege, IN BOOL enable)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;
	BOOL res = FALSE;

	if (!hToken || !lpszPrivilege)
		return -1;

	/// lookup privilege
	if ( !LookupPrivilegeValue(NULL, lpszPrivilege, &luid ) )
		return -1;

	/// Enable the privilege or disable all privileges.
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (enable)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	if ( !AdjustTokenPrivileges(hToken,  FALSE, &tp,  sizeof(TOKEN_PRIVILEGES), NULL, NULL) )
	{
		DBG_OUT (("proc_setprivilege adjusttokenprivilege failed, getlasterror=%x\n",GetLastError()));
		return -1;
	}
	
	return 0;
}

/*
*	set privileges on the current process
*
*/
int proc_setcurrentprocess_privileges(IN LPCTSTR privilegename)
{
	HANDLE hToken = NULL;
	int res = -1;

	if (!privilegename)
		return -1;

	/// get this process token
	if (!OpenProcessToken (GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
	{
		DBG_OUT (("proc_setcurrentprocessprivileges failed"));
		return -1;
	}

	// set privilege
	res = proc_setprivilege (hToken,privilegename,TRUE);
	if (res != 0)
		goto __exit;

__exit:
	if (hToken)
		CloseHandle(hToken);
	return res;
}

/* 
 *	get pModuleName full path (pass NULL for current process) (unicode). returns number of wchars copied.
 *
 */
int proc_wgetmodulepath (OPTIONAL IN PWCHAR pModuleName, OUT PWCHAR outpath, IN int outpathwsize)
{
	WCHAR szProcessName [MAX_PATH];
	int len = 0;

	// check params
	if (!outpath || !outpathwsize)
		return 0;

	// get module file name
	if (GetModuleFileNameW(GetModuleHandleW(pModuleName),szProcessName,MAX_PATH) == 0)
		return 0;

	// get string size
	len = (int)wcslen (szProcessName);
	if (len > outpathwsize)
		return 0;

	// ok
	wcscpy_s (outpath,outpathwsize,szProcessName);
	return len;
}

/* 
*	get pModuleName full path (pass NULL for current process). returns number of chars copied.
*
*/
int proc_getmodulepath (OPTIONAL IN PCHAR pModuleName, OUT PCHAR outpath, IN int outpathsize)
{
	WCHAR woutpath [MAX_PATH];
	WCHAR wmodulename [MAX_PATH];
	PWCHAR pw = NULL;
	size_t numchar = 0;

	if (pModuleName)
	{
		mbstowcs_s (&numchar,wmodulename,MAX_PATH,pModuleName,_TRUNCATE);
		pw = (PWCHAR)wmodulename;
	}

	if (proc_wgetmodulepath (pw,woutpath,MAX_PATH) == 0)
		return 0;

	wcstombs_s (&numchar,outpath,outpathsize,woutpath,_TRUNCATE);

	return (int)strlen (outpath);
}

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted. unicode
*
*/
int proc_getnamebypidw (IN DWORD pid, OUT PWCHAR processname, IN int namelen, IN int nameonly)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	int len = 0;
	size_t numchar = 0;
	PCHAR p = NULL;

	if (!processname || !pid || !namelen)
		return 0;

	/// get processes list
	if (proc_getprocesseslist (&processes, NULL) != 0)
		return 0;

	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		if (pid == entry->processentry.th32ProcessID)
		{
			if (nameonly)
			{
				// get bare name only
				p = strrchr(entry->processentry.szExeFile,'\\');
				if (p)
				{
					p++;
					mbstowcs_s (&numchar,processname,namelen,p,_TRUNCATE);
				}
				else
					mbstowcs_s (&numchar,processname,namelen,entry->processentry.szExeFile,_TRUNCATE);
			}
			else
				mbstowcs_s (&numchar,processname,namelen,entry->processentry.szExeFile,_TRUNCATE);
		
			len = (int)wcslen(processname);
			break;
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return len;
}

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted
*
*/
int proc_getnamebypid (IN DWORD pid, OUT PCHAR processname, IN int namelen, IN int nameonly)
{
	WCHAR name [MAX_PATH];
	size_t numchar = 0;
	
	if (!processname || !pid || !namelen)
		return 0;

	if (!proc_getnamebypidw (pid,name,MAX_PATH,nameonly))
		return 0;
	wcstombs_s (&numchar,processname,namelen,name,_TRUNCATE);

	return (int)strlen (processname);
}

/* 
*	get name (bare name only) of the module which has focus. returns number of wchars copied.
*
*/
int proc_getmodulename_foregroundw (OUT PWCHAR outname, IN int outnamesize)
{
	HWND wnd = NULL;
	DWORD pid = 0;

	if (!outname || !outnamesize)
		return 0;

	// get window which has foreground
	wnd = GetForegroundWindow();
	if (!wnd)
		return 0;

	GetWindowThreadProcessId(wnd,&pid);
	return proc_getnamebypidw(pid,outname,outnamesize,TRUE);
}

/* 
*	get name (bare name only) of the module which has focus. returns number of chars copied.
*
*/
int proc_getmodulename_foreground (OUT PCHAR outname, IN int outnamesize)
{
	WCHAR woutname [MAX_PATH];
	size_t numchar = 0;

	if (!outname || !outnamesize)
		return 0;
	
	if (!proc_getmodulename_foregroundw(woutname,MAX_PATH))
		return 0;

	wcstombs_s (&numchar,outname,outnamesize,woutname,_TRUNCATE);

	return (int)strlen (outname);
}

/* 
*	get pModuleName name (just bare name) (pass NULL for current process) (unicode). returns number of wchars copied.
*
*/
int proc_wgetmodulename (OPTIONAL IN PWCHAR pModuleName, OUT PWCHAR outname, IN int outnamewsize)
{
	WCHAR szProcessName [MAX_PATH];
	WCHAR szFullPath [MAX_PATH];

	int len = 0;

	// check params
	if (!outname || !outnamewsize)
		return 0;

	// get module path
	if (proc_wgetmodulepath (pModuleName,szFullPath,MAX_PATH) == 0)
		return 0;

	// get module file name
	if (str_getnamefrompathw (szFullPath,szProcessName,MAX_PATH) == 0)
		return 0;

	// get string size
	len = (int)wcslen (szProcessName);
	if (len > outnamewsize)
		return 0;

	// ok
	wcscpy_s (outname,outnamewsize,szProcessName);
	return len;
}

/* 
*	get pModuleName name (just bare name) (pass NULL for current process). returns number of chars copied.
*
*/
int proc_getmodulename (OPTIONAL IN PCHAR pModuleName, OUT PCHAR outname, IN int outnamesize)
{
	WCHAR woutname [MAX_PATH];
	WCHAR wmodulename [MAX_PATH];
	PWCHAR pw = NULL;
	size_t numchar = 0;

	if (pModuleName)
	{
		mbstowcs_s (&numchar,wmodulename,MAX_PATH,pModuleName,_TRUNCATE);
		pw = (PWCHAR)wmodulename;
	}

	if (proc_wgetmodulename (pw,woutname,MAX_PATH) == 0)
		return 0;

	wcstombs_s (&numchar,outname,outnamesize,woutname,_TRUNCATE);

	return (int)strlen (outname);
}

/*
*	execute process specified by path, optionally waits for termination. returns process exitcode if wait is specified
*
*/
ULONG proc_execw (IN PWCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide)
{
	STARTUPINFOW sinfo;
	PROCESS_INFORMATION pinfo;
	ULONG res = -1;

	memset (&sinfo,0,sizeof (STARTUPINFOW));
	memset (&pinfo,0,sizeof (PROCESS_INFORMATION));
	sinfo.cb = sizeof (STARTUPINFOW);

	/* check if it must be hidden */
	if (hide)
	{
		sinfo.dwFlags = STARTF_USESHOWWINDOW;
		sinfo.wShowWindow = SW_HIDE;
	}

	/* exec process */
	if (!CreateProcessW (NULL,path,NULL,NULL,FALSE,0,NULL,NULL,&sinfo,&pinfo))
		return -1;
	
	// ok
	res = 0;

	/* wait for termination if asked */
	if (wait)
	{
		WaitForSingleObject (pinfo.hProcess,INFINITE);
		GetExitCodeProcess(pinfo.hProcess,&res);
	}

	DBG_OUT (("proc_execw result : %d\n",res));
	
	/* close handles */
	if (pinfo.hProcess)
		CloseHandle(pinfo.hProcess);
	if (pinfo.hThread)
		CloseHandle(pinfo.hThread);
	return res;

}

/*
*	execute process specified by path, optionally waits for termination (ansi). returns process exitcode if wait is specified
*
*/
ULONG proc_exec (IN PCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide)
{
	WCHAR wpath [MAX_PATH*2];
	size_t numchar = 0;

	if (!path)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH*2,path,_TRUNCATE);
	return proc_execw(wpath,wait,hide);
}

/*
 *	kill process knowing pid. may fail if the process can't be opened for termination
 *
 */
int proc_kill_by_pid (IN ULONG pid)
{
	HANDLE proc = NULL;
	int res = -1;

	if (!pid)
		return -1;

	/// open process for termination
	proc = OpenProcess (PROCESS_TERMINATE,FALSE,pid);
	if (!proc)
		return -1;
	if (TerminateProcess(proc,0))
		res = 0;

	DBG_OUT (("proc_kill_by_pid res=%d\n",res));
	
	if (proc)
		CloseHandle(proc);
	return res;
}

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_name (IN PCHAR processname, IN BOOL killallinstances)
{
	ULONG pid = 0;
	int count = 0;

	if (!processname)
		return -1;

	if (!killallinstances)
	{
		// single instance
		pid = proc_getpidbyname(processname,FALSE);
		return proc_kill_by_pid(pid);
	}

	while (TRUE)
	{
		// get pid
		pid = proc_getpidbyname(processname,FALSE);
		if (!pid)
			break;
		if (proc_kill_by_pid(pid) == 0)
			count++;
	}

	if (count > 0)
		return 0;
	return -1;
}

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_namew (IN PWCHAR processname, IN BOOL killallinstances)
{
	size_t numchar = 0;
	CHAR name [MAX_PATH];

	if (!processname)
		return -1;
	wcstombs_s (&numchar,name,MAX_PATH,processname,_TRUNCATE);
	return proc_kill_by_name(name,killallinstances);
}

/*
*	execute process specified by path in the current user session, optionally waits for termination. returns exitcode if wait is specified
*
*/
ULONG proc_exec_as_logged_userw (IN PWCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide)
{
	STARTUPINFOW sinfo;
	PROCESS_INFORMATION pinfo;
	HANDLE usertoken = NULL;
	HANDLE dupetoken = NULL;
	ULONG res = -1;
	DWORD session = 0;
	void* env = NULL;
	typedef BOOL (WINAPI *LPFNWTSGETACTIVECONSOLESESSIONID) (VOID);
	typedef BOOL (WINAPI *LPFNWTSQUERYUSERTOKEN)(ULONG, PHANDLE);
	LPFNWTSGETACTIVECONSOLESESSIONID pwtsgetactiveconsolesessionid = NULL;
	LPFNWTSQUERYUSERTOKEN pwtsqueryusertoken = NULL;
	HMODULE mod1 = NULL;
	HMODULE mod2 = NULL;
	
	// get functions (exists only on w2k+)
	pwtsgetactiveconsolesessionid = proc_getprocaddress("kernel32.dll","WTSGetActiveConsoleSessionId",&mod1);
	pwtsqueryusertoken = proc_getprocaddress("wtsapi32.dll","WTSQueryUserToken",&mod2);
	if (!pwtsqueryusertoken || !pwtsgetactiveconsolesessionid)
	{
		// probably on 2k, revert on standard createprocess
		if (mod1)
			FreeLibrary(mod1);
		if (mod2)
			FreeLibrary(mod2);
		return proc_execw (path,wait,hide);		
	}
	
	/* get current session */
	session = pwtsgetactiveconsolesessionid();
	if (session == 0xffffffff)
		goto __exit;

	/* get user token */
	if (!pwtsqueryusertoken(session,&usertoken))
		goto __exit;

	/* duplicate */
	if (!DuplicateTokenEx(usertoken, MAXIMUM_ALLOWED, NULL, SecurityIdentification, TokenPrimary, &dupetoken))
		goto __exit;

	/* create environment block */
	if (!CreateEnvironmentBlock (&env,dupetoken,FALSE))
		goto __exit;

	memset (&sinfo,0,sizeof (STARTUPINFOW));
	memset (&pinfo,0,sizeof (PROCESS_INFORMATION));
	sinfo.cb = sizeof (STARTUPINFOW);

	/* check if it must be hidden */
	if (hide)
	{
		sinfo.dwFlags = STARTF_USESHOWWINDOW;
		sinfo.wShowWindow = SW_HIDE;
	}

	/* exec process */
	if (!CreateProcessAsUserW (dupetoken,NULL,path,NULL,NULL,FALSE,CREATE_UNICODE_ENVIRONMENT,env,NULL,&sinfo,&pinfo))
		goto __exit;

	// ok
	res = 0;

	/* wait for termination if asked */
	if (wait)
	{
		WaitForSingleObject (pinfo.hProcess,INFINITE);
		GetExitCodeProcess(pinfo.hProcess,&res);
	}

__exit:
	/* close handles */
	if (pinfo.hProcess)
		CloseHandle(pinfo.hProcess);
	if (pinfo.hThread)
		CloseHandle(pinfo.hThread);
	if (usertoken)
		CloseHandle(usertoken);
	if (dupetoken)
		CloseHandle(dupetoken);
	if (env)
		DestroyEnvironmentBlock(env);
	if (mod1)
		FreeLibrary(mod1);
	if (mod2)
		FreeLibrary(mod2);
	DBG_OUT (("proc_execasloggeduserw result : %d\n",res));
	return res;
}

/*
*	execute process specified by path in the current user session, optionally waits for termination(ansi). returns exitcode if wait is specified
*
*/
ULONG proc_exec_as_logged_user (IN PCHAR path, OPTIONAL IN int wait, OPTIONAL IN int hide)
{
	WCHAR wpath [MAX_PATH*2];
	size_t numchar = 0;

	if (!path)
		return -1;

	mbstowcs_s (&numchar,wpath,MAX_PATH*2,path,_TRUNCATE);
	return proc_exec_as_logged_userw(wpath,wait,hide);
}

/*
*	detach console attachment done with proc_win32consoleoutput_attach
*
*/
void proc_win32consoleoutput_detach()
{
	typedef BOOL (WINAPI *LPFN_FREECONSOLE) (VOID);
	LPFN_FREECONSOLE pfreeconsole = NULL;
	HMODULE mod = NULL;

	// get function pointer
	pfreeconsole = proc_getprocaddress("kernel32.dll","FreeConsole",&mod);
	if (!pfreeconsole)
		return;

	__try
	{
		fflush (stdin);
		fclose (stdin);
		fflush (stderr);
		fclose (stderr);
		fflush (stdout);
		fclose (stdout);
		pfreeconsole();

		// simulate "enter" press
		keybd_event(VK_RETURN, 0, 0, 0);
		keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{

	}
	
	if (mod)
		FreeLibrary(mod);
	return;
}

/*
*	permits to a win32 application to output on console if called from cmd.exe. returns 0 if succeeds
*
*/
int proc_win32consoleoutput_attach()
{
	typedef BOOL (WINAPI *LPFN_ATTACHCONSOLE) (DWORD);
	LPFN_ATTACHCONSOLE pattachconsole = NULL;
	HMODULE mod = NULL;
	int hConHandle = 0;
	HANDLE lStdHandle = 0;
	FILE *fp = NULL;
	int res = -1;

	// get function pointer (exists only on xp+)
	pattachconsole = proc_getprocaddress("kernel32.dll","AttachConsole",&mod);
	if (!pattachconsole)
		return 0;

	__try
	{
		// attach to the calling process console
		pattachconsole(-1);

		// redirect unbuffered STDOUT to the console
		lStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "w" );
		*stdout = *fp;

		// redirect unbuffered STDIN to the console
		lStdHandle = GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "r" );
		*stdin = *fp;

		// redirect unbuffered STDERR to the console
		lStdHandle = GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle((intptr_t)lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "w" );
		*stderr = *fp;
		res = 0;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		res = -1;
	}
	
	if (mod)
		FreeLibrary(mod);
	return res;
}

/*
 *	missing implementation from the win32api .... pass GetCommandLine() for the 1st param, returns argv. returned array must be freed with globalfree
 *
 */
PCHAR* proc_CommandLineToArgvA(PCHAR CmdLine, int* _argc)
{
	PCHAR* argv;
	PCHAR  _argv;
	ULONG   len;
	ULONG   argc;
	CHAR   a;
	ULONG   i, j;

	BOOLEAN  in_QM;
	BOOLEAN  in_TEXT;
	BOOLEAN  in_SPACE;

	len = (ULONG)strlen(CmdLine);
	i = ((len+2)/2)*sizeof(PVOID) + sizeof(PVOID);

	argv = (PCHAR*)GlobalAlloc(GMEM_FIXED,
		i + (len+2)*sizeof(CHAR));

	_argv = (PCHAR)(((PUCHAR)argv)+i);

	argc = 0;
	argv[argc] = _argv;
	in_QM = FALSE;
	in_TEXT = FALSE;
	in_SPACE = TRUE;
	i = 0;
	j = 0;

	while( a = CmdLine[i] ) {
		if(in_QM) {
			if(a == '\"') {
				in_QM = FALSE;
			} else {
				_argv[j] = a;
				j++;
			}
		} else {
			switch(a) {
				case '\"':
					in_QM = TRUE;
					in_TEXT = TRUE;
					if(in_SPACE) {
						argv[argc] = _argv+j;
						argc++;
					}
					in_SPACE = FALSE;
					break;
				case ' ':
				case '\t':
				case '\n':
				case '\r':
					if(in_TEXT) {
						_argv[j] = '\0';
						j++;
					}
					in_TEXT = FALSE;
					in_SPACE = TRUE;
					break;
				default:
					in_TEXT = TRUE;
					if(in_SPACE) {
						argv[argc] = _argv+j;
						argc++;
					}
					_argv[j] = a;
					j++;
					in_SPACE = FALSE;
					break;
			}
		}
		i++;
	}
	_argv[j] = '\0';
	argv[argc] = NULL;

	(*_argc) = argc;
	return argv;
}
