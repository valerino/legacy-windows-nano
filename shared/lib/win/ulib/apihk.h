#ifndef __apihk_h__
#define __apihk_h__

#include <winsock2.h>

// Disable warning messages
#pragma warning( disable : 4311 4312 )  

// trampoline structure
typedef struct __tagAPIHK_HOOK_STRUCT {
	ULONG_PTR		TrampolineAddress;			// trampoline address
	ULONG_PTR		trampolinesize;				// trampoline size (without the added JMP)
	ULONG_PTR		FunctionAddress;			// original function address
} APIHK_HOOK_STRUCT, *PAPIHK_HOOK_STRUCT;

/*
*	install ReplacementFunc api hook on the specified FuncName in DllName, returning a hookstructure (must be freed by the caller)
*
*/
int apihk_installhook (IN char* DllName, IN char* FuncName, void* ReplacementFunc, IN OUT APIHK_HOOK_STRUCT* hk);

/*
*	uninstall hook previously set with apihk_uninstallhook and free apihook structure
*
*/
int apihk_uninstallhook (APIHK_HOOK_STRUCT* hk);

#endif // #ifndef __apihk_h__