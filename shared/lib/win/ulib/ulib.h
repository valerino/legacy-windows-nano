#ifndef __ulib_h__
#define __ulib_h__

#ifdef __cplusplus
extern "C"
{
#endif

// resources handling for rootkits
#include "drput.h"

// strings
#include <strlib.h>

// services
#include "svcut.h"

// debugging
#include <dbg.h>

// files
#include "filesut.h"

// processes/objects
#include "procut.h"

// apihooking
#include "apihk.h"

// generic rk utilities
#include "urkgenut.h"

// generic utilities (win32)
#include "genut.h"

// compression/encryption/packing (for encryption, only one module can be included)
#include <md5.h>
#include <minilzo.h>
#ifdef _USE_AES_RC6
#include <aes.h>
#endif
#ifdef _USE_AES_TWOFISH
#include <aes_2fish.h>
#endif
#include <minitar.h>

// parameters checking
#include "getopt.h"

// math
#include <mathut.h>

#ifdef __cplusplus
}
#endif

#endif