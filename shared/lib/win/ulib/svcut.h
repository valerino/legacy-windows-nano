#ifndef __svcut_h__
#define __svcut_h__

#include <strlib.h>

/*
*	check if the named device is present and running (tries to open \\\\.\\devicename). returns true if found
*
*/
int svc_isdevicepresentw (IN PWCHAR devicename);

/*
*	generate inf file on the fly
*
*/
int kmdf_generate_inf(char* infpath, char* servicename);

/*
 *	load coinstaller and get pointers to needed (post/pre) functions
 *
 */
int kmdf_load_coinstaller (char* coinstallerpath, OUT HMODULE* coinstmod, OPTIONAL OUT PULONG_PTR preinstallptr, OPTIONAL OUT PULONG_PTR postinstallptr, OPTIONAL OUT PULONG_PTR preremoveptr, OPTIONAL OUT PULONG_PTR postremoveptr);

/*
*	extract to %temp% and load the coinstaller, decrypt and get pointers. cipherkey must be binary. module must be freed with freelibrary
*   if coinstallerrsrc is null, it attempts to load the coinstaller from temp directory. coinstallerrsrc and forcedname are incompatible.
*/
HMODULE kmdf_setup_from_rsrc (OPTIONAL IN HMODULE module, OPTIONAL IN int coinstallerrsrc, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, OPTIONAL IN char* forcedname);

/*
*	extract kmdf engine to temp directory and return path. if forcedoutname is specified, it will be used instead of the one from the extracted resource
*   
*/
int kmdf_extract_from_rsrc (OPTIONAL IN HMODULE module, IN int coinstallerrsrc, OPTIONAL IN char* cipherkey, OPTIONAL IN int bits, OPTIONAL IN char* forcedoutname, OUT char* coinstallerpath, IN int coinstallerpathsize);


/*
*	add minifilter in regsitry
*
*/
int add_minifilter_registry (char* drivername, char* instancename, char* altitude);

/*
*	remove filter on class driver in registry (wdm)
*
*/
int del_class_filter_registry (PCHAR drivername, PCHAR guid, PCHAR filtertype);

/*
 *	add filter on class driver in registry (wdm)
 *
 */
int add_class_filter_registry (PCHAR drivername, PCHAR guid, PCHAR filtertype);

/*
 *	open service
 *
 */
ULONG service_open (OPTIONAL SC_HANDLE scmhandle, char* servicename, ULONG access, OUT SC_HANDLE* svchandle);

/*
 *	install service (same parameters as createservice)
 *
 */
ULONG service_install (OPTIONAL HANDLE scmhandle, char* svcname, char* svcdisplayname, char* svcgroup, char* svcpath, ULONG type, ULONG starttype, ULONG errortype, PULONG tag, char* dependencies, char* user, char* pwd);

/*
*	install service alternative (provides separate path and binary name)
*
*/
int service_install2 (IN PCHAR destdir, IN PCHAR svcname, IN PCHAR dispname, OPTIONAL IN PCHAR groupname, IN PCHAR binname, IN ULONG type, IN ULONG starttype);

/*
*	generic routine to drop and install a kmdf driver in \systemroot\system32\drivers (resource must be in the current process, added with drput functions)
*
*/
int service_kmdf_generic_install_from_rsrc (HMODULE module, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
											OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG starttype, OPTIONAL OUT PCHAR bin_namefromsrsrc, OPTIONAL IN int bin_namefromrsrclen, OPTIONAL OUT PCHAR svc_namefromsrsrc, OPTIONAL IN int svc_namefromrsrclen);

/*
*	generic routine to drop and install a win32 service in basepath
*
*/
int service_win32_generic_install_from_rsrc (OPTIONAL IN HMODULE module, IN PCHAR basepath, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
											 OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG servicetype, IN ULONG starttype, OPTIONAL OUT PCHAR bin_namefromsrsrc, OPTIONAL IN int bin_namefromrsrclen, OPTIONAL OUT PCHAR svc_namefromsrsrc, OPTIONAL IN int svc_namefromrsrclen);

/*
*	generic routine to drop and install a win32 service in basepath, specifying all
*
*/
int service_win32_generic_install_from_rsrc2 (OPTIONAL IN HMODULE module, IN PCHAR basepath, IN int resourceid, OPTIONAL IN unsigned char* enckey, 
											  OPTIONAL IN int bits, OPTIONAL IN PCHAR group, IN ULONG servicetype, IN ULONG starttype, IN PCHAR bin_name, IN PCHAR svc_name, IN PCHAR disp_name);


/*
 *	service uninstall 
 *
 */
ULONG service_uninstall (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char* servicename);

/*
*	generic routine to cleanup a failed service installation, removing service file too. destdir must be the directory where servicebin binname is.
*
*/
void service_generic_cleanup (IN PCHAR destdir, IN PCHAR svcname, IN PCHAR binname);

/*
 *	control service (stop,pause,etc...)
 *
 */
ULONG service_control (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char* servicename, ULONG ctlcode, OPTIONAL OUT SERVICE_STATUS* svcstatus);

/*
*	start/restart service
*
*/
int service_start (OPTIONAL SC_HANDLE scmhandle, OPTIONAL SC_HANDLE svchandle, OPTIONAL char*servicename, OPTIONAL int numargs, OPTIONAL char** args, int restart);

/*
*	remove protocol binding
*
*/
int del_protocol_binding_registry (IN PCHAR drivername);

/*
*	add protocol binding
*
*/
int add_protocol_binding_registry (IN PCHAR drivername);

/*
*	set service to svchost hosted (must be service dll)
*
*/
int svc_set_to_svchost (IN char* svc_name, OPTIONAL IN char* plgdir, IN char* svchost_group);

/*
*	remove servicegroup from svchost hosted (removes all in the group)
*
*/
int svc_remove_grp_from_svchost (char* svchost_group);

/*
*	set a service display name, optionally set description
*
*/
int svc_set_displayname (char* svc_name, char* displayname, OPTIONAL char* description);

#endif