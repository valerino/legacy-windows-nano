#include <stdio.h>
#include <stdlib.h>
#include <terastrings.h>
#include "include/w32kbd.h"
#include "include/pestuff.h"
#include <mathut.h>

#ifndef WIN32
/**	CHARACTER TRANSLATION FUNCTIONS **/

/** Convert a scan code to a virtual key code
* ScanCode = sc to be converted
* Flags = needed for KEY_EO or KEY_E1
*/

//it behaves like MapVirtualKeyEx with uMapType = 3
unsigned short SCToVK(unsigned char ScanCode, unsigned int Flags, PKBDTABLES KbdTable)
{
	unsigned short i;

	if(!KbdTable)
		return 0;

	if(Flags & KEY_E0)
	{
		for(i = 0; KbdTable->pVSCtoVK_E0[i].Vsc != 0; i++)
		{
			if(KbdTable->pVSCtoVK_E0[i].Vsc == ScanCode)
				return (KbdTable->pVSCtoVK_E0[i].Vk & 0xFF);
		}
	}
	else if(Flags & KEY_E1)
	{
		for(i = 0; KbdTable->pVSCtoVK_E1[i].Vsc != 0; i++)
		{
			if(KbdTable->pVSCtoVK_E1[i].Vsc == ScanCode)
				return (KbdTable->pVSCtoVK_E1[i].Vk & 0xFF);
		}
	}
	else
		return (KbdTable->pusVSCtoVK[ScanCode] & 0xFF);

	return 0;
}
/* 
* This function translates a ScanCode to the corresponding key name, it is used only for special keys, like Return, F1,F2,ALT,CONTROL,ecc...
*/

int GetKeyName(unsigned char ScanCode, int Extended, unsigned char *KeyName, unsigned long MaxKeyName, PKBDTABLES KbdTable)
{
	int i;

	if(!ScanCode || !MaxKeyName || !KeyName || !KbdTable)
		return -1;

	if(Extended)
	{
		for(i = 0; KbdTable->pKeyNamesExt[i].vsc != 0; i++)
		{
			if(KbdTable->pKeyNamesExt[i].vsc == ScanCode)
			{
				Ucs2ToUtf8(KeyName, KbdTable->pKeyNamesExt[i].pwsz, MaxKeyName);
				return strlen(KeyName);
			}
		}
	}

	for(i = 0; KbdTable->pKeyNames[i].vsc != 0; i++)
	{
		if(KbdTable->pKeyNames[i].vsc == ScanCode)
		{
			Ucs2ToUtf8(KeyName, KbdTable->pKeyNames[i].pwsz, MaxKeyName);
			return strlen(KeyName);
		}
	}

	return 0;
}	

unsigned short GetModifierBits(PMODIFIERS pModifiers, unsigned char *KeyState)
{
	PVK_TO_BIT pVkToBit = pModifiers->pVkToBit;
	unsigned short wModBits = 0;

	while(pVkToBit->Vk)
	{
		if(TestKeyDownBit(KeyState, pVkToBit->Vk))
			wModBits |= pVkToBit->ModBits;

		pVkToBit++;
	}
	return wModBits;
}

unsigned short GetModificationNumber(PMODIFIERS pModifiers, unsigned short wModBits)
{
	if(wModBits > pModifiers->wMaxModBits)
		return SHFT_INVALID;

	return pModifiers->ModNumber[wModBits];
}

int BuildDeadChar(KeyboardLayout *Kbl, PDEADKEY pDeadKey, unsigned short wchTyped, unsigned short *pUniChar, int cChar, int bBreak)
{
	unsigned long dwBoth;

	if(!Kbl || !pUniChar || !pDeadKey)
		return -1;

	dwBoth = MAKELONG(wchTyped, Kbl->wchDeadkey);

	if(pDeadKey != NULL)
	{
		if(!bBreak)
			Kbl->wchDeadkey = 0;
		while(pDeadKey->dwBoth != 0)
		{
			if(pDeadKey->dwBoth == dwBoth)
			{
				if(pDeadKey->uFlags & DKF_DEAD)
				{
					if(!bBreak)
						Kbl->wchDeadkey = (unsigned short)pDeadKey->wchComposed;
					return -1;
				}
				*pUniChar = (unsigned short)pDeadKey->wchComposed;
				return 1;
			}
			pDeadKey++;
		}
	}

	*pUniChar++ = (unsigned short)((dwBoth >> 16) & 0xFFFF);
	if(cChar > 1)
	{
		*pUniChar = (unsigned short)(dwBoth & 0xFFFF);
		return 2;
	}
	return 1;
}
int HandleDeadCharacter (
	KeyboardLayout *Kbl,
	PVK_TO_WCHAR_TABLE pVkWcharTable,
	PVK_TO_WCHARS1 pVkWchars,
	unsigned long Flags,
	unsigned short ModNumber,
	unsigned short *OutWChar,
	unsigned long MaxWChar
	)
{
	PVK_TO_WCHARS1 pVK;

	if(!Kbl || !pVkWcharTable || !pVkWchars || !OutWChar)
		return -1;

	pVK = (PVK_TO_WCHARS1)((unsigned char *)pVkWchars + pVkWcharTable->cbSize);
	if(Kbl->wchDeadkey == 0)
	{
		*OutWChar = pVK->wch[ModNumber];
//		if(!(Flags & KBDBREAK))
			Kbl->wchDeadkey = *OutWChar;
		return 0;
	}

	if((Kbl->KbdTable->pDeadKey != NULL) && (Kbl->wchDeadkey != 0))
		return BuildDeadChar(Kbl, Kbl->KbdTable->pDeadKey, pVK->wch[ModNumber], OutWChar, MaxWChar, FALSE);

	return -1;
}

int HandleLigatureCharacter ( 
	KeyboardLayout *Kbl,
	PVK_TO_WCHAR_TABLE pVkWcharTable,
	PVK_TO_WCHARS1 pVkWchars,
	unsigned long Flags,
	unsigned short ModNumber,
	unsigned short *OutWChar,
	unsigned long MaxWChar 
	)
{
	PKBDTABLES pKbdTable;
	PLIGATURE1 pLigature;

	if(!Kbl || !pVkWcharTable || !pVkWchars || !OutWChar)
		return -1;

	pKbdTable = Kbl->KbdTable;
	pLigature = pKbdTable->pLigature;
	while((pLigature->VirtualKey != 0))
	{
		int iLig = 0;
		int cwchT = 0;

		if((pLigature->VirtualKey == pVkWchars->VirtualKey) && (pLigature->ModificationNumber == ModNumber))
		{
			while((iLig < pKbdTable->nLgMaxd) && (cwchT < MaxWChar))
			{
				if(pLigature->wch[iLig] == WCH_NONE)
					return cwchT;

				if(Kbl->wchDeadkey != 0)
				{
					int cComposed;

					cComposed = BuildDeadChar(Kbl, pKbdTable->pDeadKey, pLigature->wch[iLig], OutWChar + cwchT, MaxWChar - cwchT, FALSE);
					if(cComposed)
						cwchT += cComposed;
				}
				else
					OutWChar[cwchT++] = pLigature->wch[iLig];
				iLig++;
			}
			return cwchT;
		}
		pLigature = (PLIGATURE1)((unsigned char *)pLigature + pKbdTable->cbLgEntry);
	}

	return 0;
}

int HandleGoodCharacter(PVK_TO_WCHAR_TABLE pVkWcharTable, PVK_TO_WCHARS1 pVkWchars, unsigned short ModNumber, unsigned short *OutWChar, unsigned long MaxWChar, PKBDTABLES KbdTable)
{
	if(!pVkWcharTable || !pVkWchars || !OutWChar || !KbdTable)
		return -1;

	*OutWChar = (unsigned short)pVkWchars->wch[ModNumber];
	return 1;
}

int HandleKatakana(unsigned short VirtualKey, unsigned short *OutWChar, PKBDTABLES KbdTable)
{
	PVK_TO_WCHARS1 pVK;
	PVK_TO_WCHAR_TABLE pVKT;
	unsigned short wChCtrl = 0;
	unsigned short wChShiftCtrl = 0;
	unsigned char nShift;
	unsigned short wModBits;
	unsigned short wModNumCtrl, wModNumShiftCtrl;

	if(!OutWChar || !KbdTable)
		return -1;

	wModNumCtrl = GetModificationNumber(KbdTable->pCharModifiers, KBDCTRL);
	wModNumShiftCtrl = GetModificationNumber(KbdTable->pCharModifiers, KBDSHIFT | KBDCTRL);

	for(pVKT = KbdTable->pVkToWcharTable; pVKT->pVkToWchars != NULL; pVKT++)
	{
		for(pVK = pVKT->pVkToWchars; pVK->VirtualKey != 0; pVK = (PVK_TO_WCHARS1)((unsigned char *)pVK + pVKT->cbSize))
		{
			for(nShift = 0; nShift < pVKT->nModifications; nShift++)
			{
				if(pVK->wch[nShift] == VirtualKey)
				{
					if(pVK->VirtualKey == 0xFF)
						pVK = (PVK_TO_WCHARS1)((unsigned char *)pVK - pVKT->cbSize);

					if(nShift == wModNumCtrl)
					{
						if(wChCtrl == 0)
							wChCtrl = (unsigned short)MAKEWORD(pVK->VirtualKey, KBDCTRL);
					}
					else if(nShift == wModNumShiftCtrl)
					{
						if(wChShiftCtrl == 0)
							wChShiftCtrl = (unsigned short)MAKEWORD(pVK->VirtualKey, KBDCTRL | KBDSHIFT);
					}
					else
					{	
						for(wModBits = 0; wModBits <= KbdTable->pCharModifiers->wMaxModBits; wModBits++)
						{
							if(KbdTable->pCharModifiers->ModNumber[wModBits] == nShift)
							{
								if(pVK->VirtualKey == 0xFF)
									pVK = (PVK_TO_WCHARS1)((unsigned char *)pVK - pVKT->cbSize);

								*OutWChar = (unsigned short)MAKEWORD(pVK->VirtualKey, wModBits);
								return 1;
							}
						}
					}
				}
			}
		}
	}

	if(wChCtrl)
	{
		*OutWChar = wChCtrl;
		return 1;
	}
	if(wChShiftCtrl)
	{
		*OutWChar = wChShiftCtrl;
		return 1;
	}
	return 0;
}

int TranslateCharacter( KeyboardLayout *Kbl,
						unsigned short VirtualKey,
						unsigned char ScanCode,
						unsigned long Flags,
						unsigned char *KbdState,
						unsigned short *UniChar,
						unsigned long UniCharLength )
{
	PVK_TO_WCHAR_TABLE pVkWchTable;
	PVK_TO_WCHARS1 pVkWchars;
	PKBDTABLES pKbdTable = Kbl->KbdTable;
	unsigned char *pKbdState = KbdState;
	unsigned char KeyState[(256 >> 2)] = {0};
	unsigned long i;
	unsigned short wModBits = 0;
	unsigned short nShift;

	if(!Kbl || !UniChar || !KbdState)
		return -1;

	for(i = 0; i < 256; i++, pKbdState++)
	{
		if(*pKbdState & 0x80)
			SetKeyDownBit(KeyState, i);
		else
			ClearKeyDownBit(KeyState, i);

		if(*pKbdState & 0x01)
			SetKeyToggleBit(KeyState, i);
		else
			ClearKeyToggleBit(KeyState, i);
	}

	wModBits = GetModifierBits(pKbdTable->pCharModifiers, KeyState);
	if((VirtualKey == VK_BACK) && (pKbdTable->fLocaleFlags & KLLF_LRM_RLM))
	{
		if(TestKeyDownBit(KeyState, VK_LSHIFT))
		{
			*UniChar = 0x200E;
			return 1;
		}
		else if(TestKeyDownBit(KeyState, VK_RSHIFT))
		{
			*UniChar = 0x200F;
			return 1;
		}
	}
	else if((VirtualKey == VK_PACKET) && ((ScanCode & 0xFF) == 0))
	{
		*UniChar = 0x0000; 
		return -1;
	}

	for(pVkWchTable = pKbdTable->pVkToWcharTable; pVkWchTable->pVkToWchars != NULL; pVkWchTable++)
	{
		pVkWchars = pVkWchTable->pVkToWchars;
		while(pVkWchars->VirtualKey != 0)
		{
			if(pVkWchars->VirtualKey == VirtualKey)
			{
				if((pVkWchars->Attributes & KANALOK) && (ISKANALOCKON(KeyState)))
					wModBits |= KBDKANA;
				else if((pVkWchars->Attributes & CAPSLOK) && ((wModBits & ~KBDSHIFT) == 0) &&
					ISCAPSLOCKON(KeyState))
				{
					wModBits ^= KBDSHIFT;
				}
				else if((pVkWchars->Attributes & CAPLOKALTGR) &&
					((wModBits & (KBDALT | KBDCTRL)) == (KBDALT | KBDCTRL)) &&
					ISCAPSLOCKON(KeyState))
				{
					wModBits ^= KBDSHIFT;
				}

				if((pVkWchars->Attributes & SGCAPS) && ((wModBits & ~KBDSHIFT) == 0) &&
					ISCAPSLOCKON(KeyState))
				{
					pVkWchars = (PVK_TO_WCHARS1)((unsigned char *)pVkWchars + pVkWchTable->cbSize);
				}

				nShift = GetModificationNumber(pKbdTable->pCharModifiers, wModBits);
				if(nShift == SHFT_INVALID)
					return -1;

				else if((nShift < pVkWchTable->nModifications) &&
					(pVkWchars->wch[nShift] != WCH_NONE))
				{
					switch(pVkWchars->wch[nShift])
					{
						case WCH_DEAD:
							return HandleDeadCharacter(Kbl, pVkWchTable, pVkWchars, Flags, nShift, UniChar, UniCharLength);
						case WCH_LGTR:
							return HandleLigatureCharacter(Kbl, pVkWchTable, pVkWchars, Flags, nShift, UniChar, UniCharLength);

						default:
							return HandleGoodCharacter(pVkWchTable, pVkWchars, nShift, UniChar, UniCharLength, pKbdTable);
					}
				}
				else if((wModBits == KBDCTRL) || (wModBits == (KBDCTRL | KBDSHIFT)) ||
					(wModBits == (KBDKANA|KBDCTRL)) || (wModBits == (KBDKANA|KBDCTRL|KBDSHIFT)))
				{
					if((VirtualKey >= 'A') && (VirtualKey <= 'Z'))
					{
						*UniChar = (unsigned short)(VirtualKey & 0x1F);
						return 1;
					}
					else if((VirtualKey >= 0xFF61) && (VirtualKey <= 0xFF91))
					{
						return HandleKatakana(VirtualKey, UniChar, pKbdTable);
					}
				}
			}
			pVkWchars = (PVK_TO_WCHARS1)((unsigned char *)pVkWchars + pVkWchTable->cbSize);
		}
	}

	//character not found
	return 0;
}

//translate an rva to an offset, needs to be fixed for 64bit compatibility
unsigned long RvaToOffset(IMAGE_NT_HEADERS *NT, unsigned long Rva)
{
	unsigned long Offset = Rva, Limit;
	IMAGE_SECTION_HEADER *Img;
	int i;

	if(!NT)
		return 0;

	Img = IMAGE_FIRST_SECTION(NT);

	if (Rva < Img->PointerToRawData)
		return Rva;

	for (i = 0; i < NT->FileHeader.NumberOfSections; i++)
	{
		if (Img[i].SizeOfRawData)
			Limit = Img[i].SizeOfRawData;
		else
			Limit = Img[i].Misc.VirtualSize;

		if (Rva >= Img[i].VirtualAddress &&
			Rva < (Img[i].VirtualAddress + Limit))
		{
			if (Img[i].PointerToRawData != 0)
			{
				Offset -= Img[i].VirtualAddress;
				Offset += Img[i].PointerToRawData;
			}

			return Offset;
		}
	}

	return 0;
}

void *TranslateRva(PIMAGE_NT_HEADERS NtHeaders, void *FileBase, void *Rva)
{
	void *pTranslated = NULL;

	if(!NtHeaders || !FileBase)
		return;

	pTranslated = (void *)((unsigned long)Rva - NtHeaders->OptionalHeader.ImageBase);
	pTranslated = (void *)RvaToOffset(NtHeaders, (unsigned long)pTranslated);
	return MAKE_PTR(FileBase, (unsigned long)pTranslated, void *);
}

void PatchLayoutTable(void *Base, PIMAGE_NT_HEADERS NtHeaders, PKBDTABLES KbdTable)
{
	if(!Base)
		return;

	PVK_TO_WCHAR_TABLE pCurrentVkToWcharTable;
	PVSC_LPWSTR pCurrentVscToWstr;

	TRANSLATE_ADDRESS(KbdTable->pCharModifiers, PMODIFIERS);
	TRANSLATE_ADDRESS(KbdTable->pVkToWcharTable, PVK_TO_WCHAR_TABLE);
	TRANSLATE_ADDRESS(KbdTable->pDeadKey, PDEADKEY);
	TRANSLATE_ADDRESS(KbdTable->pKeyNames, VSC_LPWSTR *);
	TRANSLATE_ADDRESS(KbdTable->pKeyNamesExt, VSC_LPWSTR *);
	TRANSLATE_ADDRESS(KbdTable->pKeyNamesDead, unsigned short **);
	TRANSLATE_ADDRESS(KbdTable->pusVSCtoVK, unsigned short *);
	TRANSLATE_ADDRESS(KbdTable->pVSCtoVK_E0, PVSC_VK);
	TRANSLATE_ADDRESS(KbdTable->pVSCtoVK_E1, PVSC_VK);
	TRANSLATE_ADDRESS(KbdTable->pLigature, PLIGATURE1);
	TRANSLATE_ADDRESS(KbdTable->pCharModifiers->pVkToBit, PVK_TO_BIT);


	//patch vkey->wchar tables address
	pCurrentVkToWcharTable = KbdTable->pVkToWcharTable;
	while( pCurrentVkToWcharTable->pVkToWchars != NULL &&
		pCurrentVkToWcharTable->nModifications != 0 &&
		pCurrentVkToWcharTable->cbSize != 0
		)
	{
		TRANSLATE_ADDRESS(pCurrentVkToWcharTable->pVkToWchars, PVK_TO_WCHARS1);
		pCurrentVkToWcharTable++;
	}

	//patch KeyNames and KeyNamesExt address
	pCurrentVscToWstr = KbdTable->pKeyNames;
	while(pCurrentVscToWstr->pwsz != NULL && pCurrentVscToWstr->vsc != 0)
	{
		TRANSLATE_ADDRESS(pCurrentVscToWstr->pwsz, unsigned short *);
		pCurrentVscToWstr++;
	}

	pCurrentVscToWstr = KbdTable->pKeyNamesExt;
	while(pCurrentVscToWstr->pwsz != NULL && pCurrentVscToWstr->vsc != 0)
	{
		TRANSLATE_ADDRESS(pCurrentVscToWstr->pwsz, unsigned short *);
		pCurrentVscToWstr++;
	}
}

KeyboardLayout *LoadLayout(char *LayoutCode,DBHandler *Dbh) {
    unsigned long ulLCode = 0;
    char szLayout[256];
    LinkedList *filter;
    DBResult *dbRes;
    LinkedList *layout;
    TaggedValue *payload;
    KeyboardLayout *kbl = NULL;

    sscanf(LayoutCode, "%x", &ulLCode);
    /* on the db , the layout is stored as an unisgned long , but we need 
     * its string representation to build the sql query string */
    sprintf(szLayout,"%lu",ulLCode);
    
    /* first let's build the filter for the query ... we are looking for a 'layout' */
    filter = CreateList();
    PushTaggedValue(filter,CreateTaggedValue("layout",szLayout,0));
    /* execute the query */
    dbRes = DBGetAllRecords(Dbh,"W32KbdLayouts",filter,FILTER_AND,NULL);
    /* free resources allocated for the filter, we don't need it anymore */
    DestroyList(filter);
    if(!dbRes) {
            /* can't execute the query ... free resources and return */
            return NULL;
    }
    /* ok, let's fecth the result */
    layout = DBFetchRow(dbRes);
    if(!layout) {
            DBFreeResult(dbRes);
            return NULL;
    }
    /* we want the "payload" column */
    payload = GetTaggedValue(layout,"payload");
    if(!payload) {
            DBFreeResult(dbRes);
            return NULL;
    }
	
    kbl = KBLLoadLayoutData(LayoutCode, payload->value, payload->vLen);

    /* release db-related resources */
    DBFreeResult(dbRes);

    return kbl;
}

KeyboardLayout *KBLLoadLayoutData(char *LayoutCode, char *Data, int Len)
{
	KeyboardLayout *kbl;
	//int iFileSize;
	PIMAGE_DOS_HEADER pDosHeader;
	PIMAGE_NT_HEADERS pNtHeaders;
	PIMAGE_EXPORT_DIRECTORY pExportTable;
	unsigned long *pdwExports;
	unsigned long *pdwExportNames;
	unsigned short *pwExportOrds;
	unsigned long dwExportEntry;
	unsigned long dwETOffset;
	char *szExportName;
	unsigned long i,j;
	
	kbl = (KeyboardLayout *)malloc(sizeof(KeyboardLayout));
	if(!kbl)
		return NULL;
	memset(kbl, 0, sizeof(KeyboardLayout));
	
	
	
	/* copy payload on a local buffer */
	kbl->Base = (void *)malloc(Len);
	if(!kbl->Base)
	{
		KBLUnloadLayout(kbl);
		return NULL;
	}
	memcpy(kbl->Base, Data, Len);
	
	pDosHeader = (PIMAGE_DOS_HEADER)kbl->Base;
	if(pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		KBLUnloadLayout(kbl);
		return NULL;
	}

	pNtHeaders = MAKE_PTR(pDosHeader, pDosHeader->e_lfanew, PIMAGE_NT_HEADERS);
	if(pNtHeaders->Signature != IMAGE_NT_SIGNATURE)
	{
		KBLUnloadLayout(kbl);
		return NULL;
	}

	if(!pNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress)
	{
		KBLUnloadLayout(kbl);
		return NULL;
	}

	dwETOffset = RvaToOffset(pNtHeaders, pNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
	pExportTable = MAKE_PTR(kbl->Base, dwETOffset, PIMAGE_EXPORT_DIRECTORY);

	if(pExportTable->NumberOfFunctions < 1)
	{
		KBLUnloadLayout(kbl);
		return NULL;
	}

	pdwExports = MAKE_PTR(kbl->Base, RvaToOffset(pNtHeaders, pExportTable->AddressOfFunctions), unsigned long *);
	pdwExportNames = MAKE_PTR(kbl->Base, RvaToOffset(pNtHeaders, pExportTable->AddressOfNames), unsigned long *);
	pwExportOrds = MAKE_PTR(kbl->Base, RvaToOffset(pNtHeaders, pExportTable->AddressOfNameOrdinals), unsigned short *);

	for(i = 0; i < pExportTable->NumberOfFunctions; i++)
	{
		if(!pdwExports[i])
			continue;

		for(j = 0; j < pExportTable->NumberOfNames; j++)
		{
			if(pwExportOrds[j] == i)
			{
				szExportName = MAKE_PTR(kbl->Base, RvaToOffset(pNtHeaders, pdwExportNames[j]), char *);
				if(strncmp(szExportName, "KbdLayerDescriptor", strlen("KbdLayerDescriptor")) == 0)
				{
					dwExportEntry = RvaToOffset(pNtHeaders, pdwExports[i]);
					break;
				}
			}
		}
	}

	//dwExportEntry contains the offset of KbdLayoutDescriptor entry point, were we find
	//"mov eax, offset KBDLAYOUT" where KBDLAYOUT is the struct that contains sc->vk infos (and vk->unicode)
	//mov eax is 5 bytes long, so dwExportEntry+1 is the VA (Virtual Address) of the structure inside the pe
	//we need to convert this address (and every other pointers inside KBDLAYOUT) to a physical offset since we are not loading the pe, but just mapping the dll file in memory
	dwExportEntry += 1;

	kbl->KbdTable = (PKBDTABLES)TranslateRva(pNtHeaders, kbl->Base, (void *)(*((unsigned long *)MAKE_PTR(kbl->Base, dwExportEntry, unsigned long *))));
	PatchLayoutTable(kbl->Base, pNtHeaders, kbl->KbdTable);
	return kbl;
}

void UnloadLayout(KeyboardLayout *Kbl)
{
	if(!Kbl)
		return;
	if(Kbl->Base)
		free(Kbl->Base);
	free(Kbl);
}
#endif

KeyboardLayout *KBLLoadLayout(char *LayoutCode, DBHandler *Dbh)
{
	KeyboardLayout *kbl;
#ifdef WIN32
	kbl = (KeyboardLayout *)malloc(sizeof(KeyboardLayout));
	if(!kbl)
		return NULL;
	kbl->ActiveLayout = LoadKeyboardLayoutA(LayoutCode, KLF_ACTIVATE);
#else
	kbl = LoadLayout(LayoutCode, Dbh);
#endif
	return kbl;
}

int KBLGetLayoutName(KeyboardLayout *Kbl, char *LayoutName, unsigned long LayoutNameLength)
{
	if(!Kbl || !LayoutName || !LayoutNameLength)
		return -1;

#ifdef WIN32
	if(!GetKeyboardLayoutNameA(LayoutName))
		return -1;
#else
	strncpy(LayoutName, Kbl->LayoutName, LayoutNameLength < strlen(Kbl->LayoutName)+1 ? LayoutNameLength : strlen(Kbl->LayoutName));
#endif
	return (int)strlen(LayoutName);
}

unsigned short KBLSCToVK(KeyboardLayout *Kbl, unsigned long ScanCode, unsigned int Flags)
{
#ifdef WIN32
	return (unsigned short)MapVirtualKeyEx(ScanCode, 3, Kbl->ActiveLayout);
#else
	return SCToVK(ScanCode, Flags, Kbl->KbdTable);
#endif
}

int KBLGetKeyName(KeyboardLayout *Kbl, unsigned long ScanCode, int Extended, unsigned char *KeyName, unsigned long KeyNameLength)
{
#ifdef WIN32
	unsigned long dwScanCode = (unsigned long)ScanCode;
	unsigned short wszKey[32] = {0};
	dwScanCode = dwScanCode << 16;
	if(Extended)
		dwScanCode = BitSet(dwScanCode, 24);
	dwScanCode = BitSet(dwScanCode, 25);
	if(GetKeyNameTextW(dwScanCode, wszKey, sizeof(wszKey)))
		return Ucs2ToUtf8(KeyName, wszKey, KeyNameLength);
	return 0;
#else
	return GetKeyName(ScanCode, Extended, KeyName, KeyNameLength, Kbl->KbdTable);
#endif
}

int KBLToUnicode(
	KeyboardLayout *Kbl, 
	unsigned long ScanCode, 
	unsigned long VirtualKey,
	unsigned long Flags,
	unsigned char *KbdState,
	unsigned short *UniOut,
	unsigned long UniOutLength )
{
#ifdef WIN32
	return ToUnicodeEx(VirtualKey, ScanCode, KbdState, UniOut, UniOutLength / sizeof(unsigned short), 0, Kbl->ActiveLayout);
#else
	return TranslateCharacter(Kbl, VirtualKey, ScanCode, Flags, KbdState, UniOut, UniOutLength);
#endif
}

void KBLUnloadLayout(KeyboardLayout *Kbl)
{
#ifdef WIN32
	UnloadKeyboardLayout(Kbl->ActiveLayout);
	free(Kbl);
#else
	UnloadLayout(Kbl);
#endif
}
