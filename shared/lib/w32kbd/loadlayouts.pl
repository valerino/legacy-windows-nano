#!/usr/bin/perl

use DBI;
use strict;

$|=1;
open(LDESCR,"layouts.txt");
my %tab = ();

unless(scalar(@ARGV) >= 3) {
	print("Usage: $0 <dbhost> <dbname> <dbuser> [ <dbpass> ]\n");
	exit(-1);
}
my ($dbhost,$dbname,$dbuser,$dbpass) = @ARGV;

print("Loading layout->dll table ... ");
foreach my $l (<LDESCR>) {
	$l =~ s/[\r\n]+$//;
	my ($layout,$name) = split('=',$l);
	$name =~ tr/[A-Z]/[a-z]/;
	if($tab{$name}) {
		#if(ref($tab{$name}) eq "ARRAY") {
			push(@{$tab{$name}},$layout);
		#}
	}
	else {
		$tab{$name} = [$layout];
	}
}
print("Done \n");

print("Connecting to database ... ");
my $dbh = DBI->connect("DBI:mysql:$dbname:$dbhost", $dbuser, $dbpass);
print("Done \n");
print("Deleting old layouts ... ");
$dbh->do("DELETE FROM W32KbdLayouts");
print("Done \n");

opendir(LAYOUTS,"layouts");
foreach my $dll (readdir(LAYOUTS)) {
	next if($dll =~ /^\.+/);
	next unless($dll=~/\.dll$/);
	my $name = $dll;
	$name =~ tr/[A-Z]/[a-z]/;
	next unless($tab{$name});
	foreach my $l (@{$tab{$name}}) {
		my $lang = $name;
		$lang =~ s/^kbd(.*?)\.dll/$1/i;
		my $layout = hex($l);
		print("Loading layout 0x$l ($layout) => $dll ... ");
		open(DLL,"layouts/$dll");
		my @stats = stat(DLL);
		my $buff = undef;
		my $inBytes = read(DLL,$buff,$stats[7]);
		$dbh->do("INSERT INTO W32KbdLayouts (layout,lang,payload) VALUES ($layout,'$lang',".$dbh->quote($buff).")");
		close(DLL);
		print("Done \n");
	}
}
exit(0);
