#ifndef _W32KBDLAYOUT_H
#define _W32KBDLAYOUT_H

#ifndef WIN32
#include <w32_defs.h>
#endif
#include <stdio.h>
#include <xdbaccess.h>
#include <vkdefs.h>

#ifndef WIN32
#define TRANSLATE_ADDRESS(address, cast) {if(address != NULL) \
	address = (cast)TranslateRva(NtHeaders, Base, address);}
#define TYPEDEF_VK_TO_WCHARS(i) \
	typedef struct _VK_TO_WCHARS ## i { \
	unsigned char VirtualKey; \
	unsigned char Attributes; \
	unsigned short wch[i]; \
} VK_TO_WCHARS ## i, *PVK_TO_WCHARS ## i;

//taken from ReactOS and leaked win2k source code (tounicod.c, xlate.c, kbd.h)
typedef struct _VK_TO_BIT {
	unsigned char Vk;
	unsigned char ModBits;
} VK_TO_BIT, *PVK_TO_BIT;

typedef struct _MODIFIERS {
	PVK_TO_BIT pVkToBit;
	unsigned short wMaxModBits;
	unsigned char ModNumber[];
} MODIFIERS, *PMODIFIERS;

TYPEDEF_VK_TO_WCHARS(1)
TYPEDEF_VK_TO_WCHARS(2)
TYPEDEF_VK_TO_WCHARS(3)
TYPEDEF_VK_TO_WCHARS(4)
TYPEDEF_VK_TO_WCHARS(5)
TYPEDEF_VK_TO_WCHARS(6)
TYPEDEF_VK_TO_WCHARS(7)
TYPEDEF_VK_TO_WCHARS(8)
TYPEDEF_VK_TO_WCHARS(9)
TYPEDEF_VK_TO_WCHARS(10)

typedef struct _VK_TO_WCHAR_TABLE {
	PVK_TO_WCHARS1 pVkToWchars;
	unsigned char nModifications;
	unsigned char cbSize;
} VK_TO_WCHAR_TABLE, *PVK_TO_WCHAR_TABLE;

typedef struct _DEADKEY {
	unsigned long dwBoth;
	unsigned short wchComposed;
	unsigned short uFlags;
} DEADKEY, *PDEADKEY;

typedef struct _VSC_LPWSTR {
	unsigned char vsc;
	unsigned short* pwsz;
} VSC_LPWSTR, *PVSC_LPWSTR;

typedef struct _VSC_VK {
	unsigned char Vsc;
	unsigned short Vk;
} VSC_VK, *PVSC_VK;

#define TYPEDEF_LIGATURE(i) \
	typedef struct _LIGATURE ## i { \
	unsigned char VirtualKey; \
	unsigned short ModificationNumber; \
	unsigned short wch[i]; \
} LIGATURE ## i, *PLIGATURE ## i;

TYPEDEF_LIGATURE(1)
TYPEDEF_LIGATURE(2)
TYPEDEF_LIGATURE(3)
TYPEDEF_LIGATURE(4)
TYPEDEF_LIGATURE(5)

typedef unsigned short *DEADKEY_LPWSTR;

#define DKF_DEAD 1

#define KBD_VERSION 1
#define GET_KBD_VERSION(p) (HIWORD((p)->fLocalFlags))
#define KLLF_ALTGR 1
#define KLLF_SHIFTLOCK 2
#define KLLF_LRM_RLM 4

typedef struct _KBDTABLES {
	PMODIFIERS pCharModifiers;
	PVK_TO_WCHAR_TABLE pVkToWcharTable;
	PDEADKEY pDeadKey;
	VSC_LPWSTR *pKeyNames;
	VSC_LPWSTR *pKeyNamesExt;
	unsigned short* *pKeyNamesDead;
	unsigned short *pusVSCtoVK;
	unsigned char bMaxVSCtoVK;
	PVSC_VK pVSCtoVK_E0;
	PVSC_VK pVSCtoVK_E1;
	unsigned long fLocaleFlags;
	unsigned char nLgMaxd;
	unsigned char cbLgEntry;
	PLIGATURE1 pLigature;
} KBDTABLES, *PKBDTABLES;

#define TestKeyDownBit(pb, vk)\
	(pb[vk >> 2] & (1 << ((vk & 3) << 1)))
#define SetKeyDownBit(pb, vk)\
	(pb[vk >> 2] |= (1 << ((vk & 3) << 1)))
#define ClearKeyDownBit(pb, vk)\
	(pb[vk >> 2] &= ~(1 << ((vk & 3) << 1)))
#define TestKeyToggleBit(pb, vk)\
	(pb[vk >> 2] & (1 << (((vk & 3) << 1) + 1)))
#define SetKeyToggleBit(pb, vk)\
	(pb[vk >> 2] |= (1 << (((vk & 3) << 1) + 1)))
#define ClearKeyToggleBit(pb, vk)\
	(pb[vk >> 2] &= ~(1 << (((vk & 3) << 1) + 1)))
#define ToggleKeyToggleBit(pb, vk)\
	(pb[vk >> 2] ^= (1 << (((vk & 3) << 1) + 1)))
#define TestKeyRecentDownBit(pb, vk)\
	(pb[vk >> 3] & (1 << (vk & 7)))
#define SetKeyRecentDownBit(pb, vk)\
	(pb[vk >> 3] |= (1 << (vk & 7)))
#define ClearKeyRecentDownBit(pb, vk)\
	(pb[vk >> 3] &= ~(1 << (vk & 7)))

#define TestKeyStateDown(pq, vk)\
	TestKeyDownBit(pq->afKeyState, vk)
#define SetKeyStateDown(pq, vk)\
	SetKeyDownBit(pq->afKeyState, vk)
#define ClearKeyStateDown(pq, vk)\
	ClearKeyDownBit(pq->afKeyState, vk)
#define TestKeyStateToggle(pq, vk)\
	TestKeyToggleBit(pq->afKeyState, vk)
#define SetKeyStateToggle(pq, vk)\
	SetKeyToggleBit(pq->afKeyState, vk)
#define ClearKeyStateToggle(pq, vk)\
	ClearKeyToggleBit(pq->afKeyState, vk)

/* Constants that help table decoding */
#define WCH_NONE 0xf000
#define WCH_DEAD 0xf001
#define WCH_LGTR 0xf002

#define SHFT_INVALID 0x0F

#define CAPITAL_BIT   0x80000000

#define CAPSLOK 1
#define SGCAPS 2
#define CAPLOKALTGR 4
#define KANALOK 8
#define GRPSELTAP 0x80

#define KBDBASE 0
#define KBDSHIFT 1
#define KBDCTRL 2
#define KBDALT 4
// three symbols KANA, ROYA, LOYA are for FE
#define KBDKANA 8
#define KBDROYA 0x10
#define KBDLOYA 0x20
#define KBDGRPSELTAP 0x80
#define KBDBREAK (unsigned short)0x8000

#define ISCAPSLOCKON(pf) (TestKeyToggleBit(pf, VK_CAPITAL) != 0)
#define ISNUMLOCKON(pf)  (TestKeyToggleBit(pf, VK_NUMLOCK) != 0)
#define ISSHIFTDOWN(w)   (w & 0x01)
#define ISKANALOCKON(pf) (TestKeyToggleBit(pf, VK_KANA)    != 0)

struct _KeyboardLayout
{
	PKBDTABLES KbdTable;
	void *Base;
	unsigned short wchDeadkey; //contains the current deadkey, for diacritics
	char LayoutName[32];
};
#else
#include <winsock2.h>
struct _KeyboardLayout
{
	HKL ActiveLayout;
};
#endif // #ifdef WIN32

#define KEYBOARD_KANA_LOCK_ON     8
#define KEYBOARD_CAPS_LOCK_ON     4
#define KEYBOARD_NUM_LOCK_ON      2
#define KEYBOARD_SCROLL_LOCK_ON   1
#define KEY_MAKE  0
#define KEY_BREAK 1
#define KEY_E0    2
#define KEY_E1    4

//this file contains function for win32 keyboard layout handling
//this is only needed on UNIX system, on Win32 native functions will be used

typedef struct _KeyboardLayout KeyboardLayout;
KeyboardLayout *KBLLoadLayout(char *LayoutCode, DBHandler *Dbh);
KeyboardLayout *KBLLoadLayoutData(char *LayoutCode, char *Data, int Len);
int KBLGetLayoutName(KeyboardLayout *Kbl, char *LayoutName, unsigned long LayoutNameLength);
unsigned short KBLSCToVK(KeyboardLayout *Kbl, unsigned long ScanCode, unsigned int Flags);
int KBLGetKeyName(KeyboardLayout *Kbl, unsigned long ScanCode, int Extended, unsigned char *KeyName, unsigned long KeyNameLength);
int KBLToUnicode(	KeyboardLayout *Kbl, 
					unsigned long ScanCode, 
					unsigned long VirtualKey,
					unsigned long Flags,
					unsigned char *KbdState,
					unsigned short *UniOut,
					unsigned long UniOutLength );

void KBLUnloadLayout(KeyboardLayout *Kbl);

#endif
