/*
*	sockets routines
*  -vx-
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#if defined(WIN32) || defined (WINCE)
#include <winsock2.h>
#include <strsafe.h>
#else
#include <w32_defs.h>
#endif // #if defined(WIN32)

#include <gensock.h>

/*
*	initialize sockets
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsInitialize ()
{
#if defined (WIN32) || defined (WINCE)
	WSADATA data;
	int res = WSAStartup(MAKEWORD( 2, 2),&data);

	if (res != 0)
		return -1;
#endif
	return STATUS_SUCCESS;
}

/*
*	finalize sockets
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsFinalize ()
{
#if defined (WIN32) || defined (WINCE)
	if (WSACleanup() != 0)
		return -1;
#endif
	return STATUS_SUCCESS;
}

/*
*	disconnect socket
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsDisconnect(IN ksocket* sock)
{

	// check params
	if (!sock)
		return -1;

	// shutdown socket (send/receive)
	if (shutdown(sock->sock,2) != 0)
		return -1;

	return STATUS_SUCCESS;
}

/*
*	connect socket to address:port (both nbo)
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsConnect(IN ksocket* sock, IN unsigned long Address, IN unsigned short Port)
{
	struct sockaddr_in addr;
	int res = -1;

	// check params
	if (!sock || !Address || !Port)
		return -1;

	// fill address
	memset (&addr,0,sizeof (struct sockaddr_in));
	addr.sin_addr.s_addr = Address;
	addr.sin_port = Port;
	addr.sin_family = AF_INET;

	// connect socket
	res = connect(sock->sock,(struct sockaddr*)&addr,sizeof (struct sockaddr_in));
	if (res == SOCKET_ERROR)
		return -1;

	return STATUS_SUCCESS;
}

/*
*	receives a buffer thru socket
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsReceive (IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesReceived)
{
	int res = -1;

	// check params
	if (!sock || !Buffer || !SizeBuffer)
		return -1;
	if (BytesReceived)
		*BytesReceived = 0;
	
	// receive data
	res = recv (sock->sock,Buffer,SizeBuffer,0);
	if (res == SOCKET_ERROR)
		return -1;
	
	if (BytesReceived)
		*BytesReceived = res;
	return STATUS_SUCCESS;
}

/*
*	sends a buffer thru socket
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsSend(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesSent)
{
	int res = -1;

	// check params
	if (!sock || !Buffer || !SizeBuffer)
		return -1;
	if (BytesSent)
		*BytesSent = 0;

	// send data
	res = send (sock->sock, (char*)Buffer,SizeBuffer,0);
	if (res == SOCKET_ERROR)
		return -1;
	
	if (BytesSent)
		*BytesSent = res;

	return STATUS_SUCCESS;
}

/*
*	print a formatted string to socket. Max string length is 1024 bytes
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsPrintf(IN ksocket* sock, OPTIONAL OUT unsigned long* SentBytes, IN const char* format, ...)
{
	va_list ap;
	char buf [1024] = {0};
	
	// check params
	if (!sock || !format)
		return -1;

	// print formatted buffer
	va_start (ap,format);
#ifdef WIN32
	StringCbVPrintfA (buf,sizeof(buf),format,ap);
#else
	vsnprintf(buf, sizeof (buf), format, ap); 
#endif
	va_end (ap);

	// send buffer
	return KSocketsSend (sock,buf,(unsigned long)strlen(buf),SentBytes);
}

/*
*	read an ascii line (until EOL) from socket
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsReadLn(IN ksocket* sock, IN char* buf, IN unsigned long buflen, OPTIONAL OUT unsigned long* ReceivedBytes)
{
	int res = -1;
	char c = 0;
	char* p = NULL;
	int ok = FALSE;
	unsigned long recvlen = 0;

	// check params
	if (!sock || !buf || !buflen)
		return -1;
	memset (buf,0,buflen);
	if(ReceivedBytes)
		*ReceivedBytes = 0;

	// receive data until EOL
	p=buf;
	while (recvlen < buflen)
	{
		// read char by char
		res = KSocketsReceive (sock,p,1,NULL);
		if (res != 0)
			break;
		recvlen++;

		// check for end-of-line
		if (*p == '\r' || *p == '\n')
		{
			c = *p; 
			//*p = '\0';
			if (c == '\n')
			{
				// finish on \n
				ok = TRUE;
				break;
			}
		}

		// read next char
		p++;
	}

	// check errors
	if (!ok)
		return -1;

	if(ReceivedBytes)
		*ReceivedBytes = recvlen;
	return STATUS_SUCCESS;
}

/*
*	close a socket opened with USocksCreate
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsClose(IN ksocket* sock)
{
	int res = -1;

	// check params
	if (!sock)
		return -1;

	// close socket
#ifndef WIN32
	res = close(sock->sock);
#else
	res = closesocket(sock->sock);
#endif
	free (sock);
	if (res != 0)
		return -1;

	return STATUS_SUCCESS;
}

/*
*	create a socket object
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsCreate(IN OUT ksocket** sock)
{
	ksocket* createdsocket = NULL;

	// check params
	if (!sock)
		return -1;
	*sock = NULL;
	
	// allocate memory
	createdsocket = calloc (1,sizeof (ksocket));
	if (!createdsocket)
		return -1;

	// create socket
	createdsocket->sock = (int)socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (createdsocket->sock == INVALID_SOCKET)
	{
		free (createdsocket);
		return -1;
	}

	*sock = createdsocket;
	return STATUS_SUCCESS;
}

/*
 *	returns address from hostname (nbo). hostname can be string or dotted ip (xxx.xxx.xxx.xxx)
 *	returns : -1 on error
 */
NTSTATUS KSocketsGetHostByName (IN char* HostName, OUT unsigned long* pAddress)
{
	struct hostent* pHost = NULL;
	unsigned long addr = 0;

	// check params
	if (!pAddress || !HostName)
		return -1;
	*pAddress = 0;

	// get host address. If it's a numeric string, use standard inet_addr
	pHost = gethostbyname(HostName);
	if (pHost)
	{
		// copy back address
		memcpy (pAddress,pHost->h_addr_list[0],sizeof (unsigned long));
		return STATUS_SUCCESS;
	}
	else
	{
		// try if it's a numeric string
		addr = inet_addr(HostName);
		if (addr != INADDR_NONE)
		{
			// copy back address
			memcpy (pAddress,&addr,sizeof (unsigned long));
			return STATUS_SUCCESS;
		}
	}
	
	return -1;
}

/*
*	connects a socket specifying name and port (nbo)
*	returns : -1 result, or 0
*/
NTSTATUS KSocketsConnectByName (IN ksocket* sock, IN char* pHostName, IN unsigned short usPort)                                                                     
{
	unsigned long dwAddress = 0;

	// check params
	if (!sock || !pHostName || !usPort)
		return -1;

	// get address from name
	if (KSocketsGetHostByName(pHostName,&dwAddress) != 0)
		return -1;
	
	// call real sock connect
	return KSocketsConnect (sock,dwAddress,usPort);

}
