#ifndef __crc32_h__
#define __crc32_h__

/* Return a 32-bit CRC of the contents of the buffer. */
unsigned int crc32(const unsigned char *s, unsigned int len);

#endif // #ifndef __crc32_h__
