#ifndef __lists_h__
#define __lists_h__

#include <stdio.h>
#include <stdlib.h>

#if (defined (WIN32) || defined (WINCE)) && !defined (_KERNELMODE)
#include <winsock2.h>
#endif

#if defined (_KERNELMODE)
#include <ntifs.h>
#endif

#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
#include <w32_defs.h>
#endif


#ifndef InitializeListHead
#define InitializeListHead(ListHead) (\
	(ListHead)->Flink = (ListHead)->Blink = (ListHead))
#endif

#ifndef IsListEmpty
#define IsListEmpty(ListHead) \
	((ListHead)->Flink == (ListHead))
#endif

#ifndef InsertTailList
#define InsertTailList(ListHead,Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_ListHead;\
	_EX_ListHead = (ListHead);\
	_EX_Blink = _EX_ListHead->Blink;\
	(Entry)->Flink = _EX_ListHead;\
	(Entry)->Blink = _EX_Blink;\
	_EX_Blink->Flink = (Entry);\
	_EX_ListHead->Blink = (Entry);\
}
#endif

#ifndef RemoveEntryList
#define RemoveEntryList(Entry) {\
	PLIST_ENTRY _EX_Blink;\
	PLIST_ENTRY _EX_Flink;\
	_EX_Flink = (Entry)->Flink;\
	_EX_Blink = (Entry)->Blink;\
	_EX_Blink->Flink = _EX_Flink;\
	_EX_Flink->Blink = _EX_Blink;\
}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

#ifndef RemoveTailList
#define RemoveTailList(ListHead) \
	(ListHead)->Blink;\
{RemoveEntryList((ListHead)->Blink)}
#endif

#ifndef RemoveHeadList
#define RemoveHeadList(ListHead) \
	(ListHead)->Flink;\
{RemoveEntryList((ListHead)->Flink)}
#endif

/*
 *	tagged value (max value size is 260 wchars = 520 bytes)
 *
 */
#pragma pack(push, 1)
typedef struct _tagged_value {
	unsigned short tag [32];			/// wsz tag
	unsigned short wszvalue [260];		/// wsz value
} tagged_value;
#pragma pack(pop)

/*
 *	simple free of list entries allocated with malloc
 *
 */
#if !defined (WIN32) && !defined (WINCE) && !defined (_KERNELMODE)
static void inline list_free_simple (LIST_ENTRY* list)
#else
void __inline list_free_simple (LIST_ENTRY* list)
#endif
{
	void* entry = NULL;
	
	while (!IsListEmpty(list))
	{
		entry = RemoveHeadList(list);
		if (entry)
			free (entry);
	}
}

#endif
