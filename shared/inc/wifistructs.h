/*
 *	defines strucutes and infrastructure / encryption types for wifi networks
 *	-vx-
 */

#ifndef __wifistructs_h__
#define __wifistructs_h__

#define NDIS802_11_INFRASTRUCTURE_IBSS				0 /// aka "ad-hoc"
#define NDIS802_11_INFRASTRUCTURE_INFRASTRUCTURE	1
#define NDIS802_11_INFRASTRUCTURE_AUTO_UNKNOWN		2

#define NDIS802_11_AUTH_OPEN		0
#define NDIS802_11_AUTH_SHARED		1	/// wep
#define NDIS802_11_AUTH_AUTO		2
#define NDIS802_11_AUTH_WPA			3
#define NDIS802_11_AUTH_WPA_PSK		4
#define NDIS802_11_AUTH_WPA_NONE	5
#define NDIS802_11_AUTH_WPA2		6
#define NDIS802_11_AUTH_WPA2_PSK	7
#define NDIS802_11_AUTH_WPA2_NONE	8

/*
 *	defines accesspoint informations
 *
 */
#pragma pack (push, 1)
typedef struct _BSSIDInfo
{
	unsigned short BSSID[32];/// mac
	unsigned short SSID[32]; /// ssid
	int strength;		/// signal strength (0 weak, 100 max)
	int channel;	/// channel used
	int infrastructure;	/// infrastructure
	int auth;			/// auth
} BSSIDInfo;
#pragma pack (pop)

#define REVERT_BSSIDINFO(_by_) {		\
	BSSIDInfo* _bq_ = (_by_);			\
	_bq_->strength = htonl (_bq_->strength);	\
	_bq_->channel = htonl (_bq_->channel);	\
	_bq_->infrastructure = htonl (_bq_->infrastructure);	\
	_bq_->auth = htonl (_bq_->auth);	\
}

#endif /// #ifndef __wifistructs_h__
