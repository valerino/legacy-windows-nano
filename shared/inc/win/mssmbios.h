#ifndef __mssmbios_h__
#define __mssmbios_h__

/*
*	this is the structure prepended at Buffer result of IoWmiQueryAllData
*
*/
typedef struct _smbios_init_block {
	BYTE  Used20CallingMethod;			/// internal
	BYTE  SMBIOSMajorVersion;			/// smbios maj
	BYTE  SMBIOSMinorVersion;			/// smbios min
	BYTE  DmiRevision;					/// rev
	DWORD Length;						/// size of data
	BYTE  SMBIOSTableData[];			/// data
}smbios_init_block;

/*
 *	these info comes from http://www.dmtf.org/standards/published_documents/DSP0134.pdf
 *
 */

/*
 *	generic BIOS Information (Table 0)
 *
 */
typedef struct _smbios_table_0 {
	BYTE type;						/// must be 0
	BYTE length;					/// length of this table
	WORD  handle;					/// internal
	BYTE vendor_strnum;				/// string# for vendor after this struct
	BYTE biosversion_strnum;		/// string# for biosversion after this struct
	WORD bios_starting_address_segment; /// internal
	BYTE bios_reldate_strnum;		/// string# for biosreleasedate after this struct
	BYTE bios_romsize;				/// internal
	LONGLONG bios_characteristics;	/// internal
} smbios_table_0;

/*
*	generic System Information (Table 1)
*
*/
#define SMBIOS_21_LENGTH 0x19

typedef struct _smbios_table_1 {
	BYTE type;						// must be 1
	BYTE length;					// if >= SMBIOS_21_LENGTH, uuid is included
	WORD  handle;					/// internal
	BYTE manufacturer_strnum;		/// string# for manufacturer after this struct
	BYTE productname_strnum;		/// string# for productname after this struct	
	BYTE version_strnum;			/// string# for productversion after this struct
	BYTE serialnum_strnum;			/// string# for serialnumber after this struct
	BYTE uuid [16];					/// unique id of the machine (mandatory for SMBIOS from 2.3)
} smbios_table_1;

#endif