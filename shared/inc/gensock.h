/*
 *	this include is valid for both the usermode and kernelmode sockets implementation
 *
 */
#ifndef __gensock_h__
#define __gensock_h__

#ifdef __cplusplus
extern "C"
{
#endif

/*
 *	these are valid only in windows kernelmode
 *
 */
#if(defined(_KERNELMODE))
#include <ntifs.h>
#if (NTDDI_VERSION >= NTDDI_LONGHORN)
#include <wsk.h>
#endif /// #if (NTDDI_VERSION >= NTDDI_LONGHORN)

/*
*	network macros
*
*/
#ifndef htonl
#define htonl(l)			\
	((((l)&0xFF000000L)>> 24) | \
	(((l)&0x00FF0000L)>> 8) |   \
	(((l)&0x0000FF00L)<< 8) |   \
	(((l)&0x000000FFL)<< 24))
#endif

#ifndef htons
#define htons(s)			\
	((((s)&0xFF00)>>8) |		\
	(((s)&0x00FF)<<8))
#endif

/*
*	kernel socket
*
*/
#if (NTDDI_VERSION >= NTDDI_LONGHORN)
typedef struct _ksocket {
	IO_STATUS_BLOCK iosb;	/// status of completion operation
	KEVENT evt;				/// signal completion
	PIRP irp;				/// associated irp
	PMDL mdl;				/// mdl for send/recv
	PWSK_SOCKET s;			/// wsk socket
	BOOLEAN connected;		/// connected/disconnected
	BOOLEAN closing;		/// KSocketsClose called
} ksocket;
#else
typedef struct _ksocket
{
	PFILE_OBJECT transport;			/// transport fileobject
	HANDLE		transporthandle;	/// transport handle
	PFILE_OBJECT conn;				/// connection fileobject
	HANDLE		connhandle;			/// connection handle
	BOOLEAN		connected;		/// connected/not connected
} ksocket;
#endif /// #if (NTDDI_VERSION >= NTDDI_LONGHORN)

#else

/*
 *	these are defined only when windows kernelmode isnt defined
 *
 */

#ifndef STATUS_SUCCESS
#define STATUS_SUCCESS 0
#endif

/*
 *	usermode socket
 *
 */
typedef struct _ksocket {
	int sock; /// usermode socket
} ksocket;

#if defined (WIN32) || defined (WINCE)
#include <winsock2.h>
typedef long NTSTATUS;
#else
#include <w32_defs.h>
#endif

#endif /// #if(defined(_KERNELMODE))

#define SOCKET_MEM_SIZE	1024	/// size of the fixed buffer allocated by lookaside list when needed

/*
*	initialize sockets
*
*/
NTSTATUS KSocketsInitialize ();

/*
*	initialize sockets using a deviceobject (for xp compatibility)
*
*/
NTSTATUS KSocketsInitializeWithDeviceObject (IN void* tcpdeviceobject);

/*
*	disconnect socket
*
*/
NTSTATUS KSocketsDisconnect(IN ksocket* sock);

/*
*	connect socket to address:port (both nbo) (non-nbo for kernelmode)
*
*/
NTSTATUS KSocketsConnect(IN ksocket* sock, IN unsigned long Address, IN unsigned short Port);

/*
*	receive a buffer thru socket
*
*/
NTSTATUS KSocketsReceive(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesReceived);

/*
*	sends a buffer thru socket
*
*/
NTSTATUS KSocketsSend(IN ksocket* sock, IN void* Buffer, IN unsigned long SizeBuffer, OPTIONAL OUT unsigned long* BytesSent);

/*
*	print a formatted string to socket. Max string length is SOCKET_MEM_SIZE
*
*/
NTSTATUS KSocketsPrintf (IN ksocket* sock, OPTIONAL OUT unsigned long* SentBytes, IN const char* format, ...);

/*
*	read an ascii line (until EOL) from socket
*
*/
NTSTATUS KSocketsReadLn(IN ksocket* sock, IN char* buf, IN unsigned long buflen, OPTIONAL OUT unsigned long* ReceivedBytes);

/*
*	close a socket opened with KSocketsCreate
*
*/
NTSTATUS KSocketsClose(IN ksocket* sock);

/*
*	create a socket object
*
*/
NTSTATUS KSocketsCreate(IN OUT ksocket** sock);

/*
*	returns address from hostname (nbo). hostname can be string or dotted ip (xxx.xxx.xxx.xxx) (available in usermode only)
*	
*/
NTSTATUS KSocketsGetHostByName (IN char* HostName, OUT unsigned long* pAddress);

/*
*	connects a socket specifying name and port (nbo) (available in usermode only)
*	
*/
NTSTATUS KSocketsConnectByName (IN ksocket* sock, IN char* pHostName, IN unsigned short usPort);

/*
*	finalize sockets (available in usermode only)
*	
*/
NTSTATUS KSocketsFinalize ();

#ifdef __cplusplus
}
#endif

#endif /// #ifndef __gensock_h__

