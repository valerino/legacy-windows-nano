/*
 *	math macros
 *
 */

#ifndef __mathut_h__
#define __mathut_h__

/* convert to nbo (long) */
#ifndef htonl
#define htonl(l)			\
	((((l)&0xFF000000L)>> 24) | \
	(((l)&0x00FF0000L)>> 8) |   \
	(((l)&0x0000FF00L)<< 8) |   \
	(((l)&0x000000FFL)<< 24))
#endif

/* convert to nbo (short) */
#ifndef htons
#define htons(s)			\
	((((s)&0xFF00)>>8) |		\
	(((s)&0x00FF)<<8))
#endif

/*
 *	convert double to nbo
 *
 */
static __inline double htond (double x)   
{
	int * p = (int*)&x;
	int tmp = p[0];

	p[0] = htonl(p[1]);
	p[1] = htonl(tmp);
	return x;
}

/*
 *	convert float to nbo
 *
 */
static __inline float htonf (float x)
{
	int * p = (int *)&x;

	*p = htonl(*p);
	return x;
}

//***********************************************************************
// bit operations                                                                     
//
//
//
//************************************************************************/
#ifndef BitSet
#define BitSet(arg,posn) ((arg) | (1L << (posn)))
#endif
#ifndef BitClr
#define BitClr(arg,posn) ((arg) & ~(1L << (posn)))
#endif
#ifndef BitFlp
#define BitFlp(arg,posn) ((arg) ^ (1L << (posn)))
#endif
#ifndef BitTst
#define BitTst(arg,posn) ((arg) & (1L << (posn)))
#endif

#endif // #ifndef __mathut_h__
