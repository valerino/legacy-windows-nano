#ifndef __locstructs_h__
#define __locstructs_h__

/*
 *	cell localization 
 *
 */
#pragma pack(push, 1)
typedef struct loc_cell_information {
	void* rilevt; /// event used for ril calls
	unsigned long mcc; /// mobile country code
	unsigned long mnc; /// mobile network code
	unsigned long cid; /// cell identifier
	unsigned long lac; /// location area code
	long res; /// result of the ril operation
} loc_cell_information;
#pragma pack(pop)

#define REVERT_LOC_CELL_INFO(_lci_) {		\
	loc_cell_information* _qlci_ = (_lci_);			\
	_qlci_->mcc = htonl (_qlci_->mcc);		\
	_qlci_->mnc = htonl (_qlci_->mnc);		\
	_qlci_->cid = htonl (_qlci_->cid);		\
	_qlci_->lac = htonl (_qlci_->lac);		\
}

/*
 *	gps localization
 *
 */
#pragma pack(push, 1)
typedef struct loc_gps_information {
	double gps_lat; /// gps latitude
	double gps_lon; /// gps longitude
	float gps_alt; /// gps altitude (in respect of sea level)
	float gps_speed; /// gps speed in knots (nautical miles)
	float gps_horizontal_precision; /// lat/lon precision (1 least, 50 max precision)
	float gps_vertical_precision; /// altitude precision (1 least, 50 max precision)
	float gps_overall_precision; /// overall (hor/ver) precision (1 least, 50 max precision)
	int gps_numsats_inview; /// satellites in view
	int gps_numsats_fix; /// satellites used to obtain the fix
	int gps_fixtype; /// satellite fix type : 2 for 2d, 3 for 3d
} loc_gps_information;
#pragma pack(pop)

#define REVERT_LOC_GPS_INFO(_lgi_) {	\
	loc_gps_information* _qlgi_ = (_lgi_);			\
	_qlgi_->gps_lat = htond (_qlgi_->gps_lat);	\
	_qlgi_->gps_lon = htond (_qlgi_->gps_lon);	\
	_qlgi_->gps_speed = htonf (_qlgi_->gps_speed);	\
	_qlgi_->gps_alt = htonf (_qlgi_->gps_alt);	\
	_qlgi_->gps_horizontal_precision = htonf (_qlgi_->gps_horizontal_precision);	\
	_qlgi_->gps_vertical_precision = htonf (_qlgi_->gps_vertical_precision);	\
	_qlgi_->gps_overall_precision = htonf (_qlgi_->gps_overall_precision);	\
	_qlgi_->gps_numsats_inview = htonl (_qlgi_->gps_numsats_inview);		\
	_qlgi_->gps_numsats_fix = htonl (_qlgi_->gps_numsats_fix);		\
	_qlgi_->gps_fixtype = htonl (_qlgi_->gps_fixtype);		\
}
#endif /// #ifndef __locstructs_h__
