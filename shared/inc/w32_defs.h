#ifndef __w32defs_h__
#define __w32defs_h__

/// missing windows stuff on unix
#if (!defined  WIN32) && (!defined WINCE) && (!defined _KERNELMODE)

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <time.h>
#include <dlfcn.h>

/*
 * macros
 *
 */
#define MAKE_PTR(base, value, cast) ((cast)((unsigned char*)base + value))
#define MAKEWORD(a, b)      ((unsigned short)(((unsigned char)(((long)(a)) & 0xff)) | ((unsigned short)((unsigned char)(((long)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((long)(((unsigned short)(((long)(a)) & 0xffff)) | ((unsigned long)((unsigned short)(((long)(b)) & 0xffff))) << 16))

/*
 *	types
 */
typedef void						VOID, *PVOID, *LPVOID;
typedef void* HANDLE;

typedef long						LONG, *LPLONG;
typedef unsigned int				ULONG, *PULONG;
typedef unsigned long long int		ULONGLONG, *PULONGLONG;
typedef long long int				LONGLONG, *PLONGLONG;

typedef int							INT, *PINT;
typedef unsigned int				UINT, *PUINT;

typedef long long					__int64;
typedef unsigned long long 			u_longlong;

typedef unsigned int				DWORD, *PDWORD;
typedef unsigned short				WORD, *PWORD;

typedef unsigned short				USHORT, *PUSHORT;
typedef short						SHORT, *PSHORT;

typedef unsigned short				WCHAR, *PWCHAR;

typedef unsigned char				BYTE, *PBYTE;

typedef unsigned char				UCHAR, *PUCHAR;
typedef char						CHAR, *PCHAR;

typedef int							BOOL, *PBOOL;
typedef unsigned char				BOOLEAN, *PBOOLEAN;    

typedef long NTSTATUS, *PNTSTATUS;

#ifndef STATUS_SUCCESS
#define STATUS_SUCCESS 0
#endif

#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)
#endif

#ifndef SOCKET
#define SOCKET int
#endif

#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif

#ifndef INADDR_NONE
#define INADDR_NONE -1
#endif

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#if defined(_UX64)
typedef int64_t INT_PTR, *PINT_PTR;
typedef uint64_t UINT_PTR, *PUINT_PTR;
typedef int64_t LONG_PTR, *PLONG_PTR;
typedef uint64_t ULONG_PTR, *PULONG_PTR;
#else
typedef int INT_PTR, *PINT_PTR;
typedef unsigned int UINT_PTR, *PUINT_PTR;
typedef long LONG_PTR, *PLONG_PTR;
typedef unsigned long ULONG_PTR, *PULONG_PTR;
#define __int3264   __int32
#endif

/*
*	win32 specific structures
*
*/
#ifndef DEFINED_LIST_ENTRY
#pragma pack(push, 1) 
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *Flink;
	struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;
#pragma pack(pop) 
#endif // #ifndef DEFINED_LIST_ENTRY

#pragma pack(push, 1) 
#if defined(MIDL_PASS)
typedef struct _LARGE_INTEGER {
#else // MIDL_PASS
typedef union _LARGE_INTEGER {
	struct {
		unsigned long LowPart;
		long HighPart;
	};
	struct {
		unsigned long LowPart;
		long HighPart;
	} u;
#endif //MIDL_PASS
	uint64_t QuadPart;
} LARGE_INTEGER;
#pragma pack(pop) 

typedef LARGE_INTEGER *PLARGE_INTEGER;

#pragma pack(push, 1) 
#if defined(MIDL_PASS)
typedef struct _ULARGE_INTEGER {
#else // MIDL_PASS
typedef union _ULARGE_INTEGER {
	struct {
		unsigned long LowPart;
		unsigned long HighPart;
	};
	struct {
		unsigned long LowPart;
		unsigned long HighPart;
	} u;
#endif //MIDL_PASS
	unsigned long long QuadPart;
} ULARGE_INTEGER;
#pragma pack(pop) 

typedef ULARGE_INTEGER *PULARGE_INTEGER;

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef OPTIONAL
#define OPTIONAL
#endif

#ifndef FALSE
#define FALSE 0
#define TRUE 1
#endif

/*
 *	win32 time functions
 *
 */
#pragma pack(push, 1) 
typedef struct _SYSTEMTIME {
	unsigned short wYear;
	unsigned short wMonth;
	unsigned short wDayOfWeek;
	unsigned short wDay;
	unsigned short wHour;
	unsigned short wMinute;
	unsigned short wSecond;
	unsigned short wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;
#pragma pack (pop)

#pragma pack(push, 1) 
typedef struct _FILETIME {  
	unsigned long dwLowDateTime;  
	unsigned long dwHighDateTime;
} FILETIME,  *PFILETIME;
#pragma pack(pop)

/*
 *	win32 system information
 *
 */
#pragma pack(push, 1) 
typedef struct _OSVERSIONINFOEXW {
	unsigned long dwOSVersionInfoSize;
	unsigned long dwMajorVersion;
	unsigned long dwMinorVersion;
	unsigned long dwBuildNumber;
	unsigned long dwPlatformId;
	unsigned short  szCSDVersion[ 128 ];     // Maintenance string for PSS usage
	unsigned short   wServicePackMajor;
	unsigned short   wServicePackMinor;
	unsigned short   wSuiteMask;
	unsigned char  wProductType;
	unsigned char  wReserved;
} OSVERSIONINFOEXW, *POSVERSIONINFOEXW, *LPOSVERSIONINFOEXW, RTL_OSVERSIONINFOEXW, *PRTL_OSVERSIONINFOEXW;
#pragma pack(pop) 

#endif // #if (!defined  WIN32) && (!defined WINCE)


#endif //#ifndef __w32defs.h__


